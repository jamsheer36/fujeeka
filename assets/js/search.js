redrct = '';
redrct_to = '';
$(window).load(function () {
    setTimeout(function () {
        $(".alert").delay(50000).hide();
    }, 5000);
});
$(document).ready(function () {
    $(document).on('submit', '.frmSupplierRegister', function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        var formData = $(this).serializeArray();
        $.ajax({
            type: 'post',
            url: url,
            dataType: 'json',
            data: formData,
            beforeSend: function (xhr) {
                $('.btnSupplierRegister').html('Please wait...');
                $('.btnSupplierRegister').prop('disabled', true);
            },
            success: function (resp) {
                messageBox(resp);
            },
            complete: function (jqXHR, textStatus) {
                // messageBox(jqXHR.responseJSON);
                $('.btnSupplierRegister').html('Send Request');
                if (jqXHR.responseJSON.status === 'success') {
                    $('.frmSupplierRegister')[0].reset();
                }
                $('.btnSupplierRegister').prop('disabled', false);
            }
        });
    });


    $(document).on('submit', '.frmLogisticsQuote', function (e) {
        e.preventDefault();
        var url = $(this).attr('action');

        $.ajax({
            type: 'post',
            url: url,
            dataType: 'json',
            data: $('.frmLogisticsQuote').serializeArray(),
            beforeSend: function (xhr) {
                $('.btnSubmitLogisticQuote').html('Please wait...');
                $('.btnSubmitLogisticQuote').prop('disabled', true);
            },
            success: function (resp) {
                $('#inq-pop').modal('toggle');
                messageBox(resp);
            }
        });
    });

    /*
     * ----------------------------------------------------------------------------------------
     message box
     * ----------------------------------------------------------------------------------------
     */
    function messageBox(response) {
        $('.' + response.status + '_msgBox').show();
        $(".msgBoxAj").show();
        $("." + response.status + "_msg").html(response.msg);
        $('.' + response.status + '_msgBox').delay(2000).fadeOut('slow');
        window.setTimeout(function () {
            // dont change this, change will break chat notification.....tested
            $(".alert").css('display', 'none');
        }, 2000);
    }


    /*----------------------------------------------------*/
    /* Main-Menu background Change
     /*----------------------------------------------------*/
    $(document).on('click', '.btnFav', function (e) {
        var _this = $(this);
        var url = $(this).attr('data-url');
        var islogged = $(this).attr('is-logged');
        if (islogged == 1) {
            $.ajax({
                type: 'post',
                url: url,
                dataType: 'json',
                success: function (resp) {
                    var childCount = _this.children('#fav').length;
                    if (resp.action == 'c') {
                        if (childCount === 0) {
                            _this.toggleClass('heart-blinking');
                        }
                        _this.children("#fav").toggleClass("fas red-icn");
                    } else {
                        if (childCount === 0) {
                            _this.removeClass('selected').removeClass('heart-blinking').addClass('heart');
                        }
                        _this.children("#fav").removeClass("fas red-icn");
                    }
                    messageBox(resp);
                }
            });
        } else {
            var obj = jQuery.parseJSON('{ "msg": "Please login to favorite product", "status":"success"}');
            messageBox(obj);
            location.href = site_url + "user/login?caldp=" + islogged;
        }
    });

    $(document).on('click', '.btnFollow', function (e) {
        var _this = $(this);
        var url = $(this).attr('data-url');
        var islogged = $(this).attr('is-logged');
        if (islogged == 1) {
            $.ajax({
                type: 'post',
                url: url,
                dataType: 'json',
                success: function (resp) {
                    if (resp.action == 'c') {
                        _this.addClass("green-icn");
                    } else {
                        _this.removeClass("green-icn");
                    }
                    messageBox(resp);
                }
            });
        } else {
            var obj = jQuery.parseJSON('{ "msg": "Please login to follow supplier", "status":"success"}');
            messageBox(obj);
            location.href = site_url + "user/login?caldp=" + islogged;
        }
    });
    /*
     * ----------------------------------------------------------------------------------------
     Quick search
     * ----------------------------------------------------------------------------------------
     */
    //     $(document).on('keyup', '.txtQuickSearch', function(e) {
    //         search();
    //     });
    // $(document).on('click', '#search-modal .btnSearchMaster', function() {
    //     atr = $(".sup_home").attr("data-from");
    //     if (atr == "sup_home") {
    //         if ($('#search-modal .dropdown-menu').hasClass("show")) {
    //             $('#search-modal .dropdown-menu').removeClass("show");
    //         } else {
    //             $('#search-modal .dropdown-menu').addClass("show");
    //         }
    //     }
    // });


    $(document).on('click', '.btnQuickSearch', function () {
        atr = $(".sup_home").attr("data-from");
        if (atr == "sup_home") {
            var seg = $('#search-modal .btnSearchMaster').html().toLowerCase().trim();
            var keyWord = $('#search-modal .commonAutoComplete').val();
            var url = $(this).attr('data-url') + '/' + keyWord + '?seg=' + seg;
            if (seg == "product") {
                url = site_url + 'product/product-listing?SearchText=' + keyWord;
            }
        } else {
            var seg = $('.btnSearchMaster').html().toLowerCase().trim();
            var keyWord = $('.commonAutoComplete').val();
            var url = $(this).attr('data-url') + '/' + keyWord + '?seg=' + seg;
            if (seg == "product") {
                url = site_url + 'product/product-listing?SearchText=' + keyWord;
            }
        }

        if (keyWord != '') {
            location.href = url.replace(/ /g, "+");
        }
    });
    $('#ser_text').keypress(function (e) {
        var key = e.which;
        if (key == 13) // the enter key code
        {
            $('.btnQuickSearch').click();
        }
    });

    function search() {

        var sec = $('.btnSearchMaster').html().toLowerCase();
        var key = $('.txtQuickSearch').val();
        var url = $('.btnSearchMaster').attr('data-url');
        if (key !== '') {
            $.ajax({
                type: 'post',
                url: url,
                data: {
                    section: sec,
                    keyword: key
                },
                success: function (resp) {
                    $('.divSearchResult').html(resp);
                }
            });
        }
        $('.divSearchResult').html('');
    }




    $(document).on('keydown.commonAutoComplete', '.commonAutoComplete', function () {
        if ($(this).hasClass('txtQuickSearch')) {
            var sec = $('.btnSearchMaster').html().toLowerCase();
            var url = $('.btnSearchMaster').attr('data-url');
            $(this).autocomplete({
                dataType: 'json',
                serviceUrl: url + '?section=' + sec,
                onSelect: function (suggestion) {
                    if (sec == 'product') {
                        location.href = site_url + 'product/product-listing?SearchText=' + suggestion.value;
                    }
                }
            });
        } else if ($(this).hasClass('Infrengemntprdct')) {
            var url = $('.btnProductSearch').attr('data-url');
            $(this).autocomplete({
                dataType: 'json',
                serviceUrl: url + '?section=Product',
                onSelect: function (suggestion) {
                    var key = suggestion.value;
                    var url = site_url + 'home/productsearch';

                    if (key !== '') {
                        $.ajax({
                            type: 'post',
                            url: url,
                            dataType: 'json',
                            data: {
                                keyword: key,
                                page: 1
                            },
                            success: function (resp) {

                                $('#datadiv').html(resp.data);


                            }
                        });
                    }


                }
            });
        } else {
            aa = $("#sell2").val() ? $("#sell2").val() : $("#sell1").val();
            $(this).autocomplete({
                dataType: 'json',
                serviceUrl: $(this).attr('data-url') + '/' + aa,
                onSelect: function (suggestion) {
                    $("#prd_id").val(suggestion.data);
                    // $("#moq_span").text("*MOQ for this product is " + suggestion.moq);
                    // $('.inp_qty').attr("min", suggestion.moq);
                    $(".unit_sel").val(suggestion.unit);

                }
            });
        }
    });






    $(document).on('click', 'body', function () {
        $('.divSearchResult').html('');
    });

    /**
     * Subscribe Newsletter
     */
    $(document).on('submit', '.frmNewsletterSub', function (e) {
        e.preventDefault();
        var submitButtom = $("button", this);
        var email = $('.txtNewsSubEmail').val();
        var url = $('.frmNewsletterSub').attr('data-url');

        if (email !== '') {
            $.ajax({
                type: 'post',
                url: url,
                data: {
                    email: email
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    $('.newsLtrMsg').html('');
                    submitButtom.html('Please wait...');
                    submitButtom.prop('disabled', true);
                },
                success: function (resp) {
                    submitButtom.html('Subscribe');
                    submitButtom.prop('disabled', false);
                    $('.newsLtrMsg').html(resp.msg);
                    $('.txtNewsSubEmail').val('');
                }
            });
        }
    });
});
$('#loginModal').on('shown.bs.modal', function (event) {
    var triggerElement = $(event.relatedTarget); // Button that triggered the modal
    redrct = triggerElement.data('redrct');
    redrct_to = triggerElement.data('redrct_to');
});
$("#modal_lgn_btn").click(function () {
    sec = $(this).attr("data-sec");
    if (sec == "fpswd_mail_submit") {
        forgot_password();
    }
    if (sec == "otp_verification") {
        verify_otp();
    }
    if (sec == "update_paswd") {
        update_password();
    }
    if (sec == "login") {
        identity = $("#identity").val();
        password = $("#password").val();
        $("#u_error").html('');
        $("#p_error").html('');
        if (!identity) {
            $("#u_error").html('Enter username');
        }
        if (!password) {
            $("#p_error").html('Enter password');
        }
        if (typeof redrct_to === "undefined") {
            redirect_to = '';
        } else
            redirect_to = redrct_to;
        if (typeof redrct === "undefined") {
            redirect_url = '';
        } else {
            redirect_url = redrct;
        }
        if (password && identity) {
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: site_url + 'user/quick_login',
                data: {
                    'identity': identity,
                    'password': password,
                    'redirect_url': redrct,
                    'redirect_to': redrct_to,
                },
                beforeSend: function () {
                    $("#modal_lgn_btn").prop('disabled', 'true');
                    $("#modal_lgn_btn").html('Please wait <i class="fa fa-spinner fa-spin"></i>');
                },
                success: function (resp) {
                    if (resp.status == 'failed') {
                        $("#p_error").html(resp.msg);
                        setTimeout(function () { $("#p_error").html(''); }, 3000);
                        $("#modal_lgn_btn").prop('disabled', '');
                        $("#modal_lgn_btn").text('Login');
                    }
                    if (resp.status == 'success') {
                        $("#loginModal").modal("hide");
                        if (resp.redirect) {
                            if (resp.redirect == "trigger_#chat") {
                                window.location.reload();
                            } else
                                window.location.href = resp.redirect;
                        } else
                            window.location.reload();
                    }
                }
            });
        }
    }

    return false;
});
$("#a_fpwd").click(function () {
    $(".lgn_grp").addClass("d-none");
    $(".fpswd_grp").removeClass("d-none");
    $("#modal_lgn_btn").text('Submit');
    $("#modal_lgn_btn").attr('data-sec', 'fpswd_mail_submit');
    $("#a_fpwd").addClass("d-none");
    $("#a_lgn").removeClass("d-none");
});
$("#a_lgn").click(function () {
    $(".lgn_grp").removeClass("d-none");
    $(".fpswd_grp").addClass("d-none");
    $("#modal_lgn_btn").text('Login');
    $("#a_lgn").addClass("d-none");
    $("#a_fpwd").removeClass("d-none");
    $("#modal_lgn_btn").attr('data-sec', 'login');
    $("#modal_lgn_btn").prop('disabled', '');
    $("#modal_lgn_btn").text('Login');
});

function forgot_password() {
    email = $("#email").val();
    $("#m_error").html('');
    if (!email) {
        $("#m_error").html('Enter email');
    }
    regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    is_mail_valid = regex.test(email);
    if (!is_mail_valid && email) {
        $("#m_error").html('Email not valid');
    }
    if (email && is_mail_valid) {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: site_url + 'user/forgot-password',
            data: {
                'email': email
            },
            beforeSend: function () {
                $("#modal_lgn_btn").prop('disabled', 'true');
                $("#modal_lgn_btn").html('Please wait <i class="fa fa-spinner fa-spin"></i>');
            },
            success: function (resp) {
                if (resp.status == 'fail') {
                    $("#m_error").html(resp.msg);
                    setTimeout(function () { $("#m_error").html(''); }, 3000);
                    $("#modal_lgn_btn").prop('disabled', '');
                    $("#modal_lgn_btn").text('Submit');
                }
                if (resp.status == 'success') {
                    $(".fpswd_grp").addClass("d-none");
                    $(".otp_grp").removeClass("d-none");
                    $("#a_lgn").addClass("d-none");
                    $("#a_resend_mail").removeClass("d-none");
                    $("#mail_success").html(resp.msg);
                    $("#resend_mail_ip").val(email);
                    $("#forgot_code").val(resp.frgtPassCode);

                    setTimeout(function () { $("#mail_success").html(''); }, 5000);
                    $("#modal_lgn_btn").prop('disabled', '');
                    $("#modal_lgn_btn").text('Verify OTP');
                    $("#modal_lgn_btn").attr('data-sec', 'otp_verification');
                }
            }
        });
    }
}

function verify_otp() {
    otp = $("#otp").val();
    $("#otp_error").html('');
    if (!otp) {
        $("#otp_error").html('Enter OTP');
    }
    if (otp) {
        forgot_code = $("#forgot_code").val();
        email = $("#resend_mail_ip").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: site_url + 'user/forgot-password',
            data: {
                'email': email,
                'forgot_code': forgot_code,
                'forgot_otp': otp,
            },
            beforeSend: function () {
                $("#modal_lgn_btn").prop('disabled', 'true');
                $("#modal_lgn_btn").html('Verifying <i class="fa fa-spinner fa-spin"></i>');
            },
            success: function (resp) {
                if (resp.st == 'fail') {
                    $("#otp_error").html(resp.msg);
                    setTimeout(function () { $("#otp_error").html(''); }, 3000);
                    $("#modal_lgn_btn").prop('disabled', '');
                    $("#modal_lgn_btn").text('Verify OTP');
                }
                if (resp.st == 'suc') {
                    $(".change_grp").removeClass("d-none");
                    $(".otp_grp").addClass("d-none");
                    $("#a_resend_mail").addClass("d-none");
                    $("#rest_code").val(forgot_code);
                    $("#modal_lgn_btn").prop('disabled', '');
                    $("#modal_lgn_btn").text('Update Password');
                    $("#modal_lgn_btn").attr('data-sec', 'update_paswd');
                }
            }
        });
    }
}

function update_password() {
    pswd = $("#pswd").val();
    cpswd = $("#cpswd").val();
    code = $("#rest_code").val();
    $("#cpswd_error").html('');
    $("#pswd_error").html('');
    flag = true;
    if (!pswd) {
        flag = false;
        $("#pswd_error").html('Enter new password');
    }
    if (pswd.length < 8) {
        flag = false;
        $("#pswd_error").html('Enter at least 8 characters');
    }
    if (!cpswd) {
        flag = false;
        $("#cpswd_error").html('Enter confirm password');
    }
    if (pswd != cpswd) {
        flag = false;
        $("#cpswd_error").html('Password mismatch');
    }
    if (flag) {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: site_url + 'user/reset_password_modal',
            data: {
                'pswd': pswd,
                'cpswd': cpswd,
                'code': code
            },
            beforeSend: function () {
                $("#modal_lgn_btn").prop('disabled', 'true');
                $("#modal_lgn_btn").html('Processing <i class="fa fa-spinner fa-spin"></i>');
            },
            success: function (resp) {
                if (resp.status == 'fail') {
                    $("#cpswd_error").html(resp.msg);
                    setTimeout(function () { $("#cpswd_error").html(''); }, 3000);
                    $("#modal_lgn_btn").prop('disabled', '');
                    $("#modal_lgn_btn").text('Update Password');
                }
                if (resp.status == 'success') {
                    $("#reset_success").html(resp.msg);
                    $(".change_grp").addClass("d-none");
                    $(".lgn_grp").removeClass("d-none");
                    $("#a_fpwd").removeClass("d-none");
                    $("#modal_lgn_btn").prop('disabled', '');
                    $("#modal_lgn_btn").text('Login');
                    $("#modal_lgn_btn").attr('data-sec', 'login');
                    setTimeout(function () { $("#reset_success").html(''); }, 5000);
                }
            }
        });
    }

}
$("#a_resend_mail").click(function () {
    email = $("#resend_mail_ip").val();
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: site_url + 'user/forgot-password',
        data: {
            'email': email
        },
        beforeSend: function () {
            $("#modal_lgn_btn").prop('disabled', 'true');
            $("#modal_lgn_btn").html('Sending <i class="fa fa-spinner fa-spin"></i>');
        },
        success: function (resp) {
            if (resp.status == 'fail') {
                $("#m_error").html(resp.msg);
                setTimeout(function () { $("#m_error").html(''); }, 3000);
                $("#modal_lgn_btn").prop('disabled', '');
                $("#modal_lgn_btn").text('Verify OTP');
            }
            if (resp.status == 'success') {
                $("#mail_success").html(resp.msg);
                $("#resend_mail_ip").val(email);
                $("#forgot_code").val(resp.frgtPassCode);

                setTimeout(function () { $("#mail_success").html(''); }, 5000);
                $("#modal_lgn_btn").prop('disabled', '');
                $("#modal_lgn_btn").text('Verify OTP');
            }
        }
    });
});

$(function () {
    $("#modal_lgn_form input").keypress(function (e) {
        if (e.keyCode == 13) {
            $("#modal_lgn_btn").trigger("click");
        }
    });
});
$('#loginModal').on('hidden.bs.modal', function (e) {
    $(".otp_grp").addClass("d-none");
    $("#a_resend_mail").addClass("d-none");
    $(".change_grp").addClass("d-none");
    $(".fpswd_grp").addClass("d-none");
    
    $(".lgn_grp").removeClass("d-none");
    
    $("#a_lgn").addClass("d-none");
    $("#a_fpwd").removeClass("d-none");
    $("#modal_lgn_btn").prop('disabled', '');
    $("#modal_lgn_btn").text('Login');
    // $('#modal_lgn_form').trigger("reset");
})