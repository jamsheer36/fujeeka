sup = '';

function load_page_data(page, sup = '', cate = '', order_by = '', new_sort = $(".new_sort:checked").val()) {
    // alert(new_sort);
    prd_type = $("#prd_type").val();
    if (new_sort != '') {
        prd_type = new_sort;
    }
    $.ajax({
        url: site_url + "product/product_listing/" + page + "?page=" + page + "&category=" + cate + "&order_by=" + order_by + "&suplier=" +
            sup + "&searchText=" + getUrlParameter('SearchText') + "&prd_type=" + prd_type,
        method: "GET",
        dataType: "json",
        beforeSend: function() {
            $(".ajax_loader1").removeClass("d-none");
        },
        complete: function() {
            $(".ajax_loader1").addClass("d-none");
        },
        success: function(data) {
            $('#product_listing').html(data.data);
            $('#pagination_link').html(data.pagination_link);

        }
    });
}

function load_sub_categories(cate) {
    $.ajax({
        url: site_url + "category/getSubCategories/" + cate,
        method: "GET",
        dataType: "json",
        success: function(data) {
            $('.sub_cate').html(data.data);
        }
    });
}

load_page_data(1);

$(document).on("click", ".pagination li a", function(event) {
    event.preventDefault();
    ref = $(this).attr('href');
    page = ref.substring(ref.lastIndexOf("/") + 1, ref.length);
    cate = '';
    order_by = '';
    if ($("#sel1").val() != '') {
        cate = $("#sel1").val();
    }
    if (typeof($("#sell2").val()) !== "undefined" && $("#sell2").val() != '') {
        cate = $("#sell2").val();
    }
    if ($("#sort_by").val() != '') {
        order_by = $("#sort_by").val();
    }
    if (page == '')
        page = 1;
    load_page_data(page, sup, cate, order_by);
    window.scrollTo(0, 0);
});

$(document).on("change", "#sel1", function() {
    $("#sell2").val('');
    cate = $("#sel1").val();
    if (cate == '') {
        $("#sell2").addClass('d-none');
    } else {
        $("#sell2").removeClass('d-none');
    }
    order_by = $("#sort_by").val();
    load_sub_categories(cate);
    load_page_data(1, sup, cate, order_by);
});
$(document).on("change", "#sell2", function() {
    cate = $("#sel1").val();
    order_by = $("#sort_by").val();

    if ($("#sell2").val() != '') {
        cate = $("#sell2").val();
    }
    load_page_data(1, sup, cate, order_by);
});


$(document).on("change", "#sort_by", function() {
    order_by = $(this).val();
    cate = $("#sel1").val();
    if (typeof($("#sell2").val()) !== "undefined" && $("#sell2").val() != '') {
        cate = $("#sell2").val();
    }
    load_page_data(1, sup, cate, order_by);
});

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
        return '';
    }
}

function loadNewSort(e) {
    if ($('#' + e).prop("checked") == false) {
        history.replaceState('', '', site_url + 'product/product-listing');
        $('#' + e).attr('checked', false);
    } else {
        $(".new_sort").attr('checked', false);
        $('#' + e).prop('checked', 'true');
        history.replaceState('', '', site_url + 'product/product-listing?type=' + $('#' + e).val());
    }
    $("#sel1").val('');
    $("#sell2").val('');
    $("#sort_by").val('')
    $("#sell2").addClass('d-none');
    load_page_data(1, '', '', '');
}