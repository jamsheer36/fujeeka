var busy = false;
var limit = 40;
var offset = 40;

$("#results").on("click", ".360_li", function() {
    // $(".360_li").click(function() {
    $(".360_li").removeClass('select');
    $(this).addClass('select');
    fsrc = $(this).attr('data-fsrc');
    $("#360_frame").attr("src", fsrc);
    link = $(this).attr('data-sup_link');
    $("#sup_href").attr("href", link);
});
$('.searchBox').on('keyup', function(e) {
    var query = $.trim($(this).val()).toLowerCase();
    $('.names').each(function() {
        var $this = $(this);
        if ($this.text().toLowerCase().indexOf(query) === -1)
            $this.closest('.360_li').hide();
        else $this.closest('.360_li').show();
    });
});

var elements = document.getElementsByClassName("single-prod");
var i;

function listView() {
    $(".btn_list").addClass("btn-selected");
    $(".btn_grid").removeClass("btn-selected");
    for (i = 0; i < elements.length; i++) {
        $(".single-prod").addClass("list-style");
        $(".single-prod").removeClass("grid-style");
    }
}

function gridView() {
    $(".btn_list").removeClass("btn-selected");
    $(".btn_grid").addClass("btn-selected");
    for (i = 0; i < elements.length; i++) {
        $(".single-prod").removeClass("list-style");
        $(".single-prod").addClass("grid-style");
    }
}

function displayRecords(lim, off, from = '') {
    url = site_url + "supplier/get_360_list";
    cat_id = $("#category").val();
    mar_id = $("#market").val();
    build_id = $("#building").val();
    type = true;
    $.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        data: "limit=" + lim + "&offset=" + off + "&category=" + cat_id + "&market=" + mar_id + "&building=" + build_id + "&type=" + type,
        cache: false,
        beforeSend: function() {
            // $("#results").html("<i style='color:#00b904;margin:1% 50%' class='fa fa-spinner fa-spin fa-3x'></i>");
        },
        success: function(html) {
            if (from == 'scroll') {
                $("#results").append(html.details);
            } else {
                $("#results").html(html.details);
            }
        }
    });
}
$(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() > $("#results").height() && !busy) {
        busy = true;
        offset = limit + offset;
        displayRecords(limit, offset, 'scroll');

    }
});
$(".filter").change(function() {
    limit = 40;
    offset = 0;
    displayRecords(limit, offset);
});