/* menu color */
$(document).ready(function() {
    $(".nav-link").click(function() {
        $(".nav-link").removeClass("active");
        // $(".tab").addClass("active"); // instead of this do the below
        $(this).addClass("active");
    });
});


/* Search bar */

$(document).ready(function(e) {
    $('.search-panel .dropdown-menu').find('a').click(function(e) {
        e.preventDefault();
        var param = $(this).attr("href").replace("#", "");
        var concept = $(this).text();
        $('.search-panel span#search_concept').text(concept);
        $('.input-group #search_param').val(param);
    });
    $(".chat-pop").click(function() {
        $('.chat-options').toggle();
    });
    $(".chat_with").click(function() {
        $('.chat-options').hide();
    });
});

/* tooltip */

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

/* QR Code */

$(document).ready(function() {
    $('st-actionContainer').launchBtn({ openDuration: 500, closeDuration: 300 });
});

/*sticky right div*/
// $(document).ready(function() {
//     $(".fav").click(function() {
//         $("#fav").toggleClass("fas red-icn2");
//     });
// });

// $(document).ready(function() {
//     $('#fav').click(function() {
//         $('.alert').show()
//     })
// });



/* category slider */
/*------------------------------------------------------*/
/* Latest Product Slider
/*-----------------------------------------------------*/

// $('#catgory').lightSlider({
//     adaptiveHeight: false,
//     item: 5,
//     slideMargin: 0,
//     loop: true,
//     pager: false,
//     controls: true,
//     auto: false,
//     responsive: [{
//             breakpoint: 959,
//             settings: {
//                 item: 2,
//                 slideMove: 1,
//                 slideMargin: 10
//             }
//         },
//         {
//             breakpoint: 580,
//             settings: {
//                 item: 1,
//                 slideMove: 1,
//             }
//         }
//     ]
// });

/* 360 modal */


function loadChatbox(id, name) {
    usr_id = id;
    $('.chat_with_name').text(name);
    $('#ddd').attr('src', site_url + 'user/chat_head/' + usr_id);

    var e = document.getElementById("minim-chat");
    e.style.display = "block";
    var e = document.getElementById("maxi-chat");
    e.style.display = "none";
    var e = document.getElementById("chatbox");
    e.style.margin = "0";

    var h = $('.chat_append');
    var height = h[0].scrollHeight;
    h.scrollTop(height);
}

function closeChatbox() {
    var e = document.getElementById("chatbox");
    e.style.margin = "0 0 -1500px 0";
}

function minimChatbox() {
    var e = document.getElementById("minim-chat");
    e.style.display = "none";
    var e = document.getElementById("maxi-chat");
    e.style.display = "block";
    var e = document.getElementById("chatbox");
    e.style.margin = "0 0 -455px 0";
}

function maximChatbox() {
    var e = document.getElementById("minim-chat");
    e.style.display = "block";
    var e = document.getElementById("maxi-chat");
    e.style.display = "none";
    var e = document.getElementById("chatbox");
    e.style.margin = "0";
}


function messageBox(response) {
    $('.' + response.status + '_msgBox').show();
    $(".msgBoxAj").show();
    $("." + response.status + "_msg").html(response.msg);
    $('.' + response.status + '_msgBox').delay(2000).fadeOut('slow');
}