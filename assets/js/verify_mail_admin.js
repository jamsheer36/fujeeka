$(document).on('click', '.ver-email', function() {
    if ($(".show_mail_inp").text() != "Cancel") {
        $.ajax({
            type: 'post',
            url: site_url + "user/verify_mail",
            dataType: 'json',
            data: {
                'type': 'send'
            },
            beforeSend: function() {
                // $('.ver-email').attr('disabled', 'disabled');
                $('.ver-email').bind('click', false);
                $(".ver-email").html('<i class="fa fa-spinner fa-spin"></i> Sending');
            },
            success: function(resp) {

                $('#verif-mail').modal('hide');
                $('.ver-email').unbind('click', false);
                $(".ver-email").text('Resend Email');
                message(resp);
            }
        });
    } else {
        $('.mail-form').parsley().validate();
        if ($('.mail-form').parsley().isValid() != false) {
            email = $("#mail_inp").val();
            $.ajax({
                url: site_url + "user/change_mail",
                method: "POST",
                dataType: "json",
                data: {
                    'new_mail': email,
                },
                beforeSend: function() {
                    $('.ver-email').bind('click', false);
                    $(".ver-email").html('<i class="fa fa-spinner fa-spin"></i> Sending');
                },
                complete: function() {
                    $('.ver-email').unbind('click', false);
                    // $(".ver-email").text('Resend Email');

                },
                success: function(data) {
                    if (data.status == "success") {
                        $('#verif-mail').modal('hide');
                        $("#ver_mail_span").removeClass("hidden");
                        $("#mail_inp").addClass("hidden");
                        $(".ver-email").text('Resend Email');
                        $(".show_mail_inp").text('Change email');
                        $("#ver_mail_span").text("(" + email + ") to activate all the features.");
                        message(data);
                    } else {
                        $(".ver-email").text('update and Resend');
                        $(".show_mail_inp").text('Cancel');
                        $("#par_error").text(data.msg);
                        $("#par_error").css("color", "red");
                    }
                    // message(data);
                }
            });
        }
    }
});


function message(data) {
    $('.success_msgBox1').show();
    if (data.status == "success") {
        $('.success_msgBox1').show();
        $(".success_msg1").html(data.msg);
        setTimeout(
            function() {
                $('.fail_msgBox1').css('display', 'none');
            }, 4000);

    } else {
        $('.fail_msgBox1').css('display', 'block');
        $('.fail_msg1').html(data.msg);
        setTimeout(
            function() {
                $('.fail_msgBox1').css('display', 'none');
            }, 4000);

    }

}
$(".show_mail_inp").click(function() {
    console.log($(".show_mail_inp").text());

    if ($(".show_mail_inp").text() != "Cancel") {
        $("#ver_mail_span").addClass("hidden");
        $("#mail_inp").removeClass("hidden");
        $("#mail_inp").focus();
        $(".ver-email").text('update and Resend');
        $(".show_mail_inp").text('Cancel');
    } else {
        $("#ver_mail_span").removeClass("hidden");
        $("#mail_inp").addClass("hidden");
        $(".ver-email").text('Resend Email');
        $(".show_mail_inp").text('Change email');
        $("#par_error").remove();
    }

})