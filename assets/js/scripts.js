/*
 * ----------------------------------------------------------------------------------------
 Scripts carried from homepage
 * ----------------------------------------------------------------------------------------
 */

$(document).ready(function(e) {
    function messageBox(response) {
        $('.' + response.status + '_msgBox').show();
        $(".msgBoxAj").show();
        $("." + response.status + "_msg").html(response.msg);
        $('.' + response.status + '_msgBox').delay(2000).fadeOut('slow');
    }

    /*Tooltip*/
    $('[data-toggle="tooltip"]').tooltip();
});


/*----------------------------------------------------*/
/* Main-Menu background Change
 /*----------------------------------------------------*/
$(window).on('scroll', function() {
    if ($(window).scrollTop() > 100) {
        $('.header-top-area').addClass('stick-menu');
        $('.logo img').attr('src', 'images/fujeeka-logo-green.png');
        $(".autocomplete-suggestions").remove();
    } else {
        $('.header-top-area').removeClass('stick-menu');
        $('.logo img').attr('src', 'images/fujeeka-logo-g.png');
        $(".autocomplete-suggestions").remove();
    }
});



/*----------------------------------------------------*/
/* Homepage Slider
 /*----------------------------------------------------*/

$('.slide').slide({
    'slideSpeed': 3000,
    'isShowArrow': true,
    'isShowDots': false,
    'isHoverStop': false,
    'dotsEvent': 'mouseenter',
    'isLoadAllImgs': true
});

/*----------------------------------------------------*/
/* Search bar
 /*----------------------------------------------------*/

$(document).ready(function(e) {
    $('.search-panel .dropdown-menu').find('a').click(function(e) {
        e.preventDefault();
        var param = $(this).attr("href").replace("#", "");
        var concept = $(this).text();
        $('.search-panel span#search_concept').text(concept);
        $('.input-group #search_param').val(param);
    });
});



/*------------------------------------------------------*/
/* Latest Product Slider
 /*-----------------------------------------------------*/

$('#latest-prod').lightSlider({
    adaptiveHeight: false,
    item: 1,
    slideMargin: 0,
    loop: true,
    pager: false,
    controls: false,
});

/*------------------------------------------------------*/
/* Hot selling Product Slider
 /*-----------------------------------------------------*/

$('#hot-prod').lightSlider({
    adaptiveHeight: false,
    item: 5,
    slideMargin: 0,
    loop: true,
    pager: false,
    controls: true,
    auto: false,
    responsive: [{
            breakpoint: 959,
            settings: {
                item: 2,
                slideMove: 1,
                slideMargin: 10
            }
        },
        {
            breakpoint: 580,
            settings: {
                item: 1,
                slideMove: 1,
            }
        }
    ]
});

/*------------------------------------------------------*/
/* Stock Product Slider
 /*-----------------------------------------------------*/

$('#stock-prod').lightSlider({
    adaptiveHeight: false,
    item: 1,
    slideMargin: 0,
    loop: true,
    pager: false,
    controls: false,
});

/*------------------------------------------------------*/
/* advt Slider
 /*-----------------------------------------------------*/

$('#advt').lightSlider({
    adaptiveHeight: false,
    item: 1,
    slideMargin: 0,
    loop: true,
    pager: true,
    controls: false,
    pause: 2600,
    mode: 'fade'
});

/*------------------------------------------------------*/
/* Recent Product Slider
 /*-----------------------------------------------------*/

$('#recent-prod').lightSlider({
    adaptiveHeight: false,
    item: 7,
    slideMargin: 0,
    loop: true,
    pager: false,
    controls: true,
    auto: false,
    responsive: [{
            breakpoint: 959,
            settings: {
                item: 3,
                slideMove: 1,
                slideMargin: 10
            }
        },
        {
            breakpoint: 580,
            settings: {
                item: 2,
                slideMove: 1,
            }
        }
    ]
});


/*------------------------------------------------------*/
/* tooltip
 /*-----------------------------------------------------*/

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});


// Auto close alert box
//window.setTimeout(function () {
//     $(".alert-success").fadeTo(500, 0).slideUp(500, function () {
////          $(this).remove();
//     });
//}, 3000);


/*----------------------------------------------*/
/*stock scroll */

$(function() {
    $('#marquee-vertical').marquee();
});