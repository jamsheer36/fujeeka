$(function() {
    $(".numOnly").keypress(function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $(document).on('change', '.bindToDropdown', function() {
        var id = $(this).val();
        var url = $(this).attr('data-url');
        var bind = $(this).attr('data-bind');
        var defaultSelect = $(this).attr('data-dflt-select');

        $.ajax({
            type: 'post',
            url: url,
            dataType: 'json',
            data: {
                id: id
            },
            success: function(resp) {
                $('.' + bind).html('');
                $('.' + bind).append('<option value="0">' + defaultSelect + '</option>');
                $.each(resp, function(index, value) {
                    $('.' + bind).append('<option value="' + value.col_id + '">' + value.col_title + '</option>');
                });
            }
        });
    });
    $(document).on('blur', '#email', function() {
        $('#uname_span').text($(this).val());
        $('#mail_exist').text('');

        site_url = $('#site_url').val();
        var emailid = $(this).val();

        $.ajax({
            type: 'post',
            url: site_url + "user/checkEmailExists",
            dataType: 'json',
            data: {
                email: emailid
            },
            success: function(resp) {
                if (resp.status === 'false') {
                    $('#mail_exist').html(resp.msg);
                    $('.nxt_link_btn').attr('disabled', 'disabled');
                    $('.nxt_link').addClass('disabled');
                } else {
                    $('.nxt_link_btn').prop("disabled", false);
                    $('.nxt_link').removeClass('disabled');
                }
            }
        });
    });

    $(".frmSignUpStepFirst").validate({
        rules: {
            usr_email: {
                required: true,
                email: true
            },
            usr_captcha: {
                required: true,
                equalTo: "#captcha_code"
            }
        },
        messages: {
            usr_email: "Please enter a valid email address",
            usr_captcha: "Invalid text entered"
        },
         submitHandler: function(form) {
             $('.btnSubmitFirstStep').html('Please wait...');
             $('.btnSubmitFirstStep').prop('disabled', true);
             $('.frmSignUpStepFirst')[0];
             $.ajax({
                 type: 'post',
                 url: site_url + "user/signupFirstStep",
                 dataType: 'json',
                 data: $(form).serialize(),
                 success: function(resp) {
                     if (resp.status === 'success') {
                         location.href = resp.redirect;
                     }
                 }
             });
         }
    });

    $(".frmSignUpStepSecond").validate({
        rules: {
            usr_country: {
                required: true
            },
            usr_state: {
                required: true
            },
            usr_first_name: {
                required: true
            },
            usr_password: {
                required: true
            },
            usr_cpassword: {
                equalTo: "#pwd"
            }
        },
        messages: {
            usr_email: "Please enter a valid email address",
            usr_captcha: "Invalid text entered"
        },
        submitHandler: function(form) {
            $('.btnSubmitSecondStep').html('Please wait...');
            $('.frmSignUpStepSecond')[0];
            $('.btnSubmitSecondStep').prop('disabled', true);
            $.ajax({
                type: 'post',
                url: site_url + "user/update?sendVerification=1",
                dataType: 'json',
                data: $(form).serialize(),
                success: function(resp) {
                    if (resp.status === 'success') {
                        $('.frmSignUpStepSecond').hide();
                        $('.divSuccess').show();
                        $('.btnSecondStep').removeClass('btn-indigo');
                        $('.btnLastStep').addClass('btn-indigo');
                    } else {
                        $('.btnSubmitSecondStep').prop('disabled', false);
                        $('.divValidationMessage').html(resp.msg);
                        $('.btnSubmitSecondStep').html('Proceed');
                    }
                }
            });
        }
    });
    $(document).on('click', '.refresh_captcha', function() {
        $.ajax({
            url: site_url + "user/refreshCaptcha",
            method: "GET",
            dataType: "json",
            beforeSend: function() {
                $(".ajax_loader").removeClass("d-none");
            },
            complete: function() {
                $(".ajax_loader").addClass("d-none");
            },
            success: function(data) {
                $('#captcha_code').val('');
                $('#captcha_img').html(data.data.image);
                $('#captcha_code').val(data.data.word);
                $('#captcha').val('');
            }
        });
    });
    $(".ver_btn").click(function(e) {
        e.preventDefault();
    });
});