channel.bind('rfq-event', function(data) {
    incimg = $('#site_url').val() + data.from_img;

    msg = '<div class="incoming_msg" id="msg' + data.rfq_id + '"><div class="incoming_msg_img"><div class="chat_img"> <img src="' + incimg + '" class = "img-circle"> </div></div><div class="received_msg"><div class="received_withd_msg">\
                <p>' + data.message +
        '</p><span class="time_date">' + data.time +
        '<strong> ' + data.from + '</strong>\
        </span></div></div></div> ';
    $('.msg_history').append(msg);
    var h = $('.msg_history');
    var height = h[0].scrollHeight;
    h.scrollTop(height);
});


var loadRFQComments = function() {
    var rfqloaded = new Array();
    $("input[name='rfqloaded[]']").each(function() {
        rfqloaded.push($(this).val());
    });
    var rfq_id = $('.rfq_id').val();
    var from = $('.txtComments').attr('from');
    var to = $('.txtComments').attr('to');
    var height = 0;

    $.ajax({
        type: 'post',
        url: $('#site_url').val() + 'product/loadRFQComments/' + rfq_id,
        dataType: 'json',
        data: { 'ids': rfqloaded, 'from': from, 'to': to },
        success: function(resp) {
            if (resp.design !== "") {
                $('.msg_history').append(resp.design);
                var h = $('.msg_history');
                var height = h[0].scrollHeight;
                h.scrollTop(height);
            }
        }
    });
};
$(".btnComments").click(function() {
    var h = $('.msg_history');
    var height = h[0].scrollHeight;
    h.scrollTop(height);
})

$('.txtComments').on("keypress", function(e) {
    if (e.keyCode == 13) { // Enter key pressed
        sendRFQComments();
        return false; // prevent the button click from happening
    }
});

$('.msg_send_btn').on("click", function() {
    sendRFQComments();
});

function sendRFQComments() {
    var _this = $('.txtComments');
    if (_this.val() !== '') {
        $.ajax({
            type: 'post',
            url: _this.attr('data-url'),
            dataType: 'json',
            data: {
                'rfqn_rfq_id': _this.attr('rfqn_rfq_id'),
                'rfqn_added_by': _this.attr('from'),
                'rfqn_supplier': _this.attr('to'),
                'rfqn_comments': _this.val()
            },
            beforeSend: function() {
                byimg = $('#site_url').val() + $("#lgd_img").val();

                msg = '<div class="outgoing_msg">\
                    <div class="outgoing_msg_img">\
                        <div class="chat_img">\
                            <img src="' + byimg + '" class = "img-circle"></img>\
                        </div>\
                    </div>\
                    <div class="sent_msg">\
                        <p>' + _this.val() + '</p>\
                        <span class="time_date txt-align-rt">\
                            ' + time + '\
                        </span>\
                    </div>\
                </div>';
                $('.msg_history').append(msg);
                var h = $('.msg_history');
                var height = h[0].scrollHeight;
                h.scrollTop(height);
                _this.val('');
            },
            success: function(resp) {
                _this.focus();

            }
        });
    }
}
$('.btnReload').on("click", function() {
    loadRFQComments();
});

/*Rfq message module*/