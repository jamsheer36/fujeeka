 var pusher = new Pusher('853be53b86c43083fe5c', {
     cluster: 'ap2',
     forceTLS: true
 });
 var ch_name = $("#channel").val();
 var channel = pusher.subscribe(ch_name);
 channel.bind('pusher:subscription_error', function(status) {
     console.log("error: " + status);
 });
 channel.bind('pusher:subscription_succeeded', function() {
     console.log("success: connected");
 });
 channel.bind('chat-event', function(data) {

     if (data.message != '' && data.is_prd == 0) {
         a = '<div class="chat-bx-outer"><div class="chat-bx white-bx">' + data.message + '</div><div class="r-time">' + settime() + '</div></div><div style="clear:both;"></div>';
         $(".chat_append").append(a);
     }
     if (data.attachment != '' && data.is_prd == 0) {
         attch = data.attachment;
         attr = 'target="_blank" rel="noopener noreferrer"';
         title = 'View Image';
         if (data.ext != 'gif' && data.ext != 'jpg' && data.ext != 'png' && data.ext != 'jpeg') {
             attch = 'images/attachment.jpg';
             attr = 'download';
             title = 'Download Document';
         }
         a = '<div class="chat-bx-outer"><div class="chat-bx white-bx"><div class="img-holder"><a title="' + title + '" href = "' + data.attachment + '" ' + attr + '><img src="' + attch + '" alt="image not found"></a></div></div><div class="r-time">' + settime() + '</div></div><div style="clear:both;"></div>';
         $(".chat_append").append(a);
     }
     if (data.is_prd != 0) {
         a = '<div class="chat-bx-outer">\
        <div class="green-box">\
          <div class="product-info">\
              <div class="image">\
                  <img src="' + data.attachment + '" alt="">\
              </div>\
              <div class="detail">\
                  <p class="heading6" >' + data.message + '</p>\
              </div>\
          </div>\
          <div class = "interest">User Intrested In This Product</div></div></div><div style="clear:both;"></div> ';
         $(".chat_append").append(a);
     }

     var h = $('.chat_append');
     var height = h[0].scrollHeight;
     h.scrollTop(height);
 });

 function messageBox(data) {
     $('.msgBox').show();
     $(".sus_msg").html(data.message);
     $('.msgBox').delay(5000).fadeOut('slow');
 }
 $('#message').keypress(function(e) {
     var key = e.which;
     if (key == 13) {
         $('#btn-chat').click();
         return false;
     }
 });

 $('#btn-chat').click(function() {
     id = generateId();
     message = $("#message").val();

     $("#message").val("");
     var form_data = new FormData($("#chat_frm")[0]);
     var file_data = $('#file').prop('files')[0];
     if (file_data != '' && typeof(file_data) !== "undefined") {
         message = '';
         getImagePath(file_data.type);
     }
     $('#file').val(null);
     if (message == '' && (file_data == '' || typeof(file_data) === "undefined"))
         return false;
     var to_usr = $('#to_usr').val();
     form_data.append('file', file_data);
     form_data.append('content', message);
     form_data.append('to_usr', to_usr);
     $.ajax({
         url: $("#url").val() + "user/pusher/",
         method: "post",
         dataType: "json",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,


         beforeSend: function() {
             if (message != '') {
                 a = '<div class="chat-bx-outer rght"><div class="chat-bx green-bx">' + message + '</div><div class="r-time">' + settime() + '</div></div><div style="clear:both;"></div>';
                 $(".chat_append").append(a);
                 var h = $('.chat_append');
                 var height = h[0].scrollHeight;
                 h.scrollTop(height);
             }

         },
         success: function(data) {}
     });
 });

 function generateId() {
     return Math.round(new Date().getTime() + (Math.random() * 100));
 }

 function settime() {
     now = new Date();
     year = "" + now.getFullYear();
     month = "" + (now.getMonth() + 1);
     if (month.length == 1) { month = "0" + month; }
     day = "" + now.getDate();
     if (day.length == 1) { day = "0" + day; }
     hour = "" + now.getHours();
     if (hour.length == 1) { hour = "0" + hour; }
     minute = "" + now.getMinutes();
     if (minute.length == 1) { minute = "0" + minute; }
     second = "" + now.getSeconds();
     if (second.length == 1) { second = "0" + second; }
     return (year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second);
 }


 function getImagePath(ext) {
     if (ext != 'image/gif' && ext != 'image/jpg' && ext != 'image/png' && ext != 'image/jpeg') {
         a = '<div class="chat-bx-outer rght"><div class="chat-bx green-bx"><div class="img-holder"><img src="images/attachment.jpg" alt="Attachment"></div></div><div class="r-time">' + settime() + '</div></div><div style="clear:both;"></div>';
         $(".chat_append").append(a);
         var h = $('.chat_append');
         var height = h[0].scrollHeight;
         h.scrollTop(height);
         return true;
     }
     var reader = new FileReader();
     reader.onload = function(e) {
         a = '<div class="chat-bx-outer rght"><div class="chat-bx green-bx"><div class="img-holder"><img src="' + e.target.result + '" alt="Attachment"></div></div><div class="r-time">' + settime() + '</div></div><div style="clear:both;"></div>';
         $(".chat_append").append(a);
         var h = $('.chat_append');
         var height = h[0].scrollHeight;
         h.scrollTop(height);
     }
     reader.readAsDataURL($('#file').prop('files')[0]);
     return true;
 }

 $("#file").change(function() {
     file_data = $('#file')[0].files[0];
     $("#message").val(file_data.name);
 });
 $("#det_send").click(function() {
     message = $("#pname").text();
     var form_data = new FormData($("#chat_frm")[0]);
     var attachment = $('#pimg').attr('src');
     var to_usr = $('#to_usr').val();
     form_data.append('attachment', attachment);
     form_data.append('content', message);
     form_data.append('to_usr', to_usr);
     $.ajax({
         url: $("#url").val() + "user/pusher/",
         method: "post",
         dataType: "json",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,


         beforeSend: function() {
             if (message != '') {
                 a = '<div class="chat-bx-outer rght">\
                 <div class="green-box">\
                   <div class="product-info">\
                       <div class="image">\
                           <img src="' + attachment + '" alt="">\
                       </div>\
                       <div class="detail">\
                           <p class="heading6">' + message + '</p>\
                       </div>\
                   </div>\
                   <div class="interest">\
                       Details sent to supplier\
                   </div>\
                 </div>\
                 <div class="r-time">' + settime() + '</div>\
               </div><div style="clear:both;"></div>'
                     //  a = '<div class="chat-bx-outer rght"><div class="chat-bx green-bx">' + message + '</div><div class="r-time">' + settime() + '</div></div>';
                 $(".p_details").hide();
                 $(".chat_append").append(a);
                 var h = $('.chat_append');
                 var height = h[0].scrollHeight;
                 h.scrollTop(height);
             }

         },
         success: function(data) {}
     });
 });
 $("#det_cancel").click(function() {
     $(".p_details").hide();
 });