$(document).ready(function() {

    function load_page_data(page, market, cate = '', building = '', order_by = '', cls = '') {
        $.ajax({
            url: site_url + "market/markets/" + page + "?page=" + page + "&market=" + market + "&category=" + cate + "&building=" + building + "&order_by=" + order_by + "&cls=" + cls,
            method: "GET",
            dataType: "json",
            beforeSend: function() {
                $(".ajax_loader1").removeClass("d-none");
            },
            complete: function() {
                $(".ajax_loader1").addClass("d-none");
            },
            success: function(data) {
                $('#product_listing').html(data.data);
                $('#pagination_link').html(data.pagination_link);

                // window.scrollTo(0, 0);
            }
        });
    }
    mar = $("#country").val();
    build = $("#building").val();
    load_page_data(1, mar, '', build);

    $(document).on("click", ".pagination li a", function(event) {
        event.preventDefault();
        ref = $(this).attr('href');
        page = ref.substring(ref.lastIndexOf("/") + 1, ref.length);
        mar = $("#country").val();
        building = $("#building").val();

        order_by = '';
        cate = $("#cate").val();


        if ($("#sort_by").val() != '') {
            order_by = $("#sort_by").val();
        }
        if (page == '')
            page = 1;
        var cls = 'grid-style';
        if ($(".single-prod").hasClass("list-style")) {
            cls = "list-style";
        }
        load_page_data(page, mar, cate, building, order_by, cls);
        window.scrollTo(0, 0);
    });

    $(document).on("change", "#building", function() {
        load();
    });
    $(document).on("change", "#cate", function() {
        load();
    });
    $(document).on("change", "#country", function() {
        load();
    });
    $(document).on("change", "#sort_by", function() {
        load();
    });

    function load() {
        cate = $("#cate").val();
        order_by = $("#sort_by").val();
        mar = $("#country").val();
        building = $("#building").val();
        var cls = 'grid-style';
        if ($(".single-prod").hasClass("list-style")) {
            cls = "list-style";
        }
        load_page_data(1, mar, cate, building, order_by, cls);
    }
    /* 360 modal */
    $('#product_listing').on('click', '.btn-360', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $(".360_body").html('<iframe width="100%" height="560" frameborder="0" scrolling="no" allowtransparency="true" src="' + url + '"></iframe>');
    });
});