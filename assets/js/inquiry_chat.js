channel.bind('inquiry-event', function(data) {
    incimg = $('#site_url').val() + data.from_img;
    msg = '<div class="incoming_msg">\
    <div class="incoming_msg_img"> \
         <div class="chat_img"> \
            <img src="' + incimg + '" class = "img-circle">\
         </div>\
    </div>\
    <div class="received_msg">\
         <div class="received_withd_msg">\
              <div>\
                   <div>\
                        <strong>' + data.title + '</strong>\
                   </div>\
                        <hr>\
                        <div class="chat-desc">\
                             ' + data.desc + '\
                        </div>\
              </div> \
              <span class="time_date">\
                    ' + data.time + '\
                    <strong>' + data.from + '</strong>\
              </span>\
         </div>\
    </div>\
</div>';
    $('.msg_history').append(msg);
    var h = $('.msg_history');
    var height = h[0].scrollHeight;
    h.scrollTop(height);
});

$('.txtInqComments').on("keypress", function(e) {
    if (e.keyCode == 13) { // Enter key pressed
        sendInquiryComments();
        return false; // prevent the button click from happening
    }
});

$('.btnSendInqMessage').on("click", function(e) {
    sendInquiryComments();
});

function sendInquiryComments() {
    var _this = $('.txtInqComments');
    if (_this.val() !== '') {
        $.ajax({
            type: 'post',
            url: _this.attr('data-url'),
            dataType: 'json',
            data: {
                'poc_order_id': _this.attr('ord_id'),
                'poc_from': _this.attr('from'),
                'poc_to': _this.attr('to'),
                'poc_title': _this.val(),
                'poc_desc': $('.poc_desc').val()
            },
            beforeSend: function() {
                byimg = $('#site_url').val() + $("#lgd_img").val();
                msg = '<div class="outgoing_msg">\
                            <div class="outgoing_msg_img">\
                                <div class="chat_img"><img src="' + byimg + '" class = "img-circle"></div>\
                            </div>\
                            <div class="sent_msg">\
                                <div class="">\
                                    <div>\
                                        <div><strong>' + _this.val() + '</strong></div>\
                                        <hr><div class="chat-desc">' + $('.poc_desc').val() + '</div>\
                                    </div>\
                                </div>\
                                <span  class="time_date txt-align-rt">' + time + '</span>\
                            </div>\
                        </div> ';
                $('.msg_history').append(msg);
                var h = $('.msg_history');
                var height = h[0].scrollHeight;
                h.scrollTop(height);
                _this.val('');
                $('.poc_desc').val('');
            },
            success: function(resp) {
                _this.focus();
            }
        });
    }
}

/*Inquiry message module*/