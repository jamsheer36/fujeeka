/*
* ----------------------------------------------------------------------------------------
  Scripts carried from products page
* ----------------------------------------------------------------------------------------
*/
/*----------------------------------------------------*/
/* inner header sticky
/*----------------------------------------------------*/
$(window).on('scroll', function() {
    if ($(window).scrollTop() > 100) {
        $('.header-top-area').addClass('stick-menu');
        $('.logo img').attr('src', 'images/fujeeka-logo-green.png');
        $(".autocomplete-suggestions").remove();
    } else {
        $('.header-top-area').removeClass('stick-menu');
        $('.logo img').attr('src', 'images/fujeeka-logo-g.png');
        $(".autocomplete-suggestions").remove();
    }
});

/*------------------------------------------------------*/
/* list / grid view
/*-----------------------------------------------------*/

// Get the elements with class="column"
var elements = document.getElementsByClassName("single-prod");

// Declare a loop variable
var i;

//List View

function listView() {
    for (i = 0; i < elements.length; i++) {
        $(".single-prod").addClass("list-style");
        $(".single-prod").removeClass("grid-style");
    }
    $(".main_prd_grid").addClass("d-none");
    $(".main_prd_list").removeClass("d-none");
}

// Grid View
function gridView() {
    for (i = 0; i < elements.length; i++) {
        $(".single-prod").removeClass("list-style");
        $(".single-prod").addClass("grid-style");
    }
    $(".main_prd_grid").removeClass("d-none");
    $(".main_prd_list").addClass("d-none");
}
$(".btn_view").click(function() {
    $(".btn_view").removeClass("btn-selected");
    $(this).addClass("btn-selected");
});
/*----------------------------------------------------*/
/* Search bar
/*----------------------------------------------------*/

$(document).ready(function(e) {
    $('.search-panel .dropdown-menu').find('a').click(function(e) {
        e.preventDefault();
        var param = $(this).attr("href").replace("#", "");
        var concept = $(this).text();
        $('.search-panel span#search_concept').text(concept);
        $('.input-group #search_param').val(param);
    });
});


/*------------------------------------------------------*/
/* list / grid view
/*-----------------------------------------------------*/




/*------------------------------------------------------*/
/* tooltip
/*-----------------------------------------------------*/

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});