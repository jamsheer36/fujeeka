function load_page_data(page, search = '', status = '') {
     base_url = $("#base_url").val();
     $.ajax({
          url: base_url + "product/index/" + page + "?page=" + page + "&search=" + search + "&status=" + status,
          method: "GET",
          dataType: "json",
          beforeSend: function () {
               $(".ajax_loader").removeClass("hidden");
          },
          complete: function () {
               $(".ajax_loader").addClass("hidden");
          },
          success: function (data) {
               $('tbody').html(data.data);
               $('#pagination_link').html(data.pagination_link);
               window.scrollTo(0, 0);
          }
     });
}
$(document).on("click", ".pagination li a", function (event) {
     event.preventDefault();
     ref = $(this).attr('href');
     page = ref.substring(ref.lastIndexOf("/") + 1, ref.length);
     if (page == '')
          page = 1;
     search = $("#prd_search").val();
     status = $("#crnt_status").val();
     load_page_data(page, search, status);
});
$("#search_btn").click(function () {
     search = $("#prd_search").val();
     // if(search) 
     status = $("#crnt_status").val();
     load_page_data(1, search, status);
});
$(document).on("click", "#tbl tbody td", function () {
     url = $(this).attr("data-url");
     if (url)
          window.location.href = $(this).attr("data-url");
});

$("#prd_search").on("keyup", function (event) {
     event.preventDefault();
     if (event.keyCode === 13) {
          $("#search_btn").trigger("click");
          ;
     }
});
$(".st_div").click(function () {
     $(".st_div").removeClass("active");
     $(".st_div").addClass("disable");
     $(this).addClass("active");
     $(this).removeClass("disable");
     status = $(this).attr("data-status");
     $("#prd_search").val('');
     $("#crnt_status").val(status);
     load_page_data(1, '', status)
});