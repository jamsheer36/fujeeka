27-08-2018 - JK
---------------
1) ALTER TABLE `cpnl_products` ADD `prd_offer_price_min` VARCHAR(100) NULL AFTER `prd_price_max`, ADD `prd_offer_price_max` VARCHAR(100) NULL AFTER `prd_offer_price_min`;

28-08-2018 - Irfan
------------------
1) ALTER TABLE `cpnl_products` ADD `prd_hot_selling` INT(1) NULL AFTER `prd_oem`;

30-08-2018 - Irfan
------------------
ALTER TABLE cpnl_products_keyword ADD FULLTEXT (`pkwd_val_en`, `pkwd_val_ar`, `pkwd_val_ch`)

CREATE TABLE `cpnl_recommendation` (
  `rec_id` int(11) NOT NULL,
  `rec_user_id` int(11) NOT NULL DEFAULT '0',
  `rec_key_word` text NOT NULL,
  `rec_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
); 

31-08-2018 - JK
---------------
1) ALTER TABLE `cpnl_users` CHANGE `usr_city` `usr_city` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

09-09-2018 - JK
---------------
1) CREATE TABLE IF NOT EXISTS `cpnl_products_order_master` (
  `ord_id` int(11) NOT NULL AUTO_INCREMENT,
  `ord_prod_id` int(11) DEFAULT NULL,
  `ord_number` varchar(100) DEFAULT NULL,
  `ord_buyer` int(11) DEFAULT NULL,
  `ord_supplier` int(11) DEFAULT NULL,
  `ord_subject` text CHARACTER SET utf8,
  `ord_message` text CHARACTER SET utf8,
  `ord_attachment` varchar(255) DEFAULT NULL,
  `ord_qty` int(11) DEFAULT NULL,
  `ord_unit` int(11) DEFAULT NULL,
  `ord_status` int(11) DEFAULT '0',
  `ord_added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ord_added_by` int(11) DEFAULT NULL,
  `ord_verified_by` int(11) DEFAULT NULL,
  `ord_verified_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ord_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

2) CREATE TABLE IF NOT EXISTS `cpnl_products_order_statuses` (
  `pos_id` int(11) NOT NULL AUTO_INCREMENT,
  `pos_status` varchar(100) DEFAULT NULL,
  `pos_desc` text,
  `pos_added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`pos_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

3) CREATE TABLE IF NOT EXISTS `cpnl_products_units` (
  `punt_id` int(11) NOT NULL AUTO_INCREMENT,
  `punt_title` varchar(100) DEFAULT NULL,
  `punt_desc` text,
  `punt_added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`punt_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

12-09-2018 - JK
---------------
ALTER TABLE `cpnl_supplier_master` ADD `supm_status` INT(1) NULL DEFAULT '0' AFTER `supm_added_by`;

17-09-2018 - Irfan
------------------
1) CREATE TABLE `cpnl_rfq` (
  `rfq_id` int(11) NOT NULL,
  `rfq_category` int(11) DEFAULT NULL,
  `rfq_market` int(11) NOT NULL,
  `rfq_product` int(11) NOT NULL,
  `rfq_qty` int(11) NOT NULL,
  `rfq_unit` int(11) NOT NULL,
  `rfq_requirment` text NOT NULL,
  `rfq_attachment` varchar(255) NOT NULL,
  `rfq_validity` date NOT NULL,
  `rfq_mail` varchar(255) NOT NULL,
  `rfq_user` int(11) NOT NULL,
  `rfq_reply_count` int(11) NOT NULL DEFAULT '0',
  `rfq_status` int(11) NOT NULL DEFAULT '1',
  `rfq_added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

2) CREATE TABLE `cpnl_rfq_notification` (
  `rfqn_id` int(11) NOT NULL,
  `rfqn_rfq_id` int(11) NOT NULL,
  `rfqn_supplier` int(11) NOT NULL,
  `rfqn_is_viewed` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

17-09-2018 - JK
---------------
ALTER TABLE `cpnl_products` ADD `prd_status` TINYINT(1) NULL DEFAULT '0' AFTER `prd_added_by`;

19-09-2018 - Irfan
-------------------
ALTER TABLE `cpnl_rfq_notification` DROP `rfqn_is_viewed`;
ALTER TABLE `cpnl_market_places` ADD `mar_added_by` INT NOT NULL AFTER `mar_status`;

21-09-2018 -Irfan
------------------
ALTER TABLE `cpnl_category` ADD `cat_added_by` INT NOT NULL AFTER `cat_is_name_pendent`;

23-09-2018 - JK
---------------
ALTER TABLE `cpnl_products` ADD `prd_unit` INT(11) NULL AFTER `prd_id`, ADD `prd_qty` VARCHAR(10) NULL AFTER `prd_unit`;

DROP TABLE IF EXISTS `cpnl_products_units`;
CREATE TABLE IF NOT EXISTS `cpnl_products_units` (
  `unt_id` int(11) NOT NULL AUTO_INCREMENT,
  `unt_unit_name_en` varchar(100) DEFAULT NULL,
  `unt_unit_name_ar` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `unt_unit_name_ch` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `unt_unit_en` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `unt_unit_ar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `unt_unit_ch` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `unt_numbers` varchar(100) DEFAULT '0',
  `unt_desc_en` text CHARACTER SET utf8,
  `unt_desc_ar` text CHARACTER SET utf8,
  `unt_desc_ch` text CHARACTER SET utf8,
  PRIMARY KEY (`unt_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `cpnl_products_units` (`unt_id`, `unt_unit_name_en`, `unt_unit_name_ar`, `unt_unit_name_ch`, `unt_unit_en`, `unt_unit_ar`, `unt_unit_ch`, `unt_numbers`, `unt_desc_en`, `unt_desc_ar`, `unt_desc_ch`) VALUES
(1, 'Kilogram', '', '', 'Kg', '', '', '', NULL, NULL, NULL),
(2, 'Meter', '', '', 'M', '', '', '', NULL, NULL, NULL),
(3, 'Centimeter', '', '', 'CM', '', '', '', NULL, NULL, NULL),
(4, 'Dozen', '', '', 'dz', '', '', '12', 'Enter how many boxes (12 number per dozen)', '', '');

26-09-2018 - jk
---------------
DROP TABLE IF EXISTS `cpnl_mailboox_master`;
CREATE TABLE IF NOT EXISTS `cpnl_mailboox_master` (
  `mms_id` int(11) NOT NULL AUTO_INCREMENT,
  `mms_from` int(11) DEFAULT NULL,
  `mms_to` int(11) DEFAULT NULL,
  `mms_subject` text CHARACTER SET utf8,
  `mms_message` text CHARACTER SET utf8,
  `mms_added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mms_last_viewd_on` timestamp NULL DEFAULT NULL,
  `mms_status` int(2) DEFAULT '1',
  PRIMARY KEY (`mms_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `cpnl_mailboox_attachments`;
CREATE TABLE IF NOT EXISTS `cpnl_mailboox_attachments` (
  `mat_id` int(11) NOT NULL AUTO_INCREMENT,
  `mat_master_id` int(11) DEFAULT NULL,
  `mat_attachment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`mat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

26-09-2018 -Irfan
------------------
ALTER TABLE `cpnl_products` ADD `prd_is_stock_prod` INT NOT NULL DEFAULT '0' AFTER `prd_id`;

02-10-2018 - Irfan
-------------------
CREATE TABLE `cpnl_chats` (
  `c_id` int(11) NOT NULL,
  `c_from_id` int(11) NOT NULL,
  `c_to_id` int(11) NOT NULL,
  `c_content` text NOT NULL,
  `c_attachment` text NOT NULL,
  `c_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `c_is_read` int(11) NOT NULL DEFAULT '0',
  `c_with` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


03-10-2018 -Irfan
------------------
ALTER TABLE `cpnl_chats` DROP `c_with`

ALTER TABLE `cpnl_chats` ADD INDEX(`c_to_id`);
ALTER TABLE `cpnl_chats` ADD INDEX(`c_from_id`);
ALTER TABLE `cpnl_chats` CHANGE `c_time` `c_time` VARCHAR(50) NOT NULL;

06-10-2018 - Irfan
-------------------
ALTER TABLE `cpnl_supplier_master` ADD `supm_home_banner` TEXT NULL DEFAULT NULL AFTER `supm_default_image`;

08-10-2018 - JK
---------------
ALTER TABLE `cpnl_products` CHANGE `prd_price_min` `prd_price_min` DECIMAL(10,2) NULL DEFAULT NULL;
ALTER TABLE `cpnl_products` CHANGE `prd_price_max` `prd_price_max` DECIMAL(10,2) NULL DEFAULT NULL;
ALTER TABLE `cpnl_products` CHANGE `prd_offer_price_min` `prd_offer_price_min` DECIMAL(10,2) NULL DEFAULT NULL;
ALTER TABLE `cpnl_products` CHANGE `prd_offer_price_max` `prd_offer_price_max` DECIMAL(10,2) NULL DEFAULT NULL;

ALTER TABLE `cpnl_products` CHANGE `prd_price_min` `prd_price_min` DECIMAL(10,2) NOT NULL DEFAULT '0.00', CHANGE `prd_price_max` `prd_price_max` DECIMAL(10,2) NOT NULL DEFAULT '0.00', CHANGE `prd_offer_price_min` `prd_offer_price_min` DECIMAL(10,2) NOT NULL DEFAULT '0.00', CHANGE `prd_offer_price_max` `prd_offer_price_max` DECIMAL(10,2) NOT NULL DEFAULT '0.00';

10-10-2018 -Irfan
-----------------
ALTER TABLE `cpnl_products_order_master` ADD `ord_is_notified` INT NOT NULL DEFAULT '0' AFTER `ord_verified_on`;
CREATE TABLE `cpnl_seller_requests` (
  `sr_id` int(11) NOT NULL,
  `sr_usr_id` int(11) NOT NULL,
  `details` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0-pending 1-approved',
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `cpnl_seller_requests`
  ADD PRIMARY KEY (`sr_id`);
ALTER TABLE `cpnl_seller_requests`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

15-10-2018 - JK
---------------
CREATE TABLE IF NOT EXISTS `cpnl_banner` (
  `bnr_id` int(11) NOT NULL AUTO_INCREMENT,
  `bnr_title` text,
  `bnr_image` text,
  `bnr_order` int(11) DEFAULT NULL,
  `bnr_added_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `bnr_status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bnr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

ALTER TABLE `cpnl_supplier_grade` ADD `sgrd_icon` VARCHAR(255) NULL AFTER `sgrd_grade_ch`;

10-15-2018 -Irfan
------------------
ALTER TABLE `cpnl_users` ADD `usr_token` VARCHAR(64) NOT NULL AFTER `usr_added_date`;

18-10-2018 - JK
---------------
CREATE TABLE IF NOT EXISTS `cpnl_favorite_list` (
  `fav_id` int(11) NOT NULL AUTO_INCREMENT,
  `fav_consign` varchar(50) DEFAULT NULL,
  `fav_consign_id` int(11) DEFAULT NULL,
  `fav_added_by` int(11) DEFAULT NULL,
  `fav_added_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fav_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

20-10-2018 - JK
---------------
ALTER TABLE `cpnl_supplier_master` ADD `supm_panoramic_image` VARCHAR(255) NULL AFTER `supm_home_banner`;

CREATE TABLE IF NOT EXISTS `cpnl_advertisement` (
  `add_id` int(11) NOT NULL AUTO_INCREMENT,
  `add_title` varchar(255) DEFAULT NULL,
  `add_url` text,
  `add_slug` varchar(200) DEFAULT NULL,
  `add_image` varchar(200) DEFAULT NULL,
  `add_added_by` int(11) DEFAULT NULL,
  `add_added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `add_order` int(11) DEFAULT NULL,
  `add_show_on_home_page` int(1) DEFAULT '1',
  `add_status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`add_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

23-10-2018 - Irfan
-------------------
ALTER TABLE `cpnl_products` ADD INDEX(`prd_name_en`);

24-10-2018 - Irfan
-------------------
ALTER TABLE `cpnl_users` ADD `usr_otp` INT NOT NULL AFTER `usr_token`;

25-10-2018 - Irfan
-------------------
ALTER TABLE `cpnl_chats` ADD `c_is_prd_details` INT NOT NULL DEFAULT '0' AFTER `c_is_read`;


27-10-2018 - JK
---------------
CREATE TABLE IF NOT EXISTS `cpnl_rfq` (
  `rfq_id` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_category` int(11) DEFAULT NULL,
  `rfq_market` int(11) DEFAULT NULL,
  `rfq_product` int(11) DEFAULT '0',
  `rfq_product_name` text CHARACTER SET utf8,
  `rfq_qty` int(11) DEFAULT NULL,
  `rfq_unit` int(11) DEFAULT NULL,
  `rfq_requirment` text,
  `rfq_attachment` varchar(255) DEFAULT NULL,
  `rfq_validity` date DEFAULT NULL,
  `rfq_mail` varchar(255) DEFAULT NULL,
  `rfq_user` int(11) DEFAULT NULL,
  `rfq_reply_count` int(11) DEFAULT '0',
  `rfq_status` int(11) DEFAULT '1',
  `rfq_added_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`rfq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `cpnl_rfq_notification` (
  `rfqn_id` int(11) NOT NULL AUTO_INCREMENT,
  `rfqn_rfq_id` int(11) NOT NULL,
  `rfqn_supplier` int(11) NOT NULL COMMENT 'To',
  `rfqn_comments` text,
  `rfqn_added_by` int(11) DEFAULT NULL COMMENT 'From',
  `rfqn_added_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `rfqn_read_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rfqn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;


31-10-2018 - Irfan
-------------------
CREATE TABLE `cpnl_enquiry` (
  `enq_id` int(11) NOT NULL,
  `enq_sup_id` int(11) NOT NULL,
  `enq_name` varchar(155) NOT NULL,
  `enq_email` varchar(155) NOT NULL,
  `enq_phone` varchar(18) NOT NULL,
  `enq_subject` text NOT NULL,
  `enq_query` text NOT NULL,
  `enq_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `cpnl_enquiry`
  ADD PRIMARY KEY (`enq_id`);

ALTER TABLE `cpnl_enquiry`
  MODIFY `enq_id` int(11) NOT NULL AUTO_INCREMENT;
  
31-10-2018 - JK
---------------
ALTER TABLE `cpnl_products` ADD `prd_is_show_on_profile` TINYINT(1) NULL DEFAULT '0' AFTER `prd_default_image`, 
ADD `prd_is_main_product` TINYINT(1) NULL DEFAULT '0' AFTER `prd_is_show_on_profile`;

02-11-2018 - JK
---------------
ALTER TABLE `cpnl_products` ADD `prd_stock_qty` DECIMAL(10,2) NULL AFTER `prd_is_stock_prod`;

ALTER TABLE `cpnl_users` ADD `usr_forgotten_password_otp` VARCHAR(10) NULL AFTER `usr_forgotten_password_time`;

13-11-2018 - JK
---------------
ALTER TABLE `cpnl_advertisement` ADD `add_category` INT(11) NULL AFTER `add_title`;
ALTER TABLE `cpnl_banner` ADD `bnr_category` INT(11) NULL AFTER `bnr_id`;
ALTER TABLE `cpnl_banner` ADD `bnr_url` VARCHAR(255) NULL AFTER `bnr_category`;

15-11-2018 - JK
---------------
ALTER TABLE `cpnl_supplier_master` ADD `supm_desc` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `supm_panoramic_image`;
ALTER TABLE `cpnl_market_places` ADD `mar_email` VARCHAR(255) NULL AFTER `mar_contact_number`, ADD `mar_desc` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `mar_email`;

18-11-2018 - Irfan
-------------------
ALTER TABLE `cpnl_users` ADD `usr_is_mail_verified` INT NOT NULL DEFAULT '0' AFTER `usr_otp`;

19-11-2018 - JK
---------------
CREATE TABLE IF NOT EXISTS `cpnl_newsletter_subscribe` (
  `nls_id` int(11) NOT NULL AUTO_INCREMENT,
  `nls_ip` varchar(100) DEFAULT NULL,
  `nls_email` varchar(200) DEFAULT NULL,
  `nls_added_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `nls_if_logged` int(11) DEFAULT NULL,
  PRIMARY KEY (`nls_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

21-11-2018- JK
--------------
ALTER TABLE `cpnl_rfq` ADD `rfq_supplier` INT(11) NULL AFTER `rfq_product`;
ALTER TABLE `cpnl_rfq` CHANGE `rfq_supplier` `rfq_supplier` INT(11) NULL DEFAULT '0';

26-11-2018 - Irfan
------------------
ALTER TABLE `cpnl_users` ADD `usr_pushy_token` VARCHAR(155) NOT NULL AFTER `usr_is_mail_verified`;

28-11-2018 - Irfan
-------------------
ALTER TABLE `cpnl_products` ADD `prd_updated_on` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `prd_status`;



30-11-2018 - JK
---------------
CREATE TABLE IF NOT EXISTS `cpnl_rfq_products_send` (
  `rps_id` int(11) NOT NULL AUTO_INCREMENT,
  `rps_rfq_id` int(11) DEFAULT NULL,
  `rps_supplier_id` int(11) DEFAULT NULL,
  `rps_nottification_id` int(11) DEFAULT NULL,
  `rps_prd_id_sent` int(11) DEFAULT NULL,
  PRIMARY KEY (`rps_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

1-12-2018 - Irfan
------------------
CREATE TABLE `cpnl_feeds` (
  `f_id` int(11) NOT NULL,
  `f_sup_id` int(11) NOT NULL,
  `f_prd_id` int(11) NOT NULL,
  `f_text` text NOT NULL,
  `f_status` int(11) NOT NULL DEFAULT '1',
  `f_added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `cpnl_feeds`
  ADD PRIMARY KEY (`f_id`);

ALTER TABLE `cpnl_feeds`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

  04-12-2018 - JK
  ---------------
  CREATE TABLE IF NOT EXISTS `cpnl_supplier_followers` (
  `sfol_id` int(11) NOT NULL AUTO_INCREMENT,
  `sfol_supplier` int(11) DEFAULT NULL,
  `sfol_followed_by` int(11) DEFAULT NULL,
  `sfol_added_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sfol_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

07-12-2018 - Irfan
------------------
ALTER TABLE `cpnl_products` ADD `prd_is_flash_sale` INT NOT NULL DEFAULT '0' AFTER `prd_is_main_product`;

12-11-2018 - JK
---------------
ALTER TABLE `cpnl_pushnotification_master` ADD `pnm_user_group` INT(11) NULL AFTER `pnm_id`;

12-12-2018 - Irfan
-------------------
ALTER TABLE `cpnl_pushnotification_master` ADD `pnm_image` TEXT NULL AFTER `pnm_message`;

13-12-2018 - JK 
---------------
ALTER TABLE `cpnl_products_order_master` ADD `ord_supplier_user_id` INT(11) NULL AFTER `ord_supplier`;

18-12-2018 - JK
---------------
ALTER TABLE `cpnl_users` ADD `usr_desc` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `usr_address1`;
ALTER TABLE `cpnl_users` CHANGE `usr_job_title` `usr_since` INT(5) NULL DEFAULT '0';
ALTER TABLE `cpnl_users` ADD `usr_description1` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `usr_desc`;
ALTER TABLE `cpnl_users` ADD `usr_first_name_ar` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL
AFTER `usr_first_name`, ADD `usr_first_name_ch` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `usr_first_name_ar`;

CREATE TABLE IF NOT EXISTS `cpnl_logistics_comp_images` (
  `lci_id` int(11) NOT NULL AUTO_INCREMENT,
  `lci_user_id` int(11) NOT NULL DEFAULT '0',
  `lci_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`lci_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `cpnl_logistics_comp_specification` (
  `lcs_id` int(11) NOT NULL AUTO_INCREMENT,
  `lcs_user_id` int(11) NOT NULL DEFAULT '0',
  `lcs_key_en` text CHARACTER SET utf8,
  `lcs_key_ar` text CHARACTER SET utf8,
  `lcs_key_ch` text CHARACTER SET utf8,
  `lcs_val_en` text CHARACTER SET utf8,
  `lcs_val_ar` text CHARACTER SET utf8,
  `lcs_val_ch` text CHARACTER SET utf8,
  `lcs_added_by` int(11) DEFAULT '0',
  `lcs_added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`lcs_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `cpnl_logistics_types` (
  `logt_id` int(11) NOT NULL AUTO_INCREMENT,
  `logt_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logt_desc` text CHARACTER SET utf8,
  `logt_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`logt_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `cpnl_logistics_types` (`logt_id`, `logt_title`, `logt_desc`, `logt_status`) VALUES
(1, 'LESS-THAN-CONTAINER LOAD [ LCL ]', 'LESS-THAN-CONTAINER LOAD [ LCL ]', 1),
(2, 'FULL CONTAINER LOAD [ FCL ]', 'FULL CONTAINER LOAD [ FCL ]', 1),
(3, 'AIR DOOR TO DOOR', 'AIR DOOR TO DOOR', 1),
(4, 'AIR CARGO', 'AIR CARGO', 1);

CREATE TABLE IF NOT EXISTS `cpnl_users_logistics` (
  `ulog_id` int(11) NOT NULL AUTO_INCREMENT,
  `ulog_user_id` int(11) NOT NULL,
  `ulog_logistics_id` int(11) NOT NULL,
  `ulog_added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ulog_added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ulog_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

21-12-2018 - JK
---------------
ALTER TABLE `cpnl_logistics_comp_images` ADD `lci_default` TINYINT(1) NOT NULL DEFAULT '0' AFTER `lci_image`;

25-12-2018 - JK
---------------
CREATE TABLE IF NOT EXISTS `cpnl_logistics_services` (
  `logs_id` int(11) NOT NULL AUTO_INCREMENT,
  `logs_logistics_id` int(11) NOT NULL DEFAULT '0',
  `logs_service_title` text CHARACTER SET utf8,
  `logs_desc` text CHARACTER SET utf8 NOT NULL,
  `logs_added_by` int(11) NOT NULL DEFAULT '0',
  `logs_added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`logs_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `cpnl_logistics_services_images` (
  `logsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `logsi_service_id` int(11) NOT NULL DEFAULT '0',
  `logsi_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`logsi_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

26-12-2018 - Irfan
-------------------
ALTER TABLE `cpnl_chats` ADD `c_price` DECIMAL(10,2) NOT NULL AFTER `c_attachment`;

26-12-2018 - JK
---------------
CREATE TABLE IF NOT EXISTS `cpnl_logistics_quotes` (
  `logq_id` int(11) NOT NULL AUTO_INCREMENT,
  `logq_service` int(11) NOT NULL DEFAULT '0',
  `logq_logistics` int(11) NOT NULL DEFAULT '0',
  `logq_cbm` varchar(255) DEFAULT NULL,
  `logq_weight` varchar(100) DEFAULT NULL,
  `logq_prod_type` varchar(255) DEFAULT NULL,
  `logq_email` varchar(255) DEFAULT NULL,
  `logq_mobile` varchar(20) DEFAULT NULL,
  `logq_name` varchar(255) DEFAULT NULL,
  `logq_desc` text CHARACTER SET utf8,
  `logq_added_by` int(11) NOT NULL DEFAULT '0',
  `logq_added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`logq_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `cpnl_logistics_quotes_types` (
  `lqt_id` int(11) NOT NULL AUTO_INCREMENT,
  `lqt_quote` int(11) NOT NULL DEFAULT '0',
  `lqt_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lqt_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

28-12-2018 - JK
---------------
ALTER TABLE `cpnl_logistics_quotes` DROP `logq_name`;
ALTER TABLE `cpnl_logistics_quotes` ADD `logq_loc_from` VARCHAR(255) NULL AFTER `logq_mobile`, ADD `logq_loc_to` VARCHAR(255) NULL AFTER `logq_loc_from`;
ALTER TABLE `cpnl_logistics_quotes` ADD `logq_viewed_on` TIMESTAMP NULL AFTER `logq_added_on`;

04-01-2019 - JK
---------------
ALTER TABLE `cpnl_banner` CHANGE `bnr_category` `bnr_category` VARCHAR(200) NULL DEFAULT NULL;
ALTER TABLE `cpnl_banner` ADD `bnr_desc` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `bnr_title`;

ALTER TABLE `cpnl_rfq` ADD `rfq_sub_category` INT(11) NOT NULL DEFAULT '0' AFTER `rfq_category`;

17-01-2019 - JK
---------------
CREATE TABLE IF NOT EXISTS `cpnl_supplier_banner_images` (
  `sbi_id` int(11) NOT NULL AUTO_INCREMENT,
  `sbi_supplier` int(11) NOT NULL DEFAULT '0',
  `sbi_image` text,
  `sbi_is_default` int(11) NOT NULL DEFAULT '0',
  `sbi_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sbi_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

ALTER TABLE `cpnl_users` ADD `usr_banner` VARCHAR(255) NULL AFTER `usr_avatar`;


CREATE TABLE `fujeeka_server`.`cpnl_complaint_types` ( `c_id` INT NOT NULL AUTO_INCREMENT , `c_type` VARCHAR(150) NOT NULL , `c_name` VARCHAR(150) NOT NULL , `c_status` INT NOT NULL DEFAULT '1' , PRIMARY KEY (`c_id`)) ENGINE = InnoDB;

30-1-2019 - Irfan
-----------------
ALTER TABLE `cpnl_supplier_master` ADD `supm_domain_prefix` VARCHAR(100) NOT NULL AFTER `supm_desc`;



04-2-2019 - dkv
-----------------
CREATE TABLE `fujeeka_server`.`cpnl_property_infringement` ( `id` INT NOT NULL AUTO_INCREMENT , `type` VARCHAR(100) NOT NULL , `product_link` TEXT NOT NULL , `complaint_desc` TEXT NOT NULL , `company_name` VARCHAR(100) NOT NULL , `complainant _name` VARCHAR(100) NOT NULL , `complainant _email` VARCHAR(100) NOT NULL , `complainant _phone` VARCHAR(100) NOT NULL , `id_type` VARCHAR(100) NOT NULL , `b_id` VARCHAR(100) NOT NULL , `attachment` VARCHAR(150) NOT NULL , `ipr_name` VARCHAR(100) NOT NULL , `ipr_id_type` VARCHAR(100) NOT NULL , `ipr_b_id` VARCHAR(100) NOT NULL , `ipr_attachment` VARCHAR(150) NOT NULL , `ipr_poa` VARCHAR(150) NOT NULL , `stat` INT NOT NULL DEFAULT '0' , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

04-02-1019 - JK
---------------
ALTER TABLE `cpnl_supplier_banner_images` ADD `sbi_type` INT(1) NOT NULL DEFAULT '1' COMMENT '1- Website, 2 - Mobile/App' AFTER `sbi_status`;

09-2-2019 - dkv
ALTER TABLE `cpnl_property_infringement` CHANGE `attachment` `attachment_file` VARCHAR(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `cpnl_property_infringement` CHANGE `stat` `status` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `cpnl_property_infringement` ADD `seen` INT NOT NULL DEFAULT '0' AFTER `status`;
ALTER TABLE `cpnl_property_infringement` ADD `user_id` INT NOT NULL AFTER `id`;

14-2-2019 - dkv
ALTER TABLE `cpnl_products_images` ADD `pimg_alttag` VARCHAR(250) NULL DEFAULT NULL AFTER `pimg_image`;

ALTER TABLE `cpnl_products_images` CHANGE `pimg_alttag` `pimg_alttag` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'product image';

15-2-2019 - dkv
ALTER TABLE `cpnl_supplier_master` ADD `supm_panoramic_alttag` VARCHAR(250) NOT NULL DEFAULT 'supplier panorama image' AFTER `supm_panoramic_image`;

ALTER TABLE `cpnl_supplier_banner_images` ADD `sbi_alttag` VARCHAR(250) NOT NULL DEFAULT 'supplier banner image' AFTER `sbi_image`;

ALTER TABLE `cpnl_supplier_shop_images` ADD `ssi_alttag` VARCHAR(250) NOT NULL DEFAULT 'supplier shop image' AFTER `ssi_image`;

ALTER TABLE `cpnl_users` ADD `alt_user_avatar` VARCHAR(250) NOT NULL DEFAULT 'user image' AFTER `usr_avatar`;

16-2-2019 - dkv

ALTER TABLE `cpnl_logistics_comp_images` ADD `ici_alttag` VARCHAR(250) NOT NULL DEFAULT 'logistics image' AFTER `lci_image`;

ALTER TABLE `cpnl_banner` ADD `bnr_imagealt` VARCHAR(250) NOT NULL DEFAULT 'banner image' AFTER `bnr_image`;

ALTER TABLE `cpnl_advertisement` ADD `add_imgalt` VARCHAR(250) NOT NULL DEFAULT 'add image' AFTER `add_image`;

ALTER TABLE `cpnl_market_places` ADD `mar_imgalt` VARCHAR(250) NOT NULL DEFAULT 'market place' AFTER `mar_image`;


19-2-2019 - dkv

ALTER TABLE `cpnl_buildings_images` ADD `bimg_alt` VARCHAR(250) NOT NULL DEFAULT 'building image' AFTER `bimg_image`;


27-2-2019 - dkv

ALTER TABLE `cpnl_logistics_services_images` ADD `img_altatag` VARCHAR(250) NOT NULL DEFAULT 'logistics service' AFTER `logsi_image`;

01-03-2019 - Irfan
-------------------
INSERT INTO `cpnl_settings` (`set_id`, `set_key`, `set_value`, `set_date`, `set_status`) VALUES (NULL, 'thumbnail_width', '50', CURRENT_TIMESTAMP, '1');
INSERT INTO `cpnl_settings` (`set_id`, `set_key`, `set_value`, `set_date`, `set_status`) VALUES (NULL, 'thumbnail_height', '44', CURRENT_TIMESTAMP, '1');
INSERT INTO `cpnl_settings` (`set_id`, `set_key`, `set_value`, `set_date`, `set_status`) VALUES (NULL, 'home_page_market', '1', CURRENT_TIMESTAMP, '1');

20-03-2019 - Irfan
-------------------
ALTER TABLE `cpnl_products` CHANGE `prd_price_max` `prd_price_max` DECIMAL(10,2) NULL DEFAULT NULL;
ALTER TABLE `cpnl_products` CHANGE `prd_offer_price_min` `prd_offer_price_min` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0';
ALTER TABLE `cpnl_products` CHANGE `prd_offer_price_min` `prd_offer_price_min` DOUBLE(10,2) NULL DEFAULT '0';
ALTER TABLE `cpnl_products` CHANGE `prd_offer_price_max` `prd_offer_price_max` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0';
ALTER TABLE `cpnl_products` CHANGE `prd_offer_price_max` `prd_offer_price_max` DECIMAL(10,2) NULL DEFAULT '0'; 


CREATE TABLE `fujeeka_server`.`web_service_request` ( `id` INT NOT NULL AUTO_INCREMENT , `request_type` VARCHAR(25) NOT NULL , `method` VARCHAR(75) NOT NULL , `request_data` TEXT NOT NULL , `server` TEXT NOT NULL , `uploads` TEXT NOT NULL , `date_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;