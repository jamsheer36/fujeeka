
<?php
class LanguageLoader
{
    public function initialize()
    {
        $ci = &get_instance();
        $ci->load->helper('language');
        $fujeekaLange = $ci->session->userdata('fujeeka_lang');
        if ($fujeekaLange) {
            $ci->lang->load('common', $fujeekaLange);
        } else {
            $ci->lang->load('common', 'english');
        }
    }
}
