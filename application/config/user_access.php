<?php
  $config['modules_exclude'] = array(
      'f_cpanel' => array(
          'index' => 'index',
          'login' => 'login'
      ),
      'user_permission' => array(
          'access_denied' => 'Access denied'
      ),
      'emp_details' => array(
          'updateprofile' => 'updateProfile',
          'migrate' => 'migrate'
      ),
      'dashboard' => array(
          'index' => 'Home'
      ),
      'general' => array(
          'showme' => 'showme',
          'getnotifications' => 'Get Notifications',
          'newslettersub' => 'newsletterSub',
          'douploadfromeditor' => 'doUploadFromEditor'
      ),
      'clean' => array(
          'index' => 'index',
          'cleanenquires' => 'cleanEnquires'
      ),
      'building' => array(
          'getbuildingcategories' => 'getBuildingCategories',
          'setdefaultimage' => 'setDefaultImage',
          'removeimage' => 'removeImage'
      ),
      'category' => array(
          'removeimage' => 'removeImage',
          'removebanner' => 'removeBanner',
          'checkcategoryexists' => 'checkCategoryExists',
          'getsubcategories' => 'getSubCategories'
      ),
      'market' => array(
          'removeimage' => 'removeImage',
          'getcaregorybymarket' => 'getCaregoryByMarket',
          'markets' => 'markets',
          'market_register' => 'market_register'
      ),
      'supplier' => array(
          'setdefaultimage' => 'setDefaultImage',
          'removeavatar' => 'removeAvatar',
          'removeimage' => 'removeImage',
          'checkifvalueexists' => 'checkIfValueExists',
          'supplier_list' => 'supplier_list',
          'activate_supplier' => 'Activate supplier',
          'home' => 'home',
          'contact' => 'contact',
          'profile' => 'profile',
          'supplier_product' =>'products',
          'addtofavorate' => 'addToFavorate',
          'get_360' =>'get_360',
          'enquiry' =>'enquiry',
          'supplier_register' => 'supplier register',
          'feeds' => 'feeds',
          'getsuppliercategories' => 'getsuppliercategories',
          'get_360_list' => 'get_360_list',
          'myprofile' => 'myprofile'
      ),
      'supplier_privilege' => array(
          'setpermission' => 'Set permission',
          'getpermission' => 'Get permission',
      ),
      'user_permission' => array(
          'setpermission' => 'Set permission',
          'getpermission' => 'Get permission',
      ),
      'home' => array(
          'index' => 'index',
          'search' => 'search',
          'get_app' =>'get_app',
          'productsearch' => 'productsearch',
      ),
      'commodity' => array(
          'index' => 'index'
      ),
      'states' => array(
          'getstatesbycountry' => 'getStatesByCountry'
      ),
      'user' => array(
          'index' => 'index',
          'insert' => 'insert',
          'login' => 'Login',
          'signup' => 'Signup',
          'logout' => 'Logout',
          'checkemailexists' => 'checkEmailExists',
          'signupfirststep' => 'signupFirstStep',
          'activate' => 'activate',
          'update' => 'insert',
          'logout' => 'Logout',
          'inserttestvalues' => 'insertTestValues',
          'forgot_password' => 'Forgot password',
          'reset_password' => 'reset password',
          'refreshcaptcha' => 'Refresh Captcha',
          'changeuserpermissions' => 'changeUserPermissions',
          'chat' => 'chat',
          'loadchat' => 'loadChat',
          'pusher' => 'pusher',
          'become_seller' =>'become_seller',
          'chat_head' =>'chat_head',
          'seller_requests'=>'seller_requests',
          'change_password' => 'change password',
          'verify_mail' => 'verify_mail',
          'change_mail' => 'change_mail',
          'personal_information' => 'Personal info',
          'quick_login' =>'quick_login',
          'reset_password_modal' => 'reset_password_modal',
          'sup_home' => 'sup_home'
      ),
      'product' => array(
          'product_listing' => 'product_listing',
          'product_details' => 'product_details',
          'productautospecification' => 'productAutoSpecification',
          'contact_supplier' => 'contact_supplier',
          'supplier_products' => 'supplier_products',
          'docontactsupplier' => 'doContactSupplier',
          'productautoname' => 'productAutoName',
          'getsuppliernotification' => 'get all notification',
          'setdefaultimage' => 'setDefaultImage',
          'removeimage' => 'removeImage',
          'sendrfqquote' => 'sendRfqQuote',
          'search' => 'search',
          'editorimageupload' => 'editorImageUpload',
          'category' => 'category',
          'sendrfqcomments' => 'sendRFQComments',
          'loadrfqcomments' => 'loadRFQComments',
          'loadseperaterfqcomment' => 'loadseperaterfqcomment',
          'stock' =>'stock',
          'rfq'=>'rfq',
          'activeproducts' => 'activeProducts',
          'active' => 'active'
      ),
      'mailbox' => array(
          'index' => 'index',
          'read' => 'read',
          'outbox' => 'outbox',
          'inbox' => 'inbox',
          'deletemail' => 'deleteMail',
          'unread' => 'unread',
          'trash' => 'trash',
          'sent' => 'sent',
          'bindrecipient' => 'Bind recipient',
          'filterrecipient' => 'filterRecipient',
          'reply' => 'reply'
      ),
      'add' => array(
          'removeimage' => 'removeImage'
      ),
      'favorites' => array(
          'addproducttofavoritelist' => 'addproducttofavoritelist',
          'addsuppliertofavoritelist' => 'addSupplierToFavoriteList',
          'supplier_list' => 'supplier_list',
          'product_list' => 'product_list',
          'removefaveprod' => 'removefaveprod',
          'removefavesupp' => 'removefavesupp',
          'addtofollow' => 'addToFollow',
          'removefollow' => 'addToFollow',
          'following' => 'following',
          'followers' => 'followers'
      ),
      'terms_policies' => array(
          'index' => 'index',
          'listing_policy' => 'listing_policy',
          'terms_of_use' => 'terms_of_use',
          'intellectual_property' => 'intellectual_property',
          'complaint_terms' => 'complaint_terms',
          'service_agreement' => 'service_agreement',
          'rfq_policy' => 'rfq_policy',
          'advertising_policy' => 'advertising_policy',
          'faq' => 'faq',
          'contact_us' =>'contact_us',
          'complaints' => 'complaints',
          'complaint_form' => 'complaint_form',
          'about_us' => 'about_us',
          'terms_conditions' => 'terms_conditions',
          'quality_control' => 'quality-control',
          'registration_process' => 'registration-process',
          'sitemap' => 'sitemap'
      ),
      'order' => array(
          'sendcomments' => 'sendComments',
          'loadinquirycomments' => 'loadInquiryComments'
      ),
      'logistics' => array(
          'index' => 'default',
          'register' => 'register',
          'removeimage' => 'removeImage',
          'setdefaultimage' => 'setDefaultImage',
          'getquote' => 'getQuote',
          'newlogisticsquote' => 'newLogisticsQuote',
          'request_quotes_details' => 'request_quotes_details'
      ),
      'quality_control' => array(
          'index' => 'default',
          'register' => 'register',
          'removeimage' => 'removeImage',
          'setdefaultimage' => 'setDefaultImage',
          'getquote' => 'getQuote',
          'newqaquote' => 'newLogisticsQuote',
          'request_quotes_details' => 'request_quotes_details'
      ),

      'complaint' => array(
        'index' => 'index',
        'update' => 'update',
        'updatestatus' => 'updatestatus',
        'complaint_details' => 'complaint_details',
        'infringement_complaints' => 'infringement_complaints',
        
        
      ),
      
      'property1_complaints' => array(
        'index' => 'index',
        'update' => 'update',
        'updatestatus' => 'updatestatus',
        'complaint_details' => 'complaint_details'
      ),

      'complaint_register' => array(
        'index' => 'index',
        'register' => 'register',
        'registration' => 'registration',
        'register_complaint' => 'register_complaint',
        'complaints_property_review' =>'complaints_property_review',
        'register_property_infringement' =>'register_property_infringement',
        'productsearch' => 'productsearch',
    ),
    
      'contact' => array(
        'enquiry' => 'enquiry'
    )

);

$config['modules'] = array(
      'emp_details' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete',
          'changeuserstatus' => 'Change user status'
      ),
      'building' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete'
      ),
      'category' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete'
      ),
      'states' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete'
      ),

      'country' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete'
      ),
      'market' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete',
          'registrations' => 'Registrations'
      ),
      'supplier' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete',
          'changestatus' => 'Change suppliers status',
          'removesupplierbanner' => 'Delete Supplier Banner',
          'setdefaultbannerimage' => 'Set Default Banner',
          'removepanorama' => 'Remove Panorama Image',
          'report' => 'Export Supplier Report'
          
      ),
      'supplier_grade' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete'
      ),
      'supplier_privilege' => array(
          'index' => 'Set supplier permission'
      ),
      'user_permission' => array(
          'index' => 'Set user permission'
      ),
      'category_stock' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete'
      ),
      'order' => array(
          'index' => 'List inquiry',
          'order_summery' => 'View inquiry summery',
          'changeorderstatuse' => 'Change inquiry status',
          'report' => 'Export Inquiry Report'
      ),

      'product' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete',
          'changestatus' => 'Change product status',
          'rfq' => 'RFQ',
          'new_rfq' => 'new rfq',
          'rfq_list' => 'List RFQ',
          'pending_rfq_list' => 'Pending RFQ list',
          'report' => 'Export RFQ Report',
          'product_report' => 'Export Product Report'
      ),

      'units' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete'
      ),
      'manage_banner' => array(
          'index' => 'List',
          'add' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete'
      ),
      'add' => array(
          'index' => 'List',
          'newadd' => 'Add',
          'view' => 'View',
          'update' => 'Update',
          'delete' => 'Delete'
      ),
      'feed' => array(
        'index' => 'index',
        'add_feed' => 'add_feed',
        'delete' => 'delete',
        'update' => 'update',
      ),
      'notification' => array(
          'pushnotification' => 'pushnotification'
      ),
      'logistics' => array(
          'listlogistics' => 'List logistics',
          'edit' => 'Update logistics',
          'add' => 'Add new logistics',
          'changestatus' => 'Change logistics status',
          'services' => 'List services',
          'new_service' => 'Create new service',
          'edit_service' => 'Update service',
          'delete_service' => 'Delete service',
          'request_quotes' => 'Request quotes'
      ),
      'quality_control' => array(
          'listqa' => 'List quality control',
          'edit' => 'Update quality control',
          'add' => 'Add new quality control',
          'changestatus' => 'Change quality control status',
          'services' => 'List services',
          'new_service' => 'Create new service',
          'edit_service' => 'Update service',
          'delete_service' => 'Delete service',
          'request_quotes' => 'Request quotes'
      )

  );
  
  //'lossofenquiryvehicles' => 'Loss of sale or purchase'