<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  /*
    |--------------------------------------------------------------------------
    | File and Directory Modes
    |--------------------------------------------------------------------------
    |
    | These prefs are used when checking and setting modes when working
    | with the file system.  The defaults are fine on servers with proper
    | security, but you may wish (or even need) to change the values in
    | certain environments (Apache running a separate process for each
    | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
    | always be used to set the mode correctly.
    |
   */
  define('FILE_READ_MODE', 0644);
  define('FILE_WRITE_MODE', 0666);
  define('DIR_READ_MODE', 0755);
  define('DIR_WRITE_MODE', 0777);

  /*
    |--------------------------------------------------------------------------
    | File Stream Modes
    |--------------------------------------------------------------------------
    |
    | These modes are used when working with fopen()/popen()
    |
   */

  define('FOPEN_READ', 'rb');
  define('FOPEN_READ_WRITE', 'r+b');
  define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
  define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
  define('FOPEN_WRITE_CREATE', 'ab');
  define('FOPEN_READ_WRITE_CREATE', 'a+b');
  define('FOPEN_WRITE_CREATE_STRICT', 'xb');
  define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

  define('TABLE_PREFIX', 'cpnl_');
  define('TABLE_PREFIX_RANA', 'rana_');
  define('UPLOAD_PATH', './assets/uploads/');

  define('DEFAULT_THUMB_H', 44);
  define('DEFAULT_THUMB_W', 50);

  define('FILE_UPLOAD_PATH', './assets/uploads/');

  define('PUSHER_APP_KEY', '853be53b86c43083fe5c');
  define('PUSHER_APP_SECRET', '9e8eb557eb1c089a631e');
  define('PUSHER_APP_ID', '609938');
  define('PUSHER_CLUSTER', 'ap2');
  define('PUSHY_SECRET_API_KEY', '4550461087bd86217c72d3efc9fae2ef2bccfb05e998a4d6d7ef67382e882c29');
  define('PUSHY_SUP_SECRET_API_KEY', 'fc855639ac3887fa414bc0b64a33365583f083c240579d9d88cc3f650bdb90d4');

  define('FOLLOW_UP_STATUS', serialize(array('1' => 'Hot', '2' => 'Warm', '3' => 'Cold')));
  define('ENQUIRY_UP_STATUS', serialize(array('1' => 'Hot', '2' => 'Warm', '3' => 'Cold')));
  define('VEHICLE_DETAILS_STATUS', serialize(array('1' => 'Sale', '2' => 'Buy', '3' => 'Exchange')));
  define('FUAL', serialize(array('2' => 'Diesel', '1' => 'Petrol', '3' => 'Gas', '4' => 'Hybrid', '5' => 'Electric' ,'6' => 'CNG')));
  define('CUST_AGE_GROUP', serialize(array('20-30' => '20-30', '30-40' => '30-40', '40-50' => '40-50')));
  define('MODE_OF_CONTACT', serialize(array(
      '1' => 'Phone', 
      '2' => 'Whatsup', 
      '3' => 'Mail', 
      '4' => 'Facebook', 
      '5' => 'Events',
      '6' => 'Referal', 
      '7' => 'OLX', 
      '8' => 'Fasttrack', 
      '9' => 'Showroom', 
      '10' => 'C/O MD', 
      '11' => 'C/O VP', 
      '12' => 'C/O CEO', 
      '13' => 'C/O Others',
      '14' => 'Car Trade',
      '15' => 'Just Dial',
      '16' => 'Car Wale',
      '17' => 'Field')));
  define('MODE_OF_CONTACT_FOLLOW_UP', serialize(array('1' => 'Telephone', '2' => 'Direct meet', '3' => 'Showroom visit')));
  define('INSURANCE_TYPES', serialize(array('1' => 'RTI', '2' => 'Platinum/Gold/Silver', '3' => 'B2B', '4' => 'First Class', '5' => 'Second Class')));
  define('EVALUATION_TYPES', serialize(array('1' => 'Our own', '2' => 'Park and sale', '3' => 'Park and sale with customer')));
  
  /*Definition for today*/
  define('TODAY', 'DATE(CURDATE())'); 
  define('YESTERDAY', 'DATE(DATE_ADD(CURDATE(), INTERVAL -1 DAY))'); 
  
  define('STATIC_TITLE' , 'fujeeka');


  define('MAIL_HOST', 'mail.halal86.com');
  define('MAIL_USERNAME', 'info@halal86.com');
  define('MAIL_PASSWORD', '[ly$-2hup.}7');
  define('FROM_MAIL', 'info@halal86.com');
  define('FROM_NAME', 'Fujishka');
  define('REPLY_TO_MAIL', 'info@halal86.com');
  define('REPLY_TO_NAME', 'Fujishka');