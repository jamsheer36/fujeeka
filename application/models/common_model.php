<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Common_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tbl_users = TABLE_PREFIX . 'users';
        $this->tbl_groups = TABLE_PREFIX . 'groups';
        $this->tbl_products = TABLE_PREFIX . 'products';
        $this->tbl_statuses = TABLE_PREFIX . 'statuses';
        $this->tbl_qc_quotes = TABLE_PREFIX . 'qc_quotes';
        $this->tbl_general_log = TABLE_PREFIX . 'general_log';
        $this->tbl_user_access = TABLE_PREFIX . 'user_access';
        $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
        $this->tbl_market_places = TABLE_PREFIX . 'market_places';
        $this->tbl_favorite_list = TABLE_PREFIX . 'favorite_list';
        $this->tbl_mailboox_master = TABLE_PREFIX . 'mailboox_master';
        $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
        $this->tbl_logistics_quotes = TABLE_PREFIX . 'logistics_quotes';
        $this->tbl_rfq_notification = TABLE_PREFIX . 'rfq_notification';
        $this->tbl_supplier_followers = TABLE_PREFIX . 'supplier_followers';
        $this->tbl_newsletter_subscribe = TABLE_PREFIX . 'newsletter_subscribe';
        $this->tbl_complaints = TABLE_PREFIX . 'complaints';
        $this->tbl_property_infringement = TABLE_PREFIX . 'property_infringement';
        $this->tbl_contacts = TABLE_PREFIX . 'contacts';

    }

    public function getUser($id = null, $excludeAdminUser = false)
    {

        $this->db->select($this->tbl_users . '.*');
        if ($excludeAdminUser) {
            $this->db->where("usr_id !=", 1);
        }

        if ($id) {
            $this->db->where('usr_id', $id);
            $user = $this->db->get($this->tbl_users)->row_array();
        } else {
            $user = $this->db->get($this->tbl_users)->result_array();
        }
        $permission = $this->db->select(array($this->tbl_user_access . '.*'))->where($this->tbl_user_access . '.cua_group_id', $id)
            ->get($this->tbl_user_access)->row_array();
        return array_merge($user, $permission);
    }

    public function generateLog($data, $table)
    {
        if (!empty($data)) {
            $this->db->insert(TABLE_PREFIX . $table, $data);
            return true;
        } else {
            return false;
        }
    }

    public function clearSupplier()
    {
        $this->db->truncate(TABLE_PREFIX . 'supplier_master');
        $this->db->truncate(TABLE_PREFIX . 'supplier_shop_images');
        $this->db->truncate(TABLE_PREFIX . 'supplier_grade');
        $this->db->truncate(TABLE_PREFIX . 'supplier_keywords');
        $this->db->truncate(TABLE_PREFIX . 'supplier_buildings_cate_assoc');

        $this->db->where('usr_id != 1');
        $this->db->delete(TABLE_PREFIX . 'users');

        return true;
    }

    public function cleanProduct()
    {
        $this->db->truncate(TABLE_PREFIX . 'products');
        $this->db->truncate(TABLE_PREFIX . 'products_categories');
        $this->db->truncate(TABLE_PREFIX . 'products_images');
        $this->db->truncate(TABLE_PREFIX . 'products_keyword');
        $this->db->truncate(TABLE_PREFIX . 'products_specification');
    }

    public function getStatuses($category)
    {
        return $this->db->where(array('sts_category' => $category, 'sts_status' => 1))
            ->order_by('sts_order')
            ->like('sts_access', $this->usr_grp, 'BOTH')
            ->get($this->tbl_statuses)->result_array();
    }

    public function addToRecommendation($keyword, $uid)
    {
        foreach ($keyword as $row) {
            $res = $this->db->where('rec_key_word', $row['pkwd_val_en'])->where('rec_user_id', $uid)->get(TABLE_PREFIX . 'recommendation')->result_array();
            if (!$res) {
                $this->db->set('rec_user_id', $uid);
                $this->db->set('rec_key_word', $row['pkwd_val_en']);
                $this->db->insert(TABLE_PREFIX . 'recommendation');
            }
        }
        return 1;
    }

    public function getRecommendations($id)
    {
        $limit = get_settings_by_key('total_keyword_for_recommendation');
        $rec = $this->db->select('distinct(rec_key_word)')
            ->where('rec_user_id', $id)
            ->order_by('rec_created_at', 'desc')
            ->limit($limit)
            ->get(TABLE_PREFIX . 'recommendation')->result_array();

        if ($rec) {
            //$rec = implode(',', array_column($rec, 'rec_key_word')); array_column will not work with server php version
            //alternate for array_column
            $newarr = array();
            foreach ($rec as $row) {
                $newarr[] = $row['rec_key_word'];
            }
            $rec = implode(',', $newarr);
            //alternate for array_column end
            // $produt_ids = $this->db->query("select pkwd_product,MATCH(pkwd_val_en, pkwd_val_ar, pkwd_val_ch) AGAINST ('$rec' IN NATURAL LANGUAGE MODE) as search_score from cpnl_products_keyword where MATCH(pkwd_val_en, pkwd_val_ar,pkwd_val_ch) AGAINST ('$rec' IN NATURAL LANGUAGE MODE)>0 order by search_score  desc limit $limit")->result_array();
            $produt_ids = $this->db->query("select pkwd_product from " . TABLE_PREFIX . "products_keyword where MATCH(pkwd_val_en, pkwd_val_ar,pkwd_val_ch) AGAINST ('$rec' IN NATURAL LANGUAGE MODE) limit $limit")->result_array();
            if ($produt_ids) {
                //alternate for array_column
                $newarr = array();
                foreach ($produt_ids as $row) {
                    $newarr[] = $row['pkwd_product'];
                }
                $produt_ids = $newarr;
                //alternate for array_column end

                $this->db->select('*');
                $this->db->limit(12);
                $this->db->where_in('prd_id', $produt_ids);
                // $this->db->where_in('prd_id', array_column($produt_ids, 'pkwd_product'));array_column will not work with server php version
                $products = $this->db->get(TABLE_PREFIX . 'products')->result_array();
                foreach ($products as $key => $val) {
                    if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                        $images = $this->db->get_where(TABLE_PREFIX . 'products_images', array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                    } else {
                        $images = $this->db->limit(1)->get_where(TABLE_PREFIX . 'products_images', array('pimg_product' => $val['prd_id']))->row_array();
                    }
                    $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                    //     $products[$key]['default_image'] = 'assets/uploads/product/173x150_' . $defaultImg;
                    $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
                }
                return json_encode($products);
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function insertProducts($count)
    {

        $image_array = array(
            '6318054advt3.jpg', 'earphone.jpg', 'mcard.jpg', 'mcard2.jpg', 'mcard3.jpg',
            'mobilestand.jpg', 'phone.jpg', 'phone-cover.jpg', 'powerbank.jpg', 'prod-3.jpg', 'watch.jpg',
        );
        for ($i = 0; $i < $count; $i++) {
            $item = array_rand($image_array);
            $name = substr(str_shuffle("QWERTYUIOPASDFGHJKLMNBVCXZ"), 0, 5);
            $product = array(
                'prd_number' => rand(999, 1000000),
                'prd_supplier' => rand(1, 100),
                'prd_name_en' => $name,
                'prd_name_ar' => $name,
                'prd_name_ch' => $name,
                'prd_oem' => rand(0, 1),
                'prd_hot_selling' => rand(0, 1),
                'prd_moq' => rand(10, 999),
                'prd_price_min' => rand(1000, 9999),
                'prd_price_max' => rand(100000, 999999),
                'prd_offer_price_min' => rand(100, 999),
                'prd_offer_price_max' => rand(10000, 99999),
                'prd_desc' => substr(str_shuffle("QWERTYUIOPASDFGHJKLMNBVCXZ"), 0, 10),
                'prd_video' => 'No video',
            );
            $this->db->insert(TABLE_PREFIX . 'products', $product);
            $lastId = $this->db->insert_id();

            $image = array(
                'pimg_product' => $lastId,
                'pimg_image' => $image_array[$item],
            );
            $this->db->insert(TABLE_PREFIX . 'products_images', $image);
            resize(FILE_UPLOAD_PATH . 'product/' . $image_array[$item], 275, 206);
            resize(FILE_UPLOAD_PATH . 'product/' . $image_array[$item], 154, 116);
            resize(FILE_UPLOAD_PATH . 'product/' . $image_array[$item], 207, 180);
            resize(FILE_UPLOAD_PATH . 'product/' . $image_array[$item], 173, 150);
            resize(FILE_UPLOAD_PATH . 'product/' . $image_array[$item], 450, 338);

            $cat = array(
                'pcat_product' => $lastId,
                'pcat_category' => rand(1, 12),
            );
            $this->db->insert(TABLE_PREFIX . 'products_categories', $cat);

            $keys = array(
                'pkwd_product' => $lastId,
                'pkwd_val_en' => $name,
                'pkwd_val_ar' => $name,
                'pkwd_val_ch' => $name,
            );

            $this->db->insert(TABLE_PREFIX . 'products_keyword', $keys);

            $specifi = array(
                'psp_product' => $lastId,
                'psp_key_en' => $name,
                'psp_key_ar' => $name,
                'psp_key_ch' => $name,
                'psp_val_en' => $name,
                'psp_val_ar' => $name,
                'psp_val_ch' => $name,
            );
            $this->db->insert(TABLE_PREFIX . 'products_specification', $specifi);
        }
    }

    public function insertSupplier($count)
    {

        $image_array = array(
            '3285001tv.jpeg', '40088851_(1).jpg',
        );

        for ($i = 0; $i < $count; $i++) {
            $item = array_rand($image_array);
            $name = substr(str_shuffle("QWER TYUIOP ASD FG HJKL MNB VCXZ"), 0, 15);
            $supplier = array(
                'supm_ref_number' => rand(999, 1000000),
                'supm_name_en' => $name,
                'supm_email' => $name . '@gmail.com',
                'supm_number' => rand(9000000000, 9999999999),
                'supm_country' => 101,
                'supm_state' => 19,
                'supm_city_en' => 'Calicut',
                'supm_market' => 1,
                'supm_building' => 1,
                'supm_address_en' => $name,
                'supm_room_number' => rand(100, 999),
                'supm_reg_year' => rand(2000, 2018),
                'supm_grade' => 1,
                'supm_trade_assurance_en' => 'Trade assurance',
                'supm_transaction_level_en' => 'Higher',
                'supm_total_revenue' => rand(999, 1000000),
                'supm_transactions_en' => 'Trnsaction',
                'supm_official_video' => 'none',
                'supm_home_banner' => '7523007banner2.jpg',
                'supm_panoramic_image' => '1093119jedel360.jpg',
            );
            $this->db->insert(TABLE_PREFIX . 'supplier_master', $supplier);
            $lastId = $this->db->insert_id();

            $this->db->insert(TABLE_PREFIX . 'supplier_buildings_cate_assoc', array(
                'sbca_supplier' => $lastId,
                'sbca_building' => 1,
                'sbca_category' => 1,
            ));

            $this->db->insert(TABLE_PREFIX . 'supplier_keywords', array(
                'supk_supplier' => $lastId,
                'supk_keyword_en' => $name,
                'supk_keyword_ar' => $name,
                'supk_keyword_ch' => $name,
            ));

            $image = array(
                'ssi_supplier' => $lastId,
                'ssi_image' => $image_array[$item],
            );
            $this->db->insert(TABLE_PREFIX . 'supplier_shop_images', $image);
            //resize(FILE_UPLOAD_PATH . 'shops/' . $image_array[$item], 660, 396);
            $this->load->model('emp_details/emp_details_model', 'emp_details');
            $user['usr_group'] = 2;
            $user['usr_supplier'] = $lastId;

            $user['usr_username'] = $name . $lastId;
            $user['usr_password'] = '123456';
            $user['usr_email'] = 'supplier' . $lastId . '@gmail.com';
            $user['usr_first_name'] = $name;
            $user['usr_last_name'] = $name;
            $user['usr_phone'] = rand(9000000000, 9999999999);
            $user['usr_address'] = 'Calicut';
            $user['usr_city'] = 'Calicut';
            $user['usr_country'] = 101;
            $user['usr_state'] = 19;
            $this->emp_details->register($user);
        }
    }

    public function getAllCategories($parent = '')
    {
        $this->db->select('cat_id,cat_title');
        $this->db->where('cat_status', 1);
        if ($parent) {
            $this->db->where('cat_parent', $parent);
        } else {
            $this->db->where('cat_parent', '!=', 0);
        }
        return $this->db->get(TABLE_PREFIX . 'category')->result_array();
    }

    /**
     * Show the pending request on top of admin panel
     * @param type $limit
     * @return type
     * Author : JK
     */
    public function getNewSuppliersNotification($limit = '10')
    {
        $return['total'] = count($this->db->select($this->tbl_supplier_master . '.*,' . $this->tbl_users . '.*')
                ->join($this->tbl_users, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_users . '.usr_supplier', 'RIGHT')
                ->where($this->tbl_supplier_master . '.supm_status = 0')->order_by('supm_added_on', 'DESC')
                ->get($this->tbl_supplier_master)->result_array());

        $return['pendingSupRequest'] = $this->db->select($this->tbl_supplier_master . '.*,' . $this->tbl_users . '.*')
            ->join($this->tbl_users, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_users . '.usr_supplier', 'RIGHT')
            ->where($this->tbl_supplier_master . '.supm_status = 0')->order_by('supm_added_on', 'DESC')
            ->limit($limit)->get($this->tbl_supplier_master)->result_array();
        return $return;
    }

    /**
     * Return supplier by supplier id
     * @param int $id
     * @return array
     */
    public function getSupplier($id)
    {
        return $this->db->select($this->tbl_supplier_master . '.*,' . $this->tbl_users . '.*')
            ->join($this->tbl_users, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_users . '.usr_supplier', 'LEFT')
            ->where($this->tbl_supplier_master . '.supm_id', $id)->get($this->tbl_supplier_master)->row_array();
    }

    /**
     * Check permission exists for specified user.by passing user id and user slug like supplier is SP
     * @param string $grpslug
     * @return boolean
     */
    public function privilegeExists($grpslug, $userid = '')
    {
        $userid = empty($userid) ? $this->uid : $userid;
        if (!empty($grpslug)) {
            $permission = $this->db->select($this->tbl_users_groups . '.*,' . $this->tbl_groups . '.*')
                ->join($this->tbl_groups, $this->tbl_groups . '.id=' . $this->tbl_users_groups . '.group_id', 'LEFT')
                ->where(array($this->tbl_users_groups . '.user_id' => $userid, $this->tbl_groups . '.grp_slug' => $grpslug))
                ->get($this->tbl_users_groups)->result_array();
            return empty($permission) ? false : true;
        }
        return false;
    }

    /**
     * This is a general function to change status field of any table
     * @param int $pkId <primary key id>
     * @param int $ischecked <current status 0/1>
     * @param string $table <table name which we want to edit>
     * @param int $statusFieldName <status field name in specified table>
     * @param string $whrFieldName <primary key field name>
     * @return boolean
     * Author : JK
     */
    public function changeStatus($pkId, $ischecked, $table, $statusFieldName, $whrFieldName)
    {
        $this->db->where($whrFieldName, $pkId);
        if ($this->db->update(TABLE_PREFIX . $table, array($statusFieldName => $ischecked))) {
            return true;
        }
        return false;
    }

    /**
     * Show new added product for activation as admin top banner notification
     * @param int $limit
     * @return array
     * Author : JK
     */
    public function getUnVerifiedProduct($limit = '10')
    {
        $return = array();
        if (is_root_user()) { // If admin logged
            $return['notifiUnverifiedProductCount'] = $this->db->where('prd_status', 0)
                ->count_all_results($this->tbl_products);

            $return['notifiUnverifiedProduct'] = $this->db->select($this->tbl_products . '.*, ' . $this->tbl_users . '.*')
                ->join($this->tbl_users, $this->tbl_products . '.prd_added_by = ' . $this->tbl_users . '.usr_id', 'LEFT')
                ->where($this->tbl_products . '.prd_status', 0)
                ->order_by($this->tbl_products . '.prd_added_on', 'DESC')->limit($limit)->get($this->tbl_products)->result_array();
        } else if (privilege_exists('SP')) { // If supplier logged
            $return['notifiUnverifiedProductCount'] = $this->db->where('prd_status', 0)
                ->where('prd_supplier', $this->uid)->count_all_results($this->tbl_products);

            $return['notifiUnverifiedProduct'] = $this->db->select($this->tbl_products . '.*, ' . $this->tbl_users . '.*')
                ->join($this->tbl_users, $this->tbl_products . '.prd_added_by = ' . $this->tbl_users . '.usr_id', 'LEFT')
                ->where($this->tbl_products . '.prd_status', 0)
                ->where($this->tbl_products . '.prd_supplier', $this->uid)->order_by($this->tbl_products . '.prd_added_on', 'DESC')
                ->limit($limit)->get($this->tbl_products)->result_array();
        }
        return $return;
    }

    /**
     * Function for get unready messages on top panel
     * @param type $limit
     * @return type
     * Author : JK
     */
    public function getUnreadMessage($limit = 10)
    {
        $return = array();
        $return['unreadMail'] = $this->db->select($this->tbl_mailboox_master . '.*,' . $this->tbl_users . '.*')
            ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_mailboox_master . '.mms_from')
            ->where(array($this->tbl_mailboox_master . '.mms_to' => $this->uid,
                $this->tbl_mailboox_master . '.mms_last_viewd_on' => null,
                $this->tbl_mailboox_master . '.mms_status' => 1))
            ->limit($limit)->get($this->tbl_mailboox_master)->result_array();

        $return['unreadMailCount'] = $this->db->get_where($this->tbl_mailboox_master, array('mms_to' => $this->uid, 'mms_last_viewd_on' => null, 'mms_status' => 1))->num_rows();
        return $return;
    }

    public function saveChat($insert)
    {
        if ($insert['c_content'] == '' && $insert['c_attachment'] == '') {
            $cnt = $this->db->where(array('c_from_id' => $insert['c_from_id'], 'c_to_id' => $insert['c_to_id'], 'c_content' => '', 'c_attachment' => ''))->get(TABLE_PREFIX . 'chats')->num_rows();
            if ($cnt) {
                $ins = $insert;
                $insert['c_time'] = microtime(true);
                return $this->db->where($ins)->update(TABLE_PREFIX . 'chats', $insert);
            }
        }
        $insert['c_time'] = microtime(true);
        $this->db->insert(TABLE_PREFIX . 'chats', $insert);
        return $this->db->insert_id();
    }

    public function getUserIdFromSupId($sup_id)
    {
        return $this->db->select($this->tbl_users . '.usr_id')
            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id')
            ->where('usr_supplier', $sup_id)
            ->where('group_id', 2)
            ->get($this->tbl_users)->row_array();
    }

    public function getNotifications()
    {
        $suplier = get_logged_user('usr_supplier');
        $returnHtml = '';
        $limit = 10;
        if (is_root_user()) {
            /* Supplier registration nofitication */
            $supRegi = $this->db->order_by('supm_added_on', 'desc')->get_where($this->tbl_supplier_master, array('supm_status' => 0))->result_array();
            if (!empty($supRegi)) {
                $returnHtml = ' <li role="presentation" class="dropdown ntfySupRegi" data-toggle="tooltip" data-placement="bottom" title="Suppliers">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                          <i class="fa fa-user"></i>
                                          <span class="badge bg-green">' . count($supRegi) . '</span>
                                     </a>'
                    . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                              <div class="scroll-cont" data-simplebar>';
                foreach ((array) $supRegi as $key => $reg) {
                    $returnHtml .= '<li class="drp-mnu">
                            <span class="message">
                                <a href="' . site_url('supplier/activate-supplier/' . encryptor($reg['supm_id'])) . '">New Supplier Registration ' . $reg['supm_email'] . ' Click Here To View</a>
                            </span>
                          </li>';
                }
                $returnHtml .= '</div></ul></li>';
            }

            /* Market registration nofitication */
            $marketNofity = $this->db->get_where($this->tbl_market_places, array('mar_status' => 0))->result_array();
            if (!empty($marketNofity)) {
                $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi" data-toggle="tooltip" data-placement="bottom" title="Markets">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                          <i class="fa fa-sitemap"></i>
                                          <span class="badge bg-green">' . count($marketNofity) . '</span>
                                     </a>'
                    . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                              <div class="scroll-cont" data-simplebar>';
                foreach ((array) $marketNofity as $key => $reg) {
                    $returnHtml .= '<li class="drp-mnu">
                            <span class="message">
                                <a href="' . site_url('market/view/' . encryptor($reg['mar_id'])) . '">New Market Registration ' . $reg['mar_email'] . ' Click Here To View</a>
                            </span>
                          </li>';
                }
                $returnHtml .= '</div></ul></li>';
            }
        }
        /* Unvarified products */
        $productCount = 0;
        $products = array();
        if (is_root_user()) { // If admin logged
            $productCount = $this->db->where('prd_status', 0)->count_all_results($this->tbl_products);

            $products = $this->db->select($this->tbl_products . '.*, ' . $this->tbl_users . '.*')
                ->join($this->tbl_users, $this->tbl_products . '.prd_added_by = ' . $this->tbl_users . '.usr_id', 'LEFT')
                ->where($this->tbl_products . '.prd_status', 0)
                ->order_by($this->tbl_products . '.prd_added_on', 'DESC')->limit($limit)->get($this->tbl_products)->result_array();
        } else if (privilege_exists('SP')) { // If supplier logged
            $supUser = $this->db->get_where($this->tbl_users, array('usr_id' => $this->uid))->row()->usr_supplier;
            $productCount = $this->db->where('prd_status', 0)->where('prd_supplier', $supUser)->count_all_results($this->tbl_products);
            $products = $this->db->select($this->tbl_products . '.*, ' . $this->tbl_users . '.*')
                ->join($this->tbl_users, $this->tbl_products . '.prd_added_by = ' . $this->tbl_users . '.usr_id', 'LEFT')
                ->where($this->tbl_products . '.prd_status', 0)
                ->where($this->tbl_products . '.prd_supplier', $supUser)->order_by($this->tbl_products . '.prd_added_on', 'DESC')
                ->limit($limit)->get($this->tbl_products)->result_array();
        }

        if (!empty($products)) {
            $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi" data-toggle="tooltip" data-placement="bottom" title="Verification Request">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                          <i class="fa fa-shopping-cart"></i>
                                          <span class="badge bg-green">' . $productCount . '</span>
                                     </a>'
                . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                         <div class="scroll-cont" data-simplebar>';
            foreach ((array) $products as $key => $prd) {
                $returnHtml .= '<li class="drp-mnu">
                            <span class="message">
                                <a href="' . site_url('product/view/' . $prd['prd_id']) . '">' . get_snippet($prd['prd_name_en'], 3) . '
                                     <span class="time">Added by : ' . $prd['usr_first_name'] . '</span>
                                </a>
                            </span>
                          </li>';
            }
            $returnHtml .= '</div></ul></li>';
        }
        /* Unread mail */
        $unreadMail = $this->db->select($this->tbl_mailboox_master . '.*,' . $this->tbl_users . '.*')
            ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_mailboox_master . '.mms_from')
            ->where(array($this->tbl_mailboox_master . '.mms_to' => $this->uid,
                $this->tbl_mailboox_master . '.mms_last_viewd_on' => null,
                $this->tbl_mailboox_master . '.mms_status' => 1))->limit($limit)->order_by('mms_added_on', 'desc')->get($this->tbl_mailboox_master)->result_array();

        $unreadMailCount = $this->db->get_where($this->tbl_mailboox_master, array('mms_to' => $this->uid, 'mms_last_viewd_on' => null, 'mms_status' => 1))->num_rows();

        if (!empty($unreadMail)) {
            $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi" data-toggle="tooltip" data-placement="bottom" title="Mail">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                          <i class="fa fa-envelope-o"></i>
                                          <span class="badge bg-green">' . $unreadMailCount . '</span>
                                     </a>'
                . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                         <div class="scroll-cont" data-simplebar>';
            foreach ((array) $unreadMail as $key => $mail) {
                $linkId = empty($mail['mms_parent']) ? $mail['mms_id'] : $mail['mms_parent'];
                $returnHtml .= '<li class="drp-mnu">
                            <span class="message">
                                <a href="' . site_url('mailbox/read/' . encryptor($linkId)) . '">' . $mail['usr_first_name'] . '
                                     <span class="time"> : ' . get_timeago($mail['mms_added_on']) . '</span><br>
                                     ' . get_snippet($mail['mms_subject'], 10) . '
                                </a>
                            </span>
                          </li>';
            }
            $returnHtml .= '</div></ul></li>';
        }

        /* RFQ */
        if ($this->usr_grp != 'ST') {
            $rfqNotify = $this->db->select($this->tbl_rfq_notification . '.*,' .
                'sup.usr_supplier AS sup_usr_supplier, sup.usr_first_name AS sup_usr_first_name, sup.usr_last_name AS sup_usr_last_name, sup.usr_avatar AS sup_usr_avatar,' .
                'buy.usr_id AS buy_usr_id, buy.usr_first_name AS buy_usr_first_name, buy.usr_last_name AS buy_usr_last_name, buy.usr_avatar AS buy_usr_avatar')
                ->join($this->tbl_users . ' sup', 'sup.usr_supplier = ' . $this->tbl_rfq_notification . '.rfqn_supplier', 'LEFT')
                ->join($this->tbl_users . ' buy', 'buy.usr_id = ' . $this->tbl_rfq_notification . '.rfqn_added_by', 'LEFT')
                ->join($this->tbl_products, $this->tbl_products . '.prd_id = ' . $this->tbl_rfq_notification . '.rfqn_added_by', 'LEFT')
                ->where($this->tbl_rfq_notification . '.rfqn_supplier', $this->uid)->where($this->tbl_rfq_notification . '.rfqn_read_on IS NULL')
                ->order_by($this->tbl_rfq_notification . '.rfqn_added_on', 'ASC')->get($this->tbl_rfq_notification)->result_array();

            if (!empty($rfqNotify)) {
                $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi" data-toggle="tooltip" data-placement="bottom" title="RFQ">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                          <i class="fa fa-envelope-o"></i>
                                          <span class="badge bg-green">' . count($rfqNotify) . '</span>
                                     </a>'
                    . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                              <div class="scroll-cont" data-simplebar>';
                foreach ((array) $rfqNotify as $key => $rfq) {
                    $returnHtml .= '<li class="drp-mnu">
                            <span class="message">
                                <a href="' . site_url('product/rfq-list/' . encryptor($rfq['rfqn_rfq_id'])) . '/#messages">New RFQ Message ' .
                    get_snippet($rfq['rfqn_comments'], 3) . ' Click Here To View</a>
                            </span>
                          </li>';
                }
                $returnHtml .= '</div></ul></li>';
            }
        }
        /* Enquiry */
        $this->db->select('ord_id,ord_prod_id,prd_name_en');
        $this->db->join(TABLE_PREFIX . 'products', 'prd_id = ord_prod_id');
        $this->db->where('ord_is_notified', 0);
        $this->db->order_by('ord_added_on', 'desc');
        if (!is_root_user()) {
            $this->db->where('ord_supplier', $suplier);
        }
        $enquires = $this->db->get(TABLE_PREFIX . 'products_order_master')->result_array();

        if (!empty($enquires)) {
            $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi" data-toggle="tooltip" data-placement="bottom" title="Enquiry">'
            . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                          <i class="fa fa-question"></i>
                                          <span class="badge bg-green">' . count($enquires) . '</span>
                                     </a>'
                . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                         <div class="scroll-cont" data-simplebar>';
            foreach ((array) $enquires as $key => $enq) {
                $returnHtml .= '<li class="drp-mnu">
                            <span class="message">
                                <a href = ' . site_url('order/order_summery/' . encryptor($enq['ord_id'])) . '>New Enquiry For ' . $enq['prd_name_en'] . '</a>
                            </span>
                          </li>';
            }
            $returnHtml .= '</div></ul></li>';
        }
        if (is_root_user()) { // If admin logged
            $newLogisticsQuote = $this->db->where('logq_viewed_on IS NULL')->order_by('logq_added_on', 'desc')->get($this->tbl_logistics_quotes)->result_array();
            if (!empty($newLogisticsQuote)) {
                $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi" data-toggle="tooltip" data-placement="bottom" title="Logistics Quotes">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                          <i class="fa fa-truck"></i>
                                          <span class="badge bg-green">' . count($newLogisticsQuote) . '</span>
                                     </a>'
                    . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                         <div class="scroll-cont" data-simplebar>';
                foreach ((array) $newLogisticsQuote as $key => $quotes) {
                    $returnHtml .= '<li class="drp-mnu">
                            <span class="message">
                                <a href = ' . site_url('logistics/request_quotes_details/' . encryptor($quotes['logq_id'])) . '>New Logistics quote from ' . $quotes['logq_email'] . '</a>
                            </span>
                          </li>';
                }
                $returnHtml .= '</div></ul></li>';
            }

            //New logistics registration
            $newLogistics = $this->db->select($this->tbl_users . '.*, ' . $this->tbl_users_groups . '.group_id as group_id')
                ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                ->where(array($this->tbl_users_groups . '.group_id' => 6, $this->tbl_users . '.usr_active' => 0))
                ->order_by('usr_added_date', 'desc')
                ->get($this->tbl_users)->result_array();

            if (!empty($newLogistics)) {
                $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi" data-toggle="tooltip" data-placement="bottom" title="Logistics Registration">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                          <i class="fa fa-truck"></i>
                                          <span class="badge bg-green">' . count($newLogistics) . '</span>
                                     </a>'
                    . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                         <div class="scroll-cont" data-simplebar>';
                foreach ((array) $newLogistics as $key => $quotes) {
                    $returnHtml .= '<li class="drp-mnu">
                            <span class="message">
                                <a href = ' . site_url('logistics/edit/' . encryptor($quotes['usr_id'])) . '>Logistics Request from ' . $quotes['usr_email'] . '</a>
                            </span>
                          </li>';
                }
                $returnHtml .= '</div></ul></li>';
            }

            //New quality control registration
            $newQa = $this->db->select($this->tbl_users . '.*, ' . $this->tbl_users_groups . '.group_id as group_id')
                ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                ->where(array($this->tbl_users_groups . '.group_id' => 7, $this->tbl_users . '.usr_active' => 0))
                ->get($this->tbl_users)->result_array();
            if (!empty($newQa)) {
                $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi" data-toggle="tooltip" data-placement="bottom" title="Quality Control Registration">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                          <i class="fa fa-check-circle"></i>
                                          <span class="badge bg-green">' . count($newQa) . '</span>
                                     </a>'
                    . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                         <div class="scroll-cont" data-simplebar>';
                foreach ((array) $newQa as $key => $quotes) {
                    $returnHtml .= '<li class="drp-mnu">
                            <span class="message">
                                <a href = ' . site_url('quality_control/edit/' . encryptor($quotes['usr_id'])) . '>New Quality control registration from ' . $quotes['usr_email'] . '</a>
                            </span>
                          </li>';
                }
                $returnHtml .= '</div></ul></li>';
            }

            //New quality control
            $newQCQuote = $this->db->where('qcq_viewed_on IS NULL')->order_by('qcq_added_on', 'desc')->get($this->tbl_qc_quotes)->result_array();
            if (!empty($newQCQuote)) {
                $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi"  data-toggle="tooltip" data-placement="bottom" title="Quality Control Quote">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                          <i class="fa fa-check-circle"></i>
                                          <span class="badge bg-green">' . count($newQCQuote) . '</span>
                                     </a>'
                    . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                         <div class="scroll-cont" data-simplebar>';
                foreach ((array) $newQCQuote as $key => $quotes) {
                    $returnHtml .= '<li class="drp-mnu">
                            <span class="message">
                                <a href = ' . site_url('quality_control/request_quotes_details/' . encryptor($quotes['qcq_id'])) . '>New Quality Control Quote from ' . $quotes['qcq_email'] . '</a>
                            </span>
                          </li>';
                }
                $returnHtml .= '</div></ul></li>';
            }

//New td complaint

            $newComplaints = $this->db->select($this->tbl_complaints . '.id, ' . $this->tbl_complaints . '.contact_email,' . $this->tbl_users . '.usr_first_name,' . $this->tbl_users . '.usr_last_name')
                ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_complaints . '.user_id', 'LEFT')
                ->where($this->tbl_complaints . '.seen', 0)
                ->get($this->tbl_complaints)->result_array();

            if (!empty($newComplaints)) {
                $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi"  data-toggle="tooltip" data-placement="bottom" title="Trade Dispute Complaints">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                 <i class="fa fa-list"></i>
                                 <span class="badge bg-green">' . count($newComplaints) . '</span>
                            </a>'
                    . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                <div class="scroll-cont" data-simplebar>';
                foreach ((array) $newComplaints as $key => $cmpl) {
                    $returnHtml .= '<li class="drp-mnu">
                   <span class="message">
                       <a href = ' . site_url('complaint') . '>New Complaint Registartion (CMPL' . $cmpl['id'] . ') from ' . $cmpl['contact_email'] . '</a>
                   </span>
                 </li>';
                }
                $returnHtml .= '</div></ul></li>';
            }
            //New PI complaint registartion

            $picomplaints = $this->db->select($this->tbl_property_infringement . '.id, ' . $this->tbl_property_infringement . '.complainant_email,' . $this->tbl_users . '.usr_first_name,' . $this->tbl_users . '.usr_last_name')
                ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_property_infringement . '.user_id', 'LEFT')
                ->where($this->tbl_property_infringement . '.seen', 0)
                ->get($this->tbl_property_infringement)->result_array();

            if (!empty($picomplaints)) {
                $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi"  data-toggle="tooltip" data-placement="bottom" title="Property Infringement Complaints">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                 <i class="fa fa-list"></i>
                                 <span class="badge bg-green">' . count($picomplaints) . '</span>
                            </a>'
                    . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                <div class="scroll-cont" data-simplebar>';
                foreach ((array) $picomplaints as $key => $cmpl) {
                    $returnHtml .= '<li class="drp-mnu">
                   <span class="message">
                       <a href = ' . site_url('complaint/infringement_complaints') . '>New Complaint Registartion (CMPLPI' . $cmpl['id'] . ') from ' . $cmpl['complainant_email'] . '</a>
                   </span>
                 </li>';
                }
                $returnHtml .= '</div></ul></li>';
            }
            //New contact enquiry
            $newContacts = $this->db->where('c_seen', 0)->get($this->tbl_contacts)->result_array();
            if (!empty($newContacts)) {
                $returnHtml .= ' <li role="presentation" class="dropdown ntfySupRegi"  data-toggle="tooltip" data-placement="bottom" title="Contact Enquiry">'
                . '<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                 <i class="fa fa-phone"></i>
                                 <span class="badge bg-green">' . count($newContacts) . '</span>
                            </a>'
                    . '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                <div class="scroll-cont" data-simplebar>';
                foreach ((array) $newContacts as $key => $cntct) {
                    $returnHtml .= '<li class="drp-mnu">
                   <span class="message">
                       <a href = ' . site_url('contact/enquiry') . '>New Contact Enquiry ' . $cntct['c_email'] . '</a>
                   </span>
                 </li>';
                }
                $returnHtml .= '</div></ul></li>';
            }

        }
        return $returnHtml;
    }

    public function newsletterSub($email)
    {
        if ($email) {
            $alreadySub = $this->db->get_where($this->tbl_newsletter_subscribe, array('nls_email' => $email))->row();
            if (empty($alreadySub)) {
                $this->db->insert($this->tbl_newsletter_subscribe, array('nls_email' => $email,
                    'nls_ip' => $_SERVER['REMOTE_ADDR'],
                    'nls_if_logged' => $this->uid)
                );
                return true;
            }
            return false;
        }
    }

    public function toggler($table, $updateField, $pkField, $pkId)
    {
        $pkId = encryptor($pkId, 'D');
        $updateValue = $this->db->select($updateField)->get_where(TABLE_PREFIX . $table, array($pkField => $pkId))->row()->{$updateField};
        $updateValue = ($updateValue == 0) ? 1 : 0;
        $this->db->where($pkField, $pkId);
        $this->db->update(TABLE_PREFIX . $table, array($updateField => $updateValue));
        return true;
    }

    public function getBuyingConfig($type)
    {
        if ($type == 'mar') {
            return $this->db->select('mar_name,mar_id')->get(TABLE_PREFIX . 'market_places')->result_array();
        } else {
            return $this->db->select('cat_title,cat_id')->where('cat_parent', 0)->get(TABLE_PREFIX . 'category')->result_array();
        }
    }

    public function sendPushNotification($data, $to, $options)
    {
        // Insert your Secret API Key here
        $apiKey = PUSHY_SECRET_API_KEY;

        // Default post data to provided options or empty array
        $post = $options ?: array();

        // Set notification payload and recipients
        $post['to'] = $to;
        $post['data'] = $data;
        if($data['msg_type'] == 'chat'){
            $notification['body'] = $data['content'];
            $notification['title'] = $data['from_name'];
            $notification['badge'] = 1;
            $notification['body'] = 'ping.aiff';
            $post['notification'] = $notification;
        }
        if($data['msg_type'] == 'rfq_chat_notfication'){
            $notification['body'] = $data['content'];
            $notification['title'] = $data['from_name'];
            $notification['badge'] = 1;
            $notification['body'] = 'ping.aiff';
            $post['notification'] = $notification;
        }
        if($data['msg_type'] == 'general_notfication'){
            $notification['body'] = $data['title'];
            $notification['title'] = $data['from'];
            $notification['badge'] = 1;
            $notification['body'] = 'ping.aiff';
            $post['notification'] = $notification;
        }
        if($data['msg_type'] == 'rfq_reply_notfication'){
            $notification['body'] = $data['suplier']." send a quote for your RFQ";
            $notification['title'] = 'New quote received!';
            $notification['badge'] = 1;
            $notification['body'] = 'ping.aiff';
            $post['notification'] = $notification;
        }
        if($data['msg_type'] == 'prod_inqry_reply_notfication'){
            $notification['body'] = $data['title'];
            $notification['title'] = $data['from'];
            $notification['badge'] = 1;
            $notification['body'] = 'ping.aiff';
            $post['notification'] = $notification;
        }
        if($data['msg_type'] == 'feed'){
            $notification['body'] = $data['from_name']." has added a feed first time for a while.";
            $notification['title'] = 'You have new feeds';
            $notification['badge'] = 1;
            $notification['body'] = 'ping.aiff';
            $post['notification'] = $notification;
        }

        // Set Content-Type header since we're sending JSON
        $headers = array(
            'Content-Type: application/json',
        );

        // Initialize curl handle
        $ch = curl_init();

        // Set URL to Pushy endpoint
        curl_setopt($ch, CURLOPT_URL, 'https://api.pushy.me/push?api_key=' . $apiKey);

        // Set request method to POST
        curl_setopt($ch, CURLOPT_POST, true);

        // Set our custom headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Get the response back as string instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // debug(json_encode($post, JSON_UNESCAPED_UNICODE));
        // Set post data as JSON
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, JSON_UNESCAPED_UNICODE));

        // Actually send the push
        $result = curl_exec($ch);

        // Display errors
        if (curl_errno($ch)) {
            echo curl_error($ch);
            die();
        }

        // Close curl handle
        curl_close($ch);
        // debug($result);
        return $result;
        // Debug API response
        //echo $result;
    }

    public function getSupplierUser($supId)
    {
        return $this->db->select($this->tbl_users . '.*')
            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
            ->where(array($this->tbl_users_groups . '.group_id' => 2, $this->tbl_users . '.usr_supplier' => $supId))
            ->get($this->tbl_users)->row_array();
    }

    public function getSuppliersFollowersAndFavs($supId = 0)
    {
        $supId = empty($supId) ? $this->suplr : $supId;
        return $this->db->query("SELECT fav_foll.*, " . $this->tbl_users . ".* FROM
                    (SELECT sfol_followed_by AS user_id FROM " . $this->tbl_supplier_followers . " where sfol_supplier = $supId union
                    SELECT fav_added_by AS user_id FROM " . $this->tbl_favorite_list . " where fav_consign = 'SUP' AND fav_consign_id = $supId) AS fav_foll
                    left join " . $this->tbl_users . " ON " . $this->tbl_users . ".usr_id = fav_foll.user_id WHERE " . $this->tbl_users . '.usr_id !=1')->result_array();
    }

    public function sendPushNotificationForSupplier($data, $to, $options)
    {
        // Insert your Secret API Key here
        $apiKey = PUSHY_SUP_SECRET_API_KEY;

        // Default post data to provided options or empty array
        $post = $options ?: array();

        // Set notification payload and recipients
        $post['to'] = $to;
        $post['data'] = $data;

        // Set Content-Type header since we're sending JSON
        $headers = array(
            'Content-Type: application/json',
        );

        // Initialize curl handle
        $ch = curl_init();

        // Set URL to Pushy endpoint
        curl_setopt($ch, CURLOPT_URL, 'https://api.pushy.me/push?api_key=' . $apiKey);

        // Set request method to POST
        curl_setopt($ch, CURLOPT_POST, true);

        // Set our custom headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Get the response back as string instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // debug(json_encode($post, JSON_UNESCAPED_UNICODE));
        // Set post data as JSON
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, JSON_UNESCAPED_UNICODE));

        // Actually send the push
        $result = curl_exec($ch);

        // Display errors
        if (curl_errno($ch)) {
            echo curl_error($ch);
            die();
        }

        // Close curl handle
        curl_close($ch);
        // debug($result);
        return $result;
        // Debug API response
        //echo $result;
    }
    public function checkIsSupplier($user_id)
    {
        return $this->db->where('user_id', $user_id)->where('group_id', 2)
            ->get($this->tbl_users_groups)->num_rows();
    }

    public function checkIsBuyer($user_id)
    {
        return $this->db->where('user_id', $user_id)->where('group_id', 4)
            ->get($this->tbl_users_groups)->num_rows();

    }

    public function isFirstChat($user1, $user2)
    {
        $count = $this->db->where("(c_from_id = $user1 AND c_to_id = $user2)OR (c_from_id = $user2 AND c_to_id = $user1)")->get(TABLE_PREFIX . 'chats')->num_rows();

        if ($count > 1) {
            return false;
        } else {
            return true;
        }

    }
    public function getVal($col, $table, $whrFieldName, $whrCond)
    {
        $this->db->where($whrFieldName, $whrCond);
        return $this->db->select($col)->get(TABLE_PREFIX.$table)->row()->$col;

    }

}
