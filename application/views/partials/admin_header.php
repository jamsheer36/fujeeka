<?php
  if ($this->uid) {
       $userName = '';
       if (get_logged_user('usr_username'))
            $userName = get_logged_user('usr_username');
       else
            $userName = get_logged_user('usr_first_name');
       //empty(get_logged_user('usr_username')) ? get_logged_user('usr_username') : get_logged_user('usr_first_name');
       ?> 
       <!--Note: Prior to PHP 5.5, empty() only supports variables;-->
       <div class="col-md-3 left_col">
            <div class="left_col scroll-view" style="max-width:100%">
                 <div class="navbar nav_title" style="border: 0;">
                      <div class="profile clearfix">
                           <div class="profile_pic">
                                <?php
                                echo img(array('src' => 'assets/uploads/avatar/' . $this->session->userdata('usr_avatar'),
                                    'class' => 'img-circle profile_img'));
                                ?>
                           </div>
                           <div class="profile_info">
                                <span>Welcome,</span>
                                <h2 style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis"><?php echo $userName;?></h2>
                           </div>
                      </div>

                 </div>

                 <div class="clearfix"></div>

                 <!-- menu profile quick info -->

                 <!-- <?php
                 if (!is_root_user() && privilege_exists('SP')) {

                      $suppId = get_logged_user('usr_supplier');
                      $supplierId = !empty($suppId) ? '/' . encryptor($suppId) : '';

                      $supplierDetails = $this->common_model->getSupplier($suppId);
                      $suppIsActive = (isset($supplierDetails['supm_status']) && !empty($supplierDetails['supm_status'])) ? true : false;
                      $href = ($suppIsActive) ? site_url('user/changeUserPermissions') : site_url('supplier/activate-supplier' . $supplierId);
                      if ($suppIsActive) {
                           ?>
                                                                                                                                                                                           <div style="text-align: center;padding-top: 10px;">
                                                                                                                                                                                                <div class="btn-group">
                                                                                                                                                                                                     <button onclick="location.href = '<?php echo site_url('user/changeUserPermissions/BY');?>'" class="btn btn-round <?php if ($this->usr_grp == 'BY') {?> btn-success <?php }?> " type="button"> Buyer</button>
                                                                                                                                                                                                     <button onclick="location.href = '<?php echo site_url('supplier/activate-supplier' . $supplierId);?>'" class="btn btn-round <?php if ($this->usr_grp == 'SP') {?> btn-success <?php }?> " type="button"> Supplier</button>
                                                                                                                                                                                                </div>
                                                                                                                                                                                           </div>
                           <?php
                      }
                 }
                 ?> -->
                 <!-- /menu profile quick info -->

                 <br />

                 <!-- sidebar menu -->
                 <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                      <div class="menu_section">
                           <ul class="nav side-menu">

                                <!--<?php if ($this->usr_grp == 'BY' && !privilege_exists('SP')) {?>-->
                                     <!--     <li>-->
                                     <!--          <a href="<?php echo site_url('user/become-seller') . '/' . encryptor($this->uid);?>" style="color:#00b904">-->
                                     <!--               <i class="fa fa-cny"></i> Become a Seller -->
                                     <!--          </a>-->
                                     <!--     </li>-->
                                     <!--<?php }?>-->
                                <li><a href="<?php echo site_url('dashboard');?>"><i class="fa fa-home"></i> Dashboard </a>
                                     <!--<ul class="nav child_menu">-->
                                     <!--     <li><a href="<?php echo site_url('dashboard');?>">Dashboard</a></li>-->
                                     <!--</ul>-->
                                </li>
                                <!--<?php if (is_root_user()) {?>-->
                                     <!--<li>-->
                                     <!--     <a href="<?php echo site_url('user/seller_requests');?>">-->
                                     <!--          <i class="fa fa-user" aria-hidden="true"></i> Supplier Requests -->
                                     <!--     </a>-->
                                     <!--</li>-->
                                <!--<?php }?>-->
                                <?php
                                 if (check_permission('supplier', 'index') ||
                                        check_permission('supplier', 'add') ||
                                        check_permission('supplier', 'view')) {
                                     ?>
                                     <li><a><i class="fa fa-industry"></i> Supplier <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php
                                               if (check_permission('supplier', 'index') ||
                                                       check_permission('supplier', 'view')) {
                                                    ?>
                                                    <li><a href="<?php echo site_url('supplier');?>">List</a></li>
                                               <?php } if (check_permission('supplier', 'add')) {?>
                                                    <li><a href="<?php echo site_url('supplier/add');?>">New Supplier</a></li>
                                               <?php } if (check_permission('supplier_grade', 'index')) {?>
                                                    <li><a href="<?php echo site_url('supplier_grade');?>">Supplier Grade</a></li>
                                               <?php } if (check_permission('supplier_privilege', 'index')) {?>
                                                    <li><a href="<?php echo site_url('supplier_privilege');?>">Supplier Privilege</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } 


                                if (check_permission('emp_details', 'index') ||
                                        check_permission('emp_details', 'add') ||
                                        check_permission('user_permission', '') ||
                                        check_permission('emp_details', 'view')) {
                                     ?>
                                     <li><a><i class="fa fa-users"></i> Staff Details <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php
                                               if (check_permission('emp_details', 'index') ||
                                                       check_permission('emp_details', 'view')) {
                                                    ?>
                                                    <li><a href="<?php echo site_url('emp_details');?>">Staff List</a></li>
                                               <?php } if (check_permission('emp_details', 'add')) {?>
                                                    <li><a href="<?php echo site_url('emp_details/add');?>">New Appointment</a></li>
                                               <?php } if (check_permission('user_permission', 'index')) {?>
                                                    <li><a href="<?php echo site_url('user_permission');?>">Staff Permission</a></li>
                                               <?php } if ($this->usr_grp == "SP") {?>
                                                    <li><a href="<?php echo site_url('emp_details/migrate');?>">Migrate Staff Data</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } if (check_permission('country', 'index') ||
                                        check_permission('country', 'add') ||
                                        check_permission('country', 'view')) {
                                     ?>
                                     <li><a><i class="fa fa-globe"></i> Country <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php
                                               if (check_permission('country', 'index') ||
                                                       check_permission('country', 'view')) {
                                                    ?>
                                                    <li><a href="<?php echo site_url('country');?>">List</a></li>
                                               <?php } if (check_permission('country', 'add')) {?>
                                                    <li><a href="<?php echo site_url('country/add');?>">New Country</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } if (check_permission('states', 'index') ||
                                        check_permission('states', 'add') ||
                                        check_permission('states', 'view')) {
                                     ?>
                                     <li><a><i class="fa fa-flag"></i> States <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php
                                               if (check_permission('states', 'index') ||
                                                       check_permission('states', 'view')) {
                                                    ?>
                                                    <li><a href="<?php echo site_url('states');?>">List</a></li>
                                               <?php } if (check_permission('states', 'add')) {?>
                                                    <li><a href="<?php echo site_url('states/add');?>">New States</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } if (check_permission('category', 'index') ||
                                        check_permission('category', 'add') ||
                                        check_permission('category', 'view') ||
                                        check_permission('market', 'add') ||
                                        check_permission('market', 'registrations')) {
                                     ?>
                                     <li><a><i class="fa fa-sitemap"></i> Market Places <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php
                                               if (check_permission('category', 'index') ||
                                                       check_permission('category', 'view')) {
                                                    ?>
                                                    <li><a href="<?php echo site_url('category');?>">Categories</a></li>
                                               <?php } if (check_permission('category', 'add')) {?>
                                                    <li><a href="<?php echo site_url('category/add');?>">New Category</a></li>
                                                    <?php
                                               } if (check_permission('market', 'index') ||
                                                       check_permission('market', 'view')) {
                                                    ?>
                                                    <li><a href="<?php echo site_url('market');?>">Market</a></li>
                                               <?php } if (check_permission('market', 'add')) {?>
                                                    <li><a href="<?php echo site_url('market/add');?>">New Market</a></li>
                                               <?php } if (check_permission('market', 'registrations')) {?>
                                                    <li><a href="<?php echo site_url('market/registrations');?>">New Registrations</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } if (check_permission('building', 'index') ||
                                        check_permission('building', 'add') ||
                                        check_permission('building', 'view')) {
                                     ?>
                                     <li><a><i class="fa fa-building"></i> Building <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php
                                               if (check_permission('building', 'index') ||
                                                       check_permission('building', 'view')) {
                                                    ?>
                                                    <li><a href="<?php echo site_url('building');?>">Buildings</a></li>
                                               <?php } if (check_permission('building', 'add')) {?>
                                                    <li><a href="<?php echo site_url('building/add');?>">New building</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } if (check_permission('product', 'index') ||
                                        check_permission('product', 'add') ||
                                        check_permission('product', 'view') ||
                                        check_permission('units', 'index') ||
                                        check_permission('units', 'add') ||
                                        check_permission('units', 'view')) {
                                     ?>
                                     <li><a><i class="fa fa-shopping-bag"></i> Products <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php
                                               if (check_permission('product', 'index') ||
                                                       check_permission('product', 'view')) {
                                                    ?>
                                                    <li><a href="<?php echo site_url('product');?>">List</a></li>
                                               <?php } if (check_permission('product', 'add')) {?>
                                                    <li><a <?php if (!get_logged_user('usr_is_mail_verified')) {?> href="javascript:;" class="nav-link" data-toggle="modal"  data-target="#verif-mail" <?php } else {?> href="<?php echo site_url('product/add');?>" <?php }?>>New Product</a></li>
                                               <?php } if (check_permission('units', 'index')) {?>
                                                    <li><a href="<?php echo site_url('units');?>">Units</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } if (check_permission('buyer', 'index') ||
                                        check_permission('buyer', 'view')) {
                                     ?>
                                     <li><a><i class="fa fa-shopping-cart"></i> Buyer <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php
                                               if (check_permission('buyer', 'index') ||
                                                       check_permission('buyer', 'view')) {
                                                    ?>
                                                    <li><a href="<?php echo site_url('buyer');?>">List</a></li>

                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } if (check_permission('order', 'index')) {
                                     ?>
                                     <li><a><i class="fa fa-bar-chart"></i> Inquiry <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php
                                               if (check_permission('order', 'index')) {
                                                    ?>
                                                    <li><a href="<?php echo site_url('order');?>">List</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } if (check_permission('product', 'rfq_list') || check_permission('product', 'pending_rfq_list')) {
                                     ?>
                                     <li><a><i class="fa fa-calculator"></i> RFQ <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php if (check_permission('product', 'rfq_list')) {?>
                                                    <li><a <?php if (!get_logged_user('usr_is_mail_verified')) {?> href="javascript:;" class="nav-link" data-toggle="modal"  data-target="#verif-mail" <?php } else {?> href="<?php echo site_url('product/rfq-list');?>" <?php }?>>List</a></li>
                                               <?php } if (privilege_exists('BY')) {?>
                                                    <li><a <?php if (!get_logged_user('usr_is_mail_verified')) {?> href="javascript:;" class="nav-link" data-toggle="modal"  data-target="#verif-mail" <?php } else {?> href="<?php echo site_url('product/new-rfq');?>"<?php }?> >New</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } if (check_permission('manage_banner', 'index') ||
                                        check_permission('manage_banner', 'add') ||
                                        check_permission('manage_banner', 'view') ||
                                        check_permission('manage_banner', 'update') ||
                                        check_permission('manage_banner', 'delete')) {
                                     ?>
                                     <li>
                                          <a><i class="fa fa-picture-o"></i> Manage banner <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php if (check_permission('manage_banner', 'index')) {?>
                                                    <li><a href="<?php echo site_url('manage_banner/add');?>">New banner</a></li>
                                               <?php } if (check_permission('manage_banner', 'index')) {?>
                                                    <li><a href="<?php echo site_url('manage_banner');?>">List</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                     <?php
                                } if (check_permission('add', 'index') ||
                                        check_permission('add', 'add') ||
                                        check_permission('add', 'view') ||
                                        check_permission('add', 'update') ||
                                        check_permission('add', 'delete')) {
                                     ?>
                                     <li>
                                          <a><i class="fa fa-bullhorn"></i> Advertisement <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php if (check_permission('add', 'index')) {?>
                                                    <li><a href="<?php echo site_url('add/newAdd');?>">New Add</a></li>
                                               <?php } if (check_permission('add', 'index')) {?>
                                                    <li><a href="<?php echo site_url('add');?>">List</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                <?php } if ($this->usr_grp == 'BY') {?>

                                     <li>
                                          <a><i class="fa fa-heart"></i> Favourites <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <li><a href="<?php echo site_url('favorites/product-list');?>">Favourite Products</a></li>
                                               <li><a href="<?php echo site_url('favorites/supplier-list');?>">Favourite Suppliers</a></li>
                                          </ul>
                                     </li>
                                <?php }?>
                                <?php
                                if ($this->usr_grp == 'SP' || $this->usr_grp == 'ST') {
                                     if (check_permission('feed', 'index') || check_permission('feed', 'add_feed') || check_permission('feed', 'update')) {
                                          ?> 

                                          <li>

                                               <a><i class="fa fa-feed"></i> Feeds <span class="fa fa-chevron-down"></span></a>

                                               <ul class="nav child_menu">

                                                    <?php if (check_permission('feed', 'add_feed')) {?>

                                                         <li><a href="<?php echo site_url('feed/add-feed');?>">Add Feed</a></li>

                                                    <?php }?>     

                                                    <?php if (check_permission('feed', 'index')) {?> 

                                                         <li><a href="<?php echo site_url('feed/index');?>">List</a></li>

                                                    <?php }?> 



                                               </ul>

                                          </li>

                                          <?php
                                     }
                                } if (check_permission('notification', 'pushnotification')) {
                                     ?>
                                     <li>
                                          <a><i class="fa fa-microphone"></i> Notification <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <li><a href="<?php echo site_url('notification/pushnotification');?>">Send push notification</a></li>
                                          </ul>
                                     </li>
                                <?php } if (can_access_module('logistics')) {?>
                                     <li>
                                          <a><i class="fa fa-truck"></i> Logistics <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php if (check_permission('logistics', 'listlogistics')) {?>
                                                    <li><a href="<?php echo site_url('logistics/listlogistics');?>">Logistics</a></li>
                                               <?php } if (check_permission('logistics', 'add')) {?>
                                                    <li><a href="<?php echo site_url('logistics/add');?>">New Logistics</a></li>
                                               <?php } if (check_permission('logistics', 'services')) {?>
                                                    <li><a href="<?php echo site_url('logistics/services');?>">Services</a></li>
                                               <?php } if (check_permission('logistics', 'new_service')) {?>
                                                    <li><a href="<?php echo site_url('logistics/new_service');?>">New services</a></li>
                                               <?php } if (check_permission('logistics', 'request_quotes')) {?>
                                                    <li><a href="<?php echo site_url('logistics/request-quotes');?>">Request quotes</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                <?php } if (can_access_module('quality_control')) {?>
                                     <li>
                                          <a><i class="fa fa-check-circle"></i> QC <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <?php if (check_permission('quality_control', 'listQa')) {?>
                                                    <li><a href="<?php echo site_url('quality_control/listQa');?>">List quality control</a></li>
                                               <?php } if (check_permission('quality_control', 'add')) {?>
                                                    <li><a href="<?php echo site_url('quality_control/add');?>">New quality control</a></li>
                                               <?php } if (check_permission('quality_control', 'services')) {?>
                                                    <li><a href="<?php echo site_url('quality_control/services');?>">Services</a></li>
                                               <?php } if (check_permission('quality_control', 'new_service')) {?>
                                                    <li><a href="<?php echo site_url('quality_control/new_service');?>">New services</a></li>
                                               <?php } if (check_permission('quality_control', 'request_quotes')) {?>
                                                    <li><a href="<?php echo site_url('quality_control/request_quotes');?>">Request quotes</a></li>
                                               <?php }?>
                                          </ul>
                                     </li>
                                <?php } if ($this->usr_grp != 'ST' && $this->usr_grp != 'SBA') {?>
                                     <li>
                                          <a <?php if (!get_logged_user('usr_is_mail_verified')) {?> href="javascript:;" class="nav-link" data-toggle="modal"  data-target="#verif-mail" <?php } else {?> href="<?php echo site_url('user/chat');?>" <?php }?>>
                                               <i class="fa fa-comments"></i> Chats 
                                          </a>
                                     </li>
                                <?php }?>

                                <?php if ($this->usr_grp == 'SA') {?>

                                   <li>
                                          <a><i class="fa fa-list"></i> Complaints <span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                               <li><a href="<?php echo site_url('complaint');?>">Trade Dispute</a></li>

                                               <li><a href="<?php echo site_url('complaint/infringement_complaints');?>">Property Infringement</a></li>
                                          </ul>

                                       
                                     </li>



                                   
                                     <li>
                                          <a  href="<?php echo site_url('contact/enquiry');?>">
                                               <i class="fa fa-question-circle-o"></i> Contact Enquiry
                                          </a>
                                     </li>
                                <?php }?>
                                <li>
                                     <a <?php if (!get_logged_user('usr_is_mail_verified')) {?> href="javascript:;" class="nav-link" data-toggle="modal"  data-target="#verif-mail" <?php } else {?> href="<?php echo site_url('mailbox');?>" <?php }?> >
                                          <i class="fa fa-envelope"></i> Messages 
                                     </a>
                                </li>
                                <?php if($this->usr_grp == 'SP') { ?>
                                    <li>
                                         <a href="" class="green-txt" data-toggle="modal" data-target="#D-App">
                                              <i class="fa fa-download"></i> Download App 
                                         </a>
                                    </li>
                                <?php } ?>
                                
                                <li>
                                     <a href="<?php echo site_url();?>" class="orange-txt">
                                          <i class="fa fa-external-link"></i> Go to Website 
                                     </a>
                                </li>
                           </ul>
                      </div>
                 </div>
                 <!-- /sidebar menu -->


                 <!-- /menu footer buttons -->

                 <div class="sidebar-footer hidden-small">
                      <a href="<?php echo site_url('user/change-password');?>" data-toggle="tooltip" data-placement="top" title="Change password">
                           <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
                      </a>

                      <a data-toggle="tooltip" data-placement="top" title="<?php echo is_root_user() ? 'Settings' : 'My profile';?>" 
                         href="<?php
                         echo is_root_user() ? site_url('settings/general_settings') :
                                 site_url('emp-details/update-profile/' . encryptor($this->uid));
                         ?>">
                           <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                      </a>

                      <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo site_url('user/logout');?>">
                           <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                      </a>
                 </div>

                 <!-- /menu footer buttons -->
            </div>
       </div>

       <!-- top navigation -->
       <div class="top_nav">
            <div class="nav_menu">
                 <nav>
                      <div class="nav toggle" style="width: 50%;float: left;">
                           <a id="menu_toggle" style="padding: 0px 15px 0;float: left;"><i class="fa fa-bars"></i></a>
                           <!-- title -->
                           <div class="f-heading" style="width: 100%;">
                                <h1>Fujeeka <span>Admin Panel</span></h1>
                           </div>
                           <!-- -->
                      </div>

                      <ul class="nav navbar-nav navbar-right" style="width: auto;">
                           <li class="liMyAccount">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                     My Account
                                     <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                     <?php if ($this->usr_grp == 'SP') {?>
                                          <li><a href="<?php echo site_url('supplier/myprofile');?>"> Profile</a></li>
                                     <?php } else {?>
                                          <li><a href="<?php echo site_url('emp-details/update-profile/' . encryptor($this->uid));?>"> Profile</a></li>
                                     <?php }?>
                                     <li><a href="<?php echo site_url('user/change-password');?>"> Change Password</a></li>
                                     <?php if (is_root_user()) {?>
                                          <li>
                                               <a href="<?php echo site_url('settings/general_settings');?>">
                                                    <span>Settings</span>
                                               </a>
                                          </li>
                                     <?php }?>
                                     <li><a href="<?php echo site_url('user/logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                           </li>
                      </ul>
                      <ul class="nav navbar-nav navbar-right ulNotifications" style="width: auto;float: left;">

                      </ul>
                 </nav>
            </div>
       </div>
  <?php }?>
<script src="../vendors/jquery/dist/jquery.min.js"></script>

 <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
    });
</script>

<style>
     .blink_me {
          animation: blinker 1s linear infinite;
     }

     @keyframes blinker {
          50% {
               opacity: 0;
          }
     }        
</style>   

<script>
     $(document).ready(function () {
          checkRfq();
          var interval = 20000;
          setInterval(checkRfq, interval);
     });
     function checkRfq() {
          $.ajax({
               url: "<?php echo site_url();?>" + "general/getNotifications",
               method: "GET",
               dataType: "json",
               success: function (resp) {
                    $('.ulNotifications').html(resp);
               }
          });
     }
</script>

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
</script>
<!-- Autocomplete -->
<script type="text/javascript" src="../vendors/devbridge-autocomplete/jquery.autocomplete.min.js"></script>
<!-- General -->
<script src="js/my.script.min.js"></script>