
<!-- footer content -->
<!-- mail modal -->
<div class="modal verif-mail" id="verif-mail" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Verify Email</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
          <form class="mail-form" data-parsely-validate="true">
            Hi <?=$this->session->userdata('usr_fullname');?>,<br> Please verify your email 
            <input id="mail_inp" type="email" autocomplete="off" required="" data-parsley-required-message="" data-parsley-type-message="Invalid email" data-parsley-errors-container="#par_error" name="new_mail" class="hidden" placeholder="Enter new email here">
            <span id="ver_mail_span" class="font-bold">(<?=$this->session->userdata('usr_email');?>) to activate all the features.</span>
            <a href="javascript:;" class="show_mail_inp green-txt">Change email</a>
            <br>
            <span id="par_error"></span>
            <div class="al-btns" style="text-align: center;">
              <a class="btn btn-success btn-green ver-email mx-auto mt-4">Resend Email</a>
            </div>
            
          </form> 
          </div>


        </div>
      </div>
    </div>

<!-- inquiry modal -->
<footer>
     <div class="follow-alert alert alert-success alert-dismissable success_msgBox1" style="display: none;">
          <button type="button" class="close" onclick="$(this).parent('div').hide();">×</button>
          <span class="success_msg1"></span>
     </div>
     <div class="follow-alert alert alert-danger alert-dismissable fail_msgBox1" style="display: none;">
          <button type="button" class="close" onclick="$(this).parent('div').hide();">×</button>
          <span class="fail_msg1"></span>
     </div>
     <input type="hidden" id="site_url" value="<?php echo base_url();?>">
     <div class="ajax_loader hidden"></div>
     <div class="pull-right">
          Fujeeka portal 1.0
     </div>
     <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>
<!-- Scrollbar -->
  <link rel="stylesheet" href="css/scrollbar.css" />
  <script src="js/scrollbar.js"></script>
  <script type="text/javascript">
     var objDiv = $(".scroll-box");
     var h = objDiv.get(0).scrollHeight;
     objDiv.animate({scrollTop: h});
  </script>
<!-- redactor editor -->
<link rel="stylesheet" href="../vendors/redactor/css/redactor.css">
<!--<script src="../vendors/redactor/js/ga.js" async="" type="text/javascript"></script>-->
<!--<script src="../vendors/redactor/js/combined.js"></script>-->
<script src="../vendors/redactor/js/redactor.js"></script>
<script src="../vendors/redactor/js/tabler.js"></script>
<script src="../vendors/redactor/js/video.js"></script>
<!-- redactor editor -->

<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="../vendors/Flot/jquery.flot.js"></script>
<script src="../vendors/Flot/jquery.flot.pie.js"></script>
<script src="../vendors/Flot/jquery.flot.time.js"></script>
<script src="../vendors/Flot/jquery.flot.stack.js"></script>
<script src="../vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- searched dropdown -->
<script src="../vendors/bootstrap-select/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../vendors/bootstrap-select/js/ajax-bootstrap-select.js"></script>

<!-- Custom Theme Scripts -->
<script src="../assets/js/custom.min.js"></script>
<script src="../vendors/switchery/dist/switchery.min.js"></script>

<!-- jQuery Smart Wizard -->
<script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<!-- bootstrap-datetimepicker -->
<link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<!-- bootstrap-datetimepicker -->    
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Jquery form validation -->
<script src="js/jquery.validate.js"></script>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<!--  -->
<script type="text/javascript" src="../vendors/bootstrap-multiselect/js/prettify.min.js"></script>
<link type="text/css" rel="stylesheet" href="../vendors/bootstrap-multiselect/css/bootstrap-multiselect.css">
<script type="text/javascript" src="../vendors/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
<!-- Input mask -->
<script src="js/jquery.mask.js"></script>

<!-- crop -->
<link href="../vendors/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
<script src="../vendors/jcrop/js/jquery.Jcrop.min.js"></script>
<script src="../vendors/jcrop/js/jcrop.js"></script>
<!-- crop -->

<!-- Number format -->
<script src="js/jquery.number.min.js"></script>
<script src="js/parsley.min.js"></script>
<script src="https://js.pusher.com/4.3/pusher.min.js"></script>
<!-- <script src="js/pusher.js"></script> -->

<!-- Sumo select -->
<script src="../vendors/sumoselect/js/jquery.sumoselect.js"></script>
<link href="../vendors/sumoselect/css/sumoselect.css" rel="stylesheet" />

<script>
               var url_method= "<?=$this->router->fetch_method()?>";
               var pusher = new Pusher('853be53b86c43083fe5c', {
                    cluster: 'ap2',
                    forceTLS: true
               });
               var ch_name = "<?php echo encryptor($this->uid);?>";
               var channel = pusher.subscribe(ch_name);
               channel.bind('pusher:subscription_error', function (status) {
                    console.log("error: " + status);
               });
               // channel.bind('pusher:subscription_succeeded', function () {
               //      console.log("success: connected");
               // });
               channel.bind('chat-event', function (data) {
                    var audio = new Audio('http://halal86.com/fujeeka_web/assets/notification.mp3');
                    audio.play();
                    $('.success_msgBox1').show();
                    $(".success_msg1").html("You have a new message from " + data.from + "<br> <a style='color:#fea317' href='<?php echo site_url('user/chat/' . encryptor($this->uid))?>' >Cick to view<a>");
                    $('.success_msgBox1').delay(9000).fadeOut('slow');
               });
               channel.bind('rfq-event', function (data) {
                    if(!$("#messages").hasClass('active') || url_method =='order_summery'){
                         var audio = new Audio('http://halal86.com/fujeeka_web/assets/notification.mp3');
                         audio.play();
                         $('.success_msgBox1').show();
                         $(".success_msg1").html("You have a new rfq chat from " + data.from + "<br> <a style='color:#fea317' href='"+data.rfq_link+"' >Cick to view<a>");
                         $('.success_msgBox1').delay(9000).fadeOut('slow');
                    }   
               });
               channel.bind('inquiry-event', function (data) {
                    if(!$("#messages").hasClass('active') || url_method =='rfq_list'){
                         var audio = new Audio('http://halal86.com/fujeeka_web/assets/notification.mp3');
                         audio.play();
                         $('.success_msgBox1').show();
                         $(".success_msg1").html("You have a new inquiry reply from " + data.from + "<br> <a style='color:#fea317' href='"+data.inq_link+"' >Cick to view<a>");
                         $('.success_msgBox1').delay(9000).fadeOut('slow');
                    }   
               });
               
</script>

<script>
     $(document).ready(function () {
          window.ParsleyValidator
                  .addValidator('fileextension', function (value, requirement) {
                       var fileExtension = value.split('.').pop();
                       extns = requirement.split(',');
                       if (extns.indexOf(fileExtension) == -1) {
                            return fileExtension === requirement;
                       }

                  }, 32)
                  .addMessage('en', 'fileextension', 'Only files with type jpg,png,gif,jpeg are supported');
                  $('#frmSupplier').parsley().validate();
     });
</script>
<style>
     .ajax_loader {
          position: fixed;
          left: 0px;
          top: 0px;
          width: 100%;
          height: 100%;
          z-index: 9999;
          background: url("images/loading.gif") 50% 50% no-repeat #f9f9f9;
     }
</style>
<script>
$(".btnNextTab").click(function() {
    $("html, body").animate({ scrollTop: 0 }, "fast");
});
$(".btnPrevTab").click(function() {
    $("html, body").animate({ scrollTop: 0 }, "fast");
});
     //Auto close alert box
          window.setTimeout(function () {
            // dont change this, change will break chat notification.....tested
            $(".alert").css('display','none');
          }, 5000);
</script>
<?php if (!get_logged_user('usr_is_mail_verified')) {?>

<script src="js/verify_mail_admin.js"></script>
<style>
     .mail-form ul.parsley-errors-list {
          list-style: none;
          color: #E74C3C;
          padding-left: 3px;
          display:inline-block;
     }
</style>

    <script type="text/javascript">
     var objDiv = $(".scroll-box");
     var h = objDiv.get(0).scrollHeight;
     objDiv.animate({scrollTop: h});
  </script>
<?php }?>
<?php if($this->router->fetch_method()=="rfq_list" && !is_root_user()){ ?>
     <script>
          var time= "<?=date('h:i A') . ' | ' . date('M d')?>";
     </script>
     <script src="js/rfq_chat.js"></script>
<?php } ?>
<?php if($this->router->fetch_method()=="order_summery" && !is_root_user()){ ?>
     <script>
          var time= "<?=date('h:i A') . ' | ' . date('M d')?>";
     </script>
     <script src="js/inquiry_chat.js"></script>
<?php } ?>