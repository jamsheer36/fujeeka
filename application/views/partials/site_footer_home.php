
<footer>
        <div class="container">
            <div class="links-container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Registration</h5>
                                <ul>
                                   <li><a href="<?php echo site_url('user/signup');?>">Buyer</a></li>
                                   <li><a href="<?php echo site_url('supplier/supplier-register');?>">Supplier</a></li>
                                   <li><a href="<?php echo site_url('market/market-register');?>">Building</a></li>
                                   <li><a href="<?php echo site_url('logistics/register');?>">Logistics</a></li>
                                   <li><a href="<?= site_url('quality-control/register')?>">Quality Control</a></li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <h5>Info</h5>
                                <ul>
                                   <li><a href="<?=site_url('about-us')?>">About Fujeeka</a></li>
                                    <li><a href="<?php echo site_url('logistics');?>">Logistics</a></li>
                                    <li><a href="<?php echo site_url('quality-control');?>">Quality Control</a></li>
                                    <li><a href="<?=site_url('contact-us')?>">Contact Us</a></li>
                                    <li><a href="javascript:;">Sitemap</a></li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <h5>LEGAL</h5>
                                <ul>
                                   <li><a href="<?=site_url('terms-of-use');?>">Terms of Use</a></li>
                                   <li><a href="<?= site_url('privacy-policies'); ?>">Privacy Policy</a></li>
                                   <li><a href="<?=site_url('product-listing-policies');?>">Product Listing Policy</a></li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <h5>Help</h5>
                                <ul>
                                    <li><a href="javascript:;">Help Center</a></li>
                                    <li><a href="<?=site_url('complaint_register')?>">Submit Complaint</a> </li>
                                    <li><a href="<?=site_url('faq')?>">FAQ</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="f-rfq">
                            <h5>Post your buying request</h5>
                            <?php echo form_open_multipart(site_url('product/rfq'), array('class' => "form-signin", 'data-parsley-validate' => "true"))?>
                                <div class="form-group">
                                    <select class="form-control" id="sell1" name="rfq_category" required data-parsley-required-message="Select a category">
                                         <option value="">Select Category</option>
                                         <?php foreach(get_buying_config('cat') as $cat){ ?>
                                             <option value="<?=$cat['cat_id']?>"><?=$cat['cat_title']?></option>
                                         <?php } ?>    
                                    </select>
                                </div>

                                <div class="form-group">
                                    <select class="form-control" id="markets" name="markets">
                                        <option value="">Select Market</option>
                                        <?php foreach(get_buying_config('mar') as $mar){ ?>
                                             <option value="<?=$mar['mar_id']?>"><?=$mar['mar_name']?></option>
                                        <?php } ?>      
                                   </select>
                                </div>

                                <div class="form-group">
                                    <input type="text" required data-parsley-required-message="Enter product name" class="form-control commonAutoComplete" id="product" placeholder="Keywords of products you are looking for" name="pname" data-url="<?php echo site_url('product/productAutoName');?>">
                                    <input type="hidden" id="prd_id" value="0" name="rfq_product">
                                    <input type="hidden"  class="unit_sel"  name="unit">

                                </div>

                                <button class="btn btn-success btn-green" value="footer" name="from_footer" type="submit">Get Quote</button>
                            <?php echo form_close()?>
                        </div>
                    </div>

                    <div class="col-md-4">
                    
                        <!-- Subscribe -->
                        <div class="subscribe-section">
                              <h5>Subscribe Newsletter</h5>
                              <form class="form-inline frmNewsletterSub" data-url="<?php echo site_url('general/newsletterSub'); ?>">
                                   <div class="form-group mr-2 mb-2">
                                        <label for="inputPassword2" class="sr-only">Password</label>
                                        <input type="email" class="form-control txtNewsSubEmail" id="subs-email" placeholder="Enter your email ">
                                   </div>
                                   <button type="submit" class="btn btn-success btn-green mb-2">Subscribe</button>
                                   <p>Subscribe newsletter to get notifications</p>
                              </form>
                              <p class="newsLtrMsg" style="color: red;font-size: 10px;"></p>
                         </div>
                        <!-- /Subscribe -->

                        <div class="gapp">
                            <h5>Get App</h5>
                            <div class="app-qr">
                                <a href="<?=site_url('home/get_app')?>"><img src="images/app-store.png" alt="scan code to install ios app">
                                <img src="images/playstore.png" alt="scan code to install android app"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-base">
                <div class="row">
                    <!-- <div class="col-md-4 download-app">
              <img src="assets/images/app-store.png" alt="Download from Appstore" class="img-fluid">
              <img src="assets/images/playstore.png" alt="Download from Playstore" class="img-fluid">
            </div> -->
                    <div class="col-md-6 text-left">
                        &copy; Copyright <?=date('Y')?> Fujeeka | All Rights Reserved
                    </div>
                    <div class="col-md-6">
                        <div class="sicon">
                            <div class="ficn">
                                <div class="icon-circle">
                                    <a href="javascript:;" class="ifacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>

                            <div class="ficn">
                                <div class="icon-circle">
                                    <a href="javascript:;" class="itwittter" title="Twitter"><i class="fab fa-twitter"></i></a>
                                </div>
                            </div>

                            <div class="ficn">
                                <div class="icon-circle">
                                    <a href="javascript:;" class="iInsta" title="Instagram"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </footer>

<!-- sticky-->
<?php if($this->uid!=1){?>
<div class="sticky-container">
     <ul class="sticky">
          <li>
          <?php if($this->uid) { ?>
               <a href="javascript:;"  data-sup_usr_id="<?php echo encryptor(1);?>" 
                  onclick="loadChatbox('<?php echo encryptor(1);?>', 'Admin')"
                  class="chat_with" title="" data-original-title="Chat">
                    <i class="fas fa-comments"></i>
                    <p>Chat with Fujeeka</p>
               </a>
          <?php } else{ ?>
                <a href="javascript:;" data-toggle="modal" data-target="#loginModal">
                    <i class="fas fa-comments"></i>
                    <p>Chat with Fujeeka</p>
               </a>
          <?php } ?>     
          </li>
     </ul>
</div>
<?php }?> 

<!-- Chat DIV -->
<div class="chatbox" id="chatbox">
     <span class="chat-text chat_with_name"></span>
     <iframe id="ddd" src="" scrolling="no" frameborder="0" width="280px" height="450px" style="border:0; margin:0; padding: 0;">
     </iframe>       
     <div id="close-chat" onclick="closeChatbox()">&times;</div>
     <div id="minim-chat" onclick="minimChatbox()"><span class="minim-button">&minus;</span></div>
     <div id="maxi-chat" onclick="maximChatbox()"><span class="maxi-button">&plus;</span></div>
</div>
<!-- /Chat DIV -->  

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<!-- modernizr JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<!-- Lazyload -->
  <script src="js/lazyload.js"></script>
  <script>
    $('.lazy').lazyload({
      placeholder: "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
    });
  </script>

<!-- product/supplier search -->
<script src="js/search.js"></script>
<!-- Slider JS -->
<script src="js/jquery.slide.js"></script>
<!-- shop slider -->
<script src="js/ninja-slider.js"></script>
<script src="js/thumbnail-slider.js"></script>
<!-- latest prod slider -->
<script src="js/lightslider.js"></script>
<!-- Menu js  -->
<script src="js/webslidemenu.js"></script>
<!-- CUSTOM JS -->
<script src="js/scripts.js"></script>
<script type="text/javascript" src="../vendors/devbridge-autocomplete/jquery.autocomplete.min.js"></script>
<!-- stock list scroll -->
<script type="text/javascript" src="js/jquery.marquee.js"></script>
<!--scroll to top btn -->
<script type="text/javascript" src="js/scroll-tp.js"></script>

<script>
          $(".thumb").click(function () {
              //window.location.href = "<?php echo site_url()?>supplier/home/" + $(this).attr('data-supname') + '/' + $(this).attr('data-id');
              window.location.href = "http://"+$(this).attr('data-dmn')+".<?=$this->http_host?>";
          })
          $('.btn-360').on('click', function(e) {
              e.preventDefault();
              var url = $(this).attr('href');
              $(".360_body").html('<iframe width="100%" height="560" frameborder="0" scrolling="no" allowtransparency="true" src="' + url + '"></iframe>');
              $(".modal-360").modal("show");
          });
</script>
<?php if ($this->uid) {?>
  
       <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
       <script src="js/pusher.js"></script>
       <script>
                 var pusher = new Pusher('853be53b86c43083fe5c', {
                      cluster: 'ap2',
                      forceTLS: true
                 });
                 var ch_name = "<?php echo encryptor($this->uid);?>";
                 var channel = pusher.subscribe(ch_name);
                 channel.bind('pusher:subscription_error', function (status) {
                      console.log("error: " + status);
                 });
                 channel.bind('pusher:subscription_succeeded', function () {
                      console.log("success: connected");
                 });
                 channel.bind('chat-event', function (data) {
                      var audio = new Audio('http://halal86.com/fujeeka_web/assets/notification.mp3');
                      audio.play();  
                      $('.msgBox').show();
                      $(".sus_msg").html("You have a new message from " + data.from + "<br> <a style='color:#fea317' href='<?php echo site_url('user/chat/' . encryptor($this->uid))?>' >Cick to view<a>");
                      $('.msgBox').delay(15000).fadeOut('slow');
                 });
                 
       </script>

       <!-- <a id="cht_ref" href='<?php echo site_url('user/chat/' . encryptor($this->uid))?>' > -->
            <div class="alert alert-success alert-dismissible msgBox" style="color:white;background-color:#00b904; display: none;"  >
                 <button class="close" aria-label="close" onclick="$(this).parent('div').css('display','none');">&times;</button>
                 <p class="sus_msg"></p>
            </div>
       <!-- </a> -->
  <?php }?>

