<header class="nw-head white-bg">
  <div class="htop">
    <div class="container">
      <div class="row">
        <div class="col-md-4 b-capt">The complete wholesale B2B website</div>
        <div class="col-md-8">
          <!-- Start Menu -->
          <div class="menu-container">
            <div class="wsmenucontainer clearfix">
              <div id="overlapblackbg"></div>
              <div class="wsmobileheader clearfix">
                <a id="wsnavtoggle" class="animated-arrow"><span></span></a> <!-- <a class="smallogo"><img src="assets/images/fujeeka-logo.png"  alt="" /></a> -->
              </div>
              <div class="headerfull">
                <!--Main Menu HTML Code-->
                <div class="wsmain">
                  <nav class="wsmenu clearfix">
                    <ul class="mobile-sub wsmenu-list">
                      <li>
                        <a href=<?=site_url('home/get_app')?> class="navtext"><i class="fas fa-mobile-alt green-txt"></i> Get App</a>
                      </li>
                      <li class="signin sh">
                        <a  class="navtext">
                          <?php if (!$this->uid) {?>
                          <div class="reg">
                            Sign In <span class="seperator"></span> <span class="rgs">Register</span>
                          </div>
                          <?php } else {?>
                          <div class="reg">
                            My Fujeeka
                          </div>
                          <?php }?>
                        </a>
                        <div class="megamenu clearfix halfmenu">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="signin-container">
                                <?php if (!$this->uid) {?>
                                <div class="top-section">
                                  <h4 class="heading3">Get started now!</h4>
                                  <a class="btn btn-success btn-green btn-signin" href="javascript:;"  data-toggle="modal" data-target="#loginModal">Sign In</a>
                                  <div class="w-100">
                                    or
                                  </div>
                                  <a class="btn btn-success btn-reg" href="<?php echo site_url('user/signup');?>">Buyer Sign up</a>
                                </div>
                                <?php }?>
                                <?php if ($this->uid) {?>
                                <div class="top2">
                                  <div class="img">
                                    <?php
                                      echo img(array('src' => 'assets/uploads/avatar/' . $this->session->userdata('usr_avatar'),
                                          'class' => 'img-fluid'));
                                      ?>
                                    <!-- <img src="images/avatar" alt="" class="img-fluid"> -->
                                  </div>
                                  <div class="name">
                                    <h3><?php echo $this->session->userdata('usr_username');?></h3>
                                    <p><a href="<?php echo site_url('dashboard');?>" target="_blank" rel="noopener noreferrer">View Dashboard</a></p>
                                  </div>
                                </div>
                                <div class="my-section">
                                  <div class="w-100 heading-bx">
                                    My Fujeeka
                                  </div>
                                  <ul>
                                    <li><a href="<?= site_url('product/rfq-list')?>" target="_blank" rel="noopener noreferrer">Manage RFQ</a></li>
                                    <li><a href="<?= site_url('order')?>" target="_blank" rel="noopener noreferrer">My Orders</a></li>
                                    <li><a href="<?= site_url('dashboard')?>" target="_blank" rel="noopener noreferrer">My Account</a></li>
                                    <li class="signout"><a href="<?= site_url('user/logout')?>" >Signout</a></li>
                                  </ul>
                                </div>
                                <?php }?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </nav>
                </div>
                <!--Menu HTML Code-->
              </div>
            </div>
            <!-- /menu -->
            <div class="clearfix"></div>
          </div>
          <!-- / End Menu -->
        </div>
      </div>
    </div>
  </div>
  <div class="header-top-area">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="headr-container">
            <!-- Start Logo -->
            <div class="logo-bx">
              <div class="logo">
                <a href="<?=site_url();?>">
                <img src="images/fujeeka-logo-g.png" alt="fujeeka logo">
                </a>
              </div>
            </div>
            <!-- / End Logo -->
            <div class="right-sec">
              <!-- search bar -->
              <?php echo $this->load->view('searchbar');?>
              <!--/ searchbar -->
              <!-- 360 button -->
              <button type="button" name="button" class="btn btn-orange" onclick="location.href='<?=site_url('supplier/360-market')?>'">check our 360&#176;</button>
              <!-- <a href="" class="btn-orange">Check our 360</a> -->
              <!-- /360 button -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end topbar -->
  <div class="clearfix"></div>
  <div class="ajax_loader d-none"></div>
  <?php if($this->uid && !get_logged_user('usr_is_mail_verified')){ ?>
  <!-- The Modal -->
  <div class="modal verif-mail" id="verif-mail" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Verify Email</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <form class="mail-form" data-parsely-validate="true">
            Hi <?=$this->session->userdata('usr_fullname');?>,<br> Please verify your email 
            <input id="mail_inp" type="email" autocomplete="off" required="" data-parsley-required-message="" data-parsley-type-message="Invalid email" data-parsley-errors-container="#par_error" name="new_mail" class="d-none" placeholder="Enter new email here">
            <span id="ver_mail_span" class="font-bold">(<?=$this->session->userdata('usr_email');?>) to activate all the features.</span>
            <a href="javascript:;" class="show_mail_inp green-txt">Change email</a>
            <br>
            <span id="par_error"></span>
            <div class="al-btns" style="text-align: center;">
              <a class="btn btn-success btn-green ver-email mx-auto mt-4">Resend Email</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /Activate Email -->
  <?php } ?>
</header>
<div class="modal" id="loginModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="login-m">
          <div class="left-sec">
            <h2>Login</h2>
            <p>Get access to the complete wholesale B2B website</p>
          </div>
          <div class="right-sec">
            <div class="form-container">
              <form action="" id="modal_lgn_form">
              <span style="color:green" id="reset_success"></span>
                <div class="form-group lgn_grp">
                  <input type="text" class="form-control" id="identity" placeholder="Username">
                  <span style="color:red" id="u_error"></span>
                </div>
                <div class="form-group lgn_grp">
                  <input type="password" class="form-control" id="password" placeholder="Password">
                  <span style="color:red" id="p_error"></span>
                </div>
                <div class="form-group fpswd_grp d-none">
                  <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                  <span style="color:red" id="m_error"></span>
                </div>
                <div class="form-group otp_grp d-none">
                  <span style="color:green" id="mail_success"></span>
                  <input type="text" class="form-control" id="otp" placeholder="Enter OTP" name="otp">
                  <span style="color:red" id="otp_error"></span>
                  <input type="hidden" id="resend_mail_ip">
                  <input type="hidden" id="forgot_code"> 
                </div>
                <div class="form-group change_grp d-none">
                  <input type="password" class="form-control" id="pswd" placeholder="Enter New Password" name="pswd">
                  <span style="color:red" id="pswd_error"></span>
                  <input type="password" class="form-control" id="cpswd" placeholder="Confirm New Password" name="cpswd">
                  <span style="color:red" id="cpswd_error"></span>
                  <input type="hidden" id="rest_code"> 
                </div>
                <div class="form-group mb-1 text-right">
                  <a id="a_fpwd" href="javascript:;">Forgot Password?</a>
                  <a id="a_lgn" class="d-none" href="javascript:;">Back to login</a>
                  <a id="a_resend_mail" class="d-none" href="javascript:;">Resend OTP</a>
                </div>
                <div class="clearfix"></div>
                <div>
                  <button type="button" data-sec="login" id="modal_lgn_btn" class="btn btn-success btn-login btn-inline">Login</button>
                  <div class="new-u mt-5">
                    New user? <a href="<?=site_url('user/signup')?>">Sign up</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="stop" class="scrollTop">
  <span><a href="">
  <i class="fas fa-arrow-circle-up"></i>
  </a></span>
</div>