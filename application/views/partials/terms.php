<div class="col-md-3">
                    <div class="card card-terms">
                         <h2 class="heading2">
                              POLICIES & TERMS
                         </h2>
                         <?php $method = $this->router->fetch_method();?>
                         <ul>
                             <li <?php if($method=="terms_conditions") {?>  class="active" <?php } ?>>
                                   <a href="<?=site_url('terms-conditions')?>">Terms &amp; Conditions</a>
                              </li>
                              <li <?php if($method=="index") {?>  class="active" <?php } ?>>
                                   <a href="<?=site_url('privacy-policies')?>">Privacy Policy</a>
                              </li>
                              <li <?php if($method=="listing_policy") {?>  class="active" <?php } ?>>
                                   <a href="<?=site_url('product-listing-policies')?>">Product Listing Policy</a>
                              </li>
                              <li <?php if($method=="terms_of_use") {?>  class="active" <?php } ?>>
                                   <a href="<?=site_url('terms-of-use')?>">Terms of Use</a>
                              </li>
                              <li <?php if($method=="intellectual_property") {?>  class="active" <?php } ?>>
                                   <a href="<?=site_url('intellectual-property-rights')?>">Intellectual Property Rights</a>
                              </li>
                              <li <?php if($method=="complaint_terms") {?>  class="active" <?php } ?>>
                                   <a href="<?=site_url('complaint-terms')?>">Complaint Terms</a>
                              </li>
                              <!--<li <?php if($method=="service_agreement") {?>  class="active" <?php } ?>>-->
                              <!--     <a href="<?=site_url('service-agreement')?>">Service Agreement</a>-->
                              <!--</li>-->
                              <!--<li <?php if($method=="rfq_policy") {?>  class="active" <?php } ?>>-->
                              <!--     <a href="<?=site_url('rfq-policy')?>">RFQ Policy</a>-->
                              <!--</li>-->
                              
                              <li <?php if($method=="quality_control") {?>  class="active" <?php } ?>>
                                  <a href="<?=site_url('quality-control-terms')?>">Quality Control</a>
                              </li>
                              
                              <li <?php if($method=="advertising_policy") {?>  class="active" <?php } ?>>
                                   <a href="<?=site_url('advertising-policy')?>">Advertising Policy</a>
                              </li>
                              
                              <li <?php if($method=="registration_process") {?>  class="active" <?php } ?>>
                                  <a href="<?=site_url('registration-process')?>">Registration Process</a>
                              </li>
                              
                              <li <?php if($method=="faq") {?>  class="active" <?php } ?>>
                                   <a href="<?=site_url('faq')?>">FAQ</a>
                              </li>
                         </ul>

                    </div>
               </div>