<!doctype html>

<html lang="en">
     <head>
          <!-- Required meta tags -->
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="keywords" content="">
          <meta name="author" content="Fujishka">
          <base href="<?php echo base_url('assets/');?>/"/>
          <title><?php echo $template['title']?></title>
          <?php echo $template['metadata']?>
          <script>
               var site_url = "<?php echo site_url();?>";
               var none_image = "<?php echo $this->config->item('none_image');?>";
          </script>
          <!-- favicon-->
          <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
          <link rel="icon" href="images/favicon.ico" type="image/x-icon">
          <!-- Bootstrap CSS
          <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"> -->
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
          <!--Main Menu File-->
          <link rel="stylesheet" type="text/css" media="all" href="css/webslidemenu.css" />
          <!-- SLIDER CSS -->
          <link rel="stylesheet" href="css/jquery.slide.css" />
          <!-- ICONS CSS -->
          <link rel="stylesheet" href="css/ionicons.min.css">
          <!-- fontawesome CSS -->
          <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> -->
          <link rel="stylesheet" href="../vendors/fontawesome-5.7.2/css/all.css">
          <!-- shop slider -->
          <link rel="stylesheet" href="css/ninja-slider.css" />
          <link rel="stylesheet" href="css/thumbnail-slider.css" />
          <!-- latest prod slider -->
          <link rel="stylesheet"  href="css/lightslider.css"/>
          <!--Main Menu File-->
          <script type="text/javascript" src="js/webslidemenu.js"></script>
          <!-- CUSTOM CSS -->
          <link rel="stylesheet" href="css/main.css">
          <!-- Chat -->
          <script src="js/supplier-scripts.js"></script>
          <link rel="stylesheet" href="css/chat-main2.css">
     </head>
     <body>
          <?php echo $template['partials']['site_header_home'];?>
          <?php echo $template['partials']['flash_messages']?>
          <?php echo $template['body']?>
          <?php echo $template['partials']['site_footer_home'];?>
     </body>
</html>
