<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="Fujishka">
      <base href="<?php echo base_url('assets/'); ?>/"/>
      <!-- favicon-->
      <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
      <link rel="icon" href="images/favicon.ico" type="image/x-icon">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
      <!--Main Menu File-->
      <link rel="stylesheet" type="text/css" media="all" href="css/webslidemenu.css" />
      <!-- fontawesome CSS -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
      <!-- slider for companies -->
      <link href="css/banner-slider-style.css" rel="stylesheet"/>
      <!-- category slider -->
      <!-- <link rel="stylesheet" href="css/lightslider.css" /> -->
      <!-- qr code -->
      <link rel="stylesheet" type="text/css" href="css/st.action-panel.css" />
      <!-- supplier gallery -->
      <link rel='stylesheet' href='css/unite-gallery.css' />
      <link rel='stylesheet' href='themes/default/ug-theme-default.css' />
      <!-- CUSTOM CSS -->
      <link rel="stylesheet" href="css/main.css">
      <link rel="stylesheet" href="css/chat-main2.css">
      <!-- image gallery css -->
      <link rel="stylesheet" href="css/magiczoomplus.css">
      <title>Fujeeka</title>
      <script>
         var site_url = "<?php echo site_url(); ?>";
         var none_image = "<?php echo $this->config->item('none_image'); ?>";
      </script>
   </head>
   <body ontouchstart="">
      <!-- Header Area -->
      <header class="no-bg">
         <div class="header-top-area inner-head no-sticky sup-header">
            <div class="container">
               <div class="col-12 flex-container">
                  <div class="logo-container">
                     <!-- Start Logo -->
                     <div class="logo2">
                        <a href="<?php echo site_url('home') ?>">
                        <img src="images/fujeeka-logo-green.png" alt="fujeeka logo">
                        </a>
                     </div>
                     <!-- / End Logo -->
                  </div>
                  

                  <!-- white strip -->
                    <section class="white-strip">
                      <div class="container">
                        <ul>
                          <li onclick="window.location.href='<?php echo site_url('market/markets'); ?>'">
                            <div class="icons-container">
                              <div class="icn">
                                <img src="images/home-market-icon.png" alt="market icon">
                              </div>
                              <div class="icn-txt">
                                <h5>Market</h5>
                                <p>Wholesale</p>
                              </div>
                            </div>
                          </li>
                          <li data-toggle="modal" id="search_a_modal" data-target="#search-modal">
                            <div class="icons-container">
                              <div class="icn">
                                <img src="images/home-product-icon.png" alt="products icon">
                              </div>
                              <div class="icn-txt">
                                <h5>Products</h5>
                                <p>Stock &amp; OEM</p>
                              </div>
                            </div>
                          </li>
                          
                          <li onclick="window.location.href = '<?php echo site_url('product/stock');?>'">
                              <div class="icons-container">
                                <div class="icn">
                                  <img src="images/home-stock-icon.png" alt="Stock icon">
                                </div>
                                <div class="icn-txt">
                                  <h5>Stock</h5>
                                  <p>Stock and Flash stocks</p>
                                </div>
                              </div>
                            </li>

                          <li onclick="window.location.href='<?php echo site_url('product/rfq'); ?>'">
                            <div class="icons-container">
                              <div class="icn">
                                <img src="images/home-rfq-icon.png" alt="rfq icon">
                              </div>
                              <div class="icn-txt">
                                <h5>RFQ</h5>
                                <p>One request, Multiple quotes</p>
                              </div>
                            </div>
                          </li>

                        </ul>
                      </div>
                    </section>
                    <!--/ white strip -->
                    
                  <!-- Start Menu -->
                  <div class="menu-container">
                     <div class="wsmenucontainer clearfix">
                        <div id="overlapblackbg"></div>
                        <div class="wsmobileheader clearfix">
                           <a id="wsnavtoggle" class="animated-arrow"><span></span></a> <!-- <a class="smallogo"><img src="images/fujeeka-logo.png"  alt="" /></a> -->
                        </div>
                        <div class="headerfull">
                           <!--Main Menu HTML Code-->
                           <div class="wsmain">
                              <nav class="wsmenu clearfix">
                                 <ul class="mobile-sub wsmenu-list">
                                    <li class="signin sh">
                                       <a  class="navtext p-0">
                                          <?php if (!$this->uid) {?>
                                          <div class="reg">
                                             <div class="usr-pic">
                                                <img src="images/icons/user.png" alt="My account">
                                             </div>
                                          </div>
                                          <?php } else {?>
                                          <div class="reg">
                                             <div class="usr-pic">
                                                <img src="images/icons/user.png" alt="My account">
                                             </div>
                                          </div>
                                          <?php }?>
                                       </a>
                                       <div class="megamenu clearfix halfmenu">
                                          <div class="container-fluid">
                                             <div class="row">
                                                <div class="signin-container">
                                                   <?php if (!$this->uid) {?>
                                                   <div class="top-section">
                                                      <h4 class="heading3">Get started now!</h4>
                                                      <!-- <button class="btn btn-success btn-green btn-signin"  onclick=" window.location.href = '<?php echo site_url('user/sup_home'); ?>'">Sign In</button> -->
                                                      <button class="btn btn-success btn-green btn-signin"  data-toggle="modal" data-target="#loginModal">Sign In</button>
                                                      <div class="w-100">
                                                         or
                                                      </div>
                                                      <button class="btn btn-success btn-reg" onclick=" window.location.href = '<?php echo site_url('user/signup'); ?>'">Register Now</button>
                                                   </div>
                                                   <?php }?>
                                                   <?php if ($this->uid) {?>
                                                   <div class="top2">
                                                      <div class="img">
                                                         <?php
                                                            echo img(array('src' => 'assets/uploads/avatar/' . $this->session->userdata('usr_avatar'),
                                                                'class' => 'img-fluid'));
                                                                ?>
                                                         <!-- <img src="images/avatar" alt="" class="img-fluid"> -->
                                                      </div>
                                                      <div class="name">
                                                         <h3>Welcome, <?php echo $this->session->userdata('usr_username'); ?></h3>
                                                         <p><a href="<?php echo site_url('dashboard'); ?>" target="_blank" rel="noopener noreferrer">View Dashboard</a></p>
                                                      </div>
                                                   </div>
                                                   <div class="my-section">
                                                      <div class="w-100 heading-bx">
                                                         My Fujeeka
                                                      </div>
                                                      <ul>
                                                         <li><a href="<?=site_url('product/rfq-list')?>" target="_blank" rel="noopener noreferrer">Manage RFQ</a></li>
                                                         <li><a href="<?=site_url('order')?>" target="_blank" rel="noopener noreferrer">My Orders</a></li>
                                                         <li><a href="<?=site_url('dashboard')?>" target="_blank" rel="noopener noreferrer">My Account</a></li>
                                                         <li class="signout"><a href="<?php echo site_url('user/logout'); ?>">Signout</a></li>
                                                      </ul>
                                                   </div>
                                                   <?php }?>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </nav>
                           </div>
                           <!--Menu HTML Code-->
                        </div>
                     </div>
                     <!-- /menu -->
                     <div class="clearfix"></div>
                  </div>
                  <!-- / End Menu -->
               </div>
            </div>
         </div>
         <!-- end topbar -->
         <div class="clearfix"></div>
      </header>
      <div class="modal" id="loginModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="login-m">
          <div class="left-sec">
            <h2>Login</h2>
            <p>Get access to the complete wholesale B2B website</p>
          </div>
          <div class="right-sec">
            <div class="form-container">
              <!-- <form action="<?=site_url('user/login')?>" method="post" id="modal_lgn_form"> -->
              <?php echo form_open('user/login', array('id' => 'modal_lgn_form', 'method' =>'post'))?>
              <span style="color:green" id="reset_success"></span>
                <div class="form-group lgn_grp">
                  <input type="text" class="form-control" id="identity" name="identity" placeholder="Username">
                  <span style="color:red" id="u_error"></span>
                </div>
                <div class="form-group lgn_grp">
                  <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                  <span style="color:red" id="p_error"></span>
                </div>
                <input type="hidden" name="from" value="sup_home">
                <div class="form-group fpswd_grp d-none">
                  <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                  <span style="color:red" id="m_error"></span>
                </div>
                <div class="form-group otp_grp d-none">
                  <span style="color:green" id="mail_success"></span>
                  <input type="text" class="form-control" id="otp" placeholder="Enter OTP" name="otp">
                  <span style="color:red" id="otp_error"></span>
                  <input type="hidden" id="resend_mail_ip">
                  <input type="hidden" id="forgot_code"> 
                </div>
                <div class="form-group change_grp d-none">
                  <input type="password" class="form-control" id="pswd" placeholder="Enter New Password" name="pswd">
                  <span style="color:red" id="pswd_error"></span>
                  <input type="password" class="form-control" id="cpswd" placeholder="Confirm New Password" name="cpswd">
                  <span style="color:red" id="cpswd_error"></span>
                  <input type="hidden" id="rest_code"> 
                </div>
                <div class="form-group mb-1 text-right">
                  <a id="a_fpwd" href="javascript:;">Forgot Password?</a>
                  <a id="a_lgn" class="d-none" href="javascript:;">Back to login</a>
                  <a id="a_resend_mail" class="d-none" href="javascript:;">Resend OTP</a>
                </div>
                <div class="clearfix"></div>
                <div>
                  <button type="submit" data-sec="login" id="modal_lgn_btnw" class="btn btn-success btn-login btn-inline">Login</button>
                  <div class="new-u mt-5">
                    New user? <a href="<?=site_url('user/signup')?>">Sign up</a>
                  </div>
                </div>
               <?php echo form_close()?>
              <!-- </form> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
      <?php if ($this->uid && !get_logged_user('usr_is_mail_verified')) {?>
         <!-- Activate Email -->
         <div class="modal verif-mail" id="verif-mail" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
            <div class="modal-content">

               <!-- Modal Header -->
               <div class="modal-header">
                  <h4 class="modal-title">Verify Email</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>

               <!-- Modal body -->
               <div class="modal-body">
               <form class="mail-form" data-parsely-validate="true">
                  Hi <?=$this->session->userdata('usr_fullname');?>,<br> Please verify your email 
                  <input id="mail_inp" type="email" autocomplete="off" required="" data-parsley-required-message="" data-parsley-type-message="Invalid email" data-parsley-errors-container="#par_error" name="new_mail" class="d-none" placeholder="Enter new email here">
                  <span id="ver_mail_span" class="font-bold">(<?=$this->session->userdata('usr_email');?>) to activate all the features.</span>
                  <a href="javascript:;" class="show_mail_inp green-txt">Change email</a>
                  <br>
                  <span id="par_error"></span>
                  <div class="al-btns" style="text-align: center;">
                  <a class="btn btn-success btn-green ver-email mx-auto mt-4">Resend Email</a>
                  </div>
                  
               </form> 
               </div>


            </div>
            </div>
         </div>
         <!-- /Activate Email -->
         <?php }?>
      <!-- / Header Area -->
      <!-- company webpage-->
      <div class="c-page">
         <section class="c-header">
            <div class="container">
               <div class="row">
                  <div class="col-12 c-logo">
                     <?php echo img(array('src' => FILE_UPLOAD_PATH . 'avatar/' . $suppliers['usr_avatar'])); ?>
                     <!-- <img src="images/company/logo3.jpg" alt="Company Name"> -->
                  </div>
               </div>
            </div>
         </section>
         <section class="c-menu">
            <div class="container">
               <div class="row">
                  <nav class="navbar navbar-expand-lg navbar-light bg-light">
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                     </button>
                     <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                           <li class="nav-item">
                              <a class="nav-link <?php if ($method == "home") {?> active <?php }?>" href="<?="http://".$suppliers['supm_domain_prefix'] .'.'.$this->http_host?>">Home</a>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product Categories</a>
                              <div class="dropdown-menu w-1" aria-labelledby="navbarDropdown">
                                 <?php
                                    if (isset($categories) && !empty($categories)) {
                                        foreach ($categories as $key => $value) {
                                            ?>
                                 <a class="dropdown-item" href="<?php echo "http://".$suppliers['supm_domain_prefix'] .'.'.$this->http_host.'/'.'product/category/' . slugify($value['cat_title']) . '/' . encryptor($value['cat_id']) . '?rdfrom=profile&supid=' . encryptor($suppliers['supm_id']); ?>">
                                 <?php echo $value['cat_title']; ?>
                                 </a>
                                 <?php
                                    }
                                    }
                                    ?>
                              </div>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link <?php if ($method == "profile") {?> active <?php }?>" href="<?="http://".$suppliers['supm_domain_prefix'] .'.'.$this->http_host.'/about'?>" id="" role="button" >
                              <span class="drop-btn">About Company</span>
                              </a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" href="<?="http://".$suppliers['supm_domain_prefix'] .'.'.$this->http_host.'#scontact'?>">Contact</a>
                           </li>
                           <li class="nav-item v360">
                              <a data-toggle="modal" id="_360_a" class="nav-link btn-360" href="<?php echo site_url() . 'supplier/get_360/' . encryptor($suppliers['supm_id']); ?>" data-target="#Modal-360">
                              <img src="images/360-o.png" alt="view 360"></a>
                           </li>
                        </ul>
                     </div>
                  </nav>
                  <div class="c-search">
                  </div>
               </div>
            </div>
         </section>
        
         <?php echo $template['partials']['flash_messages'] ?>
         <?php echo $template['body'] ?>
         <footer>
        <div class="container">
            <div class="links-container">
                <div class="row">
                <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Registration</h5>
                                <ul>
                                   <li><a href="<?php echo site_url('user/signup');?>">Buyer</a></li>
                                   <li><a href="<?php echo site_url('supplier/supplier-register');?>">Supplier</a></li>
                                   <li><a href="<?php echo site_url('market/market-register');?>">Building</a></li>
                                   <li><a href="<?php echo site_url('logistics/register');?>">Logistics</a></li>
                                   <li><a href="<?= site_url('quality-control/register')?>">Quality Control</a></li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <h5>Info</h5>
                                <ul>
                                   <li><a href="<?=site_url('about-us')?>">About Fujeeka</a></li>
                                    <li><a href="<?php echo site_url('logistics');?>">Logistics</a></li>
                                    <li><a href="<?php echo site_url('quality-control');?>">Quality Control</a></li>
                                    <li><a href="<?=site_url('contact-us')?>">Contact Us</a></li>
                                    <li><a href="javascript:;">Sitemap</a></li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <h5>LEGAL</h5>
                                <ul>
                                   <li><a href="<?=site_url('terms-of-use');?>">Terms of Use</a></li>
                                   <li><a href="<?= site_url('privacy-policies'); ?>">Privacy Policy</a></li>
                                   <li><a href="<?=site_url('product-listing-policies');?>">Product Listing Policy</a></li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <h5>Help</h5>
                                <ul>
                                    <li><a href="javascript:;">Help Center</a></li>
                                    <li><a href="<?=site_url('complaint_register')?>">Submit Complaint</a> </li>
                                    <li><a href="<?=site_url('faq')?>">FAQ</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="f-rfq">
                            <h5>Post your buying request</h5>
                            <?php echo form_open_multipart(site_url('product/rfq'), array('class' => "form-signin", 'data-parsley-validate' => "true"))?>
                                <div class="form-group">
                                    <select class="form-control" id="sell1" name="rfq_category" required data-parsley-required-message="Select a category">
                                         <option value="">Select Category</option>
                                         <?php foreach(get_buying_config('cat') as $cat){ ?>
                                             <option value="<?=$cat['cat_id']?>"><?=$cat['cat_title']?></option>
                                         <?php } ?>    
                                    </select>
                                </div>

                                <div class="form-group">
                                    <select class="form-control" id="markets" name="markets">
                                        <option value="">Select Market</option>
                                        <?php foreach(get_buying_config('mar') as $mar){ ?>
                                             <option value="<?=$mar['mar_id']?>"><?=$mar['mar_name']?></option>
                                        <?php } ?>      
                                   </select>
                                </div>

                                <div class="form-group">
                                    <input type="text" required data-parsley-required-message="Enter product name" class="form-control commonAutoComplete" id="product" placeholder="Keywords of products you are looking for" name="pname" data-url="<?php echo site_url('product/productAutoName');?>">
                                    <input type="hidden" id="prd_id" value="0" name="rfq_product">
                                    <input type="hidden"  class="unit_sel"  name="unit">

                                </div>

                                <button class="btn btn-success btn-green" value="footer" name="from_footer" type="submit">Get Quote</button>
                            <?php echo form_close()?>
                        </div>
                    </div>

                    <div class="col-md-4">
                    
                        <!-- Subscribe -->
                        <div class="subscribe-section">
                              <h5>Subscribe Newsletter</h5>
                              <form class="form-inline frmNewsletterSub" data-url="<?php echo site_url('general/newsletterSub'); ?>">
                                   <div class="form-group mr-2 mb-2">
                                        <label for="inputPassword2" class="sr-only">Password</label>
                                        <input type="email" class="form-control txtNewsSubEmail" id="subs-email" placeholder="Enter your email ">
                                   </div>
                                   <button type="submit" class="btn btn-success btn-green mb-2">Subscribe</button>
                                   <p>Subscribe newsletter to get notifications</p>
                              </form>
                              <p class="newsLtrMsg" style="color: red;font-size: 10px;"></p>
                         </div>
                        <!-- /Subscribe -->

                        <div class="gapp">
                            <h5>Get App</h5>
                            <div class="app-qr">
                                <a href="<?=site_url('home/get_app')?>"><img src="<?=site_url('assets/images/app-store.png')?>" alt="scan code to install ios app">
                                <img src="<?=site_url('assets/images/playstore.png')?>" alt="scan code to install android app"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-base">
                <div class="row">
                    <!-- <div class="col-md-4 download-app">
              <img src="assets/images/app-store.png" alt="Download from Appstore" class="img-fluid">
              <img src="assets/images/playstore.png" alt="Download from Playstore" class="img-fluid">
            </div> -->
                    <div class="col-md-6 text-left">
                        &copy; Copyright <?=date('Y')?> Fujeeka | All Rights Reserved
                    </div>
                    <div class="col-md-6">
                        <div class="sicon">
                            <div class="ficn">
                                <div class="icon-circle">
                                    <a href="javascript:;" class="ifacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>

                            <div class="ficn">
                                <div class="icon-circle">
                                    <a href="javascript:;" class="itwittter" title="Twitter"><i class="fab fa-twitter"></i></a>
                                </div>
                            </div>

                            <div class="ficn">
                                <div class="icon-circle">
                                    <a href="javascript:;" class="iInsta" title="Instagram"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      </div>
      
      <!-- qr code -->
      <div class="icon-bar">
         <a href="javascript:;" class="icons-bx" data-toggle="modal" title="Inquiry" data-target="#inq-pop"><i class="fas fa-question"></i></a>
         <!-- <a href="javascript:;" id="chat" data-sup_usr_id="<?php echo encryptor($supplier_user_id['usr_id']); ?>" <?php if (!$this->uid) {?> onclick="location.href = '<?php echo site_url('user/chat/' . encryptor($supplier_user_id['usr_id'])); ?>'" <?php } else {?> onclick="loadChatbox()" <?php }?>  class="icons-bx" data-toggle="tooltip" title="" data-original-title="Chat"><i class="fas fa-comments"></i></a> -->
         <!-- Chat icons -->
         <a href="javascript:;" class="icons-bx chat-pop">
         <i class="fas fa-comments"></i>
         </a>
         <div class="chat-options">
            <ul>
               <?php if ($this->uid != $supplier_user_id['usr_id']) {
                  if($this->uid && !get_logged_user('usr_is_mail_verified')){?>
               <li><a href="javascript:;" data-toggle="modal"  data-target="#verif-mail"   class="icons-bx chat_with" >Chat with Supplier</a></li>
               <?php } else { ?>
               <li><a href="javascript:;"  data-sup_usr_id="<?php echo encryptor($supplier_user_id['usr_id']); ?>" <?php if (!$this->uid) {?> data-toggle="modal" data-target="#loginModal" <?php } else {?> onclick="loadChatbox('<?php echo encryptor($supplier_user_id['usr_id']); ?>', '<?php
                  if (strlen($suppliers['supm_name_en']) > 20) {
                  echo substr($suppliers['supm_name_en'], 0, 20) . '...';
                  } else {
                  echo $suppliers['supm_name_en'];
                  }
                  ?>')" <?php }?>  class="icons-bx chat_with" data-toggle="tooltip" title="" data-original-title="Chat">Chat with Supplier</a></li>
               <?php } }?>
               <li><a href="javascript:;"  data-sup_usr_id="<?php echo encryptor(1); ?>" <?php if (!$this->uid) {?> data-toggle="modal" data-target="#loginModal" <?php } else {?> onclick="loadChatbox('<?php echo encryptor(1); ?>', 'Admin')" <?php }?>  class="icons-bx chat_with" data-toggle="tooltip" title="" data-original-title="Chat">Chat with Fujeeka</a></li>
            </ul>
         </div>
         <div class="st-actionContainer">
            <div class="st-panel" style="display: none;">
               <?php echo img(array('src' => FILE_UPLOAD_PATH . 'QR/' . $suppliers['supm_ref_number'] . '.png', 'alt' => 'scan qr code')); ?>
               <p class="text-center mb-0 pt-2">Scan QR Code</p>
            </div>
            <div class="st-btn-container">
               <div class="st-button-main rotateBackward" data-toggle="tooltip" title="" data-original-title="Scan QR Code"><i class="fas fa-qrcode"></i></div>
            </div>
         </div>
         <!-- /QR code -->
      </div>
      <!-- Sort  Modal -->
      <div id="search-modal" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!--Filter Modal content-->
            <div class="modal-content">
               <div class="modal-body sup_home" data-from="sup_home">
                  <!-- search bar -->
                  <?php echo $this->load->view('searchbar'); ?>
                  <!--/ searchbar -->
                  <p class="it-txt grey-txt">Eg : Mobile phones, headsets, power banks etc.</p>
               </div>
            </div>
         </div>
      </div>
      <!-- 360 modal -->
      <div id="Modal-360" class="modal fade modal-360" role="dialog">
         <div class="modal-dialog">
            <!--Filter Modal content-->
            <div class="modal-content">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <div class="modal-body 360_body">
               </div>
            </div>
         </div>
      </div>
      <!-- /360 modal -->
      <!-- inquiry modal -->
      <div id="inq-pop" class="modal fade inquiry" role="dialog">
         <div class="modal-dialog">
            <!--Modal content-->
            <div class="modal-content">
               <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
               <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="enq-container">
                     <div class="left-sec">
                        <div class="pic">
                           <img src="images/inquiry.png" alt="" class="img-fluid">
                        </div>
                        <div class="cont">
                           <h2>Have any questions?</h2>
                           <p>Don’t hesistate to ask, just fill the form and send to us. We will get back to you.</p>
                        </div>
                     </div>
                     <div class="frms">
                     <form id="qryform" action="" method="post" role="form">
                        <div class="form-group">
                           <label>Name:</label>
                           <input type="text" class="form-control"  placeholder="Enter Name" name="enq_name">
                        </div>
                        <input type="hidden" name="enq_sup_id" value="<?=encryptor($suppliers['supm_id'])?>">
                        <div class="form-group">
                           <label>Email:</label>
                           <input type="email" class="form-control"  placeholder="Enter Email" name="enq_email">
                        </div>
                        <div class="form-group">
                           <label>Phone No.:</label>
                           <input type="text" class="form-control"  placeholder="Enter Phone no." name="enq_phone">
                        </div>
                        <div class="form-group">
                           <label>Subject:</label>
                           <input type="text" class="form-control"  placeholder="Subject" name="enq_subject">
                        </div>
                        <div class="form-group">
                           <label>Issues/query:</label>
                           <textarea name="enq_query" class="form-control"  placeholder="Enter your Issues/query"></textarea>
                        </div>
                        <button type="submit" class="btn btn-success btn-green enq_submit">Submit</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <!-- end modal content-->
         </div>
      </div>
      <!-- inquiry modal -->
      <!-- Chat DIV -->
      <div class="chatbox" id="chatbox">
         <span class="chat-text chat_with_name"></span>
         <iframe id="ddd" src="" scrolling="no" frameborder="0" width="280px" height="450px" style="border:0; margin:0; padding: 0;">
         </iframe>
         <div id="close-chat" onclick="closeChatbox()">&times;</div>
         <div id="minim-chat" onclick="minimChatbox()"><span class="minim-button">&minus;</span></div>
         <div id="maxi-chat" onclick="maximChatbox()"><span class="maxi-button">&plus;</span></div>
      </div>
      <!-- /Chat DIV -->
      <!-- Alert box followed -->
      <div class="follow-alert alert alert-success alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> You are now following <span>Shenzhen Jedel Electronics Co., Ltd!</span>
      </div>
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script> -->
     
      
      <script src="js/jquery.validate.min.js"></script>                         
      <script>
         $("#qryform").validate({
         rules: {
         enq_email: {
            required: true,
            email: true
         },
         enq_name: {
            required: true
         },
         enq_phone: {
            required: true
         },
         enq_subject: {
            required: true
         },
         enq_query: {
            required: true
         },
         },
         messages: {
         enq_email: {
            required: "Enter email address",
            email: "Inalid email address",
         },
         enq_name: "Your name required",
         enq_phone: "Phone required",
         enq_subject: "Subject required",
         enq_query: "Issues/query required",
         },
         submitHandler: function(form) {
         $('.enq_submit').html('Please wait...');
         $('.enq_submit').prop('disabled', true);
         $('qryform')[0];
         $.ajax({
            type: 'post',
            url: site_url + "supplier/enquiry",
            dataType: 'json',
            data: $(form).serialize(),
            success: function(resp) {
                $('#inq-pop').modal('hide');
                $('.enq_submit').html('Submit');
                $('.enq_submit').prop('disabled', false);
                $("#qryform").find("input[type=text],input[type=email], textarea").val("");
                messageBox(resp);
            }
         });
         }
         });
         $('a.btn-360').on('click', function(e) {
          e.preventDefault();
          var url = $(this).attr('href');
          $(".360_body").html('<iframe width="100%" height="560" frameborder="0" scrolling="no" allowtransparency="true" src="' + url + '"></iframe>');
         });
      </script>
      <!-- modernizr JS -->
      <script src="js/modernizr-2.8.3.min.js"></script>
      <!--Main Menu File-->
      <script type="text/javascript" src="js/webslidemenu.js"></script>
      
      
      <!-- slider  for companies -->
      <?php if ($method == 'home') {?>
                 <script src="js/main-banner-script.js"></script>
                 <script src="js/jssor.slider.min.js" type="text/javascript"></script>
      <?php }?>
      <!-- QR Code -->
      <script src="js/st.action-panel.js"></script>
      <!-- supplier gallery -->
      <script type='text/javascript' src='js/unitegallery.min.js'></script>
      <script type='text/javascript' src='themes/default/ug-theme-default.js'></script>
      <!-- CUSTOM JS -->
      <!-- CUSTOM JS -->
      <script src="js/scripts-inner.js"></script>
      <!-- <script src="js/jquery.validate.min.js"></script> -->
      
      <script src="js/supplier-scripts.js"></script>
      
      <script type="text/javascript" src="js/search.js"></script>
      <script src="js/parsley.min.js"></script>
      <script type="text/javascript" src="../vendors/devbridge-autocomplete/jquery.autocomplete.min.js"></script>
      <script src="js/magiczoomplus.js" type="text/javascript"></script>
      <?php if ($this->uid && !get_logged_user('usr_is_mail_verified')) {?>
      <script src="js/verify_mail.js"></script>
      <style>
         .mail-form ul.parsley-errors-list {
         list-style: none;
         color: #E74C3C;
         padding-left: 3px;
         display:inline-block;
         }
      </style>
      <?php }?>
      <style>
         p.parsley-success {
         color: #468847;
         background-color: #DFF0D8;
         border: 1px solid #D6E9C6
         }
         p.parsley-error {
         color: #B94A48;
         background-color: #F2DEDE;
         border: 1px solid #EED3D7
         }
         ul.parsley-errors-list {
         list-style: none;
         color: #E74C3C;
         padding-left: 0
         }
         input.parsley-error,
         select.parsley-error,
         textarea.parsley-error {
         background: #FAEDEC;
         border: 1px solid #E85445
         }
      </style>
   </body>
</html>
<style>
   .ajax_loader1 {
   position: absolute;
   left: 0px;
   /* top: 0px; */
   width: 100%;
   height: 70%;
   z-index: 9999;
   background: url("images/loading.gif") 50% 50% no-repeat #f9f9f9;
   }
   .error{
   color:red;
   font-size:12px;
   }
</style>
<script>
   $(document).ready(function () {
        $(document).on("click", ".moveto", function () {
             var moveTo = $(this).attr('data-moveto');
             $('html, body').animate({
                  scrollTop: $(moveTo).offset().top
             }, 1000);
        });
        $("#_360_a_2").click(function () {
             $("#_360_a")[0].click();
        });
   });
</script>
<?php if ($this->router->fetch_method() == "supplier_product") {?>
<script>
   $(document).ready(function () {
        sup = "<?php echo $supplier; ?>";
        cat_id = $("#sel1").val();
        function load_page_data(page, sup = '', cate = '', order_by = '')
        {
             $.ajax({
                  url: "<?php echo base_url(); ?>supplier/supplier_product/" + page + "?page=" + page + "&category=" + cate + "&order_by=" + order_by + "&suplier=" + sup,
                  method: "GET",
                  dataType: "json",
                  beforeSend: function () {
                       $(".ajax_loader1").removeClass("d-none");
                  },
                  complete: function () {
                       $(".ajax_loader1").addClass("d-none");
                  },
                  success: function (data)
                  {
                       $('#product_listing').html(data.data);
                       $('#pagination_link').html(data.pagination_link);
                  }
             });
        }
        load_page_data(1, sup, cat_id);
        $(document).on("click", ".pagination li a", function (event) {
             event.preventDefault();
             ref = $(this).attr('href');
             page = ref.substring(ref.lastIndexOf("/") + 1, ref.length);
             cate = '';
             order_by = '';
             if ($("#sel1").val() != '') {
                  cate = $("#sel1").val();
             }
             if (typeof ($("#sel2").val()) !== "undefined" && $("#sel2").val() != '') {
                  cate = $("#sel2").val();
             }
             if ($("#sort_by").val() != '') {
                  order_by = $("#sort_by").val();
             }
             if (page == '')
                  page = 1;
             load_page_data(page, sup, cate, order_by);
             window.scrollTo(0, 0);
        });
        $(document).on("change", "#sel1", function () {
             $("#sel2").val('');
             cate = $("#sel1").val();
             if (cate == '') {
                  $("#sel2").addClass('d-none');
             } else {
                  $("#sel2").removeClass('d-none');
             }
             order_by = $("#sort_by").val();
             $("#cat_name").text($("#sel1 option:selected").text());
             load_page_data(1, sup, cate, order_by);
        });
       
        $(document).on("change", "#sort_by", function () {
             order_by = $(this).val();
             cate = $("#sel1").val();
             if (typeof ($("#sel2").val()) !== "undefined" && $("#sel2").val() != '') {
                  cate = $("#sel2").val();
             }
             load_page_data(1, sup, cate, order_by);
        });
   });
</script>
<?php }
   ?>
<script type="text/javascript">
   jQuery(document).ready(function () {
        jQuery("#gallery").unitegallery();
   });
</script>
<?php if ($this->uri->segment(2) == 'category') {?>
<script>
   function loadChatbox(id, name) {
        usr_id = id;
        $('.chat_with_name').text(name);
        $('#ddd').attr('src', site_url + 'user/chat_head/' + usr_id);
   
        var e = document.getElementById("minim-chat");
        e.style.display = "block";
        var e = document.getElementById("maxi-chat");
        e.style.display = "none";
        var e = document.getElementById("chatbox");
        e.style.margin = "0";
   
        var h = $('.chat_append');
        var height = h[0].scrollHeight;
        h.scrollTop(height);
   }
   
   function closeChatbox() {
        var e = document.getElementById("chatbox");
        e.style.margin = "0 0 -1500px 0";
   }
   
   function minimChatbox() {
        var e = document.getElementById("minim-chat");
        e.style.display = "none";
        var e = document.getElementById("maxi-chat");
        e.style.display = "block";
        var e = document.getElementById("chatbox");
        e.style.margin = "0 0 -455px 0";
   }
   
   function maximChatbox() {
        var e = document.getElementById("minim-chat");
        e.style.display = "block";
        var e = document.getElementById("maxi-chat");
        e.style.display = "none";
        var e = document.getElementById("chatbox");
        e.style.margin = "0";
   }
   
</script>
<?php } else if ($this->uri->segment(2) == 'product-details') {?>
<script>
   function loadChatbox() {
        $(".chatbox").removeClass("d-none");
        from = "product_details";
        if (from == "search") {
             $("#chat_name_span").html($("#chat").attr("data-sup_name"));
        }
        url = "http://localhost/fujeeka/user/chat_head";
        usr_id = $("#chat").attr("data-sup_usr_id");
        img_src = $("#prd_details").attr("data-src");
        p_name = $("#prd_details").attr("data-pname");
        p_price = $("#prd_details").attr("data-pprice");
        $('#ddd').attr('src', url + '/' + usr_id + '/' + img_src + '/' + p_name + '/' + p_price);
        var e = document.getElementById("minim-chat");
        e.style.display = "block";
        var e = document.getElementById("maxi-chat");
        e.style.display = "none";
        var e = document.getElementById("chatbox");
        e.style.margin = "0";
   }
   function closeChatbox() {
        var e = document.getElementById("chatbox");
        e.style.margin = "0 0 -1500px 0";
   }
   function maximChatbox() {
        var e = document.getElementById("minim-chat");
        e.style.display = "block";
        var e = document.getElementById("maxi-chat");
        e.style.display = "none";
        var e = document.getElementById("chatbox");
        e.style.margin = "0";
   }
   function minimChatbox() {
        var e = document.getElementById("minim-chat");
        e.style.display = "none";
        var e = document.getElementById("maxi-chat");
        e.style.display = "block";
        var e = document.getElementById("chatbox");
        e.style.margin = "0 0 -455px 0";
   }
</script>
<?php }?>