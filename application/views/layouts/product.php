<!doctype html>
<html lang="en">
     <head>
          <!-- Required meta tags -->
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="keywords" content="">
          <meta name="author" content="Fujishka">
          <base href="<?php echo base_url('assets/');?>/"/>
          <title><?php echo $template['title']?></title>
          <?php echo $template['metadata']?>
          <!-- favicon-->
          <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
          <link rel="icon" href="images/favicon.ico" type="image/x-icon">

          <!-- Bootstrap CSS -->
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
          <!--Main Menu File-->
          <link rel="stylesheet" type="text/css" media="all" href="css/webslidemenu.css" />

          <!-- fontawesome CSS -->
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
          <!-- CUSTOM CSS -->
          <link rel="stylesheet" href="css/main.css">
          <!-- image gallery css -->
          <link rel="stylesheet" href="css/magiczoomplus.css">



          <script>
               var site_url = "<?php echo site_url();?>";
               var none_image = "<?php echo $this->config->item('none_image');?>";
          </script>

     </head>

     <body class=<?php if ($this->router->fetch_method() == "product_details") echo "n-cont";?> >
          <?php echo $template['partials']['site_default_header'];?>
          <?php echo $template['partials']['flash_messages']?>
          <?php echo $template['body']?>
          <?php echo $template['partials']['site_default_footer'];?>
     </body>
</html>
<!-- layout/product used in 3 controller methods -->
<style>
     .ajax_loader1 {
          position: absolute;
          left: 0px;
          /* top: 0px; */
          width: 100%;
          height: 70%;
          z-index: 9999;
          background: url("images/loading.gif") 50% 50% no-repeat #f9f9f9;
     }
</style>
<?php if ($this->router->fetch_method() == "product_listing") {?>
     <script src="js/prod_listing.js"></script>
<?php }?>

<script>
     $(document).ready(function () {

          <?php if ($this->session->flashdata('from_login')){?>
               $("#chat").trigger("click");
          <?php } ?>
          $(document).on('change', '.cmbProductFilter', function () {
               var cid = $(this).val();
               var param = $(this).attr('data-url-param');
               window.location = replaceUrlParameter(param, cid);
          });
     });

     function replaceUrlParameter(paramName, paramValue) {
          var url = window.location.href;
          if (paramValue == null)
               paramValue = '';
          var pattern = new RegExp('\\b(' + paramName + '=).*?(&|$)')
          if (url.search(pattern) >= 0) {
               return url.replace(pattern, '$1' + paramValue + '$2');
          }
          return url + (url.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue
     }
     $('a.btn-360').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $(".360_body").html('<iframe width="100%" height="560" frameborder="0" scrolling="no" allowtransparency="true" src="' + url + '"></iframe>');
     });
</script>