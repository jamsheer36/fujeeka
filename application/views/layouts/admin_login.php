<!DOCTYPE html>
<html lang="en">
     <head>
          <meta charset="UTF-8">
          <title><?php echo $template['title']?></title>
          <base href="<?php echo base_url('assets/');?>/"/>
          <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/images/favicon.png');?>" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <script>
               var site_url = "<?php echo site_url();?>";
               var none_image = "<?php echo $this->config->item('none_image');?>";
          </script>

          <?php echo $template['metadata']?>
          <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
          <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900&subset=latin,latin-ext'>
          <link rel="stylesheet" href="css/login-style.css">
     </head>
     <body>
          <?php echo $template['body']?>
     </body>

     <script src="../vendors/jquery/dist/jquery.min.js"></script>
     <script src="js/login.js"></script>
</body>
</html>

<?php
  if ($success = $this->session->flashdata('app_success')):
       ?>
       <div class="alert alert-success alert-dismissible msgBox" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong><?php echo $success?></strong>
       </div>
  <?php endif?>

<style>
     .alert {

          position: absolute;
          right: 20px;
          bottom: 0px;
          padding: 15px;
          margin-bottom: 20px;
          border: 1px solid transparent;
          border-radius: 4px;
     }
     button.close {
          -webkit-appearance: none;
          padding: 0;
          cursor: pointer;
          background: 0 0;
          border: 0;
          float: right;
          margin-left: 10px;
     } 
     .alert-success {
          color: #fff;
          background-color: rgba(38, 185, 154, .88);
          border-color: rgba(38, 185, 154, .88);
     }

</style>