<!doctype html>

<html lang="en">

     <head>

          <!-- Required meta tags -->

          <meta charset="utf-8">

          <meta http-equiv="X-UA-Compatible" content="IE=edge">

          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

          <meta name="description" content="">

          <meta name="keywords" content="">

          <meta name="author" content="Fujishka">

          <base href="<?php echo base_url('assets/');?>/"/>

          <!-- favicon-->

          <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

          <link rel="icon" href="images/favicon.ico" type="image/x-icon">



          <!-- Bootstrap CSS -->

          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

          <!-- fontawesome CSS -->

          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

          <!-- CUSTOM CSS -->

          <link rel="stylesheet" href="css/main.css">

          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

          <script src="js/jquery.validate.min.js"></script>

          <title>Fujeeka</title>

     </head>

     <body>

          <?php echo ($this->router->fetch_class() != 'user') ? $template['partials']['header'] : '';?>

          <?php 
          // echo $template['partials']['flash_messages']
          ?>

          <?php echo $template['body']?>



          <!-- jQuery first, then Popper.js, then Bootstrap JS -->

          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

          <!-- modernizr JS -->

          <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
          <script src="js/jquery.easyWizard.js"></script>
          <script src="js/login.js"></script>

          <!-- CUSTOM JS -->

          <script src="js/scripts-inner.js"></script>



     </body>

</html>