<!DOCTYPE html>
<html>
     <head>
          <!-- Required meta tags -->
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="keywords" content="">
          <meta name="author" content="Fujishka">
          <title><?php echo $template['title']?></title>
          <base href="<?php echo base_url('assets/');?>/"/>
          <script>
               var site_url = "<?php echo site_url();?>";
               var none_image = "<?php echo $this->config->item('none_image');?>";
          </script>
          <!-- favicon-->
          <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
          <link rel="icon" href="images/favicon.ico" type="image/x-icon">
          <!-- Bootstrap CSS -->
          <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"> -->
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
          <link rel="stylesheet" href="css/form-style.css">
          <link rel="stylesheet" href="css/main.css">
          <!-- fontawesome CSS -->
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
          <script src="js/jquery.validate.min.js"></script>

     <body class="grey-bg">

          <div class="wrapper">
               <header class="form-header">
                    <div class="container">
                         <div class="row">
                              <div class="col-12 logo">
                                   <a href="<?= site_url();?>"><img src="images/fujeeka-logo-g.png" alt="Fujeeka" class="d-block mx-auto"></a>
                              </div>

                         </div>
                    </div>
               </header>
               <?php echo $template['partials']['flash_messages']?>
               <?php echo $template['body']?>

               <footer class="grey-bg" style="border:0; padding: 15px 0 0;">
                    <div class="copy">
                         Copyright &copy 2018 Fujeeka. All rights reserved.
                    </div>
               </footer>
          </div>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
          <!--<script src="js/form-js.js"></script>-->
          <!-- <script src="js/login.js"></script> -->
          <script src="js/signup.js"></script>

     </body>
</html>
