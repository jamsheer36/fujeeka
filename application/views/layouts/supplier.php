<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Fujishka">
    <base href="<?php echo base_url('assets/'); ?>/"/>
    <title><?php echo $template['title'] ?></title>
    <?php echo $template['metadata'] ?>
    <!-- favicon-->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <!--Main Menu File-->
    <link rel="stylesheet" type="text/css" media="all" href="css/webslidemenu.css" />

    <!-- fontawesome CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="css/main.css">
    <!-- image gallery css -->
    <link rel="stylesheet" href="css/magiczoomplus.css">



    <script>
        var site_url = "<?php echo site_url(); ?>";
        var none_image = "<?php echo $this->config->item('none_image'); ?>";
    </script>

  </head>

     <body class=<?php if ($this->router->fetch_method() == "product_details") echo "n-cont"; ?> >
          <?php echo $template['partials']['site_default_header']; ?>
          <?php echo $template['partials']['flash_messages'] ?>
          <?php echo $template['body'] ?>
          <?php echo $template['partials']['site_default_footer']; ?>
     </body>
</html>
<style>
.ajax_loader1 {
    position: absolute;
    left: 0px;
    /* top: 0px; */
    width: 100%;
    height: 70%;
    z-index: 9999;
    background: url("images/loading.gif") 50% 50% no-repeat #f9f9f9;
}
</style>
<!-- layout/supplier will be used in another controller methods -->

<?php if ($this->router->fetch_method() == "supplier_list") {?>

<script>
$(document).ready(function(){
    function load_page_data(page,cate = '',order_by = '')
    {
      $.ajax({
      url:"<?php echo base_url(); ?>supplier/supplier_list/"+page+"?page="+page+"&category="+cate+"&order_by="+order_by,
      method:"GET",
      dataType:"json",
      beforeSend: function() {
        $(".ajax_loader1").removeClass("d-none");
      },
      complete: function() {
        $(".ajax_loader1").addClass("d-none");
      },
      success:function(data)
      {
        $('#supplier_listing').html(data.data);
        $('#pagination_link').html(data.pagination_link);
        window.scrollTo(0, 0);
      }
      });
    }
    function load_sub_categories(cate)
    {
      $.ajax({
      url:"<?php echo base_url(); ?>category/getSubCategories/"+cate,
      method:"GET",
      dataType:"json",
      success:function(data)
      {
        $('.sub_cate').html(data.data);
      }
      });
    }

    load_page_data(1);

    $(document).on("click", ".pagination li a", function(event){
      event.preventDefault();
      ref = $(this).attr('href');
      page = ref.substring(ref.lastIndexOf("/") + 1, ref.length);
      cate = '';
      order_by = '';
      if($("#sel1").val()!=''){
        cate = $("#sel1").val();
      }
      if (typeof($("#sel2").val()) !== "undefined" && $("#sel2").val()!='') {
        cate = $("#sel2").val();
      }
      if($("#sort_by").val()!=''){
        order_by = $("#sort_by").val();
      }
      if(page=='')
        page = 1;
      load_page_data(page,cate,order_by);
    });

    $(document).on("change","#sel1",function(){
      $("#sel2").val('');
      cate = $("#sel1").val();
      if(cate==''){
        $("#sel2").addClass('d-none');
      }else{
        $("#sel2").removeClass('d-none');
      }
      order_by = $("#sort_by").val();
      load_sub_categories(cate);
      load_page_data(1,cate,order_by);
    });
    $(document).on("change","#sel2",function(){
      cate = $("#sel1").val();
      order_by = $("#sort_by").val();
      
      if ($("#sel2").val()!='') {
        cate = $("#sel2").val();
      } 
      load_page_data(1,cate,order_by);
    });


    $(document).on("change","#sort_by",function(){
      order_by = $(this).val();
      cate = $("#sel1").val();
      if (typeof($("#sel2").val()) !== "undefined" && $("#sel2").val()!='') {
        cate = $("#sel2").val();
      }
      load_page_data(1,cate,order_by);
    });

});
</script>
<?php }?>