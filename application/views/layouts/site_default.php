<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml">
     <head>
          <meta charset="utf-8"/>
          <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
          <meta name="description" content=""/>
          <meta name="keywords" content=""/>
          <meta name="author" content="Fujishka"/>

          <base href="<?php echo base_url('assets/');?>/"/>
          <title><?php echo $template['title']?></title>
          <?php echo $template['metadata']?>
          <script>
               var site_url = "<?php echo site_url();?>";
               var none_image = "<?php echo $this->config->item('none_image');?>";
          </script>

          <!-- favicon-->
          <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
          <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>

          <!-- Bootstrap CSS -->
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>

          <!--Main Menu File-->
          <link rel="stylesheet" type="text/css" media="all" href="css/webslidemenu.css" />

          <!-- fontawesome CSS -->
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" 
                integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"/>
          <!-- CUSTOM CSS -->
          <link rel="stylesheet" href="css/main.css"/>
          
          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
          
     </head>
     <body>
          <?php echo $template['partials']['site_default_header'];?>
          <?php echo $template['partials']['flash_messages']?>
          <?php echo $template['body']?>
          <?php echo $template['partials']['site_default_footer'];?>
     </body>
</html>