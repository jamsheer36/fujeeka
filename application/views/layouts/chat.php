<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="keywords" content="chatbox Fujeeka">
    <meta name="author" content="Fujeeka Chat">
    <title>Fujeeka Chat</title>
    <base href="<?php echo base_url('assets/');?>/"/>
    <!-- favicon-->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <!-- Font icons -->
    <link rel="stylesheet" href="css/fontello.css">
    <!-- Scrollbar -->
    <link rel="stylesheet" href="css/scrollbar.css" />

    <!-- Main Css -->
    <link rel="stylesheet" href="css/chat-main.css">
    <style>
        .ajax_loader {
            /* position: fixed; */
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url("images/loading.gif") 50% 50% no-repeat #f9f9f9;
        }
        /*Fetch from product page */

.green-box {
    background: #00b904;
    margin: 20px 0;
    border-radius: 5px;
    padding: 10px;
    max-width: 300px;
}

.product-info {
    display: flex;
    background: #fff;
    padding: 5px;
    color: #484848;
    border-radius: 5px;
}

.product-info .image {
    width: auto;
    height: 80px;
    /* border : solid 1px #e0e0e0; */
    padding: 10px;
    text-align: center;
    margin-right: 15px;
    background: #fff;
}

.product-info .image img {
    width: auto;
    height: 100%;
    margin: auto;
}

.product-info .detail {
    margin-top: 5px;
    text-align: left;
}

.product-info .detail span {
    /* color: $dark-grey; */
}

.product-info .detail p {
    margin-bottom: 10px;
}

.heading6 {
    font-family: 'robotoregular';
    font-size: 14px;
    color: #484848;
    line-height: 18px;
    margin-bottom: 10px;
}

.price-txt {
    color: #00b904 !important;
    font-family: 'robotoregular';
    font-size: 14px;
}

.interest {
    width: 100%;
    display: block !important;
    padding: 8px 0 0 !important;
    text-align: center;
    color: #fff;
    font-size: 13px;
    font-family: 'robotoregular';
    padding-top: 5px;
}
    </style>
</head>

<body>

<input type="hidden" value="<?php echo base_url(); ?>" name="" id="url">
<input type="hidden" value="<?php echo encryptor($this->uid) ?>" name="" id="channel">
<?php echo $template['body']?>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.1/jquery-migrate.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- modernizr JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    
    <?php if($is_open){?>
        <script>
            $(document).ready(function(){
                $('.chat_head1').click();
            })
        </script>
    <?php }?>
    <!-- Scrollbar -->
    <script src="js/scrollbar.js"></script>
    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <script src="js/pusher.js"></script>
</body>

</html>