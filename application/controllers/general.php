<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  require './vendor/autoload.php';

  class general extends App_Controller {

       public function __construct() {
            parent::__construct();

            require_once(APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php');
            $this->mail = new PHPMailer;
       }

       function doUploadFromEditor() {
            $newFileName = rand(9999999, 0);
            $config['upload_path'] = FILE_UPLOAD_PATH . 'redactor/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['file_name'] = $newFileName;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                 debug($this->upload->display_errors());
            } else {
                 $data = $this->upload->data();
                 $array = array(
                     'filelink' => '../assets/uploads/redactor/' . $data['file_name']
                 );

                 echo json_encode($array);
                 exit;
            }
       }

       function clearSupplier() {
            if ($this->common_model->clearSupplier()) {
                 $this->session->set_flashdata('app_success', 'Database cleaned successfully!');
            } else {
                 $this->session->set_flashdata('app_error', 'Error occured!');
            }
            redirect(__CLASS__ . '/dbclean');
       }

       function cleanProduct() {
            if ($this->common_model->cleanProduct()) {
                 $this->session->set_flashdata('app_success', 'Database cleaned successfully!');
            } else {
                 $this->session->set_flashdata('app_error', 'Error occured!');
            }
            redirect(__CLASS__ . '/dbclean');
       }

       function dbclean() {
            $this->load->view('dbclean');
       }

       function showme() {
            debug($this->session->userdata);
       }

       function getNotifications() {
            /* New supplier registration */
            echo json_encode($this->common_model->getNotifications());
       }

       function newsletterSub() {
            if ($this->common_model->newsletterSub($this->input->post('email'))) {
                 /* Mail to subscriber */
                 $this->mail->isSMTP();
                 $this->mail->Host = 'bh-44.webhostbox.net';
                 $this->mail->SMTPAuth = true;
                 $this->mail->Username = 'support@fujishka.org';
                 $this->mail->Password = 'support#@1';
                 $this->mail->SMTPSecure = 'ssl';
                 $this->mail->Port = 465;
                 $this->mail->setFrom('info@fujishka.org', 'Fujishka');
                 $this->mail->addReplyTo('info@fujishka.org', 'Fujishka');
                 $this->mail->addAddress($this->input->post('email'));
                 $this->mail->Subject = 'Newsletter subscription';
                 $this->mail->isHTML(true);
                 $mailData['email'] = $this->input->post('email');
                 $this->mail->Body = $this->load->view('newsletter-subscription-template', $mailData, true);
                 $this->mail->send();
                 die(json_encode(array('status' => 'success', 'msg' => 'You successfully subscribed!')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => 'You already subscribed!')));
            }
       }

       function toggler($table, $updateField, $pkField, $pkId) {
            if (!empty($table) && !empty($updateField) && !empty($pkField)&& !empty($pkId)) {
                  if ($this->common_model->toggler($table, $updateField, $pkField, $pkId)) {
                       die(json_encode(array('status' => 'success', 'msg' => 'Field successfully updated')));
                  }
            }
            die(json_encode(array('status' => 'fail', 'msg' => 'Error occured')));
       }
  }  