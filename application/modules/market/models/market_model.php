<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class market_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_states = TABLE_PREFIX . 'states';
            $this->tbl_buildins = TABLE_PREFIX . 'buildins';
            $this->tbl_category = TABLE_PREFIX . 'category';
            $this->tbl_countries = TABLE_PREFIX . 'countries';
            $this->tbl_favorite_list = TABLE_PREFIX . 'favorite_list';
            $this->tbl_market_places = TABLE_PREFIX . 'market_places';
            $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
            $this->tbl_market_cate_assoc = TABLE_PREFIX . 'market_cate_assoc';
            $this->tbl_supplier_followers = TABLE_PREFIX . 'supplier_followers';
            $this->tbl_supplier_shop_images = TABLE_PREFIX . 'supplier_shop_images';
            $this->tbl_supplier_buildings_cate_assoc = TABLE_PREFIX . 'supplier_buildings_cate_assoc';
            $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
            $this->tbl_users = TABLE_PREFIX . 'users';
       }

       function gerMarketPlaces($id = '') {
            if (empty($id)) {

                 $this->db->select($this->tbl_market_places . '.*,' . $this->tbl_countries . '.*,' . $this->tbl_states . '.*')
                         ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_market_places . '.mar_country_id')
                         ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_market_places . '.mar_state_id');

                 return $this->db->order_by('mar_id', 'DESC')->get($this->tbl_market_places)->result_array();
            } else {

                 $this->db->select($this->tbl_market_places . '.*,' . $this->tbl_countries . '.*,' . $this->tbl_states . '.*')
                         ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_market_places . '.mar_country_id', 'LEFT')
                         ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_market_places . '.mar_state_id', 'LEFT');

                 $return['marketPlace'] = $this->db->where('mar_id', $id)->get($this->tbl_market_places)->row_array();

                 $return['categoriesSelected'] = $this->db->select('GROUP_CONCAT(mca_category_id) AS mca_category_id')
                                 ->where('mca_market_id', $id)
                                 ->get($this->tbl_market_cate_assoc)->row()->mca_category_id;
                 return $return;
            }
       }

       function newMarket($data) {
            if (!empty($data)) {
                 $market = $data['market'];
                 $market['mar_slug'] = slugify($market['mar_name']);
                 $market['mar_added_by'] = $this->uid;
                 $this->db->insert($this->tbl_market_places, $market);
                 $marketId = $this->db->insert_id();

                 if (isset($data['categories']) && !empty($data['categories'])) {
                      foreach ($data['categories'] as $key => $value) {
                           $assoc = array('mca_market_id' => $marketId, 'mca_category_id' => $value);
                           $this->db->insert($this->tbl_market_cate_assoc, $assoc);
                      }
                 }

                 generate_log(array(
                     'log_title' => 'Create new record',
                     'log_desc' => 'New market created',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => $marketId,
                     'log_added_by' => $this->uid
                 ));
                 return true;
            }

            generate_log(array(
                'log_title' => 'Create new record',
                'log_desc' => 'Error while create market',
                'log_controller' => strtolower(__CLASS__),
                'log_action' => 'C',
                'log_ref_id' => 0,
                'log_added_by' => $this->uid
            ));
            return false;
       }

       function removeImage($id) {
            $market = $this->gerMarketPlaces($id);
            if (isset($market['marketPlace']) && !empty($market['marketPlace'])) {

                 if (file_exists(FILE_UPLOAD_PATH . 'market_place/' . $market['marketPlace']['mar_image'])) {
                      unlink(FILE_UPLOAD_PATH . 'market_place/' . $market['marketPlace']['mar_image']);
                      unlink(FILE_UPLOAD_PATH . 'market_place/thumb_' . $market['marketPlace']['mar_image']);
                 }
                 $this->db->where('mar_id', $id);
                 $this->db->update($this->tbl_market_places, array('mar_image' => ''));

                 generate_log(array(
                     'log_title' => 'Delete image',
                     'log_desc' => 'Removed market image',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'D',
                     'log_ref_id' => $id,
                     'log_added_by' => $this->uid
                 ));
                 return true;
            }
            return false;
       }

       function updateMarket($data) {
            $id = isset($data['mar_id']) ? $data['mar_id'] : 0;

            if (!empty($data)) {
                 $this->db->where('mar_id', $id);
                 $this->db->update($this->tbl_market_places, $data['market']);

                 if (isset($data['categories']) && !empty($data['categories'])) {
                      $this->db->where('mca_market_id', $id);
                      $this->db->delete($this->tbl_market_cate_assoc);
                      foreach ($data['categories'] as $key => $value) {
                           $assoc = array('mca_market_id' => $id, 'mca_category_id' => $value);
                           $this->db->insert($this->tbl_market_cate_assoc, $assoc);
                      }
                 }

                 generate_log(array(
                     'log_title' => 'Record updated',
                     'log_desc' => 'Update market',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $id,
                     'log_added_by' => $this->uid
                 ));

                 return true;
            } else {
                 generate_log(array(
                     'log_title' => 'Record updated',
                     'log_desc' => 'Error while update market',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $id,
                     'log_added_by' => $this->uid
                 ));
                 return false;
            }
       }

       function deleteMarket($id) {
            $this->removeImage($id);
            $this->db->where('mar_id', $id);
            $this->db->delete($this->tbl_market_places);
            $this->db->where('mca_market_id', $id);
            $this->db->delete($this->tbl_market_cate_assoc);

            generate_log(array(
                'log_title' => 'Delete record',
                'log_desc' => 'Market deleted',
                'log_controller' => strtolower(__CLASS__),
                'log_action' => 'D',
                'log_ref_id' => $id,
                'log_added_by' => $this->uid
            ));
            return true;
       }

       function getCaregoryByMarket($id) {
            return $this->db->select($this->tbl_market_cate_assoc . '.*,' . $this->tbl_category . '.*')
                            ->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . $this->tbl_market_cate_assoc . '.mca_category_id')
                            ->where($this->tbl_market_cate_assoc . '.mca_market_id', $id)
                            ->get($this->tbl_market_cate_assoc)->result_array();
       }

       function getMarkets() {
            return $this->db->select('mar_id,mar_name')->where('mar_status', 1)->order_by('mar_id', 'DESC')->get($this->tbl_market_places)->result_array();
       }

       function getBuildings() {
            return $this->db->select('bld_id,bld_title')->order_by('bld_id', 'DESC')->get($this->tbl_buildins)->result_array();
       }

       public function getCategories() {
            $this->db->select('cat_id,cat_title');
            $this->db->where('cat_status', 1);
            return $this->db->get($this->tbl_category)->result_array();
       }

       public function getMarketSuppliers($limit, $start, $filter) {
            $cls = isset($filter['cls']) ? $filter['cls'] : 'grid-style';
            $hide_grid = '';
            $hide_list = '';
            if ($cls == "grid-style") {
                 $hide_list = 'd-none';
            } else {
                 $hide_grid = 'd-none';
            }
            $output = '';
            $this->db->select('supm_name_en,supm_city_en,supm_address_en,supm_id,supm_reg_year,supm_domain_prefix,supm_default_image');
            $this->db->join($this->tbl_users,'usr_supplier = supm_id','RIGHT');
            $this->db->join($this->tbl_users_groups, 'user_id = usr_id', 'RIGHT');
            $this->db->where('group_id',2);
            $this->db->where('usr_active',1);
            $this->db->limit($limit, $start);
            $this->db->order_by($this->tbl_supplier_master . '.supm_added_on', 'desc');
            $this->db->where($this->tbl_supplier_master . '.supm_status', 1);
            if (isset($filter['market'])) {
                 $this->db->where('supm_market', $filter['market']);
            }
            if (isset($filter['building'])) {
                 $this->db->where('supm_building', $filter['building']);
            }
            $suppliers = $this->db->get($this->tbl_supplier_master)->result_array();

            foreach ($suppliers as $key => $val) {
                 $this->db->select($this->tbl_category . '.cat_id,cat_title');
                 $this->db->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . $this->tbl_supplier_buildings_cate_assoc . '.sbca_category');
                 $this->db->where('sbca_supplier', $val['supm_id']);
                 $catg = $this->db->get($this->tbl_supplier_buildings_cate_assoc)->result_array();

                 if (isset($filter['category'])) {
                      if (!in_array($filter['category'], array_column($catg, 'cat_id'))) {
                           unset($suppliers[$key]);
                           continue;
                      }
                 }
                 $suppliers[$key]['categories'] = $catg;
                 if (isset($suppliers[$key]['supm_default_image']) && !empty($suppliers[$key]['supm_default_image'])) {
                      $images = $this->db->get_where($this->tbl_supplier_shop_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
                 } else {
                      $images = $this->db->limit(1)->get_where($this->tbl_supplier_shop_images, array('ssi_supplier' => $val['supm_id']))->row_array();
                 }
                 $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
                 $suppliers[$key]['default_image'] = 'assets/uploads/shops/' . $defaultImg;
                 $suppliers[$key]['pimg_alttag'] = isset($image['ssi_alttag']) ? $image['ssi_alttag'] : 'supplier image';
            }
            foreach ($suppliers as $sup) {

                 $isFavorited = $this->db->get_where($this->tbl_favorite_list, array(
                             'fav_consign' => 'SUP', 'fav_consign_id' => $sup['supm_id'], 'fav_added_by' => $this->uid))->row_array();

                 $isFollowed = $this->db->get_where($this->tbl_supplier_followers, array(
                             'sfol_supplier' => $sup['supm_id'], 'sfol_followed_by' => $this->uid))->row_array();

                 $ecn_id = encryptor($sup['supm_id']);
                 $_360 = "data-toggle='modal' id='_360_a' class='nav-link btn-360' href='" . site_url('supplier/get_360') . "/" . $ecn_id . "' data-target='#Modal-360'";
                 $year = date('Y') - $sup['supm_reg_year'];
                 $cat = implode(', ', array_column($sup['categories'], 'cat_title'));
                 $cat_grid = $cat;
                 if (strlen($cat_grid) > 25) {
                      $cat_grid = substr($cat_grid, 0, 25) . '...';
                 }
                 $islogged = ($this->uid > 0) ? '1' : $this->calldrop;
                 $favClass = (!empty($isFavorited)) ? ' fas red-icn' : '';
                 $folClass = (!empty($isFollowed)) ? ' green-icn' : '';
                 $home = "http://".$sup['supm_domain_prefix'] .'.'.$this->http_host;
                 $favFollButtons = '';
                 if ($this->usr_grp != 'SP' || $this->usr_grp != 'ST') {
                      $favFollButtons = "<div class='left-btns'>
                           <div class='favourite'>
                             <a is-logged='" . $islogged . "' href='javascript:;'"
                              . "data-url='" . site_url('favorites/addSupplierToFavoriteList/' . encryptor($sup['supm_id'])) . "' class='btnFav fav'>
                              <i title='Favorites' id='fav' class='far fa-heart " . $favClass . "'></i></a>
                           </div>
                           <div is-logged='" . $islogged . "' class='" . $folClass . " btnFollow follow' id='foll' data-url='" . site_url('favorites/addToFollow/' . encryptor($sup['supm_id'])) . "'>
                             <a title='Follow' href='javascript:;' class='foll'><i class='fas fa-plus'></i></a>
                           </div>
                         </div>";
                 }
                 $output .= "<div class='single-prod " . $cls . "'>
                <div class='prod-box'>
                  <div class='prod-img position-relative'>
                    <a rel='noopener noreferrer' target='_blank' href='" . $home . "'>" . img(array('src' => $sup['default_image'], 'id' => '','alt' => $sup['pimg_alttag'])) . "</a>
                    <div class='tp-360'>
                           <a " . $_360 . " data-toggle='tooltip' title='View the shop in 360 viewer'><img src='images/360-b.png' alt='' class='img-fluid'></a>
                    </div>
                    " . $favFollButtons . "     
                  </div>
                  <div class='prod-detail txt-height'>
                    <div class='details'>
                      <h4 class='prod-title-s'><a rel='noopener noreferrer' target='_blank' href='" . $home .  "'>" . $sup['supm_name_en'] . "</a></h4>
                      <p class='loc'>Main Products :<br>
                      <span class='main_prd_grid " . $hide_grid . "'>" . $cat_grid . "</span></p>
                      <span class='main_prd_list " . $hide_list . "'>" . $cat . "</span></p>
                      <div class='sub-details'>
                        <p>Location : <span>".$sup['supm_city_en']."</span></p>
                        <div class='icon-details'>
                         <div class='icon verified'>Verified Information</div>
                       </div>

                      </div>
                    </div>

                    <div class='company-details'>
                      <p class='year'>" . $year . "th year</p>
                      <p class='loc'>Address : <span>" . $sup['supm_address_en'] . "</span></p>

                    <div class='clearfix'></div>
                      <p class='check'><a rel='noopener noreferrer' target='_blank' href='" . site_url('supplier/supplier-product/' . $ecn_id) . "'>Check All Products</a></p>
                    </div>
                    
                  </div>
                </div>
              </div>";
            }
            if (!$output) {
                 $output = "<h3 class='text-center'>No Suppliers Found Here!!</h3>";
            }
            return $output;
       }

       // to get total count for pagination
       function countAllMarketSupplier($filter = '') {
            $this->db->select($this->tbl_supplier_master . '.*');
            if (isset($filter['category'])) {
                 $this->db->join($this->tbl_supplier_buildings_cate_assoc, $this->tbl_supplier_buildings_cate_assoc . '.sbca_supplier = ' . $this->tbl_supplier_master . '.supm_id');
                 $this->db->where($this->tbl_supplier_buildings_cate_assoc . '.sbca_category', $filter['category']);
            }
            if (isset($filter['market'])) {
                 $this->db->where('supm_market', $filter['market']);
            }
            if (isset($filter['building'])) {
                 $this->db->where('supm_building', $filter['building']);
            }
            $this->db->where($this->tbl_supplier_master . '.supm_status', 1);
            $products = $this->db->get($this->tbl_supplier_master);
            return $products->num_rows();
       }

       function marketRegister($data) {
            if (!empty($data)) {

                 $data = array_filter($data);
                 $data['mar_status'] = 0;
                 $data['mar_slug'] = slugify($data['mar_name']);
                 $this->db->insert($this->tbl_market_places, $data);
                 $marketId = $this->db->insert_id();

                 generate_log(array(
                     'log_title' => 'Market register',
                     'log_desc' => 'New market register',
                     'log_controller' => 'market-registration-site',
                     'log_action' => 'C',
                     'log_ref_id' => $marketId,
                     'log_added_by' => $this->uid,
                 ));
                 return true;
            }
            return false;
       }

       function gerMarketRegistrations() {
            return $this->db->order_by('mar_added_on', 'DESC')
                            ->get_where($this->tbl_market_places, array('mar_status' => 0))->result_array();
       }

  }
  