<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>New Market Places</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("market/add", array('id' => "frmCategory", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market Name</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter name" type="text" class="form-control col-md-7 col-xs-12" name="market[mar_name]" id="cat_title" placeholder="Market Name"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market Category</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div class="div-category">
                                        <ul class="li-category">
                                             <?php foreach ((array) $categories as $key => $value) {?>
                                                    <li><input  required data-parsley-errors-container="#cat_err" data-parsley-required-message="Select category"  name="categories[]" value="<?php echo $value['cat_id'];?>" type="checkbox"/><span><?php echo $value['cat_title'];?></span></li>
                                               <?php }?>
                                        </ul>
                                   </div><span id="cat_err"></span>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select required data-parsley-required-message="Select country" class="form-control col-md-7 col-xs-12 bindToDropdown"
                                           data-dflt-select="Select State" name="market[mar_country_id]"
                                           data-url="<?php echo site_url('states/getStatesByCountry');?>"
                                           data-bind="cmbModel">
                                        <option value="">Select Country</option>
                                        <?php foreach ((array) $country as $key => $value) {?>
                                               <option value="<?php echo $value['ctr_id'];?>"><?php echo $value['ctr_name'];?></option>
                                          <?php }?>
                                   </select>
                              </div>
                         </div>
                         
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">State</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select required data-parsley-required-message="Select state" class="cmbModel select2_group form-control" name="market[mar_state_id]">
                                        <option value="">Select country first</option>
                                   </select>
                              </div>
                         </div>
                         
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter number" type="text" class="form-control col-md-7 col-xs-12" name="market[mar_contact_number]"  placeholder="Contact Number"/>
                              </div>
                         </div>
                         
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12" name="market[mar_city]" id="cat_title" placeholder="City"/>
                              </div>
                         </div>
                         
                          <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="market[mar_address]" class='editor'></textarea>
                              </div>
                         </div>
                         
                          <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description About Market</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="market[mar_des_market]" class='editor'></textarea>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market Image</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div id="newupload">
                                        <input type="hidden" id="x11" name="x1[]" />
                                        <input type="hidden" id="y11" name="y1[]" />
                                        <input type="hidden" id="x21" name="x2[]" />
                                        <input type="hidden" id="y21" name="y2[]" />
                                        <input type="hidden" id="w1" name="w[]" />
                                        <input type="hidden" id="h1" name="h[]" />
                                        <input required data-parsley-required-message="Upload a image" data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control col-md-7 col-xs-12" name="marketImage" 
                                               id="image_file1" onchange="fileSelectHandler('1', '600', '300')" />
                                        <img id="preview1" class="preview"/>
                                   </div>
                                    <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="market[mar_imgalt]" id="image_alt0" data-parsley-required ="true"data-parsley-required-message="Please enter an alt tag for the image"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                         </div>
                              </div>
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
</style>