<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Edit Market Places</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("market/update", array('id' => "frmCategory", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <input value="<?php echo $marketPlace['mar_id'];?>" type="hidden" name="mar_id"/>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market Name</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter name" value="<?php echo $marketPlace['mar_name'];?>" type="text" class="form-control col-md-7 col-xs-12" name="market[mar_name]" id="cat_title" placeholder="Market Name"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market Category</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div class="div-category">
                                        <ul class="li-category">
                                             <?php foreach ((array) $categories as $key => $value) {?>
                                                    <li><input required data-parsley-errors-container="#cat_err" data-parsley-required-message="Select category" <?php echo in_array($value['cat_id'], $categoriesSelected) ? 'checked="checked"' : '';?> 
                                                              name="categories[]" value="<?php echo $value['cat_id'];?>" type="checkbox"/>
                                                         <span><?php echo $value['cat_title'];?></span></li>
                                               <?php }?>
                                        </ul>
                                   </div><span id="cat_err"></span>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select required data-parsley-required-message="Select country"  class="form-control col-md-7 col-xs-12 bindToDropdown"
                                           data-dflt-select="Select State" name="market[mar_country_id]"
                                           data-url="<?php echo site_url('states/getStatesByCountry');?>"
                                           data-bind="cmbModel">
                                        <option value=''>Select Country</option>
                                        <?php foreach ((array) $country as $key => $value) {?>
                                               <option <?php echo ($value['ctr_id'] == $marketPlace['mar_country_id']) ? 'selected="selected"' : '';?>
                                                    value="<?php echo $value['ctr_id'];?>"><?php echo $value['ctr_name'];?></option>
                                               <?php }?>
                                   </select>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">State</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select required data-parsley-required-message="Select state" class="cmbModel select2_group form-control" name="market[mar_state_id]">
                                        <option value=''>Select country first</option>
                                        <?php foreach ((array) $states as $key => $value) {?>
                                               <option <?php echo ($value['stt_id'] == $marketPlace['mar_state_id']) ? 'selected="selected"' : '';?>
                                                    value="<?php echo $value['stt_id'];?>"><?php echo $value['stt_name'];?></option>
                                               <?php }?>
                                   </select>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter number" value="<?php echo $marketPlace['mar_contact_number']; ?>" type="text" 
                                          class="form-control col-md-7 col-xs-12" name="market[mar_contact_number]"  placeholder="Contact Number"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input value="<?php echo $marketPlace['mar_city']; ?>" type="text" 
                                          class="form-control col-md-7 col-xs-12" name="market[mar_city]" id="cat_title" placeholder="City"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="market[mar_address]" class='editor'><?php echo $marketPlace['mar_address']; ?></textarea>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description About Market</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="market[mar_des_market]" class='editor'><?php echo $marketPlace['mar_des_market']; ?></textarea>
                              </div>
                         </div>

                         <div id="mydiv">
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                             <?php echo img(array('src' => FILE_UPLOAD_PATH . 'market_place/thumb_' . $marketPlace['mar_image'], 'id' => 'imgBrandImage'));?>
                                        </div>
                                        <?php if ($marketPlace['mar_image']) {?>
                                               <span class="help-block">
                                                    <a data-url="<?php echo site_url('market/removeImage/' . $marketPlace['mar_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                               </span>
                                          <?php }?>
                                          
                                         <div id="altTag">
                                              <input placeholder="Alt tag for image" type="text" name="market[mar_imgalt]" value="<?php echo $marketPlace['mar_imgalt'] ?>" id="image_alt0" data-parsley-required ="true"data-parsley-required-message="Please enter an alt tag for the image" class="form-control col-md-9 col-xs-12"/>
                                             </div>
                                          

                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Market Image</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x10" name="x1[]" />
                                             <input type="hidden" id="y10" name="y1[]" />
                                             <input type="hidden" id="x20" name="x2[]" />
                                             <input type="hidden" id="y20" name="y2[]" />
                                             <input type="hidden" id="w0" name="w[]" />
                                             <input type="hidden" id="h0" name="h[]" />
                                             <input <?php if(!$marketPlace['mar_image']){?> required data-parsley-required-message="Upload a image" <?php } ?> data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control col-md-7 col-xs-12" name="marketImage" 
                                                    id="image_file0" onchange="fileSelectHandler('0', '600', '300')" />
                                             <img id="preview0" class="preview"/>
                                        </div>
                                          <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="mar_imgalt_new" id="bImgAlt" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                         </div>
                                   </div>
                              </div>
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success">Submit</button>' : '';?>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
</style>

<script>
  $('input[type=file]').change(function(e){
   var image =  $('input[type=file]')[0].files[0].name;
   if(image)
   {
    $('#bImgAlt').attr('data-parsley-required',true);
   }
   else
   {
          $('#bImgAlt').attr('data-parsley-required',false);
   }
});
  </script>