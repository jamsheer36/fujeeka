<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class contact extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->page_title = 'Contact Enquiry';
            $this->load->model('contact_model', 'contact');
       }

       public function enquiry() {
         
            $data['enquiry'] = $this->contact->getEnquiries();
           // print_r($data);die("");
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       public  function updatestatus($cid)
          {
               $complaint_id = encryptor($cid,'D');
               $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
               if ($complaint_id && $this->complaint->updateComplaint($complaint_id,$ischecked)) {
                    if($ischecked)
                    $this->sendMailtoUser($complaint_id);

                    generate_log(array(
                        'log_title' => 'Status changed',
                        'log_desc' => 'Complaint status changes',
                        'log_controller' => 'Complaint-status-changed',
                        'log_action' => 'U',
                        'log_ref_id' => $complaint_id,
                        'log_added_by' => $this->uid,
                    ));
                    $msg = ($ischecked == 1) ? "Resolved this complaint " : "Complaint is acive ";
                    die(json_encode(array('status' => 'success', 'msg' => $msg)));
               } else {
                    die(json_encode(array('status' => 'fail', 'msg' => "Error occured")));
               }
              
       }



       public function sendMailtoUser($cid)
       {
             $mailInfo = $this->complaint->getComplaint($cid);
             $data=  $mailInfo[0];
        
              $this->mail->isSMTP();
              $this->mail->Host = MAIL_HOST;
              $this->mail->SMTPAuth = true;
              $this->mail->Username = MAIL_USERNAME;
              $this->mail->Password = MAIL_PASSWORD;
              $this->mail->SMTPSecure = 'ssl';
              $this->mail->Port = 465;
              $this->mail->setFrom(FROM_MAIL, FROM_NAME);
              $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
              $this->mail->addAddress($data['contact_email']);
              $this->mail->Subject = 'Complaint Resolved - '.$data['c_name'];
              $this->mail->isHTML(true);
              $this->mail->Body = $this->load->view('complaint-user_notification-template', $data, true);

              //print_r($this->mail->Body);die();
              $this->mail->send();
       }
      
  }