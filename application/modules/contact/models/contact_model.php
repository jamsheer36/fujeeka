<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class contact_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->tbl_contacts = TABLE_PREFIX . 'contacts';
       }

    

       function getEnquiries() {
                  $this->db->set('c_seen',1)->update($this->tbl_contacts);
               return $this->db->select('*')->get($this->tbl_contacts)->result_array();
       }

  }
  