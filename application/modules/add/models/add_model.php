<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class add_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->tbl_advertisement = TABLE_PREFIX . 'advertisement';
            $this->pk = 'add_id';
       }

       public function getAdd($id = '') {

            if (!empty($id)) {
                 return $this->db->where($this->tbl_advertisement . '.' . $this->pk, $id)->get($this->tbl_advertisement)->row_array();
            }
            return $this->db->order_by($this->tbl_advertisement . '.add_order', 'asc')->get($this->tbl_advertisement)->result_array();
       }

       public function newAdd($datas) {
            $datas['add_slug'] = slugify($datas['add_title']);
            if ($this->db->insert($this->tbl_advertisement, $datas)) {
                 $id = $this->db->insert_id();
                 $this->db->where($this->pk, $id);
                 $this->db->update($this->tbl_advertisement, array('add_order' => $id));
                 return $id;
            } else {
                 return false;
            }
       }

       public function removeImage($id) {
            if ($id) {
                 $this->db->where('add_id', $id);
                 $data = $this->db->get($this->tbl_advertisement)->row_array();
                 if (isset($data['add_image']) && !empty($data['add_image'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'advt/' . $data['add_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'advt/' . $data['add_image']);
                      }
                      if (file_exists(FILE_UPLOAD_PATH . 'advt/thumb_' . $data['add_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'advt/thumb_' . $data['add_image']);
                      }
                      $this->db->where('add_id', $id);
                      if ($this->db->update($this->tbl_advertisement, array('add_image' => '')))
                           return true;
                 }
            }
            return false;
       }

       public function updateGallery($datas) {
            $datas['add_show_on_home_page'] = isset($datas['add_show_on_home_page']) ?
                    $datas['add_show_on_home_page'] : '0';
            $datas['add_slug'] = slugify($datas['add_title']);
            $this->db->where($this->pk, $datas[$this->pk]);
            unset($datas[$this->pk]);
            if ($this->db->update($this->tbl_advertisement, $datas)) {
                 return true;
            } else {
                 return false;
            }
       }

       public function deleteGallery($id) {
            $this->removeImage($id);

            $this->db->where($this->pk, $id);
            if ($this->db->delete($this->tbl_advertisement)) {
                 return true;
            } else {
                 return false;
            }
       }

       public function addImages($image) {
            if ($this->db->insert($this->tbl_advertisement_images, $image)) {
                 return $this->db->insert_id();
            } else {
                 return false;
            }
       }

       function setDefaultImage($imgId, $projectId) {

            $this->db->where($this->pk, $projectId);
            if ($this->db->update($this->tbl_advertisement, array('add_default_image' => $imgId))) {
                 return true;
            } else {
                 return false;
            }
            exit;
       }

       function getCount() {
            $count = $this->db->select('COUNT(*) AS count')->from($this->tbl_advertisement)->get()->row_array();
            return isset($count['count']) ? $count['count'] : 0;
       }

       function setPriority($projId, $newOrder) {
            $add_on_neworder = $this->db->select('*')->from($this->tbl_advertisement)->where('add_order', $newOrder)->get()->row_array();
            $add_on_projectid = $this->db->select('*')->from($this->tbl_advertisement)->where($this->pk, $projId)->get()->row_array();

            $this->db->where($this->pk, $add_on_neworder['add_id']);
            $this->db->update($this->tbl_advertisement, array('add_order' => $add_on_projectid['add_order']));

            $this->db->where($this->pk, $projId);
            $this->db->update($this->tbl_advertisement, array('add_order' => $newOrder));
            return true;
       }

       function getNextOrder($max = false) {
            if ($max) {
                 return $this->db->count_all_results($this->tbl_advertisement);
            } else {
                 return $this->db->select_max('add_order')->get($this->tbl_advertisement)->row()->add_order + 1;
            }
       }

       function getHomePageAdd() {
            return $this->db->where(array('add_status' => 1, 'add_show_on_home_page' => 1))
                            ->order_by($this->tbl_advertisement . '.add_order', 'asc')
                            ->get($this->tbl_advertisement)->result_array();
       }

  }
  