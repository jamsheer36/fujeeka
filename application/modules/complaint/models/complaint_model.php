<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class complaint_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_complaints = TABLE_PREFIX . 'complaints';
            $this->complaint_types = TABLE_PREFIX . 'complaint_types';
            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_property_infringement = TABLE_PREFIX . 'property_infringement';
       }

    

       function getComplaint($cid=null) {
         
          if($cid)
          {
               $this->db->set('seen',1)->where($this->tbl_complaints.'.id', $cid)->update($this->tbl_complaints);
               return $this->db->select($this->tbl_complaints . '.*,'.$this->tbl_users  . '.usr_first_name ,'. $this->tbl_users  . '.usr_last_name ' )

               ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_complaints . '.user_id', 'INNER')
               ->where($this->tbl_complaints.'.id', $cid)->get($this->tbl_complaints)->result_array();
          }
          else
          {

       
          $this->db->set('seen',1)->update($this->tbl_complaints);
                   return  $this->db->select($this->tbl_complaints . '.*,'.$this->tbl_users  . '.usr_first_name ,'. $this->tbl_users  . '.usr_last_name ' )
                            ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_complaints . '.user_id', 'INNER')
                            ->order_by($this->tbl_complaints .'.status','ASC')->get($this->tbl_complaints)->result_array();
          }
       }


       function updateComplaint($cid,$stat)
       {
            $this->db->set('status',$stat);
            $this->db->where('id',$cid);
            $this->db->update($this->tbl_complaints);
            return true;

       }

       function resolveComplaint($cid,$stat)
       {
            $this->db->set('status',$stat);
            $this->db->where('id',$cid);
            $this->db->update($this->tbl_property_infringement);
            return true;

       }


       function getInfringementComplaint($cid=null) {
         
          if($cid)
          {
               $this->db->set('seen',1)->where($this->tbl_property_infringement.'.id', $cid)->update($this->tbl_property_infringement);
               return $this->db->select($this->tbl_property_infringement . '.*,'.$this->tbl_users  . '.usr_first_name ,'. $this->tbl_users  . '.usr_last_name ' )

               ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_property_infringement . '.user_id', 'INNER')
               ->where($this->tbl_property_infringement.'.id', $cid)->get($this->tbl_property_infringement)->result_array();
          }
          else
          {

       
          $this->db->set('seen',1)->update($this->tbl_property_infringement);
                   return  $this->db->select($this->tbl_property_infringement . '.*,'.$this->tbl_users  . '.usr_first_name ,'. $this->tbl_users  . '.usr_last_name ' )
                            ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_property_infringement . '.user_id', 'INNER')
                            ->order_by($this->tbl_property_infringement .'.status','ASC')->get($this->tbl_property_infringement)->result_array();
          }
       }





  }
  