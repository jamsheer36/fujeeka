<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class complaint extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->page_title = 'Complaints';
            $this->load->model('complaint_model', 'complaint');
            require_once APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php';
            $this->mail = new PHPMailer;
            $this->lock_in();
       }

       public function index() {
         
            $data['complaints'] = $this->complaint->getComplaint();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }
       
       public function infringement_complaints() {
         
          $data['complaints'] = $this->complaint->getInfringementComplaint();
          $this->render_page(strtolower(__CLASS__) . '/infringement_complaints', $data);
     }

       public  function updatestatus($cid)
          {
               $complaint_id = encryptor($cid,'D');
               $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
               if ($complaint_id && $this->complaint->updateComplaint($complaint_id,$ischecked)) {
                    if($ischecked)
                    $this->sendMailtoUser($complaint_id);

                    generate_log(array(
                        'log_title' => 'Status changed',
                        'log_desc' => 'Complaint status changes',
                        'log_controller' => 'Complaint-status-changed',
                        'log_action' => 'U',
                        'log_ref_id' => $complaint_id,
                        'log_added_by' => $this->uid,
                    ));
                    $msg = ($ischecked == 1) ? "Resolved this complaint " : "Complaint is acive ";
                    die(json_encode(array('status' => 'success', 'msg' => $msg)));
               } else {
                    die(json_encode(array('status' => 'fail', 'msg' => "Error occured")));
               }
              
       }



       public function sendMailtoUser($cid)
       {
             $mailInfo = $this->complaint->getComplaint($cid);
             $data=  $mailInfo[0];
        
              $this->mail->isSMTP();
              $this->mail->Host = MAIL_HOST;
              $this->mail->SMTPAuth = true;
              $this->mail->Username = MAIL_USERNAME;
              $this->mail->Password = MAIL_PASSWORD;
              $this->mail->SMTPSecure = 'ssl';
              $this->mail->Port = 465;
              $this->mail->setFrom(FROM_MAIL, FROM_NAME);
              $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
              $this->mail->addAddress($data['contact_email']);
              $this->mail->Subject = 'Complaint Resolved - '.$data['c_name'];
              $this->mail->isHTML(true);
              $this->mail->Body = $this->load->view('complaint-user_notification-template', $data, true);

            
              $this->mail->send();
       }
      

       public function complaint_details($id) {
          $this->page_title = 'Home | Complaint Details';
          $id = encryptor($id, 'D');
          $data = $this->complaint->getComplaint($id);
          $this->render_page(strtolower(__CLASS__) . '/complaint_details.php', $data['0']);
     }

     public function view_complaint($id) {
          $this->page_title = 'Home | Complaint Details';
          $id = encryptor($id, 'D');
          $data = $this->complaint->getInfringementComplaint($id);
          $pid= explode(',', $data[0]['product_link']);
          $i=0;
          $links ='';
          foreach ($pid as $val)
          {
               $i++;
               $redirect = "'".site_url()."product/product_details/".encryptor($val)."'";
               $links.= "<a href=".$redirect."  target='_balank' >link $i</a> ," ;
          }
            $data[0]['product_link'] = $links;


          $this->render_page(strtolower(__CLASS__) . '/complaint_detail.php', $data[0]);
     }
     


     public  function resolve($cid)
     {
          $complaint_id = encryptor($cid,'D');
          $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
          if ($complaint_id && $this->complaint->resolveComplaint($complaint_id,$ischecked)) {
               if($ischecked)
              $this->sendMailtoCustomer($complaint_id);

               generate_log(array(
                   'log_title' => 'Status changed',
                   'log_desc' => 'Complaint status changes',
                   'log_controller' => 'Complaint-status-changed',
                   'log_action' => 'U',
                   'log_ref_id' => $complaint_id,
                   'log_added_by' => $this->uid,
               ));
               $msg = ($ischecked == 1) ? "Resolved this complaint " : "Complaint is acive ";
               die(json_encode(array('status' => 'success', 'msg' => $msg)));
          } else {
               die(json_encode(array('status' => 'fail', 'msg' => "Error occured")));
          }
         
  }


  public function sendMailtoCustomer($cid)
       {
             $mailInfo = $this->complaint->getInfringementComplaint($cid);
             $data=  $mailInfo[0];
        
              $this->mail->isSMTP();
              $this->mail->Host = MAIL_HOST;
              $this->mail->SMTPAuth = true;
              $this->mail->Username = MAIL_USERNAME;
              $this->mail->Password = MAIL_PASSWORD;
              $this->mail->SMTPSecure = 'ssl';
              $this->mail->Port = 465;
              $this->mail->setFrom(FROM_MAIL, FROM_NAME);
              $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
              $this->mail->addAddress($data['complainant_email']);
              $this->mail->Subject = 'Complaint Resolved - '.$data['type'];
              $this->mail->isHTML(true);
              $this->mail->Body = $this->load->view('complaint-user_notification-template', $data, true);

           
              $this->mail->send();
       }

       public function productsearch() {
          die("fdfdf");
         // $return = $this->complaint->productByKeyword($this->input->get('keyword'));
        //  print_r($return);die();
          }


  }