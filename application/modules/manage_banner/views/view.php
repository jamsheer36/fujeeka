<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Edit banner</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <?php echo form_open_multipart("manage_banner/update", array('id' => "frmBanner", 'class' => "form-horizontal"))?>
                         <input type="hidden" name="bnr_id" value="<?php echo $banner['bnr_id']?>" />
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Priority</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php if (!empty($order)) {?>
                                          <select name="banner[bnr_order]" id="bnr_order" class="form-control">
                                               <option value="">Select Priority</option>
                                               <?php for ($i = 1; $i <= $order; $i++) {?>
                                                    <option <?php echo ($i == $banner['bnr_order']) ? "selected='selected'" : '';?> 
                                                         value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php }?>
                                          </select>
                                     <?php }?>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select class="form-control cmbBannerCategory" name="banner[bnr_category]" required
                                           data-parsley-required-message="Select platform">
                                        <option value="">Select platform</option>
                                        <optgroup label="App / Mobile version">
                                             <option dimension=".D800x400" <?php echo ($banner['bnr_category'] == 2) ? "selected='selected'" : '';?> value="2">Home</option>
                                             <option dimension=".D800x400" <?php echo ($banner['bnr_category'] == 'app_logistics') ? "selected='selected'" : '';?> value="app_logistics">Logistics</option>
                                             <option dimension=".D800x400" <?php echo ($banner['bnr_category'] == 'app_quality_control') ? "selected='selected'" : '';?> value="app_quality_control">Quality control</option>
                                             <option dimension=".D800x400" <?php echo ($banner['bnr_category'] == 'app_complaints') ? "selected='selected'" : '';?> value="app_complaints">Complaints App</option>
                                        </optgroup>
                                        <optgroup label="Web pages">
                                             <option dimension=".D1920x468" <?php echo ($banner['bnr_category'] == 'home') ? "selected='selected'" : '';?> value="home">Website home page</option>
                                             <option dimension=".D1920x633" <?php echo ($banner['bnr_category'] == 'logistics') ? "selected='selected'" : '';?> value="logistics">Logistics</option>
                                             <option dimension=".D1920x468" <?php echo ($banner['bnr_category'] == 'quality_control') ? "selected='selected'" : '';?> value="quality_control">Quality control</option>
                                             <option dimension=".D1920x468" <?php echo ($banner['bnr_category'] == 'complaints') ? "selected='selected'" : '';?> value="complaints">Complaints Web</option>
                                        </optgroup>
                                   </select>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Url</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input value="<?php echo $banner['bnr_url'];?>" type="text" class="form-control" name="banner[bnr_url]"/>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea placeholder="Description" class="editor" name="banner[bnr_desc]"><?php echo $banner['bnr_desc'];?></textarea>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div class="input-group">
                                        <?php echo img(array('src' => FILE_UPLOAD_PATH . 'banner/' . $banner['bnr_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                   </div>
                                   <?php if ($banner['bnr_image']) {?>
                                          <span class="help-block">
                                               <a data-url="<?php echo site_url('manage_banner/removeImage/' . $banner['bnr_id'] . '/' . $banner['bnr_image']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                          </span>
                                     <?php }?>

                                      <div id="altTag">
                           <input placeholder="Alt tag for image" type="text" 
                          name="bnr_imagealt_update"  value="<?php echo $banner['bnr_imagealt']; ?>" id="bImgAlt" required  data-parsley-required-message="Please enter an alt tag for the image"class="form-control col-md-9 col-xs-12"/>
                  </div>
                              </div>
                         </div>
                         <br />
                         <div class="divDIM">
                              <?php
                                if ($banner['bnr_category'] == 2 || $banner['bnr_category'] == 'app_logistics' ||
                                        $banner['bnr_category'] == 'app_quality_control' ||
                                        $banner['bnr_category'] == 'app_complaints') {
                                     ?>
                                     <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                               <div id="newupload">
                                                    <input type="hidden" id="x10" name="x1[]" />
                                                    <input type="hidden" id="y10" name="y1[]" />
                                                    <input type="hidden" id="x20" name="x2[]" />
                                                    <input type="hidden" id="y20" name="y2[]" />
                                                    <input type="hidden" id="w0" name="w[]" />
                                                    <input type="hidden" id="h0" name="h[]" />
                                                    <input type="file" class="form-control" name="banner" id="image_file0" 
                                                           onchange="fileSelectHandler('0', 800, 400, true)" />
                                                    <img id="preview0" class="preview"/>
                                               </div>
                                               <span class="help-inline">Upload approximate this dimension 800(W) × 400(H)</span>
                                               <div id="altTag">
                           <input placeholder="Alt tag for image" type="text" name="banner[bnr_imagealt]" id="bImgAlt" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image"class="form-control col-md-9 col-xs-12"/>
                  </div>
                                          </div>
                                     </div>
                                <?php } else if($banner['bnr_category']=='logistics') {?>
                                     <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                               <div id="newupload">
                                                    <input type="hidden" id="x10" name="x1[]" />
                                                    <input type="hidden" id="y10" name="y1[]" />
                                                    <input type="hidden" id="x20" name="x2[]" />
                                                    <input type="hidden" id="y20" name="y2[]" />
                                                    <input type="hidden" id="w0" name="w[]" />
                                                    <input type="hidden" id="h0" name="h[]" />
                                                    <input type="file" class="form-control" name="banner" id="image_file0" 
                                                           onchange="fileSelectHandler('0', 1920, 633,true)" />
                                                    <img id="preview0" class="preview"/>
                                               </div>
                                               <span class="help-inline">Upload approximate this dimension 1920(W) × 633(H)</span>
                                               <div id="altTag">
                           <input placeholder="Alt tag for image" type="text" name="banner[bnr_imagealt]" id="bImgAlt" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image"class="form-control col-md-9 col-xs-12"/>
                  </div>
                                          </div>
                                     </div>
                                <?php } else{ ?>
                                   <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                               <div id="newupload">
                                                    <input type="hidden" id="x10" name="x1[]" />
                                                    <input type="hidden" id="y10" name="y1[]" />
                                                    <input type="hidden" id="x20" name="x2[]" />
                                                    <input type="hidden" id="y20" name="y2[]" />
                                                    <input type="hidden" id="w0" name="w[]" />
                                                    <input type="hidden" id="h0" name="h[]" />
                                                    <input type="file" class="form-control" name="banner" id="image_file0" 
                                                           onchange="fileSelectHandler('0', 1920, 468,true)" />
                                                    <img id="preview0" class="preview"/>
                                               </div>
                                               <span class="help-inline">Upload approximate this dimension 1920(W) × 468(H)</span>
                                               <div id="altTag">
                           <input placeholder="Alt tag for image" type="text" name="banner[bnr_imagealt]" id="bImgAlt" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image"class="form-control col-md-9 col-xs-12"/>
                  </div>
                                          </div>
                                     </div>   
                                <?php }?>
                         </div>
                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>

<div class="D800x400 hidden">
     <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
               <div id="newupload">
                    <input type="hidden" id="x10" name="x1[]" />
                    <input type="hidden" id="y10" name="y1[]" />
                    <input type="hidden" id="x20" name="x2[]" />
                    <input type="hidden" id="y20" name="y2[]" />
                    <input type="hidden" id="w0" name="w[]" />
                    <input type="hidden" id="h0" name="h[]" />
                    <input type="file" class="form-control" name="banner" id="image_file0" 
                           onchange="fileSelectHandler('0', 800, 400, true)" />
                    <img id="preview0" class="preview"/>
               </div>
               <span class="help-inline">Upload approximate this dimension 800(W) × 400(H)</span>
               <div id="altTag">
                           <input placeholder="Alt tag for image" type="text" name="banner[bnr_imagealt]" id="bImgAlt" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image"class="form-control col-md-9 col-xs-12"/>
                  </div>
          </div>
     </div>
</div>

<div class="D1920x468 hidden">
     <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
               <div id="newupload">
                    <input type="hidden" id="x10" name="x1[]" />
                    <input type="hidden" id="y10" name="y1[]" />
                    <input type="hidden" id="x20" name="x2[]" />
                    <input type="hidden" id="y20" name="y2[]" />
                    <input type="hidden" id="w0" name="w[]" />
                    <input type="hidden" id="h0" name="h[]" />
                    <input type="file" class="form-control" name="banner" id="image_file0" 
                           onchange="fileSelectHandler('0', 1920, 468,true)" />
                    <img id="preview0" class="preview"/>
               </div>
               <span class="help-inline">Upload approximate this dimension 1920(W) × 468(H)</span>
               <div id="altTag">
                           <input placeholder="Alt tag for image" type="text" name="banner[bnr_imagealt]" id="bImgAlt" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image"class="form-control col-md-9 col-xs-12"/>
                  </div>
          </div>
     </div>
</div>
<div class="D1920x633 hidden">
     <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
               <div id="newupload">
                    <input type="hidden" id="x10" name="x1[]" />
                    <input type="hidden" id="y10" name="y1[]" />
                    <input type="hidden" id="x20" name="x2[]" />
                    <input type="hidden" id="y20" name="y2[]" />
                    <input type="hidden" id="w0" name="w[]" />
                    <input type="hidden" id="h0" name="h[]" />
                    <input type="file" class="form-control" name="banner" id="image_file0" 
                           onchange="fileSelectHandler('0', 1920, 633,true)" />
                    <img id="preview0" class="preview"/>
               </div>
               <span class="help-inline">Upload approximate this dimension 1920(W) × 633(H)</span>
               <div id="altTag">
                           <input placeholder="Alt tag for image" type="text" name="banner[bnr_imagealt]" id="bImgAlt" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image"class="form-control col-md-9 col-xs-12"/>
                  </div>
          </div>
     </div>
</div>
  
<script>
  $('input[type=file]').change(function(e){

   var image =  $('input[type=file]')[0].files[0].name;
   if(image)
   {
    $('#bImgAlt').attr('data-parsley-required',true);
   }
   else
   {
          $('#bImgAlt').attr('data-parsley-required',false);
   }
});
  </script>

