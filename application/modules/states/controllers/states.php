<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class states extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->page_title = 'States';
            $this->load->model('states_model', 'states');
            $this->load->model('country/country_model', 'country');
       }

       public function index() {
            $this->lock_in();
            $data['countries'] = $this->states->get();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       public function add() {
            $this->lock_in();
            if (!empty($_POST)) {
                 if ($this->states->newStates($_POST)) {
                      $this->session->set_flashdata('app_success', 'Row successfully updated!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't updated row!");
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $data['country'] = $this->country->get();
                 $this->render_page(strtolower(__CLASS__) . '/add',$data);
            }
       }

       public function view($id) {
            $this->lock_in();
            $data['states'] = $this->states->get($id);
            $data['country'] = $this->country->get();
            $this->render_page(strtolower(__CLASS__) . '/view', $data);
       }

       public function update() {
            $this->lock_in();
            if ($this->states->updateStates($this->input->post())) {
                 $this->session->set_flashdata('app_success', 'Successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', "Can't update data!");
            }
            redirect(strtolower(__CLASS__));
       }

       function deleteStates($id = '') {
            if ($this->states->deleteStates($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Row successfully deleted'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete row"));
            }
       }
       
       function getStatesByCountry() {
            $cntryId = isset($_POST['id']) ? $_POST['id'] : '';
            echo json_encode($this->states->getStatesByCountry($cntryId));die();
       }
    }
  