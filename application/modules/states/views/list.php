<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>States</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>State Name</th>
                                        <th>County Name</th>
                                        <th>County Code</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $countries as $key => $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url('states/view/' . $value['stt_id']);?>">
                                               <td class="trVOE"><?php echo $value['stt_name'];?></td>
                                               <td class="trVOE"><?php echo $value['ctr_name'];?></td>
                                               <td class="trVOE"><?php echo $value['ctr_country_code'];?></td>
                                               <?php if (check_permission($controller, 'delete')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url('states/deleteStates/' . $value['stt_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>