<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>New State</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("states/add", array('id' => "frmNewsEvents", 'class' => "form-horizontal",'data-parsley-validate'=>"true"))?>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required type="text" data-parsley-required-message="Enter state name" name="stt_name" id="stt_name" class="form-control col-md-7 col-xs-12"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select name="stt_country_id" id="stt_country_id" class="form-control col-md-7 col-xs-12">
                                        <?php foreach ((array) $country as $key => $value) {?>
                                               <option value="<?php echo $value['ctr_id'];?>"><?php echo $value['ctr_name'];?></option>
                                          <?php }?>
                                   </select>
                              </div>
                         </div>
                         
                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>