<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class states_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->tbl_states = TABLE_PREFIX . 'states';
            $this->tbl_countries = TABLE_PREFIX . 'countries';
       }

       function get($id = '') {
            if (!empty($id)) {
                 $this->db->get_where($this->tbl_states, array($this->tbl_states . '.stt_id' => $id))->row_array();
                 return $this->db->select($this->tbl_states . '.*,' . $this->tbl_countries . '.*')
                                 ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_states . '.stt_country_id', 'LEFT')
                                 ->get_where($this->tbl_states)->row_array();
            }
            return $this->db->select($this->tbl_states . '.*,' . $this->tbl_countries . '.*')
                            ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_states . '.stt_country_id', 'LEFT')
                            ->get_where($this->tbl_states)->result_array();
       }

       function newStates($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_states, $data);
                 return true;
            } else {
                 return false;
            }
       }

       function updateStates($data) {
            if ($data) {
                 $id = isset($data['stt_id']) ? $data['stt_id'] : '';
                 unset($data['stt_id']);
                 $this->db->where('stt_id', $id);
                 $this->db->update($this->tbl_states, $data);
                 return true;
            } else {
                 return FALSE;
            }
       }

       function deleteStates($id) {
            if (!empty($id)) {
                 $this->db->where('stt_id', $id);
                 $this->db->delete($this->tbl_states);
                 return true;
            } else {
                 return false;
            }
       }

       function getStatesByCountry($cntryId = '') {
            
            return $this->db->select('stt_id AS col_id, stt_name AS col_title')->order_by('col_title')
                    ->get_where($this->tbl_states, array('stt_country_id' => $cntryId))->result_array();
       }

  }
  