<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class notification extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->load->model('notification_model', 'notification');
       }

       public function pushnotification() {
            if (!empty($_POST)) {
                 
                 if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
                    /* Category image */
                    $newFileName = uniqid(rand(1, 8888888), true) . rand(1, 9999999) . $_FILES['image']['name'];
                    $config['upload_path'] = FILE_UPLOAD_PATH . 'notification/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['file_name'] = $newFileName;
                    $this->load->library('upload', $config);
   
                    if (!$this->upload->do_upload('image')) {
                         array('error' => $this->upload->display_errors());
                    } else {
                         $data = array('upload_data' => $this->upload->data());
                         crop($this->upload->data(), $this->input->post());
                    }
                    $_POST['pushnify']['pnm_image'] = isset($data['upload_data']['file_name']) ? $data['upload_data']['file_name'] : '';
                 }
                 $this->notification->newPushNotification($this->input->post());
                 $this->session->set_flashdata('app_success', 'Sent Pushnotofication ');
                 redirect('notification/pushnotification');
            }
            //$data['receivers'] = $this->notification->getReceivers();
            $this->render_page(strtolower(__CLASS__) . '/pushnotification');
       }

  }
  