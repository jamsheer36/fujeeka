<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class mailbox_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_rfq = TABLE_PREFIX . 'rfq';
            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_groups = TABLE_PREFIX . 'groups';
            $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
            $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
            $this->tbl_mailboox_master = TABLE_PREFIX . 'mailboox_master';
            $this->tbl_mailboox_attachments = TABLE_PREFIX . 'mailboox_attachments';
            $this->tbl_products_order_master = TABLE_PREFIX . 'products_order_master';
       }

       function read($id = '') {
            if (!empty($id)) {

                 generate_log(array(
                     'log_title' => 'Read mail',
                     'log_desc' => 'User read the mail',
                     'log_controller' => 'mailbox',
                     'log_action' => 'R',
                     'log_ref_id' => $id,
                     'log_added_by' => $this->uid
                 ));
                 $mail = $this->db->select($this->tbl_mailboox_master . '.*,' . 'frommail.usr_first_name AS frm_usr_first_name, '
                                         . 'tomail.usr_first_name AS to_usr_first_name')
                                 ->join($this->tbl_users . ' frommail', 'frommail.usr_id = ' . $this->tbl_mailboox_master . '.mms_from')
                                 ->join($this->tbl_users . ' tomail', 'tomail.usr_id = ' . $this->tbl_mailboox_master . '.mms_to')
                                 ->where(array($this->tbl_mailboox_master . '.mms_id' => $id))->get($this->tbl_mailboox_master)->row_array();

                 if (!empty($mail)) {
                      $mail['attachments'] = $this->db->get_where($this->tbl_mailboox_attachments, array('mat_master_id' => $mail['mms_id']))->result_array();
                 }

                 if (!empty($mail)) {
                      $mail['replay'] = $this->db->order_by('mms_id')->get_where($this->tbl_mailboox_master, array('mms_parent' => $mail['mms_id']))
                              ->result_array();
                 }

                 if (isset($mail['replay']) && !empty($mail['replay'])) {
                      foreach ($mail['replay'] as $key => $value) {
                           if ($value['mms_to'] == $this->uid) {
                                $this->db->where('mms_id', $value['mms_id']);
                                $this->db->update($this->tbl_mailboox_master, array('mms_last_viewd_on' => date('Y-m-d H:i:s')));

                                generate_log(array(
                                    'log_title' => 'Read mail',
                                    'log_desc' => 'Recipient read the mail',
                                    'log_controller' => 'mailbox',
                                    'log_action' => 'R',
                                    'log_ref_id' => $value['mms_id'],
                                    'log_added_by' => $this->uid
                                ));
                           }
                      }
                 }

                 if (isset($mail['mms_to']) && ($mail['mms_to'] == $this->uid)) {
                      $this->db->where('mms_id', $id);
                      $this->db->update($this->tbl_mailboox_master, array('mms_last_viewd_on' => date('Y-m-d H:i:s')));

                      generate_log(array(
                          'log_title' => 'Read mail',
                          'log_desc' => 'Recipient read the mail',
                          'log_controller' => 'mailbox',
                          'log_action' => 'R',
                          'log_ref_id' => $id,
                          'log_added_by' => $this->uid
                      ));
                 }
                 return $mail;
            } else {
                 $qryInbox = "(SELECT " . $this->tbl_mailboox_master . ".*, frommail.usr_first_name AS frm_usr_first_name, tomail.usr_first_name AS to_usr_first_name 
                                   FROM (" . $this->tbl_mailboox_master . ") 
                                   JOIN " . $this->tbl_users . " frommail ON frommail.usr_id = " . $this->tbl_mailboox_master . ".mms_from 
                                   JOIN " . $this->tbl_users . " tomail ON tomail.usr_id = " . $this->tbl_mailboox_master . ".mms_to 
                                   WHERE " . $this->tbl_mailboox_master . ".mms_to = " . $this->uid . " AND " . $this->tbl_mailboox_master . ".mms_status = 1
                                   AND " . $this->tbl_mailboox_master . ".mms_parent = 0 order by " . $this->tbl_mailboox_master . ".mms_id DESC)
                                   UNION 
                                   (SELECT " . $this->tbl_mailboox_master . ".*, frommail.usr_first_name AS frm_usr_first_name, tomail.usr_first_name AS to_usr_first_name 
                                   FROM (cpnl_mailboox_master) 
                                   JOIN " . $this->tbl_users . " frommail ON frommail.usr_id = " . $this->tbl_mailboox_master . ".mms_from 
                                   JOIN " . $this->tbl_users . " tomail ON tomail.usr_id = " . $this->tbl_mailboox_master . ".mms_to 
                                   WHERE " . $this->tbl_mailboox_master . ".mms_to = " . $this->uid . " AND " . $this->tbl_mailboox_master . ".mms_status = 1
                                   AND " . $this->tbl_mailboox_master . ".mms_parent > 0 order by " . $this->tbl_mailboox_master . ".mms_id DESC LIMIT 1)";
                 return $this->db->query($qryInbox)->result_array();
            }
       }

       function send($fromID = '', $toID, $subject, $message, $parent = 0) {
            $fromID = empty($fromID) ? $this->uid : $fromID;
            if (!empty($fromID) && !empty($toID) && !empty($message)) {
                 // If only one receipent.
                 $send['mms_to'] = $toID;
                 $send['mms_from'] = $fromID;
                 $send['mms_subject'] = $subject;
                 $send['mms_message'] = $message;
                 $send['mms_parent'] = $parent;
                 $this->db->insert($this->tbl_mailboox_master, $send);
                 $mailId = $this->db->insert_id();

                 generate_log(array(
                     'log_title' => 'Mail reply',
                     'log_desc' => $fromID . '-' . $toID,
                     'log_controller' => 'mailbox',
                     'log_action' => 'C',
                     'log_ref_id' => $mailId,
                     'log_added_by' => $this->uid
                 ));
                 return $mailId;
            }
       }

       /**
        * Function to get all recipient with associated logged user.
        * @return array
        * Author : JK
        */
       function recipient($sqry = '') {
            if (is_root_user()) { // Is root user
                 if (!empty($sqry)) {
                      $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
                 }
                 return $this->db->select($this->tbl_users . '.usr_id, ' . $this->tbl_users . '.usr_email')
                                 ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_users . '.usr_supplier', 'LEFT')
                                 ->where($this->tbl_users . '.usr_id !=' . '1')->get($this->tbl_users)->result_array();
            } else if ($this->usr_grp == 'SP') { // if supplier logged
                 // staff under supplier
                 if (!empty($sqry)) {
                      $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
                 }
                 $this->db->where($this->tbl_users . '.usr_supplier', $this->suplr);
                 $this->db->where($this->tbl_users . '.usr_id !=', 1);
                 $this->db->where($this->tbl_users . '.usr_id !=', $this->uid);

                 $staff = $this->db->select($this->tbl_users . '.usr_id, ' . $this->tbl_users . '.usr_email')
                                 ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                                 ->where(array($this->tbl_groups . '.id' => 3, $this->tbl_users . '.usr_active' => 1,
                                     $this->tbl_users . '.usr_id !=' => $this->uid))->get($this->tbl_users)->result_array();
                 //rfq
                 if (!empty($sqry)) {
                      $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
                 }
                 $rfq = $this->db->select($this->tbl_users . '.usr_id,' . $this->tbl_users . '.usr_email')
                                 ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_rfq . '.rfq_user', 'LEFT')
                                 ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->where($this->tbl_rfq . '.rfq_supplier', $this->uid)->or_where($this->tbl_rfq . '.rfq_supplier = ', 0)
                                 ->where(array($this->tbl_users . '.usr_active' => 1,
                                     $this->tbl_users . '.usr_id !=' => $this->uid))->get($this->tbl_rfq)->result_array();
                 $rfq = array_unique($rfq, SORT_REGULAR);
                 //inquiry
                 if (!empty($sqry)) {
                      $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
                 }
                 $inquiry = $this->db->select($this->tbl_users . '.usr_id,' . $this->tbl_users . '.usr_email')
                                 ->join($this->tbl_products_order_master, $this->tbl_products_order_master . '.ord_buyer = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->where(array($this->tbl_products_order_master . '.ord_supplier' => $this->suplr,
                                     $this->tbl_users . '.usr_active' => 1, $this->tbl_users . '.usr_id !=' => $this->uid))
                                 ->get($this->tbl_users)->result_array();

                 //admin
                 $admin = $this->db->select($this->tbl_users . '.usr_id,' . $this->tbl_users . '.usr_email')
                                 ->where($this->tbl_users . '.usr_id', 1)->get($this->tbl_users)->result_array();

                 $inquiry = array_unique($inquiry, SORT_REGULAR);

                 $recepient = array_merge($staff, $rfq, $inquiry, $admin);
                 asort($recepient);
                 return $recepient;
            } else if ($this->usr_grp == 'BY') { // if buyer
                 //admin
                 $admin = $this->db->select($this->tbl_users . '.usr_id,' . $this->tbl_users . '.usr_email')
                                 ->where($this->tbl_users . '.usr_id', 1)->get($this->tbl_users)->result_array();
                 if (!empty($sqry)) {
                      $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
                 }

                 $suppliers = $this->db->select($this->tbl_users . '.usr_id, ' . $this->tbl_users . '.usr_email')
                                 ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                                 ->where(array($this->tbl_groups . '.id' => 2, $this->tbl_users . '.usr_active' => 1, $this->tbl_users . '.usr_id !=' => $this->uid))
                                 ->get($this->tbl_users)->result_array();

                 $recepient = array_merge($suppliers, $admin);
                 return $recepient;
            } else { // if staff
                 //admin
                 $admin = $this->db->select($this->tbl_users . '.usr_id,' . $this->tbl_users . '.usr_email')
                                 ->where($this->tbl_users . '.usr_id', 1)->get($this->tbl_users)->result_array();
                 if (!empty($sqry)) {
                      $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
                 }
                 $this->db->where($this->tbl_users . '.usr_supplier', $this->suplr);
                 $this->db->where($this->tbl_users . '.usr_id !=', $this->uid);
                 $this->db->where($this->tbl_users . '.usr_active', 1);
                 $this->db->where('(' . $this->tbl_groups . '.id = 3 OR ' . $this->tbl_groups . '.id = 2)');
                 $colgs = $this->db->select($this->tbl_users . '.usr_id, ' . $this->tbl_users . '.usr_email')
                                 ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                                 ->get($this->tbl_users)->result_array();

                 $recepient = array_merge($colgs, $admin);
                 return $recepient;
            }
       }

       function addAttachments($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_mailboox_attachments, $data);
                 return true;
            }
            return false;
       }

       function outbox() {
            return $this->db->select($this->tbl_mailboox_master . '.*,' . 'frommail.usr_first_name AS frm_usr_first_name, '
                                    . 'tomail.usr_first_name AS to_usr_first_name')
                            ->join($this->tbl_users . ' frommail', 'frommail.usr_id = ' . $this->tbl_mailboox_master . '.mms_from')
                            ->join($this->tbl_users . ' tomail', 'tomail.usr_id = ' . $this->tbl_mailboox_master . '.mms_to')
                            ->where(array($this->tbl_mailboox_master . '.mms_from' => $this->uid,
                                $this->tbl_mailboox_master . '.mms_status' => 1))
                            ->get($this->tbl_mailboox_master)->result_array();
       }

       function delete($id) {
            if (!empty($id)) {
                 $this->db->where('mms_id', $id);
                 $this->db->update($this->tbl_mailboox_master, array('mms_status' => 0));
                 return true;
            }
            return false;
       }

       function unread() {
            return $this->db->select($this->tbl_mailboox_master . '.*,' . 'frommail.usr_first_name AS frm_usr_first_name, '
                                    . 'tomail.usr_first_name AS to_usr_first_name')
                            ->join($this->tbl_users . ' frommail', 'frommail.usr_id = ' . $this->tbl_mailboox_master . '.mms_from')
                            ->join($this->tbl_users . ' tomail', 'tomail.usr_id = ' . $this->tbl_mailboox_master . '.mms_to')
                            ->where(array($this->tbl_mailboox_master . '.mms_to' => $this->uid,
                                $this->tbl_mailboox_master . '.mms_status' => 1, 'mms_last_viewd_on' => NULL))
                            ->get($this->tbl_mailboox_master)->result_array();
       }

       function trash() {
            return $this->db->select($this->tbl_mailboox_master . '.*,' . 'frommail.usr_first_name AS frm_usr_first_name, '
                                    . 'tomail.usr_first_name AS to_usr_first_name')
                            ->join($this->tbl_users . ' frommail', 'frommail.usr_id = ' . $this->tbl_mailboox_master . '.mms_from')
                            ->join($this->tbl_users . ' tomail', 'tomail.usr_id = ' . $this->tbl_mailboox_master . '.mms_to')
                            ->where(array($this->tbl_mailboox_master . '.mms_to' => $this->uid,
                                $this->tbl_mailboox_master . '.mms_status' => 0))
                            ->get($this->tbl_mailboox_master)->result_array();
       }

  }
  