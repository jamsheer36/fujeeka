<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class mailbox extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Mail box';
            $this->load->model('mailbox_model', 'mailbox');
            $this->load->library('upload');
            $this->lock_in();
            if (!$this->mail_verified) {
                 $this->session->set_flashdata('app_error', 'You should verify your email first');
                 redirect('dashboard');
            }
       }

       function index() {
            $data['recipient'] = $this->mailbox->recipient();
            $data['mail'] = $this->mailbox->read();
            $html = $this->load->view('maillist', $data, true);
            //die(json_encode(array('status' => 'success', 'msg' => '', 'html' => $html)));
            $this->render_page(strtolower(__CLASS__) . '/inbox', $data);
       }

       function inbox() {
            if (isset($_POST['type']) && $_POST['type'] == 'ajax') {
                 $data['mail'] = $this->mailbox->read();
                 $html = $this->load->view('maillist', $data, true);
                 die(json_encode(array('status' => 'success', 'msg' => '', 'html' => $html)));
            } else {
                 $data['recipient'] = $this->mailbox->recipient();
                 $data['mail'] = $this->mailbox->read();
                 $this->render_page(strtolower(__CLASS__) . '/inbox', $data);
            }
       }

       function outbox() {
            if (isset($_POST['type']) && $_POST['type'] == 'ajax') {
                 $data['mail'] = $this->mailbox->outbox();
                 $html = $this->load->view('maillist', $data, true);
                 die(json_encode(array('status' => 'success', 'msg' => '', 'html' => $html)));
            } else {
                 //$data['recipient'] = $this->mailbox->recipient();
                 $data['mail'] = $this->mailbox->outbox();
                 $this->render_page(strtolower(__CLASS__) . '/inbox', $data);
            }
       }

       function sent() {
            if ($mailMaster = $this->mailbox->send('', $_POST['mms_to'], $_POST['mms_subject'], $_POST['mms_message'])) {
                 $fileCount = isset($_FILES['attachments']['name']) ? count($_FILES['attachments']['name']) : 0;
                 if ($fileCount > 0) {
                      for ($i = 0; $i < $fileCount; $i++) {
                           $newFileName = rand(9999999, 0) . $_FILES['attachments']['name'][$i];
                           $config['upload_path'] = FILE_UPLOAD_PATH . 'mail_attachments/';
                           $config['allowed_types'] = 'gif|jpg|png';
                           $config['file_name'] = $newFileName;
                           $this->upload->initialize($config);

                           $_FILES['tmp_attach']['name'] = $_FILES['attachments']['name'][$i];
                           $_FILES['tmp_attach']['type'] = $_FILES['attachments']['type'][$i];
                           $_FILES['tmp_attach']['tmp_name'] = $_FILES['attachments']['tmp_name'][$i];
                           $_FILES['tmp_attach']['error'] = $_FILES['attachments']['error'][$i];
                           $_FILES['tmp_attach']['size'] = $_FILES['attachments']['size'][$i];

                           if (!$this->upload->do_upload('tmp_attach')) {
                                $uploadDataEr = $this->upload->display_errors();
                           } else {
                                $uploadData = $this->upload->data();
                                $file = isset($uploadData['file_name']) ? $uploadData['file_name'] : '';
                                $this->mailbox->addAttachments(array('mat_master_id' => $mailMaster, 'mat_attachment' => $file));
                           }
                      }
                 }
                 die(json_encode(array('status' => 'success', 'msg' => 'Message successfully sent')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't sent message")));
            }
       }

       function reply() {
            if ($mailMaster = $this->mailbox->send('', $_POST['mms_to'], $_POST['mms_subject'], $_POST['mms_message'], $_POST['mms_parent'])) {
                 $fileCount = isset($_FILES['attachments']['name']) ? count($_FILES['attachments']['name']) : 0;
                 if ($fileCount > 0) {
                      for ($i = 0; $i < $fileCount; $i++) {
                           $newFileName = rand(9999999, 0) . $_FILES['attachments']['name'][$i];
                           $config['upload_path'] = FILE_UPLOAD_PATH . 'mail_attachments/';
                           $config['allowed_types'] = 'gif|jpg|png';
                           $config['file_name'] = $newFileName;
                           $this->upload->initialize($config);

                           $_FILES['tmp_attach']['name'] = $_FILES['attachments']['name'][$i];
                           $_FILES['tmp_attach']['type'] = $_FILES['attachments']['type'][$i];
                           $_FILES['tmp_attach']['tmp_name'] = $_FILES['attachments']['tmp_name'][$i];
                           $_FILES['tmp_attach']['error'] = $_FILES['attachments']['error'][$i];
                           $_FILES['tmp_attach']['size'] = $_FILES['attachments']['size'][$i];

                           if (!$this->upload->do_upload('tmp_attach')) {
                                $uploadDataEr = $this->upload->display_errors();
                           } else {
                                $uploadData = $this->upload->data();
                                $file = isset($uploadData['file_name']) ? $uploadData['file_name'] : '';
                                $this->mailbox->addAttachments(array('mat_master_id' => $mailMaster, 'mat_attachment' => $file));
                           }
                      }
                 }
                 $newMail = '<div class="mail_list"><div><h3><small>'.date('j M Y h:i:s A').'</small></h3>
                             <p></p><p>'.$_POST['mms_message'].'</p> <p></p></div></div>';
                 die(json_encode(array('status' => 'success', 'msg' => 'Message successfully sent', 'newMail' => $newMail)));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't sent message")));
            }
       }

       public function read($id) {
            $data['recipient'] = $this->mailbox->recipient();
            $id = encryptor($id, 'D');
            $data['mail'] = $this->mailbox->read($id);
            $this->render_page(strtolower(__CLASS__) . '/read', $data);
       }

       function deleteMail($id) {
            $id = encryptor($id, 'D');
            $this->mailbox->delete($id);
            die(json_encode(array('status' => 'success', 'msg' => 'Message successfully deleted')));
       }

       function unread() {
            if (isset($_POST['type']) && $_POST['type'] == 'ajax') {
                 $data['mail'] = $this->mailbox->unread();
                 $html = $this->load->view('maillist', $data, true);
                 die(json_encode(array('status' => 'success', 'msg' => '', 'html' => $html)));
            } else {
                 $data['recipient'] = $this->mailbox->recipient();
                 $data['mail'] = $this->mailbox->unread();
                 $this->render_page(strtolower(__CLASS__) . '/inbox', $data);
            }
       }

       function trash() {
            if (isset($_POST['type']) && $_POST['type'] == 'ajax') {
                 $data['mail'] = $this->mailbox->trash();
                 $html = $this->load->view('maillist', $data, true);
                 die(json_encode(array('status' => 'success', 'msg' => '', 'html' => $html)));
            } else {
                 $data['recipient'] = $this->mailbox->recipient();
                 $data['mail'] = $this->mailbox->trash();
                 $this->render_page(strtolower(__CLASS__) . '/inbox', $data);
            }
       }

       function filterRecipient() {
            $recipient = $this->mailbox->recipient($this->input->post('q'));
            $recipient = array_values($recipient);
            die(json_encode($recipient));
       }

  }
  