<?php
  if (!empty($mail)) {
       foreach ($mail as $key => $value) {
            ?>
            <a href="<?php echo site_url('mailbox/read/' . encryptor($value['mms_id']));?>">
                 <div class="mail_list">
                      <div class="">
                           <h3>To&nbsp;&nbsp;: <?php echo $value['to_usr_first_name']?><small><?php echo get_timeago($value['mms_added_on']);?></small></h3>
                           <h3>From&nbsp;&nbsp;: <?php echo $value['frm_usr_first_name']?></h3>
                           <p>Subject&nbsp;: <?php echo get_snippet($value['mms_subject'], 10);?></p>
                      </div>
                 </div>
            </a>
            <?php
       }
  } else {
       ?>
       <a href="javascript:;">
            <div class="mail_list" style="text-align: center;border: none;">
                 <h3>You have no mail :(</h3>
            </div>
       </a>
  <?php }
?>