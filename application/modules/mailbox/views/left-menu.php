<div class="col-sm-3 mail_list_column">
                                        <!--<a href="<?php echo site_url('payroll/compose_leave');?>" id="compose" class="btn btn-sm btn-success btn-block" type="button">COMPOSE LEAVE</a>-->
     <a href="javascript:;" class="btnMailCompose" id="compose">
          <div class="mail_list">
               <div style="margin: 10px 10px;">
                    <h3> <i class="fa fa-paper-plane"></i> Compose <small></small></h3>
               </div>
          </div>
     </a>
     <a href="javascript:;" class="btnInbox" data-url="<?php echo site_url('mailbox/inbox');?>">
          <div class="mail_list">
               <div style="margin: 10px 10px;">
                    <h3> <i class="fa fa-inbox"></i> Inbox <small></small></h3>
               </div>
          </div>
     </a>
     <a href="javascript:;" class="btnOutBox" data-url="<?php echo site_url('mailbox/outbox'); ?>">
          <div class="mail_list">
               <div style="margin: 10px 10px;">
                    <h3> <i class="fa fa-share"></i> Outbox <small></small></h3>
               </div>
          </div>
     </a>
     <a href="javascript:;" class="btnUnread" data-url="<?php echo site_url('mailbox/unread'); ?>">
          <div class="mail_list">
               <div style="margin: 10px 10px;">
                    <h3> <i class="fa fa-envelope"></i> Unread <small></small></h3>
               </div>
          </div>
     </a>
     <a href="javascript:;" class="btnTrash" data-url="<?php echo site_url('mailbox/trash'); ?>">
          <div class="mail_list">
               <div style="margin: 10px 10px;">
                    <h3> <i class="fa fa-trash"></i> Trash <small></small></h3>
               </div>
          </div>
     </a>
</div>