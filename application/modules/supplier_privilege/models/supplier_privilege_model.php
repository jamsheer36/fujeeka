<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class supplier_privilege_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->tbl_user = TABLE_PREFIX . 'users';
            $this->tbl_groups = TABLE_PREFIX . 'groups';
            $this->tbl_user_access = TABLE_PREFIX . 'user_access';
            $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
            $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
       }

       function getAllUserGroups() {

            $this->db->where($this->tbl_user . '.usr_id !=', 1);
            return $this->db->select(
                                    $this->tbl_user . '.*, ' .
                                    $this->tbl_users_groups . '.group_id as group_id, ' .
                                    $this->tbl_groups . '.name as group_name, ' .
                                    $this->tbl_groups . '.description as group_desc, ' . $this->tbl_supplier_master . '.*'
                            )
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_user . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_user . '.usr_supplier', 'LEFT')
                            ->where($this->tbl_groups . '.id', 2)
                            ->get($this->tbl_user)->result_array();
       }

       function addUserPermission($data) {
            
            if (!empty($data)) {
                 if ($this->getUserPermission($data['cua_group_id'])) {
                      $this->db->where('cua_group_id', $data['cua_group_id'])->delete($this->tbl_user_access);
                 }

                 $data['cua_access'] = !empty($data['cua_access']) ? serialize($data['cua_access']) : serialize(array());
                 $this->db->insert($this->tbl_user_access, $data);
            } else {
                 return false;
            }
       }

       function getUserPermission($id) {
            if (!empty($id)) {
                 return $this->db->select('*')->where('cua_group_id', $id)->get($this->tbl_user_access)->row_array();
            } else {
                 return false;
            }
       }

  }
  