<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Supplier Privilege</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/setPermission", array('id' => "frmPermission", 'class' => "form-horizontal form-label-left up-check", 'data-parsley-validate' => "true"))?>
                         <div class="form-group">
                              <label for="usr_showroom" class="control-label col-md-3 col-sm-3 col-xs-12">Supplier</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php if (!empty($users)) {?>
                                          <select required data-parsley-required-message="Select a supplier" name="cua_group_id" class="select2_group form-control cmbUser" data-url="<?php echo site_url('user_permission/getPermission');?>">
                                               <option value="">Select Supplier</option>
                                               <?php foreach ($users as $key => $value) {?>
                                                    <option value="<?php echo $value['usr_id'];?>">
                                                         <?php
                                                         $companyName = !empty($value['usr_username']) ? $value['usr_username'] : $value['usr_first_name'];
                                                         echo $value['supm_name_en'] . ' ' . $companyName;
                                                         ?>
                                                    </option>
                                               <?php }?>
                                          </select>
                                     <?php }?>
                              </div>
                         </div>

                         <?php
                           if ($modules) {
                                foreach ($modules as $module => $permissions) {
                                     ?>
                                     <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php if($module=="order"){echo "Product Inquiry";} else{ echo clean_text($module);}?></label>
                                          <div class="col-md-7 col-sm-7 col-xs-12">
                                               <?php if (!empty($permissions)) {?>
                                                    <?php foreach ($permissions as $controller => $val) {?>
                                                         <input class="chkPermissions" id="<?php echo $module . '-' . $controller?>" type="checkbox" value="<?php echo $controller;?>" 
                                                                name="cua_access[<?php echo $module;?>][]"/>
                                                                <?php echo $val;?>
                                                         
                                                    <?php }?>
                                               <?php }?>
                                          </div>
                                     </div>
                                     <div class="clearfix"></div>
                                     <?php
                                }
                           }
                         ?>

                         <!-- <div class="ln_solid"></div> -->
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                      <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>