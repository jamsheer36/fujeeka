<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>General Settings</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("settings/insert", array('id' => "frmSettings", 'class' => "form-horizontal form-label-left"))?>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Total Keyword For Recommendation</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                          name="settings[total_keyword_for_recommendation]" id="mod_title" placeholder="Total Keyword For Recommendation"
                                          value="<?php echo get_settings_by_key('total_keyword_for_recommendation');?>"/>
                              </div>
                         </div>   
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">MOQ Limit</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                          name="settings[moq_limit]" id="mod_title" placeholder="MOQ Limit"
                                          value="<?php echo get_settings_by_key('moq_limit');?>"/>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Wholesale Shops Limit</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                          name="settings[home_wholesale_shops_limit]" id="mod_title" placeholder="Wholesale Shops Limit"
                                          value="<?php echo get_settings_by_key('home_wholesale_shops_limit');?>"/>
                                   <small>Home page wholesale shops slider limit</small>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Admin E-mail</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                          name="settings[admin_email]" id="mod_title" placeholder="Admin E-mail"
                                          value="<?php echo get_settings_by_key('admin_email');?>"/>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div class="panel panel-default">
                                        <div class="panel-heading">Suppliers image upload options</div>
                                        <div class="panel-body">
                                             <label class="control-label col-md-5 col-sm-3 col-xs-12">Maximum shop image</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[sup_shop_img_limit]" id="mod_title" placeholder="Maximum shop image"
                                                         value="<?php echo get_settings_by_key('sup_shop_img_limit');?>"/>
                                                  <small>Maximum shop image</small>
                                             </div>
                                             
                                             <label class="control-label col-md-5 col-sm-3 col-xs-12">Maximum home banners</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                                         name="settings[sup_home_bnr_img_limit]" id="mod_title" placeholder="Maximum home banners"
                                                         value="<?php echo get_settings_by_key('sup_home_bnr_img_limit');?>"/>
                                                  <small>Maximum home banner</small>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Product image limit</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                          name="settings[prod_img_limit]" id="mod_title" placeholder="Product image limit"
                                          value="<?php echo get_settings_by_key('prod_img_limit');?>"/>
                                   <small>Products image upload limit</small>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Thumbnail Width</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                          name="settings[thumbnail_width]" id="mod_title" placeholder="Product Thumbnail Width"
                                          value="<?php echo get_settings_by_key('thumbnail_width');?>"/>
                                   <small>Product Thumbnail Width</small>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Thumbnail Height</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input type="text" class="form-control col-md-7 col-xs-12" autocomplete="off"
                                          name="settings[thumbnail_height]" id="mod_title" placeholder="Product Thumbnail Height"
                                          value="<?php echo get_settings_by_key('thumbnail_height');?>"/>
                                   <small>Product Thumbnail Heigh</small>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Page Market</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select  class="form-control col-md-7 col-xs-12" name="settings[home_page_market]" id="mod_title" />
                                        <option value="">Select Market</option>
                                        <?php foreach($markets as $val){ ?>
                                        <option <?php if($val['mar_id'] == get_settings_by_key('home_page_market')) {?> selected <?php } ?> value="<?=$val['mar_id']?>"><?=$val['mar_name']?></option>
                                        <?php } ?>     
                                   </select>
                                   <small>Home Page Market</small>
                              </div>
                         </div>

                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>