<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class Product_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_rfq = TABLE_PREFIX . 'rfq';
            $this->tbl_model = TABLE_PREFIX . 'model';
            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_states = TABLE_PREFIX . 'states';
            $this->tbl_products = TABLE_PREFIX . 'products';
            $this->tbl_category = TABLE_PREFIX . 'category';
            $this->tbl_countries = TABLE_PREFIX . 'countries';
            $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
            $this->tbl_favorite_list = TABLE_PREFIX . 'favorite_list';
            $this->tbl_products_units = TABLE_PREFIX . 'products_units';
            $this->tbl_products_images = TABLE_PREFIX . 'products_images';
            $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
            $this->tbl_products_images = TABLE_PREFIX . 'products_images';
            $this->tbl_rfq_notification = TABLE_PREFIX . 'rfq_notification';
            $this->tbl_products_keyword = TABLE_PREFIX . 'products_keyword';
            $this->tbl_rfq_products_send = TABLE_PREFIX . 'rfq_products_send';
            $this->tbl_products_order = TABLE_PREFIX . 'products_order_master';
            $this->tbl_supplier_followers = TABLE_PREFIX . 'supplier_followers';
            $this->tbl_products_categories = TABLE_PREFIX . 'products_categories';
            $this->tbl_products_specification = TABLE_PREFIX . 'products_specification';
            $this->tbl_products_order_comments = TABLE_PREFIX . 'products_order_comments';
       }

       public function getProduct($id = '',$filter='') {

            if (!is_root_user() && empty($id)) {
                 if (privilege_exists('SP')) { //Check if buyer have supplier privilege.
                      $this->db->where($this->tbl_products . '.prd_supplier', $this->suplr);
                 } else {
                      $this->db->where(array($this->tbl_products . '.prd_added_by' => $this->uid));
                 }
            }

            $this->db->select($this->tbl_products . '.*,' . $this->tbl_supplier_master . '.*,' . $this->tbl_products_units . '.*')
                    ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_products . '.prd_supplier')
                    ->join($this->tbl_products_units, $this->tbl_products_units . '.unt_id = ' . $this->tbl_products . '.prd_unit', 'LEFT');
            if (!empty($id)) {
                 $products = $this->db->where($this->tbl_products . '.prd_id', $id)->get($this->tbl_products)->row_array();
                 if (!empty($products)) {
                      $products['specification'] = $this->db->order_by("psp_id", "asc")->
                                      get_where($this->tbl_products_specification, array('psp_product' => $products['prd_id']))->result_array();
                      $products['keyword'] = $this->db->get_where($this->tbl_products_keyword, array('pkwd_product' => $products['prd_id']))->result_array();
                      if (isset($products['prd_default_image']) && !empty($products['prd_default_image'])) {
                           $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $products['prd_id'], 'pimg_id' => $products['prd_default_image']))->row_array();
                      } else {
                           $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $products['prd_id']))->row_array();
                      }
                      $products['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                      $products['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $products['prd_id']))->result_array();
                      $products['categories'] = $this->db->get_where($this->tbl_products_categories, array('pcat_product' => $products['prd_id']))->row_array();
                      $products['catId'] = explode(',', $this->db->select('GROUP_CONCAT(pcat_category) AS pcat_category')
                                      ->where(array('pcat_product' => $products['prd_id']))
                                      ->get($this->tbl_products_categories)->row()->pcat_category);
                 }
            } else {

           if (isset($filter['date1']) && !empty($filter['date1']) && isset($filter['date2']) && !empty($filter['date2'])) {

                $date1 = $filter['date1']." 00:00:00";
                $date2 = $filter['date2']." 11:59:59";
                 $this->db->where("prd_added_on between '$date1' and '$date2'");
            }


                 $this->db->order_by($this->tbl_products . '.prd_added_on', 'desc');
                 $products = $this->db->get($this->tbl_products)->result_array();
            }
            return $products;
       }

       public function addNewProduct($datas) {

            $datas['product']['prd_number'] = gen_random();
            $datas['product']['prd_added_by'] = $this->uid;
            if (is_root_user()) {
                 $datas['product']['prd_status'] = 1;
            } else {
                 $datas['product']['prd_status'] = 0;
            }

            $datas['product'] = array_filter($datas['product']);
            if ($this->db->insert($this->tbl_products, $datas['product'])) {
                 $lastId = $this->db->insert_id();
                 $categories = isset($datas['categories']) ? $datas['categories'] : array();
                 foreach ((array) $categories as $key => $value) {
                      $cat = array(
                          'pcat_product' => $lastId,
                          'pcat_category' => $value
                      );
                      $this->db->insert($this->tbl_products_categories, $cat);
                 }

                 $keywords = isset($datas['keywords']) ? $datas['keywords'] : 0;

                 if (!empty($keywords)) {
                      $count = isset($datas['keywords']['en']) ? count($datas['keywords']['en']) : 0;
                      for ($i = 0; $i <= $count - 1; $i++) {
                           $keys = array(
                               'pkwd_product' => $lastId,
                               'pkwd_val_en' => isset($keywords['en'][$i]) ? clean_text($keywords['en'][$i], 0) : ''
                           );

                           $this->db->insert($this->tbl_products_keyword, $keys);
                      }
                 }

                 $spcEnKey = isset($datas['specification_key']['en']) ? $datas['specification_key']['en'] : 0;
                 $spcArKey = isset($datas['specification_key']['ar']) ? $datas['specification_key']['ar'] : 0;
                 $spcChKey = isset($datas['specification_key']['ch']) ? $datas['specification_key']['ch'] : 0;
                 $spcEnVal = isset($datas['specification_val']['en']) ? $datas['specification_val']['en'] : 0;
                 $spcArVal = isset($datas['specification_val']['ar']) ? $datas['specification_val']['ar'] : 0;
                 $spcChVal = isset($datas['specification_val']['ch']) ? $datas['specification_val']['ch'] : 0;
                 $count = isset($spcEnKey) ? count($spcEnKey) : 0;
                 for ($i = 0; $i <= $count - 1; $i++) {
                      $specifi = array(
                          'psp_product' => $lastId,
                          'psp_key_en' => clean_text($spcEnKey[$i], 0),
                          'psp_key_ar' => clean_text($spcArKey[$i], 0),
                          'psp_key_ch' => clean_text($spcChKey[$i], 0),
                          'psp_val_en' => clean_text($spcEnVal[$i], 0),
                          'psp_val_ar' => clean_text($spcArVal[$i], 0),
                          'psp_val_ch' => clean_text($spcChVal[$i], 0)
                      );
                      $this->db->insert($this->tbl_products_specification, $specifi);
                 }

                 generate_log(array(
                     'log_title' => 'Create new record',
                     'log_desc' => 'New product added',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => $lastId,
                     'log_added_by' => $this->uid
                 ));
                 return $lastId;
            } else {
                 return false;
            }
       }

       public function addImages($image) {
            if ($this->db->insert($this->tbl_products_images, $image)) {
                 return true;
            } else {
                 return false;
            }
       }

       public function removePrductImage($id) {
            if ($id) {
                 $this->db->where('pimg_id', $id);
                 $image = $this->db->get($this->tbl_products_images)->row_array();
                 if (isset($image['pimg_image']) && !empty($image['pimg_image'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'product/' . $image['pimg_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'product/' . $image['pimg_image']);
                           @unlink(FILE_UPLOAD_PATH . 'product/thumb_' . $image['pimg_image']);
                      }
                      $this->db->where('pimg_id', $id);
                      $this->db->delete($this->tbl_products_images);

                      $this->db->set('prd_default_image',0);
                      $this->db->where('prd_default_image', $id);
                      $this->db->update($this->tbl_products);
                      return true;
                 }
            }
            return false;
       }

       public function updateProduct($datas) {

            if (isset($datas['prd_id']) && !empty($datas['prd_id'])) {

                 $this->db->where('prd_id', $datas['prd_id']);
                 $datas['product']['prd_oem'] = isset($datas['product']['prd_oem']) ? $datas['product']['prd_oem'] : 0;

                 /* Check box unset */

                 if (isset($datas['product']['prd_is_stock_prod'])) {
                      $datas['product']['prd_is_stock_prod'] = $datas['product']['prd_is_stock_prod'];
                      $datas['product']['prd_is_flash_sale'] = isset($datas['product']['prd_is_flash_sale']) ? 1 : 0;
                 } else {
                      $datas['product']['prd_is_stock_prod'] = 0;
                      $datas['product']['prd_stock_qty'] = 0;
                      $datas['product']['prd_is_flash_sale'] = 0;
                 }

                 $datas['product']['prd_oem'] = isset($datas['product']['prd_oem']) ? $datas['product']['prd_oem'] : 0;
                 $datas['product']['prd_hot_selling'] = isset($datas['product']['prd_hot_selling']) ? $datas['product']['prd_hot_selling'] : 0;
                 $datas['product']['prd_is_show_on_profile'] = isset($datas['product']['prd_is_show_on_profile']) ? $datas['product']['prd_is_show_on_profile'] : 0;
                 $datas['product']['prd_is_main_product'] = isset($datas['product']['prd_is_main_product']) ? $datas['product']['prd_is_main_product'] : 0;
                 /* Check box unset */
                 //   $datas['product'] = array_filter($datas['product']);
                 if ($this->db->update($this->tbl_products, $datas['product'])) {
                      $prodId = $datas['prd_id'];

                      $categories = isset($datas['categories']) ? $datas['categories'] : array();
                      $this->db->where('pcat_product', $prodId)->delete($this->tbl_products_categories);

                      foreach ((array) $categories as $key => $value) {
                           $cat = array(
                               'pcat_product' => $prodId,
                               'pcat_category' => $value
                           );
                           $this->db->insert($this->tbl_products_categories, $cat);
                      }

                      $keywords = isset($datas['keywords']) ? $datas['keywords'] : 0;

                      if (!empty($keywords)) {
                           $this->db->where('pkwd_product', $prodId)->delete($this->tbl_products_keyword);
                           $count = isset($datas['keywords']['en']) ? count($datas['keywords']['en']) : 0;
                           for ($i = 0; $i <= $count - 1; $i++) {
                                $keys = array(
                                    'pkwd_product' => $prodId,
                                    'pkwd_val_en' => isset($keywords['en'][$i]) ? clean_text($keywords['en'][$i], 0) : '',
                                );

                                $this->db->insert($this->tbl_products_keyword, $keys);
                           }
                      }

                            if (!empty($altTags = $datas['imgaltTags'])) {

                           foreach ($altTags as $idofImg => $imgAtl ) {
                             $this->db->where('pimg_id', $idofImg)
                                     ->update($this->tbl_products_images, array('pimg_alttag' =>$imgAtl));
                             }
                          }

                      $spcEnKey = isset($datas['specification_key']['en']) ? $datas['specification_key']['en'] : 0;
                      $spcArKey = isset($datas['specification_key']['ar']) ? $datas['specification_key']['ar'] : 0;
                      $spcChKey = isset($datas['specification_key']['ch']) ? $datas['specification_key']['ch'] : 0;
                      $spcEnVal = isset($datas['specification_val']['en']) ? $datas['specification_val']['en'] : 0;
                      $spcArVal = isset($datas['specification_val']['ar']) ? $datas['specification_val']['ar'] : 0;
                      $spcChVal = isset($datas['specification_val']['ch']) ? $datas['specification_val']['ch'] : 0;
                      $count = isset($spcEnKey) ? count($spcEnKey) : 0;
                      $this->db->where('psp_product', $prodId)->delete($this->tbl_products_specification);
                      for ($i = 0; $i <= $count - 1; $i++) {
                           $specifi = array(
                               'psp_product' => $prodId,
                               'psp_key_en' => clean_text($spcEnKey[$i], 0),
                               'psp_key_ar' => clean_text($spcArKey[$i], 0),
                               'psp_key_ch' => clean_text($spcChKey[$i], 0),
                               'psp_val_en' => clean_text($spcEnVal[$i], 0),
                               'psp_val_ar' => clean_text($spcArVal[$i], 0),
                               'psp_val_ch' => clean_text($spcChVal[$i], 0)
                           );
                           $this->db->insert($this->tbl_products_specification, $specifi);
                      }
                      return true;
                 } else {
                      return false;
                 }
            } else {
                 return false;
            }
       }

       public function deleteProduct($id) {
            if (!empty($id)) {
                 $this->db->delete($this->tbl_products, array('prd_id' => $id));
                 $this->db->delete($this->tbl_products_categories, array('pcat_product' => $id));
                 $this->db->delete($this->tbl_products_keyword, array('pkwd_product' => $id));
                 $this->db->delete($this->tbl_products_specification, array('psp_product' => $id));
                 $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $id))->result_array();
                 $this->db->delete($this->tbl_products_images, array('pimg_product' => $id));

                 if (!empty($images)) {
                      foreach ($images as $key => $value) {
                           if (file_exists(FILE_UPLOAD_PATH . 'product/' . $value['pimg_image'])) {
                                unlink(FILE_UPLOAD_PATH . 'product/' . $value['pimg_image']);
                                @unlink(FILE_UPLOAD_PATH . 'product/thumb_' . $value['pimg_image']);
                           }
                      }
                 }

                 return true;
            } else {
                 return false;
            }
       }

       function getBrandIdByBrandName($brandName) {
            $brandName = trim($brandName);
            if (!empty($brandName)) {
                 $result = $this->db->select('brd_id')->from(TABLE_PREFIX . 'brand')->
                                 like('brd_title', $brandName)->get()->row_array();
                 if (isset($result['brd_id']) && !empty($result['brd_id'])) {
                      return $result['brd_id'];
                 } else {
                      return null;
                 }
            } else {
                 return null;
            }
       }

       function setDefaultImage($imgId, $prodId) {
            $this->db->where('prd_id', $prodId);
            $this->db->update($this->tbl_products, array('prd_default_image' => $imgId));
            return true;
       }

       public function getProductListing($limit, $start, $filter) {
            $output = '';
            if (isset($filter['category'])) {
                 $categories[] = $filter['category'];
                 $sub_categories = $this->common_model->getAllCategories($filter['category']);
                 $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
            }
            if (isset($filter['searchText']) && !empty($filter['searchText'])) {
                 $prodIds = $this->db->select('GROUP_CONCAT(pkwd_product) AS pkwd_product')
                                 ->like("pkwd_val_en", $filter['searchText'], 'both')
                                 ->get($this->tbl_products_keyword)->row()->pkwd_product;
                                 if($prodIds){
                                     $where = '( ' . $this->tbl_products . '.prd_id IN(' . $prodIds . ') OR ' .
                                         $this->tbl_products . ".prd_name_en LIKE '%" . $filter['searchText'] . "%') ";
                                 }else{
                                     $where = '(' .
                                         $this->tbl_products . ".prd_name_en LIKE '%" . $filter['searchText'] . "%') ";
                                 }
                                 
                 $this->db->where($where);
            }
            $this->db->select('prd_name_en,prd_price_min,prd_moq,prd_id,prd_offer_price_min,prd_is_stock_prod,prd_default_image,prd_stock_qty')
                ->join($this->tbl_supplier_master,'supm_id=prd_supplier')
                ->where('supm_status',1);
            if (isset($filter['category'])) {
                 $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
                 $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
            }
            if (isset($filter['suplier']))
                 $this->db->where($this->tbl_products . '.prd_supplier', $filter['suplier']);

            if (isset($filter['prd_type']) && $filter['prd_type'] == "hot-selling") {
                 $this->db->where('prd_hot_selling', 1);
            }
            if (isset($filter['prd_type']) && $filter['prd_type'] == "offer") {
                 $this->db->where('prd_offer_price_min >', 0);
                 $this->db->where('prd_offer_price_max >', 0);
            }
            $this->db->limit($limit, $start);
            $this->db->order_by($this->tbl_products . '.prd_added_on', 'desc');
            $this->db->where($this->tbl_products . '.prd_status', 1);
            $products = $this->db->get($this->tbl_products)->result_array();
            // debug($this->db->last_query());
            foreach ($products as $key => $val) {
                 if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                      $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                 } else {
                      $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                 }

                 $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                 $products[$key]['pimg_alttag']  = isset($images['pimg_alttag']) ? $images['pimg_alttag'] : 'product image';
                 $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
                 $products[$key]['sort_price'] = $products[$key]['prd_offer_price_min'] > 0 ? $products[$key]['prd_offer_price_min'] : $products[$key]['prd_price_min'];
            }
            if (isset($filter['order_by'])) {
                 if ($filter['order_by'] == "price_desc") {
                      usort($products, function($a, $b) {
                           return $a['sort_price'] < $b['sort_price'];
                      });
                 }
                 if ($filter['order_by'] == "price_asc") {
                      usort($products, function($a, $b) {
                           return $a['sort_price'] > $b['sort_price'];
                      });
                 }
            }
            foreach ($products as $prod) {
                 $isStock = $prod['prd_is_stock_prod'] == 1 ? '<div class="stock-notif">Stock</div>' : '';
                 $deadStock = $prod['prd_is_stock_prod'] == 1 ? '<p> Stock : ' . floatval($prod['prd_stock_qty']) . '</p>' : '';
                 $output .= "<div class='single-prod'>
              <div class='prod-box'>
              <div class='prod-img'>";
                 $output .= "<a href=";
                 $output .= site_url('product/product_details/' . encryptor($prod['prd_id']));
                 $output .= ">";
                 $output .= img(array('src' => $prod['default_image'], 'id' => '', 'alt' => $prod['pimg_alttag'], 'class' => 'img-responsive img-fluid'));
                 $output .= "</a></div>" . $isStock;
                 $output .= "<div class='prod-detail txt-height'>
                          <div class='details'>
                          <h4 class='prod-title-s'>
                          <a href='" . site_url('product/product_details/' . encryptor($prod['prd_id'])) . "'>";
                 $output .= get_snippet($prod['prd_name_en'], 3);
                 $output .= "</a></h4>";
                 if ($prod['prd_offer_price_min'] > 0) {
                      $output .= "<p><span class='strike-price'>US $";
                      $output .= $prod['prd_price_min'];
                      $output .= "</span> <span class='price-text'>US $";
                      $output .= $prod['prd_offer_price_min'];
                      $output .= "</span></p>";
                 } else {
                      $output .= "<p>From <span class='price-text'>US $";
                      $output .= $prod['prd_price_min'];
                      $output .= "</span> / Piece</p>";
                 }
                 $output .= "<p class='moq'>";
                 $output .= $prod['prd_moq'];
                 $output .= " Piece  (MOQ)</p></div>";
                 //   $islogged = ($this->uid > 0) ? '1' : $this->calldrop;
                 //   $dataUrl = site_url('favorites/addProductToFavoriteList/' . encryptor($prod['prd_id']));
                 //   $isfav = (isset($prod['isFavorite']) && !empty($prod['isFavorite'])) ? ' fas red-icn' : '';
                 //   $output .= '<div class="favourite"><a is-logged="' . $islogged . '" href="javascript:;" class="btnFav" data-url="' . $dataUrl . '">' .
                 //           '<i id="fav" class="far fa-heart ' . $isfav . '"></i></a></div>';
                 $output .= '</div></div></div>';
            }
            if (!$output) {
                 $output = "<h3 class='text-center'>No Products Found!!</h3>";
            }

            return $output;
       }

       public function getProductDetails($id) {
            $this->db->select($this->tbl_products . '.*,supm_domain_prefix,supm_name_en,supm_id,supm_reg_year,supm_panoramic_image,supm_default_image,ctr_name,stt_name,unt_unit_en')
                    ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_products . '.prd_supplier')
                    ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_supplier_master . '.supm_country', 'LEFT')
                    ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_supplier_master . '.supm_state', 'LEFT')
                    ->join($this->tbl_products_units, $this->tbl_products_units . '.unt_id = ' . $this->tbl_products . '.prd_unit', 'LEFT');
            $products = $this->db->where($this->tbl_products . '.prd_id', $id)->where('supm_status',1)->get($this->tbl_products)->row_array();
            if (!empty($products)) {
                 $products['specification'] = $this->db->order_by("psp_id", "asc")->get_where($this->tbl_products_specification, array('psp_product' => $products['prd_id']))->result_array();
                 $keyword = $this->db->get_where($this->tbl_products_keyword, array('pkwd_product' => $products['prd_id']))->result_array();
                 if ($this->uid) {
                      $this->common_model->addToRecommendation($keyword, $this->uid);
                 }
                 if (isset($products['supm_default_image']) && !empty($products['supm_default_image'])) {
                      $sup_images = $this->db->get_where(TABLE_PREFIX . 'supplier_shop_images', array('ssi_supplier' => $products['supm_id'], 'ssi_id' => $products['supm_default_image']))->row_array();
                 } else {
                      $sup_images = $this->db->limit(1)->get_where(TABLE_PREFIX . 'supplier_shop_images', array('ssi_supplier' => $products['supm_id']))->row_array();
                 }
                 $defaultImg = isset($sup_images['ssi_image']) ? $sup_images['ssi_image'] : '';
                 $products['sup_default_image'] = 'assets/uploads/shops/' . $defaultImg;

                 $products['images'] = $this->db->limit(6)->get_where($this->tbl_products_images, array('pimg_product' => $products['prd_id']))->result_array();
                 $products['default_img'] = array();
               // print_r($products['images']);
                 if ($products['prd_default_image']) {
                      foreach ($products['images'] as $key => $val) {
                           if ($val['pimg_id'] == $products['prd_default_image']) {
                                $products['default_img'][0]['pimg_id'] = $val['pimg_id'];
                                $products['default_img'][0]['pimg_product'] = $val['pimg_product'];
                                $products['default_img'][0]['pimg_image'] = $val['pimg_image'];
                                $products['default_img'][0]['pimg_alttag'] = $val['pimg_alttag'];
                                unset($products['images'][$key]);
                                break;
                           }
                      }
                 }
//print_r($products);die();

               //   $products['categories'] = $this->db->get_where($this->tbl_products_categories, array('pcat_product' => $products['prd_id']))->result_array();
                 $products['catId'] = explode(',', $this->db->select('GROUP_CONCAT(pcat_category) AS pcat_category')
                                 ->where(array('pcat_product' => $products['prd_id']))
                                 ->get($this->tbl_products_categories)->row()->pcat_category);
                 $products['category_name'] = $this->db->select('cat_id,cat_title')
                                 ->where_in('cat_id', $products['catId'])
                                 ->get(TABLE_PREFIX . 'category')->result_array();
                 $products['related_products'] = $this->getRelatedProductByKeyWordArray($keyword, $id, $products['catId']);
                 $products['isFavorite'] = $this->db->get_where($this->tbl_favorite_list, array(
                             'fav_consign' => 'PRD', 'fav_consign_id' => $id, 'fav_added_by' => $this->uid))->row_array();

                 //If supplier favarated and followed
                 $products['isFav'] = $this->db->get_where($this->tbl_favorite_list, array(
                             'fav_consign' => 'SUP', 'fav_consign_id' => $products['supm_id'], 'fav_added_by' => $this->uid))->row_array();
                 $products['isFol'] = $this->db->get_where($this->tbl_supplier_followers, array(
                             'sfol_supplier' => $products['supm_id'], 'sfol_followed_by' => $this->uid))->row_array();
            }
            return $products;
       }

       //total count for pagination
       function countAllProduct($filter = '') {

            if (isset($filter['searchText']) && !empty($filter['searchText'])) {
                 $prodIds = $this->db->select('GROUP_CONCAT(pkwd_product) AS pkwd_product')
                                 ->like("REPLACE(LOWER(pkwd_val_en), ' ', '-')", $filter['searchText'], 'both')
                                 ->get($this->tbl_products_keyword)->row()->pkwd_product;
                 $this->db->where_in($this->tbl_products . '.prd_id', explode(',', $prodIds));
            }

            if (!isset($filter['countall'])) {
                 $this->db->where($this->tbl_products . '.prd_status', 1);
            } else {
                 if (!is_root_user()) {
                      if (privilege_exists('SP')) { //Check if buyer have supplier privilege.
                           $this->db->where($this->tbl_products . '.prd_supplier', get_logged_user('usr_supplier'));
                      } else {
                           $this->db->where(array($this->tbl_products . '.prd_added_by' => $this->uid));
                      }
                 }
                 if (isset($filter['active'])) {
                      $this->db->where($this->tbl_products . '.prd_status', 1);
                 }
                 if (isset($filter['inactive'])) {
                      $this->db->where($this->tbl_products . '.prd_status', 0);
                 }
            }

            $this->db->select($this->tbl_products . '.*');
            if (isset($filter['category'])) {
                 $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
                 $this->db->where($this->tbl_products_categories . '.pcat_category', $filter['category']);
            }
            if (isset($filter['suplier'])) {
                 $this->db->where($this->tbl_products . '.prd_supplier', $filter['suplier']);
            }

            if (isset($filter['search']) && !empty($filter['search'])) {
                 $this->db->like($this->tbl_products . '.prd_name_en', $filter['search'], 'both');
            }
            if (isset($filter['prd_type']) && !empty($filter['prd_type'])) {
                 if ($filter['prd_type'] == "hot-selling") {
                      $this->db->where('prd_hot_selling', 1);
                 }
                 if ($filter['prd_type'] == "offer") {
                      $this->db->where('prd_offer_price_min >', 0);
                      $this->db->where('prd_offer_price_max >', 0);
                 }
            }

            $products = $this->db->get($this->tbl_products);
            return $products->num_rows();
       }

       function productAutoSpecification($language, $query) {
            $this->db->select('psp_id AS data, psp_key_' . $language . ' AS value');
            $this->db->like('psp_key_' . $language, $query, 'both');
            return $this->db->get($this->tbl_products_specification)->result_array();
       }

       function placeOrder($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_products_order, $data);
                 $lastInsertId = $this->db->insert_id();

                 $this->db->insert($this->tbl_products_order_comments, array(
                     'poc_order_id' => $lastInsertId,
                     'poc_from' => $this->uid,
                     'poc_to' => $data['ord_supplier_user_id'],
                     'poc_title' => $data['ord_subject'],
                     'poc_desc' => $data['ord_message'],
                 ));

                 generate_log(array(
                     'log_title' => 'New order',
                     'log_desc' => 'New order generated',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => $lastInsertId,
                     'log_added_by' => $this->uid
                 ));
                 return $lastInsertId;
            }
            return false;
       }

       function getUnits() {
            return $this->db->get($this->tbl_products_units)->result_array();
       }

       function productAutoName($query, $category = '') {
            $this->db->select('prd_id AS data, prd_name_en  AS value, prd_unit AS unit, prd_moq AS moq')
                ->join($this->tbl_supplier_master,'supm_id=prd_supplier')
                ->where('supm_status',1);
            if ($category) {
                 $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
                 $this->db->where($this->tbl_products_categories . '.pcat_category', $category);
            }
            // $this->db->like('prd_name_en', $query, 'after');
            $this->db->like('prd_name_en', $query);
            return $this->db->get($this->tbl_products)->result_array();
       }

       function insertRfq($data) {
            $data = array_filter($data);

            if (isset($data['rfq_product']) && !empty($data['rfq_product'])) {
                 $supplier = $this->db->select($this->tbl_users . '.usr_id')
                                 ->join($this->tbl_users, $this->tbl_users . '.usr_supplier = ' . $this->tbl_products . '.prd_supplier')
                                 ->get_where($this->tbl_products, array('prd_id' => $data['rfq_product']))->row_array();
                 $data['rfq_supplier'] = isset($supplier['usr_id']) ? $supplier['usr_id'] : 0;
            }

            $this->db->insert(TABLE_PREFIX . 'rfq', $data);
            $rfqId = $this->db->insert_id();

            $categorySupprs = $this->db->select('GROUP_CONCAT(sbca_supplier) AS sbca_supplier')->where('sbca_category', $data['rfq_category'])
                            ->get(TABLE_PREFIX . 'supplier_buildings_cate_assoc')->row()->sbca_supplier;
            $userIdAssocSupplier = $this->db->select('usr_id')->join($this->tbl_users_groups,'user_id=usr_id')->where('group_id',2)->where_in('usr_supplier', explode(',', $categorySupprs))
                            ->get($this->tbl_users)->result_array();

            foreach ((array) $userIdAssocSupplier as $key => $value) {
                 $this->db->insert($this->tbl_rfq_notification, array(
                     'rfqn_rfq_id' => $rfqId,
                     'rfqn_supplier' => $value['usr_id'],
                     'rfqn_comments' => $data['rfq_requirment'],
                     'rfqn_added_by' => $data['rfq_user']
                 ));

                   $det = $this->getUserById($value['usr_id']);
        if($det['usr_pushy_token'])
   
        {
        $this->load->model('supplier_api/supplier_api_model', 'supplier_api');
        $push = $this->supplier_api->getRfq($value['usr_id'],array('rfq_id'=>$lastInsertId));
        $push['type'] = 'recieved';
        $push['msg_type'] = 'new_rfq';
        $this->common_model->sendPushNotificationForSupplier($push,
                    $det['usr_pushy_token'], '');
        }
            }
            generate_log(array(
                'log_title' => 'New RFQ generated',
                'log_desc' => 'New order generated',
                'log_controller' => strtolower(__CLASS__),
                'log_action' => 'C',
                'log_ref_id' => $rfqId,
                'log_added_by' => $this->uid
            ));
            return true;
       }

       function getSuppliersAssocRFQ($rfqId) {

            $suppliersIds = $this->db->distinct('rfqn_added_by')->select('rfqn_added_by')
                            ->where('rfqn_rfq_id', $rfqId)
                            ->get($this->tbl_rfq_notification)->result_array();

            $suppliersIds = array_column($suppliersIds, 'rfqn_added_by');
            if (!empty($suppliersIds)) {
                 return $this->db->select($this->tbl_users . '.*, ' . $this->tbl_supplier_master . '.*')
                                 ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_users . '.usr_supplier', 'LEFT')
                                 ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id')
                                 ->where($this->tbl_users . '.usr_supplier !=', '')->where($this->tbl_users_groups . '.group_id = 2')
                                 ->where_in($this->tbl_users . '.usr_id', $suppliersIds)->get($this->tbl_users)->result_array();
            }
            return null;
       }

       function getSupplierNotification() {
            $suplier = get_logged_user('usr_supplier');
            $result = array();
            $rfqData = '';
            $RFQcount = 0;

            $result = $this->db->select($this->tbl_rfq_notification . '.*,' .
                                    'sup.usr_supplier AS sup_usr_supplier, sup.usr_first_name AS sup_usr_first_name, sup.usr_last_name AS sup_usr_last_name, sup.usr_avatar AS sup_usr_avatar,' .
                                    'buy.usr_id AS buy_usr_id, buy.usr_first_name AS buy_usr_first_name, buy.usr_last_name AS buy_usr_last_name, buy.usr_avatar AS buy_usr_avatar')
                            ->join($this->tbl_users . ' sup', 'sup.usr_supplier = ' . $this->tbl_rfq_notification . '.rfqn_supplier', 'LEFT')
                            ->join($this->tbl_users . ' buy', 'buy.usr_id = ' . $this->tbl_rfq_notification . '.rfqn_added_by', 'LEFT')
                            ->join($this->tbl_products, $this->tbl_products . '.prd_id = ' . $this->tbl_rfq_notification . '.rfqn_added_by', 'LEFT')
                            ->where($this->tbl_rfq_notification . '.rfqn_supplier', $this->uid)->where($this->tbl_rfq_notification . '.rfqn_read_on IS NULL')
                            ->order_by($this->tbl_rfq_notification . '.rfqn_added_on', 'ASC')->get($this->tbl_rfq_notification)->result_array();

            foreach ($result as $res) {
                 $RFQcount += 1;
                 $rfqData .= '<li>
                            <span class="message">
                                <a href="' . site_url('product/rfq-list/' . encryptor($res['rfqn_rfq_id'])) . '/#messages">New RFQ Message ' . get_snippet($res['rfqn_comments'], 3) . ' Click Here To View</a>
                            </span>
                          </li>';
                 if ($RFQcount == 6) {
                      $rfqData .= '<li>
                                <div class="text-center">
                                    <a href=' . site_url('product/rfq-list') . '>
                                        <strong>View RFQ</strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </li>';
                      break;
                 }
            }

            $order_data = '';
            $order_count = 0;
            $this->db->select('ord_id,ord_prod_id,prd_name_en');
            $this->db->join(TABLE_PREFIX . 'products', 'prd_id = ord_prod_id');
            $this->db->where('ord_is_notified', 0);
            $this->db->where('ord_supplier', $suplier);
            $result = $this->db->get(TABLE_PREFIX . 'products_order_master')->result_array();
            foreach ($result as $res) {
                 $order_count += 1;
                 $order_data .= '<li>
                           <span class="message">
                               <a href = ' . site_url('order/order_summery/' . encryptor($res['ord_id'])) . '>New Enquiry For ' . $res['prd_name_en'] . '</a>
                           </span>
                         </li>';
                 if ($order_count == 6) {
                      $order_data .= '<li>
                                <div class="text-center">
                                    <a href=' . site_url('order') . '>
                                        <strong>View All Enquiries</strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </li>';
                      break;
                 }
            }
            $output = array(
                'count' => $RFQcount,
                'data' => $rfqData,
                'order_count' => $order_count,
                'order_data' => $order_data
            );
            die(json_encode($output));
       }

       public function getRfq($rfq_id = '', $date1='', $date2='') {
            $suplier = get_logged_user('usr_supplier');
            if ($this->usr_grp == "SP" && !is_root_user()) {
                 $sup_category = $this->db->select('sbca_category')->where('sbca_supplier', $suplier)->get(TABLE_PREFIX . 'supplier_buildings_cate_assoc')->result_array();
                 $categories = array_column($sup_category, 'sbca_category');
                 $this->db->where_in('rfq_category', $categories);
                 $this->db->where('rfq_status', 1);
            }
            $this->db->select(TABLE_PREFIX . 'rfq.*,' . TABLE_PREFIX . 'category.cat_title,' . TABLE_PREFIX . 'market_places.mar_name,' .
                    TABLE_PREFIX . 'products.*,' . TABLE_PREFIX . 'products_units.*,' . TABLE_PREFIX . 'users.usr_first_name,'.TABLE_PREFIX . 'users.usr_last_name,'.TABLE_PREFIX . 'users.usr_email,'.TABLE_PREFIX . 'users.usr_phone');
            $this->db->join(TABLE_PREFIX . 'category', TABLE_PREFIX . 'category.cat_id =' . TABLE_PREFIX . 'rfq.rfq_category', 'left')
                    ->join(TABLE_PREFIX . 'market_places', TABLE_PREFIX . 'market_places.mar_id =' . TABLE_PREFIX . 'rfq.rfq_market', 'left')
                    ->join(TABLE_PREFIX . 'products', TABLE_PREFIX . 'products.prd_id =' . TABLE_PREFIX . 'rfq.rfq_product', 'left')
                    ->join(TABLE_PREFIX . 'products_units', TABLE_PREFIX . 'products_units.unt_id =' . TABLE_PREFIX . 'rfq.rfq_unit', 'left')
                    ->join(TABLE_PREFIX . 'users', TABLE_PREFIX . 'users.usr_id =' . TABLE_PREFIX . 'rfq.rfq_user', 'left');

            if ($this->usr_grp == 'BY') {
                 $this->db->where('rfq_user', $this->uid);
            }

            $this->db->order_by('rfq_added_on', 'DESC');
            if ($rfq_id) {
                 $this->db->where('rfq_id', $rfq_id);
                 $rfqSingle = $this->db->get(TABLE_PREFIX . 'rfq')->row_array();
                 if ($this->usr_grp != "BY") {
                      $usr = $rfqSingle['rfq_user'];
                      $where = "rfqn_added_by = $this->uid and rfqn_supplier = $usr or(rfqn_added_by = $usr and rfqn_supplier = $this->uid)";
                      $rfqSingle['comments'] = $this->db->select($this->tbl_rfq_notification . '.*,' .
                                              'sup.usr_supplier AS sup_usr_supplier, sup.usr_first_name AS sup_usr_first_name, sup.usr_last_name AS sup_usr_last_name, sup.usr_avatar AS sup_usr_avatar,' .
                                              'buy.usr_id AS buy_usr_id, buy.usr_first_name AS buy_usr_first_name, buy.usr_last_name AS buy_usr_last_name, buy.usr_avatar AS buy_usr_avatar')
                                      ->join($this->tbl_users . ' sup', 'sup.usr_supplier = ' . $this->tbl_rfq_notification . '.rfqn_supplier', 'LEFT')
                                      ->join($this->tbl_users . ' buy', 'buy.usr_id = ' . $this->tbl_rfq_notification . '.rfqn_added_by', 'LEFT')
                                      ->where($where)
                                      ->order_by('rfqn_added_on','asc')
                                   //    ->where($this->tbl_rfq_notification . '.rfqn_rfq_id', $rfq_id)->where($this->tbl_rfq_notification . '.rfqn_supplier', $this->uid)
                                   //    ->or_where($this->tbl_rfq_notification . '.rfqn_added_by', $this->uid)->order_by($this->tbl_rfq_notification . '.rfqn_added_on', 'ASC')
                                      ->get($this->tbl_rfq_notification)->result_array();
                      if (!empty($rfqSingle['comments'])) {
                           foreach ($rfqSingle['comments'] as $key => $value) {
                                if (empty($value['rfqn_read_on']) && ($this->uid == $value['rfqn_supplier'])) {
                                     $this->db->where('rfqn_id', $value['rfqn_id']);
                                     $this->db->update($this->tbl_rfq_notification, array('rfqn_read_on' => date('Y-m-d h:i:s')));
                                }
                           }
                      }
                 }
                 return $rfqSingle;
            } else {

             if($date1 && $date2)
             {
                $date1 = $date1." 00:00:00";
                $date2 = $date2." 11:59:59";
                          return $this->db->where("rfq_added_on between '$date1' and '$date2'")
                                         ->get(TABLE_PREFIX . 'rfq')->result_array();
             }
               else
                return $this->db->get(TABLE_PREFIX . 'rfq')->result_array();
            }
       }

       function loadSeperateRFQComment($rfq_id, $from = '', $to = '') {
            $rfqComments['comments'] = $this->db->select($this->tbl_rfq_notification . '.*,' .
                                    // 'sup.usr_supplier AS sup_usr_supplier, sup.usr_first_name AS sup_usr_first_name, sup.usr_last_name AS sup_usr_last_name, sup.usr_avatar AS sup_usr_avatar,' .
                                    'buy.usr_id AS buy_usr_id, buy.usr_first_name AS buy_usr_first_name, buy.usr_last_name AS buy_usr_last_name, buy.usr_avatar AS buy_usr_avatar')
                            // ->join($this->tbl_users . ' sup', 'sup.usr_supplier = ' . $this->tbl_rfq_notification . '.rfqn_supplier', 'LEFT')
                            ->join($this->tbl_users . ' buy', 'buy.usr_id = ' . $this->tbl_rfq_notification . '.rfqn_added_by', 'LEFT')
                            ->where($this->tbl_rfq_notification . '.rfqn_rfq_id', $rfq_id)
                            ->where('((' . $this->tbl_rfq_notification . '.rfqn_added_by = ' . $from . ' AND ' . $this->tbl_rfq_notification . '.rfqn_supplier = ' . $to . ') OR '
                                    . '(' . $this->tbl_rfq_notification . '.rfqn_added_by = ' . $to . ' AND ' . $this->tbl_rfq_notification . '.rfqn_supplier = ' . $from . '))')
                            ->order_by($this->tbl_rfq_notification . '.rfqn_added_on', 'ASC')->get($this->tbl_rfq_notification)->result_array();

            return $rfqComments;
       }

       function loadRFQComments($excludeIds, $rfq_id, $from = '', $to = '') {
            if ($excludeIds) {
                 if (is_root_user()) {
                      $data['comments'] = $this->db->select($this->tbl_rfq_notification . '.*,' .
                                              'sup.usr_supplier AS sup_usr_supplier, sup.usr_first_name AS sup_usr_first_name, sup.usr_last_name AS sup_usr_last_name, sup.usr_avatar AS sup_usr_avatar,' .
                                              'buy.usr_id AS buy_usr_id, buy.usr_first_name AS buy_usr_first_name, buy.usr_last_name AS buy_usr_last_name, buy.usr_avatar AS buy_usr_avatar')
                                      ->join($this->tbl_users . ' sup', 'sup.usr_supplier = ' . $this->tbl_rfq_notification . '.rfqn_supplier', 'LEFT')
                                      ->join($this->tbl_users . ' buy', 'buy.usr_id = ' . $this->tbl_rfq_notification . '.rfqn_added_by', 'LEFT')
                                      ->where($this->tbl_rfq_notification . '.rfqn_rfq_id', $rfq_id)
                                      ->where('((' . $this->tbl_rfq_notification . '.rfqn_supplier = ' . $from . ' AND ' . $this->tbl_rfq_notification . '.rfqn_added_by = ' . $to . ') OR '
                                              . '(' . $this->tbl_rfq_notification . '.rfqn_supplier = ' . $to . ' AND ' . $this->tbl_rfq_notification . '.rfqn_added_by = ' . $from . '))')
                                      ->where_not_in($this->tbl_rfq_notification . '.rfqn_id', $excludeIds)
                                      ->order_by($this->tbl_rfq_notification . '.rfqn_added_on', 'ASC')
                                      ->get($this->tbl_rfq_notification)->result_array();
                 } else {
                      $data['comments'] = $this->db->select($this->tbl_rfq_notification . '.*,' .
                                              'sup.usr_supplier AS sup_usr_supplier, sup.usr_first_name AS sup_usr_first_name, sup.usr_last_name AS sup_usr_last_name, sup.usr_avatar AS sup_usr_avatar,' .
                                              'buy.usr_id AS buy_usr_id, buy.usr_first_name AS buy_usr_first_name, buy.usr_last_name AS buy_usr_last_name, buy.usr_avatar AS buy_usr_avatar')
                                      ->join($this->tbl_users . ' sup', 'sup.usr_supplier = ' . $this->tbl_rfq_notification . '.rfqn_supplier', 'LEFT')
                                      ->join($this->tbl_users . ' buy', 'buy.usr_id = ' . $this->tbl_rfq_notification . '.rfqn_added_by', 'LEFT')
                                      ->where($this->tbl_rfq_notification . '.rfqn_rfq_id', $rfq_id)
                                      ->where('((' . $this->tbl_rfq_notification . '.rfqn_supplier = ' . $from . ' AND ' . $this->tbl_rfq_notification . '.rfqn_added_by = ' . $to . ') OR '
                                              . '(' . $this->tbl_rfq_notification . '.rfqn_added_by = ' . $to . ' AND ' . $this->tbl_rfq_notification . '.rfqn_supplier = ' . $from . '))')
                                      ->where_not_in($this->tbl_rfq_notification . '.rfqn_id', $excludeIds)
                                      ->order_by($this->tbl_rfq_notification . '.rfqn_added_on', 'ASC')
                                      ->get($this->tbl_rfq_notification)->result_array();
                 }
                 return $data;
            }
       }

       function sendRFQComments($data) {

            if (isset($data['rfqn_rfq_id']) && isset($data['rfqn_added_by']) &&
                    isset($data['rfqn_supplier']) && isset($data['rfqn_comments'])) {
                 return $this->db->insert($this->tbl_rfq_notification, $data);
            }
       }

       public function updateRfqStatusAndCount($suplier, $options) {
            //update reply count
            if (!empty($options) && !empty($suplier)) {
                 $rfq_id = isset($options['rfq_id']) ? $options['rfq_id'] : 0;
                 $reply = isset($options['reply']) ? $options['reply'] : 0;

                 /* Get RFQ */
                 $rfqDetails = $this->db->get_where($this->tbl_rfq, array('rfq_id' => $rfq_id))->row_array();
                 $rfqSender = isset($rfqDetails['rfq_user']) ? $rfqDetails['rfq_user'] : 0;

                 //Send pushnotification
                 $user = $this->ion_auth->user($rfqDetails['rfq_user'])->row_array();
                 if (isset($user['usr_pushy_token']) && !empty($user['usr_pushy_token'])) {

                      $pid = isset($options['rfqn_prod_sent'][0]) ? $options['rfqn_prod_sent'][0] : 0;
                      $productDetas = $this->getProduct($pid);
                      if (!empty($productDetas)) {
                           $push['id'] = $rfq_id ;
                           $push['msg_type'] = 'rfq_reply_notfication';
                           $push['from'] = 'Fujeeka';
                           $push['title'] = $options['reply'];
                           $push['image'] = isset($productDetas['default_image'])?site_url() . 'assets/uploads/product/' . $productDetas['default_image']:'';
                           $push['time'] = date("Y-m-d H:i:s");
                           $push['suplier'] = $productDetas['supm_name_en'];
                           $this->common_model->sendPushNotification($push, $user['usr_pushy_token'], '');
                      }
                 }
                 $suplierUserId = $this->db->select('usr_id')->join($this->tbl_users_groups,'user_id=usr_id')->where('group_id',2)->where('usr_supplier', $suplier)
                 ->get($this->tbl_users)->row()->usr_id;

                 $check = $this->db->where('rfqn_added_by',$suplierUserId)->where('rfqn_rfq_id',$rfq_id) ->count_all_results(TABLE_PREFIX . 'rfq_notification');

                 if(!$check){
                    $this->db->set('rfq_reply_count', 'rfq_reply_count+1', FALSE);
                    $this->db->where('rfq_id', $rfq_id);
                    $this->db->update(TABLE_PREFIX . 'rfq');
                 }
                //change status if rfq_reply_count = 20
                 $this->db->set('rfq_status', 0);
                 $this->db->where('rfq_id', $rfq_id);
                 $this->db->where('rfq_reply_count', 20);
                 $this->db->update(TABLE_PREFIX . 'rfq');
                 //insert into notification table to hide rfq notification for this supplier
                 //$suplierUserId = $this->db->get_where($this->tbl_users, array('usr_supplier' => $suplier))->row()->usr_id;

                 $det = array(
                     'rfqn_rfq_id' => $rfq_id,
                     'rfqn_supplier' => $rfqSender,
                     'rfqn_comments' => $reply,
                     'rfqn_added_by' => $suplierUserId
                 );
                 $this->db->insert(TABLE_PREFIX . 'rfq_notification', $det);
                 $notifyId = $this->db->insert_id();

                 //Products set by supplier
                 if (isset($options['rfqn_prod_sent']) && !empty($options['rfqn_prod_sent'])) {
                      foreach ($options['rfqn_prod_sent'] as $key => $value) {
                           $this->db->insert($this->tbl_rfq_products_send, array(
                               'rps_rfq_id' => $rfq_id,
                               'rps_supplier_id' => $suplierUserId,
                               'rps_nottification_id' => $notifyId,
                               'rps_prd_id_sent' => $value
                           ));
                      }
                 }
            }


            return true;
       }

       //pagination for list product in backend
       public function getAllProductForAjaxPagination($limit, $start, $filter) {
            if (!is_root_user()) {
                 if (privilege_exists('SP')) { //Check if buyer have supplier privilege.
                      $this->db->where($this->tbl_products . '.prd_supplier', get_logged_user('usr_supplier'));
                 } else {
                      $this->db->where(array($this->tbl_products . '.prd_added_by' => $this->uid));
                 }
            }
            if (isset($filter['active'])) {
                 $this->db->where($this->tbl_products . '.prd_status', 1);
            }
            if (isset($filter['inactive'])) {
                 $this->db->where($this->tbl_products . '.prd_status', 0);
            }
            $this->db->select('prd_id,prd_name_en,prd_status,supm_name_en,prd_added_on,prd_is_stock_prod,prd_is_flash_sale,prd_price_min,prd_offer_price_min,' . $this->tbl_users . '.usr_username, ,' . $this->tbl_users . '.usr_first_name,');
            $this->db->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_products . '.prd_supplier');
            $this->db->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_products . '.prd_added_by');
            if (isset($filter['search']) && !empty($filter['search'])) {
                 $qry = $filter['search'];
                 $where = "(prd_name_en LIKE '%$qry%' OR supm_name_en LIKE '%$qry%')";
                 $this->db->where($where);
            }
            $this->db->order_by($this->tbl_products . '.prd_added_on', 'desc');
            $this->db->limit($limit, $start);
            $products = $this->db->get($this->tbl_products)->result_array();
            $output = "";
            foreach ($products as $prod) {
                 $output .= "<tr>";
                 if (is_root_user()) {
                      $output .= "<td data-url='" . site_url('product/view/' . $prod['prd_id']) . "' class='trVOE'>" . $prod['supm_name_en'] . "</td>";
                 }
                 if ($this->usr_grp == 'SP' || is_root_user()) {
                      $output .= "<td data-url='" . site_url('product/view/' . $prod['prd_id']) . "' class='trVOE'>" . $prod['usr_first_name'] . "</td>";
                 }
                 $output .= "<td data-url='" . site_url('product/view/' . $prod['prd_id']) . "' class='trVOE'>" . $prod['prd_name_en'] . "</td>";
                 $price = '<span style="color: #00b904;">$'.$prod['prd_price_min'].'</span>';
                 if($prod['prd_offer_price_min'] > 0){
                    $price =' <span style="text-decoration: line-through;">$'.$prod['prd_price_min'].'</span><br>
                    <span style="color: #00b904;">$'.$prod['prd_offer_price_min'].'</span>';
                 }
                 $output .= "<td data-url='" . site_url('product/view/' . $prod['prd_id']) . "' class='trVOE'>" . $price . "</td>";

                 $output .= "<td data-url='" . site_url('product/view/' . $prod['prd_id']) . "' class='trVOE'>" . $prod['prd_added_on'] . "</td>";

                 if (check_permission('product', 'delete')) {
                      $output .= "<td>
                                    <a class='pencile deleteListItem' href='javascript:void(0);'
                                        data-url='" . site_url('product/delete/' . $prod['prd_id']) . "' data-status='" . $prod['prd_status'] . "'>
                                        <i class='fa fa-remove'></i>
                                    </a>
                                </td>";
                 }
                 if (check_permission('product', 'changestatus')) {
                      $output .= "<td>
                                <label class='switch'>
                                    <input type='checkbox' value='1' class='chkOnchange'";
                      if ($prod['prd_status'] == 1)
                           $output .= "checked ";
                      $output .= "data-url='" . site_url('product/changestatus/' . encryptor($prod['prd_id'])) . "'>
                                    <span class='slider round'></span>
                                </label>
                            </td>";
                 }
                 if (!is_root_user()) {
                      $output .= "<td>";
                      $output .= ($prod['prd_status'] == 1) ? "<span style='color:green'>Active</span>" : "<span style='color:red'>Inactive</span>";
                      $output .= "</td>";
                 }

              
                    if(check_permission('product','duplicate')){
                      $output .= "<td class='text-center'><a href='".site_url('product/duplicate/'.encryptor($prod['prd_id']))."'><i style='cursor:pointer'  class='fa fa-clone' aria-hidden='true'></i></a></td>";
                    }
                


                 $output .= "</tr>";
            }
            if (!$output) {
                 $output = "<td class='text-center' colspan='4'><h4>No Products Found!!</h4></td>";
            }

            return $output;
       }

       function stuffSearch($element, $limit = 0, $index = 0) {
            $this->load->model('supplier/supplier_model', 'supplier');
            $productBySupplier = array();

            $category = $this->input->get('scid') ? $this->input->get('scid') : $this->input->get('cid');
            $prodIds = $this->db->select('GROUP_CONCAT(pkwd_product) AS pkwd_product')
                            ->like("REPLACE(LOWER(pkwd_val_en), ' ', '-')", slugify($element), 'both')
                            ->get($this->tbl_products_keyword)->row()->pkwd_product;
            $where = '';
            if (!empty($category)) {
               $categories[] = $category;
               $sub_categories = $this->common_model->getAllCategories($category);
               $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
            }
            if (isset($_GET['seg']) && $_GET['seg'] == 'stock') {
                 $where .= $this->tbl_products . '.prd_is_stock_prod = 1 AND ';
            }

            if (!empty($prodIds) && !empty($element)) {
                 $where .= '( ' . $this->tbl_products . '.prd_id IN(' . $prodIds . ') OR ' .
                         $this->tbl_products . ".prd_name_en LIKE '%" . $element . "%') ";
            } else {
                 $where .= $this->tbl_products . ".prd_name_en LIKE '%" . $element . "%'";
            }
            $where .= 'AND ' . $this->tbl_products . '.prd_status = 1';

            $this->db->where($where);

            $this->db->select('prd_default_image,prd_id,prd_moq,prd_is_stock_prod,prd_price_min,prd_stock_qty,prd_name_en,supm_status,supm_id,supm_name_en,supm_city_en,supm_reg_year');
            $this->db->join($this->tbl_supplier_master, 'supm_id = prd_supplier');
            if (!empty($category)) {
               $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
               $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
            }
            if (!empty($limit)) {
                 $this->db->limit($limit, $index);
            }

            if ($this->input->get('sort') == "price_desc")
                 $this->db->order_by($this->tbl_products . ".prd_price_min", "desc");
            if ($this->input->get('sort') == "price_asc")
                 $this->db->order_by($this->tbl_products . ".prd_price_min", "asc");

            $products = $this->db->get($this->tbl_products)->result_array();
            if (isset($filter['category'])) {
               $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
               $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
            }
            if (isset($_GET['seg']) && trim($_GET['seg']) == 'supplier') {
                 if (!empty($products)) {
                      $i = 0;
                      foreach ($products as $key => $value) {
                           if ($value['supm_status'] == 1 && $i <= 2) {

                              //   $this->db->select($this->tbl_category . '.cat_id,cat_title');
                              //   $this->db->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . TABLE_PREFIX . 'supplier_buildings_cate_assoc.sbca_category');
                              //   $this->db->where('sbca_supplier', $value['supm_id']);
                              //   $catg = $this->db->get(TABLE_PREFIX . 'supplier_buildings_cate_assoc')->result_array();
                              //   $value['main_prds'] = implode(', ', array_column($catg, 'cat_title'));
                                if (!empty($products[$key]['prd_default_image'])) {
                                     $sup_images = $this->db->get_where($this->tbl_products_images, array('pimg_id' => $value['prd_default_image']))->row_array();
                                } else {
                                     $sup_images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $value['prd_id']))->row_array();
                                }
                                $defaultImg = isset($sup_images['pimg_image']) ? $sup_images['pimg_image'] : '';
                                $value['sup_default_image'] = 'assets/uploads/product/' . $defaultImg;


                                $productBySupplier[$value['supm_id']]['products'][] = $value;
                           }
                           $i++;
                      }
                 }
                 return $productBySupplier;
            } else {
                 if (!empty($products)) {
                      foreach ($products as $key => $value) {
                           if (!empty($products[$key]['prd_default_image'])) {
                                $sup_images = $this->db->get_where($this->tbl_products_images, array('pimg_id' => $value['prd_default_image']))->row_array();
                           } else {
                                $sup_images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $value['prd_id']))->row_array();
                           }
                           $defaultImg = isset($sup_images['pimg_image']) ? $sup_images['pimg_image'] : '';
                           $products[$key]['sup_default_image'] = 'assets/uploads/product/' . $defaultImg;
                      }
                 }
                 return $products;
            }
       }

       function stuffSearchCount($element) {

            $category = $this->input->get('scid') ? $this->input->get('scid') : $this->input->get('cid');

            $prodIds = $this->db->select('GROUP_CONCAT(pkwd_product) AS pkwd_product')
                            ->like("REPLACE(LOWER(pkwd_val_en), ' ', '-')", slugify($element), 'both')
                            ->get($this->tbl_products_keyword)->row()->pkwd_product;
            $where = '';
            if (isset($_GET['seg']) && $_GET['seg'] == 'stock') {
                 $where .= $this->tbl_products . '.prd_is_stock_prod = 1 AND ';
            }

            if (!empty($prodIds) && !empty($element)) {
                 $where .= '( ' . $this->tbl_products . '.prd_id IN(' . $prodIds . ') OR ' .
                         $this->tbl_products . ".prd_name_en LIKE '%" . $element . "%') ";
            } else {
                 $where .= $this->tbl_products . ".prd_name_en LIKE '%" . $element . "%'";
            }
            $where .= 'AND ' . $this->tbl_products . '.prd_status = 1';

            $this->db->where($where);

            $this->db->select($this->tbl_products . '.*,' . $this->tbl_supplier_master . '.*,' .
                            $this->tbl_countries . '.ctr_name,' . $this->tbl_states . '.stt_name,' . $this->tbl_products_categories . '.*')
                    ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_products . '.prd_supplier')
                    ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_supplier_master . '.supm_country', 'LEFT')
                    ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_supplier_master . '.supm_state', 'LEFT');

            $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
            if (!empty($category)) {

                 $this->db->where($this->tbl_products_categories . '.pcat_category = ' . $category);
            }

            if ($this->input->get('sort') == "price_desc")
                 $this->db->order_by($this->tbl_products . ".prd_price_min", "desc");
            if ($this->input->get('sort') == "price_asc")
                 $this->db->order_by($this->tbl_products . ".prd_price_min", "asc");

            $products = $this->db->get($this->tbl_products)->result_array();
            return $products;
       }

       /**
        * Show products under category
        * @param string $slug
        * @param id $id
        * @param int $page
        * Author : JK
        */
       function getProductsByCategory($id, $limit = 0, $index = 0, $count = true, $supId = '') {
            if (!empty($id)) {
                 $childs = $this->getAllChildCategories($id);
                 array_push($childs, $id);
                 $productId = $this->db->select('GROUP_CONCAT(pcat_product) AS pcat_product')
                                 ->where_in('pcat_category', $childs)->get($this->tbl_products_categories)->row()->pcat_product;

                 if (!empty($limit)) {
                      $this->db->limit($limit, $index);
                 }
                 if (!empty($supId)) {
                      $this->db->where($this->tbl_products . '.prd_supplier', $supId);
                 }
                 $this->db->where_in($this->tbl_products . '.prd_id', explode(',', $productId));

                 if ($this->input->get('sort') == "price_desc")
                      $this->db->order_by($this->tbl_products . ".prd_price_min", "desc");
                 if ($this->input->get('sort') == "price_asc")
                      $this->db->order_by($this->tbl_products . ".prd_price_min", "asc");

                 if ($count) {
                      return $this->db->count_all_results($this->tbl_products);
                 }

                 $products = $this->db->get($this->tbl_products)->result_array();

                 foreach ($products as $key => $val) {
                      if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                           $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                      } else {
                           $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                      }
                      $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                      $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
                 }
                 return $products;
            }
            return false;
       }

       /**
        * Get all child categories
        * @param int $rootid
        * @return array
        * Author : JK
        */
       function getAllChildCategories($rootid) {
            $children = array();
            $result = $this->db->where('cat_parent', $rootid)->get($this->tbl_category)->result_array();
            if (!empty($result)) {
                 foreach ($result as $key => $value) {
                      $children[] = $value['cat_id'];
                      $arr = $this->getAllChildCategories($value['cat_id']);
                      if (count($arr) > 0) {
                           $subids = $this->getAllChildCategories($value['cat_id']);
                           $intId = isset($subids[0]) ? $subids[0] : 0;
                           array_push($children, $intId);
                      }
                 }
            }
            return $children;
       }

       public function getRelatedProductByKeyWordArray($keywords, $prd_id = '', $catId) {

            $rec_array = array();
            $rec = implode(',', array_column($keywords, 'pkwd_val_en'));
            $cat_ids = implode(',', $catId);
            $produt_ids = '';
            if($cat_ids){
               $produt_ids = $this->db->query("select pkwd_product from " . TABLE_PREFIX . "products_keyword join " . $this->tbl_products_categories . " on pkwd_product=pcat_product where MATCH(pkwd_val_en, pkwd_val_ar,pkwd_val_ch) AGAINST ('$rec' IN NATURAL LANGUAGE MODE) AND pcat_category IN(" . $cat_ids . ")")->result_array();
            }
            if ($produt_ids) {
                 $this->db->select('prd_id,prd_name_en,prd_price_min,unt_unit_en,prd_moq,prd_default_image')
                    ->join($this->tbl_supplier_master,'supm_id=prd_supplier')
                    ->where('supm_status',1);
                 $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
                 $this->db->where_in('prd_id', array_column($produt_ids, 'pkwd_product'));
                 $this->db->limit(3);
                 $products = $this->db->get(TABLE_PREFIX . 'products')->result_array();
                 foreach ($products as $key => $val) {
                      if ($val['prd_id'] == $prd_id) {
                           continue;
                      }
                      $rec_array[$key]['prd_id'] = encryptor($val['prd_id']);
                      $rec_array[$key]['name'] = $val['prd_name_en'];
                      $rec_array[$key]['price'] = $val['prd_price_min'];
                      $rec_array[$key]['unit'] = $val['unt_unit_en'];
                      $rec_array[$key]['moq'] = $val['prd_moq'];
                      if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                           $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                      } else {
                           $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                      }
                      $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
                      $rec_array[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
                 }
            }
            return array_values($rec_array);
       }

       function pendingRFQList() {
            $suplier = get_logged_user('usr_supplier');
            $sup_category = $this->db->select('sbca_category')->where('sbca_supplier', $suplier)
                            ->get(TABLE_PREFIX . 'supplier_buildings_cate_assoc')->result_array();
            $categories = array_column($sup_category, 'sbca_category');
            $this->db->where_in('rfq_category', $categories);
            $this->db->where('rfq_status', 1);

            $this->db->select(TABLE_PREFIX . 'rfq.*,' . TABLE_PREFIX . 'category.cat_title,' . TABLE_PREFIX . 'market_places.mar_name,' .
                    TABLE_PREFIX . 'products.*,' . TABLE_PREFIX . 'products_units.*,' . TABLE_PREFIX . 'users.usr_first_name');
            $this->db->join(TABLE_PREFIX . 'category', TABLE_PREFIX . 'category.cat_id =' . TABLE_PREFIX . 'rfq.rfq_category', 'left')
                    ->join(TABLE_PREFIX . 'market_places', TABLE_PREFIX . 'market_places.mar_id =' . TABLE_PREFIX . 'rfq.rfq_market', 'left')
                    ->join(TABLE_PREFIX . 'products', TABLE_PREFIX . 'products.prd_id =' . TABLE_PREFIX . 'rfq.rfq_product', 'left')
                    ->join(TABLE_PREFIX . 'products_units', TABLE_PREFIX . 'products_units.unt_id =' . TABLE_PREFIX . 'rfq.rfq_unit', 'left')
                    ->join(TABLE_PREFIX . 'users', TABLE_PREFIX . 'users.usr_id =' . TABLE_PREFIX . 'rfq.rfq_user', 'left');
            $f = $this->db->get($this->tbl_rfq)->result_array();
            debug($f);
            //$f = $this->db->where('DATEDIFF(rfq_validity, NOW()) < 0')->get($this->tbl_rfq)->result_array();
            //debug($f);
       }

       public function getStockProducts($limit, $start, $filter) {
            $output = '';
            if (isset($filter['category'])) {
                 $categories[] = $filter['category'];
                 $sub_categories = $this->common_model->getAllCategories($filter['category']);
                 $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
            }
            $this->db->select('unt_unit_name_en,prd_id,prd_stock_qty,prd_default_image,prd_updated_on,prd_name_en,prd_price_min,prd_offer_price_min,supm_name_en');
            if (isset($filter['category'])) {
                 $this->db->join($this->tbl_products_categories, 'pcat_product = prd_id');
                 $this->db->where_in('pcat_category', $categories);
            }
            $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
            $this->db->join($this->tbl_supplier_master, 'supm_id = prd_supplier', 'LEFT');
            $this->db->where('prd_status', 1);
            $this->db->where('supm_status', 1);
            $this->db->where('prd_is_stock_prod', 1);
            if (isset($filter['prd_type']) && !empty($filter['prd_type'])) {
                 if ($filter['prd_type'] == "flash") {
                      $this->db->where('prd_is_flash_sale', 1);
                 }
                 if ($filter['prd_type'] == "offer") {
                      $this->db->where('prd_offer_price_min >', 0);
                      $this->db->where('prd_offer_price_max >', 0);
                 }
            }
            $this->db->limit($limit, $start);
            $this->db->order_by('prd_updated_on', 'DESC');
            $products = $this->db->get($this->tbl_products)->result_array();
            foreach ($products as $key => $val) {
                 if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                      $image = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                 } else {
                      $image = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                 }
                 $products[$key]['catId'] = explode(',', $this->db->select('GROUP_CONCAT(pcat_category) AS pcat_category')
                                 ->where(array('pcat_product' => $products[$key]['prd_id']))
                                 ->get($this->tbl_products_categories)->row()->pcat_category);
                 $products[$key]['category_name'] = $this->db->select('GROUP_CONCAT(cat_title) AS cat_title')
                                 ->where_in('cat_id', $products[$key]['catId'])
                                 ->get(TABLE_PREFIX . 'category')->row()->cat_title;
                 $defaultImg = isset($image['pimg_image']) ? $image['pimg_image'] : '';
                  $products[$key]['pimg_alttag'] = isset($image['pimg_alttag']) ? $image['pimg_alttag'] : 'product image';
                 $products[$key]['sort_price'] = $products[$key]['prd_offer_price_min'] > 0 ? $products[$key]['prd_offer_price_min'] : $products[$key]['prd_price_min'];
                 $products[$key]['default_image'] = 'assets/uploads/product/' . $defaultImg;
            }
            if (isset($filter['order_by'])) {
                 if ($filter['order_by'] == "price_desc") {
                      usort($products, function($a, $b) {
                           return $a['sort_price'] < $b['sort_price'];
                      });
                 }
                 if ($filter['order_by'] == "price_asc") {
                      usort($products, function($a, $b) {
                           return $a['sort_price'] > $b['sort_price'];
                      });
                 }
            }
            foreach ($products as $prod) {

                 $updated_time = date('Y-m-d H:i:s', strtotime($prod['prd_updated_on']));
                 $output .="<li onclick='";
                 $output .= 'location.href="';
                 $output .= site_url('product/product_details/' . encryptor($prod['prd_id'])).'"' ;
                 $output .= "'style='cursor:pointer'>
               <div class='prd-name'>
                   <div class='img'>
                     " . img(array('src' => $prod['default_image'],'alt' => $prod['pimg_alttag'])) . "
                   </div>
                   <div class='nme'><h5>
                       " . $prod['prd_name_en'] . "</h5>
                       <div class='sub-det'>
                          <p>Category : " . $prod['category_name'] . "</p>
                       </div><h4>
                       " . $prod['supm_name_en'] . "</h4>
                   </div>
               </div>";
                 if ($prod['prd_offer_price_min'] > 0) {
                      $output .="<div class='prd-prce'><del>US$ " . $prod['prd_price_min'] . "</del> US$ " . $prod['prd_offer_price_min'] . "</div>";
                 } else {
                      $output .= "<div class='prd-prce'> US$ " . $prod['prd_price_min'] . "</div>";
                 }
                 $output .= "<div class='prd-update'>" . get_timeago($updated_time) . "</div>
               <div class='prd-stck'>" . floatval($prod['prd_stock_qty']) . $prod['unt_unit_name_en'] . "</div>
           </li>";
            }
            if (!$output) {
                 $output = "<h3 class='text-center'>No Products Found!!</h3>";
            }

            return $output;
       }

       function RFQretaledProducts($categoryId) {
            if (!empty($categoryId)) {
                 return $this->db->select($this->tbl_products . '.*, categories.*')
                                 ->join('(select * from ' . $this->tbl_products_categories . ' where pcat_category = ' . $categoryId . ') categories ', 'categories.pcat_product = cpnl_products.prd_id')
                                 ->where($this->tbl_products . '.prd_supplier', $this->suplr)
                                 ->get($this->tbl_products)->result_array();
            }
            return null;
       }

       function productsSentBySupplier($rfqId) {
            if (!empty($rfqId)) {
                 $suppliers = $this->db->get_where($this->tbl_rfq_products_send, array('rps_rfq_id' => $rfqId))->result_array();
                 $return = array();
                 foreach ((array) $suppliers as $key => $value) {
                      if (!array_key_exists($value['rps_supplier_id'], $return)) {
                           $return[$value['rps_supplier_id']]['products'] = $this->db->select($this->tbl_rfq_products_send . '.*, ' . $this->tbl_supplier_master . '.*,' . $this->tbl_products . '.*')
                                           ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_rfq_products_send . '.rps_supplier_id', 'LEFT')
                                           ->join($this->tbl_products, $this->tbl_products . '.prd_id = ' . $this->tbl_rfq_products_send . '.rps_prd_id_sent', 'LEFT')
                                           ->where($this->tbl_rfq_products_send . '.rps_supplier_id', $value['rps_supplier_id'])->get($this->tbl_rfq_products_send)->result_array();
                      }
                 }
                 return $return;
            }
            return null;
       }

       function getProductImage($prdId, $imgClass = '', $style = '') {
            $products = $this->db->get_where($this->tbl_products, array('prd_id' => $prdId))->row_array();
            if (!empty($products)) {

                 $defaultImage = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $products['prd_id'], 'pimg_id' => $products['prd_default_image']))->row_array();
                 $defaultImage = isset($defaultImage['pimg_image']) ? $defaultImage['pimg_image'] : '';

                 $fistImages = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $products['prd_id']))->row_array();
                 $fistImages = isset($fistImages['pimg_image']) ? $fistImages['pimg_image'] : '';

                 $imageToShow = !empty($defaultImage) ? $defaultImage : $fistImages;
                 return img(array('src' => site_url() . 'assets/uploads/product/' . $imageToShow,
                     'style' => $style, 'class' => $imgClass));
            }
       }

       public function getUserById($id) {
            $user = $this->db->get_where($this->tbl_users, array('usr_id' => $id))->row_array();
            return $user;
       }

       function countAllStockProduct($filter = '') {

            $this->db->where('prd_status', 1);
            $this->db->where('prd_is_stock_prod', 1);
            $this->db->select('*');
            if (isset($filter['category'])) {
                 $this->db->join($this->tbl_products_categories, 'pcat_product = prd_id');
                 $this->db->where('pcat_category', $filter['category']);
            }
            if (isset($filter['prd_type']) && !empty($filter['prd_type'])) {
                 if ($filter['prd_type'] == "flash") {
                      $this->db->where('prd_is_flash_sale', 1);
                 }
                 if ($filter['prd_type'] == "offer") {
                      $this->db->where('prd_offer_price_min >', 0);
                      $this->db->where('prd_offer_price_max >', 0);
                 }
            }
            $products = $this->db->get($this->tbl_products);
            return $products->num_rows();
       }



       public function getSupplierProductDetails($id = '') {


            $this->db->select($this->tbl_products . '.*,'. $this->tbl_products_units . '.unt_unit_name_en as unt')

                    ->join($this->tbl_products_units, $this->tbl_products_units . '.unt_id = ' . $this->tbl_products . '.prd_unit', 'LEFT');

                 $products = $this->db->where($this->tbl_products . '.prd_id', $id)->get($this->tbl_products)->row_array();

                 if (!empty($products)) {
                      $products['specification'] = $this->db->order_by("psp_id", "asc")->
                                      get_where($this->tbl_products_specification, array('psp_product' => $products['prd_id']))->result_array();

                      $products['keyword'] = $this->db->get_where($this->tbl_products_keyword, array('pkwd_product' => $products['prd_id']))->result_array();

                      if (isset($products['prd_default_image']) && !empty($products['prd_default_image'])) {
                           $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $products['prd_id'], 'pimg_id' => $products['prd_default_image']))->row_array();
                      } else {
                           $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $products['prd_id']))->row_array();
                      }

                      $products['default_image'] = isset($images['pimg_image']) ? site_url() . 'assets/uploads/product/'.$images['pimg_image'] : '';


                      $products['categories'] = $this->db->get_where($this->tbl_products_categories, array('pcat_product' => $products['prd_id']))->row_array();

                      $products['catId'] = explode(',', $this->db->select('GROUP_CONCAT(pcat_category) AS pcat_category')
                                      ->where(array('pcat_product' => $products['prd_id']))
                                      ->get($this->tbl_products_categories)->row()->pcat_category);


                       $products['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $products['prd_id']))->result_array();

                        foreach ( $products['images']  as $key => $value) {
                          $products['images'][$key]['pimg_image']=  site_url() . 'assets/uploads/product/'.$value['pimg_image'];
                        }

                 }

            return $products;
       }


            public function countImages($id) {

            return  $this->db->query("SELECT COUNT(*) as total FROM  `cpnl_products_images` WHERE `pimg_product` = (SELECT `pimg_product`  FROM `cpnl_products_images` WHERE `pimg_id` = $id)")->row('total');

       }

  }
