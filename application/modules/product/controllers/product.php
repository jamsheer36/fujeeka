<?php
defined('BASEPATH') or exit('No direct script access allowed');
require './vendor/autoload.php';

class Product extends App_Controller
{
    public $mail = '';
    public function __construct()
    {
        parent::__construct();
        $this->body_class[] = 'skin-blue';
        $this->page_title = 'Product';
        $this->load->model('supplier/supplier_model', 'supplier');
        $this->load->model('category/category_model', 'category_model');
        $this->load->model('logistics/logistics_model', 'logistics');
        $this->load->model('quality_control/quality_control_model', 'quality_control');
        $this->load->model('product_model', 'product_model');
        $this->load->model('units/units_model', 'units');
        require_once APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php';
        $this->mail = new PHPMailer;
    }
    public function index()
    {
        $this->lock_in();
        if ($this->input->get('page')) {
            //we pass value $filter['countall'] to get count of active+inactive products
            // getAllProductForAjaxPagination ->model function, we need new function to render html code to view page
            $params = $this->input->get();
            if ($params['search']) {
                $filter['search'] = $params['search'];
            }
            $filter['countall'] = '';
            if ($params['status'] == 'active') {
                $filter['active'] = '';
            }
            if ($params['status'] == 'inactive') {
                $filter['inactive'] = '';
            }
            $count = $this->product_model->countAllProduct($filter);
            pagination($this->input->get('page'), base_url() . "product", $count, 'product_model', 'getAllProductForAjaxPagination', $filter);
        }
        $data['total_produts'] = $this->product_model->countAllProduct(array('countall' => ''));
        $data['total_active'] = $this->product_model->countAllProduct(array('countall' => '', 'active' => ''));
        $data['total_inactive'] = $this->product_model->countAllProduct(array('countall' => '', 'inactive' => ''));
        $this->render_page(strtolower(__CLASS__) . '/list', $data);
    }

    public function active()
    {
        $this->lock_in();
        if ($this->input->get('page')) {
            //we pass value $filter['countall'] to get count of active+inactive products
            // getAllProductForAjaxPagination ->model function, we need new function to render html code to view page
            $params = $this->input->get();
            if ($params['search']) {
                $filter['search'] = $params['search'];
            }
            $filter['countall'] = '';
            if ($params['status'] == 'active') {
                $filter['active'] = '';
            }
            if ($params['status'] == 'inactive') {
                $filter['inactive'] = '';
            }
            $count = $this->product_model->countAllProduct($filter);
            pagination($this->input->get('page'), base_url() . "product", $count, 'product_model', 'getAllProductForAjaxPagination', $filter);
        }
        $data['total_produts'] = $this->product_model->countAllProduct(array('countall' => ''));
        $data['total_active'] = $this->product_model->countAllProduct(array('countall' => '', 'active' => ''));
        $data['total_inactive'] = $this->product_model->countAllProduct(array('countall' => '', 'inactive' => ''));
        $this->render_page(strtolower(__CLASS__) . '/active_products', $data);
    }

    public function add()
    {
        $this->lock_in();
        if (!empty($_POST)) {
            if ($prdId = $this->product_model->addNewProduct($this->input->post())) {
                $this->load->library('upload');
                $x1 = $this->input->post('x12');
                $fileCount = count($x1);
                $up = array();
                for ($j = 0; $j < $fileCount; $j++) {
                    $data = array();
                    $angle = array();
                    $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                    $config['upload_path'] = FILE_UPLOAD_PATH . 'product/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['file_name'] = $newFileName;
                    $this->upload->initialize($config);
                    $angle['x1']['0'] = $_POST['x12'][$j];
                    $angle['x2']['0'] = $_POST['x22'][$j];
                    $angle['y1']['0'] = $_POST['y12'][$j];
                    $angle['y2']['0'] = $_POST['y22'][$j];
                    $angle['w']['0'] = $_POST['w2'][$j];
                    $angle['h']['0'] = $_POST['h2'][$j];
                    if ($angle['h']['0'] < 240) {
                        $this->product_model->deleteProduct($prdId);
                        $this->session->set_flashdata('app_error', "Minmum image height should be 240px");
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                    $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                    $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                    $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                    $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                    $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];
                    if (!$this->upload->do_upload('prd_image_tmp')) {
                        $up = array('error' => $this->upload->display_errors());
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                        crop($this->upload->data(), $angle, false);
                        $this->product_model->addImages(array('pimg_product' => $prdId, 'pimg_image' => $data['upload_data']['file_name'], 'pimg_alttag' => $_POST['altTags'][$j]));
                        //resize(FILE_UPLOAD_PATH . 'product/' . $data['upload_data']['file_name'], 450, 338);
                    }
                }
                $this->session->set_flashdata('app_success', 'Product successfully added!');
            } else {
                $this->session->set_flashdata('app_error', "Can't add product!");
            }
            redirect(strtolower(__CLASS__));
        } else {
            $data['units'] = $this->units->get();
//                 if ($this->session->userdata('usr_supplier') != 0) {
            //                      $data['suppliers'][0] = $this->supplier->suppliers($this->session->userdata('usr_supplier'));
            //                 } else {
            //                      $data['suppliers'] = $this->supplier->suppliers();
            //                 }
            $data['suppliers'] = $this->supplier->suppliers();
            if (!is_root_user()) {
                $data['category'] = $this->supplier->getSupplierCategories($this->session->userdata('usr_supplier'));
            }
            $this->render_page(strtolower(__CLASS__) . '/add', $data);
        }
    }

    public function view($id = '')
    {
        $this->lock_in();
        if (!$id) {
            redirect('error404');
        }
        $data['products'] = $this->product_model->getProduct($id);
        if (!$data['products']) {
            redirect('error404');
        }
        $data['units'] = $this->units->get();
        $data['suppliers'] = $this->supplier->suppliers();
        $data['category'] = $this->supplier->getSupplierCategories($data['products']['prd_supplier']);
        $this->render_page(strtolower(__CLASS__) . '/view', $data);
    }

    public function removeImage($id)
    {

        if ($this->product_model->countImages($id) > 1) {

            if ($this->product_model->removePrductImage($id)) {
                echo json_encode(array('status' => 'success', 'msg' =>
                    'Product image deleted successfully '));die();
            } else {
                echo json_encode(array('status' => 'fail', 'msg' =>
                    "Can't delete product image"));die();
            }

        } else {
            echo json_encode(array('status' => 'fail', 'msg' => "Can't delete the last image"));die();}
    }

    public function update()
    {
        $this->lock_in();
        $prdId = $this->input->post('prd_id');
        if ($this->product_model->updateProduct($this->input->post())) {
            $this->load->library('upload');
            $x1 = $this->input->post('x12');
            $fileCount = count($x1);
            $up = array();
            for ($j = 0; $j < $fileCount; $j++) {
                $data = array();
                $angle = array();
                $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                $config['upload_path'] = FILE_UPLOAD_PATH . 'product/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['file_name'] = $newFileName;
                $this->upload->initialize($config);
                $angle['x1']['0'] = $_POST['x12'][$j];
                $angle['x2']['0'] = $_POST['x22'][$j];
                $angle['y1']['0'] = $_POST['y12'][$j];
                $angle['y2']['0'] = $_POST['y22'][$j];
                $angle['w']['0'] = $_POST['w2'][$j];
                $angle['h']['0'] = $_POST['h2'][$j];
                //   if($angle['h']['0'] < 240){
                //      $this->session->set_flashdata('app_error', "Minmum image height should be 240px");
                //      redirect(strtolower(__CLASS__).'/view/'.$prdId);
                //   }
                $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];
                if (!$this->upload->do_upload('prd_image_tmp')) {
                    $up = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    crop($this->upload->data(), $angle);
                    $this->product_model->addImages(array('pimg_product' => $prdId, 'pimg_image' => $data['upload_data']['file_name'], 'pimg_alttag' => $_POST['altTags'][$j]));
                }
            }
            $this->session->set_flashdata('app_success', 'Product successfully added!');
        } else {
            $this->session->set_flashdata('app_error', "Can't add product!");
        }
        redirect(strtolower(__CLASS__));
    }

    public function delete($id)
    {
        if ($this->product_model->deleteProduct($id)) {
            echo json_encode(array('status' => 'success', 'msg' => 'Product successfully deleted'));
            die();
        } else {
            echo json_encode(array('status' => 'fail', 'msg' => "Can't delete product"));
            die();
        }
    }

    public function import()
    {
        $data['category'] = $this->category_model->categoryTree();
        $this->render_page(strtolower(__CLASS__) . '/import', $data);
    }

    public function setDefaultImage($imgId, $prodId)
    {
        if (!empty($imgId)) {
            if ($this->product_model->setDefaultImage($imgId, $prodId)) {
                echo json_encode(array('status' => 'success', 'msg' => 'Updated default image'));
                die();
            } else {
                echo json_encode(array('status' => 'fail', 'msg' => "Can't update default image"));
                die();
            }
        } else {
            return false;
        }
    }

    //  function product_listing(){
    //     $this->page_title = 'Home | Product Listing';
    //     $this->template->set_layout('product');
    //     $data['products'] = $this->product_model->getProductListing();
    //     $this->render_page(strtolower(__CLASS__) . '/product_listing.php',$data);
    //  }
    public function product_details($id)
    {
        $this->page_title = 'Home | Product Details';
        $id = encryptor($id, 'D');

        $data['products'] = $this->product_model->getProductDetails($id);
        if (!$data['products']) {
            redirect('error404');
        }
        $data['rdfrom'] = '';
        if (isset($_GET['rdfrom'])) {
            $data['rdfrom'] = $_GET['rdfrom'];
            $supId = $data['products']['prd_supplier'];
            $data['suppliers'] = $this->supplier->supplierHome($supId);
            $data['categories'] = $this->supplier->getSupplierCategories($supId);
            $data['stock'] = $this->supplier->getProducts(12, $supId, 'stock');
            $data['featured'] = $this->supplier->getProducts(12, $supId);
            $data['supplier_user_id'] = $this->common_model->getUserIdFromSupId($supId);
            $data['suppliersProducts'] = $this->supplier->getMyProducts(12, $supId);
            $this->template->set_layout('supplier_home');
        } else {
            if (isset($_COOKIE['recent'])) {
                $recent = json_decode($_COOKIE['recent'], true);
            }
            $recent[] = $id;
            set_cookie('recent', json_encode($recent), '3600');
            $this->template->set_layout('product');
        }
        if ($data['products']) {
            $data['supplier_user_id'] = $this->common_model->getUserIdFromSupId($data['products']['supm_id']);
        }

        //print_r($data);die();
        $this->render_page(strtolower(__CLASS__) . '/product_details.php', $data);
    }

    public function product_listing()
    {
        $filter = array();
        if ($this->input->get('page')) {
            $params = $this->input->get();
            if ($params['category']) {
                $filter['category'] = $params['category'];
            }
            if ($params['suplier']) {
                $filter['suplier'] = $params['suplier'];
            }
            if ($params['prd_type']) {
                $filter['prd_type'] = $params['prd_type'];
            }
            if ($params['searchText']) {
                $filter['searchText'] = $params['searchText'];
            }
            $count = $this->product_model->countAllProduct($filter);
            if ($params['order_by']) {
                $filter['order_by'] = $params['order_by'];
            }
            pagination($this->input->get('page'), base_url() . "product/product_listing", $count, 'product_model', 'getProductListing', $filter);
        }
        $data['categories'] = $this->common_model->getAllCategories();
        $this->page_title = 'Home | Product Listing';
        $this->template->set_layout('product');
        $data['searchString'] = isset($_GET['SearchText']) ? $_GET['SearchText'] : '';
        $data['type'] = isset($_GET['type']) ? $_GET['type'] : '';
        $this->render_page(strtolower(__CLASS__) . '/product_listing.php', $data);
    }

    public function productAutoSpecification($language = 'en')
    {
        $query = isset($_GET['query']) ? $_GET['query'] : '';
        $reply['suggestions'] = $this->product_model->productAutoSpecification($language, $query);
        echo json_encode($reply);
        exit;
    }

    public function supplier_products($id)
    {
        //   $this->lock_in();
        $data['supplier'] = encryptor($id, 'D');
        $this->page_title = 'Home | Supplier Products';
        $this->template->set_layout('product');
        $data['categories'] = $this->common_model->getAllCategories();
        $this->render_page(strtolower(__CLASS__) . '/product_listing.php', $data);
    }

    /**
     * Function related to submit supplier contact for
     * Author : JK
     * @param int $id
     */
    public function contact_supplier($id)
    {

        if (!get_logged_user('usr_is_mail_verified')) {
            $this->session->set_flashdata('app_success', 'You should verify your email to request a quote');
            redirect($_SERVER['HTTP_REFERER']);

        }
        $this->template->set_layout('');
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata(array('callback' => site_url(strtolower(__CLASS__)) . '/contact-supplier' . '/' . $id));
            redirect('user/login');
        }
        $this->page_title = 'Home | Contact Supplier';
        $id = encryptor($id, 'D');
        $data['captcha'] = getCiCaptcha();
        $data['product'] = $this->product_model->getProduct($id);
        $data['units'] = $this->product_model->getUnits();
        $this->render_page(strtolower(__CLASS__) . '/contact_supplier', $data);
    }

    /**
     * Process product order
     * Author : JK
     */
    public function doContactSupplier()
    {
        $this->form_validation->set_rules('ord_subject', 'subject', 'required');
        if ($this->form_validation->run() == true && (isset($_POST['chkConfirm']) && ($_POST['chkConfirm'] == 1))) {
            if (isset($_POST['chkConfirm'])) {
                unset($_POST['chkConfirm']);
            }
            $_POST['ord_added_by'] = $this->uid;
            $_POST['ord_number'] = gen_random();
            $_POST['ord_buyer'] = $this->uid;
            $pridId = encryptor($_POST['ord_prod_id'], 'D');
            $_POST['ord_prod_id'] = $pridId;
            $_POST['ord_supplier'] = encryptor($_POST['ord_supplier'], 'D');
            $this->load->library('upload');
            $newFileName = microtime(true) . $_FILES['ord_attachment']['name'];
            $config['upload_path'] = FILE_UPLOAD_PATH . 'product_enquiry/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|txt';
            $config['file_name'] = $newFileName;
            $config['max_size'] = 2100;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('ord_attachment')) {
                $up = $this->upload->display_errors();
            } else {
                $data = $this->upload->data();
                $_POST['ord_attachment'] = isset($data['file_name']) ? $data['file_name'] : '';
            }
            unset($_POST['captcha_code']);
            unset($_POST['usr_captcha']);
            $lastInsertId=$this->product_model->placeOrder($_POST);
            $this->session->set_flashdata('app_success', 'Inquiry submitted successfully. Will get back to you!');

        $det = $this->product_model->getUserById($_POST['ord_supplier']);
        if($det['usr_pushy_token'])
   
        {
        $this->load->model('supplier_api/supplier_api_model', 'supplier_api');
        $push = $this->supplier_api->getOrders($_POST['ord_supplier'],$lastInsertId,array());

        $push['msg_type'] = 'new_inquiry';
        $this->common_model->sendPushNotificationForSupplier($push,
                    $det['usr_pushy_token'], '');
        }
     

            redirect('product/product-details/' . encryptor($pridId));
        } else {
            debug(validation_errors(), 0);
        }
    }

    public function rfq()
    {
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata(array('callback' => site_url(strtolower(__CLASS__)) . '/rfq'));
            redirect('user/login');
        }
        if ($this->input->post('rfq_page')) {
            $this->form_validation->set_rules('category', 'product', 'rfq_qty', 'rfq_mail', 'required');
            if ($this->form_validation->run() == true && (isset($_POST['chkConfirm']))) {
                if (isset($_POST['chkConfirm'])) {
                    unset($_POST['chkConfirm']);
                }
                $_POST['rfq_product_name'] = $_POST['pname'];
                unset($_POST['pname']);
                unset($_POST['rfq_page']);
                //   if (!$_POST['rfq_product']) {
                //        $this->session->set_flashdata('app_error', "Product not available select another");
                //        redirect('product/rfq');
                //   }
                $_POST['rfq_user'] = $this->uid;
                if (isset($_FILES['rfq_attachment']['name']) && !empty($_FILES['rfq_attachment']['name'])) {
                    $this->load->library('upload');
                    $newFileName = microtime(true) . $_FILES['rfq_attachment']['name'];
                    $config['upload_path'] = FILE_UPLOAD_PATH . 'rfq/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|txt';
                    $config['file_name'] = $newFileName;
                    $config['max_size'] = 2100;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('rfq_attachment') && !empty($_FILES['rfq_attachment']['name'])) {
                        $up = $this->upload->display_errors();
                        $this->session->set_flashdata('app_error', $up);
                        redirect('product/rfq');
                    } else {
                        $data = $this->upload->data();
                        $_POST['rfq_attachment'] = isset($data['file_name']) ? $data['file_name'] : '';
                    }
                }
                unset($_POST['fujeeka_csrf']);
                $this->product_model->insertRfq($_POST);

        $det = $this->product_model->getUserById($_POST['ord_supplier']);
        if($det['usr_pushy_token'])
   
        {
        $this->load->model('supplier_api/supplier_api_model', 'supplier_api');
        $push = $this->supplier_api->getOrders($_POST['ord_supplier'],$lastInsertId,array());
        $push['type'] = 'recieved';
        $push['msg_type'] = 'new_inquiry';
        $this->common_model->sendPushNotificationForSupplier($push,
                    $det['usr_pushy_token'], '');
        }


                $this->session->set_flashdata('app_success', 'Request submitted, Will get back to you!');
                redirect('product/rfq');
            } else {
                debug(validation_errors(), 0);
            }
        }
        $data['categ'] = $data['mar'] = $data['prd_name'] = $data['prd_unit'] = '';
        $data['prd_id'] = 0;
        if ($this->input->post('from_footer')) {
            $data['categ'] = $this->input->post('rfq_category');
            $data['mar'] = $this->input->post('markets');
            $data['prd_name'] = $this->input->post('pname');
            $data['prd_id'] = $this->input->post('rfq_product');
            $data['prd_unit'] = $this->input->post('unit');
        }
        $this->template->set_layout('');
        $this->load->model('market/market_model', 'market');
        $this->page_title = "REQUEST FOR QUOTE";
        $data['categories'] = $this->common_model->getAllCategories();
        $data['units'] = $this->product_model->getUnits();
        $data['marketPlaces'] = $this->market->gerMarketPlaces();
        $data['captcha'] = getCiCaptcha();
        $this->render_page(strtolower(__CLASS__) . '/rfq', $data);
    }

    public function productAutoName($category = '')
    {
        $query = isset($_GET['query']) ? $_GET['query'] : '';
        $reply['suggestions'] = $this->product_model->productAutoName($query, $category);
        echo json_encode($reply);
        exit;
    }

    public function getSupplierNotification()
    {
        $this->product_model->getSupplierNotification();
    }

    public function rfq_list($id = '')
    {
        $this->lock_in();
        if (!$this->mail_verified) {
            $this->session->set_flashdata('app_error', 'Please verify your email');
            redirect('dashboard');
        }
        $this->page_title = "RFQ List";
        if ($id) {
            $this->page_title = "RFQ Details";
            $id = encryptor($id, 'D');
            $data['suppliers'] = $this->product_model->getSuppliersAssocRFQ($id);
            $data['rfq'] = $this->product_model->getRfq($id);
            $category = isset($data['rfq']['rfq_category']) ? $data['rfq']['rfq_category'] : 0;
            $data['rfqRelatedProducts'] = $this->product_model->RFQretaledProducts($category);
            $data['productsSentBySupplier'] = array();
            if ($this->usr_grp == 'BY') {
                $data['productsSentBySupplier'] = $this->product_model->productsSentBySupplier($id);
            }
            $this->render_page(strtolower(__CLASS__) . '/rfq_details', $data);
        } else {
            $data['rfq'] = $this->product_model->getRfq();
            $this->render_page(strtolower(__CLASS__) . '/rfq_list', $data);
        }
    }

    public function report()
    {

        $date1 = isset($_POST['start_date']) ? $_POST['start_date'] : null;
        $date2 = isset($_POST['end_date']) ? $_POST['end_date'] : null;

        if ($date1 && $date2) {
            $exportData = $this->product_model->getRfq(null, $date1, $date2);
            if (empty($exportData)) {

                $this->session->set_flashdata('app_error', "No Data exist between $date1 and $date2");
                redirect(strtolower(__CLASS__) . '/rfq_list');

            } else {

                foreach ($exportData as $key => $value) {

                    $content[$key]['Sl No'] = $key + 1;
                    $content[$key]['Buyer Name'] = $value['usr_first_name'] . " " . $value['usr_last_name'];
                    $content[$key]['Buyer Email'] = isset($value['usr_email']) ? $value['usr_email'] : '';
                    $content[$key]['Buyer Phone'] = isset($value['usr_phone']) ? $value['usr_phone'] : '';
                    $content[$key]['Product Category'] = $value['cat_title'];
                    $content[$key]['Product Name'] = $value['rfq_product_name'];
                    $content[$key]['Product Quantity'] = $value['rfq_qty'] . " " . $value['unt_unit_name_en'];
                    $content[$key]['Product Requirment'] = $value['rfq_requirment'];
                    $content[$key]['Validity'] = $value['rfq_validity'];

                }
            }
            if ($_POST['type'] == 'excel') {
                $this->load->library('excel');
                $this->excel->stream("Rfq Report ($date1 -$date2)-" . now() . " .xls", $content);
            } else {
                $html = $this->getHtmlTable($content);
                $pdfFilePath = "Rfq-Report (" . $date1
                . "-" . $date2 . ")-" . now() . ".pdf";
                $this->load->library('m_pdf');
                $this->m_pdf->pdf->WriteHTML($html);
                $this->m_pdf->pdf->Output($pdfFilePath, "D");
                $this->m_pdf->pdf->Output($serverpath, 'F');

            }

            $this->session->set_flashdata('app_success', 'Report Generated Succesfully');
            redirect(strtolower(__CLASS__) . '/rfq_list');
        } else {
            $this->session->set_flashdata('app_error', 'Invalid dates');
            redirect(strtolower(__CLASS__) . '/rfq_list');

        }

    }

    public function getHtmlTable($exportData)
    {

        $start = "<style type='text/css'>
                      .tg  {border-collapse:collapse;border-spacing:0;border-width:1px;border-style:solid;border-color:#aaa;}
                      .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
                      .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}
                      .tg .tg-7d57{background-color:#FCFBE3;border-color:inherit;text-align:left;vertical-align:top}
                      .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
                      @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>
                      <div class='tg-wrap'><table class='tg'>
                        <tr>
                         <th class='tg-0pky'><span style='font-weight:bold'>Sl No</span></th>
                          <th class='tg-0pky'><span style='font-weight:bold'>Buyer Name</span></th>
                          <th class='tg-0pky'><span style='font-weight:bold'>Buyer Email</span></th>
                          <th class='tg-0pky'><span style='font-weight:bold'>Buyer Phone</span></th>
                          <th class='tg-0pky'><span style='font-weight:bold'>Product Category</span></th>
                          <th class='tg-0pky'><span style='font-weight:bold'>Product Name</span></th>
                          <th class='tg-0pky'><span style='font-weight:bold'>Product Quantity</span></th>
                          <th class='tg-0pky'><span style='font-weight:bold'>Product Requirment</span></th>
                          <th class='tg-0pky'><span style='font-weight:bold'>Validity</span></th>

                        </tr>";
        $tr = '';
        foreach ($exportData as $key => $td) {

            $tr = $tr . "<tr>
                                 <td class='tg-7d57'>" . $td['Sl No'] . "</td>
                                  <td class='tg-7d57'>" . $td['Buyer Name'] . "</td>
                                  <td class='tg-7d57'>" . $td['Buyer Email'] . "</td>
                                  <td class='tg-7d57'>" . $td['Buyer Phone'] . "</td>
                                  <td class='tg-7d57'>" . $td['Product Category'] . "</td>
                                  <td class='tg-7d57'>" . $td['Product Name'] . "</td>
                                  <td class='tg-7d57'>" . $td['Product Quantity'] . "</td>
                                  <td class='tg-7d57'>" . $td['Product Requirment'] . "</td>
                                  <td class='tg-7d57'>" . $td['Validity'] . "</td>

                                </tr>";

        }
        return $start . $tr . "</table></div>";

    }

    public function sendRFQComments()
    {
        $post_det = $this->input->post();
        $det = $this->product_model->getUserById($post_det['rfqn_supplier']);
        $chat['message'] = $post_det['rfqn_comments'];
        $chat['type'] = 'rfq';
        $chat['time'] = date('h:i A') . ' | ' . date('M d');
        $chat['from'] = $this->session->userdata('usr_username');
        $chat['rfq_id'] = $post_det['rfqn_rfq_id'];
        $chat['rfq_link'] = site_url('product/rfq-list/' . encryptor($post_det['rfqn_rfq_id']));
        $chat['from_img'] = 'assets/uploads/avatar/' . $this->session->userdata('usr_avatar');
        $res = $this->sendPusherMessage(encryptor($post_det['rfqn_supplier']), 'rfq-event', $chat);
        if ($det['usr_pushy_token']) {
            $push['type'] = 'recieved';
            $push['msg_type'] = 'rfq_chat_notfication';
            $sup_det = $this->product_model->getUserById($post_det['rfqn_added_by']);
            $push['from_id'] = encryptor($sup_det['usr_supplier']);
            $push['content'] = $post_det['rfqn_comments'];
            $push['from_name'] = $this->session->userdata('usr_username');
            $push['time'] = date("Y-m-d H:i:s");
            $push['rfq_id'] = $post_det['rfqn_rfq_id'];

            if ($isSup = $this->common_model->checkIsSupplier($post_det['rfqn_supplier'])) {
                $push['from_id'] = $sup_det['usr_supplier'];
                $this->common_model->sendPushNotificationForSupplier($push,
                    $det['usr_pushy_token'], '');
            } else {
                $this->common_model->sendPushNotification($push, $det['usr_pushy_token'], '');
            }
        }
        $newOne = $this->product_model->sendRFQComments($post_det);
        die(json_encode(array('status' => 'success', 'design' => '')));
    }

    public function loadRFQComments($rfq_id)
    {
        $data['rfqDetails'] = $this->product_model->getRfq($rfq_id);
        $data['rfq'] = $this->product_model->loadRFQComments($this->input->post('ids'), $rfq_id, $this->input->post('from'), $this->input->post('to'));
        $design = $this->load->view('rfq-chatting', $data, true);
        die(json_encode(array('status' => 'success', 'design' => $design)));
    }

    public function loadSeperateRFQComment($thirdPartyId, $rfq_id)
    {
        $data['rfqDetails'] = $this->product_model->getRfq($rfq_id);
        $data['rfq'] = $this->product_model->loadSeperateRFQComment($rfq_id, $this->input->post('from'), $this->input->post('to'));
        $design = $this->load->view('rfq-chatting', $data, true);
        die(json_encode(array('status' => 'success', 'design' => $design)));
    }

    /**
     * Function for change product status.
     * @param type $prodId
     * Author : JK
     */
    public function changestatus($prodId)
    {
        $prodId = encryptor($prodId, 'D');
        $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
        if ($this->common_model->changeStatus($prodId, $ischecked, 'products', 'prd_status', 'prd_id')) {
            generate_log(array(
                'log_title' => 'Status changed',
                'log_desc' => 'Product status changes',
                'log_controller' => 'product-status-changed',
                'log_action' => 'U',
                'log_ref_id' => $prodId,
                'log_added_by' => $this->uid,
            ));
            $msg = ($ischecked == 1) ? "Activated this record successfully" : "De-activated this record successfully";
            die(json_encode(array('status' => 'success', 'msg' => $msg)));
        } else {
            die(json_encode(array('status' => 'fail', 'msg' => "Error occured")));
        }
    }

    public function sendRfqQuote()
    {
        if ($this->input->post() && $this->usr_grp != "BY") {
            $suplier = get_logged_user('usr_supplier');
            $rfq_id = $this->input->post('rfq_id');
            $content = $this->input->post('reply');
            $to = $this->input->post('bussiness_mail');
            $this->mail->isSMTP();
            $this->mail->Host = MAIL_HOST;
            $this->mail->SMTPAuth = true;
            $this->mail->Username = MAIL_USERNAME;
            $this->mail->Password = MAIL_PASSWORD;
            $this->mail->SMTPSecure = 'ssl';
            $this->mail->Port = 465;
            // $this->mail->SMTPDebug = 4;
            //   $this->mail->SMTPOptions = array(
            //       'ssl' => array(
            //           'verify_peer' => false,
            //           'verify_peer_name' => false,
            //           'allow_self_signed' => true,
            //       ),
            //   );
            $this->mail->setFrom(FROM_MAIL, FROM_NAME);
            $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
            $this->mail->addAddress($to);
            $this->mail->Subject = 'RFQ Quote';
            $this->mail->isHTML(true);
            $mailContent = $content;
            $this->mail->Body = $mailContent;
            //   if ($this->mail->send()) {
            $res = $this->product_model->updateRfqStatusAndCount($suplier, $this->input->post());
            if ($res) {
                $this->session->set_flashdata('app_success', 'RFQ Successfully Sent');
                redirect('product/rfq_list');
            } else {
                $this->session->set_flashdata('app_error', 'Failed to sent reply');
                redirect('product/rfq_list');
            }
        } else {
            $this->session->set_flashdata('app_error', 'You Dont have permission to reply');
            redirect('product/rfq_list');
        }
    }

    public function search($element, $page = 0)
    {
        $limit = 10;
        $seg = isset($_GET['seg']) ? slugify(trim($_GET['seg'])) : '';

        if ($seg == 'logistics') {
            $logSearch = $this->logistics->searchLogistics(urldecode($element), $limit, $page);
            $data['serachList'] = $logSearch['userDetails'];
            $totalCount = $logSearch['count'];
        } else if ($seg == 'quality-control') {
            $qcSearch = $this->quality_control->searchQC(urldecode($element), $limit, $page);
            $data['serachList'] = $qcSearch['userDetails'];
            $totalCount = $qcSearch['count'];
        } else {
            $data['serachList'] = $this->product_model->stuffSearch(urldecode($element), $limit, $page);
            $totalCount = count($data['serachList']);
        }
        $this->load->library('pagination');
        $cfg = getPaginationDesign();
        if (count($_GET) > 0) {
            $cfg['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $cfg['base_url'] = base_url() . 'stuffs/search/' . $element . '/';
        $cfg['use_page_numbers'] = true;
        $cfg['uri_segment'] = 4;
        $cfg['per_page'] = $limit;
        $cfg['total_rows'] = $totalCount;
        $this->pagination->initialize($cfg);
        $data['pagination'] = $this->pagination->create_links();
        $data['serachList'] = $data['serachList'];
        $this->template->set_layout('product');
        $catId = $this->input->get('cid');
        $data['cid'] = $catId;
        $data['scid'] = $this->input->get('scid');
        $data['sort'] = $this->input->get('sort');
        $data['categories'] = $this->common_model->getAllCategories();
        $data['subcategories'] = !empty($catId) ? $this->common_model->getAllCategories($catId) : array();
        $data['searchString'] = $element;
        $data['segments'] = (isset($_GET['seg']) && !empty($_GET['seg'])) ? ucfirst($_GET['seg']) : '';
        if ($seg == 'logistics') {
            $this->render_page(strtolower(__CLASS__) . '/search_logistics', $data);
        } else if ($seg == 'quality-control') {
            $this->render_page(strtolower(__CLASS__) . '/search_qc', $data);
        } else if ($seg == 'supplier') {
            $this->render_page(strtolower(__CLASS__) . '/search_supplier', $data);
        } else {
            $this->render_page(strtolower(__CLASS__) . '/search_product', $data);
        }
    }

    public function editorImageUpload()
    {
        if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
            $this->load->library('upload');
            $uploadData = array();
            $pixel = array();
            $newFileName = microtime(true) . $_FILES['file']['name'];
            $config['upload_path'] = FILE_UPLOAD_PATH . 'redactor/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['file_name'] = $newFileName;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('file')) {
                $uploadData = $this->upload->data();
                $file = $uploadData['file_name'];
                $array = array(
                    'filelink' => base_url() . $config['upload_path'] . $file,
                );
                echo stripslashes(json_encode($array));
                die();
            } else {
                echo json_encode(array('error' => $this->upload->display_errors('', '')));
                die();
            }
        }
    }

    /**
     * Show products under category
     * @param string $slug
     * @param id $id
     * @param int $page
     * Author : JK
     */
    public function category($slug, $id, $page = 0)
    {
        $limit = 12;
        $supId = isset($_GET['supid']) ? encryptor($_GET['supid'], 'D') : '';
        $id = encryptor($id, 'D');
        $this->load->library('pagination');
        $cfg = getPaginationDesign();
        $cfg['base_url'] = base_url() . 'product/category/' . $slug . '/' . encryptor($id) . '/';
        $cfg['use_page_numbers'] = true;
        $cfg['uri_segment'] = 5;
        $cfg['per_page'] = $limit;
        $cfg['total_rows'] = count($this->product_model->getProductsByCategory($id, 0, 0, true, $supId));
        $this->pagination->initialize($cfg);
        $data['pagination'] = $this->pagination->create_links();
        $data['categories'] = $this->common_model->getAllCategories();
        $data['subcategories'] = !empty($id) ? $this->common_model->getAllCategories($id) : array();
        $data['serachList'] = $this->product_model->getProductsByCategory($id, $limit, $page, false, $supId);
        $data['cid'] = $id;
        $data['scid'] = $this->input->get('scid');
        $data['sort'] = $this->input->get('sort');
        if (isset($_GET['rdfrom'])) {
            $data['rdfrom'] = $_GET['rdfrom'];
            $data['suppliers'] = $this->supplier->supplierHome($supId);
            $data['categories'] = $this->supplier->getSupplierCategories($supId);
            $data['stock'] = $this->supplier->getProducts(12, $supId, 'stock');
            $data['featured'] = $this->supplier->getProducts(12, $supId);
            $data['supplier_user_id'] = $this->common_model->getUserIdFromSupId($supId);
            $data['suppliersProducts'] = $this->supplier->getMyProducts(12, $supId);
            $this->template->set_layout('supplier_home');
            $data['rdfromParam'] = '?rdfrom=profile';
        } else {
            $this->template->set_layout('product');
        }
        $this->render_page(strtolower(__CLASS__) . '/product_by_category', $data);
    }

    public function pending_rfq_list()
    {
        $this->product_model->pendingRFQList();
    }

    public function new_rfq()
    {
        if (!$this->mail_verified) {
            $this->session->set_flashdata('app_error', 'Please verify your email');
            redirect('dashboard');
        }
        $this->load->model('market/market_model', 'market');
        $data['categories'] = $this->common_model->getAllCategories();
        $data['units'] = $this->product_model->getUnits();
        $data['marketPlaces'] = $this->market->gerMarketPlaces();
        $data['captcha'] = getCiCaptcha();
        $this->render_page(strtolower(__CLASS__) . '/new_rfq', $data);
    }

    public function stock()
    {
        $filter = array();
        if ($this->input->get('page')) {
            $params = $this->input->get();
            if ($params['category']) {
                $filter['category'] = $params['category'];
            }
            $filter['prd_type'] = $params['prd_type'];
            $count = $this->product_model->countAllStockProduct($filter);
            if ($params['order_by']) {
                $filter['order_by'] = $params['order_by'];
            }
            pagination($this->input->get('page'), base_url() . "product/stock", $count, 'product_model', 'getStockProducts', $filter);
        }
        $this->page_title = 'Home | Stocks';
        $this->template->set_layout('');
        // $data['stock'] = $this->product_model->getStockProducts();
        $data['type'] = isset($_GET['type']) ? $_GET['type'] : '';
        $data['categories'] = $this->common_model->getAllCategories();
        $this->render_page(strtolower(__CLASS__) . '/stock.php', $data);
    }
    public function sendPusherMessage($channel, $event, $chat)
    {
        $options = array(
            'cluster' => PUSHER_CLUSTER,
            'useTLS' => true,
        );
        $pusher = new Pusher\Pusher(
            PUSHER_APP_KEY, PUSHER_APP_SECRET, PUSHER_APP_ID, $options
        );
        return $pusher->trigger($channel, $event, $chat);
    }

    public function product_report()
    {

        $date1 = isset($_POST['start_date']) ? $_POST['start_date'] : null;
        $date2 = isset($_POST['end_date']) ? $_POST['end_date'] : null;

        if ($date1 && $date2) {
            $exportData = $this->product_model->getProduct(0, array('date1' => $date1, 'date2' => $date2, 'countall' => '', 'active' => ''));

            if (empty($exportData)) {

                $this->session->set_flashdata('app_error', "No Data exist between $date1 and $date2");
                redirect(strtolower(__CLASS__) . '/rfq_list');

            } else {
                foreach ($exportData as $key => $value) {

                    $content[$key]['Sl No'] = $key + 1;

                    $content[$key]['Product Number'] = $value['prd_number'];
                    $content[$key]['Product Name'] = $value['prd_name_en'];
                    $content[$key]['Product Status'] = ($value['prd_status'] == 1) ? 'Active' : 'Inactive';
                    $content[$key]['Product Prize (min - max)'] = $value['prd_price_min'] . "-" . $value['prd_price_max'];
                    $content[$key]['Offer Prize (min - max)'] = $value['prd_offer_price_min'] . "-" . $value['prd_offer_price_max'];
                    $content[$key]['Product Added Date'] = isset($value['prd_added_on']) ? $value['prd_added_on'] : '';

                    $content[$key]['Product quantity'] = $value['prd_qty'] . " " . $value['unt_unit_name_en'];

                    $content[$key]['Supplier Name'] = $value['supm_name_en'];
                    $content[$key]['Supplier Email'] = isset($value['supm_email']) ? $value['supm_email'] : '';
                    $content[$key]['Supplier Phone'] = isset($value['supm_number']) ?
                    $value['supm_number'] : '';

                    $content[$key]['Supplier City'] = isset($value['supm_city_en']) ?
                    $value['supm_city_en'] : '';

                }
            }
            if ($_POST['type'] == 'excel') {
                $this->load->library('excel');
                $this->excel->stream("Product Report ($date1 -$date2)-" . now() . " .xls", $content);
            } else {
                $html = $this->getHtmlTabledata($content);
                $pdfFilePath = "Product-Report (" . $date1
                . "-" . $date2 . ")-" . now() . ".pdf";
                $this->load->library('m_pdf');
                $this->m_pdf->pdf->WriteHTML($html);
                $this->m_pdf->pdf->Output($pdfFilePath, "D");
                $this->m_pdf->pdf->Output($serverpath, 'F');

            }

            $this->session->set_flashdata('app_success', 'Report Generated Succesfully');
            redirect(strtolower(__CLASS__) . '/rfq_list');
        } else {
            $this->session->set_flashdata('app_error', 'Invalid dates');
            redirect(strtolower(__CLASS__) . '/rfq_list');

        }

    }

    public function getHtmlTabledata($exportData)
    {

        $start = "<style type='text/css'>
                  .tg  {border-collapse:collapse;border-spacing:0;border-width:1px;border-style:solid;border-color:#aaa;}
                  .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
                  .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}
                  .tg .tg-7d57{background-color:#FCFBE3;border-color:inherit;text-align:left;vertical-align:top}
                  .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
                  @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>
                  <div class='tg-wrap'><table class='tg'>
                    <tr>
                     <th class='tg-0pky'><span style='font-weight:bold'>Sl No</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Product Number</span></th>

                      <th class='tg-0pky'><span style='font-weight:bold'>Product Name</span></th>

                      <th class='tg-0pky'><span style='font-weight:bold'>Product Satus</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Product Prize (min - max)</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Offer Prize (min - max)</span></th>
                         <th class='tg-0pky'><span style='font-weight:bold'>Product quantity</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Product Added Date</span></th>

                      <th class='tg-0pky'><span style='font-weight:bold'>Supplier Name</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Supplier Email</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Supplier Phone</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Supplier City</span></th>
                    </tr>";
        $tr = '';
        foreach ($exportData as $key => $td) {

            $tr = $tr . "<tr>
                       <td class='tg-7d57'>" . $td['Sl No'] . "</td>
                       <td class='tg-7d57'>" . $td['Product Number'] . "</td>
                       <td class='tg-7d57'>" . $td['Product Name'] . "</td>
                       <td class='tg-7d57'>" . $td['Product Status'] . "</td>
                      <td class='tg-7d57'>" . $td['Product Prize (min - max)'] . "</td>
                      <td class='tg-7d57'>" . $td['Offer Prize (min - max)'] . "</td>
                      <td class='tg-7d57'>" . $td['Product quantity'] . "</td>
                      <td class='tg-7d57'>" . $td['Product Added Date'] . "</td>
                      <td class='tg-7d57'>" . $td['Supplier Name'] . "</td>
                      <td class='tg-7d57'>" . $td['Supplier Email'] . "</td>
                      <td class='tg-7d57'>" . $td['Supplier Phone'] . "</td>
                      <td class='tg-7d57'>" . $td['Supplier City'] . "</td>
                    </tr>";

        }
        return $start . $tr . "</table></div>";

    }
    public function duplicate($id = '')
    {
        $this->lock_in();
        $id = encryptor($id, 'D');
        if (!$id) {
            redirect('error404');
        }
        $data['products'] = $this->product_model->getProduct($id);
        if (!$data['products']) {
            redirect('error404');
        }
        $data['from'] = 'duplicate';
        $data['units'] = $this->units->get();
        $data['suppliers'] = $this->supplier->suppliers();
        $data['category'] = $this->supplier->getSupplierCategories($data['products']['prd_supplier']);
        $this->render_page(strtolower(__CLASS__) . '/view', $data);
    }

}
