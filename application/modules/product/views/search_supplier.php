<div class="total-container">
     <div class="container">
          <div class="main-area">
               <!-- tab 2 -->
               <div id="suppliers" class="tab-pane">
                    <!-- filters -->
                    <div class="row">
                         <div class="filter-container">
                              <div class="col-9">
                                   <!-- <form>
                                        <label class="checkbox-inline">
                                             <input type="checkbox" value="">Verified Manufacturers
                                        </label>
                                        <label class="checkbox-inline">
                                             <input type="checkbox" value="">Accepts Small Orders
                                        </label>
                                        <label class="checkbox-inline">
                                             <input type="checkbox" value="">Accepts Sample Orders
                                        </label>
                                   </form> -->
                              </div>

                              <div class="col-3">
                                   <!-- <div class="sort">
                                        <label for="sel1">Sort by:</label>
                                        <select class="form-control" id="sel1">
                                             <option>Best Match</option>
                                             <option>Transaction Level</option>
                                             <option>Response Rate</option>
                                        </select>
                                   </div> -->
                              </div>
                              <div class="clearfix"></div>
                         </div>
                    </div>
                    <!--/ filters -->
                    <div class="row">
                         <!-- supplier container -->
                         <div class="supplier-container">
                              <!-- single supplier -->
                              <?php
                                foreach ((array) $serachList as $supplierID => $products) {
                                     $this->load->model('supplier/supplier_model', 'supplier');
                                     $supplierDetails = $this->supplier->suppliers($supplierID);
                                     $supplier_user_id = $supplierDetails['supplier_user_id'];
                                     ?>
                                     <div class="single-supplier">
                                          <div class="prod-box">
                                               <h4 class="prod-title-b">
                                                    <a href="<?="http://".$supplierDetails['supm_domain_prefix'] .'.'.$this->http_host?>"><?php echo $supplierDetails['supm_name_en']?></a>
                                               </h4>
                                               <div class="prod-img">
                                                    <ul>
                                                         <?php
                                                         if (isset($products['products']) && !empty($products['products'])) {
                                                              foreach ($products['products'] as $key => $value) {
                                                                   if($key <= 2) {
                                                                   ?>
                                                                   <li>
                                                                        <a href="<?php echo site_url('product/product-details/' . encryptor($value['prd_id']));?>">
                                                                             <div class="prod">
                                                                                  <div class="img">
                                                                                       <?php echo img(array('src' => $value['sup_default_image']));?>
                                                                                  </div>
                                                                                  <h5><?php echo get_snippet($value['prd_name_en'], 5);?></h5>
                                                                             </div>
                                                                        </a>
                                                                   </li>
                                                                   <?php
                                                                   }
                                                              }
                                                         }
                                                         ?>
                                                    </ul>
                                                    <p class="float-left"><a href="<?php echo site_url('supplier/supplier-product/' . encryptor($supplierDetails['supm_id']));?>">+ view all products</a></p>
                                               </div>

                                               <div class="prod-detail txt-height">
                                                    <div class="company-details">
                                                         <p class="year"><?php
                                                              $regYear = isset($supplierDetails['supm_reg_year']) ? $supplierDetails['supm_reg_year'] : 0;
                                                              echo date('Y') - $regYear;
                                                              ?><sup>th</sup> year
                                                         </p>
                                                         <p class="loc">Main Products : <span><?=implode(', ', array_column($supplierDetails['building'], 'cat_title'));?></span><br>
                                                              Location : <span><?php echo isset($supplierDetails['supm_city_en']) ?
                                                               $supplierDetails['supm_city_en'] : ''
                                                              ?></span></br>
                                                              <!-- Top 3 Markets : <span>South America, Africa, Western Europe</span> <br> -->
                                                              Response : <span>High / Less than 24h</span></p>
                                                         <div class="icon-details">
                                                              <div class="icon verified">Verified Information</div>
                                                              <div class="icon gold-plus">Gold Plus Supplier</div>
                                                         </div>
                                                         <div class="clearfix"></div>
                                                         <!-- <div class="contcts">
                                                              <a href="javascript:;" class="btn btn-contact float-left">Contact Supplier</a>
                                                              <?php 

                                                       if ($this->uid != $supplier_user_id['usr_id']) {
                                                            if ($this->uid && !get_logged_user('usr_is_mail_verified')) {
                                                                 ?> 
                                                                 <a id="chat"  href="javascript:;" data-toggle="modal"  data-target="#verif-mail" class="chat float-right" 
                                                                 data-toggle="tooltip" title="Chat with us!">
                                                                   <i class="fas fa-comments" ></i> Chat with us</a>
                                                            <?php } else {
                                                               $name = $supplierDetails['supm_name_en'];
                                                              if (strlen($supplierDetails['supm_name_en']) > 20) {
                                                                 $name = substr($supplierDetails['supm_name_en'], 0, 20) . '...';
                                                                 } ?>
                                                              <a id="chat" data-from="search" href="javascript:;" data-sup_name="<?=$name?>" data-sup_usr_id="<?php echo encryptor($supplier_user_id['usr_id']); ?>" <?php if (!$this->uid) {?> onclick="location.href = '<?php echo site_url('user/chat/' . encryptor($supplier_user_id['usr_id']));?>'" <?php } else {?> onclick="loadChatbox()" <?php }?> class="chat float-right" 
                                                                 data-toggle="tooltip" title="Chat with us!">
                                                                   <i class="fas fa-comments" ></i> Chat with us</a>
                                                       <?php }} ?>
                                                              <div class="clearfix"></div>
                                                         </div> -->
                                                    </div>
                                               </div>
                                          </div>
                                     </div>
                                <?php }
                              ?>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<!-- Chat DIV -->
<div class="chatbox" id="chatbox">
     <span class="chat-text" id="chat_name_span"></span>
     <iframe id="ddd" src="" scrolling="no" frameborder="0" width="280px" height="450px" style="border:0; margin:0; padding: 0;">
     </iframe>       
     <div id="close-chat" onclick="closeChatbox()">&times;</div>
     <div id="minim-chat" onclick="minimChatbox()"><span class="minim-button">&minus;</span></div>
     <div id="maxi-chat" onclick="loadChatbox()"><span class="maxi-button">&plus;</span></div>
</div>
<!-- /Chat DIV -->            