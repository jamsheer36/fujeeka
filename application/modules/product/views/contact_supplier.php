<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Fujishka">
    <base href="<?php echo base_url('assets/'); ?>/"/>
    <title><?php echo $template['title'] ?></title>
    <?php echo $template['metadata'] ?>
    <!-- favicon-->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/form-style.css">
    <link rel="stylesheet" type="text/css" media="all" href="css/webslidemenu.css" />
    <link rel="stylesheet" href="css/main.css">
    <!-- fontawesome CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  <body class="grey-bg">
    <div class="wrapper">
      <header class="no-bg">
        <div class="header-top-area inner-head no-sticky mb-3 sup-header">
          <div class="container-fluid">
            <div class="col-12 flex-container">
              <div class="logo-container">
                <!-- Start Logo -->
                <div class="logo2">
                  <a href="<?php echo site_url('home') ?>">
                  <img src="images/fujeeka-logo-green.png" alt="fujeeka logo">
                  </a>
                </div>
                <!-- / End Logo -->
              </div>
              <!-- white strip -->
              <section class="white-strip">
                <div class="container">
                  <ul>
                    <li onclick="window.location.href='<?php echo site_url('market/markets'); ?>'">
                      <div class="icons-container">
                        <div class="icn">
                          <img src="images/home-market-icon.png" alt="market icon">
                        </div>
                        <div class="icn-txt">
                          <h5>Market</h5>
                          <p>Wholesale</p>
                        </div>
                      </div>
                    </li>
                    <li data-toggle="modal" id="search_a_modal" data-target="#search-modal">
                      <div class="icons-container">
                        <div class="icn">
                          <img src="images/home-product-icon.png" alt="products icon">
                        </div>
                        <div class="icn-txt">
                          <h5>Products</h5>
                          <p>Stock &amp; OEM</p>
                        </div>
                      </div>
                    </li>
                    <li onclick="window.location.href = '<?php echo site_url('product/stock'); ?>'">
                      <div class="icons-container">
                        <div class="icn">
                          <img src="images/home-stock-icon.png" alt="Stock icon">
                        </div>
                        <div class="icn-txt">
                          <h5>Stock</h5>
                          <p>Stock & Flash stocks</p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </section>
              <!--/ white strip -->
              <!-- Start Menu -->
              <div class="menu-container">
                <div class="wsmenucontainer clearfix">
                  <div id="overlapblackbg"></div>
                  <div class="wsmobileheader clearfix">
                    <a id="wsnavtoggle" class="animated-arrow"><span></span></a> <!-- <a class="smallogo"><img src="images/fujeeka-logo.png"  alt="" /></a> -->
                  </div>
                  <div class="headerfull">
                    <!--Main Menu HTML Code-->
                    <div class="wsmain">
                      <nav class="wsmenu clearfix">
                        <ul class="mobile-sub wsmenu-list">
                          <li class="signin sh">
                            <a  class="navtext p-0">
                              <?php if (!$this->uid) {?>
                              <div class="reg">
                                <div class="usr-pic">
                                  <img src="images/icons/user.png" alt="My account">
                                </div>
                              </div>
                              <?php } else {?>
                              <div class="reg">
                                <div class="usr-pic">
                                  <img src="images/icons/user.png" alt="My account">
                                </div>
                              </div>
                              <?php }?>
                            </a>
                            <div class="megamenu clearfix halfmenu">
                              <div class="container-fluid">
                                <div class="row">
                                  <div class="signin-container">
                                    <?php if (!$this->uid) {?>
                                    <div class="top-section">
                                      <h4 class="heading3">Get started now!</h4>
                                      <button class="btn btn-success btn-green btn-signin"  href="javascript:;"  data-toggle="modal" data-target="#loginModal">Sign In</button>
                                      <div class="w-100">
                                        or
                                      </div>
                                      <button class="btn btn-success btn-reg" onclick=" window.location.href = '<?php echo site_url('user/signup'); ?>'">Register Now</button>
                                    </div>
                                    <?php }?>
                                    <?php if ($this->uid) {?>
                                    <div class="top2">
                                      <div class="img">
                                        <?php
                                          echo img(array('src' => 'assets/uploads/avatar/' . $this->session->userdata('usr_avatar'),
                                              'class' => 'img-fluid'));
                                              ?>
                                        <!-- <img src="images/avatar" alt="" class="img-fluid"> -->
                                      </div>
                                      <div class="name">
                                        <h3>Welcome, <?php echo $this->session->userdata('usr_username'); ?></h3>
                                        <p><a href="<?php echo site_url('dashboard'); ?>" target="_blank" rel="noopener noreferrer">View Dashboard</a></p>
                                      </div>
                                    </div>
                                    <div class="my-section">
                                      <div class="w-100 heading-bx">
                                        My Fujeeka
                                      </div>
                                      <ul>
                                        <li><a href="<?=site_url('product/rfq-list')?>" target="_blank" rel="noopener noreferrer">Manage RFQ</a></li>
                                        <li><a href="<?=site_url('order')?>" target="_blank" rel="noopener noreferrer">My Orders</a></li>
                                        <li><a href="<?=site_url('dashboard')?>" target="_blank" rel="noopener noreferrer">My Account</a></li>
                                        <li class="signout"><a href="<?php echo site_url('user/logout'); ?>">Signout</a></li>
                                      </ul>
                                    </div>
                                    <?php }?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </nav>
                    </div>
                    <!--Menu HTML Code-->
                  </div>
                </div>
                <!-- /menu -->
                <div class="clearfix"></div>
              </div>
              <!-- / End Menu -->
            </div>
          </div>
        </div>
        <!-- end topbar -->
        <div class="clearfix"></div>
      </header>
      <div class="modal" id="loginModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="login-m">
          <div class="left-sec">
            <h2>Login</h2>
            <p>Get access to the complete wholesale B2B website</p>
          </div>
          <div class="right-sec">
            <div class="form-container">
              <form action="" id="modal_lgn_form">
              <span style="color:green" id="reset_success"></span>
                <div class="form-group lgn_grp">
                  <input type="text" class="form-control" id="identity" placeholder="Username">
                  <span style="color:red" id="u_error"></span>
                </div>
                <div class="form-group lgn_grp">
                  <input type="password" class="form-control" id="password" placeholder="Password">
                  <span style="color:red" id="p_error"></span>
                </div>
                <div class="form-group fpswd_grp d-none">
                  <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                  <span style="color:red" id="m_error"></span>
                </div>
                <div class="form-group otp_grp d-none">
                  <span style="color:green" id="mail_success"></span>
                  <input type="text" class="form-control" id="otp" placeholder="Enter OTP" name="otp">
                  <span style="color:red" id="otp_error"></span>
                  <input type="hidden" id="resend_mail_ip">
                  <input type="hidden" id="forgot_code"> 
                </div>
                <div class="form-group change_grp d-none">
                  <input type="password" class="form-control" id="pswd" placeholder="Enter New Password" name="pswd">
                  <span style="color:red" id="pswd_error"></span>
                  <input type="password" class="form-control" id="cpswd" placeholder="Confirm New Password" name="cpswd">
                  <span style="color:red" id="cpswd_error"></span>
                  <input type="hidden" id="rest_code"> 
                </div>
                <div class="form-group mb-1 text-right">
                  <a id="a_fpwd" href="javascript:;">Forgot Password?</a>
                  <a id="a_lgn" class="d-none" href="javascript:;">Back to login</a>
                  <a id="a_resend_mail" class="d-none" href="javascript:;">Resend OTP</a>
                </div>
                <div class="clearfix"></div>
                <div>
                  <button type="button" data-sec="login" id="modal_lgn_btn" class="btn btn-success btn-login btn-inline">Login</button>
                  <div class="new-u mt-5">
                    New user? <a href="<?=site_url('user/signup')?>">Sign up</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
      <div id="search-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!--Filter Modal content-->
          <div class="modal-content">
            <div class="modal-body">
              <!-- search bar -->
              <?php echo $this->load->view('searchbar'); ?>
              <!--/ searchbar -->
              <p class="it-txt grey-txt">Eg : Mobile phones, headsets, power banks etc.</p>
            </div>
          </div>
        </div>
      </div>
      <section class="multiform-container">
        <div class="container-fluid">
          <div class="row">
            <!-- Steps form -->
            <div class="card">
              <div class="card-body">
                <!-- Stepper -->
                <form role="form" action="<?php echo site_url('product/doContactSupplier'); ?>" method="post"
                  class="frmContactSupplier" enctype="multipart/form-data" data-parsley-validate="true">
                  <input type="hidden" name="ord_prod_id" value="<?php echo encryptor($product['prd_id']); ?>"/>
                  <input type="hidden" name="ord_supplier" value="<?php echo encryptor($product['prd_supplier']); ?>"/>
                  <input type="hidden" name="ord_supplier_user_id" value="<?php echo get_supplier_user($product['prd_supplier'], 'usr_id'); ?>"/>
                  <!-- First Step -->
                  <div class="setup-content" id="query">
                    <div class="row">
                      <div class="col-12">
                        <div class="product-info">
                          <div class="image">
                            <img src="uploads/product/<?php echo $product['default_image']; ?>?scale.height=200" alt=""/>
                          </div>
                          <div class="detail">
                            <p>Product : <span><?php echo $product['prd_name_en']; ?></span></p>
                            <p>Supplier : <span><?php echo $product['supm_name_en']; ?></span></p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <div>
                          <div class="form-group md-form">
                            <label for="subject" data-error="wrong" data-success="right">Subject</label>
                            <input id="subject" data-parsley-required-message="Enter subject" type="text" name="ord_subject" required class="form-control validate">
                          </div>
                          <div class="form-group md-form mt-3">
                            <label for="message" data-error="wrong" data-success="right">Message</label>
                            <textarea required data-parsley-required-message="Enter message" id="message" type="text" name="ord_message" rows="2" class="md-textarea validate form-control"></textarea>
                          </div>
                          <label class="btn-bs-file">
                          <i class="fas fa-paperclip"></i> Attach files <small>(Maximum File Size: 2MB)</small>
                          <input type="file" name="ord_attachment" data-prsley-trigger="change" data-parsley-errors-container="#file_err" id="attach_file"  data-parsley-fileextension="jpg,png,gif,jpeg,pdf,docx,xlsx,xls,txt"/><span id="file_name"></span>
                          </label>
                          <span id="file_err"></span>
                          <div class="qty">
                            <label for="quantity" data-error="wrong" data-success="right">Quantity</label>
                            <div class="d-flex">
                              <div class="form-group md-form mt-2 pl-0">
                                <input required data-parsley-required-message="Enter quantity"  id="quantity" type="text" name="ord_qty" class="form-control validate numOnly">
                              </div>
                              <div class="form-group md-form mt-2">
                                <select name="ord_unit" class="btn btn-default dropdown-toggle">
                                  <option value="">Select  Unit</option>
                                  <?php foreach ((array) $units as $key => $value) {?>
                                  <option <?php if ($value['unt_id'] == $product['prd_unit']) {?> selected  <?php }?> value="<?php echo $value['unt_id']; ?>"><?php echo $value['unt_unit_en'] . ' (' . $value['unt_unit_name_en'] . ')'; ?></option>
                                  <?php }?>
                                </select>
                              </div>
                            </div>
                            <!-- <span style="color:green">*MOQ for this product is <?php echo $product['prd_moq']; ?></span> -->
                          </div>
                          <div class="col-6 form-group md-form mt-3 pl-0 captcha">
                            <!--<img src="images/captcha.jpg" alt="" class="mx-auto d-block">-->
                            <!-- <?php echo $captcha['image']; ?>
                              <input value="<?php echo $captcha['word']; ?>" type="hidden" name="captcha_code" id="captcha_code"/>
                              
                              
                              
                                <label for="captcha" data-error="wrong" data-success="right">Captcha</label> -->
                            <div class="input-group ">
                              <div id="captcha_img"><?php echo $captcha['image']; ?></div>
                              <input value="<?php echo $captcha['word']; ?>" type="hidden" name="captcha_code" id="captcha_code"/>
                              <div class="input-group-append">
                                <a  class="btn btn-default btn-sm refresh_captcha">
                                <i class="fas fa-redo"></i>
                                </a>
                              </div>
                            </div>
                            <label for="captcha" data-error="wrong" data-success="right"></label>
                            <input data-parsley-equalto="#captcha_code" data-parsley-trigger="change" data-parsley-equalto-message="Incorrect captcha" id="captcha" name="usr_captcha" type="text" required="required" autocomplete="off"
                              class="form-control validate md-form mt-3" placeholder="Enter the captcha text">
                          </div>
                          <div class="row qty">
                            <label class="chkcontainer">
                            <input type="checkbox" required data-parsley-required-message="You should agree our terms and conditions" data-parsley-errors-container="#check_err" name="chkConfirm" value="1"/>
                            <span class="checkmark"></span>
                            </label>
                            <p class="m-grey-txt">By submitting you agree that you have read our <a href="">Terms and condition</a> and <a href="">Privacy policy</a>
                              <span for="chkConfirm" generated="true" class="error" style="padding-top: 10px;"></span>
                              <br><span id="check_err"></span>
                          </div>
                          <button class="btn btn-indigo btn-rounded nextBtn" type="submit">Submit inquiry now</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <footer>
      <div class="container">
        <div class="links-container">
          <div class="row">
            <div class="col-md-4">
              <div class="row">
                <div class="col-md-6">
                  <h5>Registration</h5>
                  <ul>
                    <li><a href="<?php echo site_url('user/signup');?>">Buyer</a></li>
                    <li><a href="<?php echo site_url('supplier/supplier-register');?>">Supplier</a></li>
                    <li><a href="<?php echo site_url('market/market-register');?>">Building</a></li>
                    <li><a href="<?php echo site_url('logistics/register');?>">Logistics</a></li>
                    <li><a href="<?= site_url('quality-control/register')?>">Quality Control</a></li>
                  </ul>
                </div>
                <div class="col-md-6">
                  <h5>Info</h5>
                  <ul>
                    <li><a href="<?=site_url('about-us')?>">About Fujeeka</a></li>
                    <li><a href="<?php echo site_url('logistics');?>">Logistics</a></li>
                    <li><a href="<?php echo site_url('quality-control');?>">Quality Control</a></li>
                    <li><a href="javascript:;">Sitemap</a></li>
                  </ul>
                </div>
                <div class="col-md-6">
                  <h5>LEGAL</h5>
                  <ul>
                    <li><a href="<?=site_url('terms-of-use');?>">Terms of Use</a></li>
                    <li><a href="<?= site_url('privacy-policies'); ?>">Privacy Policy</a></li>
                    <li><a href="<?=site_url('product-listing-policies');?>">Product Listing Policy</a></li>
                  </ul>
                </div>
                <div class="col-md-6">
                  <h5>Help</h5>
                  <ul>
                    <li><a href="javascript:;">Help Center</a></li>
                    <li><a href="<?=site_url('contact-us')?>">Contact Us</a></li>
                    <li><a href="<?=site_url('faq')?>">FAQ</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="f-rfq">
                <h5>Post your buying request</h5>
                <?php echo form_open_multipart(site_url('product/rfq'), array('class' => "form-signin", 'data-parsley-validate' => "true"))?>
                <div class="form-group">
                  <select class="form-control" id="sell1" name="rfq_category" required data-parsley-required-message="Select a category">
                    <option value="">Select Category</option>
                    <?php foreach(get_buying_config('cat') as $cat){ ?>
                    <option value="<?=$cat['cat_id']?>"><?=$cat['cat_title']?></option>
                    <?php } ?>    
                  </select>
                </div>
                <div class="form-group">
                  <select class="form-control" id="markets" name="markets">
                    <option value="">Select Market</option>
                    <?php foreach(get_buying_config('mar') as $mar){ ?>
                    <option value="<?=$mar['mar_id']?>"><?=$mar['mar_name']?></option>
                    <?php } ?>      
                  </select>
                </div>
                <div class="form-group">
                  <input type="text" required data-parsley-required-message="Enter product name" class="form-control commonAutoComplete" id="product" placeholder="Keywords of products you are looking for" name="pname" data-url="<?php echo site_url('product/productAutoName');?>">
                  <input type="hidden" id="prd_id" value="0" name="rfq_product">
                  <input type="hidden"  class="unit_sel"  name="unit">
                </div>
                <button class="btn btn-success btn-green" value="footer" name="from_footer" type="submit">Get Quote</button>
                <?php echo form_close()?>
              </div>
            </div>
            <div class="col-md-4">
              <!-- Subscribe -->
              <div class="subscribe-section">
                <h5>Subscribe Newsletter</h5>
                <form class="form-inline frmNewsletterSub" data-url="<?php echo site_url('general/newsletterSub'); ?>">
                  <div class="form-group mr-2 mb-2">
                    <label for="inputPassword2" class="sr-only">Password</label>
                    <input type="email" class="form-control txtNewsSubEmail" id="subs-email" placeholder="Enter your email ">
                  </div>
                  <button type="submit" class="btn btn-success btn-green mb-2">Subscribe</button>
                  <p>Subscribe newsletter to get notifications</p>
                </form>
                <p class="newsLtrMsg" style="color: red;font-size: 10px;"></p>
              </div>
              <!-- /Subscribe -->
              <div class="gapp">
                <h5>Get App</h5>
                <div class="app-qr">
                  <a href="<?=site_url('home/get_app')?>"><img src="images/app-store.png" alt="scan code to install ios app">
                  <img src="images/playstore.png" alt="scan code to install android app"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-base">
          <div class="row">
            <!-- <div class="col-md-4 download-app">
              <img src="assets/images/app-store.png" alt="Download from Appstore" class="img-fluid">
              <img src="assets/images/playstore.png" alt="Download from Playstore" class="img-fluid">
              </div> -->
            <div class="col-md-6 text-left">
              &copy; Copyright <?=date('Y')?> Fujeeka | All Rights Reserved
            </div>
            <div class="col-md-6">
              <div class="sicon">
                <div class="ficn">
                  <div class="icon-circle">
                    <a href="javascript:;" class="ifacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                  </div>
                </div>
                <div class="ficn">
                  <div class="icon-circle">
                    <a href="javascript:;" class="itwittter" title="Twitter"><i class="fab fa-twitter"></i></a>
                  </div>
                </div>
                <div class="ficn">
                  <div class="icon-circle">
                    <a href="javascript:;" class="iInsta" title="Instagram"><i class="fab fa-instagram"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!--<script src="js/form-js.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="js/parsley.min.js"></script>
    <script type="text/javascript" src="../vendors/devbridge-autocomplete/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="js/search.js"></script>
    <script>
      site_url = "<?=site_url();?>";
      
          $(document).ready(function () {
                       window.ParsleyValidator
               .addValidator('fileextension', function (value, requirement) {
                   var fileExtension = value.split('.').pop();
                   extns = requirement.split(',');
                   if(extns.indexOf(fileExtension) == -1){
                     return fileExtension === requirement;
                   }
      
               }, 32)
               .addMessage('en', 'fileextension', 'Only files with type jpg,png,gif,jpeg,pdf,docx,xlsx,xls,txt are supported');
               $(".numOnly").keypress(function (e) {
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                         return false;
                    }
               });
               $("#attach_file").change(function() {
                   for (var e = $("#attach_file")[0].files[0].name, t = $("#attach_file")[0].files[0].size, a = new Array("Bytes", "KB", "MB", "GB"), n = 0; t > 900;) t /= 1024, n++;
                   var l = Math.round(100 * t) / 100 + " " + a[n];
                   $("#file_name").text(e + " (" + l + ")")
               });
               $(document).on('click', '.refresh_captcha', function () {
                    $.ajax({
      
                         url: "<?php echo site_url(); ?>user/refreshCaptcha",
      
                         method: "GET",
                         dataType: "json",
                         beforeSend: function () {
                              $(".ajax_loader").removeClass("d-none");
                         },
                         complete: function () {
                              $(".ajax_loader").addClass("d-none");
                         },
                         success: function (data) {
                              $('#captcha_code').val('');
                              $('#captcha_img').html(data.data.image);
                              $('#captcha_code').val(data.data.word);
                              $('#captcha').val('');
                         }
                    });
               });
          });
      
    </script>
  </body>
  <style>
    span.error {
    font-size: 10px !important;
    color: red !important;
    float: left;
    width: 100%;
    }
    span#mail_exist {
    font-size: 10px;
    color: red;
    }
  </style>
</html>
<style>
  /* The container */
  .chkcontainer {
  display: block;
  position: relative;
  padding-left: 25px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  }
  /* Hide the browser's default checkbox */
  .chkcontainer input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  }
  /* Create a custom checkbox */
  .checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 18px;
  width: 18px;
  background-color: #eee;
  border: 1px solid #c1bfbf;
  }
  /* On mouse-over, add a grey background color */
  .chkcontainer:hover input ~ .checkmark {
  background-color: #ccc;
  }
  /* When the checkbox is checked, add a blue background */
  .chkcontainer input:checked ~ .checkmark {
  background-color: #00b904;
  border: 1px solid #00b904;
  }
  /* Create the checkmark/indicator (hidden when not checked) */
  .checkmark:after {
  content: "";
  position: absolute;
  display: none;
  }
  /* Show the checkmark when checked */
  .chkcontainer input:checked ~ .checkmark:after {
  display: block;
  }
  /* Style the checkmark/indicator */
  .chkcontainer .checkmark:after {
  left: 5px;
  top: 0px;
  width: 7px;
  height: 14px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
  }
  p.parsley-success {
  color: #468847;
  background-color: #DFF0D8;
  border: 1px solid #D6E9C6
  }
  p.parsley-error {
  color: #B94A48;
  background-color: #F2DEDE;
  border: 1px solid #EED3D7
  }
  ul.parsley-errors-list {
  list-style: none;
  color: #E74C3C;
  padding-left: 0
  }
  input.parsley-error,
  select.parsley-error,
  textarea.parsley-error {
  background: #FAEDEC;
  border: 1px solid #E85445
  }
  .btn-group .parsley-errors-list {
  display: none
  }
</style>