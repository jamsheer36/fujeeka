<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Fujishka">
    <base href="<?php echo base_url('assets/');?>/"/>
    <title><?php echo $template['title']?></title>
    <?php echo $template['metadata']?>
    <!-- favicon-->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/form-style.css">
    <!--Main Menu File-->
    <link rel="stylesheet" type="text/css" media="all" href="css/webslidemenu.css" />
    <link rel="stylesheet" href="css/main.css">
    <!-- fontawesome CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  <body class="grey-bg">
    <?php echo $template['partials']['flash_messages']?>
    <input type="hidden" value="<?php echo site_url();?>" id="site_url">
    <div class="wrapper2">
      <header class="no-bg">
        <div class="header-top-area inner-head no-sticky mb-3 sup-header">
          <div class="container">
            <div class="col-12 flex-container">
              <div class="logo-container">
                <!-- Start Logo -->
                <div class="logo2">
                  <a href="<?php echo site_url('home') ?>">
                  <img src="images/fujeeka-logo-green.png" alt="fujeeka logo">
                  </a>
                </div>
                <!-- / End Logo -->
              </div>
              <!-- white strip -->
              <section class="white-strip">
                <div class="container">
                  <ul>
                    <li onclick="window.location.href='<?php echo site_url('market/markets'); ?>'">
                      <div class="icons-container">
                        <div class="icn">
                          <img src="images/home-market-icon.png" alt="market icon">
                        </div>
                        <div class="icn-txt">
                          <h5>Market</h5>
                          <p>Wholesale</p>
                        </div>
                      </div>
                    </li>
                    <li data-toggle="modal" id="search_a_modal" data-target="#search-modal">
                      <div class="icons-container">
                        <div class="icn">
                          <img src="images/home-product-icon.png" alt="products icon">
                        </div>
                        <div class="icn-txt">
                          <h5>Products</h5>
                          <p>Stock &amp; OEM</p>
                        </div>
                      </div>
                    </li>
                    <li onclick="window.location.href = '<?php echo site_url('product/stock');?>'">
                      <div class="icons-container">
                        <div class="icn">
                          <img src="images/home-stock-icon.png" alt="Stock icon">
                        </div>
                        <div class="icn-txt">
                          <h5>Stock</h5>
                          <p>Stock & Flash stocks</p>
                        </div>
                      </div>
                    </li>
                    <li onclick="window.location.href='<?php echo site_url('product/rfq'); ?>'">
                      <div class="icons-container">
                        <div class="icn">
                          <img src="images/home-rfq-icon.png" alt="rfq icon">
                        </div>
                        <div class="icn-txt">
                          <h5>RFQ</h5>
                          <p>Request Quote</p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </section>
              <!--/ white strip -->
              <!-- Start Menu -->
              <div class="menu-container">
                <div class="wsmenucontainer clearfix">
                  <div id="overlapblackbg"></div>
                  <div class="wsmobileheader clearfix">
                    <a id="wsnavtoggle" class="animated-arrow"><span></span></a> <!-- <a class="smallogo"><img src="images/fujeeka-logo.png"  alt="" /></a> -->
                  </div>
                  <div class="headerfull">
                    <!--Main Menu HTML Code-->
                    <div class="wsmain">
                      <nav class="wsmenu clearfix">
                        <ul class="mobile-sub wsmenu-list">
                          <li class="signin sh">
                            <a  class="navtext p-0">
                              <?php if (!$this->uid) {?>
                              <div class="reg">
                                <div class="usr-pic">
                                  <img src="images/icons/user.png" alt="My account">
                                </div>
                              </div>
                              <?php } else {?>
                              <div class="reg">
                                <div class="usr-pic">
                                  <img src="images/icons/user.png" alt="My account">
                                </div>
                              </div>
                              <?php }?>
                            </a>
                            <div class="megamenu clearfix halfmenu">
                              <div class="container-fluid">
                                <div class="row">
                                  <div class="signin-container">
                                    <?php if (!$this->uid) {?>
                                    <div class="top-section">
                                      <h4 class="heading3">Get started now!</h4>
                                      <button class="btn btn-success btn-green btn-signin" data-toggle="modal" data-target="#loginModal">Sign In</button>
                                      <div class="w-100">
                                        or
                                      </div>
                                      <button class="btn btn-success btn-reg" onclick=" window.location.href = '<?php echo site_url('user/signup'); ?>'">Register Now</button>
                                    </div>
                                    <?php }?>
                                    <?php if ($this->uid) {?>
                                    <div class="top2">
                                      <div class="img">
                                        <?php
                                          echo img(array('src' => 'assets/uploads/avatar/' . $this->session->userdata('usr_avatar'),
                                              'class' => 'img-fluid'));
                                              ?>
                                        <!-- <img src="images/avatar" alt="" class="img-fluid"> -->
                                      </div>
                                      <div class="name">
                                        <h3>Welcome, <?php echo $this->session->userdata('usr_username'); ?></h3>
                                        <p><a href="<?php echo site_url('dashboard'); ?>" target="_blank" rel="noopener noreferrer">View Dashboard</a></p>
                                      </div>
                                    </div>
                                    <div class="my-section">
                                      <div class="w-100 heading-bx">
                                        My Fujeeka
                                      </div>
                                      <ul>
                                        <li><a href="<?=site_url('product/rfq-list')?>" target="_blank" rel="noopener noreferrer">Manage RFQ</a></li>
                                        <li><a href="<?=site_url('order')?>" target="_blank" rel="noopener noreferrer">My Orders</a></li>
                                        <li><a href="<?=site_url('dashboard')?>" target="_blank" rel="noopener noreferrer">My Account</a></li>
                                        <li class="signout"><a href="<?php echo site_url('user/logout'); ?>">Signout</a></li>
                                      </ul>
                                    </div>
                                    <?php }?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </nav>
                    </div>
                    <!--Menu HTML Code-->
                  </div>
                </div>
                <!-- /menu -->
                <div class="clearfix"></div>
              </div>
              <!-- / End Menu -->
            </div>
          </div>
        </div>
        <!-- end topbar -->
        <div class="clearfix"></div>
      </header>
      <div class="modal" id="loginModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="login-m">
          <div class="left-sec">
            <h2>Login</h2>
            <p>Get access to the complete wholesale B2B website</p>
          </div>
          <div class="right-sec">
            <div class="form-container">
              <form action="" id="modal_lgn_form">
              <span style="color:green" id="reset_success"></span>
                <div class="form-group lgn_grp">
                  <input type="text" class="form-control" id="identity" placeholder="Username">
                  <span style="color:red" id="u_error"></span>
                </div>
                <div class="form-group lgn_grp">
                  <input type="password" class="form-control" id="password" placeholder="Password">
                  <span style="color:red" id="p_error"></span>
                </div>
                <div class="form-group fpswd_grp d-none">
                  <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                  <span style="color:red" id="m_error"></span>
                </div>
                <div class="form-group otp_grp d-none">
                  <span style="color:green" id="mail_success"></span>
                  <input type="text" class="form-control" id="otp" placeholder="Enter OTP" name="otp">
                  <span style="color:red" id="otp_error"></span>
                  <input type="hidden" id="resend_mail_ip">
                  <input type="hidden" id="forgot_code"> 
                </div>
                <div class="form-group change_grp d-none">
                  <input type="password" class="form-control" id="pswd" placeholder="Enter New Password" name="pswd">
                  <span style="color:red" id="pswd_error"></span>
                  <input type="password" class="form-control" id="cpswd" placeholder="Confirm New Password" name="cpswd">
                  <span style="color:red" id="cpswd_error"></span>
                  <input type="hidden" id="rest_code"> 
                </div>
                <div class="form-group mb-1 text-right">
                  <a id="a_fpwd" href="javascript:;">Forgot Password?</a>
                  <a id="a_lgn" class="d-none" href="javascript:;">Back to login</a>
                  <a id="a_resend_mail" class="d-none" href="javascript:;">Resend OTP</a>
                </div>
                <div class="clearfix"></div>
                <div>
                  <button type="button" data-sec="login" id="modal_lgn_btn" class="btn btn-success btn-login btn-inline">Login</button>
                  <div class="new-u mt-5">
                    New user? <a href="<?=site_url('user/signup')?>">Sign up</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
      <div class="modal verif-mail" id="verif-mail" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Verify Email</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
              <form class="mail-form" data-parsely-validate="true">
                Hi <?=$this->session->userdata('usr_fullname');?>,<br> Please verify your email 
                <input id="mail_inp" type="email" autocomplete="off" required="" data-parsley-required-message="" data-parsley-type-message="Invalid email" data-parsley-errors-container="#par_error" name="new_mail" class="d-none" placeholder="Enter new email here">
                <span id="ver_mail_span" class="font-bold">(<?=$this->session->userdata('usr_email');?>) to activate all the features.</span>
                <a href="javascript:;" class="show_mail_inp green-txt">Change email</a>
                <br>
                <span id="par_error"></span>
                <div class="al-btns" style="text-align: center;">
                  <a class="btn btn-success btn-green ver-email mx-auto mt-4">Resend Email</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div id="search-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!--Filter Modal content-->
          <div class="modal-content">
            <div class="modal-body">
              <!-- search bar -->
              <?php echo $this->load->view('searchbar');?>
              <!--/ searchbar -->
              <p class="it-txt grey-txt">Eg : Mobile phones, headsets, power banks etc.</p>
            </div>
          </div>
        </div>
      </div>
      <section class="white-bg">
        <!-- <?php if ($success = $this->session->flashdata('app_success')):?>
          <div class="alert alert-success alert-dismissible" >
               <button type="button" class="close" data-dismiss="alert">&times;</button>
               <?php echo $success?>
          </div>
          <?php endif?>
          <?php if ($error = $this->session->flashdata('app_error')):?>
          <div class="alert alert-danger alert-dismissible" >
               <button type="button" class="close" data-dismiss="alert">&times;</button>
               <?php echo $error?>
          </div>
          <?php endif?> -->
        <div class="container">
          <div class="row pt-5 pb-5">
            <div class="col-md-7 border-right m-grey-txt">
              <h2 class="heading2">REQUEST FOR QUOTE</h2>
              <h4 class="heading3 green-txt">Tell suppliers your requirements!</h4>
              <p>Provide as much details as possible about your request to ensure fast response from the right suppliers. The higher the score the better responses you will get. </p>
              <div class="form-container d-grey-txt mt-4">
                <?php echo form_open_multipart('', array('id' => "frmProduct", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                <div class="form-group">
                  <label for="category">Select Category :</label>
                  <!-- <div class="filter-cat border-0">
                    <div class="s-filter"> -->
                  <div class="d-flex">
                    <div class="w-50">
                      <select class="form-control" id="sell1" name="rfq_category" required data-parsley-required-message="Select a category">
                        <option value="">Select a category</option>
                        <?php foreach ($categories as $cat) {?>
                        <option <?php if($categ==$cat['cat_id']) {?> selected <?php }?> value="<?php echo $cat['cat_id'];?>"><?php echo $cat['cat_title'];?></option>
                        <?php }?>
                      </select>
                    </div>
                    <!-- </div> -->
                    <!-- <div class="s-filter"> -->
                    <div class="s-cont sub_cate w-50">
                    </div>
                  </div>
                  <!-- </div> -->
                  <!-- </div> -->
                </div>
                <div class="form-group">
                  <label for="markets">Markets :</label>
                  <select class="form-control" id="markets" name="rfq_market">
                    <option value="">Select Market</option>
                    <?php foreach ((array) $marketPlaces as $key => $value) {?>
                    <option <?php if($mar==$value['mar_id']) {?> selected <?php }?> value="<?php echo $value['mar_id'];?>"><?php echo $value['mar_name'];?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="pname">Product Name :</label>
                  <input type="text" value="<?=$prd_name;?>" required data-parsley-required-message="Enter product name" class="form-control commonAutoComplete" id="product" placeholder="Keywords of products you are looking for" name="pname" data-url="<?php echo site_url('product/productAutoName');?>">
                  <input type="hidden" id="prd_id" value="<?=$prd_id;?>" name="rfq_product">
                </div>
                <div class="form-group">
                  <label for="quantity">Quantity :</label>
                  <div class="input-group">
                    <input type="text" class="form-control inp_qty"  data-parsley-type-message="Quantity should be a valid number" data-parsley-type="number" data-parsley-errors-container="#qtr_err"  required data-parsley-required-message="Enter Quantity" placeholder="Quantity" name="rfq_qty">
                    <div class="input-group-prepend">
                      <select  class="dropdown-toggle unit_sel" data-toggle="dropdown" name="rfq_unit" id="unit_sel">
                        <option value="">Select Unit</option>
                        <?php foreach ((array) $units as $key => $value) {?>
                        <option <?php if($prd_unit==$value['unt_id']) {?> selected <?php }?> value="<?php echo $value['unt_id'];?>"><?php echo $value['unt_unit_name_en'];?></option>
                        <?php }?>
                      </select>
                    </div>
                  </div>
                  <span id="qtr_err"></span>
                  <span class="required" id="moq_span" style="color:#008000"></span>
                </div>
                <div class="form-group">
                  <label for="requirement">Requirement :</label>
                  <textarea class="form-control" id="req" rows="5"  name="rfq_requirment"></textarea>
                </div>
                <div class="form-group">
                  <div class="img-display">
                    <img src="" alt="" class="imgPreview" style="width: 190px;"/>
                    <div type="button" class="close btnRemoveImage" data-dismiss="modal" style="display: none;">X</div>
                  </div>
                  <label class="btn-bs-file">
                  <i class="fas fa-paperclip"></i> Attach files <small>(Maximum File Size: 2MB)</small>
                  <input type="file" data-parsley-errors-container="#file_err"  data-parsley-fileextension="jpg,png,gif,jpeg,pdf,docx,xlsx,xls,txt" id="attach_file" name="rfq_attachment" /><span id="file_name"></span>
                  </label>
                  <span id="file_err"></span>
                </div>
                <div class="form-group">
                  <label for="validity">Valid To :</label>
                  <div class="input-group">
                    <select class="form-control" id="validity" name="">
                      <option value="7">7 Days</option>
                      <option value="30">30 Days</option>
                      <option value="45">45 Days</option>
                      <option value="90">90 Days</option>
                    </select>
                    <div class="input-group-prepend">
                      <input type="date" class="form-control" id="date" placeholder="" name="rfq_validity">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="email">Business Email :</label>
                  <input type="email" required data-parsley-trigger="change" value="<?php echo $this->session->userdata('usr_email')?>" data-parsley-required-message="Email required"  class="form-control" id="email" placeholder="Business Email" name="rfq_mail">
                </div>
                <div class="col-6 form-group md-form mt-3 pl-0 captcha">
                  <!--<img src="images/captcha.jpg" alt="" class="mx-auto d-block">-->
                  <!-- <?php echo $captcha['image'];?>
                    <input value="<?php echo $captcha['word'];?>" type="hidden" name="captcha_code" id="captcha_code"/>
                    
                      <label for="captcha" data-error="wrong" data-success="right">Captcha</label> -->
                  <div class="input-group ">
                    <div id="captcha_img"><?php echo $captcha['image'];?></div>
                    <input value="<?php echo $captcha['word'];?>" type="hidden"  id="captcha_code"/>
                    <div class="input-group-append">
                      <a  class="btn btn-default btn-sm refresh_captcha">
                      <i class="fas fa-redo"></i>
                      </a>
                    </div>
                  </div>
                  <label for="captcha" data-error="wrong" data-success="right"></label>
                  <input data-parsley-equalto="#captcha_code" data-parsley-trigger="change" data-parsley-equalto-message="Incorrect captcha" id="captcha"  type="text" required="required" autocomplete="off"
                    class="form-control validate md-form mt-3" placeholder="Enter the captcha text">
                </div>
                <div class="form-group form-check">
                  <label class="form-check-label">
                  <input class="form-check-input" type="checkbox" name="chkConfirm" required data-parsley-trigger="change" data-parsley-required-message="You should agree this"> I agree to share my <a href="javascript:;">business card</a> with quoted suppliers
                  </label>
                </div>
                <p class="small m-grey-txt">Please see our <a href="javascript:;">Privacy Policy</a> for information regarding the processing of your data. By submitting you agree that you have read our Privacy Policy and accepted our <a href="javascript:;">Terms of Use</a>. </p>
                <?php if (!get_logged_user('usr_is_mail_verified')) {?>
                <button type="button" data-toggle="modal"  data-target="#verif-mail" class="btn btn-success btn-green font-weight-bold">SEND RFQ</button>
                <?php } else {?>
                <button type="submit" value="rfq" name="rfq_page" value="rfq_page" class="btn btn-success btn-green font-weight-bold">SEND RFQ</button>
                <?php }?>
                <?php echo form_close()?>
              </div>
            </div>
            <div class="col-md-5">
              <h2 class="heading2">WITH ONE REQUEST<br> GET MULTIPLE QUOTES</h2>
              <p class=" m-grey-txt">Easiest way to get quotes from suppliers</p>
              <ul class="pts">
                <li>Get quotes for your custom request</li>
                <li>Let the right suppliers find you</li>
                <li>Close deals with ease</li>
              </ul>
              <img src="images/banners/rfq-sm.jpg" alt="Request for Quotation" class="mt-4">
              <div class="faq mt-5">
                <h4 class="dt-heading">FAQ</h4>
                <ul class="pts">
                  <li>The buyer’s email address should be verified before posting a buying request. </li>
                  <li>Necessary information: product name included in the subject, and other product details in the body part, such as size, color, type, place of origin, variety, and any other specifications. The more, the better.
                    P.S.: there might be some special specifications in certain industries.
                  </li>
                  <li>It is highly suggested to contact the owner or authorized distributor of any trademarked/branded product to buy it.</li>
                  <li>As we are a wholesale platform, larger MOQ (minimum order quantity) orders tend to be preferred. Note that MOQ will vary depending on the product type.</li>
                </ul>
              </div>
            </div>
          </div>
          <!-- <div class="row">
            <div class="col-md-7">
            
            </div>
            <div class="col-md-5">
              <h2 class="heading2">WITH ONE REQUEST GET MULTIPLE QUOTES</h2>
              <p>Easiest way to get quotes from suppliers</p>
              <ul>
                <li>Get quotes for your custom request</li>
                <li>Let the right suppliers find you</li>
                <li>Close deals with ease</li>
              </ul>
            </div>
            </div> -->
        </div>
      </section>
      <div class="clearfix"></div>
      <footer>
        <div class="container">
          <div class="links-container">
            <div class="row">
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-6">
                    <h5>Registration</h5>
                    <ul>
                      <li><a href="<?php echo site_url('user/signup');?>">Buyer</a></li>
                      <li><a href="<?php echo site_url('supplier/supplier-register');?>">Supplier</a></li>
                      <li><a href="<?php echo site_url('market/market-register');?>">Building</a></li>
                      <li><a href="<?php echo site_url('logistics/register');?>">Logistics</a></li>
                      <li><a href="<?= site_url('quality-control/register')?>">Quality Control</a></li>
                    </ul>
                  </div>
                  <div class="col-md-6">
                    <h5>Info</h5>
                    <ul>
                      <li><a href="<?=site_url('about-us')?>">About Fujeeka</a></li>
                      <li><a href="<?php echo site_url('logistics');?>">Logistics</a></li>
                      <li><a href="<?php echo site_url('quality-control');?>">Quality Control</a></li>
                      <li><a href="javascript:;">Sitemap</a></li>
                    </ul>
                  </div>
                  <div class="col-md-6">
                    <h5>LEGAL</h5>
                    <ul>
                      <li><a href="<?=site_url('terms-of-use');?>">Terms of Use</a></li>
                      <li><a href="<?= site_url('privacy-policies'); ?>">Privacy Policy</a></li>
                      <li><a href="<?=site_url('product-listing-policies');?>">Product Listing Policy</a></li>
                    </ul>
                  </div>
                  <div class="col-md-6">
                    <h5>Help</h5>
                    <ul>
                      <li><a href="javascript:;">Help Center</a></li>
                      <li><a href="<?=site_url('contact-us')?>">Contact Us</a></li>
                      <li><a href="<?=site_url('faq')?>">FAQ</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="f-rfq">
                  <h5>Post your buying request</h5>
                  <?php echo form_open_multipart(site_url('product/rfq'), array('class' => "form-signin", 'data-parsley-validate' => "true"))?>
                  <div class="form-group">
                    <select class="form-control" id="sell1" name="rfq_category" required data-parsley-required-message="Select a category">
                      <option value="">Select Category</option>
                      <?php foreach(get_buying_config('cat') as $cat){ ?>
                      <option value="<?=$cat['cat_id']?>"><?=$cat['cat_title']?></option>
                      <?php } ?>    
                    </select>
                  </div>
                  <div class="form-group">
                    <select class="form-control" id="markets" name="markets">
                      <option value="">Select Market</option>
                      <?php foreach(get_buying_config('mar') as $mar){ ?>
                      <option value="<?=$mar['mar_id']?>"><?=$mar['mar_name']?></option>
                      <?php } ?>      
                    </select>
                  </div>
                  <div class="form-group">
                    <input type="text" required data-parsley-required-message="Enter product name" class="form-control commonAutoComplete" id="product" placeholder="Keywords of products you are looking for" name="pname" data-url="<?php echo site_url('product/productAutoName');?>">
                    <input type="hidden" id="prd_id" value="0" name="rfq_product">
                    <input type="hidden"  class="unit_sel"  name="unit">
                  </div>
                  <button class="btn btn-success btn-green" value="footer" name="from_footer" type="submit">Get Quote</button>
                  <?php echo form_close()?>
                </div>
              </div>
              <div class="col-md-4">
                <!-- Subscribe -->
                <div class="subscribe-section">
                  <h5>Subscribe Newsletter</h5>
                  <form class="form-inline frmNewsletterSub" data-url="<?php echo site_url('general/newsletterSub'); ?>">
                    <div class="form-group mr-2 mb-2">
                      <label for="inputPassword2" class="sr-only">Password</label>
                      <input type="email" class="form-control txtNewsSubEmail" id="subs-email" placeholder="Enter your email ">
                    </div>
                    <button type="submit" class="btn btn-success btn-green mb-2">Subscribe</button>
                    <p>Subscribe newsletter to get notifications</p>
                  </form>
                  <p class="newsLtrMsg" style="color: red;font-size: 10px;"></p>
                </div>
                <!-- /Subscribe -->
                <div class="gapp">
                  <h5>Get App</h5>
                  <div class="app-qr">
                    <a href="<?=site_url('home/get_app')?>"><img src="images/app-store.png" alt="scan code to install ios app">
                    <img src="images/playstore.png" alt="scan code to install android app"></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="footer-base">
            <div class="row">
              <!-- <div class="col-md-4 download-app">
                <img src="assets/images/app-store.png" alt="Download from Appstore" class="img-fluid">
                <img src="assets/images/playstore.png" alt="Download from Playstore" class="img-fluid">
                </div> -->
              <div class="col-md-6 text-left">
                &copy; Copyright <?=date('Y')?> Fujeeka | All Rights Reserved
              </div>
              <div class="col-md-6">
                <div class="sicon">
                  <div class="ficn">
                    <div class="icon-circle">
                      <a href="javascript:;" class="ifacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                    </div>
                  </div>
                  <div class="ficn">
                    <div class="icon-circle">
                      <a href="javascript:;" class="itwittter" title="Twitter"><i class="fab fa-twitter"></i></a>
                    </div>
                  </div>
                  <div class="ficn">
                    <div class="icon-circle">
                      <a href="javascript:;" class="iInsta" title="Instagram"><i class="fab fa-instagram"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script> -->
    <script src="js/parsley.min.js"></script>
    <!-- Autocomplete -->
    <script>
      site_url = "<?= site_url();?>"
      $(document).ready(function () {
           window.ParsleyValidator
                   .addValidator('fileextension', function (value, requirement) {
                        var fileExtension = value.split('.').pop();
                        extns = requirement.split(',');
                        if (extns.indexOf(fileExtension) == -1) {
                             return fileExtension === requirement;
                        }
      
                   }, 32)
                   .addMessage('en', 'fileextension', 'Only files with type jpg,png,gif,jpeg,pdf,docx,xlsx,xls,txt are supported');
      
           function readURL(input) {
      
                if (input.files && input.files[0]) {
                     var reader = new FileReader();
      
                     reader.onload = function (e) {
                          $('#blah').attr('src', e.target.result);
                     }
      
                     reader.readAsDataURL(input.files[0]);
                }
           }
      
           $("#imgInp").change(function () {
                readURL(this);
           });
      });
    </script>
    <script type="text/javascript" src="../vendors/devbridge-autocomplete/jquery.autocomplete.min.js"></script>
    <!-- General -->
    <script src="js/rfq.js"></script>
    <script type="text/javascript" src="js/search.js"></script>
    <?php if (!get_logged_user('usr_is_mail_verified')) {?>
    <script src="js/verify_mail.js"></script>
    <style>
      .mail-form ul.parsley-errors-list {
      list-style: none;
      color: #E74C3C;
      padding-left: 3px;
      display:inline-block;
      }
    </style>
    <?php }?>
    <style>
      p.parsley-success {
      color: #468847;
      background-color: #DFF0D8;
      border: 1px solid #D6E9C6
      }
      p.parsley-error {
      color: #B94A48;
      background-color: #F2DEDE;
      border: 1px solid #EED3D7
      }
      ul.parsley-errors-list {
      list-style: none;
      color: #E74C3C;
      padding-left: 0
      }
      input.parsley-error,
      select.parsley-error,
      textarea.parsley-error {
      background: #FAEDEC;
      border: 1px solid #E85445
      }
      .btn-group .parsley-errors-list {
      display: none
      }
      .img-display {
      max-width: 200px;
      width: auto;
      max-height: 150px;
      position: relative;
      margin: 10px 0;
      overflow: hidden;
      }
      .img-display .close {
      position: absolute;
      top: 0;
      left: 0;
      background: #f01212;
      padding: 5px;
      }
    </style>
  </body>
</html>