<div class="total-container">
      <div class="container">
        
        <div class="main-area">
          <!-- <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#products">Products</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#suppliers">Suppliers</a>
            </li>
          </ul> -->

           <div class="tab-content">
             <!-- tab 1 -->
             <div id="products" class="tab-pane active">
             <div class="row">
                            <div class="col">
                                <div class="filter-cat">
                                    <h3>FILTER RESULTS BY : </h3>
                                    <div class="s-filter">
                                        <div class="s-cont">
                                            <select class="form-control" id="sel1">
                           <option>Select a category</option>
                           <option>Mobile Phone Display</option>
                           <option>Mobile Phone Holder</option>
                           <option>Mobile Phone Cables</option>
                           <option>Mobile Phone Housing</option>
                           <option>Mobile Phone Flex Cables</option>
                         </select>
                                        </div>
                                    </div>

                                    <div class="s-filter">
                                        <div class="s-cont">
                                            <select class="form-control" id="sel1">
                           <option>Select a category</option>
                           <option>Mobile Phone Display</option>
                           <option>Mobile Phone Holder</option>
                           <option>Mobile Phone Cables</option>
                           <option>Mobile Phone Housing</option>
                           <option>Mobile Phone Flex Cables</option>
                         </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


               <!-- filters -->
               <div class="row">
                 <div class="filter-container">
                   <div class="col-9">
                     <form>
                      <label class="checkbox-inline">
                        <input type="checkbox" value="">Verified Manufacturers
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" value="">Accepts Small Orders
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" value="">Accepts Sample Orders
                      </label>
                    </form>
                  </div>

                  <div class="col-3">
                    <div class="sort">
                      <label for="sel1">Sort by:</label>
                      <select class="form-control" id="sel1">
                        <option>Relevance</option>
                        <option>Price Low to High</option>
                        <option>Price High to Low</option>
                      </select>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                 </div>
               </div>
               <!--/ filters -->

               <div class="row">
                 <!-- product container -->
                 <div class="prodct-container">
                   <!-- single product -->
                   <?php foreach($products as $prod){ ?>
                   <div class="single-prod">
                     <div class="prod-box">
                       <div class="prod-img">
                       <a href="<?php echo site_url($controller . '/product_details/' . encryptor($prod['prd_id']));?>">
                        <?php echo img(array('src' =>$prod['default_image'],  'id' => '' ,'alt'=> '', 'class'=>'img-responsive img-fluid'));?>
                       </a> 
                       <!-- <a href=""><img src="images/product/watch.jpg" alt=""></a> -->
                       </div>
                       <div class="prod-detail txt-height">
                         <div class="details">
                           <h4 class="prod-title-s"><a href=""><?php echo $prod['prd_name_en']; ?></a></h4>
                           <p>From <span class="price-text">US $<?php echo $prod['prd_price_min']; ?></span> / Piece</p>
                           <p class="moq"><?php echo $prod['prd_moq']; ?> Piece  (MOQ)</p>
                           <!-- <div class="sub-details">
                             <p>Category : <span class="green-txt"><a href="">Power Bank</a></span> <br>
                                Lead Time : <span>25 days</span> <br>
                                FOB Port : <span>Shenzhen</span></p>
                           </div> -->
                         </div>

                         <!-- <div class="company-details">
                           <h4 class="prod-title-s"><a href="">Dongguan Arun Industrial Co. Ltd</a></h4>
                           <p class="year">15th year</p>
                           <p class="loc">Location : <span>China (Mainland)</span></br>
                             Response : <span>High / Less than 24h</span></p>
                           <div class="icon-details">
                            <div class="icon verified">Verified Information</div>
                          </div>
                           <div class="clearfix"></div>
                           <p class="check"><a href="">Check All 570 Products</a></p>
                         </div> -->
                         <div class="contcts">
                           <a href="" class="btn btn-contact float-left">Contact Supplier</a>
                           <a href="" class="chat float-right" data-toggle="tooltip" title="Chat with us!"><i class="fas fa-comments"></i> <span>Chat with us</span></a>
                           <div class="clearfix"></div>
                         </div>
                       </div>
                     </div>
                   </div>
                   <?php } ?> 
                   



                 </div>
                 <!-- / product container -->
               </div>
             </div>
              <!-- /tab 1 -->
               <!-- tab 2 -->
             <div id="suppliers" class="tab-pane fade">
               <!-- filters -->
               <div class="row">
                 <div class="filter-container">
                   <div class="col-9">
                     <form>
                      <label class="checkbox-inline">
                        <input type="checkbox" value="">Verified Manufacturers
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" value="">Accepts Small Orders
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" value="">Accepts Sample Orders
                      </label>
                    </form>
                  </div>

                  <div class="col-3">
                    <div class="sort">
                      <label for="sel1">Sort by:</label>
                      <select class="form-control" id="sel1">
                        <option>Best Match</option>
                        <option>Transaction Level</option>
                        <option>Response Rate</option>
                      </select>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                 </div>
               </div>
               <!--/ filters -->

               <div class="row">
                 <!-- supplier container -->
                 <div class="supplier-container">
                   <!-- single supplier -->
                   <div class="single-supplier">
                     <div class="prod-box">
                       <h4 class="prod-title-b"><a href="">Shenzhen Bluetimes Technology Co.</a></h4>
                       <div class="prod-img">
                         <ul>
                           <li><a href="">
                             <div class="prod">
                               <div class="img">
                                 <img src="images/product/mobilestand.jpg" alt="">
                               </div>
                               <h5>Product Name</h5>
                             </div>
                           </a></li>

                           <li><a href="">
                             <div class="prod">
                               <div class="img">
                                 <img src="images/product/phone.jpg" alt="">
                               </div>
                               <h5>Product Name</h5>
                             </div>
                           </a></li>

                           <li><a href="">
                             <div class="prod">
                               <div class="img">
                                 <img src="images/product/watch.jpg" alt="">
                               </div>
                               <h5>Product Name</h5>
                             </div>
                           </a></li>
                         </ul>

                         <p class="float-left"><a href="">+ view all products</a></p>
                       </div>
                       <div class="prod-detail txt-height">
                         <div class="company-details">
                           <p class="year">15th year</p>
                           <p class="loc">Main Products : <span><a href="">Car Charger</a>, <a href="">Wall Charger</a>, <a href="">Power Bank</a></span><br>
                             Location : <span>China (Mainland)</span></br>
                             Top 3 Markets : <span>South America, Africa, Western Europe</span> <br>
                             Response : <span>High / Less than 24h</span></p>
                             <div class="icon-details">
                               <div class="icon verified">Verified Information</div>
                               <div class="icon gold-plus">Gold Plus Supplier</div>
                             </div>
                           <div class="clearfix"></div>
                           <div class="contcts">
                             <a href="" class="btn btn-contact float-left">Contact Supplier</a>
                             <a href="" class="chat float-right" data-toggle="tooltip" title="Chat with us!"><i class="fas fa-comments"></i> Chat with us</a>
                             <div class="clearfix"></div>
                           </div>
                         </div>

                       </div>
                     </div>
                   </div>
                   <!-- /single supplier -->

                   <!-- single supplier -->
                   <div class="single-supplier">
                     <div class="prod-box">
                       <h4 class="prod-title-b"><a href="">Dongguan Arun Industrial Co. Ltd</a></h4>
                       <div class="prod-img">
                         <ul>
                           <li><a href="">
                             <div class="prod">
                               <div class="img">
                                 <img src="images/product/earphone.jpg" alt="">
                               </div>
                               <h5>Product Name</h5>
                             </div>
                           </a></li>

                           <li><a href="">
                             <div class="prod">
                               <div class="img">
                                 <img src="images/product/powerbank.jpg" alt="">
                               </div>
                               <h5>Product Name</h5>
                             </div>
                           </a></li>

                           <li><a href="">
                             <div class="prod">
                               <div class="img">
                                 <img src="images/product/mobilestand.jpg" alt="">
                               </div>
                               <h5>Product Name</h5>
                             </div>
                           </a></li>
                         </ul>

                         <p class="float-left"><a href="">+ view all products</a></p>
                       </div>
                       <div class="prod-detail txt-height">
                         <div class="company-details">
                           <p class="year">7th year</p>
                           <p class="loc">Main Products : <span><a href="">Car Charger</a>, <a href="">Wall Charger</a>, <a href="">Power Bank</a></span><br>
                             Location : <span>China (Mainland)</span></br>
                             Top 3 Markets : <span>South America, Africa, Western Europe </span><br>
                             Response : <span>High / Less than 24h</span></p>
                             <div class="icon-details">
                               <div class="icon verified">Verified Information</div>
                               <div class="icon silver">Silver Supplier</div>
                             </div>
                           <div class="clearfix"></div>
                           <div class="contcts">
                             <a href="" class="btn btn-contact float-left">Contact Supplier</a>
                             <a href="" class="chat float-right" data-toggle="tooltip" title="Chat with us!"><i class="fas fa-comments"></i> Chat with us</a>
                             <div class="clearfix"></div>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                   <!-- /single supplier -->

                   <!-- single supplier -->
                   <div class="single-supplier">
                     <div class="prod-box">
                       <h4 class="prod-title-b"><a href="">Shenzhen Bluetimes Technology Co.</a></h4>
                       <div class="prod-img">
                         <ul>
                           <li><a href="">
                             <div class="prod">
                               <div class="img">
                                 <img src="images/product/watch.jpg" alt="">
                               </div>
                               <h5>Product Name</h5>
                             </div>
                           </a></li>

                           <li><a href="">
                             <div class="prod">
                               <div class="img">
                                 <img src="images/product/phone.jpg" alt="">
                               </div>
                               <h5>Product Name</h5>
                             </div>
                           </a></li>

                           <li><a href="">
                             <div class="prod">
                               <div class="img">
                                 <img src="images/product/earphone.jpg" alt="">
                               </div>
                               <h5>Product Name</h5>
                             </div>
                           </a></li>
                         </ul>

                         <p class="float-left"><a href="">+ view all products</a></p>
                       </div>
                       <div class="prod-detail txt-height">
                         <div class="company-details">
                           <p class="year">9th year</p>
                           <p class="loc">Main Products : <span><a href="">Car Charger</a>, <a href="">Wall Charger</a>, <a href="">Power Bank</a></span><br>
                             Location : <span>China (Mainland)</span></br>
                             Top 3 Markets : <span>South America, Africa, Western Europe</span> <br>
                             Response : <span>High / Less than 24h</span></p>
                           <div class="icon-details">
                             <div class="icon verified">Verified Information</div>
                             <div class="icon gold">Gold Supplier</div>
                           </div>
                           <div class="clearfix"></div>
                           <div class="contcts">
                             <a href="" class="btn btn-contact float-left">Contact Supplier</a>
                             <a href="" class="chat float-right" data-toggle="tooltip" title="Chat with us!"><i class="fas fa-comments"></i> Chat with us</a>
                             <div class="clearfix"></div>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                   <!-- /single supplier -->

                 </div>
                 <!-- /supplier container
               </div>

             </div>
             /tab 2 -->
           </div>
        </div>
      </div>
    </div>
  </div>
</div>