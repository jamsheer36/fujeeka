<div class="total-container">
      <div class="container">

        <div class="main-area">
          <!-- <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#products">Products</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#suppliers">Suppliers</a>
            </li>
          </ul> -->

           <div class="tab-content">
             <!-- tab 1 -->
             <div id="products" class="tab-pane active">
             <div class="row">
                            <div class="col">
                                <div class="filter-cat">
                                    <h3>FILTER RESULTS BY : </h3>
                                    <div class="s-filter">
                                        <div class="s-cont">
                                            <select class="form-control cate" id="sel1">
                                              <option value="">Select a category</option>
                                              <?php foreach ($categories as $cat) { ?>
                                                <option value="<?php echo $cat['cat_id']; ?>"><?php echo $cat['cat_title']; ?></option>
                                              <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="s-filter">
                                        <div class="s-cont sub_cate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


               <!-- filters -->
               <div class="row">
                 <div class="filter-container">
                   <div class="col-9 d-flex">
                     <form class="d-flex">
                         
                         <div class="checkbox-inline">
                                              <input type="checkbox" <?php if($type=='hot-selling'){?> checked <?php }?> onclick="loadNewSort('hot_chk')" class="new_sort" value="hot-selling" id="hot_chk">
                          <label for="l-prd">Hot Selling</label>
                      </div>
                      
                      <div class="checkbox-inline">
                          <input type="checkbox" <?php if($type!='offer' && $type!='hot-selling'){?> checked <?php }?> onclick="loadNewSort('lat_chk')" class="new_sort"  value="latest" id="lat_chk">
                          <label for="f-prd">Latest Products</label>
                      </div>
                      
                      <div class="checkbox-inline">
                          <input type="checkbox" <?php if($type=='offer'){?> checked <?php }?> class="new_sort" onclick="loadNewSort('ofr_chk')" value="offer" id="ofr_chk">
                          <label for="o-prd">Offers</label>
                      </div>

                    </form>
                    <a href="<?= site_url('product/stock')?>" class="pl-2">View Stock Products</a>
                  </div>

                  <div class="col-3">
                    <div class="sort">
                      <label for="sort_by">Sort by:</label>
                      <select class="form-control" id="sort_by">
                        <option value="">None</option>
                        <option value="price_asc">Price Low to High</option>
                        <option value="price_desc">Price High to Low</option>
                      </select>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                 </div>
               </div>
               <!--/ filters -->
               <input type="hidden" id="prd_search" value="<?=$searchString?>">
               <input type="hidden" id="prd_type" value="<?=$type?>">
               <div class="row">
                 <!-- product container -->
                 <div class="ajax_loader1"></div>
                 <div class="prodct-container" id="product_listing">
                 </div>
                 
                             <div class="col-12">
                                <div id="pagination_link">
                                 </div>
                             </div>

                 <!-- / product container -->
               </div>
             </div>
              <!-- /tab 1 -->
               <!-- tab 2 -->
    
           </div>
        </div>
      </div>
    </div>
  </div>
</div>
