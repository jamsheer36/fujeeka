<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Update Stock</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/updateStock", array('id' => "frmProduct", 'class' => "form-horizontal"))?>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select required class="form-control col-md-9 col-xs-12" id="cst_supplier" name="cst_supplier">
                                        <option value="">Select Supplier</option>
                                        <?php foreach ((array) $suppliers as $key => $value) {?>
                                               <option value="<?php echo $value['supm_id'];?>"><?php echo $value['supm_name_en'];?></option>
                                          <?php }?>
                                   </select>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                              <div class="col-md-5 col-sm-6 col-xs-12">
                                   <?php
                                     build_category_tree($this, $locations, 0);
                                   ?>
                                   <div class="div-category">
                                        <ul class="li-category">
                                             <?php echo $locations?>
                                        </ul>
                                   </div>
                                   <?php

                                     function build_category_tree($f, &$output, $preselected, $parent = 0, $indent = "") {
                                          $ser_parent = '';
                                          $parentCategories = $f->category_model->getCategoryChaild($parent);
                                          foreach ($parentCategories as $key => $value) {
                                               $selected = ($value["cat_id"] == $ser_parent) ? "ckecked=\"true\"" : "";
                                               $output .= "<li>" . $indent . "<input name='cst_category' value='" . $value["cat_id"] . "' type='radio' /><span>" . $value["cat_title"] . '</span></li>';
                                               if ($value["cat_id"] != $parent) {
                                                    build_category_tree($f, $output, $preselected, $value["cat_id"], $indent . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                               }
                                          }
                                     }
                                   ?>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Item Name</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input placeholder="Item Name" type="text" name="cst_item_name" id="cst_item_name" 
                                          class="form-control col-md-7 col-xs-12"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">QTY</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input placeholder="Quatity" type="text" name="cst_item_qty" id="cst_item_qty" 
                                          class="form-control col-md-7 col-xs-12 numOnly"/>
                              </div>
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>