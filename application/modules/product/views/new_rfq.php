<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>REQUEST FOR QUOTE</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <div class="row pt-5 pb-5">
                              <div class="col-md-12 border-right m-grey-txt">
                                   <h4 class="heading3 green-txt">Tell suppliers your requirements!</h4>
                                   <p>
                                        Provide as much details as possible about your request to ensure fast response from the right suppliers. 
                                        The higher the score the better responses you will get.
                                   </p>
                              </div>
                         </div>
                         <?php echo form_open_multipart('product/rfq', array('id' => "frmProduct", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Category</label>
                              <div class="col-md-9 col-sm-6 col-xs-12">
                                   <select class="form-control" id="sel1" name="rfq_category" required data-parsley-required-message="Select a category">
                                        <option value="">Select a category</option>
                                        <?php foreach ($categories as $cat) {?>
                                               <option value="<?php echo $cat['cat_id'];?>"><?php echo $cat['cat_title'];?></option>
                                          <?php }?>
                                   </select>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market</label>
                              <div class="col-md-9 col-sm-6 col-xs-12">
                                   <select class="form-control" id="markets" name="rfq_market">
                                        <option value="">Select Market</option>
                                        <?php foreach ((array) $marketPlaces as $key => $value) {?>
                                               <option value="<?php echo $value['mar_id'];?>"><?php echo $value['mar_name'];?></option>
                                          <?php }?>
                                   </select>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Name</label>
                              <div class="col-md-9 col-sm-6 col-xs-12">
                                   <input type="text" required data-parsley-required-message="Enter product name" class="form-control commonAutoComplete"
                                          id="product" placeholder="Keywords of products you are looking for" name="pname" 
                                          data-url="<?php echo site_url('product/productAutoName');?>">
                                   <input type="hidden" id="prd_id" value="0" name="rfq_product">
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Quantity</label>
                              <div class="col-md-9 col-sm-6 col-xs-12">
                                   <div class="input-group">
                                        <input type="text" class="form-control inp_qty" style="width: 35%;"  data-parsley-type-message="Quantity should be a valid number" 
                                               data-parsley-type="number" data-parsley-errors-container="#qtr_err" placeholder="Quantity" name="rfq_qty"
                                               required data-parsley-required-message="Enter Quantity">

                                        <select  class="form-control" data-toggle="dropdown" name="rfq_unit" id="unit_sel" style="width: 65%;border-left: none;">
                                             <option value="">Select Unit</option>
                                             <?php foreach ((array) $units as $key => $value) {?>
                                                    <option value="<?php echo $value['unt_id'];?>"><?php echo $value['unt_unit_name_en'];?></option>
                                               <?php }?>
                                        </select>
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Requirement</label>
                              <div class="col-md-9 col-sm-6 col-xs-12">
                                   <textarea placeholder="Requirement" style="width: 100%;" name="rfq_requirment"></textarea>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                              <div class="col-md-9 col-sm-6 col-xs-12">
                                   <label class="btn-bs-file">
                                        <i class="fa fa-paperclip"></i> Attach files <small>(Maximum File Size: 2MB)</small>
                                        <input type="file" data-parsley-errors-container="#file_err" data-parsley-fileextension="jpg,png,gif,jpeg,pdf,docx,xlsx,xls,txt" 
                                               id="attach_file" name="rfq_attachment" /><span id="file_name"></span>
                                   </label>
                                   <span id="file_err"></span>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Valid To</label>
                              <div class="col-md-9 col-sm-6 col-xs-12">
                                   <div class="input-group">
                                        <select class="form-control" id="validity" style="width: 44%;">
                                             <option value="7">7 Days</option>
                                             <option value="30">30 Days</option>
                                             <option value="45">45 Days</option>
                                             <option value="90">90 Days</option>
                                        </select>
                                        <div class="input-group-prepend">
                                             <input type="date" class="form-control" style="border-left: none;width: 111%;" id="date" placeholder="" name="rfq_validity">
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Business Email</label>
                              <div class="col-md-9 col-sm-6 col-xs-12">
                                   <input type="email" required data-parsley-trigger="change" placeholder="Business Email"
                                          data-parsley-required-message="Email required" class="form-control" id="email" name="rfq_mail">
                              </div>
                         </div>

                         <!-- -->
                         <div class="input-group">
                              <div class="control-label col-md-12 col-sm-12 col-xs-12">
                                   <div id="captcha_img" style="float: left;"><?php echo $captcha['image'];?></div>
                                   <a style="float: left;" class="btn btn-default btn-sm refresh_captcha"><i class="fa fa-refresh"></i></a>
                                   
                                   <input style="float: left;width: 50%;" data-parsley-equalto="#captcha_code" data-parsley-trigger="change" 
                                          data-parsley-equalto-message="Incorrect captcha" placeholder="Enter the captcha text"
                                          id="captcha"  type="text" required="required" autocomplete="off"
                                          class="form-control validate md-form mt-3">
                              </div>
                              <div class="col-md-10 col-sm-6 col-xs-12">
                                   <input value="<?php echo $captcha['word'];?>" type="hidden" id="captcha_code"/>
                                   
                              </div>
                         </div>
                         <label for="captcha" data-error="wrong" data-success="right"></label>
                         <!-- -->

                         <div class="form-group form-check">
                              <label class="form-check-label">
                                   <input class="form-check-input" type="checkbox" name="chkConfirm" required data-parsley-trigger="change" 
                                          data-parsley-required-message="You should agree this"> I agree to share my 
                                   <a href="javascript:;" style="color: #00b904;">business card</a> with quoted suppliers
                              </label>
                         </div>
                         <p class="small m-grey-txt">Please see our <a href="javascript:;">Privacy Policy</a> for information regarding 
                              the processing of 
                              your data. By submitting you agree that you have read our Privacy Policy and accepted our 
                              <a href="javascript:;">Terms of Use</a>. 
                         </p>

                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">SEND RFQ</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>WITH ONE REQUEST GET MULTIPLE QUOTES</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <p class=" m-grey-txt">Easiest way to get quotes from suppliers</p>
                         <ul class="pts">
                              <li>Get quotes for your custom request</li>
                              <li>Let the right suppliers find you</li>
                              <li>Close deals with ease</li>
                         </ul>

                         <img src="images/banners/rfq-sm.jpg" alt="Request for Quotation" class="mt-4">

                         <div class="faq mt-5">
                              <h4 class="dt-heading">FAQ</h4>
                              <ul class="pts">
                                   <li>The buyer’s email address should be verified before posting a buying request. </li>
                                   <li>Necessary information: product name included in the subject, and other product details in the body part, such as size, color, type, place of origin, variety, and any other specifications. The more, the better.
                                        P.S.: there might be some special specifications in certain industries.</li>
                                   <li>It is highly suggested to contact the owner or authorized distributor of any trademarked/branded product to buy it.</li>
                                   <li>As we are a wholesale platform, larger MOQ (minimum order quantity) orders tend to be preferred. Note that MOQ will vary depending on the product type.</li>
                              </ul>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     .input-group {
          position: relative;
          display: -ms-flexbox;
          display: flex;
          -ms-flex-wrap: wrap;
          flex-wrap: wrap;
          -ms-flex-align: stretch;
          align-items: stretch;
          width: 100%;
          margin-bottom: 0px;
     }

     .btn-bs-file {
          position: relative;
          cursor: pointer;
     }

     .btn-bs-file small {
          color: #919191;
     }

     .btn-bs-file i {
          color: #ededed;
          background: #00b904;
          margin-right: 10px;
          padding: 3px 7px;
          font-size: 19px;
     }

     .btn-bs-file input[type="file"] {
          position: absolute;
          top: -9999999;
          filter: alpha(opacity=0);
          opacity: 0;
          width: 0;
          height: 0;
          outline: none;
          cursor: inherit;
     }
</style>

<script src="js/rfq.js"></script>