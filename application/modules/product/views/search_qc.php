<div class="total-container">
     <div class="container">
          <div class="main-area">

               <div class="row">
                    <!-- product container -->
                    <div class="prodct-container">
                         <!-- single product -->
                         <?php
                           foreach ((array) $serachList as $key => $logistics) {
                                ?>
                                <div class="market-place">
                                     <div class="prod-box">
                                          <div class="prod-img">
                                               <a href="<?php echo site_url('logistics/get-quote/' . encryptor($logistics['usr_id']));?>">
                                                    <?php
                                                    $image = isset($logistics['images'][0]['lci_image']) ? UPLOAD_PATH . 'logistics/' . $logistics['images'][0]['lci_image'] : '';
                                                    echo img(array('src' => $image));
                                                    ?>
                                               </a>
                                          </div>
                                          <div class="prod-detail txt-height">
                                               <h4 class="prod-title-s">
                                                    <a href="<?php echo site_url('logistics/get-quote/' . encryptor($logistics['usr_id']));?>">
                                                         <?php echo $logistics['usr_first_name'];?>
                                                    </a>
                                               </h4>
                                               <div class="company-details">
                                                    <p class="loc">Country : <span><?php echo $logistics['ctr_name'];?></span><br>
                                                         Contact No : <span><?php echo $logistics['usr_phone'];?></span></p>
                                                    <?php
                                                    if (isset($logistics['logisticsTypesSelected']) && !empty($logistics['logisticsTypesSelected'])) {
                                                         echo '<p class="loc mt-3">Types : <span>';
                                                         foreach ((array) $logistics['logisticsTypesSelected'] as $typekey => $types) {
                                                              echo $types['logt_title'];
                                                         }
                                                         echo '</span></p>';
                                                    }
                                                    ?>
                                                    <div class="clearfix"></div>
                                               </div>
                                               <div class="contcts">
                                                    <h5>Description</h5>
                                                    <p> <?php echo $logistics['usr_description1']?></p>
                                                    <div class="clearfix"></div>
                                               </div>
                                          </div>
                                     </div>
                                </div>
                           <?php }
                         ?>
                    </div>
               </div>
          </div>
     </div>
</div>