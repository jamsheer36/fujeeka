<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>RFQ list</h2>
                         <div class="clearfix"></div>
                    </div>
                    <?php if(check_permission('product','report')) { ?> 
                         <div class="x_content lg-bg">
                         <form id="demo-form2" class="form-inline f_tp" method="post" action="<?php echo site_url($controller . '/report');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <input value="0" type="hidden" name="" id="usr_id"/>
                              <div class="form-group">
                                       <label class="control-label" for="first-name">Start Date<span class="required">*</span>
                                   </label>
                                        <input type="text" id="date1" required="required" class="form-control" data-parsley-required-message="Start Date is required" name="start_date">
                              </div>
                              <div class="form-group">
                                       <label class="control-label" for="last-name">End Date <span class="required">*</span>
                                   </label>
                                        <input type="text" id="date2" name="end_date" required="required" data-parsley-required-message="End Date is required" class="form-control">
                              </div>

                                        <button type="submit"  name='type' value="excel" class="btn btn-success">Excel</button>
                                         <button type="submit" name='type' value="pdf" class="btn btn-danger">Pdf</button>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
                  <?php } ?>

                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Buyer</th>
                                        <th>Email</th>
                                        <th>Product</th>
                                        <th>Category</th>
                                        <th>Quantity</th>
                                        <th>Validity</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ($rfq as $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/rfq-list/' . encryptor($value['rfq_id']));?>">
                                               <td class="trVOE"><?php echo $value['usr_first_name'];?></td>
                                               <td class="trVOE"><?php echo $value['rfq_mail'];?></td>
                                               <td class="trVOE"><?php echo !empty($value['prd_name_en']) ? $value['prd_name_en'] : $value['rfq_product_name'];?></td>
                                               <td class="trVOE"><?php echo $value['cat_title'];?></td>
                                               <td class="trVOE"><?php echo $value['rfq_qty'].' '.$value['unt_unit_en'];?></td>
                                               <td class="trVOE"><?php echo $value['rfq_validity'];?></td>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>
<script>
$(function() {
  $('input[name="start_date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
     locale: {
      format: 'YYYY-MM-DD'
    }
  });
});
</script>
           
 <script>
$(function() {
  $('input[name="end_date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
     locale: {
     format: 'YYYY-MM-DD'
    }
  });
});
</script>
