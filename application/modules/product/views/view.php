<div class="right_col" role="main">
   <div class="clearfix"></div>
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2><?php if(isset($from)){ echo 'Duplicate Product'; } else {echo 'Update Product';}?></h2>
               <ul class="nav navbar-right panel_toolbox">
                  <li>
                     <?php if (is_root_user()) {?>
                     <input type="checkbox" class="js-switch chkOnchange" value="1" data-switchery="true"
                        data-url="<?php echo site_url($controller . '/changestatus/' . encryptor($products['prd_id'])); ?>"
                        <?php echo $products['prd_status'] == 1 ? 'checked' : ''; ?>>
                     <?php }?>
                  </li>
               </ul>
               <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <br />
               <?php 
               if(isset($from)){
                    echo form_open_multipart($controller . "/add", array('id' => "frmProduct", 'class' => "form-horizontal", 'data-parsley-validate' => "true"));
               } else{
                    echo form_open_multipart($controller . "/update", array('id' => "frmProduct", 'class' => "form-horizontal", 'data-parsley-validate' => "true"));
               } ?>
               <input type="hidden" name="prd_id" value="<?php echo $products['prd_id']; ?>" />
               <div class="widget-body">
                  <div class="" role="tabpanel" data-example-id="togglable-tabs">
                     <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active">
                           <a href="javascript:;" goto="#basic_details" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Basic Details</a>
                        </li>
                        <li role="presentation" class="pars">
                           <a href="javascript:;" goto="#specification" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Specification</a>
                        </li>
                        <li role="presentation" class="pars">
                           <a href="javascript:;" goto="#settings" role="tab" id="settings-tab" data-toggle="tab" aria-expanded="false">Settings</a>
                        </li>
                     </ul>
                     <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="basic_details" aria-labelledby="home-tab">
                           <?php if (is_root_user() || $this->usr_grp == 'SBA') {?>
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier</label>
                              <div class="col-md-5 col-sm-6 col-xs-12">
                                 <select name="product[prd_supplier]" class="form-control col-md-9 col-xs-12" id="prd_stock_number">
                                    <?php foreach ((array) $suppliers as $key => $value) {?>
                                    <option value="<?php echo $value['supm_id']; ?>" <?php if ($value['supm_id'] == $products['prd_supplier']) {
                                       echo "selected";
                                       }
                                       ?> ><?php echo $value['supm_name_en']; ?></option>
                                    <?php }?>
                                 </select>
                              </div>
                           </div>
                           <?php } else {?>
                           <input value="<?php echo $products['prd_supplier']; ?>" type="hidden" name="product[prd_supplier]"/>
                           <?php }?>
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Product name english</label>
                              <div class="col-md-5 col-sm-6 col-xs-12">
                                 <input required data-parsley-required-message="Enter Name" placeholder="Product Name English" type="text" value="<?php echo $products['prd_name_en']; ?>" name="product[prd_name_en]" id="supm_name_en"
                                    class="form-control col-md-9 col-xs-12"/>
                              </div>
                              <div class="col-md-1 col-sm-1 col-xs-1">
                                 <span style="float: right;cursor: pointer;font-size: 28px;"
                                    class="glyphicon glyphicon-globe btnOtherLanguage"></span>
                              </div>
                           </div>
                           <div class="divOtherLanguage" style="display: none;">
                              <div class="form-group">
                                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Product name arabic</label>
                                 <div class="col-md-5 col-sm-6 col-xs-12">
                                    <input placeholder="Product Name Arabic" type="text" value="<?php echo $products['prd_name_ar']; ?>" name="product[prd_name_ar]" id="supm_name_ar"
                                       class="form-control col-md-9 col-xs-12"/>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Product name chinese</label>
                                 <div class="col-md-5 col-sm-6 col-xs-12">
                                    <input placeholder="Product Name Chinese" type="text" value="<?php echo $products['prd_name_ch']; ?>" name="product[prd_name_ch]" id="supm_name_ch"
                                       class="form-control col-md-9 col-xs-12"/>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">MOQ</label>
                              <div class="col-md-5 col-sm-6 col-xs-12 stockText">
                                 <input placeholder="Minimum Order Quantity" type="text" value="<?php echo $products['prd_moq']; ?>" name="product[prd_moq]"
                                    max="<?php echo get_settings_by_key('moq_limit'); ?>" class="form-control col-md-9 col-xs-12 numOnly"/>
                                 <small>Minimum Order Quantity</small>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Is stock product?</label>
                              <div class="col-md-2 col-sm-3 col-xs-12 stockText">
                                 <input type="checkbox" name="product[prd_is_stock_prod]" <?php echo $products['prd_is_stock_prod'] ? 'checked' : '';?>
                                    class="js-switch chkToggle chkIsStock" value="1" data-toggle="divStock" data-switchery="true">
                                 <!-- <small>Minimum Order Quantity</small> -->
                              </div>
                              <div class="divDeadStockQty <?php echo $products['prd_is_stock_prod'] == 0 ? 'hidden' : '';?>">
                                 <label class="control-label col-md-2 col-sm-3 col-xs-12">Add to flash sale?</label>
                                 <div class="col-md-5 col-sm-3 col-xs-12 stockText">
                                    <input type="checkbox" name="product[prd_is_flash_sale]" <?php echo $products['prd_is_flash_sale'] ? 'checked' : '';?>
                                       class="js-switch chkToggle"   data-toggle="" data-switchery="true">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group divDeadStockQty <?php echo $products['prd_is_stock_prod'] == 0 ? 'hidden' : ''; ?>">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Stock quantity</label>
                              <div class="col-md-5 col-sm-6 col-xs-12 stockText">
                                 <input <?php echo $products['prd_is_stock_prod'] == 1 ? 'required' : ''; ?>
                                    data-parsley-required-message="Dead stock quantity" placeholder="Dead stock quantity"
                                    type="text" name="product[prd_stock_qty]" value="<?php echo $products['prd_stock_qty']; ?>"
                                    class="form-control col-md-9 col-xs-12 numOnly prd_stock_qty"/>
                                 <small>Dead stock quantity</small>
                              </div>
                           </div>
                           <!-- Stock -->
                           <!-- <?php $isStock = (!empty($products['prd_unit']) && !empty($products['prd_qty'])) ? true : false;?> -->
                           <!-- <div class="divStock" style="<?php echo !$isStock ? 'display: none;' : ''; ?>"> -->
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Unit</label>
                              <div class="col-md-5 col-sm-6 col-xs-12">
                                 <select required data-parsley-required-message="Select Unit" name="product[prd_unit]" class="form-control col-md-9 col-xs-12 cmbUnit" id="prd_stock_number">
                                    <option value="">Select unit</option>
                                    <?php foreach ((array) $units as $key => $value) {?>
                                    <option data='<?php echo json_encode($value); ?>' value="<?php echo $value['unt_id']; ?>"
                                       <?php echo $value['unt_id'] == $products['prd_unit'] ? 'selected="selected"' : ''; ?>>
                                       <?php echo $value['unt_unit_en'] . ' (' . $value['unt_unit_name_en'] . ')'; ?>
                                    </option>
                                    <?php }?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Quantity</label>
                              <div class="col-md-5 col-sm-6 col-xs-12">
                                 <input placeholder="Product quantity" type="text" name="product[prd_qty]" id="prd_qty"
                                    class="form-control col-md-9 col-xs-12 numOnly" value="<?php echo $products['prd_qty']; ?>"/>
                                 <small class="smlQty"><?php echo $products['unt_desc_en']; ?></small>
                              </div>
                           </div>
                           <!-- </div> -->
                           <!-- Stock -->
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Price range (USD)</label>
                              <div class="col-md-2 col-sm-6 col-xs-12">
                                 <input required  data-parsley-required-message="Enter Min Price" placeholder="Min" type="text" value="<?php echo $products['prd_price_min']; ?>" name="product[prd_price_min]" id="supm_name_en"
                                    class="form-control col-md-9 col-xs-12 numOnly"/>
                              </div>
                              <div class="col-md-2 col-sm-6 col-xs-12">
                                 <input required data-parsley-required-message="Enter Max Price" placeholder="Max" type="text" value="<?php echo $products['prd_price_max']; ?>" name="product[prd_price_max]" id="supm_name_en"
                                    class="form-control col-md-9 col-xs-12 numOnly"/>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Offer price range (USD)</label>
                              <div class="col-md-2 col-sm-6 col-xs-12">
                                 <input value="<?php echo $products['prd_offer_price_min']; ?>" placeholder="Min" type="text" name="product[prd_offer_price_min]" id="prd_offer_price_min"
                                    class="form-control col-md-9 col-xs-12 numOnly"/>
                              </div>
                              <div class="col-md-2 col-sm-6 col-xs-12">
                                 <input value="<?php echo $products['prd_offer_price_max']; ?>" placeholder="Max" type="text" name="product[prd_offer_price_max]" id="prd_offer_price_max"
                                    class="form-control col-md-9 col-xs-12 numOnly"/>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                 <div class="div-category">
                                    <ul class="li-category">
                                       <?php
                                          foreach ($category as $cat) {
                                              $selected = in_array($cat["cat_id"], $products['catId']) ? "checked='ckecked'" : "";
                                              ?>
                                       <li>
                                          <input <?php echo $selected ?> name="categories[]" required data-parsley-errors-container="#cat_err" data-parsley-required-message="Select a category" value="<?php echo $cat['cat_id'] ?>" type="checkbox"/><span><?php echo $cat['cat_title'] ?></span>
                                       </li>
                                       <?php }?>
                                    </ul>
                                 </div>
                                 <span id="cat_err"></span>
                              </div>
                           </div>
                           <?php if (!empty($products['keyword'])) {
                              foreach ($products['keyword'] as $key => $value) {?>
                           <div class="form-group">
                              <?php if ($key == 0) {?>
                              <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Product keywords</label>
                              <?php } else {?>
                              <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                              <?php }?>
                              <div class="col-md-6 col-sm-3 col-xs-12">
                                 <input id="keywords_en" placeholder="Keyword" value="<?php echo $value['pkwd_val_en']; ?>" class="form-control col-md-7 col-xs-12" type="text" name="keywords[en][]">
                              </div>
                              <?php if($key == 0) { ?>
                              <div class="col-md-1 col-sm-1 col-xs-12">
                                 <span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMoreKeys"></span>
                              </div>
                              <?php } else { ?>
                              <div class="col-md-1 col-sm-1 col-xs-12">
                                 <span style="cursor: pointer;" class="glyphicon glyphicon-trash btnAddRemoveKeys"></span>
                              </div>
                              <?php } ?>
                           </div>
                           <?php } } else { ?>
                           <div class="form-group">
                              <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Product keywords</label>
                              <div class="col-md-6 col-sm-3 col-xs-12">
                                 <input id="keywords_en" placeholder="Keyword" class="form-control col-md-7 col-xs-12" type="text" name="keywords[en][]">
                              </div>
                              <div class="col-md-1 col-sm-1 col-xs-12">
                                 <span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMoreKeys"></span>
                              </div>
                           </div>
                           <?php } ?>   
                           <div  id="divMoreProductKeywords"></div>
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                              <div class="col-md-5 col-sm-6 col-xs-12"  style="width: 50%;">
                                 <textarea required data-parsley-required-message="Enter Product Description" placeholder="Description" class="txtProdDesc editor" name="product[prd_desc]"><?php echo $products['prd_desc']; ?></textarea>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Product video link</label>
                              <div class="col-md-5 col-sm-6 col-xs-12">
                                 <input placeholder="Product Video" type="text" value="<?php echo $products['prd_video']; ?>" name="product[prd_video]" id="supm_name_en"
                                    class="form-control col-md-9 col-xs-12"/>
                              </div>
                           </div>
                           <?php if(isset($from)){ ?>
                           <div class="form-group">
                              <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Product images
                              <small>(maximum <?php echo get_settings_by_key('prod_img_limit');?> images)</small>
                              </label>
                              <div class="col-md-5 col-sm-3 col-xs-12">
                                 <div id="newupload">
                                    <input type="hidden" id="x10" name="x12[]" />
                                    <input type="hidden" id="y10" name="y12[]" />
                                    <input type="hidden" id="x20" name="x22[]" />
                                    <input type="hidden" id="y20" name="y22[]" />
                                    <input type="hidden" id="w0" name="w2[]" />
                                    <input type="hidden" id="h0" name="h2[]" />
                                    <input required data-parsley-required-message="upload atleast one image" data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control col-md-7 col-xs-12" name="shopImages[]"
                                       id="image_file0" onchange="fileSelectHandler('0', '1000', '882', true)" />
                                    <img id="preview0" class="preview"/>
                                    <span class="help-inline">Choose 1000(W) X 882(H)</span>
                                 </div>
                                 <div id="altTag">
                                    <input placeholder="Alt tag for image" type="text" name="altTags[]" id="image_alt0" data-parsley-required ="true"data-parsley-required-message="Please enter an alt tag for the image"
                                       class="form-control col-md-9 col-xs-12"/>
                                 </div>
                              </div>
                              <div class="col-md-1 col-sm-1 col-xs-12">
                                 <span style="float: right;cursor: pointer;"
                                    data-limit="<?php echo get_settings_by_key('prod_img_limit');?>"
                                    class="glyphicon glyphicon-plus btnMoreProductImages"></span>
                              </div>
                           </div>
                           <div  id="divMoreProductImages"></div>
                           <?php } else { ?>
                           <?php 
                              if (isset($products['images']) && !empty($products['images'])) {
                                  $i = 0;
                                  ?>
                           <div class="form-group ">
                              <?php
                                 foreach ($products['images'] as $key => $value) {
                                         if ($i % 4 == 0) {
                                             ?>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                              <?php }?>
                              <div class="productImages col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['pimg_id']; ?>" style="padding-left: 10px;">
                                 <div class="input-group">
                                    <?php echo img(array('src' => FILE_UPLOAD_PATH . 'product/' . $value['pimg_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage')); ?>
                                 </div>
                                 <?php if ($value['pimg_image']) {?>
                                 <span class="help-block">
                                 <a data-url="<?php echo site_url($controller . '/removeImage/' . $value['pimg_id']); ?>"
                                    img-count="<?php echo $i; ?>"
                                    href="javascript:void(0);" style="width: 100px;"
                                    class="btn btn-block btn-danger btn-xs btnDeleteProductImage">Delete</a>
                                 </span>
                                 <span class="help-block">
                                 <input   type="radio" name="supm_default_image" class="btnSetDefaultImage"
                                    <?php echo ($value['pimg_id'] == $products['prd_default_image']) ? 'checked="checked"' : ''; ?>
                                    data-url="<?php echo site_url($controller . '/setDefaultImage/' . $value['pimg_id'] . '/' . $products['prd_id']); ?>"/>&nbsp; Make it default
                                 </span>
                                 <div id="altTag">
                                    <input placeholder="Alt tag for image" type="text" name="imgaltTags[<?php echo $value['pimg_id']; ?>]" id="image_alt0" data-parsley-required ="true"data-parsley-required-message="Please enter an alt tag for the image" value="<?php echo $value['pimg_alttag']; ?>"
                                       class="form-control col-md-9 col-xs-12"/>
                                    <?php }?>
                                 </div>
                              </div>
                              <?php
                                 $i++;
                                     }
                                     ?>
                           </div>
                           <?php }
                              ?>
                           <?php if (count($products['images']) < get_settings_by_key('prod_img_limit')) {?>
                           <div class="form-group">
                              <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Product Images
                              <small>(maximum <?php echo get_settings_by_key('prod_img_limit'); ?> images)</small>
                              </label>
                              <div class="col-md-5 col-sm-3 col-xs-12">
                                 <div id="newupload">
                                    <input type="hidden" id="x10" name="x12[]" />
                                    <input type="hidden" id="y10" name="y12[]" />
                                    <input type="hidden" id="x20" name="x22[]" />
                                    <input type="hidden" id="y20" name="y22[]" />
                                    <input type="hidden" id="w0" name="w2[]" />
                                    <input type="hidden" id="h0" name="h2[]" />
                                    <input  <?php if (!isset($products['images']) || empty($products['images'])) {?> data-parsley-required ="true" <?php } else {?>  data-parsley-required ="false" <?php }?>
                                       data-parsley-required-message="Please Upload a file"  type="file" data-parsley-fileextension="jpg,png,gif,jpeg"  class="form-control col-md-7 col-xs-12" name="shopImages[]"
                                       id="image_file0" onchange="fileSelectHandler('0', '1000', '882', '1')" />
                                    <img id="preview0" class="preview"/>
                                    <span class="help-inline">Choose 1000(W) X 882(H)</span>
                                 </div>
                                 <div id="altTag">
                                    <input <?php if (!isset($products['images']) || empty($products['images'])) {?> data-parsley-required ="true" <?php } else {?>  data-parsley-required ="false" <?php }?>
                                       data-parsley-required-message="Please enter an alt tag for the image"   placeholder="Alt tag for image" type="text" name="altTags[]" id="image_alttag"
                                       class="form-control col-md-9 col-xs-12"/>
                                 </div>
                              </div>
                              <div class="col-md-1 col-sm-1 col-xs-12">
                                 <span style="float: right;cursor: pointer;"
                                    data-limit="<?php echo get_settings_by_key('prod_img_limit'); ?>"
                                    class="glyphicon glyphicon-plus btnMoreProductImages"></span>
                              </div>
                           </div>
                           <div  id="divMoreProductImages"></div>
                           <?php } }?>
                           <div class="ln_solid"></div>
                           <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                 <button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>
                              </div>
                           </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="specification" aria-labelledby="home-tab">
                           <div class="form-group">
                              <label for="enq_cus_email" class="control-label col-md-2 col-sm-3 col-xs-12">Specification</label>
                              <div class="col-md-10 col-sm-3 col-xs-12">
                                 <div class="table-responsive divVehDetailsSale">
                                    <table class="tblModifiedTable table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                       <thead>
                                          <tr>
                                             <th colspan="3" style="text-align: center;">Key</th>
                                             <th colspan="3" style="text-align: center;">Value</th>
                                             <th><span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMore"></span></th>
                                          </tr>
                                          <tr>
                                             <th>English</th>
                                             <th>Arabic</th>
                                             <th>Chinese</th>
                                             <th>English</th>
                                             <th>Arabic</th>
                                             <th>Chinese</th>
                                             <th></th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php
                                             if (!empty($products['specification'])) {
                                                 foreach ((array) $products['specification'] as $key => $value) {
                                                     ?>
                                          <tr>
                                             <td>
                                                <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['psp_key_en']; ?>"  name="specification_key[en][]">
                                             </td>
                                             <td>
                                                <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['psp_key_ar']; ?>"  name="specification_key[ar][]">
                                             </td>
                                             <td>
                                                <input  id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['psp_key_ch']; ?>"  name="specification_key[ch][]">
                                             </td>
                                             <td>
                                                <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['psp_val_en']; ?>" name="specification_val[en][]">
                                             </td>
                                             <td>
                                                <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['psp_val_ar']; ?>" name="specification_val[ar][]">
                                             </td>
                                             <td>
                                                <input  id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['psp_val_ch']; ?>" name="specification_val[ch][]">
                                             </td>
                                             <td>
                                                <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                             </td>
                                          </tr>
                                          <?php
                                             }
                                             } else {
                                                 ?>
                                          <tr>
                                             <td>
                                                <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" name="specification_key[en][]">
                                             </td>
                                             <td>
                                                <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" name="specification_key[ar][]">
                                             </td>
                                             <td>
                                                <input  id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" name="specification_key[ch][]">
                                             </td>
                                             <td>
                                                <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" name="specification_val[en][]">
                                             </td>
                                             <td>
                                                <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" name="specification_val[ar][]">
                                             </td>
                                             <td>
                                                <input  id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" name="specification_val[ch][]">
                                             </td>
                                             <td>
                                                <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                             </td>
                                          </tr>
                                          <?php }?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <div class="ln_solid"></div>
                           <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                 <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>
                                 <button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>
                              </div>
                           </div>
                        </div>
                        <!-- Settings Tab -->
                        <div role="tabpanel" class="tab-pane fade" id="settings" aria-labelledby="settings-tab">
                           <div class="form-group">
                              <label for="enq_cus_email" class="control-label col-md-2 col-sm-3 col-xs-12">Settings</label>
                              <div class="col-md-12">
                                 <div class="divider"></div>
                                 <div class="row m-t-10">
                                    <div class="col-md-12">
                                       <input type="checkbox" class="js-switch chkChangeOrderStatuse" name="product[prd_oem]"
                                          data-url="<?php echo site_url('general/toggler/products/prd_oem/prd_id/' . encryptor($products['prd_id'])); ?>"
                                          <?php echo ($products['prd_oem'] == 1) ? "checked" : ''; ?> value="1" data-switchery="true">
                                       OEM <small>(Original Equipment Manufacturer)</small>
                                    </div>
                                    <!--<div class="col-md-4">-->
                                    <!--     <div class="fullwidth">OEM <small>(Original Equipment Manufacturer)</small></div>-->
                                    <!--</div>-->
                                    <!--<div class="col-md-7 control-label text-inverse f-w-600">OEM</div>-->
                                 </div>
                                 <div class="row m-t-10">
                                    <div class="col-md-12">
                                       <input type="checkbox" class="js-switch" name="product[prd_hot_selling]"
                                          <?php echo ($products['prd_hot_selling'] == 1) ? "checked" : ''; ?> value="1" data-switchery="true">
                                       Hot Selling
                                    </div>
                                    <!--<div class="col-md-4">Hot Selling</div>-->
                                 </div>
                                 <div class="row m-t-10">
                                    <div class="col-md-12">
                                       <input type="checkbox" class="js-switch" name="product[prd_is_show_on_profile]"
                                          <?php echo ($products['prd_is_show_on_profile'] == 1) ? "checked" : ''; ?> value="1" data-switchery="true">
                                       Show on your profile page?
                                    </div>
                                    <!--<div class="col-md-10">Show on your profile page?</div>-->
                                 </div>
                                 <div class="row m-t-10">
                                    <div class="col-md-12">
                                       <input type="checkbox" class="js-switch" name="product[prd_is_main_product]"
                                          <?php echo ($products['prd_is_main_product'] == 1) ? "checked" : ''; ?> value="1" data-switchery="true">
                                       Is main product?
                                    </div>
                                    <!--<div class="col-md-4"></div>-->
                                 </div>
                              </div>
                           </div>
                           <div class="ln_solid"></div>
                           <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                 <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>
                                 <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success">Submit</button>' : ''; ?>
                              </div>
                           </div>
                        </div>
                        <!-- /Settings Tab -->
                     </div>
                  </div>
               </div>
               <?php echo form_close() ?>
            </div>
         </div>
      </div>
   </div>
</div>
<style>
   .div-category {
   max-height: 260px;
   overflow-x: hidden;
   overflow-y: scroll;
   border: 2px solid #EEEEEE;
   width: 500px;
   }
   .li-category {
   list-style: none;
   }
   .li-category li {
   margin-left: -20px;
   }
   .li-category li span {
   margin-left: 10px;
   }
   label.error {
   display: none !important;
   }
   input.error {
   border: 1px solid red;
   }
</style>
<script>
   $(".pars").click(function () {
        if (!$('#frmProduct').parsley().validate())
             return false;
   });
   
     $(document).ready(function () {
        $(".redactor-editor").keyup(function () {
             var content = $('.txtProdDesc').val();
             if($(content).text()) {
                  $(".txtProdDesc").removeAttr("required");
             } else {
                  $(".txtProdDesc").attr("required", "required");
             }
             $('#frmProduct').parsley().reset();
             var $form = $('#frmProduct');
             $form.parsley('validate');
        });
   });
   
</script>