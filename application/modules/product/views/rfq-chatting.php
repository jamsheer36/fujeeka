<?php
  if (isset($rfq['comments'])) {
       $tmpId = is_root_user() ? $rfqDetails['rfq_user'] : $this->uid;
       foreach ((array) $rfq['comments'] as $key => $value) {
            echo '<input type="hidden" name="rfqloaded[]" value="' . $value['rfqn_id'] . '"/>';
            if ($value['rfqn_added_by'] == $tmpId) { // incoming msg
                 ?>
                 <div class="outgoing_msg chat_msg_box" id="msg<?php echo $value['rfqn_id'];?>">
                      <div class="outgoing_msg_img"> 
                           <div class="chat_img"> 
                                <?php
                                echo img(array('src' => FILE_UPLOAD_PATH . '/avatar/' . $value['buy_usr_avatar'],
                                    'class' => 'img-circle'));
                                ?>
                           </div>
                      </div>
                      <div class="sent_msg">
                           <p><?php echo $value['rfqn_comments'];?></p>
                           <span class="time_date txt-align-rt"> 
                                <?php echo date('h:i A', strtotime($value['rfqn_added_on']));?> | 
                                <?php echo date('M d', strtotime($value['rfqn_added_on']));?> | 
                                <strong><?php echo $value['buy_usr_first_name'];?></strong>
                           </span> 
                      </div>
                 </div> 
            <?php } else {  // outgoing msg  ?>
                 
                 <div class="incoming_msg chat_msg_box" id="msg<?php echo $value['rfqn_id'];?>">
                      <div class="incoming_msg_img"> 
                           <div class="chat_img"> 
                                <?php
                                echo img(array('src' => FILE_UPLOAD_PATH . '/avatar/' . $value['buy_usr_avatar'],
                                    'class' => 'img-circle'));
                                ?>
                           </div>
                      </div>
                      <div class="received_msg">
                           <div class="received_withd_msg">
                                <p><?php echo $value['rfqn_comments'];?></p>
                                <span class="time_date">
                                     <strong><?php echo $value['buy_usr_first_name'];?></strong> |
                                     <?php echo date('h:i A', strtotime($value['rfqn_added_on']));?> | 
                                     <?php echo date('M d', strtotime($value['rfqn_added_on']));?> 
                                </span>
                           </div>
                      </div>
                 </div>
                 <?php
            }
       }
  }
?>