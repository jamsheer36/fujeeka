<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Categories</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Icon</th>
                                        <th>Priority</th>
                                        <th>Category</th>
                                        <th>Parent Category</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     if (!empty($categories)) {
                                          foreach ($categories as $key => $value) {
                                               ?>
                                               <tr data-url="<?php echo site_url('category/view/' . $value['cat_id']);?>">
                                                    <td class="trVOE">
                                                         <?php echo img(array('src' => FILE_UPLOAD_PATH . 'category/' . $value['cat_image'], 'width' => '30'));?>
                                                    </td>
                                                    <td class="trVOE"><?php echo $value['cat_order'];?></td>
                                                    <td class="trVOE"><?php echo $value['category_name'];?></td>
                                                    <td class="trVOE"><?php
                                                         $root = ($value['root_category']) ? $value['root_category'] . ' >> ' : '';
                                                         echo $root . $value['parent_category'];
                                                         ?>
                                                    </td>
                                                    <?php if (check_permission($controller, 'delete')) {?>
                                                         <td>
                                                              <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                                 data-url="<?php echo site_url('category/delete/' . $value['cat_id']);?>">
                                                                   <i class="fa fa-remove"></i>
                                                              </a>
                                                         </td>
                                                    <?php }?>
                                               </tr>
                                               <?php
                                          }
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>