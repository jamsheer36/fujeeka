<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class category extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->page_title = 'Category';
            $this->load->library('form_validation');
            $this->load->model('category_model', 'category');
            if($this->router->fetch_method()!='getSubCategories'){
               $this->lock_in();
            }
       }

       public function index() {
            $categories['categories'] = $this->category->getCategories();
            $this->render_page(strtolower(__CLASS__) . '/list', $categories);
       }

       public function add() {
            $categories['order'] = $this->category->getNextOrder();
            $this->render_page(strtolower(__CLASS__) . '/add', $categories);
       }

       public function insert() {
            /**/
            $data = array();
            if (isset($_FILES['cat_image']['name']) && !empty($_FILES['cat_image']['name'])) {
                 /* Category image */
                 $newFileName = rand(9999999, 0) . $_FILES['cat_image']['name'];
                 $config['upload_path'] = FILE_UPLOAD_PATH . 'category/';
                 $config['allowed_types'] = 'gif|jpg|png|jpeg';
                 $config['file_name'] = $newFileName;
                 $this->load->library('upload', $config);

                 if (!$this->upload->do_upload('cat_image')) {
                      array('error' => $this->upload->display_errors());
                 } else {
                      $data = array('upload_data' => $this->upload->data());
                      crop($this->upload->data(), $this->input->post());
                 }
                 $_POST['category']['cat_image'] = isset($data['upload_data']['file_name']) ? $data['upload_data']['file_name'] : '';
            }

            if (isset($_FILES['cat_page_banner']['name']) && !empty($_FILES['cat_page_banner']['name'])) {
                 /* Category banner */
                 $config = array();
                 $config['upload_path'] = FILE_UPLOAD_PATH . 'category/';
                 $config['allowed_types'] = 'gif|jpg|png|jpeg';
                 $config['file_name'] = rand(9999999, 0) . $_FILES['cat_page_banner']['name'];

                 $this->load->library('upload');
                 $this->upload->initialize($config);

                 $pixel['x1'][0] = $_POST['x1'][1];
                 $pixel['y1'][0] = $_POST['y1'][1];
                 $pixel['x2'][0] = $_POST['x2'][1];
                 $pixel['y2'][0] = $_POST['y2'][1];
                 $pixel['w'][0] = $_POST['w'][1];
                 $pixel['h'][0] = $_POST['h'][1];

                 if (!$this->upload->do_upload('cat_page_banner')) {
                      array('error' => $this->upload->display_errors());
                 } else {
                      $uploadData = $this->upload->data();
                      crop($this->upload->data(), $pixel);
                      $_POST['category']['cat_page_banner'] = isset($uploadData['file_name']) ? $uploadData['file_name'] : '';
                 }
            }
            if ($this->category->addNewCategory($_POST['category'])) {
                 $this->session->set_flashdata('app_success', 'Category successfully added!');
            } else {
                 $this->session->set_flashdata('app_error', "Can't add category!");
            }
            redirect(strtolower(__CLASS__));
       }

       public function update() {
            /**/
            $data = array();
            if (isset($_FILES['cat_image']['name']) && !empty($_FILES['cat_image']['name'])) {
                 $newFileName = rand(9999999, 0) . $_FILES['cat_image']['name'];
                 $config['upload_path'] = FILE_UPLOAD_PATH . 'category/';
                 $config['allowed_types'] = 'gif|jpg|png|jpeg';
                 $config['file_name'] = $newFileName;
                 $this->load->library('upload', $config);

                 if (!$this->upload->do_upload('cat_image')) {
                      $data = array('error' => $this->upload->display_errors());
                 } else {
                      $data = array('upload_data' => $this->upload->data());
                      crop($this->upload->data(), $this->input->post());
                 }
                 /**/
                 $_POST['category']['cat_image'] = isset($data['upload_data']['file_name']) ? $data['upload_data']['file_name'] : '';
            }
            if (isset($_FILES['cat_page_banner']['name']) && !empty($_FILES['cat_page_banner']['name'])) {
                 /* Category banner */
                 $config = array();
                 $config['upload_path'] = FILE_UPLOAD_PATH . 'category/';
                 $config['allowed_types'] = 'gif|jpg|png|jpeg';
                 $config['file_name'] = rand(9999999, 0) . $_FILES['cat_page_banner']['name'];
                 $this->load->library('upload');
                 $this->upload->initialize($config);
                 $pixel['x1'][0] = $_POST['x1'][1];
                 $pixel['y1'][0] = $_POST['y1'][1];
                 $pixel['x2'][0] = $_POST['x2'][1];
                 $pixel['y2'][0] = $_POST['y2'][1];
                 $pixel['w'][0] = $_POST['w'][1];
                 $pixel['h'][0] = $_POST['h'][1];

                 if (!$this->upload->do_upload('cat_page_banner')) {
                      array('error' => $this->upload->display_errors());
                 } else {
                      $uploadData = $this->upload->data();
                      crop($this->upload->data(), $pixel);
                      $_POST['category']['cat_page_banner'] = isset($uploadData['file_name']) ? $uploadData['file_name'] : '';
                 }
            }
            if ($this->category->updateCategory($_POST['category'])) {
                 $this->session->set_flashdata('app_success', 'Category successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', "Can't update category!");
            }
            redirect(strtolower(__CLASS__));
       }

       public function delete($id) {
            if($this->category->isProductUnderThisCategory($id)) {
                 echo json_encode(array('status' => 'fail', 'msg' => "You can't delete this category,there are products under this category"));die();
            }
            if ($this->category->deleteCategory($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Category successfully deleted'));die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete category"));die();
            }
       }

       public function view($id) {
            $categories['order'] = $this->category->getNextOrder(true);
            $categories['categories'] = $this->category->getCategories($id);
            $this->render_page(strtolower(__CLASS__) . '/view', $categories);
       }

       public function removeImage($id) {
            if ($this->category->removeCategoryImage($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Category image successfully deleted'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete category image"));
            }
       }

       function removeBanner($id) {
            if ($this->category->removeBannerImage($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Category banner successfully deleted'));
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete category logo"));
            }
       }
       function checkCategoryExists() {
            $categoryExists = $this->category->checkCategoryExists($_POST);
            if (empty($categoryExists['repeting'])) {
                 echo json_encode(array('status' => 'success', 'msg' => '', 'suggest' => $categoryExists['suggestion']));die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Category already exists", 'suggest' => $categoryExists['suggestion']));die();
            }
       }
       function getSubCategories($parent){
            $categories = $this->common_model->getAllCategories($parent);
            $output = '';
            if($categories){
              $output.='<select class="form-control cate" id="sell2" name="rfq_sub_category"><option value="">Sub categories</option>';
              foreach($categories as $cate){
                $output.='<option value='.$cate['cat_id'].'>'.$cate['cat_title'].'</option>';
              }
              $output.='</select>';
            }
            $return = array(
              'data'   => $output
            );
             echo json_encode($return);die();

       }

  }
  