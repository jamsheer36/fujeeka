<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class supplier_api_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tbl_users = TABLE_PREFIX . 'users';
        $this->tbl_products = TABLE_PREFIX . 'products';
        $this->tbl_products_order = TABLE_PREFIX . 'products_order_master';
        $this->tbl_rfq_notification = TABLE_PREFIX . 'rfq_notification';
        $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
        $this->tbl_supplier_followers = TABLE_PREFIX . 'supplier_followers';
        $this->tbl_products_images = TABLE_PREFIX . 'products_images';
        $this->tbl_feeds = TABLE_PREFIX . 'feeds';
        $this->tbl_products_categories = TABLE_PREFIX . 'products_categories';
        $this->tbl_category = TABLE_PREFIX . 'category';
        $this->tbl_products_order_status = TABLE_PREFIX . 'products_order_statuses';
        $this->tbl_products_order_master = TABLE_PREFIX . 'products_order_master';
        $this->tbl_countries = TABLE_PREFIX . 'countries';
        $this->tbl_states = TABLE_PREFIX . 'states';
        $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
        $this->tbl_products_order_comments = TABLE_PREFIX . 'products_order_comments';
        $this->tbl_chats = TABLE_PREFIX . 'chats';
        $this->tbl_mailbox_master = TABLE_PREFIX . 'mailboox_master';
        $this->tbl_groups = TABLE_PREFIX . 'groups';
        $this->tbl_rfq = TABLE_PREFIX . 'rfq';
        $this->tbl_products_units = TABLE_PREFIX . 'products_units';
        $this->tbl_supplier_buildings_cate_assoc = TABLE_PREFIX . 'supplier_buildings_cate_assoc';
        $this->tbl_products_keyword = TABLE_PREFIX . 'products_keyword';
        $this->tbl_pushnotification_master = TABLE_PREFIX . 'pushnotification_master';
        $this->tbl_pushnotification_receiver = TABLE_PREFIX . 'pushnotification_receiver';
        $this->tbl_products_specification = TABLE_PREFIX . 'products_specification';
        $this->tbl_favorite_list = TABLE_PREFIX . 'favorite_list';
        $this->product = TABLE_PREFIX . 'products';
        $this->feed = TABLE_PREFIX . 'feeds';
        $this->tbl_mailboox_attachments = TABLE_PREFIX . 'mailboox_attachments';
        // $this->tbl_supplier_images = TABLE_PREFIX . 'supplier_shop_images';
        // $this->tbl_supplier_buildings_cate_assoc = TABLE_PREFIX . 'supplier_buildings_cate_assoc';
        // $this->tbl_supplier_shop_images = TABLE_PREFIX . 'supplier_shop_images';
        // $this->tbl_banner = TABLE_PREFIX . 'banner';
        // $this->tbl_advertisement = TABLE_PREFIX . 'advertisement';
        // $this->tbl_market = TABLE_PREFIX . 'market_places';
        // $this->tbl_feeds = TABLE_PREFIX . 'feeds';
        // $this->tbl_rfq_products_send = TABLE_PREFIX . 'rfq_products_send';
        // $this->tbl_buildins = TABLE_PREFIX . 'buildins';
        // $this->notification = TABLE_PREFIX . 'pushnotification_master';
        // $this->order_comments = TABLE_PREFIX . 'products_order_comments';
        // $this->logistics_types = TABLE_PREFIX . 'logistics_types';
        // $this->tbl_logistics_comp_images = TABLE_PREFIX . 'logistics_comp_images';
        // $this->tbl_logistics_services_images = TABLE_PREFIX . 'logistics_services_images';
        // $this->qc_services_images = TABLE_PREFIX . 'qc_services_images';
        // $this->tbl_logistics_services = TABLE_PREFIX . 'logistics_services';
        // $this->users_logistics = TABLE_PREFIX . 'users_logistics';
        // $this->logistics_quotes = TABLE_PREFIX . 'logistics_quotes';
        // $this->qc_quotes = TABLE_PREFIX . 'qc_quotes';
        // $this->logistics_quotes_types = TABLE_PREFIX . 'logistics_quotes_types';
        // $this->tbl_qc_images = TABLE_PREFIX . 'qc_images';
        // $this->qc_services = TABLE_PREFIX . 'qc_services';
    }

    public function getUserDetailsFromToken($token)
    {
        return $this->db->select('usr_supplier,usr_company,usr_username, usr_first_name,usr_last_name,usr_id, usr_active,usr_avatar,usr_email,usr_is_mail_verified')
            ->where('usr_token', $token)->get($this->tbl_users)->row_array();
    }

    public function getUsergroupFromId($uid)
    {

        return $this->db->select('cpnl_groups.grp_slug')->from('cpnl_groups')->join('cpnl_users_groups','cpnl_users_groups.group_id = cpnl_groups.id')->where('cpnl_users_groups.user_id',$uid)->get()->row()->grp_slug;

        return $this->db->select('usr_supplier,usr_company,usr_username, usr_first_name,usr_last_name,usr_id, usr_active,usr_avatar,usr_email,usr_is_mail_verified')
            ->where('usr_token', $token)->get($this->tbl_users)->row_array();
    }
    


    public function updatePushyToken($pushy_token, $col, $val)
    {
        if ($pushy_token) {
            return $this->db->where($col, $val)->update($this->tbl_users, array('usr_pushy_token' => $pushy_token));
        }

    }
    public function home($sup_id, $usr_id)
    {

        //total product
        $data['product_count'] = $this->db->where('prd_supplier', $sup_id)->count_all_results($this->tbl_products);
        $data['inquiry_count'] = $this->db->where('ord_supplier', $sup_id)->count_all_results($this->tbl_products_order);
        $data['rfq_count'] = $this->db->select('count(distinct(rfqn_rfq_id)) as cnt')->where('rfqn_supplier', $usr_id)->get($this->tbl_rfq_notification)->row()->cnt;
        $data['active_staff'] = $this->db->join($this->tbl_users_groups, 'user_id=usr_id')->where('group_id', 3)->where('usr_supplier', $sup_id)->where('usr_active', 1)->count_all_results($this->tbl_users);
        

        $end_date = date('Y-m-d');
        $date = date('Y-m-d', strtotime('-7 day', strtotime($end_date)));
        $order = array();
        $dates = array();
        while (strtotime($date) <= strtotime($end_date)) {
            // $datesFullFormat[] = date('d-m-Y', strtotime($date));
            $dates[] = "'" . date('d-m', strtotime($date)) . "'";
            $where = "AND ord_supplier = $sup_id";
            $order[] = $this->db->query("SELECT COUNT(*) AS order_count FROM " . $this->tbl_products_order . " WHERE DATE(ord_added_on) = '$date' " . $where)->row()->order_count;
            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        }
        // $data['enquiry']['datesFullFormat'] = $datesFullFormat;
        $data['enquiry']['between'] =  $date  . ' - ' . $end_date;
        $data['enquiry']['dates'] = implode(',', $dates);
        $data['enquiry']['order'] = implode(',', $order);
        $data['followers'] = $this->getFollowers($sup_id);
        return $data;
    }
    public function getFollowers($sup_id, $limit=4, $offset=0)
    {
        $followers = $this->db->select('usr_first_name,usr_last_name,usr_avatar')
            ->join($this->tbl_users, 'usr_id=sfol_followed_by')
            ->where('sfol_supplier', $sup_id)
            ->order_by('sfol_added_on', 'desc')
            ->limit($limit, $offset)
            ->get($this->tbl_supplier_followers)->result_array();
        $follow = array();
        foreach ($followers as $key => $val) {
            $follow[$key]['name'] = $val['usr_first_name'] . ' ' . $val['usr_last_name'];
            $follow[$key]['image'] = $val['usr_avatar'] ? site_url() . 'assets/uploads/avatar/' . $val['usr_avatar'] : '';
        }
        return $follow;
    }
    public function getProductCounts($sup_id)
    {
        $data['total'] = $this->db->where('prd_supplier', $sup_id)->count_all_results($this->tbl_products);
        $data['active'] = $this->db->where('prd_supplier', $sup_id)->where('prd_status', 1)->count_all_results($this->tbl_products);
        $data['inactive'] = $this->db->where('prd_supplier', $sup_id)->where('prd_status', 0)->count_all_results($this->tbl_products);
        return $data;
    }
    public function getSupProducts($sup_id, $type, $offset, $limit,$search)
    {
        $prd = array();
        $this->db->select('prd_id,prd_added_by,prd_status,prd_name_en,prd_default_image,usr_first_name,usr_last_name')
            ->join($this->tbl_users, 'usr_id=prd_added_by', 'LEFT')
            ->where('prd_supplier', $sup_id);
            
        if ($type == 'active')
            $this->db->where('prd_status', 1);
        
        if ($type == 'inactive') 
            $this->db->where('prd_status', 0);
        
        if($search)
            $this->db->where("prd_name_en LIKE '%$search%'");
        
        $this->db->limit($limit, $offset);
        $products = $this->db->order_by('prd_added_on', 'desc')
            ->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            if ($val['prd_default_image']) {
                $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
            $prd[$key]['prd_id'] = encryptor($val['prd_id']);
            $prd[$key]['name'] = $val['prd_name_en'];
            $prd[$key]['added_by'] = $val['prd_added_by'] == 1 ? "Admin" : $val['usr_first_name'] . ' ' . $val['usr_last_name'];
            $prd[$key]['status'] = $val['prd_status']==1 ? 'Active' : 'Inactive';
            $prd[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
        }
        return $prd;
    }
    public function getSearchProducts($sup_id, $query, $offset, $limit)
    {
        $search = array();
        $this->db->select('*');

        $this->db->where('prd_status', 1);
        $this->db->where('prd_supplier', $sup_id);
        // $this->db->like('prd_name_en', $query, 'after');
        $this->db->like('prd_name_en', $query);
        $this->db->order_by('prd_added_on', 'desc');
        $this->db->limit($limit, $offset);

        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            $search[$key]['prd_id'] = encryptor($val['prd_id']);
            $search[$key]['name'] = $val['prd_name_en'];
        }

        return $search;
    }
    public function addFeed($sup_id, $prd_id, $text)
    {
        $feed['f_sup_id'] = $sup_id;
        $feed['f_prd_id'] = encryptor($prd_id, 'D');
        $feed['f_text'] = $text;
        $this->db->insert($this->tbl_feeds, $feed);
         return $this->db->insert_id();
    }
    public function getFeeds($sup_id, $offset, $limit ,$key=null)
    {
        $feed = array();
        $this->db->select('f_id,f_text,f_prd_id,prd_id,prd_name_en,prd_price_min
        ,prd_moq,prd_default_image')
            ->join($this->tbl_products, 'prd_id = f_prd_id', 'LEFT')
            ->where('f_status', 1)
            ->where('f_sup_id', $sup_id)
            ->order_by('f_added_on', 'desc');
if($key)
   $this->db->where("(prd_name_en like '%$key%') OR ('f_text' like '%$key%') "); 

        $res = $this->db->limit($limit, $offset)
            ->get($this->tbl_feeds)->result_array();

        foreach ($res as $key => $val) {
            $this->db->select($this->tbl_category . '.cat_title');
            $this->db->join($this->tbl_category, 'cat_id = pcat_category');
            $this->db->where('pcat_product', $val['f_prd_id']);
            $catg = $this->db->get($this->tbl_products_categories)->result_array();
            $cat = implode(', ', array_column($catg, 'cat_title'));
            if (isset($val['prd_default_image']) && !empty($val['prd_default_image'])) {
                $pimages = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $pimages = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $pdefaultImg = isset($pimages['pimg_image']) ? $pimages['pimg_image'] : '';
            $feed[$key]['feed_id'] = encryptor($val['f_id']);
            $feed[$key]['feed_product_id'] = encryptor($val['f_prd_id']);
            $feed[$key]['feed_text'] = $val['f_text'];
            $feed[$key]['feed_product_name'] = $val['prd_name_en'];
            $feed[$key]['feed_product_category'] = $cat;
            $feed[$key]['feed_prd_image'] = site_url() . 'assets/uploads/product/' . $pdefaultImg;

        }
        return $feed;
    }
    public function updateFeed($feed_id, $prd_id, $text)
    {
        $feed['f_prd_id'] = encryptor($prd_id, 'D');
        $feed['f_text'] = $text;
        return $this->db->where('f_id', encryptor($feed_id, 'D'))->update($this->tbl_feeds, $feed);
    }

    //upto here
    public function getUserByIdentity($identity)
    {
        if (!empty($identity)) {
            $identityColomn = 'usr_email';
            $user = $this->db->get_where($this->tbl_users, array($identityColomn => $identity))->row_array();
            return $user;
        } else {
            return false;
        }
    }



    function getOrders($suplr_id, $ord_id ,$request) {

        if($ord_id)

         $order = $this->db->select($this->tbl_products_order_master.'.ord_number,'.$this->tbl_products_order_master.'.ord_id,'  
           . $this->tbl_products_order_master . '.ord_qty,' . $this->tbl_products . '.prd_id,'. $this->tbl_products_units . '.unt_unit_en as unit,'. $this->tbl_products . '.prd_price_min,'
           . $this->tbl_products . '.prd_price_max,'. $this->tbl_products . '.prd_offer_price_min,'. $this->tbl_products . '.prd_offer_price_max,'
           . $this->tbl_products . '.prd_name_en,'. $this->tbl_products . '.prd_default_image,'.
           'buyer.usr_id AS byr_id, buyer.usr_first_name AS byr_first_name, buyer.usr_last_name AS byr_last_name , buyer.usr_email AS byr_email , buyer.usr_phone AS byr_phone ,'
           . $this->tbl_countries . '.ctr_name AS byr_ctr_name,' . $this->tbl_states . '.stt_name as byr_stt_name')
           ->join($this->tbl_products, $this->tbl_products . '.prd_id = ' . $this->tbl_products_order_master . '.ord_prod_id', 'LEFT')
           ->join($this->tbl_products_order_status, $this->tbl_products_order_status . '.pos_id = ' . $this->tbl_products_order_master . '.ord_status', 'LEFT')
           ->join($this->tbl_users . ' buyer', 'buyer.usr_id = ' . $this->tbl_products_order_master . '.ord_added_by', 'LEFT')
           ->join($this->tbl_states, $this->tbl_states . '.stt_id = buyer.usr_state', 'LEFT')
           ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = buyer.usr_country', 'LEFT')
           ->join($this->tbl_products_units, $this->tbl_products_units . '.unt_id = ' . $this->tbl_products_order_master . '.ord_unit', 'LEFT')
           ->where($this->tbl_products_order_master . '.ord_id  ', $ord_id)
           ->order_by('ord_added_on', 'DESC')->get($this->tbl_products_order_master)->result_array();



    else

      {   
    
          $offset = isset($request['offset'])?$request['offset']:0;
          $limit = isset($request['limit'])?$request['limit']:30;

        $order = $this->db->select($this->tbl_products_order_master.'.ord_number,'.$this->tbl_products_order_master.'.ord_id,'.$this->tbl_products_order_master 
        . '.ord_added_on,'.$this->tbl_products_order_status.'.pos_status,'
        . $this->tbl_products_order_master . '.ord_qty,' . $this->tbl_products . '.prd_id,'. $this->tbl_products_units . '.unt_unit_en as unit,'. $this->tbl_products . '.prd_price_min,'
        . $this->tbl_products . '.prd_price_max,'. $this->tbl_products . '.prd_offer_price_min,'. $this->tbl_products . '.prd_offer_price_max,'
        . $this->tbl_products . '.prd_name_en,'. $this->tbl_products . '.prd_default_image,'.
        'buyer.usr_id AS byr_id, buyer.usr_first_name AS byr_first_name, buyer.usr_last_name AS byr_last_name , buyer.usr_email AS byr_email , buyer.usr_phone AS byr_phone ,'
        . $this->tbl_countries . '.ctr_name AS byr_ctr_name,' . $this->tbl_states . '.stt_name as byr_stt_name')
        ->join($this->tbl_products, $this->tbl_products . '.prd_id = ' . $this->tbl_products_order_master . '.ord_prod_id', 'LEFT')
        ->join($this->tbl_products_order_status, $this->tbl_products_order_status . '.pos_id = ' . $this->tbl_products_order_master . '.ord_status', 'LEFT')
        ->join($this->tbl_users . ' buyer', 'buyer.usr_id = ' . $this->tbl_products_order_master . '.ord_added_by', 'LEFT')
        ->join($this->tbl_states, $this->tbl_states . '.stt_id = buyer.usr_state', 'LEFT')
        ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = buyer.usr_country', 'LEFT')
        ->join($this->tbl_products_units, $this->tbl_products_units . '.unt_id = ' . $this->tbl_products_order_master . '.ord_unit', 'LEFT')

        ->where($this->tbl_products_order_master . '.ord_supplier', $suplr_id) 
        ->order_by('ord_added_on', 'DESC')->limit($limit, $offset)
        ->get($this->tbl_products_order_master)->result_array();
       
      }
        foreach ($order as $key => $val) {
            if (isset($val['prd_default_image'])) 
                $image = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row()->pimg_image;
            else if(isset($val['prd_id']))
                $image = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row()->pimg_image;
            else
                $image = null;

                $order[$key]['prd_default_image'] = site_url() . 'assets/uploads/product/' . $image;

                $this->db->where('ord_id', $val['ord_id'])
                ->update($this->tbl_products_order_master, array('ord_is_notified'=>1));

        }

       return $order;

                }


                function getInquiryComments($ord_id,$request) 
              {

                $offset = isset($request['offset'])?$request['offset']:0;
                $limit = isset($request['limit'])?$request['limit']:30;
       

       
                    $comments =  $this->db->select($this->tbl_products_order_comments .'.poc_id,'.$this->tbl_products_order_comments .
                    '.poc_from,'.$this->tbl_products_order_comments .'.poc_to,'.$this->tbl_products_order_comments .'.poc_title,'.$this->tbl_products_order_comments .'.poc_desc,'
                    .$this->tbl_products_order_comments .'.poc_added_on, c_from.usr_first_name AS frm_usr_first_name, c_from.usr_last_name AS frm_usr_last_name, c_from.usr_avatar AS frm_usr_avatar')
                           ->join($this->tbl_users . ' c_from', 'c_from.usr_id = ' . $this->tbl_products_order_comments . '.poc_from', 'LEFT')
                                      ->join($this->tbl_users . ' c_to', 'c_to.usr_id = ' . $this->tbl_products_order_comments . '.poc_to', 'LEFT')
                                      ->order_by($this->tbl_products_order_comments . '.poc_added_on', 'ASC')
                                      ->where(array($this->tbl_products_order_comments . '.poc_order_id' => $ord_id))
                           ->limit($limit,$offset)
                           ->get($this->tbl_products_order_comments)->result_array();
                            foreach ($comments as $key => $val) 
                                     $comments[$key]['frm_usr_avatar'] = site_url() . 'assets/uploads/avatar/' . $val['frm_usr_avatar'];
               return $comments ;
              }

              function sendMessage($datas) 
              {

                   return  $this->db->insert($this->tbl_products_order_comments, $datas);

              }


              public function getRfq($suplier,$request) {

                     $rfq_id = isset($request['rfq_id'])?$request['rfq_id']:0;

                     $sup_category = $this->db->select('sbca_category')->where('sbca_supplier', $suplier)->get(TABLE_PREFIX . 'supplier_buildings_cate_assoc')->result_array();
                     $categories = array_column($sup_category, 'sbca_category');
                     $this->db->where_in('rfq_category', $categories);
                     $this->db->where('rfq_status', 1);
                
                    $this->db->select(TABLE_PREFIX . 'rfq.rfq_id,'.TABLE_PREFIX . 'rfq.rfq_product_name,'.TABLE_PREFIX . 'rfq.rfq_qty,'.TABLE_PREFIX . 'rfq.rfq_validity,' .TABLE_PREFIX . 'rfq.rfq_attachment,' .TABLE_PREFIX . 'rfq.rfq_requirment,' . TABLE_PREFIX . 'category.cat_title,' . TABLE_PREFIX . 'category.cat_id,'. TABLE_PREFIX . 'users.usr_id as buyr_id,' .
                    TABLE_PREFIX . 'users.usr_email as buyr_email,'. TABLE_PREFIX . 'users.usr_first_name as buyr_first_name ,'.TABLE_PREFIX . 'users.usr_last_name as buyr_last_name ,'. TABLE_PREFIX . 'products.prd_default_image,'. TABLE_PREFIX . 'products.prd_id,'.TABLE_PREFIX .'products.prd_name_en')
                         ->join(TABLE_PREFIX . 'category', TABLE_PREFIX . 'category.cat_id =' . TABLE_PREFIX . 'rfq.rfq_category', 'left')
                         ->join(TABLE_PREFIX . 'users', TABLE_PREFIX . 'users.usr_id =' . TABLE_PREFIX . 'rfq.rfq_user', 'left')
                         ->join(TABLE_PREFIX . 'products', TABLE_PREFIX . 'products.prd_id =' . TABLE_PREFIX . 'rfq.rfq_product', 'left');
   
       if($rfq_id)
                $rfq = $this->db->where('rfq_id',$rfq_id)->get(TABLE_PREFIX . 'rfq')->result_array();
             
                else
                {
                 
                    $offset = isset($request['offset'])?$request['offset']:0;
                    $limit = isset($request['limit'])?$request['limit']:30;
                    $rfq = $this->db->order_by('rfq_added_on', 'DESC')->limit($limit, $offset)
                               ->get(TABLE_PREFIX . 'rfq')->result_array();
                 }
             
              
      
                    foreach ($rfq as $key => $val) {
                        if (isset($val['prd_default_image'])) 
                            $image = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row()->pimg_image;
                        else if(isset($val['prd_id']))
                            $image = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row()->pimg_image;
                        
                            $rfq[$key]['prd_default_image'] = isset($image)?site_url() . 'assets/uploads/product/' . $image:null;
                            $rfq[$key]['rfq_attachment']  = (isset($val['rfq_attachment']) && $val['rfq_attachment'] !='')?site_url() . 'assets/uploads/rfq/' . $val['rfq_attachment']:null;
                         
                          if (empty($val['prd_name_en']))
                             $rfq[$key]['prd_name_en'] = $val['rfq_product_name'];
                   
                        }
                                       if($rfq_id)
                                        return $rfq;

                      $return['list'] = $rfq;
                      $return['totlal'] = $this->db->where_in('rfq_category', $categories)->where('rfq_status', 1)->get(TABLE_PREFIX . 'rfq')->num_rows();
                    return $return;
                      
                
           }

 
           public function getRfqComments($suplier,$rfq_id,$request)
           {

              $this->db->where('rfq_id', $rfq_id);
                 $rfqSingle = $this->db->get(TABLE_PREFIX . 'rfq')->row_array();
                   $usr = $rfqSingle['rfq_user'];
                $where = "rfqn_added_by =    $usr and rfqn_supplier = $suplier or(rfqn_added_by = $suplier and rfqn_supplier = $usr)";
            
                               /*  ->where($this->tbl_rfq_notification . '.rfqn_rfq_id', $rfq_id)->where($this->tbl_rfq_notification . '.rfqn_supplier', $suplier)
                                 ->or_where($this->tbl_rfq_notification . '.rfqn_added_by', $suplier)*/
                 $offset = isset($request['offset'])?$request['offset']:0;
                 $limit = isset($request['limit'])?$request['limit']:30;
                 $rfqcomments = $this->db->select($this->tbl_rfq_notification . '.rfqn_id,'.$this->tbl_rfq_notification . '.rfqn_rfq_id,'.$this->tbl_rfq_notification . '.rfqn_supplier,'.$this->tbl_rfq_notification . '.rfqn_added_by,'.$this->tbl_rfq_notification . '.rfqn_comments,'.$this->tbl_rfq_notification . '.rfqn_added_on,'.$this->tbl_rfq_notification . '.rfqn_read_on')
                           ->where($where)
                           ->order_by($this->tbl_rfq_notification . '.rfqn_added_on', 'DESC')
                                 ->limit($limit,$offset)
                                 ->get($this->tbl_rfq_notification)->result_array();
                
                 foreach ($rfqcomments as $key => $val) {
                  
                    if (empty($val['rfqn_read_on'])) {
                        $this->db->where('rfqn_id', $val['rfqn_id']);
                        $this->db->update($this->tbl_rfq_notification, array('rfqn_read_on' => date('Y-m-d h:i:s')));
                   }

                   $text=str_ireplace('<p>','',$val['rfqn_comments']);
                   $rfqcomments[$key]['rfqn_comments']=str_ireplace('</p>','',$text);    

                    if ($val['rfqn_added_by'] == $suplier)
                        $rfqcomments[$key]['type'] = 'sent';
                    else
                    $rfqcomments[$key]['type'] = 'recieved';
                }

            return $rfqcomments;
           
           }

           public function getUnread($usr_id,$sup_id)
           {

            $sup_category = $this->db->select('sbca_category')->where('sbca_supplier', $sup_id)->get(TABLE_PREFIX . 'supplier_buildings_cate_assoc')->result_array();
            $notfication['unread_rfq'] = 0;
            $notfication['unread_inquiry'] = $this->db->get_where($this->tbl_products_order, array('ord_supplier' => $sup_id, 'ord_is_notified' => 0))->num_rows();
            $notfication['unread_chat'] = $this->db->get_where($this->tbl_chats, array('c_to_id' => $usr_id, 'c_is_read' => 0))->num_rows();
            $notfication['unread_messages'] = $this->db->get_where($this->tbl_mailbox_master, array('mms_to' => $usr_id, 'mms_last_viewd_on' => NULL, 'mms_status' => 1))->num_rows();
            /*  $notfication['unread_rfq_chat'] = $this->db->get_where($this->tbl_rfq_notification, array('rfqn_supplier' => $usr_id, 'rfqn_read_on' => NULL))->num_rows();
            $notfication['unread_rfq'] = $this->db->where_in('rfq_category', array_column($sup_category, 'sbca_category'))->where('rfq_status', 1)->get(TABLE_PREFIX . 'rfq')->num_rows();*/
            return $notfication;

        }

        function sendmailMessage($send) {
           
                 $send['mms_parent'] = 0;
                 $this->db->insert($this->tbl_mailbox_master, $send);
                 $mailId = $this->db->insert_id();

                 generate_log(array(
                     'log_title' => 'Mail Compose',
                     'log_desc' => $send['mms_from'] . '-' . $send['mms_to'] ,
                     'log_controller' => 'mailbox',
                     'log_action' => 'C',
                     'log_ref_id' => $mailId,
                     'log_added_by' => $send['mms_from']
                 ));
                 return $mailId;
            }
                function addAttachments($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_mailboox_attachments, $data);
                 return true;
            }
            return false;
       }
         function searchRecipient($sqry,$usr_id) {
                     if (!empty($sqry)) {
                      $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
                 }
                 $this->db->where($this->tbl_users . '.usr_supplier', $this->suplr);
                 $this->db->where($this->tbl_users . '.usr_id !=', 1);
                 $this->db->where($this->tbl_users . '.usr_id !=', $this->uid);

                 $staff = $this->db->select($this->tbl_users . '.usr_id, ' . $this->tbl_users . '.usr_email,'. $this->tbl_users . '.usr_first_name,'. $this->tbl_users . '.usr_last_name')
                                 ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                                 ->where(array($this->tbl_groups . '.id' => 3, $this->tbl_users . '.usr_active' => 1,
                                     $this->tbl_users . '.usr_id !=' => $this->uid))->get($this->tbl_users)->result_array();

                 //rfq
                 if (!empty($sqry)) {
                      $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
                 }
                 $rfq = $this->db->select($this->tbl_users . '.usr_id, ' . $this->tbl_users . '.usr_email,'. $this->tbl_users . '.usr_first_name,'. $this->tbl_users . '.usr_last_name')
                                 ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_rfq . '.rfq_user', 'LEFT')
                                 ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->where($this->tbl_rfq . '.rfq_supplier', $this->uid)->or_where($this->tbl_rfq . '.rfq_supplier = ', 0)
                                 ->where(array($this->tbl_users . '.usr_active' => 1,
                                     $this->tbl_users . '.usr_id !=' => $this->uid))->get($this->tbl_rfq)->result_array();
                                 
                 $rfq = array_unique($rfq, SORT_REGULAR);
                 //inquiry
                 if (!empty($sqry)) {
                      $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
                 }
                 $inquiry = $this->db->select($this->tbl_users . '.usr_id, ' . $this->tbl_users . '.usr_email,'. $this->tbl_users . '.usr_first_name,'. $this->tbl_users . '.usr_last_name')
                                 ->join($this->tbl_products_order_master, $this->tbl_products_order_master . '.ord_buyer = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->where(array($this->tbl_products_order_master . '.ord_supplier' => $this->suplr,
                                     $this->tbl_users . '.usr_active' => 1, $this->tbl_users . '.usr_id !=' => $this->uid))
                                 ->get($this->tbl_users)->result_array();

                 //admin
                 $admin = $this->db->select($this->tbl_users . '.usr_id, ' . $this->tbl_users . '.usr_email,'. $this->tbl_users . '.usr_first_name,'. $this->tbl_users . '.usr_last_name')
                                 ->where($this->tbl_users . '.usr_id', 1)->get($this->tbl_users)->result_array();

                 $inquiry = array_unique($inquiry, SORT_REGULAR);

                 $recepient = array_merge($staff, $rfq, $inquiry, $admin);
                 asort($recepient);
                return 
                array_map("unserialize", array_unique(array_map("serialize", $recepient)));
             }
            function searchRecipient_old( $sqry,$usr_id) {

            if (!empty($sqry)) {
                $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
           }
           $this->db->where($this->tbl_users . '.usr_supplier', $usr_id);
           $this->db->where($this->tbl_users . '.usr_id !=', 1);
           $this->db->where($this->tbl_users . '.usr_id !=', $usr_id);

           $staff = $this->db->select($this->tbl_users . '.usr_id, ' . $this->tbl_users . '.usr_email')
                           ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                           ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                           ->where(array($this->tbl_groups . '.id' => 3, $this->tbl_users . '.usr_active' => 1,
                               $this->tbl_users . '.usr_id !=' =>$usr_id))->get($this->tbl_users)->result_array();

           //rfq
           if (!empty($sqry)) {
                $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
           }
           $rfq = $this->db->select($this->tbl_users . '.usr_id,' . $this->tbl_users . '.usr_email')
                           ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_rfq . '.rfq_user', 'LEFT')
                           ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                           ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                           ->where($this->tbl_rfq . '.rfq_supplier', $usr_id)->or_where($this->tbl_rfq . '.rfq_supplier = ', 0)
                           ->where(array($this->tbl_groups . '.id' => 3, $this->tbl_users . '.usr_active' => 1,
                               $this->tbl_users . '.usr_id !=' => $usr_id))->get($this->tbl_rfq)->result_array();

           $rfq = array_unique($rfq, SORT_REGULAR);

           //inquiry
           if (!empty($sqry)) {
                $this->db->like($this->tbl_users . '.usr_email', $sqry, 'both');
           }
           $inquiry = $this->db->select($this->tbl_users . '.usr_id,' . $this->tbl_users . '.usr_email')
                           ->join($this->tbl_products_order_master, $this->tbl_products_order_master . '.ord_buyer = ' . $this->tbl_users . '.usr_id', 'LEFT')
                           ->where(array($this->tbl_products_order_master . '.ord_supplier' => $usr_id,
                               $this->tbl_users . '.usr_active' => 1, $this->tbl_users . '.usr_id !=' => $usr_id))
                           ->get($this->tbl_users)->result_array();

           //admin
           $admin = $this->db->select($this->tbl_users . '.usr_id,' . $this->tbl_users . '.usr_email')
                           ->where($this->tbl_users . '.usr_id', 1)->get($this->tbl_users)->result_array();

           $inquiry = array_unique($inquiry, SORT_REGULAR);

           $recepient = array_merge($staff, $rfq, $inquiry, $admin);
           asort($recepient);
           return $recepient;

        }
       
        function filterMails($usr_id,$stat,$request) {
  

            $offset = isset($request['offset'])?$request['offset']:0;
            $limit = isset($request['limit'])?$request['limit']:30;
             $this->db->select($this->tbl_mailbox_master . '.*,' . 'frommail.usr_first_name AS frm_usr_first_name , frommail.usr_last_name AS frm_usr_last_name, '
                                    . 'tomail.usr_first_name AS to_usr_first_name,tomail.usr_last_name AS to_usr_last_name')
                            ->join($this->tbl_users . ' frommail', 'frommail.usr_id = ' . $this->tbl_mailbox_master . '.mms_from')
                            ->join($this->tbl_users . ' tomail', 'tomail.usr_id = ' . $this->tbl_mailbox_master . '.mms_to');
                     if($stat == 3 )
                             $this->db->where(array($this->tbl_mailbox_master . '.mms_to' => $usr_id , $this->tbl_mailbox_master . '.mms_status' =>0));
                     if($stat == 1 )
                     $this->db->where(array($this->tbl_mailbox_master . '.mms_to' => $usr_id ,$this->tbl_mailbox_master . '.mms_status' => 1));

                    if($stat == 2 )
                     $this->db->where(array($this->tbl_mailbox_master . '.mms_from' => $usr_id ,$this->tbl_mailbox_master . '.mms_status' => 1));
                     
                     $data= $this->db->order_by('mms_id', 'DESC')->limit($limit,$offset)->get($this->tbl_mailbox_master)->result_array();

foreach ($data as $key => $value) {
   $data[$key]['mms_message'] = strip_tags($value['mms_message']);
    $mail_attachment= $this->db->select('mat_attachment')->where('mat_master_id',$value['mms_id'])->get(TABLE_PREFIX.'mailboox_attachments')->result_array();
     $h = 0;
     $data[$key]['attachments'] = array();
    foreach ($mail_attachment as  $attachment) {
      $data[$key]['attachments'][$h] = site_url()."assets/uploads/mail_attachments/".$attachment['mat_attachment'];
      $h++;
    }

  
   if($stat == 2)
   {
     $data[$key]['frm_usr_first_name']=$value['to_usr_first_name'];
     $data[$key]['frm_usr_last_name']=$value['to_usr_last_name'];
   }
}

 $this->db->where('mms_to', $usr_id)->where('mms_last_viewd_on', NULL)
      ->update($this->tbl_mailbox_master, array('mms_last_viewd_on'=> date('Y-m-d h:i:s')));

         return $data;            
                            
       }

       function deleteMail($id) {
        if (!empty($id)) {
             $this->db->where('mms_id', $id);
             $this->db->update($this->tbl_mailbox_master, array('mms_status' => 0));
             return true;
        }
        return false;
   }

     function getUnits() {
      
        return $this->db->select($this->tbl_products_units.'.unt_id,'.$this->tbl_products_units.'.unt_unit_name_en,'.$this->tbl_products_units.'.unt_unit_en,')->get($this->tbl_products_units)->result_array();

       }


       function getSupplierCategories($supId) {
        return $this->db->select($this->tbl_category . '.cat_id,' . $this->tbl_category . '.cat_title')
                        ->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . $this->tbl_supplier_buildings_cate_assoc . '.sbca_category')
                        ->where($this->tbl_supplier_buildings_cate_assoc . '.sbca_supplier', $supId)
                        ->get($this->tbl_supplier_buildings_cate_assoc)->result_array();
   }

   public function addNewProduct($datas) {

    $datas['prd_number'] = gen_random();
    $datas['prd_status'] = 0;

    $products = $datas;
    unset($products['categories']);
    unset($products['keywords']);
    unset($products['specification_key']);
    unset($products['specification_val']);

    if ($this->db->insert($this->tbl_products, $products)) {
         $lastId = $this->db->insert_id();
         $categories = isset($datas['categories']) ? $datas['categories'] : array();
         foreach ((array) $categories as $key => $value) {
              $cat = array(
                  'pcat_product' => $lastId,
                  'pcat_category' => $value
              );
              $this->db->insert($this->tbl_products_categories, $cat);
         }

         $keywords = isset($datas['keywords']) ? $datas['keywords'] : 0;

         if (!empty($keywords)) {
              $count = isset($datas['keywords']['en']) ? count($datas['keywords']['en']) : 0;
              for ($i = 0; $i <= $count - 1; $i++) {
                   $keys = array(
                       'pkwd_product' => $lastId,
                       'pkwd_val_en' => isset($keywords['en'][$i]) ? clean_text($keywords['en'][$i], 0) : '',
                       'pkwd_val_ar' => isset($keywords['ar'][$i]) ? clean_text($keywords['ar'][$i], 0) : '',
                       'pkwd_val_ch' => isset($keywords['ch'][$i]) ? clean_text($keywords['ch'][$i], 0) : ''
                   );

                   $this->db->insert($this->tbl_products_keyword, $keys);
              }
         }

         $spcEnKey = isset($datas['specification_key']['en']) ? $datas['specification_key']['en'] : 0;
         $spcArKey = isset($datas['specification_key']['ar']) ? $datas['specification_key']['ar'] : 0;
         $spcChKey = isset($datas['specification_key']['ch']) ? $datas['specification_key']['ch'] : 0;
         $spcEnVal = isset($datas['specification_val']['en']) ? $datas['specification_val']['en'] : 0;
         $spcArVal = isset($datas['specification_val']['ar']) ? $datas['specification_val']['ar'] : 0;
         $spcChVal = isset($datas['specification_val']['ch']) ? $datas['specification_val']['ch'] : 0;
         $count = isset($spcEnKey) ? count($spcEnKey) : 0;
         for ($i = 0; $i <= $count - 1; $i++) {
              $specifi = array(
                  'psp_product' => $lastId,
                  'psp_key_en' => clean_text($spcEnKey[$i], 0),
                  'psp_key_ar' => clean_text($spcArKey[$i], 0),
                  'psp_key_ch' => clean_text($spcChKey[$i], 0),
                  'psp_val_en' => clean_text($spcEnVal[$i], 0),
                  'psp_val_ar' => clean_text($spcArVal[$i], 0),
                  'psp_val_ch' => clean_text($spcChVal[$i], 0)
              );
              $this->db->insert($this->tbl_products_specification, $specifi);
         }

         generate_log(array(
             'log_title' => 'Create new record',
             'log_desc' => 'New product added',
             'log_controller' => strtolower(__CLASS__),
             'log_action' => 'C',
             'log_ref_id' => $lastId,
             'log_added_by' => $datas['prd_added_by']
         ));
         return $lastId;
    } else {
         return false;
    }
}


public function getRecentChats($usr_id,$request)
{
    $offset = isset($request['offset'])?$request['offset']:0;
    $limit = isset($request['limit'])?$request['limit']:30;
    $qry = "select usr_id,usr_first_name,usr_last_name,usr_avatar from cpnl_chats join (select user, max(c_time) m from ((select c_to_id user, c_time from cpnl_chats where c_from_id=$usr_id ) union (select c_from_id user, c_time from cpnl_chats where c_to_id=$usr_id) ) cpnl_chats1 group by user) cpnl_chats2 on ((c_from_id=$usr_id and c_to_id=user) or (c_from_id=user and c_to_id=$usr_id)) and (c_time = m) join cpnl_users on usr_id = user order by c_time desc  limit $offset,$limit ";
    $recent_chats = $this->db->query($qry)->result_array();
   
   foreach ($recent_chats as $key => $val) {
      
    $last = $this->db->query("select c_content , c_attachment,c_is_prd_details from cpnl_chats where c_from_id = ".$val['usr_id']." and c_to_id = $usr_id  order by c_time desc limit 1")->result_array();     
    
    $atchmnt = isset($last[0]['c_attachment'])?$last[0]['c_attachment']:null;
    if ($atchmnt)
    {
      if($last[0]['c_is_prd_details'] == 1)
      $lastmsg['c_attachment'] =  site_url() . 'assets/' .$atchmnt;
      else
      $lastmsg['c_attachment'] =  site_url() . 'assets/uploads/chats/' .$atchmnt;
     
    }
    $lastmsg['c_attachment'] = null;
    $lastmsg['c_content'] = isset($last[0]['c_content'])?$last[0]['c_content']:'';

    $recent_chats[$key]['last_message'] = isset($lastmsg)?$lastmsg:null;
    $where = "c_from_id =". $val['usr_id']." and c_to_id = $usr_id  and c_is_read =0";
    $recent_chats[$key]['unread'] =  $this->db->where($where)->get(TABLE_PREFIX . 'chats')->num_rows();
    $recent_chats[$key]['usr_avatar'] = site_url() . 'assets/uploads/avatar/' .$val['usr_avatar'];

      }
//$unread[$key] =
 //array_multisort($unread, SORT_DESC, $recent_chats);

      return  $recent_chats;
    
}


function getallchats($usr_id,$usr_id2,$request)
{
    $offset = isset($request['offset'])?$request['offset']:0;
    $limit = isset($request['limit'])?$request['limit']:30;
    $where = "c_from_id = $usr_id and c_to_id = $usr_id2 or(c_from_id = $usr_id2 and c_to_id = $usr_id)";
    $chats = $this->db->where($where)->order_by('c_time', 'DESC')->limit($limit,$offset)->get(TABLE_PREFIX . 'chats')->result_array();
  
    $this->db->set('c_is_read',1)->where($where)->update(TABLE_PREFIX . 'chats');

$supported_image = array('gif','jpg','jpeg','png');
   foreach ($chats as $key => $val) {
        $atchmnt = isset($val['c_attachment'])?$val['c_attachment']:null;
        $prd_img = isset($val['c_is_prd_details'])?$val['c_is_prd_details']:null;
        if ($atchmnt) 
          {
            if($prd_img == 1)
            $chats[$key]['c_attachment'] =     site_url() . 'assets/' . $val['c_attachment'];
            else {
                   $ext = strtolower(pathinfo($val['c_attachment'], PATHINFO_EXTENSION));  
                        if(in_array($ext, $supported_image)) 
                             $chats[$key]['c_is_prd_details'] = 2;
                         else 
                             $chats[$key]['c_is_prd_details'] =3;
            
     $chats[$key]['c_attachment'] = site_url().'assets/uploads/chats/'.$val['c_attachment'];
          }

          }

          if($usr_id == $val['c_from_id'])
          $chats[$key]['type'] =  'sent';
          else 
          $chats[$key]['type'] =  'received';

          $chats[$key]['c_time'] =  gmdate("H:i:s", $val['c_time'])."|".date('M-d', strtotime($val['c_time']));
          
        }
                return  $chats;
}


public function sendPushNotification($pushdata)
{

    $pushdata['pnm_sent_by'] = $this->uid;
    $this->db->insert($this->tbl_pushnotification_master, $pushdata);
    if($pushId = $this->db->insert_id())
    {
        $favUsers = $this->db->distinct()->select('GROUP_CONCAT(fav_added_by) AS fav_added_by')
        ->get_where($this->tbl_favorite_list, array('fav_consign' => 'SUP', 'fav_consign_id' => $this->sup_id))
        ->row()->fav_added_by;

         $folUsers = $this->db->distinct()->select('GROUP_CONCAT(sfol_followed_by) AS sfol_followed_by')
        ->get_where($this->tbl_supplier_followers, array('sfol_supplier' => $this->sup_id))
        ->row()->sfol_followed_by;

        $merged = array_merge(explode(',', $favUsers), explode(',', $folUsers));

        $recipients = $this->db->select('GROUP_CONCAT(' . $this->tbl_users . '.usr_pushy_token) AS buy_pushy_token')
        ->where_in($this->tbl_users . '.usr_id', $merged)->where($this->tbl_users . '.usr_active', 1)
        ->where($this->tbl_users . ".usr_pushy_token != ''")->get($this->tbl_users)->row()->buy_pushy_token;
          
        if (!empty($recipients)) {
             $recipients = explode(',', $recipients);
             $push['id'] = $pushId;
             $push['msg_type'] = 'general_notfication';
             $push['from'] = $this->uid == 1 ? 'Fujeeka' : $this->u_name;
             $push['title'] = $pushdata['pnm_title'];
             $push['image'] = isset($pushdata['pnm_image']) && !empty($pushdata['pnm_image']) ? site_url() . 'assets/uploads/notification/' . $pushdata['pnm_image'] : '';
             $push['time'] = date("Y-m-d H:i:s");
             $push['chat_id'] = $this->uid == 1 ? 'admin' : encryptor($this->uid);
             
            return $this->common_model->sendPushNotification($push, $recipients, '');
        }
    }
    return false;

}

function getRealatedProduct($categoryId) {
    if (!empty($categoryId)) {
         return $this->db->select($this->tbl_products . '.prd_id ,'.$this->tbl_products . '.prd_name_en')
                         ->join('(select * from ' . $this->tbl_products_categories . ' where pcat_category = ' . $categoryId . ') categories ', 'categories.pcat_product = cpnl_products.prd_id')
                         ->where($this->tbl_products . '.prd_supplier', $this->sup_id)
                         ->get($this->tbl_products)->result_array();
    }
    return null;
}

public function getProducts() {
   $data = $this->db->select('prd_id,prd_name_en,prd_default_image')
                    ->where('prd_supplier', $this->suplr)
                    ->where('prd_status', 1)
                    ->order_by('prd_name_en', 'asc')
                    ->get($this->product)
                    ->result_array();

                    foreach($data as $key=>$val)
                    {
                        $data[$key]['prd_id'] = encryptor($val['prd_id']);

            if ($val['prd_default_image']) {
                $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
            $data[$key]['prd_default_image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
          }
  
                    return $data;
}


public function updateFeed1($data, $id) {
    return $this->db->where('f_id', $id)->update($this->feed, $data);
}


         function sendRFQComments($data) {


            if (isset($data['rfqn_rfq_id']) && isset($data['rfqn_added_by']) &&
                    isset($data['rfqn_supplier']) && isset($data['rfqn_comments'])) {
                 return $this->db->insert($this->tbl_rfq_notification, $data);
            }
            else
              return false;
       }



public function addProduct($datas,$products) {

    $products['prd_number'] = gen_random();
    $products['prd_status'] = 0;
     
    if ($this->db->insert($this->tbl_products, $products)) {
         $lastId = $this->db->insert_id();
         $categories = isset($datas['categoryId']) ? $datas['categoryId'] : array();
         foreach ((array) $categories as  $value) {
              $cat = array(
                  'pcat_product' => $lastId,
                  'pcat_category' => $value
              );
              $this->db->insert($this->tbl_products_categories, $cat);
         }

        $productKeyWord = isset($datas['productKeyWord']) ? $datas['productKeyWord'] : array();
         
         foreach ($productKeyWord as $keywords) {
              $keys = array(
                'pkwd_product' => $lastId,
                'pkwd_val_en' => isset($keywords['en']) ? clean_text($keywords['en'], 0) : '',
                'pkwd_val_ar' => isset($keywords['ar']) ? clean_text($keywords['ar'], 0) : '',
                'pkwd_val_ch' => isset($keywords['ch']) ? clean_text($keywords['ch'], 0) : '');

             $this->db->insert($this->tbl_products_keyword, $keys);
         }

       $productSpecs = isset($datas['productSpecs']) ? $datas['productSpecs'] : array();
         foreach ($productSpecs as $lng => $specArray) {

              $specifications = array(
                'psp_product' => $lastId,
                'psp_key_ch' => isset($specArray['ch']['key'])?clean_text($specArray['ch']['key'], 0):'',
                'psp_val_ch'=> isset($specArray['ch']['val'])?clean_text($specArray['ch']['val'], 0):'',
                'psp_key_en' => isset($specArray['en']['key'])?clean_text($specArray['en']['key'], 0):'',
                'psp_val_en'=> isset($specArray['en']['val'])?clean_text($specArray['en']['val'], 0):'',
                'psp_key_ar' => isset($specArray['ar']['key'])?clean_text($specArray['ar']['key'], 0):'',
                'psp_val_ar'=> isset($specArray['ar']['val'])?clean_text($specArray['ar']['val'], 0):'',
                   );
              $this->db->insert($this->tbl_products_specification, $specifications);
         }

         generate_log(array(
             'log_title' => 'Create new record',
             'log_desc' => 'New product added',
             'log_controller' => strtolower(__CLASS__),
             'log_action' => 'C',
             'log_ref_id' => $lastId,
             'log_added_by' => $products['prd_added_by']
         ));
         return $lastId;
    } else {
         return false;
    }
}

public function updateProduct($datas,$products) {

    /* if (isset($datas['prd_is_stock_prod'])) {
                      $products['prd_is_stock_prod'] = $datas['prd_is_stock_prod'];
                      $products['prd_is_flash_sale'] = isset($datas['prd_is_flash_sale']) ? 1 : 0;
                 } else {
                      $products['prd_is_stock_prod'] = 0;
                      $products['prd_stock_qty'] = 0;
                      $products['prd_is_flash_sale'] = 0;
                 }*/
        $prd_id = $datas['prd_id'];
             
    if ($this->db->where('prd_id',$prd_id)->update($this->tbl_products, $products)) {
       
         $categories = isset($datas['categoryId']) ? $datas['categoryId'] : array();

             if (!empty($categories) && $prd_id) {

       $this->db->where('pcat_product', $prd_id)->delete($this->tbl_products_categories);

         foreach ($categories as  $value) {
              $cat = array(
                  'pcat_product' => $prd_id,
                  'pcat_category' => $value
              );

        $this->db->insert($this->tbl_products_categories, $cat);
        
         }

         }

        $productKeyWord = isset($datas['productKeyWord']) ? $datas['productKeyWord'] : array();
            if (!empty($productKeyWord)) {
        $this->db->where('pkwd_product', $prd_id)->delete($this->tbl_products_keyword);
         foreach ($productKeyWord as $keywords) {
              $keys = array(
                'pkwd_product' => $prd_id,
                'pkwd_val_en' => isset($keywords['en']) ? clean_text($keywords['en'], 0) : '',
                'pkwd_val_ar' => isset($keywords['ar']) ? clean_text($keywords['ar'], 0) : '',
                'pkwd_val_ch' => isset($keywords['ch']) ? clean_text($keywords['ch'], 0) : '');

             $this->db->insert($this->tbl_products_keyword, $keys);
         }
     }

       $productSpecs = isset($datas['productSpecs']) ? $datas['productSpecs'] : array();
        if (!empty($productSpecs)) {
       $this->db->where('psp_product', $prd_id)->delete($this->tbl_products_specification);
         foreach ($productSpecs as $lng => $specArray) {

              $specifications = array(
                'psp_product' => $prd_id,
                'psp_key_ch' => isset($specArray['ch']['key'])?clean_text($specArray['ch']['key'], 0):'',
                'psp_val_ch'=> isset($specArray['ch']['val'])?clean_text($specArray['ch']['val'], 0):'',
                'psp_key_en' => isset($specArray['en']['key'])?clean_text($specArray['en']['key'], 0):'',
                'psp_val_en'=> isset($specArray['en']['val'])?clean_text($specArray['en']['val'], 0):'',
                'psp_key_ar' => isset($specArray['ar']['key'])?clean_text($specArray['ar']['key'], 0):'',
                'psp_val_ar'=> isset($specArray['ar']['val'])?clean_text($specArray['ar']['val'], 0):'',
                   );
              $this->db->insert($this->tbl_products_specification, $specifications);
         }
     }
         return $prd_id;
    } else {
         return false;
    }
}


    function changeOrderStatuse($orderId, $statusId) {

            $update['ord_status'] = $statusId; //New status id
            $update['ord_verified_by'] = $this->uid; // who verify the order
            $update['ord_verified_on'] = date('Y-m-d h:i:s'); // verify date
            // Get old status
            $oldStatus = $this->db->get_where($this->tbl_products_order_master, array('ord_id' => $orderId))->row_array();

            $this->db->where('ord_id', $orderId);
            if ($this->db->update($this->tbl_products_order_master, $update)) {
                 $oldStst = isset($oldStatus['ord_status']) ? $oldStatus['ord_status'] : 0; // old status
                 generate_log(array(
                     'log_title' => 'Order status changed',
                     'log_desc' => 'Order status changed to *' . $oldStst . '-' . $statusId,
                     'log_controller' => 'order-status-changed',
                     'log_action' => 'U',
                     'log_ref_id' => $orderId,
                     'log_added_by' => $this->uid
                 ));

                 return true;
            }
            return false;
       }


       function getImagesCount($prd_id)
       {

       return  $this->db->get_where($this->tbl_products_images, array('pimg_product' => $prd_id))->num_rows();
       }


            function makeMessageSeen($usr_id,$usr_id2)
        {
            
            $where = "c_from_id = $usr_id and c_to_id = $usr_id2 or(c_from_id = $usr_id2 and c_to_id = $usr_id)";
            $this->db->set('c_is_read',1)->where($where)->update(TABLE_PREFIX . 'chats');
            return true;
        }
       

       function saveApiRequest($api_type,$method,$server,$request_data,$files)
       {

        $json['request_type'] = $api_type;
        $json['method'] = $method;
        $json['request_data'] = json_encode($request_data);
        $json['server'] = json_encode($server);
        $json['uploads'] = json_encode($files);

        $this->db->insert('web_service_request', $json);
         return $this->db->insert_id();
       }

       function updateApiRequest($id,$uid)
       {
        $this->db->set('user_id',$uid)->where('id',$id)->update('web_service_request');
       }
}
