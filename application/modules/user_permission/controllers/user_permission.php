<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class user_permission extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->load->model('user_permission_model');
            $this->lock_in();
       }

       function access_denied() {
            $this->render_page(strtolower(__CLASS__) . '/nop');
       }

       public function index() {
            $data['users'] = $this->user_permission_model->getAllUserGroups();
            $data['modules'] = $this->config->item('modules');
            $permittedModules = $this->user_permission_model->getUserPermission($this->uid);
            $data['permittedModules'] = isset($permittedModules['cua_access']) ? unserialize($permittedModules['cua_access']) : array();
            $this->render_page(strtolower(__CLASS__) . '/index', $data);
       }

       function setPermission() {
            $this->user_permission_model->addUserPermission($this->input->post());
            $this->session->set_flashdata('app_success', 'User permission successfully completed!');
            redirect(strtolower(__CLASS__));
       }

       function getPermission() {
            $userpermission = $this->user_permission_model->getUserPermission($this->input->post('userid'));
            $permissionArray['permissions'] = array();
            if (isset($userpermission['cua_access']) && !empty($userpermission['cua_access'])) {
                 $tmp = unserialize($userpermission['cua_access']);
                 if (!empty($tmp) && is_array($tmp)) {
                      foreach ($tmp as $k => $v) {
                           foreach ($v as $l => $w) {
                                array_push($permissionArray['permissions'], $k . '-' . $w);
                           }
                      }
                 }
            }
            echo json_encode($permissionArray);die();
       }

  }
  