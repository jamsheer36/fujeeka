<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class building_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_buildins = TABLE_PREFIX . 'buildins';
            $this->tbl_category = TABLE_PREFIX . 'category';
            $this->tbl_market_places = TABLE_PREFIX . 'market_places';
            $this->tbl_buildings_images = TABLE_PREFIX . 'buildings_images';
            $this->tbl_market_cate_assoc = TABLE_PREFIX . 'market_cate_assoc';
            $this->tbl_buildings_market_categoey_assoc = TABLE_PREFIX . 'buildings_market_categoey_assoc';
            $this->pk = 'bld_id';
       }

       public function getBuilding($id = '') {
            $project = array();

            $this->db->select($this->tbl_buildins . '.*,' . $this->tbl_market_places . '.*')
                    ->join($this->tbl_market_places, $this->tbl_market_places . '.mar_id = ' . $this->tbl_buildins . '.bld_market', 'LEFT');

            if (!empty($id)) {
                 $this->db->where($this->tbl_buildins . '.' . $this->pk, $id);
            }
            $this->db->order_by($this->tbl_buildins . '.bld_order', 'asc');

            $productsArray = $this->db->get($this->tbl_buildins)->result_array();
            if (!empty($productsArray)) {
                 foreach ($productsArray as $key => $value) {
                      $value['projImag'] = $this->db->get_where($this->tbl_buildings_images, array('bimg_building_id' => $value['bld_id']))->result_array();
                      $value['defaultImg'] = $this->db->get_where($this->tbl_buildings_images, array('bimg_id' => $value['bld_default_image']))->row_array();

                      $value['categories'] = $this->db->select($this->tbl_market_cate_assoc . '.*,' . $this->tbl_category . '.*')
                                      ->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . $this->tbl_market_cate_assoc . '.mca_category_id')
                                      ->where($this->tbl_market_cate_assoc . '.mca_market_id', $value['bld_market'])
                                      ->get($this->tbl_market_cate_assoc)->result_array();
                      $project[] = $value;
                 }
            }
            if (!empty($id)) {
                 $project['categoriesSelected'] = explode(',', $this->db->select('GROUP_CONCAT(bcat_category_id) AS bcat_category_id')
                                 ->where('bcat_building_id', $id)->get($this->tbl_buildings_market_categoey_assoc)->row()->bcat_category_id);
            }
            return $project;
       }

       public function addNewBuilding($datas) {
            $building = $datas['project'];
            $categories = isset($datas['categories']) ? $datas['categories'] : array();
            $building['bld_slug'] = slugify($building['bld_title']);
            if ($this->db->insert($this->tbl_buildins, $building)) {
                 $id = $this->db->insert_id();

                 if (!empty($categories)) {
                      foreach ($categories as $key => $value) {
                           $this->db->insert($this->tbl_buildings_market_categoey_assoc, array('bcat_building_id' => $id, 'bcat_category_id' => $value));
                      }
                 }

                 return $id;
            } else {
                 return false;
            }
       }

       public function removeImage($id) {
            if ($id) {
                 $this->db->where('bimg_id', $id);
                 $data = $this->db->get($this->tbl_buildings_images)->row_array();
                 if (isset($data['bimg_image']) && !empty($data['bimg_image'])) {
                      if (file_exists(UPLOAD_PATH . 'building/' . $data['bimg_image'])) {
                           unlink(UPLOAD_PATH . 'building/' . $data['bimg_image']);
                      }
                      if (file_exists(UPLOAD_PATH . 'building/thumb_' . $data['bimg_image'])) {
                           unlink(UPLOAD_PATH . 'building/thumb_' . $data['bimg_image']);
                      }
                      $this->db->where('bimg_id', $id);
                      if ($this->db->delete($this->tbl_buildings_images))
                           return true;
                 }
            }
            return false;
       }

       public function updateBuilding($datas) {
            $building = $datas['project'];
            $categories = $datas['categories'];
            $id = $building[$this->pk];

            foreach ($datas['bimg_alt'] as $bimg_id => $bimg_alt) {
                 $this->db->where('bimg_id', $bimg_id)
                 ->update($this->tbl_buildings_images, array('bimg_alt' => $bimg_alt));
                    }

            unset($building[$this->pk]);
            $datas['bld_slug'] = slugify($datas['bld_title']);
            $this->db->where($this->pk, $id);

            if ($this->db->update($this->tbl_buildins, $building)) {
                 if (!empty($categories)) {
                      $this->db->where('bcat_building_id', $id);
                      $this->db->delete($this->tbl_buildings_market_categoey_assoc);
                      foreach ($categories as $key => $value) {
                           $this->db->insert(
                                   $this->tbl_buildings_market_categoey_assoc, array('bcat_building_id' => $id, 'bcat_category_id' => $value)
                           );
                      }
                 }

                 return true;
            } else {
                 return false;
            }
       }

       public function deleteBuilding($id) {
            $this->db->where('bimg_building_id', $id);
            $data = $this->db->get($this->tbl_buildings_images)->result_array();
            if (!empty($data)) {
                 foreach ($data as $key => $value) {
                      if (file_exists(FILE_UPLOAD_PATH . 'building/' . $value['bimg_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'building/' . $value['bimg_image']);
                      }
                      if (file_exists(FILE_UPLOAD_PATH . 'building/thumb_' . $value['bimg_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'building/thumb_' . $value['bimg_image']);
                      }
                 }
            }
            $this->db->where('bimg_building_id', $id);
            $this->db->delete($this->tbl_buildings_images);

            $this->db->where('bcat_building_id', $id);
            $this->db->delete($this->tbl_buildings_market_categoey_assoc);

            $this->db->where($this->pk, $id);
            if ($this->db->delete($this->tbl_buildins)) {
                 return true;
            } else {
                 return false;
            }
       }

       public function addImages($image) {
            if ($this->db->insert($this->tbl_buildings_images, $image)) {
                 return $this->db->insert_id();
            } else {
                 return false;
            }
       }

       function setDefaultImage($imgId, $projectId) {

            $this->db->where($this->pk, $projectId);
            if ($this->db->update($this->tbl_buildins, array('bld_default_image' => $imgId))) {
                 return true;
            } else {
                 return false;
            }
            exit;
       }

       function getCount() {
            $count = $this->db->select('COUNT(*) AS count')->from($this->tbl_buildins)->get()->row_array();
            return isset($count['count']) ? $count['count'] : 0;
       }

       function setPriority($projId, $newOrder) {
            $bld_on_neworder = $this->db->select('*')->from($this->tbl_buildins)->where('bld_order', $newOrder)->get()->row_array();
            $bld_on_projectid = $this->db->select('*')->from($this->tbl_buildins)->where($this->pk, $projId)->get()->row_array();

            $this->db->where($this->pk, $bld_on_neworder['bld_id']);
            $this->db->update($this->tbl_buildins, array('bld_order' => $bld_on_projectid['bld_order']));

            $this->db->where($this->pk, $projId);
            $this->db->update($this->tbl_buildins, array('bld_order' => $newOrder));
            return true;
       }

       function getNextOrder($max = false) {
            if ($max) {
                 return $this->db->count_all_results($this->tbl_buildins);
            } else {
                 return $this->db->select_max('bld_order')->get($this->tbl_buildins)->row()->bld_order + 1;
            }
       }

       function getBuildingCategories($buildingId) {
            return $this->db->select($this->tbl_buildings_market_categoey_assoc . '.*,' . $this->tbl_category . '.*')
                            ->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . $this->tbl_buildings_market_categoey_assoc . '.bcat_category_id')
                            ->where($this->tbl_buildings_market_categoey_assoc . '.bcat_building_id', $buildingId)
                            ->get($this->tbl_buildings_market_categoey_assoc)->result_array();
       }
       function getMarketBuilding($mid) { 
          return $this->db->select('bld_title AS col_title,bld_id AS col_id')
                          ->where('bld_market', $mid)
                          ->get($this->tbl_buildins)->result_array();
     }

  }
  