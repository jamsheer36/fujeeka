<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class building extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->page_title = 'Building';
            $this->load->model('category/category_model', 'category');
            $this->load->model('market/market_model', 'market');
            $this->load->model('building_model', 'building');
            $this->lock_in();
       }

       public function index() {
            $data['count'] = $this->building->getCount();
            $data['projects'] = $this->building->getBuilding();
            $this->render_page(__CLASS__ . '/list', $data);
       }

       public function add() {
            if (!empty($_POST)) {

                 $this->load->library('upload');
                 ini_set('memory_limit', '-1');
                 if ($id = $this->building->addNewBuilding($this->input->post())) {

                      $x1 = $this->input->post('x1');
                      $fileCount = count($x1);
                      $up = array();
                      for ($j = 0; $j < $fileCount; $j++) {
                           /**/
                           $data = array();
                           $angle = array();
                           $newFileName = rand(9999999, 0) . $_FILES['prd_image']['name'][$j];
                           $config['upload_path'] = FILE_UPLOAD_PATH . 'building/';
                           $config['allowed_types'] = 'gif|jpg|png';
                           $config['file_name'] = $newFileName;
                           $this->upload->initialize($config);

                           $angle['x1']['0'] = $_POST['x1'][$j];
                           $angle['x2']['0'] = $_POST['x2'][$j];
                           $angle['y1']['0'] = $_POST['y1'][$j];
                           $angle['y2']['0'] = $_POST['y2'][$j];
                           $angle['w']['0'] = $_POST['w'][$j];
                           $angle['h']['0'] = $_POST['h'][$j];

                           $_FILES['prd_image_tmp']['name'] = $_FILES['prd_image']['name'][$j];
                           $_FILES['prd_image_tmp']['type'] = $_FILES['prd_image']['type'][$j];
                           $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['prd_image']['tmp_name'][$j];
                           $_FILES['prd_image_tmp']['error'] = $_FILES['prd_image']['error'][$j];
                           $_FILES['prd_image_tmp']['size'] = $_FILES['prd_image']['size'][$j];
                           if (!$this->upload->do_upload('prd_image_tmp')) {
                                $up = array('error' => $this->upload->display_errors());
                           } else {
                                $data = array('upload_data' => $this->upload->data());
                                crop($this->upload->data(), $angle);
                                $imgId = $this->building->addImages(array('bimg_building_id' => $id, 'bimg_image' => $data['upload_data']['file_name'],
                                'bimg_alt' => $_POST['bimg_alt'], 'bimg_order' => $j));
                                if ($j == 0) {
                                     $this->building->setDefaultImage($imgId, $id);
                                }
                           }
                      }
                      $this->session->set_flashdata('app_success', 'Building successfully added!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't add project!");
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $data['categories'] = $this->category->getCategories();
                 $data['marketPlaces'] = $this->market->gerMarketPlaces();
                 $this->render_page(__CLASS__ . '/add', $data);
            }
       }

       public function view($id) {
            $building = $this->building->getBuilding($id);
            $data['categories'] = $this->category->getCategories();
            $data['building'] = isset($building[0]) ? $building[0] : '';
            $data['allDetails'] = $building;
            $data['marketPlaces'] = $this->market->gerMarketPlaces();
            $this->render_page(__CLASS__ . '/view', $data);
       }

       public function update() {
            ini_set('memory_limit', '-1');
            $id = $_POST['project']['bld_id'];
            $this->load->library('upload');
            /**/
            $x1 = $this->input->post('x1');
            $fileCount = count($x1);
            $up = array();
            for ($j = 0; $j < $fileCount; $j++) {
                 /**/
                 $data = array();
                 $angle = array();
                 $newFileName = rand(9999999, 0) . $_FILES['prd_image']['name'][$j];
                 $config['upload_path'] = FILE_UPLOAD_PATH . 'building/';
                 $config['allowed_types'] = 'gif|jpg|png';
                 $config['file_name'] = $newFileName;
                 $this->upload->initialize($config);

                 $angle['x1']['0'] = $_POST['x1'][$j];
                 $angle['x2']['0'] = $_POST['x2'][$j];
                 $angle['y1']['0'] = $_POST['y1'][$j];
                 $angle['y2']['0'] = $_POST['y2'][$j];
                 $angle['w']['0'] = $_POST['w'][$j];
                 $angle['h']['0'] = $_POST['h'][$j];

                 $_FILES['prd_image_tmp']['name'] = $_FILES['prd_image']['name'][$j];
                 $_FILES['prd_image_tmp']['type'] = $_FILES['prd_image']['type'][$j];
                 $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['prd_image']['tmp_name'][$j];
                 $_FILES['prd_image_tmp']['error'] = $_FILES['prd_image']['error'][$j];
                 $_FILES['prd_image_tmp']['size'] = $_FILES['prd_image']['size'][$j];
                 if (!$this->upload->do_upload('prd_image_tmp')) {
                      $up = array('error' => $this->upload->display_errors());
                 } else {
                      $data = array('upload_data' => $this->upload->data());
                      crop($this->upload->data(), $angle);
                      $this->building->addImages(array('bimg_building_id' => $id, 'bimg_image' => $data['upload_data']['file_name'], 'bimg_alt' => $_POST['imagealt'], 
                        'bimg_order' => $j));
                 }
            }

            if ($this->building->updateBuilding($this->input->post())) {
                 $this->session->set_flashdata('app_success', 'Building successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', "Can't update project!");
            }
            redirect(strtolower(__CLASS__));
       }

       public function removeImage($id) {
            if ($this->building->removeImage($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Building image successfully deleted'));die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete project image"));die();
            }
       }

       public function delete($id) {
            if ($this->building->deleteBuilding($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Product successfully deleted'));die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete product"));die();
            }
       }

       function setDefaultImage($imgId, $projectId) {
            if (!empty($imgId) && !empty($projectId)) {
                 if ($this->building->setDefaultImage($imgId, $projectId)) {
                      echo json_encode(array('status' => 'success', 'msg' => 'This image set as default'));die();
                 } else {
                      echo json_encode(array('status' => 'fail', 'msg' => "Can't set this image as default"));die();
                 }
            }
       }

       function getBuildingCategories() {
            $buildingId = isset($_POST['marketID']) ? $_POST['marketID'] : 0;
            if (!empty($buildingId)) {
                 echo json_encode($this->building->getBuildingCategories($buildingId));die();
            }
       }
       function getMarketBuilding() {
          $mid = isset($_POST['id']) ? $_POST['id'] : 0;
          if (!empty($mid)) {
               echo json_encode($this->building->getMarketBuilding($mid));die();
          }
     }

  }
  