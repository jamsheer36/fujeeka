<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>New Building</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("building/add", array('id' => "frmProject", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Building Name</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter name" type="text" style="width: 500px;" class="form-control" 
                                          name="project[bld_title]" id="bld_title" placeholder="Building Name"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select required data-parsley-required-message="Select market" name="project[bld_market]" id="bld_type" class="form-control cmbMarket"
                                           data-url="<?php echo site_url('market/getCaregoryByMarket');?>">
                                        <option value="">Select Market</option>
                                        <?php foreach ((array) $marketPlaces as $key => $value) {?>
                                               <option value="<?php echo $value['mar_id'];?>"><?php echo $value['mar_name'];?></option>
                                          <?php }?>
                                   </select>
                              </div>
                         </div>
                         <div class="divCategories">
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Building Category</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="div-category">
                                             <ul class="li-category">
                                                  <?php foreach ((array) $categories as $key => $value) {?>
                                                         <li>
                                                              <input required data-parsley-errors-container="#cat_err" data-parsley-required-message="Select category" name="categories[]" value="<?php echo $value['cat_id']; ?>" type="checkbox"><span><?php echo $value['cat_title']; ?></span>
                                                         </li>
                                                    <?php }?>
                                             </ul>
                                        </div><span id="cat_err"></span>
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Building Number</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter building number"  type="text" style="width: 500px;" class="form-control" 
                                          name="project[bld_number]" id="bld_number" placeholder="Building Number"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="project[bld_address]" class='editor'></textarea>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="project[bld_description]" class='editor'></textarea>
                              </div>
                         </div>

                         <div class="divImageGroup">
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x10" name="x1[]" />
                                             <input type="hidden" id="y10" name="y1[]" />
                                             <input type="hidden" id="x20" name="x2[]" />
                                             <input type="hidden" id="y20" name="y2[]" />
                                             <input type="hidden" id="w0" name="w[]" />
                                             <input type="hidden" id="h0" name="h[]" />
                                             <input type="file" required data-parsley-required-message="Upload a image" class="form-control" data-parsley-fileextension="jpg,png,gif,jpeg"
                                                    name="prd_image[]" id="image_file0" onchange="fileSelectHandler('0', 595, 467)" />
                                             <img id="preview0" class="preview"/>
                                        </div>
                                        
                                        <span class="help-inline">Approximate image dimension 467(Height) X 595(Width)</span>
                                          <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="bimg_alt" id="bimg_alt" data-parsley-required ="true"data-parsley-required-message="Please enter an alt tag for the image"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                         </div>
                                   </div>
                              </div>
                              <div  id="divMoreProductImages">

                              </div>
                              <!--                              <div class="form-group">
                                                                 <label class="control-label col-md-3 col-sm-3 col-xs-12">More Image</label>
                                                                 <div class="col-md-6 col-sm-6 col-xs-12">
                                                                      <a href="javascript:void(0);" id="btnMoreProductImages" class="btn"><i class="icon-plus"></i></a>
                                                                 </div>
                                                            </div>-->
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>
<script type="text/template" id="temSpecification">
     <div class="form-group grp-specification">
     <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
     <div class="row-fluid">
     <div class="span4">
     <div class="form-group no-margin-bot">
     <div class=" col-md-6 col-sm-6 col-xs-12-row">
     <input type="text" class="input-block-level" placeholder="Technical Details Key" name="specification[spe_specification][]">
     </div>
     </div>
     </div>
     <div class="span4">
     <div class="form-group no-margin-bot">
     <div class=" col-md-6 col-sm-6 col-xs-12-row">
     <input type="text" class="input-block-level" placeholder="Technical Details Value" name="specification[spe_specification_detail][]">
     </div>
     </div>
     </div>
     <div class="span1">
     <div class="form-group no-margin-bot">
     <div class=" col-md-6 col-sm-6 col-xs-12-row">
     <button id="btnRemoveMoreSpecification" class="btn btnRemoveMoreSpecification"><i class="icon-minus"></i></button>
     </div>
     </div>
     </div>
     </div>
     </div>
</script>
<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
</style>