<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Update Building</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("building/update", array('id' => "frmProject", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <input type="hidden" name="project[bld_id]" value="<?php echo $building['bld_id'];?>" />
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Building Name</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter name" type="text" style="width: 500px;" class="form-control" 
                                          value="<?php echo $building['bld_title'];?>"
                                          name="project[bld_title]" id="bld_title" placeholder="Building Name"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Market</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select required data-parsley-required-message="Select market" name="project[bld_market]" id="bld_type" class="form-control cmbMarket"
                                           data-url="<?php echo site_url('market/getCaregoryByMarket');?>">
                                        <option value="">Select Market</option>
                                        <?php foreach ((array) $marketPlaces as $key => $value) {?>
                                               <option <?php echo $building['bld_market'] == $value['mar_id'] ? 'selected="selected"' : '';?>
                                                    value="<?php echo $value['mar_id'];?>"><?php echo $value['mar_name'];?></option>
                                               <?php }?>
                                   </select>
                              </div>
                         </div>
                         <div class="divCategories">
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Building Category</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="div-category">
                                             <ul class="li-category">
                                                  <?php
                                                    // $buildingCates = ((!empty($building['bld_market'])) || (isset($building['categories']) && !empty($building['categories']))) ?
                                                    //         $building['categories'] : $categories;

                                                    foreach ((array) $categories as $key => $value) {
                                                         ?>
                                                         <li><input required data-parsley-errors-container="#cat_err" data-parsley-required-message="Select category" name="categories[]" value="<?php echo $value['cat_id'];?>" 
                                                              <?php echo in_array($value['cat_id'], $allDetails['categoriesSelected']) ? 'checked="checked"' : '';?>
                                                                    type="checkbox"/><span><?php echo $value['cat_title'];?></span></li>
                                                              <?php
                                                         }
                                                       ?>
                                             </ul>
                                        </div><span id="cat_err"></span>
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Building Number</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required data-parsley-required-message="Enter building number" type="text" style="width: 500px;" class="form-control" 
                                          value="<?php echo $building['bld_number'];?>"
                                          name="project[bld_number]" id="bld_number" placeholder="Building Number"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="project[bld_address]" class='editor'><?php echo $building['bld_address'];?></textarea>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <textarea name="project[bld_description]" class='editor'><?php echo $building['bld_description'];?></textarea>
                              </div>
                         </div>

                         <?php if (isset($building['projImag']) && !empty($building['projImag'])) {?>
                                <?php foreach ($building['projImag'] as $key => $value) {?>
                                     <div class="form-group imgBox<?php echo $value['bimg_id'];?>">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                          <div class="col-md-6 col-sm-6 col-xs-12" style="padding-left: 10px;">
                                               <div class="input-group">
                                                    <?php echo img(array('src' => FILE_UPLOAD_PATH . 'building/' . $value['bimg_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                               </div>
                                               <?php if ($value['bimg_image']) {?>
                                                    <span class="help-block">
                                                         <a data-url="<?php echo site_url('building/removeImage/' . $value['bimg_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                    </span>
                                                    <span class="help-block">
                                                         <input <?php echo ($value['bimg_id'] == $building['bld_default_image']) ? 'checked' : '';?> 
                                                              class="btnSetDefaultImage" type="radio" name="bld_default_image" 
                                                              data-url="<?php echo site_url('building/setDefaultImage/' . $value['bimg_id'] . '/' . $building['bld_id']);?>"/>&nbsp; Make it default
                                                    </span>
                                                     <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="bimg_alt[<?php echo $value['bimg_id']?>]" id="bimg_alt" data-parsley-required ="true"data-parsley-required-message="Please enter an alt tag for the image" value="<?php echo $value['bimg_alt']; ?>"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                            </div>
                                               <?php }?>
                                          </div>
                                     </div>
                                     <?php
                                }
                           }
                         ?>
                         <div class="divImageGroup">
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="newupload">
                                             <input type="hidden" id="x10" name="x1[]" />
                                             <input type="hidden" id="y10" name="y1[]" />
                                             <input type="hidden" id="x20" name="x2[]" />
                                             <input type="hidden" id="y20" name="y2[]" />
                                             <input type="hidden" id="w0" name="w[]" />
                                             <input type="hidden" id="h0" name="h[]" />
                                             <input <?php if(isset($building['projImag']) && empty($building['projImag'])) {?> required data-parsley-required-message="Upload a image" <?php } ?> data-parsley-fileextension="jpg,png,gif,jpeg" type="file" class="form-control" 
                                                    name="prd_image[]" id="image_file0" onchange="fileSelectHandler('0', '595', '467')" />
                                             <img id="preview0" class="preview"/>
                                        </div>
                                        <span class="help-inline">Approximate image dimension 467(Height) X 595(Width)</span>

  <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="imagealt" id="bImgAlt" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                         </div>
                                   </div>



                              </div>
                              <div  id="divMoreProductImages">

                              </div>
                              <!--                              <div class="form-group">
                                                                 <label class="control-label col-md-3 col-sm-3 col-xs-12">More Image</label>
                                                                 <div class="col-md-6 col-sm-6 col-xs-12">
                                                                      <a href="javascript:void(0);" id="btnMoreProductImages" class="btn"><i class="icon-plus"></i></a>
                                                                 </div>
                                                            </div>-->
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success">Submit</button>' : '';?>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>
<script type="text/template" id="temSpecification">
     <div class="control-group grp-specification">
     <label class="control-label"></label>
     <div class="row-fluid">
     <div class="span4">
     <div class="control-group no-margin-bot">
     <div class=" controls-row">
     <input type="text" class="input-block-level" placeholder="Technical Details Key" name="specification[spe_specification][]">
     </div>
     </div>
     </div>
     <div class="span4">
     <div class="control-group no-margin-bot">
     <div class=" controls-row">
     <input type="text" class="input-block-level" placeholder="Technical Details Value" name="specification[spe_specification_detail][]">
     </div>
     </div>
     </div>
     <div class="span1">
     <div class="control-group no-margin-bot">
     <div class=" controls-row">
     <button id="btnRemoveMoreSpecification" class="btn btnRemoveMoreSpecification"><i class="icon-minus"></i></button>
     </div>
     </div>
     </div>
     </div>
     </div>
</script>

<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
</style>

<script>
  $('input[type=file]').change(function(e){
   var image =  $('input[type=file]')[0].files[0].name;
   if(image)
   {
    $('#bImgAlt').attr('data-parsley-required',true);
   }
   else
   {
          $('#bImgAlt').attr('data-parsley-required',false);
   }
});
  </script>