<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Building</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Image</th>
                                        <th>Building Number</th>
                                        <th>Building Name</th>
                                        <th>Building Address</th>
                                        <th>Market</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $projects as $key => $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url('building/view/' . $value['bld_id']);?>">
                                               <td style="width: 100px;"  class="trVOE">
                                                    <?php
                                                    $img = isset($value['defaultImg']['bimg_image']) ? $value['defaultImg']['bimg_image'] : '';
                                                    $oneImage = isset($value['projImag'][0]['bimg_image']) ? $value['projImag'][0]['bimg_image'] : '';
                                                    $img = empty($img) ? $oneImage : $img;
                                                    echo img(array('src' => FILE_UPLOAD_PATH . 'building/' . $img, 'height' => '50', 'width' => '130'));
                                                    ?>
                                               </td>
                                               <td class="trVOE"><?php echo $value['bld_number'];?></td>
                                               <td class="trVOE"><?php echo $value['bld_title'];?></td>
                                               <td class="trVOE"><?php echo $value['mar_address'];?></td>
                                               <td class="trVOE"><?php echo $value['mar_name'];?></td>
                                               <?php if (check_permission($controller, 'delete')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url('building/delete/' . $value['bld_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>