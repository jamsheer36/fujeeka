<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
          <?php echo $template['partials']['terms']?>
          <div class="col-md-9">
                    <div class="cntent trms">
                        <h2 class="heading2">
                  Complaint Terms
                </h2>

                <p>This Policy establishes a User focused complain management system which will enable the handling of complaints in a consistent, fair and professional manner. We are committed to providing exceptional service, developing and maintaining a healthy relationship with Users. If a user wants to make a complaint about a service, they should find it easy to do so and be assured that their complaints will be properly attended to. Fujeeka see complaints as an opportunity to improve service delivery to Users.</p>

                <p>This policy will ensure:</p>
                <ul>
                  <li>Fujeeka team are equipped with the knowledge, tools, skills and techniques to resolve complaints in a timely manner.</li>
                  <li>Provision of clear review option for users who may be dissatisfied with the outcome of a complaint.</li>
                  <li>Consistent improvement in the way Fujeeka interacts with users.</li>
                  <li>Complaints are managed in an objective, fair and transparent manner.</li>
                </ul>
                <p>If you want to make a formal complaint, kindly please submit the following information to us in writing: </p>

                <ul>
                  <li>Your full name(s), physical address, telephone number and email address;</li>
                  <li>The details of the problem you may have experienced regarding such service.</li>
                  <li>when you received the product </li>
                  <li>Your invoice numbers</li>
                  <li>Delivery date</li>
                  <li>A statement confirming that you are making the complaint in good faith;</li>
                  <li>A statement confirming that the information you are providing to us is, to the best of your knowledge, true and correct; Please include your signature in your complaint.</li>
                </ul>

                <p>In the event that the submitted information meets the complaints terms, Fujeeka will inform the company being complained of about the complaint and request that he reacts to the complaints within a time frame. Fujeeka has the right to change the reaction time in light of the actual situation. When the reaction period elapses and the company being complained of has not reacted, Fujeeka has the right to order certain discipline on such company appropriately.</p>
                <p>In the event that the company being complained of presents a counter-notice within the response time frame, which is considered as reasonable and substantial at first sight, Fujeeka will forward the counter-notice details to the complainant and request that he reacts to it within a set timeframe. On the off chance that the complainant objects to the counter-notice, the complainant may issue a dispute notice with Fujeeka.</p>

                <h5 class="sub-heading">Fujeeka may dissolve complaints if:</h5>
                <ul>
                  <li>Users fail to perform or otherwise materially breaches any term of this agreement.</li>
                  <li>Any materials submitted by users for complaints is fake or illegal.</li>
                </ul>


                    </div>
                </div>

          </div>
     </div>
</section>
