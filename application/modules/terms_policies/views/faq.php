<link rel="stylesheet" href="css/faq.css">
<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
          <?php echo $template['partials']['terms']?>
          <div class="col-md-9">
              <div class="cntent">
                <h2 class="heading2">
                  Frequently Asked Questions
                </h2>
              
                	<div class="content-pp">

                    <div class="accordion" id="accordionFAQ">
                        <div class="card">
                            <div class="card-header" id="faq_head1">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ1" aria-expanded="true" aria-controls="collapseFAQ1">
                                       <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;How can I buy products from Fujeeka?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFAQ1" class="collapse contcollapse in show" aria-labelledby="faq_head1" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>You can add multiple products in the cart and proceed the order against each seller or you can directly start the order from the product listing and detail view pages.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head2">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ2" aria-expanded="true" aria-controls="collapseFAQ2">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;How can I sell products in Fujeeka?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ2" class="collapse contcollapse" aria-labelledby="faq_head2" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>To sell your products, first you have to register as a seller and fill the ecom settings. Then you can add your products in Fujeeka to showcase them for sale.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head3">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ3" aria-expanded="true" aria-controls="collapseFAQ3">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;How the purchase flow works?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ3" class="collapse contcollapse" aria-labelledby="faq_head3" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>Buyer can start the order by negotiating the product price. After mutual acceptance from seller and buyer, the order amount will be paid by buyer to Fujeeka's secure account.
                                        The amount will be held until the seller delivers the order. Once the delivery is confirmed by the buyer, the amount will be transferred to the seller.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head4">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ4" aria-expanded="true" aria-controls="collapseFAQ4">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;Where can I see my orders in Fujeeka?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ4" class="collapse contcollapse" aria-labelledby="faq_head4" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>If you are a buyer, you can see your orders in Dashboard-Buying Tool-My Orders. If you are a seller, you can see your orders in Dashboard-Selling Tool-Order Management System.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head5">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ5" aria-expanded="true" aria-controls="collapseFAQ5">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;What are the details need to be submitted for seller and buyer?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ5" class="collapse contcollapse" aria-labelledby="faq_head5" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>As a buyer you have to provide shipping address with contact details. As a seller you have to provide Pickup address, Invoice address, Bank Details, Tax details and Shipping Cost.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head6">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ6" aria-expanded="true" aria-controls="collapseFAQ6">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;Is it risk free to buy or sell products through Fujeeka?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ6" class="collapse contcollapse" aria-labelledby="faq_head6" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>Yes, it is 100% secure to do business with Fujeeka. The order has been paid, cancelled and refunded based on mutual agreement (Between buyer and seller) only.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head7">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ7" aria-expanded="true" aria-controls="collapseFAQ7">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;How can I get my payment once I deliver the order to buyer?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ7" class="collapse contcollapse" aria-labelledby="faq_head7" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>Once the delivery has been confirmed by the buyer, your order amount will be transferred to your specified bank account.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head8">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ8" aria-expanded="true" aria-controls="collapseFAQ8">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;How can I get my payment if the buyer didn't confirm the delivery?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ8" class="collapse contcollapse" aria-labelledby="faq_head8" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>If the buyer didn't take any action to confirm your delivery, it will be automatically confirmed on the 3rd day from the actual date of delivery.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head9">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ9" aria-expanded="true" aria-controls="collapseFAQ9">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;How can I get my refund if I return the delivered product?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ9" class="collapse contcollapse" aria-labelledby="faq_head9" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>Once seller has accepted your refund request, the order has been cancelled and your amount will be transferred to your specified bank account.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head10">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ10" aria-expanded="true" aria-controls="collapseFAQ10">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;Who can have the access to cancel the order?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ10" class="collapse contcollapse" aria-labelledby="faq_head10" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>Both seller and buyer has the access to cancel their orders if it is unpaid. Once it is paid then buyer needs to send cancel request to seller, if it is accepted by seller then the order gets cancelled.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head11">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ11" aria-expanded="true" aria-controls="collapseFAQ11">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;Can I cancel the order after payment?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ11" class="collapse contcollapse" aria-labelledby="faq_head11" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>Yes. If you need to cancel your order after payment, you can send a cancel request to seller. Once the seller accepts your request, your amount will be refunded from Fujeeka's secure account.
                                        <br>
                                        You can send cancel request till the order gets confirmed.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head12">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ12" aria-expanded="true" aria-controls="collapseFAQ12">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;Can I cancel the order after delivery confirmation?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ12" class="collapse contcollapse" aria-labelledby="faq_head12" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>No. You can't cancel your order if the delivery is confirmed.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head13">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ13" aria-expanded="true" aria-controls="collapseFAQ13">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;Where can i view my complete order details?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ13" class="collapse contcollapse" aria-labelledby="faq_head13" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>You can view your complete log against your order through Order History option which is available in My orders or OMS page of your dash board.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head14">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ14" aria-expanded="true" aria-controls="collapseFAQ14">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;How can I know my order payment settlements?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ14" class="collapse contcollapse" aria-labelledby="faq_head14" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>If you are a seller, once the order amount transferred to your specified bank account you will be notified and your corresponding Order Status will be changed to 'Order Fulfilled' with Payment Status as 'Payment Released'.<br></p>
                                    <p> If you are a buyer, once the refund amount transferred to your specified bank account you will be notified and your corresponding Order Status will be changed to 'Order Returned' with Payment Status as 'Payment Refunded'.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head15">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ15" aria-expanded="true" aria-controls="collapseFAQ15">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;Can I do partial return after delivery?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ15" class="collapse contcollapse" aria-labelledby="faq_head15" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>No. You cannot do partial return. As per our policy, the refund is progressed only if the 'whole order' is returned and accepted by seller.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faq_head16">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFAQ16" aria-expanded="true" aria-controls="collapseFAQ16">
                                        <i class="fa fa-angle-right f-700" aria-hidden="true"></i>&nbsp;&nbsp;Can Fujeeka support logistics and insurance?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFAQ16" class="collapse contcollapse" aria-labelledby="faq_head16" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    <p>No. The seller will be responsible for logistics and insurance. But very soon we will come with those features.</p>
                                </div>
                            </div>
                        </div>

                    </div> 
                    

                </div>
               

              </div>
            </div>

          </div>
     </div>
</section>