<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
          <?php echo $template['partials']['terms']?>
          <div class="col-md-9">
                    <div class="cntent trms">
                        <h2 class="heading2">
                  Intellectual Property Rights
                </h2>


                        <p>Fujeeka is the owner and the lawful licensee of all the of rights to the Web Site and its content ("Web Site Content"). Web Site means the materials displayed on this website, including without limitation all editorial materials, information, photographs, illustrations, artwork and other graphic materials, and names, logos and trademarks, are solely our property and may be protected, at our sole discretion, by copyright, trade mark and other intellectual property laws.</p>
                        <p>Fujeeka respects the intellectual property rights of others and we expect our users to honor the same. All title, ownership, and intellectual property rights in the Web Site and the Web Site Content shall remain in Fujeeka, its affiliates or licensor’s of the Web Site content, as the case may be. We're committed to protect the intellectual property rights of third parties and to provide our members with a safe platform to buy and sell.  Fujeeka cannot confirm in any manner that sellers have the right to distribute or sell their listed items. Be that as it may, We are committed to removing infringing or unlicensed items once an authorized representative of the rights owner properly reports to us. We ensure that item listed does not infringe upon the copyright, trademark or other intellectual property rights of third parties.
</p>

                       <p>All rights, not generally asserted under this Agreement by Fujeeka, are hereby reserved. Any data or advertisements contained on, disseminated through, or connected, downloaded or got from any of the services contained on the Web Site or any offer showed on or regarding any service offered on the Web Site ("Website Information") is proposed, exclusively to give general information to the personal use of the User(s), who completely acknowledge any responsibility and liabilities emerging from and out of the use of such Information.
</p>

                   <p>Provided that you are eligible to use the Site, you are granted a limited license to access and use the Site and to download or print a copy of any portion of the Content to which you have properly gained access solely for your personal, non-commercial use. We reserve all rights not expressly granted to you in and to the Site, the Content and the Marks.
</p>
                    </div>
                </div>

          </div>
     </div>
</section>
