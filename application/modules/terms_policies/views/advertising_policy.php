<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
          <?php echo $template['partials']['terms']?>
          <div class="col-md-9">
                    <div class="cntent trms">
                        <h2 class="heading2">
                          Advertising Policy
                        </h2>

                    <p>By using the Site, users consented and agree to the Rules stated in this policy. www.fujeeka.com reserves the right to amend all the terms and conditions of these Rules without prior notice to its user. The reviewed rules will have effect upon publication on the Site. If a user disagree with the modified rules user shall instantly cease to use the Site. If a member continues to use the Site after the publication of the changed rules, such user shall be deemed as agreeing and accepting the revised rules.</p>

                    <h5 class="sub-heading">Prohibited content</h5>
                    <ul>
                      <li>Adverts must not comprise, encourage or promote illicit items, services or activities. Adverts focused to minors must not promote products, services or content that are improper, unlawful or perilous, or that misuse, misdirect or apply undue weight on the age groups focused on. </li>
                      <li>Adverts must not take part in predatory promoting practices or contain content that discriminates, irritates, incites or belittles individuals who use this Site. </li>
                      <li>Adverts must not promote the sale or use of hazardous supplements.</li>
                      <li>Adverts must not promote the sale or use of adult items or services. Adverts for contraceptives can be promoted yet should center around the contraceptive features of the item and not on sexual pleasure or sexual augmentation, and should be focused to individuals aged 18 years or more. </li>
                      <li>Adverts must not contain adult content. This incorporates nudity, demonstrations of people in clear or suggestive positions or activities that are excessively suggestive or sexually provocative. </li>
                      <li>Adverts must not contain content that infringes on or violates the rights of any third party, including copyright, trademark, privacy, publicity or other personal or proprietary rights.</li>
                      <li>Adverts must not contain beguiling, false or misleading content, including tricky claims, offers or business practices.</li>
                    </ul>

                    <h5 class="sub-heading">Adverts review process</h5>
                    <p>Before adverts display on Our Website, Fujeeka Technologies limited management reviews the content of your Ads to make sure they meet our advertising policy. </p>

                      <h5 class="sub-heading">Here is what We do:</h5>
                      <p>In the process of reviewing your advert, we’ll check your advert's images, text, targeting and, content. Your advert will not be approved if it doesn’t match the products or services promoted. It is also disapproved in the event that it doesn’t fully comply with our advertising policy.</p>
                      <p>On the off chance that your advert is disapproved due to not fully complying with our advertising policy, you should edit the advert to comply with our advertising policy and resubmit it for approval.</p>

                      <h5 class="sub-heading">Take note:</h5>
                      <p>All advert mechanisms, including any text, images or other media, must be related and appropriate to the product or service being offered and the audience viewing the advert.</p>
                      <p>Adverts must clearly represent the company, product, service or brand that is being advertised.</p>

                      <h5 class="sub-heading">We have the right to remove:</h5>
                      <ul>
                        <li>Adverts in the event of any complaints received as touching the banner's content.</li>
                        <li>Adverts on the ground of any scams or scam related complaints in the advert’s content. </li>
                        <li>Adverts in the event of credit card or any other payment related disputes.</li>
                      </ul>

                    </div>
                </div>

          </div>
     </div>
</section>
