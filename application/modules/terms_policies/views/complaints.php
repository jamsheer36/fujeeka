<div class="container-fluid banner">
    <img src="images/banners/submit.jpg">
</div>
<div class="container-fluid contback">
    <div class="container contcont">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading2">Submit a Complaint</h2>
                <p>Fujeeka.com is a platform connecting global buyers and China suppliers. To avoid misunderstanding and provide better support for our members, Fujeeka.com have a professional team to handle complaints of trade dispute and Intellectual Property Infringement.</p>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
            <div class="col-md-12">
                <h5 class="heading2">Service Process</h5>
            </div>
            <div class="col-md-3">
                <div class="service text-center">
                    <img src="images/icons/icons8-submit-resume-96.png" class="img-circle img-thumbnail" width="80" alt="">
                    <p>Submit a Complaint</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="service text-center">
                    <img src="images/icons/icons8-process-96.png" class="img-circle img-thumbnail" width="80" alt="">
                    <p>Confirm to process</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="service text-center">
                    <img src="images/icons/icons8-voice-recognition-96.png" class="img-circle img-thumbnail" width="80" alt="">
                    <p>Communication and Resolving</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="service text-center">
                    <img src="images/icons/icons8-report-card-96.png" class="img-circle img-thumbnail" width="80" alt="">
                    <p>Receive Results and Resolutions</p>
                </div>
            </div>
          
            <div class="col-md-12">
                <h6 class="heading2">Report a Trade Dispute</h6>
            </div>
            <div class="col-md-6">
                <div class="subty">
                    <ul>
                        <li>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="customRadio" name="example1">
                                <label class="custom-control-label" for="customRadio"> Products not received after payment</label>
                            </div>
                        </li>
                        <li>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="customRadio1" name="example1">
                                <label class="custom-control-label" for="customRadio1"> Quantity not match with contract</label>
                            </div>
                        </li>
                       
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="subty">
                    <li>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="customRadio4" name="example1">
                            <label class="custom-control-label" for="customRadio4"> Quality not as agreed</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="customRadio5" name="example1">
                            <label class="custom-control-label" for="customRadio5">  Packaging not as agreed</label>
                        </div>
                    </li>
                   
                </div>
            </div>
            <div class="col-md-12">
                <h6 class="heading2">Report an Intellectual Property Infringement</h6>
            </div>
            <div class="col-md-6">
                <div class="subty">
                    <ul>
                        <li>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="customRadio6" name="example1">
                                <label class="custom-control-label" for="customRadio6"> Picture Embezzlement</label>
                            </div>
                        </li>
                        <li>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="customRadio7" name="example1">
                                <label class="custom-control-label" for="customRadio7"> Copyright Infringement</label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="subty">
                    <li>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="customRadio8" name="example1">
                            <label class="custom-control-label" for="customRadio8"> Trademark Infringement</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="customRadio9" name="example1">
                            <label class="custom-control-label" for="customRadio9"> Patent Infringement</label>
                        </div>
                    </li>
                </div>
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-success btn-green btn-bold btnSupplierRegister">Continue</button>
            </div>
            <!--service-->
        </div>
    </div>
</div>