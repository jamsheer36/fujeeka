<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
          <?php echo $template['partials']['terms']?>
               <div class="col-md-9">
                    <div class="cntent trms">
                         <h2 class="heading2">
                              Quality Control
                         </h2>
                         <p>The era of paying for goods that are a far cry from what its description promised is over as we are a b2b online platform that is like no other, ensuring that all the pieces that make a successful and mutually beneficial transaction for both wholesalers and retailers, check out adequately.</p>
                         <p>Before we approve a wholesaler to partner up with us on our platform, we ensure that both the individual and his or her products are vetted to acceptable industry standards.</p>
                         <p>When a wholesaler applies to partner with us and distribute his or her goods on our platform, we ensure all the products that are proposed to be listed for sale are thoroughly tackled with our world standard quality measures, and if said product doesn't meet all our requirements, the product would be denied the access to be sold on our platform.</p>
                         <p>As long as the product you require in wholesale quantity is listed on our website, you can be sure that you are going to be purchasing only the most authentic version of said product and hence you have no worries.</p>
                         <p>Once you have made buying from our platform paramount in your business, you can be confident that your retail business will never be the same.</p>

                    </div>
               </div>

          </div>
     </div>
</section>
