
        <section class="contactback">
            <div class="container">
                <div class="con-container">
                    <div class="frms divGetAQuoteForm">
                        <div class="cardd">
                            <div class="main-login main-center">
                                <div class="col-md-12">
                                    <h2 class="heading2" style="text-transform: uppercase;margin-bottom: 20px;">Get In Touch</h2>
                                </div>
                                <form class="" method="post" action="#">
                                    <div class="row">
                                        <div class="col-md-6 fieldbot">
                                            <div class="form-group">
                                                <!--<label for="name" class="cols-sm-2 control-label">Your Name</label>-->
                                                <div class="cols-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                                        <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <!--<label for="email" class="cols-sm-2 control-label">Your Email</label>-->
                                                <div class="cols-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                                        <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <!--<label for="email" class="cols-sm-2 control-label">Mobile</label>-->
                                                <div class="cols-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                                        <input type="text" class="form-control" name="number" id="number" placeholder="Number">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <!--<label for="email" class="cols-sm-2 control-label">Country</label>-->
                                                <div class="cols-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
                                                        <input type="text" class="form-control" name="location" id="location" placeholder="Country">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 fieldbot">
                                            <div class="form-group">
                                                <!--<label for="email" class="cols-sm-2 control-label">Subject</label>-->
                                                <div class="cols-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-align-right" aria-hidden="true"></i></span>
                                                        <input type="text" class="form-control" name="number" id="number" placeholder="Subject">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <!--<label for="email" class="cols-sm-2 control-label">Message</label>-->
                                                <div class="cols-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-comment" aria-hidden="true"></i></span>
                                                        <textarea name="enq_query" class="form-control" placeholder="Message" style="    height: 150px !important;font-size: 12px;
                                                            color: #6c757d;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 fieldbot text-right" style="margin-top: 15px">
                                            <button type="button" class="btn btn-success btn-green mb-2">SUBMIT</button>
                                        </div>
                                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="left-sec">
                        <div class="pic">
                            <img src="images/banners/con1.png" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </section>