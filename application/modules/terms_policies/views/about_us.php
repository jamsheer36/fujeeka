<section class="abt">
      	<img src="images/banners/about.jpg" width="100%" class="mb-4">
      </section>

      <div class="container mb-3 mt-3">
      	<div class="abt_para">
      		<div class="row">
      			<div class="col-md-12 text-center mb-2">
      				<h2 class="heading2">About Fujeeka</h2>
      			</div>
      			<div class="col-md-12 text-center">
      			<p class="abt_sec">We are Fujeeka technology Limited, and we are here with Virtual reality experience for the buyers and suppliers around the world. </p>
      		</div>
      		</div>
      	</div>
      	<div class="abt_para2">
      		<div class="row">
      			<p class="text-center">Our team is always developing services to help the business grow and to show you the new opportunities in the modern world of business. As you know Fujeeka is not just an online B2B platform, we are here with a new Feature called Virtual Reality which enables the buyers to go through the store of the supplier and can order the products by selecting from the store. This is the new satisfaction which all the buyers need and we understood that which made us to develop the new feature Virtual Reality. Understanding buyers needs is our concern and as well as supporting suppliers to fulfil their destiny. So that we always analyse and research the market for Fujeeka to be advanced and updated. Hope you enjoy the new world of Business with us.</p>
      		</div>
      	</div>
      </div>

      <section class="wfjka">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center mb-2">
      				<h2 class="heading2 white-txt">WHY FUJEEKA?</h2>
            </div>

            <div class="col-md-12">
                <div class="wh">
                  <p class="text-center">At present a wholesale customer has to visit a particular market which deals a particular category of products (eg:  Mobile accessories), find a trusted supplier who offers lowest price from hundreds of suppliers, select the products he needs, order them, make payments, arrange shipment of purchased products to his place. The B2B business is changing now. Before a customer requires 10000 qty of product in single variant. It is easy to select such product. Now there are many variants for all kind of products. So the customer needs to buy 1000 qty of each 10 variant of the same product. This make it is very difficult to find products and suppliers.</p>

                </div>
              <h5 class="mt-5 mb-5 text-center">Advantages of Proposed System:</h5>
              <ul>
                <li>Time Saver : In this system a customer does not have to physically visit the store for purchasing. He can do the product research and purchases any time any where. At the same time supplier can list their products any time anywhere.</li>
                <li>Cost Effective : This system is cost effective for both suppliers and customers. Supplier does not have to maintain large store fronts which helps with huge room rents, electricity charges, maintenance charges, etc. The customer does not have to travel to remote destinations to find the matching products and suppliers for his requirements. This helps to cut the costs by a huge level.</li>
                <li>Better reachability : The products of the suppliers can reach a wide range of customers at global level. This ensures better sales than current system. The reachability can be further increased by ads.</li>
                <li>Easy to find products according latest trend</li>
                <li>Reduces manpower</li>
                <li>Better security : Each suppliers will be verified physically and rating system for products, suppliers & customers ensures peace of mind for both suppliers & customers.</li>
                <li>Convenient & Easy to use</li>
                <li>The language differences of Suppliers and customers won’t affect.</li>
              </ul>
      			</div>
          </div>
        </div>
      </section>

      <section class="abt_details">
      	<div class="container">
      		<div class="row">
      			<div class="col-md-4">
      				<div class="abt_details_cont mb-4 mt-4">
      				<img src="images/icons/mission.png">
      					<h4 class="pb-2 pt-2">Our Mission</h4>
      					<p class="text-center">Our mission is to make the Buyers &amp; Suppliers getting connected and to help both parties to grow their business to next level.</p>
      				</div>
      			</div>
      			<div class="col-md-4">
      				<div class="abt_details_cont mb-4 mt-4">
      				<img src="images/icons/why.png">
      					<h4 class="pb-2 pt-2">Why Fujeeka</h4>
      					<p class="text-center">Fujeeka gives you the new experience which is called a 360째 view of the store which helps the buyers to see the store and helps the suppliers to show their capability.</p>
      				</div>
      			</div>
      			<div class="col-md-4">
      				<div class="abt_details_cont mb-4 mt-4">
      				<img src="images/icons/customers.png">
      					<h4 class="pb-2 pt-2">Our Customers</h4>
      					<p class="text-center">We will be here with a lot of wholesalers which will meet the certain standards of quality and trustworth which helps the buyers to discover the new opportunities and develop their business.</p>
      				</div>
      			</div>
      		</div>
      	</div>
      </section>
