<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
          <?php echo $template['partials']['terms']?>
          <div class="col-md-9">
              <div class="cntent trms">
                <h2 class="heading2">
                  Product Listing Policy
                </h2>
                  <h5 class="sub-heading">Category, product, and listing restrictions</h5>
                	<p>Certain products cannot be listed or sold on Fujeeka Technologies Limited website. You may not post or sell any item that is restricted or prohibited by any applicable local law in any country or jurisdiction. Please, be aware that the Fujeeka Technologies Limited website (the "Site") functions as a B2B e-commerce platform, thus the selling or posting of items may be prohibited because of laws outside of the jurisdiction where you reside.</p>
                	<p>Please be aware that Fujeeka Technologies Limited website (the “Site”) does not permit any selling or posting of items that are illegal, infringe upon the intellectual property rights of others, or may easily be used for illegal purposes.</p>
               <p>Fujeeka website prohibits any supplier who try to list copy of product or copyright images, illegal product selling /services and any kind of malpractice on Fujeeka website.</p>
               <p>www.fujeeka.com reserves the right to remove any trade items and such account will be permanently blocked without any notice.</p>

              </div>
            </div>

          </div>
     </div>
</section>
