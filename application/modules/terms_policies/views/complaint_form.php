<div class="container-fluid complaintform_back">
    <div class="container complaintform_cont">
        <div class="row">
            <div class="col-md-12">
                <h4 class="heading2">Report a Trade Dispute</h4>
            </div>
            <!--<div class="col-md-12"><hr></div>-->
            <div class="col-md-12">
                <h2 class="heading3">The Respondent's Information</h2>
                <div class="complaintform_form">
                    <div class="row">
                        <div class="col-md-4 fo">
                            <p>Respondent's Company Name</p>
                        </div>
                        <div class="col-md-8 fo">
                            <input type="text" class="form-control" name="comName" value="">
                        </div>
                        <div class="col-md-4 fo">
                            <p>Respondent's Email Address</p>
                        </div>
                        <div class="col-md-8 fo">
                            <input type="text" class="form-control" name="comEmail" value="">
                        </div>
                        <div class="col-md-4 fo">
                            <p>Respondent's Company Website</p>
                        </div>
                        <div class="col-md-8 fo">
                            <input type="text" class="form-control" name="comWebsite" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h2 class="heading3">Complaint Details</h2>
                <div class="complaintform_form">
                    <div class="row">
                        <div class="col-md-4 fo">
                            <p>Product Name</p>
                        </div>
                        <div class="col-md-8 fo">
                            <input type="text" class="form-control" name="comPName" value="">
                        </div>
                        <div class="col-md-4 fo">
                            <p>Transaction Amount</p>
                        </div>
                        <div class="col-md-8 fo">
                            <div class="row">
                                <div class="col-md-3">
                                    <select name="feeType" class="form-control">
                                        <option class="option" value="0">USD</option>
                                        <option class="option" value="1">EUR</option>
                                        <option class="option" value="2">AUD</option>
                                        <option class="option" value="3">RMB</option>
                                        <option class="option" value="4">HKD</option>
                                    </select>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="comTA" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 fo">
                            <p>Complaint Type</p>
                        </div>
                        <div class="col-md-8 fo">
                            <textarea name="complaintDesc" class="form-control" rows="5"></textarea>
                        </div>
                        <div class="col-md-4 fo">
                            <p>Upload Evidence Files</p>
                        </div>
                        <div class="col-md-8 fo">
                            <input type="file" class="form-control">
                            <p style="margin-top: 10px;color: #444444">Required Evidences: First email from respondent, Communication Records (include emails or chat records from instant messenger), Payment Receipt or Documents.</p>
                            <ul style="    color: #939393;">
                                <li> Other files: Proforma Invoice, Sales Contract, Product Photos, Inspection Documents from third parties and any other proof files.</li>
                                <li> You can upload several files of same type in a zipped file.</li>
                                <li> Maximum 5 files; 2 MB for each.</li>
                                <li> Upload doc,docx,xls,xlsx,pdf,txt,msg,jpg,jpeg,gif,zip or rar format only.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h2 class="heading3">Your Contact Details</h2>
                <div class="complaintform_form">
                    <div class="row">
                        <div class="col-md-4 fo">
                            <p>Contact Person</p>
                        </div>
                        <div class="col-md-8 fo">
                            <input type="text" class="form-control" name="CP" value="">
                        </div>
                        <div class="col-md-4 fo">
                            <p>Email Address</p>
                        </div>
                        <div class="col-md-8 fo">
                            <input type="text" class="form-control" name="Emailadd" value="">
                        </div>
                        <div class="col-md-4 fo">
                            <p>Telephone Number</p>
                        </div>
                        <div class="col-md-8 fo">
                            <input type="text" class="form-control"  name="phoneComplaint">
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-4 fo">
                            <p>Verification Code</p>
                        </div>
                        <div class="col-md-8 fo">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group captcha-center ">
                                        <div id="captcha_img"><img id="Imageid" class="mx-auto d-block" src="http://halal86.com/fujeeka_web//assets/uploads/captcha/1547718962.2878X7U5.png" style="width: 150; height: 38; border: 0;" alt="captcha"></div>
                                        <input value="X7U5" type="hidden" name="captcha_code" id="captcha_code">
                                        <div class="input-group-append">
                                            <a class="btn btn-default btn-sm refresh_captcha">
                                            <i class="fas fa-redo"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">                             
                                    <input type="text" class="form-control"  name="phoneComplaint">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-right"><button type="submit" class="btn btn-success btn-green btn-bold btnSupplierRegister">submit</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>