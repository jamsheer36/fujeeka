<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class terms_policies extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Fujeeka | Terms and policies';
       }

       public function index() {
            $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/terms');
     }
     public function listing_policy() {
          $this->page_title = 'Fujeeka | Listing Policy';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/listing_policy');
     }
     public function terms_of_use() {
          $this->page_title = 'Fujeeka | Terms of use';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/terms_of_use');
     }
     public function intellectual_property() {
          $this->page_title = 'Fujeeka | Intellectual Property';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/intellectual_property');
     }
     public function complaint_terms() {
          $this->page_title = 'Fujeeka | Complaint Terms';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/complaint_terms');
     }
     public function service_agreement() {
          $this->page_title = 'Fujeeka | Service Agreement';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/service_agreement');
     }
     public function rfq_policy() {
          $this->page_title = 'Fujeeka | RFQ Policy';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/rfq_policy');
     }
     public function advertising_policy() {
          $this->page_title = 'Fujeeka | Advertising Policy';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/advertising_policy');
     }
     public function faq() {
          $this->page_title = 'Fujeeka | FAQ';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/faq');
     }
     public function contact_us() {
          $this->page_title = 'Fujeeka | Contact Us';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/contact_us');
     }
     public function complaints() {
          $this->page_title = 'Fujeeka | Complaints';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/complaints');
     }
     public function complaint_form() {
          $this->page_title = 'Fujeeka | Submit Complaint';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/complaint_form');
     }
     public function about_us() {
          $this->page_title = 'Fujeeka | About Us';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/about_us');
     }
     
      public function terms_conditions() {
          $this->page_title = 'Fujeeka | Terms &amp; Conditions';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/terms-conditions');
     }
     
     public function quality_control() {
          $this->page_title = 'Fujeeka | Quality Control';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/quality_control');
     }
     
     public function registration_process() {
          $this->page_title = 'Fujeeka | Registration Process';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/registration_process');
     }
     
     public function sitemap() {
          $this->page_title = 'Fujeeka | Sitemap';
          $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/sitemap');
     }

  }
  