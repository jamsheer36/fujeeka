<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class api_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tbl_users = TABLE_PREFIX . 'users';
        $this->tbl_products = TABLE_PREFIX . 'products';
        $this->tbl_products_images = TABLE_PREFIX . 'products_images';
        $this->tbl_products_keyword = TABLE_PREFIX . 'products_keyword';
        $this->tbl_products_categories = TABLE_PREFIX . 'products_categories';
        $this->tbl_products_specification = TABLE_PREFIX . 'products_specification';
        $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
        $this->tbl_countries = TABLE_PREFIX . 'countries';
        $this->tbl_states = TABLE_PREFIX . 'states';
        $this->tbl_products_order = TABLE_PREFIX . 'products_order_master';
        $this->tbl_favorite = TABLE_PREFIX . 'favorite_list';
        $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
        $this->tbl_supplier_images = TABLE_PREFIX . 'supplier_shop_images';
        $this->tbl_supplier_buildings_cate_assoc = TABLE_PREFIX . 'supplier_buildings_cate_assoc';
        $this->tbl_category = TABLE_PREFIX . 'category';
        $this->tbl_supplier_shop_images = TABLE_PREFIX . 'supplier_shop_images';
        $this->tbl_banner = TABLE_PREFIX . 'banner';
        $this->tbl_advertisement = TABLE_PREFIX . 'advertisement';
        $this->tbl_market = TABLE_PREFIX . 'market_places';
        $this->tbl_rfq_notification = TABLE_PREFIX . 'rfq_notification';
        $this->tbl_feeds = TABLE_PREFIX . 'feeds';
        $this->tbl_rfq_products_send = TABLE_PREFIX . 'rfq_products_send';
        $this->tbl_buildins = TABLE_PREFIX . 'buildins';
        $this->tbl_supplier_followers = TABLE_PREFIX . 'supplier_followers';
        $this->tbl_groups = TABLE_PREFIX . 'groups';
        $this->notification = TABLE_PREFIX . 'pushnotification_master';
        $this->order_comments = TABLE_PREFIX . 'products_order_comments';
        $this->logistics_types = TABLE_PREFIX . 'logistics_types';
        $this->tbl_logistics_comp_images = TABLE_PREFIX . 'logistics_comp_images';
        $this->tbl_logistics_services_images = TABLE_PREFIX . 'logistics_services_images';
        $this->qc_services_images = TABLE_PREFIX . 'qc_services_images';
        $this->tbl_logistics_services = TABLE_PREFIX . 'logistics_services';
        $this->users_logistics = TABLE_PREFIX . 'users_logistics';
        $this->logistics_quotes = TABLE_PREFIX . 'logistics_quotes';
        $this->qc_quotes = TABLE_PREFIX . 'qc_quotes';
        $this->logistics_quotes_types = TABLE_PREFIX . 'logistics_quotes_types';
        $this->tbl_qc_images = TABLE_PREFIX . 'qc_images';
        $this->qc_services = TABLE_PREFIX . 'qc_services';
        $this->supplier_banner_images = TABLE_PREFIX . 'supplier_banner_images';
    }
    public function getUserByIdentity($identity)
    {
        if (!empty($identity)) {
            $identityColomn = 'usr_email';
            $user = $this->db->get_where($this->tbl_users, array($identityColomn => $identity))->row_array();
            return $user;
        } else {
            return false;
        }
    }

    // functions to get home page details
    public function getLatestProducts($view_all = '', $offset = '', $limit = '', $filter = '', $sort_by = '', $sup_id = '', $type = '')
    {
        if ($filter) {
            $categories[] = $filter;
            $sub_categories = $this->common_model->getAllCategories($filter);
            $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
        }
        $latest = array();
        $this->db->select('*');

        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        if (!$view_all) {
            $this->db->where('prd_id in(select max(prd_id) from ' . $this->tbl_products . ' group by prd_supplier)');
        }
        if ($sup_id) {
            $this->db->where('prd_supplier', encryptor($sup_id, 'D'));
        }
        $this->db->where('prd_status', 1);
        if ($filter) {
            $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
            $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
        }
        if ($sort_by) {
            if ($sort_by == 'pricehightolow') {
                $this->db->order_by('prd_price_min', 'desc');
            } else if ($sort_by == 'pricelowtohigh') {
                $this->db->order_by('prd_price_min', 'asc');
            }
        }
        if ($type != "our_product") {
            $this->db->order_by('prd_added_on', 'desc');
        }
        if (!$view_all) {
            $this->db->limit(4);
        } else {
            $this->db->limit($limit, $offset);
        }

        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            $latest[$key]['type'] = 'product';
            $latest[$key]['prd_id'] = encryptor($val['prd_id']);
            $latest[$key]['name'] = $val['prd_name_en'];
            $latest[$key]['price'] = $val['prd_price_min'];
            $latest[$key]['unit'] = $val['unt_unit_en'];
            $latest[$key]['moq'] = $val['prd_moq'];
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
            $latest[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
        }

        return $latest;
    }
    public function getHotSellingProducts($view_all = '', $offset = '', $limit = '', $filter = '', $sort_by = '')
    {
        if ($filter) {
            $categories[] = $filter;
            $sub_categories = $this->common_model->getAllCategories($filter);
            $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
        }
        $hotsell = array();
        $this->db->select('*');
        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        $this->db->where('prd_hot_selling', 1);
        $this->db->where('prd_status', 1);
        if ($filter) {
            $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
            $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
        }
        if ($sort_by) {
            if ($sort_by == 'pricehightolow') {
                $this->db->order_by('prd_price_min', 'desc');
            } else if ($sort_by == 'pricelowtohigh') {
                $this->db->order_by('prd_price_min', 'asc');
            }
        }
        $this->db->order_by('prd_added_on', 'desc');
        if (!$view_all) {
            $this->db->limit(4);
        } else {
            $this->db->limit($limit, $offset);
        }

        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            $hotsell[$key]['type'] = 'product';
            $hotsell[$key]['prd_id'] = encryptor($val['prd_id']);
            $hotsell[$key]['name'] = $val['prd_name_en'];
            $hotsell[$key]['price'] = $val['prd_price_min'];
            $hotsell[$key]['unit'] = $val['unt_unit_en'];
            $hotsell[$key]['moq'] = $val['prd_moq'];
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
            $hotsell[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
        }

        return $hotsell;
    }
    public function getOfferProducts($view_all = '', $offset = '', $limit = '', $filter = '', $sort_by = '')
    {
        if ($filter) {
            $categories[] = $filter;
            $sub_categories = $this->common_model->getAllCategories($filter);
            $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
        }
        $offer = array();
        $this->db->select('*');
        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        $this->db->where('prd_offer_price_min >', 0);
        $this->db->where('prd_offer_price_max >', 0);
        $this->db->where('prd_status', 1);
        if ($filter) {
            $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
            $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
        }
        // if ($sort_by) {
        //     if ($sort_by == 'pricehightolow') {
        //         $this->db->order_by('prd_price_min', 'desc');
        //     } else if ($sort_by == 'pricelowtohigh') {
        //         $this->db->order_by('prd_price_min', 'asc');
        //     }
        // }
        $this->db->order_by('prd_added_on', 'desc');
        if (!$view_all) {
            $this->db->limit(6);
        } else {
            $this->db->limit($limit, $offset);
        }

        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            $offer[$key]['type'] = 'product';
            $offer[$key]['prd_id'] = encryptor($val['prd_id']);
            $offer[$key]['name'] = $val['prd_name_en'];
            $offer[$key]['org_price'] = $val['prd_price_min'];
            $offer[$key]['offer_price'] = $val['prd_offer_price_min'];
            $offer[$key]['unit'] = $val['unt_unit_en'];
            $offer[$key]['moq'] = $val['prd_moq'];
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
            $offer[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
            $offer[$key]['sort_price'] = $val['prd_offer_price_min'] > 0 ? $val['prd_offer_price_min'] : $val['prd_price_min'];

        }
        if ($sort_by) {
            if ($sort_by == 'pricehightolow') {
                usort($offer, function ($a, $b) {
                    return $a['sort_price'] < $b['sort_price'];
                });
            }
            if ($sort_by == 'pricelowtohigh') {
                usort($offer, function ($a, $b) {
                    return $a['sort_price'] > $b['sort_price'];
                });
            }
        }

        return $offer;
    }
    public function getStockProducts($view_all = '', $offset = '', $limit = '', $filter = '', $sort_by = '', $sup_id = '')
    {
        if ($filter) {
            $categories[] = $filter;
            $sub_categories = $this->common_model->getAllCategories($filter);
            $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
        }
        $stock = array();
        $this->db->select('*');
        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        $this->db->where('prd_status', 1);
        $this->db->where('prd_is_stock_prod', 1);
        $this->db->where('prd_is_flash_sale', 0);
        if ($sup_id) {
            $this->db->where('prd_supplier', encryptor($sup_id, 'D'));
        }
        if ($filter) {
            $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
            $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
        }
        if ($sort_by) {
            if ($sort_by == 'pricehightolow') {
                $this->db->order_by('prd_price_min', 'desc');
            } else if ($sort_by == 'pricelowtohigh') {
                $this->db->order_by('prd_price_min', 'asc');
            }
        }
        $this->db->order_by('prd_added_on', 'desc');

        if (!$view_all) {
            $this->db->limit(6);
        } else {
            $this->db->limit($limit, $offset);
        }

        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            $stock[$key]['type'] = 'product';
            $stock[$key]['prd_id'] = encryptor($val['prd_id']);
            $stock[$key]['name'] = $val['prd_name_en'];
            $stock[$key]['price'] = $val['prd_price_min'];
            $stock[$key]['unit'] = $val['unt_unit_en'];
            $stock[$key]['moq'] = $val['prd_moq'];
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
            $stock[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
        }

        return $stock;
    }
    public function getFlashSaleStock($view_all = '', $offset = '', $limit = '', $filter = '', $sort_by = '', $flash_filter = '')
    {
        if ($filter) {
            $categories[] = $filter;
            $sub_categories = $this->common_model->getAllCategories($filter);
            $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
        }
        $stock = array();
        $this->db->select('*');
        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        $this->db->where('prd_status', 1);
        $this->db->where('prd_is_stock_prod', 1);
        if (!$flash_filter) {
            $this->db->where('prd_is_flash_sale', 1);
        }

        if ($flash_filter == "flash_stock") {
            $this->db->where('prd_is_flash_sale', 1);
        }
        if ($flash_filter == "offers_on_stock") {
            $this->db->where('prd_offer_price_min >', 0);
            $this->db->where('prd_offer_price_max >', 0);
        }
        if ($filter) {
            $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
            $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
        }
        // if ($sort_by) {
        //     if ($sort_by == 'pricehightolow') {
        //         $this->db->order_by('prd_price_min', 'desc');
        //     } else if ($sort_by == 'pricelowtohigh') {
        //         $this->db->order_by('prd_price_min', 'asc');
        //     }
        // }
        $this->db->order_by('prd_updated_on', 'desc');

        if (!$view_all) {
            $this->db->limit(6);
        } else {
            $this->db->limit($limit, $offset);
        }

        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            $stock[$key]['type'] = 'product';
            $stock[$key]['prd_id'] = encryptor($val['prd_id']);
            $stock[$key]['name'] = $val['prd_name_en'];
            $stock[$key]['org_price'] = $val['prd_price_min'];
            $stock[$key]['offer_price'] = $val['prd_offer_price_min'];
            $stock[$key]['unit'] = $val['unt_unit_en'];
            $stock[$key]['moq'] = $val['prd_moq'];
            $stock[$key]['quantity'] = $val['prd_stock_qty'];
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
            $stock[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
            $updated_time = date('Y-m-d H:i:s', strtotime($val['prd_updated_on']));
            $stock[$key]['updated'] = get_timeago($updated_time);
            $stock[$key]['sort_price'] = $val['prd_offer_price_min'] > 0 ? $val['prd_offer_price_min'] : $val['prd_price_min'];

        }
        if ($sort_by) {
            if ($sort_by == 'pricehightolow') {
                usort($stock, function ($a, $b) {
                    return $a['sort_price'] < $b['sort_price'];
                });
            }
            if ($sort_by == 'pricelowtohigh') {
                usort($stock, function ($a, $b) {
                    return $a['sort_price'] > $b['sort_price'];
                });
            }
        }
        return $stock;
    }
    public function getRecommendations($id, $view_all = '', $offset = '', $limit = '', $filter = '', $sort_by = '')
    {
        if ($filter) {
            $categories[] = $filter;
            $sub_categories = $this->common_model->getAllCategories($filter);
            $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
        }
        $rec_array = array();
        $limit = get_settings_by_key('total_keyword_for_recommendation');
        $rec = $this->db->select('rec_key_word')
            ->where('rec_user_id', $id)
            ->order_by('rec_created_at', 'desc')
            ->limit(10)
            ->get(TABLE_PREFIX . 'recommendation')->result_array();
        if ($rec) {
            $rec = implode(',', array_column($rec, 'rec_key_word')); //array_column will not work with server php version
            // $produt_ids = $this->db->query("select pkwd_product,MATCH(pkwd_val_en, pkwd_val_ar, pkwd_val_ch) AGAINST ('$rec' IN NATURAL LANGUAGE MODE) as search_score from cpnl_products_keyword where MATCH(pkwd_val_en, pkwd_val_ar,pkwd_val_ch) AGAINST ('$rec' IN NATURAL LANGUAGE MODE)>0 order by search_score  desc limit $limit")->result_array();
            $produt_ids = $this->db->query("select pkwd_product from " . TABLE_PREFIX . "products_keyword where MATCH(pkwd_val_en, pkwd_val_ar,pkwd_val_ch) AGAINST ('$rec' IN NATURAL LANGUAGE MODE) limit $limit")->result_array();
            if ($produt_ids) {
                $this->db->select('*');
                $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
                $this->db->where_in('prd_id', array_column($produt_ids, 'pkwd_product'));
                //$this->db->order_by('prd_added_on', 'desc');
                $this->db->where('prd_status', 1);
                if ($filter) {
                    $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
                    $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
                }
                if ($sort_by) {
                    if ($sort_by == 'pricehightolow') {
                        $this->db->order_by('prd_price_min', 'desc');
                    } else if ($sort_by == 'pricelowtohigh') {
                        $this->db->order_by('prd_price_min', 'asc');
                    }
                }
                $this->db->order_by('prd_added_on', 'desc');

                if (!$view_all) {
                    $this->db->limit(6);
                } else {
                    $this->db->limit($limit, $offset);
                }

                $products = $this->db->get(TABLE_PREFIX . 'products')->result_array();
                foreach ($products as $key => $val) {
                    $rec_array[$key]['type'] = 'product';
                    $rec_array[$key]['prd_id'] = encryptor($val['prd_id']);
                    $rec_array[$key]['name'] = $val['prd_name_en'];
                    $rec_array[$key]['price'] = $val['prd_price_min'];
                    $rec_array[$key]['unit'] = $val['unt_unit_en'];
                    $rec_array[$key]['moq'] = $val['prd_moq'];
                    if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                        $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                    } else {
                        $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                    }
                    $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
                    $rec_array[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
                }
            }
        }
        return $rec_array;
    }
    public function getUserDetailsFromToken($token)
    {
        return $this->db->select('usr_company,usr_username, usr_first_name,usr_last_name,usr_id, usr_active,usr_avatar,usr_email,usr_is_mail_verified')
            ->where('usr_token', $token)->get($this->tbl_users)->row_array();
    }
    public function getUserDetailsFromId($id)
    {
        return $this->db->select('usr_company,usr_username, usr_first_name,usr_last_name,usr_id, usr_active,usr_avatar,usr_email,usr_is_mail_verified,usr_pushy_token')
            ->where('usr_id', $id)->get($this->tbl_users)->row_array();
    }
    public function getProductDetails($id)
    {
        $prod_array = array();
        $products = $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT')
            ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_products . '.prd_supplier')
            ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_supplier_master . '.supm_country', 'LEFT')
            ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_supplier_master . '.supm_state', 'LEFT')
            ->where($this->tbl_products . '.prd_id', $id)->get($this->tbl_products)->row_array();
        if (!empty($products)) {
            $prod_array['type'] = 'product';
            $prod_array['prd_id'] = encryptor($products['prd_id']);
            $prod_array['name'] = $products['prd_name_en'];
            $prod_array['org_price'] = $products['prd_price_min'];
            $prod_array['offer_price'] = $products['prd_offer_price_min'] ? $products['prd_offer_price_min'] : 0;
            $prod_array['unit'] = $products['unt_unit_en'];
            $prod_array['moq'] = $products['prd_moq'];
            $prod_array['description'] = str_replace("<td>", '<td style="border: solid 1px #e4e4e4; padding: 5px 10px;">', $products['prd_desc']);

            $html = $prod_array['description'];

            $domd = new DOMDocument();
            libxml_use_internal_errors(true);
            $domd->loadHTML($html);
            libxml_use_internal_errors(false);

            $domx = new DOMXPath($domd);
            $items = $domx->query("//img[@style]");

            foreach ($items as $item) {
                $item->removeAttribute("style");
                $item->removeAttribute("width");
                $item->removeAttribute("height");
                $item->setAttribute("style", "max-width:100%");
            }

            $prod_array['description'] = $domd->saveHTML();

            $prod_array['specs'] = $this->db->select('psp_key_en as spec_name,psp_val_en as spec_value')->order_by("psp_id", "asc")->where('psp_product', $products['prd_id'])->get($this->tbl_products_specification)->result_array();
            $prod_array['supplier']['id'] = encryptor($products['supm_id']);
            $prod_array['supplier']['name'] = $products['supm_name_en'];
            $prod_array['supplier']['year'] = date('Y') - $products['supm_reg_year'];
            $prod_array['supplier']['location'] = $products['ctr_name'] . ' (' . $products['stt_name'] . ')';
            $prod_array['supplier']['response'] = '';

            // $prod_array['supplier']['360_image'] = site_url() . 'assets/uploads/panorama-shop/' . $products['supm_panoramic_image'];
            $prod_array['supplier']['360_image'] = site_url() . 'supplier/get_360/' . $prod_array['supplier']['id'];
            $sup_id = $this->common_model->getUserIdFromSupId($products['supm_id']);
            $prod_array['supplier']['supplier_user_id'] = encryptor($sup_id['usr_id']);
            $products['keyword'] = $this->db->get_where($this->tbl_products_keyword, array('pkwd_product' => $products['prd_id']))->result_array();
            $images = $this->db->select('pimg_image')->get_where($this->tbl_products_images, array('pimg_product' => $products['prd_id']))->result_array();
            $images = array_column($images, 'pimg_image');
            $defaultImg = '';
            if (isset($products['prd_default_image']) && !empty($products['prd_default_image'])) {
                $img = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $id, 'pimg_id' => $products['prd_default_image']))->row_array();
                $defaultImg = isset($img['pimg_image']) ? $img['pimg_image'] : '';
            }
            if ($images) {
                $key = array_search($defaultImg, $images);
                $new_value = $images[$key];
                unset($images[$key]);
                array_unshift($images, $new_value);
                $prod_array['images'] = preg_filter('/^/', site_url() . 'assets/uploads/product/', $images); //array_column($images, 'pimg_image');
            }
            $prod_array['related_products'] = $this->getRelatedProductByKeyWordArray($products['keyword'], $id);

        }
        return $prod_array;
    }
    public function getRelatedProductByKeyWordArray($keywords, $prd_id = '')
    {

        $rec_array = array();
        $rec = implode(',', array_column($keywords, 'pkwd_val_en'));
        $produt_ids = $this->db->query("select pkwd_product from " . TABLE_PREFIX . "products_keyword where MATCH(pkwd_val_en, pkwd_val_ar,pkwd_val_ch) AGAINST ('$rec' IN NATURAL LANGUAGE MODE)")->result_array();
        if ($produt_ids) {
            $this->db->select('*');
            $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
            $this->db->where_in('prd_id', array_column($produt_ids, 'pkwd_product'));
            $this->db->limit(4);
            $products = $this->db->get(TABLE_PREFIX . 'products')->result_array();
            foreach ($products as $key => $val) {
                if ($val['prd_id'] == $prd_id) {
                    continue;
                }

                $rec_array[$key]['type'] = 'product';
                $rec_array[$key]['prd_id'] = encryptor($val['prd_id']);
                $rec_array[$key]['name'] = $val['prd_name_en'];
                $rec_array[$key]['price'] = $val['prd_price_min'];
                $rec_array[$key]['unit'] = $val['unt_unit_en'];
                $rec_array[$key]['moq'] = $val['prd_moq'];
                if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                    $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                } else {
                    $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                }
                $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
                $rec_array[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
            }
        }
        return array_values($rec_array);
    }
    public function getConfig()
    {
        $captcha = array();
        $i = 1;
        foreach (getCiCaptcha(6) as $key => $cap) {
            $captcha[$i]['id'] = 'c' . $cap['time'];
            $captcha[$i]['image'] = site_url() . 'assets/uploads/captcha/' . $cap['filename'];
            $i++;
        }
        $data['captcha'] = array_values($captcha);
        $data['countries'] = $this->getCountries();
        return $data;
    }
    public function getCountries()
    {
        return $this->db->select('ctr_id,ctr_name')->where('ctr_status', 1)->get($this->tbl_countries)->result_array();
    }
    public function getStates($stt_country_id = '')
    {
        return $this->db->select('stt_id,stt_name')->where('stt_country_id', $stt_country_id)->where('stt_status', 1)->get($this->tbl_states)->result_array();
    }
    public function placeOrder($data)
    {
        if (!empty($data)) {
            $this->db->insert($this->tbl_products_order, $data);
            $lastInsertId = $this->db->insert_id();
            generate_log(array(
                'log_title' => 'New order',
                'log_desc' => 'New order generated',
                'log_controller' => strtolower(__CLASS__),
                'log_action' => 'C',
                'log_ref_id' => $lastInsertId,
                'log_added_by' => $data['ord_buyer'],
            ));
            return true;
        }
        return false;
    }
    public function favouriteCount($usr_id)
    {
        $sup = $this->db->where('fav_consign','SUP')
                        ->where('fav_added_by',$usr_id)
                        ->where('supm_status',1)
                        ->join($this->tbl_supplier_master,'supm_id=fav_consign_id','RIGHT')
                        ->count_all_results($this->tbl_favorite);
                        
        $prd = $this->db->where('fav_consign','PRD')
                        ->where('fav_added_by',$usr_id)
                        ->where('prd_status',1)
                        ->join($this->tbl_products,'prd_id=fav_consign_id','RIGHT')
                        ->count_all_results($this->tbl_favorite);
       $arr['sup_count'] = $sup;
       $arr['prd_count'] = $prd;
       return $arr;


    }
    public function getAllProducts($offset, $limit, $cat_id = '', $sup_id = '', $filter = '', $sort_by = '')
    {
        if ($cat_id) {
            $categories[] = $cat_id;
            $sub_categories = $this->common_model->getAllCategories($cat_id);
            $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
        }
        if ($filter) {
            $categories[] = $filter;
            $sub_categories = $this->common_model->getAllCategories($filter);
            $categories = array_merge(array_column($sub_categories, 'cat_id'), $categories);
        }
        $latest = array();
        $this->db->select('*');

        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        $this->db->where('prd_status', 1);
        if ($sup_id) {
            $this->db->where('prd_supplier', encryptor($sup_id, 'D'));
        }
        if ($cat_id) {
            $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
            $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
        }
        if ($filter) {
            $this->db->join($this->tbl_products_categories, $this->tbl_products_categories . '.pcat_product = ' . $this->tbl_products . '.prd_id');
            $this->db->where_in($this->tbl_products_categories . '.pcat_category', $categories);
        }

        if ($sort_by) {
            if ($sort_by == 'pricehightolow') {
                $this->db->order_by('prd_price_min', 'desc');
            } else if ($sort_by == 'pricelowtohigh') {
                $this->db->order_by('prd_price_min', 'asc');
            }
        }
        $this->db->order_by('prd_added_on', 'desc');
        $this->db->limit($limit, $offset);

        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            $latest[$key]['type'] = 'product';
            $latest[$key]['prd_id'] = encryptor($val['prd_id']);
            $latest[$key]['name'] = $val['prd_name_en'];
            $latest[$key]['price'] = $val['prd_price_min'];
            $latest[$key]['unit'] = $val['unt_unit_en'];
            $latest[$key]['moq'] = $val['prd_moq'];
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
            $latest[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
        }

        return $latest;
    }
    public function getSuppliers($id = '', $offset = '', $limit = '', $uid = '')
    {

        $sup = array();
        $profile = array();
        $this->db->join($this->tbl_users, $this->tbl_users . '.usr_supplier = ' . $this->tbl_supplier_master . '.supm_id', 'LEFT')
            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
            ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_supplier_master . '.supm_country', 'LEFT')
            ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_supplier_master . '.supm_state', 'LEFT')
            ->where('supm_status', 1);
        if ($id) {
            $this->db->where('supm_id', $id);
        } else {
            $this->db->limit($limit, $offset);
        }
        $supliers = $this->db->where('group_id', 2)
            ->get($this->tbl_supplier_master)->result_array();
        foreach ($supliers as $key => $val) {

            $sup_id = $this->common_model->getUserIdFromSupId($val['supm_id']);

            if (!$id) {
                $sup[$key]['type'] = 'supplier';
                $sup[$key]['sup_id'] = encryptor($val['supm_id']);
                $sup[$key]['name'] = $val['supm_name_en'];
                $sup[$key]['year'] = date('Y') - $val['supm_reg_year'];
                $sup[$key]['location'] = $val['ctr_name'] . ' (' . $val['stt_name'] . ')';

                $sup[$key]['sup_email'] = $val['supm_email'];
                $sup[$key]['reg_year'] = $val['supm_reg_year'];
                $sup[$key]['state'] = $val['stt_name'];
                $sup[$key]['country'] = $val['ctr_name'];
                $sup[$key]['address'] = $val['supm_address_en'];
                $sup[$key]['room'] = $val['supm_room_number'];

                $sup[$key]['supplier_user_id'] = encryptor($sup_id['usr_id']);
                $sup[$key]['response'] = '';
                $sup[$key]['markets'] = 'Market 1,Market 2,Market 3';
                if (isset($val['supm_default_image']) && !empty($val['supm_default_image'])) {
                    $images = $this->db->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
                } else {
                    $images = $this->db->limit(1)->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id']))->row_array();
                }
                $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
                $sup[$key]['image'] = site_url() . 'assets/uploads/shops/' . $defaultImg;
            } else {
                $sup['type'] = 'supplier';
                $sup['sup_id'] = encryptor($val['supm_id']);
                $sup['name'] = $val['supm_name_en'];
                $sup['year'] = date('Y') - $val['supm_reg_year'];
                $sup['location'] = $val['ctr_name'] . ' (' . $val['stt_name'] . ')';

                $sup['sup_email'] = $val['supm_email'];
                $sup['reg_year'] = $val['supm_reg_year'];
                $sup['state'] = $val['stt_name'];
                $sup['country'] = $val['ctr_name'];
                $sup['address'] = $val['supm_address_en'];
                $sup['room'] = $val['supm_room_number'];

                $sup['supplier_user_id'] = encryptor($sup_id['usr_id']);
                $sup['logo'] = site_url() . 'assets/uploads/avatar/' . $val['usr_avatar'];

                $banner = array();
                $banners = $this->db->select('sbi_image')->where('sbi_status', 1)->where('sbi_type', 2)->where('sbi_supplier', $val['supm_id'])->order_by('sbi_is_default', 'desc')->get($this->supplier_banner_images)->result_array();
                if ($banners) {
                    $banner = preg_filter('/^/', site_url() . 'assets/uploads/home_banner/', array_column($banners, 'sbi_image'));
                }
                $sup['banner'] = $banner;
                // $sup['banner'] = array(site_url() . 'assets/uploads/home_banner/' . $val['supm_home_banner']);
                // $sup['360_image'] = site_url() . 'assets/uploads/panorama-shop/' . $val['supm_panoramic_image'];
                $sup['360_image'] = site_url() . 'supplier/get_360/' . $sup['sup_id'];
                //$sup['address'] = $val['supm_address_en'];
                $sup['company_name'] = $val['supm_name_en'];
                $sup['website'] = $val['supm_website'];
                $sup['website_on_fujeeka'] = $val['supm_domain_prefix'] . '.fujeeka.com';
                $sup['is_fav'] = 0;
                $sup['is_follow'] = 0;
                if ($uid) {
                    $isFav = $this->db->get_where($this->tbl_favorite, array(
                        'fav_consign' => 'SUP', 'fav_consign_id' => $val['supm_id'], 'fav_added_by' => $uid))->row_array();
                    // debug($this->db->last_query());
                    $isFol = $this->db->get_where($this->tbl_supplier_followers, array(
                        'sfol_supplier' => $val['supm_id'], 'sfol_followed_by' => $uid))->row_array();
                    if ($isFav) {
                        $sup['is_fav'] = 1;
                    }
                    if ($isFol) {
                        $sup['is_follow'] = 1;
                    }
                }

                $categories = $this->getSupplierCategories($id);
                foreach ($categories as $key => $cat) {
                    $categories[$key]['cat_image'] = site_url() . 'assets/uploads/category/' . $cat['cat_image'];
                }
                $sup['categories'] = $categories;
                $sup['featured'] = $this->getSupplierProducts(8, '', $id);
                $sup['our_product'] = $this->getSupplierProducts(8, '', $id, 'our');
                $sup['latest'] = $this->getSupplierProducts(8, '', $id, 'latest');
                $sup['stock'] = $this->getSupplierProducts(8, '', $id, 'stock');
                $shop_images = $this->db->get_where($this->tbl_supplier_shop_images, array('ssi_supplier' => $val['supm_id']))->result_array();
                $profile['banner_video_link'] = $val['supm_official_video'] != 'none' ? $val['supm_official_video'] : '';
                if ($shop_images) {
                    $profile['banner_images'] = preg_filter('/^/', site_url() . 'assets/uploads/shops/', array_column($shop_images, 'ssi_image'));
                }
                $profile['about'] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.";
                $data['supplier_home'] = $sup;
                $data['supplier_profile'] = $profile;
                return $data;
            }

        }
        return $sup;

    }
    public function getSupplierCategories($supId)
    {
        return $this->db->select('cat_id,cat_title,cat_image')
            ->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . $this->tbl_supplier_buildings_cate_assoc . '.sbca_category')
            ->where($this->tbl_supplier_buildings_cate_assoc . '.sbca_supplier', $supId)
            ->get($this->tbl_supplier_buildings_cate_assoc)->result_array();
    }
    public function getSupplierProducts($limit = '', $offset = '', $sup = '', $type = '')
    {
        $fea = array();
        $this->db->select('*');
        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        $this->db->where('prd_status', 1);
        $this->db->where('prd_supplier', $sup);
        if ($type == 'stock') {
            $this->db->where('prd_is_stock_prod', 1);
        }
        if ($type != 'our') {
            $this->db->order_by('prd_added_on', 'DESC');
        }

        if ($limit) {
            $this->db->limit($limit);
        }
        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            $fea[$key]['type'] = 'product';
            $fea[$key]['prd_id'] = encryptor($val['prd_id']);
            $fea[$key]['name'] = $val['prd_name_en'];
            $fea[$key]['price'] = $val['prd_price_min'];
            $fea[$key]['unit'] = $val['unt_unit_en'];
            $fea[$key]['moq'] = $val['prd_moq'];
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
            $fea[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
        }
        return $fea;
    }
    public function getHomeBanners()
    {
        $banner = array();
        $banners = $this->db->select('bnr_image')->where('bnr_status', 1)->where('bnr_category', 2)->order_by('bnr_order', 'asc')->get($this->tbl_banner)->result_array();
        $ads = $this->db->select('add_url,add_image')->order_by('add_order', 'asc')->where('add_show_on_home_page', 1)->get($this->tbl_advertisement)->result_array();
        if ($banners) {
            $banner['banners'] = preg_filter('/^/', site_url() . 'assets/uploads/banner/', array_column($banners, 'bnr_image'));
        }
        foreach ($ads as $key => $ad) {
            $ads[$key]['add_image'] = site_url() . 'assets/uploads/advt/' . $ad['add_image'];
        }
        $banner['ads'] = $ads;
        return $banner;
    }
    public function getCategories($parent_id = '')
    {
        $this->db->select('cat_id,cat_title');
        $this->db->where('cat_status', 1);
        if ($parent_id) {
            $this->db->where('cat_parent', $parent_id);
            return $this->db->get($this->tbl_category)->result_array();
        } else {
            $this->db->where('cat_parent', 0);
        }
        $category = $this->db->get($this->tbl_category)->result_array();

        foreach ($category as $key => $val) {
            $category[$key]['sub_categories'] = $this->getCategories($val['cat_id']);
        }
        return $category;

    }
    public function getMarkets()
    {
        $this->db->select('mar_id,mar_name');
        $this->db->where('mar_status', 1);
        return $this->db->get($this->tbl_market)->result_array();
    }
    public function getUnits()
    {
        $this->db->select('unt_id as id,unt_unit_name_en as name ,unt_unit_en as abbr');
        return $this->db->get(TABLE_PREFIX . 'products_units')->result_array();
    }
    public function insertRfq($data)
    {
        $data = array_filter($data);
        $this->db->insert(TABLE_PREFIX . 'rfq', $data);
        $rfqId = $this->db->insert_id();

        $categorySupprs = $this->db->select('GROUP_CONCAT(sbca_supplier) AS sbca_supplier')->where('sbca_category', $data['rfq_category'])
            ->get(TABLE_PREFIX . 'supplier_buildings_cate_assoc')->row()->sbca_supplier;

        $userIdAssocSupplier = $this->db->select('usr_id')->join($this->tbl_users_groups, 'user_id=usr_id')->where('group_id', 2)->where_in('usr_supplier', explode(',', $categorySupprs))
            ->get($this->tbl_users)->result_array();

        foreach ((array) $userIdAssocSupplier as $key => $value) {
            $this->db->insert($this->tbl_rfq_notification, array(
                'rfqn_rfq_id' => $rfqId,
                'rfqn_supplier' => $value['usr_id'],
                'rfqn_comments' => $data['rfq_requirment'],
                'rfqn_added_by' => $data['rfq_user'],
            ));
        }
        generate_log(array(
            'log_title' => 'New RFQ generated',
            'log_desc' => 'New order generated',
            'log_controller' => strtolower(__CLASS__),
            'log_action' => 'C',
            'log_ref_id' => $rfqId,
            'log_added_by' => $data['rfq_user'],
        ));
        return true;
    }
    public function getFeeds($usr_id, $type, $offset, $limit)
    {
        $this->db->select('sfol_supplier')->where('sfol_followed_by', $usr_id);
        $follow_sup_ids = $this->db->get($this->tbl_supplier_followers)->result_array();
        $follow_sup_ids = array_column($follow_sup_ids, 'sfol_supplier');
        if ($type == "following") {
            if ($follow_sup_ids) {
                $data['following'] = $this->getSupFeeds($offset, $limit, $follow_sup_ids, 'following');
            } else {
                $data['following'] = array();
            }
        }
        if ($type == "discover") {
            $data['discover'] = $this->getSupFeeds($offset, $limit, $follow_sup_ids, 'discover');
        }
        if ($type == "both") {
            if ($follow_sup_ids) {
                $data['following'] = $this->getSupFeeds($offset, $limit, $follow_sup_ids, 'following');
            } else {
                $data['following'] = array();
            }
            $data['discover'] = $this->getSupFeeds($offset, $limit, $follow_sup_ids, 'discover');
        }
        return $data;

    }
    public function getSupFeeds($offset, $limit, $ids, $type)
    {
        $feed = array();
        $this->db->select('supm_id,supm_name_en,supm_default_image,ctr_name,stt_name,f_text,f_prd_id,prd_id,prd_name_en,prd_price_min,prd_offer_price_min
        ,prd_moq,prd_default_image,unt_unit_en')
            ->join($this->tbl_supplier_master, 'supm_id = f_sup_id', 'LEFT')
            ->join($this->tbl_countries, 'ctr_id = supm_country', 'LEFT')
            ->join($this->tbl_states, 'stt_id = supm_state', 'LEFT')
            ->join($this->tbl_products, 'prd_id = f_prd_id', 'LEFT')
            ->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT')
            ->where('supm_status', 1)
            ->where('f_status', 1)
            ->order_by('f_added_on', 'desc');
        if ($type == "following") {
            $this->db->where_in('supm_id', $ids);
        } else {
            if ($ids) {
                $this->db->where_not_in('supm_id', $ids);
            }
        }

        $res = $this->db->limit($limit, $offset)
            ->get($this->tbl_feeds)->result_array();

        foreach ($res as $key => $val) {
            $feed[$key]['sup_id'] = encryptor($val['supm_id']);
            $feed[$key]['name'] = $val['supm_name_en'];
            $feed[$key]['location'] = $val['ctr_name'] . ' (' . $val['stt_name'] . ')';
            $feed[$key]['org_price'] = $val['prd_price_min'];
            $feed[$key]['offer_price'] = $val['prd_offer_price_min'] ? $val['prd_offer_price_min'] : 0;
            if (isset($val['supm_default_image']) && !empty($val['supm_default_image'])) {
                $simages = $this->db->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
            } else {
                $simages = $this->db->limit(1)->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id']))->row_array();
            }
            $sdefaultImg = isset($simages['ssi_image']) ? $simages['ssi_image'] : '';
            $feed[$key]['sup_image'] = site_url() . 'assets/uploads/shops/' . $sdefaultImg;

            if (isset($val['prd_default_image']) && !empty($val['prd_default_image'])) {
                $pimages = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $pimages = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $pdefaultImg = isset($pimages['pimg_image']) ? $pimages['pimg_image'] : '';
            $feed[$key]['feed_text'] = $val['f_text'];
            $feed[$key]['feed_product_id'] = encryptor($val['f_prd_id']);
            $feed[$key]['feed_product_name'] = $val['prd_name_en'];
            $feed[$key]['feed_product_moq'] = $val['prd_moq'];
            $feed[$key]['feed_product_unit'] = $val['unt_unit_en'];
            $feed[$key]['prd_image'] = site_url() . 'assets/uploads/product/' . $pdefaultImg;

        }
        return $feed;

    }
    public function addToFavourite($data)
    {
        $this->db->where($data)->delete($this->tbl_favorite);
        return $this->db->insert($this->tbl_favorite, $data);
    }
    public function getSearchResults($type, $key, $offset, $limit)
    {
        $search = array();
        if ($type == "product" || $type == "all") {
            $search['product'] = $this->getSearchProducts($key, $offset, $limit);
        }
        if ($type == "supplier" || $type == "all") {
            $search['supplier'] = $this->getSearchSuppliers($key, $offset, $limit);
        }
        return $search;
    }
    public function getSearchProducts($query, $offset, $limit, $from = '')
    {
        $search = array();
        $this->db->select('*');

        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        $this->db->where('prd_status', 1);
        // $this->db->like('prd_name_en', $query, 'after');
        $this->db->like('prd_name_en', $query);
        $this->db->order_by('prd_added_on', 'desc');
        $this->db->limit($limit, $offset);

        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            $search[$key]['type'] = 'product';
            $search[$key]['prd_id'] = encryptor($val['prd_id']);
            $search[$key]['name'] = $val['prd_name_en'];
            $search[$key]['price'] = $val['prd_price_min'];
            $search[$key]['unit'] = $val['unt_unit_en'];
            $search[$key]['moq'] = $val['prd_moq'];
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
            $search[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
        }

        return $search;
    }
    public function getSearchSuppliers($query, $offset, $limit, $from = '')
    {
        $search = array();
        $this->db->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_products . '.prd_supplier', 'LEFT')
            ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_supplier_master . '.supm_country', 'LEFT')
            ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_supplier_master . '.supm_state', 'LEFT')
            ->where('prd_status', 1)
        // ->like('prd_name_en', $query, 'after');
            ->like('supm_name_en', $query)
            ->limit($limit, $offset)
            ->group_by('prd_supplier');

        $supliers = $this->db->get($this->tbl_products)->result_array();
        foreach ($supliers as $key => $val) {

            $this->db->select($this->tbl_category . '.cat_id,cat_title');
            $this->db->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . $this->tbl_supplier_buildings_cate_assoc . '.sbca_category');
            $this->db->where('sbca_supplier', $val['supm_id']);
            $catg = $this->db->get($this->tbl_supplier_buildings_cate_assoc)->result_array();
            $search[$key]['type'] = 'supplier';
            $search[$key]['sup_id'] = encryptor($val['supm_id']);
            $search[$key]['name'] = $val['supm_name_en'];
            $search[$key]['country'] = $val['ctr_name'];
            $search[$key]['location'] = $val['ctr_name'] . ' (' . $val['stt_name'] . ')';
            $search[$key]['year'] = date('Y') - $val['supm_reg_year'];
            $search[$key]['main_products'] = implode(', ', array_column($catg, 'cat_title'));
            if (isset($val['supm_default_image']) && !empty($val['supm_default_image'])) {
                $images = $this->db->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id']))->row_array();
            }
            $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
            $search[$key]['image'] = site_url() . 'assets/uploads/shops/' . $defaultImg;
        }
        return $search;
    }
    public function getFavourites($usr_id, $type, $offset, $limit)
    {
        if ($type == 'SUP') {
            $fav['supplier'] = array();
        } else {
            $fav['product'] = array();
        }
        $ids = $this->db->select('GROUP_CONCAT(fav_consign_id) as ids')
            ->where('fav_added_by', $usr_id)
            ->where('fav_consign', $type)
            ->order_by('fav_added_on', 'desc')
            ->limit($limit, $offset)
            ->get($this->tbl_favorite)->row()->ids;
        if ($ids) {
            if ($type == 'SUP') {
                $fav['supplier'] = $this->getFavouriteSupplier(explode(',', $ids));
            } else {
                $fav['product'] = $this->getFavouriteProducts(explode(',', $ids));
            }
        }
        return $fav;
    }
    public function getFavouriteSupplier($ids)
    {
        $sup = array();
        $this->db->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_supplier_master . '.supm_country', 'LEFT')
            ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_supplier_master . '.supm_state', 'LEFT')
            ->where('supm_status', 1);
        $this->db->where_in('supm_id', $ids);
        $supliers = $this->db->get($this->tbl_supplier_master)->result_array();
        foreach ($supliers as $key => $val) {
            $sup[$key]['type'] = 'supplier';
            $sup[$key]['sup_id'] = encryptor($val['supm_id']);
            $sup[$key]['name'] = $val['supm_name_en'];
            $sup[$key]['country'] = $val['ctr_country_code'];
            $sup[$key]['year'] = date('Y') - $val['supm_reg_year'];
            if (isset($val['supm_default_image']) && !empty($val['supm_default_image'])) {
                $images = $this->db->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id']))->row_array();
            }
            $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
            $sup[$key]['image'] = site_url() . 'assets/uploads/shops/' . $defaultImg;
        }
        return $sup;
    }
    public function getFavouriteProducts($ids)
    {
        $pro = array();
        $this->db->select('*');
        $this->db->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT');
        $this->db->where('prd_status', 1);
        $this->db->order_by('prd_added_on', 'desc');
        $this->db->where_in('prd_id', $ids);
        $products = $this->db->get($this->tbl_products)->result_array();
        foreach ($products as $key => $val) {
            $pro[$key]['type'] = 'product';
            $pro[$key]['prd_id'] = encryptor($val['prd_id']);
            $pro[$key]['name'] = $val['prd_name_en'];
            $pro[$key]['price'] = $val['prd_price_min'];
            $pro[$key]['unit'] = $val['unt_unit_en'];
            $pro[$key]['moq'] = $val['prd_moq'];
            if (isset($products[$key]['prd_default_image']) && !empty($products[$key]['prd_default_image'])) {
                $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $products[$key]['images'] = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $defaultImg = isset($products[$key]['images']['pimg_image']) ? $products[$key]['images']['pimg_image'] : '';
            $pro[$key]['image'] = site_url() . 'assets/uploads/product/' . $defaultImg;
        }

        return $pro;
    }
    public function activateUserAccount($usr_id, $otp)
    {
        $this->db->where('usr_id', $usr_id)->where('usr_otp', $otp)->update($this->tbl_users, array('usr_active' => 1));
        return ($this->db->affected_rows() == 1) ? true : false;
    }
    public function checkIfValueExists($data)
    {
        $return = $this->db->like('usr_email', $data['usr_email'], 'none')->get($this->tbl_users)->row_array();
        return (empty($return)) ? true : false;
    }
    public function updateOtpByMail($email, $type)
    {
        $otp = rand(100000, 999999);
        $this->db->where('usr_email', $email)->update($this->tbl_users, array($type => $otp));
        return $otp;
    }
    public function getInbox($usr_id)
    {
        $user = array();
        $qry = "select usr_id,usr_first_name,usr_last_name,usr_avatar,c_time from cpnl_chats join (select user, max(c_time) m from ((select c_to_id user, c_time from cpnl_chats where c_from_id=$usr_id ) union (select c_from_id user, c_time from cpnl_chats where c_to_id=$usr_id) ) cpnl_chats1 group by user) cpnl_chats2 on ((c_from_id=$usr_id and c_to_id=user) or (c_from_id=user and c_to_id=$usr_id)) and (c_time = m) join cpnl_users on usr_id = user order by c_time desc";
        $res = $this->db->query($qry)->result_array();

        foreach ($res as $key => $val) {
            if ($val['usr_id']) {
                $time = DateTime::createFromFormat('U.u', $val['c_time']);
                $user[$key]['chat_with'] = $val['usr_id'] == 1 ? 'admin' : encryptor($val['usr_id']);
                // $user[$key]['chat_with'] = encryptor($val['usr_id']);
                $user[$key]['name'] = $val['usr_id'] == 1 ? 'Fujeeka' : $val['usr_first_name'] . ' ' . $val['usr_last_name'];
                // $user[$key]['name'] = $val['usr_first_name'] . ' ' . $val['usr_last_name'];
                $user[$key]['image'] = site_url() . 'assets/uploads/avatar/' . $val['usr_avatar'];
                $user[$key]['last_time'] = chat_time($time->format("Y-m-d H:i:s"));
            }
        }
        return $user;
    }
    public function inquiryCount($usr_id)
    {

        return $this->db->where('ord_buyer', $usr_id)->count_all_results($this->tbl_products_order);
    }
    public function loadChats($to, $usr_id, $offset, $limit)
    {
        $chat = array();
        $where = "c_from_id = $usr_id and c_to_id = $to or(c_from_id = $to and c_to_id = $usr_id)";
        $res = $this->db->where($where)->order_by('c_time', 'desc')->limit($limit, $offset)->get(TABLE_PREFIX . 'chats')->result_array();

        // debug($res);
        foreach ($res as $key => $val) {
            $time = DateTime::createFromFormat('U.u', $val['c_time']);
            $ext = end(explode('.', $val['c_attachment']));
            $chat[$key]['type'] = 'recieved';
            if ($val['c_from_id'] == $usr_id) {
                $chat[$key]['type'] = 'sent';
            }

            $attach = 0;
            if ($val['c_attachment']) {
                $attach = 1;
            }
            $chat[$key]['id'] = encryptor($val['c_id']);
            $chat[$key]['content'] = $val['c_content'];
            $chat[$key]['attachment_link'] = '';
            $chat[$key]['attachment_preview'] = '';
            if ($val['c_is_prd_details']) {
                $chat[$key]['attachment_link'] = site_url() . 'assets/' . $val['c_attachment'];
                $chat[$key]['attachment_preview'] = site_url() . 'assets/' . $val['c_attachment'];
            } else if ($attach) {
                $chat[$key]['attachment_link'] = site_url() . 'assets/uploads/chats/' . $val['c_attachment'];
                $chat[$key]['attachment_preview'] = site_url() . 'assets/images/attachment.jpg';
                if ($ext == 'gif' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                    $chat[$key]['attachment_preview'] = site_url() . 'assets/uploads/chats/' . $val['c_attachment'];
                }
            }

            $chat[$key]['time'] = $time->format("Y-m-d H:i:s");
            $chat[$key]['is_attachment'] = $attach;
            $chat[$key]['is_prd_details'] = $val['c_is_prd_details'];
        }
        return array_reverse($chat);

    }
    public function getUserDetails($id)
    {
        $user = $this->db->select('usr_first_name,usr_last_name,usr_avatar,usr_supplier')->get_where($this->tbl_users, array('usr_id' => $id))->row_array();
        return $user;
    }
    public function getFavIds($usr_id)
    {
        $this->db->select('fav_consign_id')->where('fav_consign', 'SUP')->where('fav_added_by', $usr_id)->order_by('fav_added_on', 'desc');
        $fav_sup_ids = $this->db->get($this->tbl_favorite)->result_array();
        $sup_ids = array();
        foreach ($fav_sup_ids as $key => $val) {
            $sup_ids[$key] = encryptor($val['fav_consign_id']);
        }
        $fav['supplier_id'] = $sup_ids;

        $this->db->select('fav_consign_id')->where('fav_consign', 'PRD')->where('fav_added_by', $usr_id)->order_by('fav_added_on', 'desc');
        $fav_prd_ids = $this->db->get($this->tbl_favorite)->result_array();
        $prd_ids = array();
        foreach ($fav_prd_ids as $key => $val) {
            $prd_ids[$key] = encryptor($val['fav_consign_id']);
        }
        $fav['product_id'] = $prd_ids;
        return $fav;

    }
    public function updatePushyToken($pushy_token, $col, $val)
    {
        if ($pushy_token) {
            return $this->db->where($col, $val)->update($this->tbl_users, array('usr_pushy_token' => $pushy_token));
        }

    }
    public function getRfqList($usr_id)
    {
        $rfq = array();
        $res = $this->db->where('rfq_user', $usr_id)->order_by('rfq_added_on', 'desc')->get(TABLE_PREFIX . 'rfq')->result_array();
        foreach ($res as $key => $val) {
            $rfq[$key]['id'] = $val['rfq_id'];
            $rfq[$key]['product'] = $val['rfq_product_name'];
            $rfqdate = strtotime($val['rfq_added_on']);
            $rfq[$key]['date'] = date('d/m/Y', $rfqdate);
            $rfq[$key]['quote'] = $this->getRfqQuoteCount($val['rfq_id'], $usr_id);
        }
        return $rfq;
    }
    public function getRfqQuoteCount($rfq_id, $usr_id)
    {
        $res = $this->db->where('rfqn_rfq_id', $rfq_id)->where('rfqn_added_by !=', $usr_id)->group_by('rfqn_added_by')->get(TABLE_PREFIX . 'rfq_notification')->result_array();
        return count($res);
    }
    public function updateMailStatus($id)
    {
        return $this->db->where('usr_id', $id)->update($this->tbl_users, array('usr_is_mail_verified' => 1));
    }
    public function changeEmail($mail, $id, $otp)
    {
        $this->db->where('usr_id', $id)->update($this->tbl_users, array('usr_email' => $mail, 'usr_otp' => $otp, 'usr_is_mail_verified' => 0));
        return ($this->db->affected_rows() == 1) ? true : false;
    }
    public function getRfqDetails($rfq_id)
    {
        $details = array();
        $res = $this->db->select('rps_rfq_id,supm_id,supm_name_en,prd_id,prd_name_en,prd_price_min
        ,prd_moq,prd_default_image,unt_unit_en,rfq_qty,rfq_validity,rfq_requirment,rfq_product_name,rfq_added_on')
            ->join($this->tbl_users, 'usr_id = rps_supplier_id', 'LEFT')
            ->join(TABLE_PREFIX . 'users_groups', 'user_id = usr_id', 'LEFT')
            ->join($this->tbl_supplier_master, 'supm_id = usr_supplier', 'LEFT')
            ->join($this->tbl_products, 'prd_id = rps_prd_id_sent', 'LEFT')
            ->join(TABLE_PREFIX . 'products_units', 'unt_id = prd_unit', 'LEFT')
            ->join(TABLE_PREFIX . 'rfq', 'rfq_id = rps_rfq_id', 'LEFT')
            ->where('rps_rfq_id', $rfq_id)
            ->where('group_id', 2)
            ->group_by('rps_supplier_id')
            ->order_by('rps_id', 'desc')
            ->get($this->tbl_rfq_products_send)->result_array();

        if ($res) {
            $details['rfq']['rfq_id'] = $res[0]['rps_rfq_id'];
            $details['rfq']['rfq_product'] = $res[0]['rfq_product_name'];
            $details['rfq']['quantity'] = $res[0]['rfq_qty'];
            $details['rfq']['expiry'] = $res[0]['rfq_validity'];
            $rfqdate = strtotime($res[0]['rfq_added_on']);
            $details['rfq']['updated'] = date('Y-m-d', $rfqdate);
            $details['rfq']['requirement'] = $res[0]['rfq_requirment'];
            foreach ($res as $key => $val) {
                $details['quotes'][$key]['sup_id'] = encryptor($val['supm_id']);
                $details['quotes'][$key]['sup_name'] = $val['supm_name_en'];
                if (isset($val['prd_default_image']) && !empty($val['prd_default_image'])) {
                    $pimages = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
                } else {
                    $pimages = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
                }
                $pdefaultImg = isset($pimages['pimg_image']) ? $pimages['pimg_image'] : '';
                $details['quotes'][$key]['product_id'] = encryptor($val['prd_id']);
                $details['quotes'][$key]['product_name'] = $val['prd_name_en'];
                $details['quotes'][$key]['product_moq'] = $val['prd_moq'];
                $details['quotes'][$key]['product_unit'] = $val['unt_unit_en'];
                $details['quotes'][$key]['product_image'] = site_url() . 'assets/uploads/product/' . $pdefaultImg;
            }
        } else {
            $res1 = $this->db->where('rfq_id', $rfq_id)->get(TABLE_PREFIX . 'rfq')->result_array();
            if ($res1) {
                $details['rfq']['rfq_id'] = $rfq_id;
                $details['rfq']['rfq_product'] = $res1[0]['rfq_product_name'];
                $details['rfq']['quantity'] = $res1[0]['rfq_qty'];
                $details['rfq']['expiry'] = $res1[0]['rfq_validity'];
                $details['rfq']['requirement'] = $res1[0]['rfq_requirment'];
                $rfqdate = strtotime($res1[0]['rfq_added_on']);
                $details['rfq']['updated'] = date('Y-m-d', $rfqdate);
                $details['quotes'] = array();
            }
        }
        if (!$details) {
            $details['rfq'] = array();
            $details['quotes'] = array();
        }
        return $details;
    }
    public function marketConfig()
    {
        $data['markets'] = $this->db->select('mar_id,mar_name')->where('mar_status', 1)->order_by('mar_id', 'DESC')->get($this->tbl_market)->result_array();
        $data['buildings'] = $this->db->select('bld_id,bld_title')->order_by('bld_id', 'DESC')->get($this->tbl_buildins)->result_array();
        $this->db->select('cat_id,cat_title');
        $this->db->where('cat_status', 1);
        $data['categories'] = $this->db->get($this->tbl_category)->result_array();
        return $data;
    }
    public function getMarketSuppliers($limit, $start, $category, $market, $building)
    {

        $suppliers = array();
        $this->db->select('supm_name_en,supm_id,supm_default_image');
        $this->db->join($this->tbl_users, 'usr_supplier = supm_id', 'RIGHT');
        $this->db->join($this->tbl_users_groups, 'user_id = usr_id', 'RIGHT');
        $this->db->where('group_id', 2);
        $this->db->where('usr_active', 1);
        $this->db->limit($limit, $start);
        $this->db->order_by('supm_added_on', 'desc');
        $this->db->where('supm_status', 1);
        if ($market) {
            $this->db->where('supm_market', $market);
        }
        if ($building) {
            $this->db->where('supm_building', $building);
        }
        $supplier = $this->db->get($this->tbl_supplier_master)->result_array();
        foreach ($supplier as $key => $val) {
            $this->db->select($this->tbl_category . '.cat_id,cat_title');
            $this->db->join($this->tbl_category, $this->tbl_category . '.cat_id = ' . $this->tbl_supplier_buildings_cate_assoc . '.sbca_category');
            $this->db->where('sbca_supplier', $val['supm_id']);
            $catg = $this->db->get($this->tbl_supplier_buildings_cate_assoc)->result_array();

            if ($category) {
                if (!in_array($category, array_column($catg, 'cat_id'))) {
                    unset($supplier[$key]);
                    continue;
                }
            }
            $suppliers[$key]['type'] = 'supplier';
            $suppliers[$key]['sup_id'] = encryptor($val['supm_id']);
            $suppliers[$key]['name'] = $val['supm_name_en'];
            $suppliers[$key]['360_image'] = site_url() . 'supplier/get_360/' . $suppliers[$key]['sup_id'];
            $cat = implode(', ', array_column($catg, 'cat_title'));
            $suppliers[$key]['categories'] = $cat;
            if (isset($suppliers[$key]['supm_default_image']) && !empty($suppliers[$key]['supm_default_image'])) {
                $images = $this->db->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id']))->row_array();
            }
            $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
            $suppliers[$key]['default_image'] = site_url() . 'assets/uploads/shops/' . $defaultImg;
        }
        return array_values($suppliers);

    }
    public function removeFavorite($data)
    {
        $this->db->where('fav_consign', $data['fav_consign'])
            ->where_in('fav_consign_id', $data['fav_consign_id'])
            ->where('fav_added_by', $data['fav_added_by'])
            ->delete($this->tbl_favorite);
        return $this->db->affected_rows();
    }
    public function addToFollow($usr_id, $sup_id)
    {
        $data['sfol_supplier'] = $sup_id;
        $data['sfol_followed_by'] = $usr_id;
        return $this->db->insert($this->tbl_supplier_followers, $data);
    }
    public function removeFollow($usr_id, $id)
    {
        $this->db->where('sfol_supplier', $id)
            ->where('sfol_followed_by', $usr_id)
            ->delete($this->tbl_supplier_followers);
        return $this->db->affected_rows();
    }
    public function getFollowings($usr_id)
    {
        $sup = array();
        $res = $this->db->select('supm_id,supm_name_en,supm_default_image,ctr_name,stt_name')
            ->join($this->tbl_supplier_master, 'supm_id=sfol_supplier')
            ->join($this->tbl_countries, 'ctr_id = supm_country', 'LEFT')
            ->join($this->tbl_states, 'stt_id = supm_state', 'LEFT')
            ->where('sfol_followed_by', $usr_id)
            ->where('supm_status', 1)
            ->order_by('sfol_added_on', 'desc')
            ->get($this->tbl_supplier_followers)
            ->result_array();
        foreach ($res as $key => $val) {
            $sup[$key]['sup_id'] = encryptor($val['supm_id']);
            $sup[$key]['name'] = $val['supm_name_en'];
            $sup[$key]['location'] = $val['ctr_name'] . ' (' . $val['stt_name'] . ')';
            if (isset($val['supm_default_image']) && !empty($val['supm_default_image'])) {
                $images = $this->db->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
            } else {
                $images = $this->db->limit(1)->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id']))->row_array();
            }
            $defaultImg = isset($images['ssi_image']) ? $images['ssi_image'] : '';
            $sup[$key]['image'] = site_url() . 'assets/uploads/shops/' . $defaultImg;
        }
        return $sup;
    }
    public function loadRfqChats($to, $usr_id, $offset, $limit)
    {
        $chat = array();
        $where = "rfqn_added_by = $usr_id and rfqn_supplier = $to or(rfqn_added_by = $to and rfqn_supplier = $usr_id)";
        $res = $this->db->where($where)->order_by('rfqn_added_on', 'desc')->limit($limit, $offset)->get(TABLE_PREFIX . 'rfq_notification')->result_array();
        foreach ($res as $key => $val) {
            $chat[$key]['type'] = 'recieved';
            if ($val['rfqn_added_by'] == $usr_id) {
                $chat[$key]['type'] = 'sent';
            }
            $chat[$key]['content'] = $val['rfqn_comments'];
            $chat[$key]['time'] = $val['rfqn_added_on'];
            $chat[$key]['id'] = encryptor($val['rfqn_id']);
        }
        return array_reverse($chat);

    }
    public function removeRfq($usr_id, $id)
    {
        $this->db->trans_start();
        $this->db->where('rfq_id', $id)
            ->where('rfq_user', $usr_id)
            ->delete(TABLE_PREFIX . 'rfq');

        $this->db->where('rfqn_rfq_id', $id)
            ->delete($this->tbl_rfq_notification);

        $this->db->where('rps_rfq_id', $id)
            ->delete($this->tbl_rfq_products_send);

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insertRfqChat($rfq_id, $usr_id, $sup_id, $content)
    {
        $data['rfqn_rfq_id'] = $rfq_id;
        $data['rfqn_supplier'] = $sup_id;
        $data['rfqn_comments'] = $content;
        $data['rfqn_added_by'] = $usr_id;
        $this->db->insert($this->tbl_rfq_notification, $data);
        return $this->db->insert_id();
    }
    public function getInquiryList($usr_id)
    {
        $details = array();
        $res = $this->db->select('ord_id,ord_added_on,prd_id,prd_name_en,prd_default_image')
            ->join($this->tbl_products, 'prd_id=ord_prod_id')
            ->where('ord_added_by', $usr_id)
            ->get($this->tbl_products_order)->result_array();
        foreach ($res as $key => $val) {
            $details[$key]['inq_id'] = encryptor($val['ord_id']);
            $details[$key]['product_name'] = $val['prd_name_en'];
            if (isset($val['prd_default_image']) && !empty($val['prd_default_image'])) {
                $pimages = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id'], 'pimg_id' => $val['prd_default_image']))->row_array();
            } else {
                $pimages = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $val['prd_id']))->row_array();
            }
            $pdefaultImg = isset($pimages['pimg_image']) ? $pimages['pimg_image'] : '';
            $details[$key]['product_image'] = site_url() . 'assets/uploads/product/' . $pdefaultImg;
            $details[$key]['date'] = $val['ord_added_on'];
        }
        return $details;
    }
    public function getInquiryDetails($id, $usr_id)
    {
        $inq = array();
        $sup_details = $this->getInquirySupDetails($id);
        if ($sup_details) {
            $inq['supplier']['id'] = encryptor($sup_details['usr_id']);
            $inq['supplier']['name'] = $sup_details['usr_first_name'];
            $inq['supplier']['image'] = site_url() . 'assets/uploads/avatar/' . $sup_details['usr_avatar'];
        }
        $res = $this->db->select('ord_id,ord_added_on,ord_message,ord_subject')
            ->where('ord_id', $id)
            ->get($this->tbl_products_order)->row_array();
        // if ($res) {
        //     $inq['comments'][0]['inq_id'] = encryptor($res['ord_id']);
        //     $inq['comments'][0]['type'] = 'sent';
        //     $inq['comments'][0]['subject'] = $res['ord_subject'];
        //     $inq['comments'][0]['description'] = $res['ord_message'];
        //     $inq['comments'][0]['time'] = $res['ord_added_on'];
        // }
        $res1 = $this->db->where('poc_order_id', $id)
            ->order_by('poc_added_on', 'asc')
            ->get($this->order_comments)
            ->result_array();
        foreach ($res1 as $key => $val) {
            $inq['comments'][$key]['type'] = 'recieved';
            if ($val['poc_from'] == $usr_id) {
                $inq['comments'][$key]['type'] = 'sent';
            }
            $inq['comments'][$key]['subject'] = $val['poc_title'];
            $inq['comments'][$key]['description'] = $val['poc_desc'];
            $inq['comments'][$key]['time'] = $val['poc_added_on'];
        }
        return $inq;
    }
    public function getInquirySupDetails($id)
    {
        return $this->db->select('usr_id,usr_first_name,usr_avatar')
            ->join($this->tbl_users, 'usr_id=ord_supplier_user_id')
            ->where('ord_id', $id)
            ->get($this->tbl_products_order)
            ->row_array();
    }
    public function getNotificationList($offset, $limit)
    {
        $res = $this->db->select('pnm_id as id ,pnm_title as title ,pnm_message as message ,pnm_sent_by,pnm_sent_on as date,usr_first_name,usr_last_name')
        // ->where('pnm_user_group', 4)
            ->join($this->tbl_users, 'usr_id=pnm_sent_by')
            ->order_by('pnm_sent_on', 'desc')
            ->limit($limit, $offset)
            ->get($this->notification)
            ->result_array();
        foreach ($res as $key => $val) {
            $res[$key]['from'] = $val['pnm_sent_by'] == 1 ? 'Fujeeka' : $val['usr_first_name'] . ' ' . $val['usr_last_name'];
            unset($res[$key]['usr_first_name']);
            unset($res[$key]['usr_last_name']);
            unset($res[$key]['pnm_sent_by']);
        }
        return $res;
    }
    public function getNotificationDetails($id)
    {
        $res = $this->db->select('pnm_id as id ,pnm_title as title ,pnm_message as message,pnm_image as image, pnm_sent_on as date,pnm_sent_by as chat_id,usr_first_name,usr_last_name')
            ->join($this->tbl_users, 'usr_id=pnm_sent_by')
            ->where('pnm_id', $id)
            ->get($this->notification)
            ->result_array();
        foreach ($res as $key => $val) {
            $res[$key]['chat_id'] = $val['chat_id'] == 1 ? 'admin' : encryptor($val['chat_id']);
            $res[$key]['from'] = $val['chat_id'] == 1 ? 'Fujeeka' : $val['usr_first_name'] . ' ' . $val['usr_last_name'];
            unset($res[$key]['usr_first_name']);
            unset($res[$key]['usr_last_name']);

            if ($val['image']) {
                $res[$key]['image'] = site_url() . 'assets/uploads/notification/' . $val['image'];
            }
        }
        return $res;
    }
    public function addInquiryReply($inq_id, $sup_id, $subject, $description, $user_id)
    {
        $data['poc_order_id'] = $inq_id;
        $data['poc_from'] = $user_id;
        $data['poc_to'] = $sup_id;
        $data['poc_title'] = $subject;
        $data['poc_desc'] = $description;
        return $this->db->insert($this->order_comments, $data);
    }
    public function addGeneralInquiry($inq)
    {
        return $this->db->insert(TABLE_PREFIX . 'enquiry', $inq);
    }
    public function getLogistics()
    {
        $data['first_section_text'] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
        $data['banner'] = array();
        $data['shipping_method'] = array();
        $data['partners'] = array();
        $bnr = $this->db->where('bnr_category', 'app_logistics')->limit(1)->get($this->tbl_banner)->row_array();
        if ($bnr) {
            $data['banner']['text'] = $bnr['bnr_desc'];
            $data['banner']['image'] = site_url() . 'assets/uploads/banner/' . $bnr['bnr_image'];
        }
        $shipping = $this->db->where('logt_status', 1)->get($this->logistics_types)->result_array();
        $i = 0;
        foreach ($shipping as $val) {
            $data['shipping_method'][$i]['title'] = $val['logt_title'];
            $data['shipping_method'][$i]['description'] = $val['logt_desc'];
            $i++;
        }
        $partners = $this->db->select($this->tbl_users . '.usr_first_name,usr_id,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
            ->where(array($this->tbl_groups . '.id' => 6, $this->tbl_users . '.usr_active' => 1))
            ->get($this->tbl_users)->result_array();
        $i = 0;
        foreach ($partners as $row) {
            $data['partners'][$i]['id'] = encryptor($row['usr_id']);
            $data['partners'][$i]['name'] = $row['usr_first_name'];
            $data['partners'][$i]['image'] = $this->getLogisticsDefaultImage($row['usr_id']);
            $i++;
        }
        return $data;

    }
    public function getLogisticsDefaultImage($userId)
    {
        $image = $this->db->get_where($this->tbl_logistics_comp_images, array('lci_user_id' => $userId, 'lci_default' => 1))->row_array();
        if (!empty($image)) {
            return site_url() . 'assets/uploads/logistics/' . $image['lci_image'];
        } else {
            $image = $this->db->order_by('lci_id', 'ASC')->get_where($this->tbl_logistics_comp_images, array('lci_user_id' => $userId))->row_array();

            if (!empty($image)) {
                return site_url() . 'assets/uploads/logistics/' . $image['lci_image'];
            }
        }
        return '';
    }
    public function getQCImage($userId)
    {
        $image = $this->db->get_where($this->tbl_qc_images, array('qci_user_id' => $userId, 'qci_default' => 1))->row_array();
        if (!empty($image)) {
            return site_url() . 'assets/uploads/qc/' . $image['qci_image'];
        } else {
            $image = $this->db->order_by('qci_id', 'ASC')->get_where($this->tbl_qc_images, array('qci_user_id' => $userId))->row_array();

            if (!empty($image)) {
                return site_url() . 'assets/uploads/qc/' . $image['qci_image'];
            }
        }
        return null;
    }
    public function getLogisticServices($id)
    {
        $data['partner'] = array();
        $data['service'] = array();
        $data['shipping_method'] = array();
        $partner = $this->db->select($this->tbl_users . '.usr_first_name,usr_last_name,usr_id,usr_description1,usr_banner,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
            ->where(array($this->tbl_groups . '.id' => 6))
            ->where('user_id', $id)
            ->get($this->tbl_users)->row_array();
        if ($partner) {
            $data['partner']['id'] = encryptor($id);
            $data['partner']['name'] = $partner['usr_first_name'] . ' ' . $partner['usr_last_name'];
            $data['partner']['description'] = $partner['usr_description1'];
            $data['partner']['image'] = $this->getLogisticsDefaultImage($partner['usr_id']);
            $data['partner']['background'] = $partner['usr_banner'] ? site_url() . 'assets/uploads/logistics/' . $partner['usr_banner'] : '';

        }

        $this->db->select($this->tbl_logistics_services . '.*,' . $this->tbl_users . '.usr_first_name')
            ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_logistics_services . '.logs_logistics_id');
        $service = $this->db->get_where($this->tbl_logistics_services, array($this->tbl_logistics_services . '.logs_logistics_id' => $id))->result_array();

        foreach ($service as $key => $value) {
            $img = $this->db->select('logsi_image')->limit(1)->get_where($this->tbl_logistics_services_images, array('logsi_service_id' => $value['logs_id']))->row()->logsi_image;
            $data['service'][$key]['id'] = encryptor($value['logs_id']);
            $data['service'][$key]['name'] = $value['logs_service_title'];
            $data['service'][$key]['description'] = $value['logs_desc'];
            $data['service'][$key]['image'] = site_url() . 'assets/uploads/logistics/' . $img;
        }
        $shipping = $this->db->select($this->logistics_types . '.*')
            ->join($this->users_logistics, 'ulog_logistics_id=logt_id')
            ->where('logt_status', 1)
            ->where('ulog_user_id', $id)
            ->get($this->logistics_types)
            ->result_array();
        $i = 0;
        foreach ($shipping as $val) {
            $data['shipping_method'][$i]['id'] = $val['logt_id'];
            $data['shipping_method'][$i]['title'] = $val['logt_title'];
            $i++;
        }
        return $data;
    }
    public function getQuality()
    {
        $data['first_section_text'] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
        $data['second_section_text'] = "The era of paying for goods that are a far cry from what its description promised is over as we are a b2b online platform that is like no other, ensuring that all the pieces that make a successful and mutually beneficial transaction for both wholesalers and retailers, check out adequately. We employ revolutionary quality control metrics that ensure the absolute quality of the products you purchase, making sure that you get precisely what you paid for. Before we approve of a wholesaler to partner up with us on our platform, we ensure that both the individual and his or her products are vetted to acceptable industry standards.

        When a wholesaler applies to partner with us and distribute his or her goods on our platform, we ensure all the products that are proposed to be listed for sale are thoroughly tackled with our world standard quality measures, and if said product doesn't meet all our requirements, the product would be denied the access to be sold on our platform. If you are looking to find a b2b platform that makes available only the highest quality whole sale goods, then you have found just the place. As long as the product you require in wholesale quantity is listed on our website, you can be sure that you are going to be purchasing only the most authentic version of said product and hence you have no worries.

        Once you have made buying from our platform paramount in your business, you can be confident that your retail business will never be the same. Let’s us help you set a premium quality standard in your trade niche by providing your customers with only the best of their demands.";

        $data['banner'] = array();
        $data['partners'] = array();
        $bnr = $this->db->where('bnr_category', 'app_quality_control')->limit(1)->get($this->tbl_banner)->row_array();
        if ($bnr) {
            $data['banner']['text'] = $bnr['bnr_desc'];
            $data['banner']['image'] = site_url() . 'assets/uploads/banner/' . $bnr['bnr_image'];
        }
        $partners = $this->db->select($this->tbl_users . '.usr_first_name,usr_id,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
            ->where(array($this->tbl_groups . '.id' => 7, $this->tbl_users . '.usr_active' => 1))
            ->get($this->tbl_users)->result_array();
        $i = 0;
        foreach ($partners as $row) {
            $data['partners'][$i]['id'] = encryptor($row['usr_id']);
            $data['partners'][$i]['name'] = $row['usr_first_name'];
            $data['partners'][$i]['image'] = $this->getQCImage($row['usr_id']);
            $i++;
        }
        return $data;

    }
    public function getQualityServices($id)
    {
        $data['partner'] = array();
        $data['service'] = array();
        $partner = $this->db->select($this->tbl_users . '.usr_first_name,usr_last_name,usr_id,usr_description1,usr_banner,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
            ->where(array($this->tbl_groups . '.id' => 7))
            ->where('user_id', $id)
            ->get($this->tbl_users)->row_array();
        if ($partner) {
            $data['partner']['id'] = encryptor($id);
            $data['partner']['name'] = $partner['usr_first_name'] . ' ' . $partner['usr_last_name'];
            $data['partner']['description'] = $partner['usr_description1'];
            $data['partner']['image'] = $this->getQCImage($partner['usr_id']);
            $data['partner']['background'] = $partner['usr_banner'] ? site_url() . 'assets/uploads/qc/' . $partner['usr_banner'] : '';
        }
        $this->db->select($this->qc_services . '.*,' . $this->tbl_users . '.usr_first_name')
            ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->qc_services . '.qcs_qc_id');
        $service = $this->db->get_where($this->qc_services, array($this->qc_services . '.qcs_qc_id' => $id))->result_array();

        foreach ($service as $key => $value) {
            $img = $this->db->select('qcsi_image')->limit(1)->get_where($this->qc_services_images, array('qcsi_service_id' => $value['qcs_id']))->row()->qcsi_image;
            $data['service'][$key]['id'] = encryptor($value['qcs_id']);
            $data['service'][$key]['name'] = $value['qcs_service_title'];
            $data['service'][$key]['description'] = $value['qcs_desc'];
            $data['service'][$key]['image'] = site_url() . 'assets/uploads/qc/' . $img;
        }

        return $data;
    }
    public function insertQuote($data, $id)
    {
        $this->db->insert($this->logistics_quotes, $data);
        $lastInsertId = $this->db->insert_id();
        if ($lastInsertId) {
            foreach ($id as $row) {
                $this->db->set('lqt_quote', $lastInsertId)
                    ->set('lqt_type', $row)
                    ->insert($this->logistics_quotes_types);
            }
            return true;
        } else {
            return false;
        }

    }
    public function getHomeSuppliers()
    {
        $limit = 6; //get_settings_by_key('home_wholesale_shops_limit');
        $sup = array();
        $this->db->select('supm_default_image,supm_name_en,supm_id');
        $this->db->limit($limit);
        $this->db->where('supm_status', 1);
        $this->db->join($this->tbl_users, 'usr_supplier=supm_id', 'RIGHT');
        $this->db->join($this->tbl_users_groups, 'user_id = usr_id', 'RIGHT');
        $this->db->where('group_id', 2);
        $this->db->where('usr_active', 1);
        $suppliers = $this->db->get($this->tbl_supplier_master)->result_array();
        foreach ($suppliers as $key => $val) {
            if (isset($suppliers[$key]['supm_default_image']) && !empty($suppliers[$key]['supm_default_image'])) {
                $image = $this->db->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id'], 'ssi_id' => $val['supm_default_image']))->row_array();
            } else {
                $image = $this->db->limit(1)->get_where($this->tbl_supplier_images, array('ssi_supplier' => $val['supm_id']))->row_array();
            }
            $defaultImg = isset($image['ssi_image']) ? $image['ssi_image'] : '';
            $sup[$key]['image'] = site_url() . 'assets/uploads/shops/' . $defaultImg;
            $sup[$key]['name'] = $val['supm_name_en'];
            $sup[$key]['id'] = encryptor($val['supm_id']);
            $sup[$key]['360_image'] = site_url() . 'supplier/get_360/' . $sup[$key]['id'];
        }
        return $sup;
    }
    public function updateUserImage($usr_id, $avatar)
    {
        return $this->db->set('usr_avatar', $avatar)
            ->where('usr_id', $usr_id)
            ->update($this->tbl_users);
    }
    public function insertQcQuote($data)
    {
        return $this->db->insert($this->qc_quotes, $data);
    }
    public function deleteChat($type, $id)
    {
        if ($type == "rfq") {
            $this->db->where('rfqn_id', encryptor($id, 'D'))->delete($this->tbl_rfq_notification);
            return $this->db->affected_rows();
        }
        if ($type == "chat") {
            $this->db->where('c_id', encryptor($id, 'D'))->delete('cpnl_chats');
            return $this->db->affected_rows();
        }

    }
    public function deleteInquiry($id)
    {
        $this->db->where('ord_id', encryptor($id, 'D'))->delete($this->tbl_products_order);
        if ($this->db->affected_rows()) {
            $this->db->where('poc_order_id', encryptor($id, 'D'))->delete($this->order_comments);
            return 1;
        }
        return 0;

    }
    public function getUserGroup($id)
    {
        return $this->db->select('group_id')->where('user_id',$id)->get($this->tbl_users_groups)->row()->group_id;
    }
    // function test($data){
    //     return $this->db->set('va',json_encode($data))->insert('test');
    // }
     function saveApiRequest($api_type,$method,$server,$request_data,$files)
       {

        $json['request_type'] = $api_type;
        $json['method'] = $method;
        $json['request_data'] = json_encode($request_data);
        $json['server'] = json_encode($server);
        $json['uploads'] = json_encode($files);

        $this->db->insert('web_service_request', $json);
         return $this->db->insert_id();
       }


        function updateApiRequest($id,$uid)
       {
        $this->db->set('user_id',$uid)->where('id',$id)->update('web_service_request');
       }
}
