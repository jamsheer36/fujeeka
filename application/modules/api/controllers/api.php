<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php';
require './vendor/autoload.php';

class api extends CI_Controller
{
    public $func_without_token = array(
        'login',
        'signup',
        'forgot_password',
        'home', 'view_all',
        'product_details',
        'get_config',
        'get_states',
        'suppliers',
        'supplier_details',
        'categories',
        'search',
        'activate',
        'resend_otp',
        'verify_otp',
        'update_password',
        'terms',
        'market_config',
        'markets',
        'feeds',
        'notification_list',
        'notification_details',
        'general_enquiry',
        'logistics',
        'logistic_services',
        'get_quote',
        'quality_control',
        'quality_services',
        'get_quality_quote',
    );
    public $mail = '';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_auth_model', 'api_auth');
        $this->load->model('api_model', 'api_model');
        $this->checkToken();
        $this->mail = new PHPMailer;
    }

    public function checkToken()
    {
        $method = $this->uri->segment(2);

        // $log_id = $this->api_model->saveApiRequest('buyer_api',$method,$_SERVER,$_REQUEST,$_FILES);

        if ((!$this->input->get_request_header('Token', true) || !$this->input->get_request_header('Token')) && !in_array($method, $this->func_without_token)) {
            $return = array('error' => 'Invalid Access Token');
            echo json_encode($return);
            die();
        }if (!in_array($method, $this->func_without_token)) {
            if (!$this->api_auth->checkTokenExist($this->input->get_request_header('Token'), $method)) {
                $return = array('error' => 'Invalid Access Token');
                echo json_encode($return);
                die();
            }
        }
        // $user = $this->api_model->getUserDetailsFromToken($this->input->get_request_header('Token'));
        // $this->api_model->updateApiRequest($log_id,$user['usr_id']);
    }

    public function login()
    {
        // $input_data = $this->input->post(); //json_decode(trim(file_get_contents('php://input')), true);
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $pushy_token = $this->input->post('pushy_token', true);
        if ($email != null && $email != '' && $password != '' && $password != null) {
            $det = $this->api_auth->login($email, $password);
            if ($det == "timeout") {
                $return = array('error' => 'Max Login Attempts Exceeded');
            } else if ($det == "inactive") {
                $return = array('error' => 'Account Inactive');
            } else if ($det == "invalid") {
                $return = array('error' => 'Invalid Credentials');
            } else {
                $this->api_model->updatePushyToken($pushy_token, 'usr_token', $det->usr_token);
                $return = array('token' => $det->usr_token, 'full_name' => $det->usr_first_name . ' ' . $det->usr_last_name, 'username' => $email, 'company' => $det->usr_company, 'image' => site_url() . 'assets/uploads/avatar/' . $det->usr_avatar, 'home' => $this->home('login', $det->usr_token));
            }
            echo json_encode($return);
            die();
        } else {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
    }
    public function signup()
    {
        $input_data = $this->input->post(); //json_decode(trim(file_get_contents('php://input')), true);
        $this->checkCaptcha($this->input->post('captcha_id', true), $this->input->post('captcha', true));
        $fname = $this->input->post('fname', true); //$input_data['fname']; //required
        $lname = isset($input_data['lname']) ? $input_data['lname'] : '';
        $company = isset($input_data['company']) ? $input_data['company'] : '';
        $mob = isset($input_data['mobile']) ? $input_data['mobile'] : '';
        $country = $this->input->post('country', true); //$input_data['country']; //required
        $state = $this->input->post('state', true); //$input_data['state']; //required
        $email = $this->input->post('email', true); //$input_data['email']; //required
        $password = $this->input->post('password', true); //$input_data['password']; //required
        $pushy_token = $this->input->post('pushy_token', true);
        if ($email != null && $email != '' && $password != '' && $password != null && $state != '' && $state != null && $country != '' && $country != null && $fname != '' && $fname != null) {
            $otp = rand(1000, 9999);
            $group = array(4);
            $data['usr_first_name'] = $fname;
            $data['usr_last_name'] = $lname;
            $data['usr_company'] = $company;
            $data['usr_phone'] = $mob;
            $data['usr_country'] = $country;
            $data['usr_state'] = $state;
            $data['usr_added_by'] = 0;
            // $data['usr_otp'] = $otp;
            $usr_tkn = $this->api_auth->register($fname, $password, $email, $data, $group);
            if ($usr_tkn) {
                $maildata['otp'] = $otp;
                $this->sendMail($email, 'reg', $maildata);
                $this->api_model->updatePushyToken($pushy_token, 'usr_token', $usr_tkn);
                $return = array('token' => $usr_tkn, 'full_name' => $fname . ' ' . $lname, 'username' => $email, 'company' => $company, 'image' => '', 'home' => $this->home('login', $usr_tkn));
                // $return = array('msg' => 'An OTP has been sent to your email', 'captcha_status' => true, 'id' => encryptor($id));
            } else {
                $return = array('error' => 'Failed to register');
            }
            echo json_encode($return);
            die();
        } else {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
    }
    public function sendMail($tomail, $type, $mdata = '')
    {
        /* Sent activation email */
        $this->mail->isSMTP();
        $this->mail->Host = MAIL_HOST;
        $this->mail->SMTPAuth = true;
        $this->mail->Username = MAIL_USERNAME;
        $this->mail->Password = MAIL_PASSWORD;
        $this->mail->SMTPSecure = 'ssl';
        $this->mail->Port = 465;
        $this->mail->setFrom(FROM_MAIL, FROM_NAME);
        $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
        $this->mail->addAddress($tomail);
        //remove this from server
        $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true,
            ),
        );
        //remove this from server
        if ($type == "reg") {
            $this->mail->Subject = 'Hi ' . $tomail . ', please verify your fujeeka account';
            $mailData['mail'] = $tomail;
            $mailData['otp'] = $mdata['otp'];
            $mailContent = $this->load->view('user/register-mail-otp-template', $mailData, true);
        }
        if ($type == "reset_pwd") {
            $this->mail->Subject = 'Reset your fujeeka password';
            $mailData['name'] = $mdata['name'];
            $mailData['otp'] = $mdata['otp'];
            $mailContent = $this->load->view('user/forget-mail-template', $mailData, true);
        }
        $this->mail->isHTML(true);

        $this->mail->Body = $mailContent;
        return $this->mail->send();
        /* Sent activation email */
    }
    public function forgot_password()
    {
        $email = $this->input->post('email');
        if ($email != null && $email != '') {
            $otp = rand(100000, 999999);
            $forgotten = $this->api_auth->forgotten_password($email, $otp);
            if ($forgotten) {
                $user = $this->api_model->getUserByIdentity($email);
                $maildata['otp'] = $otp;
                $maildata['name'] = $user['usr_first_name'] . ' ' . $user['usr_last_name'];
                $this->sendMail($email, 'reset_pwd', $maildata);
                $return = array('msg' => 'An OTP has been sent to your inbox');
            } else {
                $return = array('error' => 'No Matching Email Found');
            }
            echo json_encode($return);
            die();
        } else {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
    }
    public function verify_otp()
    {
        $email = $this->input->post('email', true);
        $otp = $this->input->post('otp', true);
        $type = $this->input->post('type', true);

        if ($email == null || $email == '' || $otp == null || $otp == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $user = $this->api_model->getUserByIdentity($email);
        if ($user) {
            if ($type == "reset_pwd") {
                if ($otp == $user['usr_forgotten_password_otp']) {
                    $return = array('msg' => 'OTP Verified', 'otp_code' => $user['usr_forgotten_password_code']);
                } else {
                    $return = array('error' => 'Invalid OTP');
                }
            } else {
                if ($otp == $user['usr_otp']) {
                    $this->api_model->updateMailStatus($user['usr_id']);
                    $return = array('msg' => 'OTP Verified');
                } else {
                    $return = array('error' => 'Invalid OTP');
                }
            }
            echo json_encode($return);
            die();
        } else {
            $return = array('error' => 'No Matching Email Found');
            echo json_encode($return);
            die();
        }

    }
    public function update_password()
    {
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $usr_forgotten_password_code = $this->input->post('otp_code', true);
        if ($email == null || $email == '' || $password == null || $password == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        if ($this->api_auth->reset_password($email, $password, $usr_forgotten_password_code)) {
            $return = array('msg' => 'Password Updated');
        } else {
            $return = array('error' => 'Failed to update');
        }
        echo json_encode($return);
        die();

    }
    public function home($from = '', $log_token = '')
    {
        $data['banners'] = $this->api_model->getHomeBanners();
        $data['latest_product'] = $this->api_model->getLatestProducts();
        $data['hotselling_product'] = $this->api_model->getHotSellingProducts();
        $data['offer_product'] = $this->api_model->getOfferProducts();
        $data['stock_product'] = $this->api_model->getStockProducts();
        $data['flash_sale_stock'] = $this->api_model->getFlashSaleStock();
        $data['suppliers'] = $this->api_model->getHomeSuppliers();
        $pushy_token = $this->input->post('pushy_token', true);
        if ($this->input->get_request_header('Token', true) || $from == 'login') {
            $token = $this->input->get_request_header('Token');
            if ($log_token) {
                $token = $log_token;
            }
            $det = $this->api_model->getUserDetailsFromToken($token);
            if ($det) {
                $this->api_model->updatePushyToken($pushy_token, 'usr_token', $token);
                $favourite = $this->api_model->getFavIds($det['usr_id']);
                $data['favourite_suppliers'] = $favourite['supplier_id'];
                $data['favourite_products'] = $favourite['product_id'];

                $user_details['full_name'] = $det['usr_first_name'] . ' ' . $det['usr_last_name'];
                $user_details['username'] = $det['usr_email'];
                $user_details['company'] = $det['usr_company'];
                $user_details['image'] = site_url() . 'assets/uploads/avatar/' . $det['usr_avatar'];
                $user_details['is_mail_verified'] = $det['usr_is_mail_verified'];
                $data['user_details'] = $user_details;
                $data['recommended_products'] = $this->api_model->getRecommendations($det['usr_id']);
                $count = $this->api_model->favouriteCount($det['usr_id']);
                $count['browsing_history'] = 10;
                $data['count'] = $count;
            }

        }
        if ($from) {
            return $data;
        }
        echo json_encode($data);
        die();
    }
    public function view_all()
    {
        $data = array();
        $type = $this->input->post('type', true);
        $offset = $this->input->post('offset', true);
        $limit = $this->input->post('limit', true);
        $cat_id = $this->input->post('cat_id', true);
        $sup_id = $this->input->post('sup_id', true);
        $filter = $this->input->post('filter', true);
        $sort_by = $this->input->post('sort_by', true);
        $flash_filter = $this->input->post('flash_sale_filter', true);
        if (!$offset) {
            $offset = 0;
        }

        if (!$limit) {
            $limit = 30;
        }
        // if ($type == null || $type == '') {
        //     $return = array('error' => 'Partial Content');
        //     echo json_encode($return);
        //     die();
        // }
        if ($type == "latest" || $type == "our_product") {
            $data['products'] = $this->api_model->getLatestProducts('view_all', $offset, $limit, $filter, $sort_by, $sup_id, $type);
        }
        if ($type == "hotselling") {
            $data['products'] = $this->api_model->getHotSellingProducts('view_all', $offset, $limit, $filter, $sort_by);
        }
        if ($type == "offer") {
            $data['products'] = $this->api_model->getOfferProducts('view_all', $offset, $limit, $filter, $sort_by);
        }
        if ($type == "stock") {
            $data['products'] = $this->api_model->getStockProducts('view_all', $offset, $limit, $filter, $sort_by, $sup_id);
        }
        if ($type == "flash_sale") {
            $data['products'] = $this->api_model->getFlashSaleStock('view_all', $offset, $limit, $filter, $sort_by, $flash_filter);
        }
        if ($type == "recommended") {
            if ($this->input->get_request_header('Token', true)) {
                $token = $this->input->get_request_header('Token');
                $det = $this->api_model->getUserDetailsFromToken($token);
                $data['products'] = $this->api_model->getRecommendations($det['usr_id'], 'view_all', $offset, $limit, $filter, $sort_by);
            } else {
                $return = array('error' => 'Access Denied');
                echo json_encode($return);
                die();
            }
        }
        if (!$type) {
            $data['products'] = $this->api_model->getAllProducts($offset, $limit, $cat_id, $sup_id, $filter, $sort_by);
        }
        echo json_encode($data);
        die();
    }
    public function product_details()
    {
        $prd_id = $this->input->post('prd_id', true);
        if ($prd_id == null || $prd_id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $return = array('product_details' => $this->api_model->getProductDetails(encryptor($prd_id, 'D')));
        echo json_encode($return);
        die();
    }
    public function contact_supplier()
    {
        $prd_id = $this->input->post('prd_id', true);
        $sup_id = $this->input->post('sup_id', true);
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $sub = $this->input->post('subject', true);
        $msg = $this->input->post('message', true);
        $unit = $this->input->post('unit', true);
        $quantity = $this->input->post('quantity', true);
        if ($prd_id == null || $prd_id == '' || $sup_id == null || $sup_id == '' || $sub == null || $sub == '' || $msg == null || $msg == '' || $quantity == null || $quantity == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $data = array();
        if (isset($_FILES['attachment']['name']) && !empty($_FILES['attachment']['name'])) {
            $file = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'product_enquiry/', 'attachment', 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|txt');
            $data['ord_attachment'] = $file;
        }
        $dec_sup = encryptor($sup_id, 'D');
        $data['ord_added_by'] = $usr_id;
        $data['ord_number'] = gen_random();
        $data['ord_buyer'] = $usr_id;
        $data['ord_message'] = $msg;
        $data['ord_subject'] = $sub;
        $data['ord_qty'] = $quantity;
        $data['ord_unit'] = $unit;
        $data['ord_prod_id'] = encryptor($prd_id, 'D');
        $sup_det = $this->api_model->getUserDetails($dec_sup);
        $data['ord_supplier'] = $sup_det['usr_supplier'];
        $data['ord_supplier_user_id'] = $dec_sup;
        if ($this->api_model->placeOrder($data)) {
            $return = array('msg' => 'Your inquiry submitted successfully.We will touch you soon!');
        } else {
            $return = array('error' => 'Something went wrong!!');
        }
        echo json_encode($return);
        die();

    }
    public function get_config()
    {
        $conf = $this->api_model->getConfig();
        echo json_encode($conf);
        die();
    }
    public function checkCaptcha($cap_id, $captcha)
    {
        if (file_exists(FILE_UPLOAD_PATH . 'captcha/' . $cap_id . $captcha . '.png')) {
            // unlink(FILE_UPLOAD_PATH . 'captcha/' . $cap_id . $captcha . '.png');
            return true;
        } else {
            $return = array('captcha_status' => false);
            echo json_encode($return);
            die();
        }

    }
    public function get_states()
    {
        $stt_country_id = $this->input->post('country_id', true);
        $return = array('states' => $this->api_model->getStates($stt_country_id));
        echo json_encode($return);
        die();

    }
    public function uploadFiles($files, $path, $name, $types)
    {
        $this->load->library('upload');
        $newFileName = microtime(true) . $files[$name]['name'];

        $config['upload_path'] = $path;
        $config['allowed_types'] = $types;
        $config['file_name'] = $newFileName;
        $config['max_size'] = 2100;

        $this->upload->initialize($config);
        if (!$this->upload->do_upload($name)) {
            $ext = pathinfo($files[$name]['name'], PATHINFO_EXTENSION);
            $up = $this->upload->display_errors();

            $return = array('error' => $up . ' ,type=' . $ext . ' ,name =' . $files[$name]['name']);
            echo json_encode($return);
            die();

        } else {
            $uploadData = $this->upload->data();
            return $uploadData['file_name'];
        }
    }
    public function suppliers()
    {
        $offset = $this->input->post('offset', true);
        $limit = $this->input->post('limit', true);
        if (!$offset) {
            $offset = 0;
        }

        if (!$limit) {
            $limit = 30;
        }
        $data['suppliers'] = $this->api_model->getSuppliers('', $offset, $limit);
        echo json_encode($data);
        die();

    }
    public function supplier_details()
    {
        $supm_id = $this->input->post('sup_id', true);
        if ($supm_id == null || $supm_id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $uid = '';
        if ($det) {
            $uid = $det['usr_id'];
        }
        $data = $this->api_model->getSuppliers(encryptor($supm_id, 'D'), '', '', $uid);
        if (!$data) {
            $data['supplier_home'] = array();
            $data['supplier_profile'] = array();
        }
        $return = array('supplier_home' => $data['supplier_home'], 'supplier_profile' => $data['supplier_profile']);
        echo json_encode($return);
        die();
    }
    public function categories()
    {
        $data['categories'] = $this->api_model->getCategories();
        echo json_encode($data);
        die();
    }
    public function markets()
    {
        $offset = $this->input->post('offset', true);
        $limit = $this->input->post('limit', true);
        $cat_id = $this->input->post('category', true);
        $market = $this->input->post('market', true);
        $building = $this->input->post('building', true);
        if (!$offset) {
            $offset = 0;
        }

        if (!$limit) {
            $limit = 30;
        }

        $data['markets'] = $this->api_model->getMarketSuppliers($limit, $offset, $cat_id, $market, $building);
        echo json_encode($data);
        die();
    }
    public function market_config()
    {
        $data = $this->api_model->marketConfig();
        echo json_encode($data);
        die();
    }
    public function rfq_config()
    {
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $captcha = array();
        $i = 1;
        foreach (getCiCaptcha(6) as $key => $cap) {
            $captcha[$i]['id'] = 'c' . $cap['time'];
            $captcha[$i]['image'] = site_url() . 'assets/uploads/captcha/' . $cap['filename'];
            $i++;
        }
        $data['captcha'] = array_values($captcha);
        $data['categories'] = $this->api_model->getCategories();
        $data['markets'] = $this->api_model->getMarkets();
        $data['units'] = $this->api_model->getUnits();
        $data['user_mail_verify_status'] = $det['usr_is_mail_verified'];
        $data['captcha'] = $captcha;
        echo json_encode($data);
        die();
    }
    public function rfq()
    {
        $cat_id = $this->input->post('cat_id', true);
        $mar_id = $this->input->post('mar_id', true);
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $product = $this->input->post('product', true);
        $requirment = $this->input->post('requirment', true);
        $quantity = $this->input->post('quantity', true);
        $valid = $this->input->post('validity', true);
        $mail = $this->input->post('mail', true);
        $unit = $this->input->post('unit', true);

        if ($cat_id == null || $cat_id == '' || $product == null || $product == '' || $requirment == null || $requirment == '' || $quantity == null || $quantity == '' || $valid == null || $valid == '' || $mail == null || $mail == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $data = array();
        if (isset($_FILES['attachment']['name']) && !empty($_FILES['attachment']['name'])) {
            $file = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'rfq/', 'attachment', 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|txt');
            $data['rfq_attachment'] = $file;
        }
        $data['rfq_category'] = $cat_id;
        $data['rfq_market'] = $mar_id != '' ? $mar_id : 0;
        $data['rfq_product_name'] = $product;
        $data['rfq_qty'] = $quantity;
        $data['rfq_unit'] = $unit;
        $data['rfq_requirment'] = $requirment;
        $data['rfq_validity'] = date('Y-m-d', strtotime($valid));
        $data['rfq_mail'] = $mail;
        $data['rfq_user'] = $usr_id;

        if ($this->api_model->insertRfq($data)) {
            $return = array('msg' => 'Request submitted, we will touch you soon!');
        } else {
            $return = array('error' => 'Something went wrong!!');
        }
        echo json_encode($return);
        die();
    }
    public function feeds()
    {
        $offset = $this->input->post('offset', true);
        $limit = $this->input->post('limit', true);
        if (!$offset) {
            $offset = 0;
        }
        if (!$limit) {
            $limit = 30;
        }
        $type = $this->input->post('type', true);
        if ($type == null || $type == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        if (!$det) {
            $det['usr_id'] = '';
        }
        $usr_id = $det['usr_id'];
        $feed = $this->api_model->getFeeds($usr_id, $type, $offset, $limit);
        echo json_encode($feed);
        die();
    }
    public function add_to_favourite()
    {
        $type = $this->input->post('type', true);
        $id = $this->input->post('id', true);
        if ($type == null || $type == '' || $id == null || $id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $data['fav_consign'] = strtoupper($type);
        $data['fav_consign_id'] = encryptor($id, 'D');
        $data['fav_added_by'] = $usr_id;
        $return = array('msg' => 'Added to favourite list');
        if (!$this->api_model->addToFavourite($data)) {
            $return = array('error' => 'Something went wrong');
        }
        echo json_encode($return);
        die();
    }
    public function search()
    {
        $type = $this->input->post('type', true);
        $key = $this->input->post('key', true);
        $offset = $this->input->post('offset', true);
        $limit = $this->input->post('limit', true);
        if (!$offset) {
            $offset = 0;
        }
        if (!$limit) {
            $limit = 30;
        }
        if ($type == null || $type == '' || $key == null || $key == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $data = $this->api_model->getSearchResults($type, $key, $offset, $limit);
        echo json_encode($data);
        die();
    }
    public function favourites()
    {
        $type = $this->input->post('type', true);
        if ($type == null || $type == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $offset = $this->input->post('offset', true);
        $limit = $this->input->post('limit', true);
        if (!$offset) {
            $offset = 0;
        }
        if (!$limit) {
            $limit = 30;
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $type = strtoupper($type);
        $data = $this->api_model->getFavourites($usr_id, $type, $offset, $limit);
        echo json_encode($data);
        die();
    }
    public function activate()
    {
        $otp = $this->input->post('otp', true);
        $usr_id = $this->input->post('usr_id', true);

        if ($otp == null || $otp == '' || $usr_id == null || $usr_id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $usr_id = encryptor($usr_id, 'D');
        $return = array('error' => 'Invalid OTP');
        if ($this->api_model->activateUserAccount($usr_id, $otp)) {
            $return = array('msg' => 'Verified Successfully');
        }
        echo json_encode($return);
        die();
    }
    public function resend_otp()
    {
        $email = $this->input->post('email', true);
        $type = $this->input->post('type', true);
        if (!$email || $this->api_model->checkIfValueExists(array('usr_email' => $email))) {
            $return = array('error' => 'Invalid email');
            echo json_encode($return);
            die();
        }
        $user = $this->api_model->getUserByIdentity($email);
        if ($user) {
            if ($type == "reset_pwd") {
                $maildata['otp'] = $this->api_model->updateOtpByMail($email, 'usr_forgotten_password_otp');
                $maildata['name'] = $user['usr_first_name'] . ' ' . $user['usr_last_name'];
                $this->sendMail($email, 'reset_pwd', $maildata);
            }
        } else {
            $return = array('error' => 'No Matching Email Found');
            echo json_encode($return);
            die();
        }

        if ($type == "reg") {
            $maildata['otp'] = $this->api_model->updateOtpByMail($email, 'usr_otp');
            $this->sendMail($email, 'reg', $maildata);
        }
        $return = array('msg' => 'OTP sent');
        echo json_encode($return);
        die();
    }
    public function inbox()
    {
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $data['inquiry_count'] = $this->api_model->inquiryCount($usr_id);
        $data['inbox'] = $this->api_model->getInbox($usr_id);
        echo json_encode($data);
        die();
    }
    public function chats()
    {
        $to = $this->input->post('to_id', true);
        $type = $this->input->post('type', true);

        if ($to == null || $to == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        if ($to == 'admin') {
            $to = encryptor(1);
        }
        $to = encryptor($to, 'D');
        $offset = $this->input->post('offset', true);
        $limit = $this->input->post('limit', true);
        if (!$offset) {
            $offset = 0;
        }
        if (!$limit) {
            $limit = 30;
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);

        if ($type == "rfq") {
            $sup_id = $this->common_model->getUserIdFromSupId($to);
            $_to_user = $this->api_model->getUserDetails($sup_id['usr_id']);
            $user_det = array(
                'name' => $_to_user['usr_first_name'] . ' ' . $_to_user['usr_last_name'],
                'image' => site_url() . 'assets/uploads/avatar/' . $_to_user['usr_avatar'],
            );
            $usr_id = $det['usr_id'];
            $data['to_user_details'] = $user_det;
            $data['chats'] = $this->api_model->loadRfqChats($sup_id['usr_id'], $usr_id, $offset, $limit);
            echo json_encode($data);
            die();
        } else {
            $_to_user = $this->api_model->getUserDetails($to);
            $user_det = array(
                'name' => $_to_user['usr_first_name'] . ' ' . $_to_user['usr_last_name'],
                'image' => site_url() . 'assets/uploads/avatar/' . $_to_user['usr_avatar'],
            );
            $usr_id = $det['usr_id'];
            $data['to_user_details'] = $user_det;
            $data['chats'] = $this->api_model->loadChats($to, $usr_id, $offset, $limit);
            echo json_encode($data);
            die();
        }
    }
    public function send_message()
    {
        $type = $this->input->get('type', true);
        $to = $this->input->get('to_id', true);
        if ($to == null || $to == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        if ($to == 'admin') {
            $to = encryptor(1);
        }
        $chnl = $to;
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $to = encryptor($to, 'D');

        $to_usr_grp = $this->api_model->getUserGroup($to);
        $to_usr_det = $this->api_model->getUserDetailsFromId($to);

        if ($type == 'rfq') {
            $sup_id = $this->common_model->getUserIdFromSupId($to);
            $rfq_id = $this->input->get('rfq_id', true);
            $content = $this->input->get('content', true);

            $chat['message'] = $content;
            $chat['time'] = date('h:i A') . ' | ' . date('M d');
            $chat['from'] = $det['usr_first_name'];
            $chat['rfq_id'] = $rfq_id;
            $chat['rfq_link'] = site_url('product/rfq-list/' . encryptor($rfq_id));
            $chat['from_img'] = 'assets/uploads/avatar/' . $det['usr_avatar'];
            $this->sendPusherMessage(encryptor($sup_id['usr_id']), 'rfq-event', $chat);

            $res = $this->api_model->insertRfqChat($rfq_id, $usr_id, $sup_id['usr_id'], $content);
            if ($res) {
                $return = array('msg' => 'Message sent', 'id' => encryptor($res));
            } else {
                $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
        } else {
            $ext = '';
            $is_prd = $this->input->get('is_prd_details', true);
            $file = $this->input->get('file', true);
            $content = $this->input->get('content', true);
            if (!$content) {
                $content = '';
            }
            if (!$file) {
                $file = '';
            }
            if ($is_prd) {
                $file = 'uploads/product/' . $file;
            } else {
                if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                    $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                    $file = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'chats/', 'file', 'gif|jpg|png|jpeg|pdf|xls|xlxs|docx|txt');
                }
            }

            $enc_id = encryptor($usr_id);
            $insert = array(
                'c_from_id' => $usr_id,
                'c_to_id' => $to,
                'c_content' => $content,
                'c_attachment' => $file,
                'c_is_prd_details' => $is_prd,
            );

            $cht_id = $this->common_model->saveChat($insert);

            if ($to_usr_grp == 2) {
                if($to_usr_det['usr_pushy_token']){
                      $push['type'] = 'recieved';
                      $push['msg_type'] = 'chat';
                      $push['from_id'] = $usr_id;
                      $push['content'] = $content;

                      $attach = 0;
                      if ($file) {
                           $attach = 1;
                      }
                      $push['attachment_link'] = '';
                      $push['attachment_preview'] = '';
                      $push['is_prd_details'] = $is_prd;
                      if ($is_prd) {
                           $push['attachment_link'] = site_url() . 'assets/' . $file;
                           $push['attachment_preview'] = site_url() . 'assets/' . $file;
                      } else if ($attach) {
                           $push['is_prd_details'] = 3;
                           $push['attachment_link'] = site_url() . 'assets/uploads/chats/' . $file;
                           $push['attachment_preview'] = site_url() . 'assets/images/attachment.jpg';
                           if ($ext == 'gif' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                                $push['attachment_preview'] = site_url() . 'assets/uploads/chats/' . $file;
                                $push['is_prd_details'] = 2;
                           }
                      }
                      $push['from_name'] = $to_usr_det['usr_first_name']." ".$to_usr_det['usr_last_name'];
                      $push['time'] = date("Y-m-d H:i:s");
                      $push['is_attachment'] = $attach;

  

                if($this->common_model->isFirstChat($usr_id,$to))
                {
                     $push['profile_image'] =isset($to_usr_det['usr_avatar'])?site_url().'assets/uploads/avatar/'.$to_usr_det['usr_avatar']:'';
                }

                     

                      $this->common_model->sendPushNotificationForSupplier($push, $to_usr_det['usr_pushy_token'], '');
                }
            }

            // if ($file) {
            //     $file = 'uploads/chats/' . $file;
            // }
            $chat['attachment'] = $file;
            $chat['message'] = $content;
            $chat['from'] = $det['usr_first_name'];
            $chat['ext'] = $ext;
            $chat['from_ch'] = $enc_id;
            $chat['is_prd'] = $is_prd;
            if ($this->sendPusherMessage($chnl, 'chat-event', $chat)) {
                $return = array('msg' => 'Message sent', 'id' => encryptor($cht_id));

            } else {
                $return = array('error' => 'Something went wrong');
            }
            echo json_encode($return);
            die();
        }
    }
    public function sendPusherMessage($channel, $event, $chat)
    {
        $options = array(
            'cluster' => PUSHER_CLUSTER,
            'useTLS' => true,
        );
        $pusher = new Pusher\Pusher(
            PUSHER_APP_KEY, PUSHER_APP_SECRET, PUSHER_APP_ID, $options
        );
        return $pusher->trigger($channel, $event, $chat);
    }
    public function rfq_list()
    {
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $return = array('rfq_list' => $this->api_model->getRfqList($usr_id));
        echo json_encode($return);
        die();
    }
    public function change_mail()
    {
        $email = $this->input->post('email', true);
        if (!$this->api_model->checkIfValueExists(array('usr_email' => $email))) {
            $return = array('error' => 'Email already taken');
            echo json_encode($return);
            die();
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $otp = rand(1000, 9999);
        if ($this->api_model->changeEmail($email, $usr_id, $otp)) {
            $maildata['otp'] = $otp;
            $this->sendMail($email, 'reg', $maildata);
            $return = array('msg' => 'An OTP has been sent to your new email');
            echo json_encode($return);
            die();
        } else {
            $return = array('error' => 'Failed to update');
            echo json_encode($return);
            die();
        }
    }
    public function terms()
    {
        $return = array('terms' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        echo json_encode($return);
        die();
    }
    public function rfq_details()
    {
        $rfq_id = $this->input->post('id', true);
        if ($rfq_id == null || $rfq_id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $details = $this->api_model->getRfqDetails($rfq_id);
        echo json_encode($details);
        die();
    }
    public function remove_favourite()
    {
        $type = $this->input->post('type', true);
        $count = $this->input->post('count', true);
        if ($type == null || $type == '' || $count == null || $count == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $id = array();
        for ($i = 0; $i < $count; $i++) {
            $id[$i] = encryptor($this->input->post("id$i"), 'D');
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $data['fav_consign'] = strtoupper($type);
        $data['fav_consign_id'] = $id;
        $data['fav_added_by'] = $usr_id;
        $dlt = $this->api_model->removeFavorite($data);
        // if ($dlt) {
        $return = array('msg' => 'Favourite removed');
        // } else {
        //     $return = array('error' => 'Something went wrong');
        // }
        echo json_encode($return);
        die();
    }
    public function follow_supplier()
    {
        $id = $this->input->post('sup_id', true);
        if ($id == null || $id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $add = $this->api_model->addToFollow($usr_id, encryptor($id, 'D'));
        if ($add) {
            $return = array('msg' => 'Followed');
        } else {
            $return = array('error' => 'Something went wrong');
        }
        echo json_encode($return);
        die();
    }
    public function unfollow()
    {
        $id = $this->input->post('sup_id', true);
        if ($id == null || $id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $add = $this->api_model->removeFollow($usr_id, encryptor($id, 'D'));
        if ($add) {
            $return = array('msg' => 'Removed');
        } else {
            $return = array('error' => 'Something went wrong');
        }
        echo json_encode($return);
        die();
    }
    public function following_list()
    {
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $data = $this->api_model->getFollowings($usr_id);
        $return = array('following' => $data);
        echo json_encode($return);
        die();
    }
    public function delete_rfq()
    {
        $id = $this->input->post('rfq_id', true);
        if ($id == null || $id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $del = $this->api_model->removeRfq($usr_id, $id);
        if ($del) {
            $return = array('msg' => 'RFQ Deleted');
        } else {
            $return = array('error' => 'Something went wrong');
        }
        echo json_encode($return);
        die();
    }
    public function inquiry_list()
    {
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $data['inquiry_list'] = $this->api_model->getInquiryList($usr_id);
        echo json_encode($data);
        die();
    }
    public function inquiry_details()
    {
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $id = $this->input->post('inq_id', true);
        if ($id == null || $id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $data['inquiry_details'] = $this->api_model->getInquiryDetails(encryptor($id, 'D'), $usr_id);
        echo json_encode($data);
        die();
    }
    public function inquiry_reply()
    {
        $inq_id = $this->input->post('inq_id', true);
        $sup_id = $this->input->post('sup_id', true);
        $subject = $this->input->post('subject', true);
        $description = $this->input->post('description', true);
        if ($inq_id == null || $inq_id == '' || $sup_id == null || $sup_id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $dec_inq_id = encryptor($inq_id, 'D');
        if ($this->api_model->addInquiryReply($dec_inq_id, encryptor($sup_id, 'D'), $subject, $description, $usr_id)) {

            $chat['title'] = $subject;
            $chat['desc'] = $description;
            $chat['type'] = 'inq';
            $chat['time'] = date('h:i A') . ' | ' . date('M d');
            $chat['from'] = $det['usr_first_name'];
            $chat['order_id'] = $dec_inq_id;
            $chat['from_img'] = 'assets/uploads/avatar/' . $det['usr_avatar'];
            $chat['inq_link'] = site_url('order/order_summery/' . $inq_id);
            $this->sendPusherMessage($sup_id, 'inquiry-event', $chat);

            $return = array('msg' => 'Reply Sent');
        } else {
            $return = array('error' => 'Partial Content');
        }
        echo json_encode($return);
        die();
    }
    public function notification_list()
    {
        $offset = $this->input->post('offset', true);
        $limit = $this->input->post('limit', true);
        if (!$offset) {
            $offset = 0;
        }
        if (!$limit) {
            $limit = 30;
        }
        $data['notification_list'] = $this->api_model->getNotificationList($offset, $limit);
        // if ($data['notification_list']) {
        //     foreach ($data['notification_list'] as $key => $val) {
        //         $data['notification_list'][$key]['from'] = 'Fujeeka';
        //     }
        // }
        echo json_encode($data);
        die();
    }
    public function notification_details()
    {
        $id = $this->input->post('id', true);
        $data['notification_details'] = $this->api_model->getNotificationDetails($id);
        echo json_encode($data);
        die();
    }
    public function general_enquiry()
    {
        $inq['enq_sup_id'] = $this->input->post('sup_id', true);
        $inq['enq_name'] = $this->input->post('enq_name', true);
        $inq['enq_email'] = $this->input->post('enq_email', true);
        $inq['enq_phone'] = $this->input->post('enq_phone', true);
        $inq['enq_subject'] = $this->input->post('enq_subject', true);
        $inq['enq_query'] = $this->input->post('enq_query', true);
        if ($inq['enq_sup_id'] == null || $inq['enq_sup_id'] == '' || $inq['enq_email'] == null || $inq['enq_email'] == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $inq['enq_sup_id'] = encryptor($inq['enq_sup_id'], 'D');
        $add = $this->api_model->addGeneralInquiry($inq);
        if ($add) {
            $return = array('msg' => 'Enquiry addedd.We will contact you soon.');
        } else {
            $return = array('error' => 'Something went wrong');
        }
        echo json_encode($return);
        die();
    }
    public function logistics()
    {
        $data['logistics'] = $this->api_model->getLogistics();
        echo json_encode($data);
        die();
    }
    public function logistic_services()
    {
        $id = $this->input->post('par_id', true);
        if ($id == null || $id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $data = $this->api_model->getLogisticServices(encryptor($id, 'D'));
        echo json_encode($data);
        die();
    }
    public function quality_control()
    {
        $data['quality_control'] = $this->api_model->getQuality();
        echo json_encode($data);
        die();
    }
    public function quality_services()
    {
        $id = $this->input->post('par_id', true);
        if ($id == null || $id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $data = $this->api_model->getQualityServices(encryptor($id, 'D'));
        echo json_encode($data);
        die();
    }
    public function get_quote()
    {
        $count = $this->input->post('count', true);
        $data['logq_service'] = $this->input->post('service_id', true);
        $data['logq_logistics'] = $this->input->post('partner_id', true);
        $data['logq_cbm'] = $this->input->post('cbm', true);
        $data['logq_weight'] = $this->input->post('weight', true);
        $data['logq_prod_type'] = $this->input->post('prd_type', true);
        $data['logq_email'] = $this->input->post('email', true);
        $data['logq_mobile'] = $this->input->post('mobile', true);
        $data['logq_loc_from'] = $this->input->post('from', true);
        $data['logq_loc_to'] = $this->input->post('to', true);
        $data['logq_desc'] = $this->input->post('description', true);
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        if ($det) {
            $data['logq_added_by'] = $det['usr_id'];
        }

        if ($data['logq_logistics'] == null || $data['logq_logistics'] == '' || $data['logq_service'] == null || $data['logq_service'] == '' || $count == null || $count == '' || $data['logq_cbm'] == null || $data['logq_cbm'] == '' || $data['logq_weight'] == null || $data['logq_weight'] == '' || $data['logq_mobile'] == null || $data['logq_mobile'] == '' || $data['logq_email'] == null || $data['logq_email'] == '' || $data['logq_loc_from'] == null || $data['logq_loc_from'] == '' || $data['logq_loc_to'] == null || $data['logq_loc_to'] == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $id = array();
        for ($i = 0; $i < $count; $i++) {
            $id[$i] = $this->input->post("shipping_id$i");
        }
        $data['logq_service'] = encryptor($data['logq_service'], 'D');
        $data['logq_logistics'] = encryptor($data['logq_logistics'], 'D');
        if ($this->api_model->insertQuote($data, $id)) {
            $return = array('msg' => 'Quote submitted.We will contact you soon.');
        } else {
            $return = array('error' => 'Something went wrong');
        }
        echo json_encode($return);
        die();

    }
    public function update_image()
    {
        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        $usr_id = $det['usr_id'];
        $avatar = '';
        if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
            $file = $this->uploadFiles($_FILES, FILE_UPLOAD_PATH . 'avatar/', 'file', 'gif|jpg|png|jpeg');
            $avatar = $file;
        }
        if (!$avatar) {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        $res = $this->api_model->updateUserImage($usr_id, $avatar);
        if ($res) {
            $return = array('msg' => 'Image updated', 'image' => site_url() . 'assets/uploads/avatar/' . $avatar);
        } else {
            $return = array('error' => 'Something went wrong');
        }
        echo json_encode($return);
        die();
    }
    public function get_quality_quote()
    {
        $count = $this->input->post('count', true);
        $data['qcq_service'] = $this->input->post('service_id', true);
        $data['qcq_qc_id'] = $this->input->post('partner_id', true);
        $data['qcq_name'] = $this->input->post('name', true);
        $data['qcq_email'] = $this->input->post('email', true);
        $data['qcq_mobile'] = $this->input->post('mobile', true);
        $data['qcq_desc'] = $this->input->post('description', true);

        $token = $this->input->get_request_header('Token');
        $det = $this->api_model->getUserDetailsFromToken($token);
        if ($det) {
            $data['qcq_added_by'] = $det['usr_id'];
        }

        if ($data['qcq_service'] == null || $data['qcq_service'] == '' || $data['qcq_qc_id'] == null || $data['qcq_qc_id'] == '' || $data['qcq_email'] == null || $data['qcq_email'] == '' || $data['qcq_mobile'] == null || $data['qcq_mobile'] == '' || $data['qcq_name'] == null || $data['qcq_name'] == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }

        $data['qcq_service'] = encryptor($data['qcq_service'], 'D');
        $data['qcq_qc_id'] = encryptor($data['qcq_qc_id'], 'D');
        if ($this->api_model->insertQcQuote($data)) {
            $return = array('msg' => 'Quote submitted.We will contact you soon.');
        } else {
            $return = array('error' => 'Something went wrong');
        }
        echo json_encode($return);
        die();

    }
    public function delete_chat()
    {
        $id = $this->input->post('id', true);
        $type = $this->input->post('type', true);
        if ($id == null || $id == '' || $type == null || $type == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        if ($this->api_model->deleteChat($type, $id)) {
            $return = array('msg' => 'Deleted');
        } else {
            $return = array('error' => 'Something went wrong');
        }
        echo json_encode($return);
        die();

    }
    public function delete_inquiry()
    {
        $id = $this->input->post('inq_id', true);
        if ($id == null || $id == '') {
            $return = array('error' => 'Partial Content');
            echo json_encode($return);
            die();
        }
        if ($this->api_model->deleteInquiry($id)) {
            $return = array('msg' => 'Deleted');
        } else {
            $return = array('error' => 'Something went wrong');
        }
        echo json_encode($return);
        die();

    }

}
