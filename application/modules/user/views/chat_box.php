<!-- Font icons -->
<base href="<?php echo base_url('assets/');?>/"/>
<link rel="stylesheet" href="css/fontello.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<!-- Main Css -->
<link rel="stylesheet" href="css/chat-main2.css">

<!-- Chat area -->
<div class="chat-area">
  <!-- Chats container -->
  <input type='hidden' id='to_usr' value="<?php echo $usr_id?>">
  <input type="hidden" value="<?php echo base_url(); ?>" name="" id="url">
<input type="hidden" value="<?php echo encryptor($this->uid) ?>" name="" id="channel">
  <section class="chats-container">
    <div class="scroll-box chat_append">

    <?php foreach($chats as $row){
        $now = DateTime::createFromFormat('U.u', $row['c_time']);
        $now->setTimeZone(new DateTimeZone('Asia/Kolkata'));
        $row['c_time'] = $now->format("Y-m-d H:i:s");
        $ext = end(explode('.',$row['c_attachment']));
        $class_outer = '';
        $class_inner = ' white-bx';
        $text = "User Intrested In This Product";
        if($row['c_from_id']==$this->uid){
            $class_outer = ' rght';
            $class_inner = ' green-bx';
            $text = 'Details sent to supplier';
        }
        if($row['c_content'] && !$row['c_is_prd_details']){
        ?>
      <div class="chat-bx-outer <?php echo $class_outer; ?>">
        <div class="chat-bx <?php echo $class_inner; ?>">
          <?php echo $row['c_content']; ?>
        </div>
        <div class="r-time">
         <?php echo $row['c_time']; ?>
        </div>
      </div>
        <?php } if($row['c_attachment'] && !$row['c_is_prd_details']){ ?>
            <div class='chat-bx-outer<?php echo $class_outer; ?>'>
                <div class='chat-bx<?php echo $class_inner; ?>'>
                    <?php if($ext=='gif' || $ext=='jpg' || $ext=='png' || $ext=='jpeg'){ ?>
                        <div class='img-holder'><a title='View Image' href='uploads/chats/<?php echo $row['c_attachment'];?>' target='_blank' rel='noopener noreferrer'><img src='uploads/chats/<?php echo $row['c_attachment'];?>' alt='image not found'></a></div>
                        <?php } else { ?>
                        <div class='img-holder'><a title='Download Document' href='uploads/chats/<?php echo $row['c_attachment'];?>' download><img src='images/attachment.jpg' alt='image not found'></a></div>
                    <?php } ?>
                </div>
                <div class='r-time'>
                    <?php echo $row['c_time'];?>
                </div>
            </div>   <div style="clear:both;"></div>      
        <?php } if($row['c_is_prd_details']){ ?>
          <div class="chat-bx-outer<?php echo $class_outer; ?>">
          <div class="green-box">
            <div class="product-info">
                <div class="image">
                    <img src="<?php echo $row['c_attachment'] ?>" alt="">
                </div>
                <div class="detail">
                    <p class="heading6" ><?php echo $row['c_content'] ?></p>
                </div>
            </div>
            <?php if($text){ ?>
            <div class="interest">
                  <?php echo $text ?>
            </div>
            <?php }?>
          </div>
        </div>  <div style="clear:both;"></div>
         <?php }} ?>  
         <?php if($img) {?>
        <div class="chat-bx-outer rght p_details">
          <div class="green-box">
            <div class="product-info">
                <div class="image">
                    <img src="<?php echo $img ?>" id="pimg" alt="">
                </div>
                <div class="detail">
                    <p class="heading6" id="pname"><?php echo $name ?></p>
                    <p>Price : <span class="price-txt">$<?php echo $price ?></span></p>
                </div>
            </div>
            <div class="interest">
              I'm interested in this product 
            </div>
            <div class="interest">
              Send to seller?
              <button id="det_send" class="btn btn-default" type="button" name="button">Send</button>
              <button id="det_cancel" class="btn btn-default btn-cancel" type="button" name="button">Cancel</button>
            </div>
          </div>
        </div> <div style="clear:both;"></div>
         <?php } ?>   
    </div>
  </section>
  <!-- /Chats container -->

  <!-- Chat send area -->
  <section class="ch-send">
    <div class="ch-container">
      <div class="attach-btn">
        <!-- <button type="button" class="btn attach-btn">
          <i class="demo-icon icon-attach"></i>
        </button> -->
        <label class="btn-bs-file">
            <i class="demo-icon icon-attach"></i>
            <input type="file" id="file"/>
        </label>
      </div>
      <div class="input-group">
        <input type="text"  id="message" class="form-control" placeholder="Type something here">
        <div class="input-grpup-append">
          <button type="button" class="btn btn-success btn-send mt-1" id="btn-chat"><i class="demo-icon icon-direction"></i></button>
        </div>
      </div>
    </div>
  </section>
  <!-- Chat send area -->
</div>
<!-- /Chat area -->
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <script src="js/pusher_head.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<!-- Scrollbar -->
<!-- <script src="js/scrollbar.js"></script> -->

<script type="text/javascript">


// var h = $('.chat_append');
//                  var height = h[0].scrollHeight;
//                  h.scrollTop(height);
     var objDiv = $(".scroll-box");
     var h = objDiv.get(0).scrollHeight;
     objDiv.animate({scrollTop: h});
     console.log(h);
     
    //  $(document).ready(function(){
    //     $('.chats-container').removeClass("d-none");
    //  });
  </script>


