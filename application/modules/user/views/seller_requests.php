<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Supplier Requests</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Email</th>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                   </tr>
                              </thead>
                              <tbody> 
                                   <?php
                                     if (!empty($requests)) {
                                          foreach ($requests as $key => $value) {
                                               ?>
                                               <tr>
                                                    <td class=""><?php echo $value['usr_email'];?></td>
                                                    <td class=""><?php echo $value['usr_first_name'] .' '. $value['usr_last_name'];?></td>
                                                    <td class=""><?php echo $value['added_on'];?></td>
                                                    <td>
                                                         <label class="switch">
                                                              <input type="checkbox" value="" class="changeRequestStatus" 
                                                              <?php echo $value['status'] == 1 ? 'checked' : '';?>
                                                                     data-url="<?php echo site_url().$controller . '/changeRequestStatus/' . encryptor($value['sr_id']) . '/' . encryptor($value['sr_usr_id']);?>">
                                                              <span class="slider round"></span>
                                                         </label>
                                                    </td>
                                               </tr>
                                               <?php
                                          }
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>