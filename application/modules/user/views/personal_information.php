<section class="multiform-container log">
     <div class="container">
          <div class="row">
               <!-- Steps form -->
               <div class="card">
                    <div class="card-body">
                         <!-- Stepper -->
                         <div class="steps-form">
                              <div class="steps-row setup-panel">
                                   <div class="steps-step">
                                        <a href="javascript:;" type="button" class="btn btn-default btn-circle">1</a>
                                        <p>Verification</p>
                                   </div>
                                   <div class="steps-step" disabled="disabled">
                                        <a href="javascript:;" type="button" class="btnSecondStep btn 
                                             <?php echo (isset($user['usr_active']) ) ? 'btn-indigo'  : '';?> btn-circle" disabled="disabled">2</a>
                                        <p>Info</p>
                                   </div>
                                   <div class="steps-step">
                                        <a href="javascript:;" type="button" class="btnLastStep btn btn-default btn-circle 
                                           <?php echo (isset($user['usr_active']) ) ? ''  : 'btn-indigo';?>" disabled="disabled">3</a>
                                        <p>Finish</p>
                                   </div>
                              </div>
                         </div>

                         <!-- Second Step -->
                        
                                <div class="row setup-content divSignUpStepSecond" id="info">
                                     <form class="frmSignUpStepSecond">
                                          <div class="col-12">
                                               <div class="form-group usr">
                                                    <p>Your Email : <span id="uname_span"><?php echo $user['usr_email'];?></span></p>
                                                    <input type="hidden" value="<?php echo encryptor($user['usr_id']);?>" name="usr_id">
                                               </div>
                                               <div class="form-group">
                                                    <label for="state">First Name:</label>
                                                    <input type="text" class="form-control" id="fname" placeholder="First Name" name="usr_first_name">
                                               </div>
                                               <div class="form-group">
                                                    <label for="state">Last Name:</label>
                                                    <input type="text" class="form-control" id="lname" placeholder="Last Name" name="usr_last_name">
                                               </div>
                                               <input type="hidden"   name="reg_email" value="<?php echo $user['usr_email'];?>">
                                               <div class="form-group">
                                                    <label for="pwd">Password:</label>
                                                    <input type="password"  class="form-control" id="pwd" placeholder="Enter password" name="usr_password">
                                               </div>
                                               <div class="form-group">
                                                    <label for="cpwd">Confirm Password:</label>
                                                    <input type="password" class="form-control" id="cpwd" placeholder="Confirm password" name="usr_cpassword">
                                               </div>
                                               <input type="hidden" id="site_url" value="<?php echo site_url();?>">
                                               <div class="form-group">
                                                    <label for="country">Country:</label>
                                                    <select class="form-control bindToDropdown" data-dflt-select="Select State" name="usr_country" data-url="<?php echo site_url('states/getStatesByCountry');?>"data-bind="cmbModel" id="supm_country">
                                                         <option value="">Select Country</option>
                                                         <?php foreach ((array) $country as $key => $value) {?>
                                                              <option value="<?php echo $value['ctr_id'];?>"><?php echo $value['ctr_name'];?></option>
                                                         <?php }?>
                                                    </select>
                                               </div>
                                               <div class="form-group">
                                                    <label for="state">State:</label>
                                                    <!-- <input type="state" class="form-control" id="cpwd" placeholder="State" name="state"> -->
                                                    <select class="cmbModel select2_group form-control" name="usr_state" i="supm_state">
                                                         <option value="">Select state</option>
                                                    </select>
                                               </div>
                                               <div class="form-group d-none">
                                                    <label for="state">Company Name:</label>
                                                    <input type="hidden" class="form-control" value="" id="cname" placeholder="Company Name" name="usr_company">
                                               </div>
                                               <div class="form-group">
                                                    <div class="phne">
                                                         <div class="form-inner ccode">
                                                              <label for="state">Country Code:</label>
                                                              <input type="ccode" class="form-control numOnly" id="ccode" placeholder="Code" name="ccode">
                                                         </div>
                                                         <div class="form-inner">
                                                              <label for="state">Phone:</label>
                                                              <input type="phone" class="form-control numOnly" id="phone" placeholder="Phone" name="usr_phone">
                                                         </div>
                                                    </div>
                                               </div>

                                               <div class="clearfix"></div>
                                               <div class="divValidationMessage" style="font-size: 10px !important; color: red !important;"></div>
                                               <button class="btn btn-indigo btn-pad btn-rounded nextBtn d-block mx-auto nxt_link_btn btnSubmitSecondStep" 
                                                       type="submit">Proceed</button>
                                          </div>
                                          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                     </form>

                                     <div class="col-12 divSuccess" style="display: none;">
                                          <div  style="text-align: center;">
                                               <h3 class="pl-0">Thank you!!</h3>
                                               <p>You have been registered succesfully.</p>
                                               <button onclick="location.href = '<?php echo site_url();?>'" 
                                                       class="btn btn-indigo btn-pad btn-rounded ">Go To Website</button>
                                                       <button onclick="location.href = '<?php echo site_url('dashboard');?>'" 
                                                       class="btn btn-pad btn-rounded ">Go To Dashboard</button>
                                          </div>
                                     </div>
                                </div>
                          
                    </div>
               </div>
          </div>
     </div>
</section>

<style>
     span.error {
          font-size: 10px !important;
          color: red !important;
     }
     span#mail_exist {
          font-size: 10px;
          color: red;
     }
</style>