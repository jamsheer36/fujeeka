<div class="ch-wrapper">
    <!-- Side section -->
    <div class="side-sec">
        
        <!-- Heading -->
      <div class="cht-heading">
        <!-- Back button -->
        <div class="back-btn" onclick="window.history.back()">
          <i class="demo-icon icon-left-big"></i>
        </div>
        <!-- /Back button -->
        <div class="h-title">
          <h1><?php echo $this->session->userdata('usr_username');?></h1>
        </div>
      </div>
      <!-- /Heading -->
      
      <div style="height:100% !important" data-simplebar>
      
      <!-- search -->
      <section class="search-tp">
        <div class="searchContainer">
          <!-- <img src="assets/images/icons/search-icon.png" alt="search" class="searchIcon"> -->
          <input class="searchBox" type="search" name="search" placeholder="Search...">
          <i class="demo-icon icon-search searchIcon"></i>
        </div>
        <!--<p class="text-center mt-2">User : <span><?php echo $this->session->userdata('usr_username');?></span>-->
      </section>
      <!-- / search -->

      <!-- chat heads -->
      <section class="chat-head" style="cursor:pointer">
        <ul class="chat_ul">
          <!-- chat head -->

          <?php 
          $i=1; 
          foreach($chats as $row) { 
             $ency_id = encryptor($row['usr_id']);
          ?>
          <li class="load_chat chat_head<?php echo $i; ?> chat_parent<?php echo $ency_id ?>" data-id="<?php echo $ency_id ?>" data-name="<?php echo $row['usr_first_name'].' '.$row['usr_last_name']; ?>">
            <div class="ch">
              <div class="ch-img-container">
                <div class="ch-img">
                  <?php echo img(array('src' => FILE_UPLOAD_PATH . 'avatar/' . $row['usr_avatar'], 'alt' => 'none'));?>
                </div>
                <div class="noti-count count<?php echo $ency_id ?> d-none">0</div>
              </div>
              <div class="ch-det">
                <h4 class="chat_names"><?php echo $row['usr_first_name']; ?></h4>
                <p>Click to load chat</p>
              </div>
            </div>
          </li>
          <?php $i++; } ?>
          <!-- /chat head -->

        </ul>
      </section>
      <!-- /chat heads -->
    </div>
    </div>
    <!-- /Side section -->
    <div class="chat-area">
    <div class="ajax_loader d-none"></div>
    <!-- Welcome container -->
        <!-- <div class="chat-area chat_div"> -->
            <section class="ch-wlcme chat_div">
                <div class="chw-cont">
                <figure>
                    <img src="images/chat-logo.png" alt="Fujeeka chat">
                </figure>
                <h3>Chat with suppliers directly here!</h3>
                <p>An easiest way to communicate and negotiate your requirement with the suppliers </p>
                </div>
            </section>
        <!-- </div> -->
        <!-- /Welcome container -->
        <section class="ch-send d-none">
            <form>
                <div class="ch-container">
                    <div class="attach-btn">
                        <!-- <button type="button" class="btn attach-btn">
              <i class="demo-icon icon-attach"></i>
            </button> -->
            <label class="btn-bs-file">
                <i class="demo-icon icon-attach"></i>
                <input type="file" id="file"/>
            </label>
                    </div>
                    <div class="input-group">
                        <input type="text" id="message" class="form-control" placeholder="Type something here">
                        <div class="input-grpup-append">
                            <button type="button" class="btn btn-success btn-green" id="btn-chat">Send</button>
                        </div>
                    </div>
                </div>
          </form>
            </section>
    </div>
    
  </div>