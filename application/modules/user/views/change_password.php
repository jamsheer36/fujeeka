<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Change password</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("user/change-password", array('id' => "frmResetPassword", 'class' => "form-horizontal"))?>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Old Password*</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php echo form_input($old_password);?>
                              </div>
                         </div>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">New Password*</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php echo form_input($new_password);?>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">New Password Confirm*</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <?php echo form_input($new_password_confirm);?>
                              </div>
                         </div>
                         <?php echo form_input($user_id);?>
                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <button type="submit" class="btn btn-success">Submit</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     .parsley-errors-list {
          display: none;
     }
     label.error{
          color: red;
          font-size: 10px;
     }
</style>