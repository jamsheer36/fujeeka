<section class="multiform-container log">
     <div class="container">
          <div class="row">
               <!-- Steps form -->
               <div class="card">
                    <div class="card-body">
                         <div class="row setup-content divSignUpStepSecond" id="finish">
                              <div class="col-12 divSuccess">
                                   <div style="text-align: center;">
                                        <h3 class="pl-0">Thank you!!</h3>
                                        <p>Your email verification successfully completed.</p>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</section>