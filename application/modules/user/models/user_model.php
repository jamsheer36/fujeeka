<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tbl_users = TABLE_PREFIX . 'users';
        $this->tbl_user_access = TABLE_PREFIX . 'user_access';
        $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
        $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
    }

    public function register($data)
    {
        if ($data) {
            $username = $data['usr_email'];
            $password = $data['usr_password'];
            $email = $data['usr_email'];
            $group = array($data['usr_group']); // Sets user to admin.

            unset($data['usr_group']);
            unset($data['usr_password']);
            unset($data['usr_cpassword']);
            unset($data['type']);
            $data['usr_added_by'] = $this->uid;
            $lastInsertId = $this->ion_auth->register($username, $password, $email, $data, $group);

            generate_log(array(
                'log_title' => 'Create new user',
                'log_desc' => 'New user added',
                'log_controller' => strtolower(__CLASS__),
                'log_action' => 'C',
                'log_ref_id' => $lastInsertId,
                'log_added_by' => $this->uid,
            ));

            return true;
        } else {
            generate_log(array(
                'log_title' => 'Create new user',
                'log_desc' => 'Failed to create new user',
                'log_controller' => strtolower(__CLASS__),
                'log_action' => 'C',
                'log_ref_id' => 0,
                'log_added_by' => $this->uid,
            ));

            return false;
        }
    }

    public function getUserByIdentity($identity, $checkAdmin = false)
    {
        if (!empty($identity)) {
            $identityColomn = $this->config->item('identity', 'ion_auth');
            $user = $this->db->get_where($this->tbl_users, array($identityColomn => $identity))->row_array();
            if ($checkAdmin) {
                return (isset($user['usr_id']) && ($user['usr_id'] == 1)) ? true : false;
            }
            return $user;
        } else {
            return false;
        }
    }
    public function getChats()
    {
        $qry = "select usr_id,usr_first_name,usr_last_name,usr_avatar from cpnl_chats join (select user, max(c_time) m from ((select c_to_id user, c_time from cpnl_chats where c_from_id=$this->uid ) union (select c_from_id user, c_time from cpnl_chats where c_to_id=$this->uid) ) cpnl_chats1 group by user) cpnl_chats2 on ((c_from_id=$this->uid and c_to_id=user) or (c_from_id=user and c_to_id=$this->uid)) and (c_time = m) join cpnl_users on usr_id = user order by c_time desc";
        return $this->db->query($qry)->result_array();
    }
    public function loadChats($usr_id, $from = '')
    {
        $where = "c_from_id = $this->uid and c_to_id = $usr_id or(c_from_id = $usr_id and c_to_id = $this->uid)";
        $chats = $this->db->where($where)->order_by('c_time', 'ASC')->get(TABLE_PREFIX . 'chats')->result_array();
        if ($from == "chat_head") {
            return $chats;
        }
        $output = "<section class='tp-sec'>
           <h2 id='chat_to_name'></h2>
        </section> <input type='hidden' id='to_usr' value=" . encryptor($usr_id) . ">";

        $output .= "<section class='chats-container'>
        <div class='scroll-bar-wrap'>
          <div class='scroll-box chat_append'>
        ";
        foreach ($chats as $row) {
            $now = DateTime::createFromFormat('U.u', $row['c_time']);
            $row['c_time'] = $now->format("Y-m-d H:i:s");
            $ext = end(explode('.', $row['c_attachment']));
            $class_outer = '';
            $class_inner = ' white-bx';
            $text = "User Intrested In This Product";
            if ($row['c_from_id'] == $this->uid) {
                $class_outer = ' rght';
                $class_inner = ' green-bx';
                $text = 'Details sent to supplier';
            }
            if ($row['c_content'] && !$row['c_is_prd_details']) {
                $output .= "<div class='chat-bx-outer" . $class_outer . "'>
                            <div class='chat-bx" . $class_inner . "'>
                                " . $row['c_content'] . "
                                </div>
                                <div class='r-time'>
                                    " . $row['c_time'] . "
                                </div>
                            </div><div style='clear:both;'></div>";
            }

            if ($row['c_attachment'] && !$row['c_is_prd_details']) {
                $output .= "<div class='chat-bx-outer" . $class_outer . "'>
                            <div class='chat-bx" . $class_inner . "'>";
                if ($ext == 'gif' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                    $output .= "<div class='img-holder'><a title='View Image' href='uploads/chats/" . $row['c_attachment'] . "' target='_blank' rel='noopener noreferrer'><img src='uploads/chats/" . $row['c_attachment'] . "' alt='image not found'></a></div>";
                } else {
                    $output .= "<div class='img-holder'><a title='Download Document' href='uploads/chats/" . $row['c_attachment'] . "' download><img src='images/attachment.jpg' alt='image not found'></a></div>";
                }
                $output .= "</div>
                                <div class='r-time'>
                                    " . $row['c_time'] . "
                                </div>
                            </div><div style='clear:both;'></div>";
            }
            if ($row['c_is_prd_details']) {
                $output .= '<div class="chat-bx-outer' . $class_outer . '">
                    <div class="green-box">
                      <div class="product-info">
                          <div class="image">
                              <img src="' . $row['c_attachment'] . '" alt="">
                          </div>
                          <div class="detail">
                              <p class="heading6" >' . $row['c_content'] . '</p>
                          </div>
                      </div>';
                if ($text) {
                    $output .= '<div class="interest">
                              ' . $text . '
                        </div>';
                }
                $output .= '</div>
                    </div><div style="clear:both;"></div> ';
            }

        }
        $output .= "</div>
            <div class='cover-bar'></div>
         </div></section>";
        echo json_encode($output);die();

    }
    public function addToSellerRequest($data)
    {
        $count = $this->db->where('sr_usr_id', $data['sr_usr_id'])->get(TABLE_PREFIX . 'seller_requests')->num_rows();
        if ($count > 0) {
            return 'duplicate';
        }
        $this->db->insert(TABLE_PREFIX . 'seller_requests', $data);
        return $this->db->insert_id();
    }
    public function getSellerRequests($id = '')
    {

        $this->db->select('*');
        $this->db->join(TABLE_PREFIX . 'users', 'usr_id = sr_usr_id');
        if ($id) {
            $this->db->where('sr_id', encryptor($id, 'D'));
        }
        $this->db->order_by('added_on', 'desc');
        return $this->db->get(TABLE_PREFIX . 'seller_requests')->result_array();
    }
    public function changeRequestStatus($sr_id, $status)
    {
        $this->db->where('sr_id', encryptor($sr_id, 'D'));
        return $this->db->update(TABLE_PREFIX . 'seller_requests', array('status' => $status));
    }
    public function updateOtpByMail($email)
    {
        $otp = rand(1000, 9999);
        $this->db->where('usr_email', $email)->update($this->tbl_users, array('usr_otp' => $otp));
        return $otp;
    }
    public function activateUserMail($otp)
    {
        $this->db->where('usr_id', $this->uid)->where('usr_otp', $otp)->update($this->tbl_users, array('usr_is_mail_verified' => 1));
        return ($this->db->affected_rows() == 1) ? true : false;
    }
    public function changeEmail($mail)
    {
        $this->db->where('usr_id', $this->uid)->update($this->tbl_users, array('usr_email' => $mail,'usr_is_mail_verified' => 0));
        return ($this->db->affected_rows() == 1) ? true : false;
    }
    public function checkIfValueExists($mail)
    {
        $return = $this->db->like('usr_email', $mail, 'none')->get($this->tbl_users)->row_array();
        return (empty($return)) ? true : false;
    }
    public function getUserById($id)
    {
        $user = $this->db->get_where($this->tbl_users, array('usr_id' => $id))->row_array();
        return $user;
    }
}
