<?php

  if (!defined('BASEPATH')) {
       exit('No direct script access allowed');
  }

  class logistics_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_states = TABLE_PREFIX . 'states';
            $this->tbl_groups = TABLE_PREFIX . 'groups';
            $this->tbl_countries = TABLE_PREFIX . 'countries';
            $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
            $this->tbl_users_logistics = TABLE_PREFIX . 'users_logistics';
            $this->tbl_logistics_types = TABLE_PREFIX . 'logistics_types';
            $this->tbl_logistics_quotes = TABLE_PREFIX . 'logistics_quotes';
            $this->tbl_logistics_services = TABLE_PREFIX . 'logistics_services';
            $this->tbl_logistics_comp_images = TABLE_PREFIX . 'logistics_comp_images';
            $this->tbl_logistics_quotes_types = TABLE_PREFIX . 'logistics_quotes_types';
            $this->tbl_logistics_services_images = TABLE_PREFIX . 'logistics_services_images';
            $this->tbl_logistics_comp_specification = TABLE_PREFIX . 'logistics_comp_specification';
       }

       function getLogistics($id = '') {
            if (!empty($id)) {
                 $data['userDetails'] = $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
                                 ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                                 ->where(array($this->tbl_users . '.usr_id' => $id))
                                 ->get($this->tbl_users)->row_array();

                 $data['images'] = $this->db->get_where($this->tbl_logistics_comp_images, array('lci_user_id' => $id))->result_array();
                 $data['specifications'] = $this->db->get_where($this->tbl_logistics_comp_specification, array('lcs_user_id' => $id))->result_array();
                 $data['logisticsTypesSelected'] = $this->db->select($this->tbl_users_logistics . '.*,' . $this->tbl_logistics_types . '.*')
                                 ->join($this->tbl_logistics_types, $this->tbl_logistics_types . '.logt_id = ' . $this->tbl_users_logistics . '.ulog_logistics_id')
                                 ->get_where($this->tbl_users_logistics, array($this->tbl_users_logistics . '.ulog_user_id' => $id))->result_array();

                 $data['logisticsServices'] = $this->db->get_where($this->tbl_logistics_services, array('logs_logistics_id' => $id))->result_array();

                 if (!empty($data['logisticsServices'])) {
                      foreach ($data['logisticsServices'] as $key => $value) {
                           $data['logisticsServices'][$key]['images'] = $this->db->get_where($this->tbl_logistics_services_images, array('logsi_service_id' => $value['logs_id']))->result_array();
                      }
                 }

                 return $data;
            }
            return $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->where(array($this->tbl_groups . '.id' => 6))
                            ->get($this->tbl_users)->result_array();
       }

       function getLogisticsTypes() {
            return $this->db->get($this->tbl_logistics_types)->result_array();
       }

       function register($datas) {
            if (isset($datas['user']) && !empty($datas['user'])) {
                 $datas['user']['usr_active'] = $this->uid == 1 ? 1 : 0;
                 $this->db->insert($this->tbl_users, $datas['user']);
                 $userId = $this->db->insert_id();
                 $this->ion_auth->add_to_group(6, $userId);

                 if (isset($datas['logistics']) && !empty($datas['logistics'])) {
                      foreach ($datas['logistics'] as $key => $value) {
                           $this->db->insert($this->tbl_users_logistics, array(
                               'ulog_user_id' => $userId,
                               'ulog_logistics_id' => $value,
                               'ulog_added_by' => $this->uid
                           ));
                      }
                 }
                 return true;
            }
            return false;
       }

       function getPendingInqueries() {
            return $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->where(array($this->tbl_groups . '.id' => 6, $this->tbl_users . '.usr_active' => 0))
                            ->get($this->tbl_users)->result_array();
       }

       function selectedLogisticsTypes($userId) {
            if (!empty($userId)) {
                 $types = $this->db->select('GROUP_CONCAT(ulog_logistics_id) AS ulog_logistics_id')
                                 ->get_where($this->tbl_users_logistics, array('ulog_user_id' => $userId))->row()->ulog_logistics_id;
                 return explode(',', $types);
            }
            return false;
       }

       function add($datas) {
            if ($this->uid == 1) {
                 $datas['users']['usr_active'] = 1;
                 $datas['users']['usr_is_mail_verified'] = 1;
            }
            $this->db->insert($this->tbl_users, $datas['users']);
            $userId = $this->db->insert_id();
            $this->ion_auth->add_to_group(6, $userId);

            if (isset($datas['logTypes']) && !empty($datas['logTypes'])) {
                 foreach ($datas['logTypes'] as $key => $value) {
                      $this->db->insert($this->tbl_users_logistics, array(
                          'ulog_user_id' => $userId,
                          'ulog_logistics_id' => $value,
                          'ulog_added_by' => $this->uid
                      ));
                 }
            }

            $spcEnKey = isset($datas['specification_key']['en']) ? $datas['specification_key']['en'] : 0;
            $spcArKey = isset($datas['specification_key']['ar']) ? $datas['specification_key']['ar'] : 0;
            $spcChKey = isset($datas['specification_key']['ch']) ? $datas['specification_key']['ch'] : 0;
            $spcEnVal = isset($datas['specification_val']['en']) ? $datas['specification_val']['en'] : 0;
            $spcArVal = isset($datas['specification_val']['ar']) ? $datas['specification_val']['ar'] : 0;
            $spcChVal = isset($datas['specification_val']['ch']) ? $datas['specification_val']['ch'] : 0;
            $count = isset($spcEnKey) ? count($spcEnKey) : 0;
            for ($i = 0; $i <= $count - 1; $i++) {
                 $specifi = array(
                     'lcs_user_id' => $userId,
                     'lcs_key_en' => clean_text($spcEnKey[$i], 0),
                     'lcs_key_ar' => clean_text($spcArKey[$i], 0),
                     'lcs_key_ch' => clean_text($spcChKey[$i], 0),
                     'lcs_val_en' => clean_text($spcEnVal[$i], 0),
                     'lcs_val_ar' => clean_text($spcArVal[$i], 0),
                     'lcs_val_ch' => clean_text($spcChVal[$i], 0)
                 );
                 $this->db->insert($this->tbl_logistics_comp_specification, $specifi);
            }
            return $userId;
       }

       function edit($datas) {
            if (!empty($datas)) {

      foreach ($datas['ici_alttag'] as $lci_id => $ici_alttag) {
                 $this->db->where('lci_id', $lci_id)
                 ->update($this->tbl_logistics_comp_images, array('ici_alttag' => $ici_alttag));
                    }

                 $userId = $datas['usr_id'];
                 $this->db->where('usr_id', $userId);
                 if ($this->uid == 1) {
                      $datas['users']['usr_active'] = 1;
                 }
                 $this->db->update($this->tbl_users, $datas['users']);
                 $this->db->where('lcs_user_id', $userId)->delete($this->tbl_logistics_comp_specification);
                 $spcEnKey = isset($datas['specification_key']['en']) ? $datas['specification_key']['en'] : 0;
                 $spcArKey = isset($datas['specification_key']['ar']) ? $datas['specification_key']['ar'] : 0;
                 $spcChKey = isset($datas['specification_key']['ch']) ? $datas['specification_key']['ch'] : 0;
                 $spcEnVal = isset($datas['specification_val']['en']) ? $datas['specification_val']['en'] : 0;
                 $spcArVal = isset($datas['specification_val']['ar']) ? $datas['specification_val']['ar'] : 0;
                 $spcChVal = isset($datas['specification_val']['ch']) ? $datas['specification_val']['ch'] : 0;
                 $count = isset($spcEnKey) ? count($spcEnKey) : 0;
                 for ($i = 0; $i <= $count - 1; $i++) {
                      $specifi = array(
                          'lcs_user_id' => $userId,
                          'lcs_key_en' => clean_text($spcEnKey[$i], 0),
                          'lcs_key_ar' => clean_text($spcArKey[$i], 0),
                          'lcs_key_ch' => clean_text($spcChKey[$i], 0),
                          'lcs_val_en' => clean_text($spcEnVal[$i], 0),
                          'lcs_val_ar' => clean_text($spcArVal[$i], 0),
                          'lcs_val_ch' => clean_text($spcChVal[$i], 0)
                      );
                      $this->db->insert($this->tbl_logistics_comp_specification, $specifi);
                 }
                 $this->db->where('ulog_user_id', $userId)->delete($this->tbl_users_logistics);
                 if (isset($datas['logTypes']) && !empty($datas['logTypes'])) {
                      foreach ($datas['logTypes'] as $key => $value) {
                           $this->db->insert($this->tbl_users_logistics, array(
                               'ulog_user_id' => $userId,
                               'ulog_logistics_id' => $value,
                               'ulog_added_by' => $this->uid
                           ));
                      }
                 }

                 return true;
            }
            return false;
       }

       function addImages($data) {
            $this->db->insert($this->tbl_logistics_comp_images, $data);
            return true;
       }

       function removeImage($id) {
            if ($id) {
                 $this->db->where('lci_id', $id);
                 $data = $this->db->get($this->tbl_logistics_comp_images)->row_array();
                 if (isset($data['lci_image']) && !empty($data['lci_image'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'logistics/' . $data['lci_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'logistics/' . $data['lci_image']);
                      }
                      if (file_exists(FILE_UPLOAD_PATH . 'logistics/thumb_' . $data['lci_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'logistics/thumb_' . $data['lci_image']);
                      }
                      $this->db->where('lci_id', $id);
                      if ($this->db->delete($this->tbl_logistics_comp_images)) {
                           generate_log(array(
                               'log_title' => 'Delete image',
                               'log_desc' => 'Removed shop image',
                               'log_controller' => strtolower(__CLASS__),
                               'log_action' => 'D',
                               'log_ref_id' => $id,
                               'log_added_by' => $this->uid,
                           ));
                           return true;
                      } else {
                           generate_log(array(
                               'log_title' => 'Delete image',
                               'log_desc' => 'Error while removing shop image',
                               'log_controller' => strtolower(__CLASS__),
                               'log_action' => 'D',
                               'log_ref_id' => $id,
                               'log_added_by' => $this->uid,
                           ));
                           return false;
                      }
                 }
            }
            return false;
       }

       function setDefaultImage($id) {

            $image = $this->db->get_where($this->tbl_logistics_comp_images, array('lci_id' => $id))->row_array();
            if (!empty($image)) {
                 $this->db->where('lci_user_id', $image['lci_user_id']);
                 $this->db->update($this->tbl_logistics_comp_images, array('lci_default' => 0));

                 $this->db->where('lci_id', $id);
                 if ($this->db->update($this->tbl_logistics_comp_images, array('lci_default' => 1))) {
                      return true;
                 }
            }
            return false;
       }

       function getDefaultImage($userId) {
            $image = $this->db->get_where($this->tbl_logistics_comp_images, array('lci_user_id' => $userId, 'lci_default' => 1))->row_array();
            if (!empty($image)) {
                 return UPLOAD_PATH . 'logistics/' . $image['lci_image'];
            } else {
                 $image = $this->db->order_by('lci_id', 'ASC')->get_where($this->tbl_logistics_comp_images, array('lci_user_id' => $userId))->row_array();

                 if (!empty($image)) {
                      return UPLOAD_PATH . 'logistics/' . $image['lci_image'];
                 }
            }
            return null;
       }

          function getImageAlt($userId) {
            $image = $this->db->get_where($this->tbl_logistics_comp_images, array('lci_user_id' => $userId, 'lci_default' => 1))->row_array();
            if (!empty($image['ici_alttag'])) 
                 return  $image['ici_alttag'];
            else {
                 $image = $this->db->order_by('lci_id', 'ASC')->get_where($this->tbl_logistics_comp_images, array('lci_user_id' => $userId))->row_array();
                 if (!empty($image['ici_alttag'])) 
                          return  $image['ici_alttag'];
            }
            return 'logistics image';
       }

       function getLogisticsServices($id = '') {
            if (!empty($id)) {
                 $this->db->select($this->tbl_logistics_services . '.*,' . $this->tbl_users . '.*')
                         ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_logistics_services . '.logs_logistics_id');
                 $data = $this->db->get_where($this->tbl_logistics_services, array($this->tbl_logistics_services . '.logs_id' => $id))->row_array();
                 if (!empty($data)) {
                      $data['images'] = $this->db->get_where($this->tbl_logistics_services_images, array('logsi_service_id' => $id))->result_array();
                      return $data;
                 }
            }
            $this->db->select($this->tbl_logistics_services . '.*,' . $this->tbl_users . '.*')
                    ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_logistics_services . '.logs_logistics_id');
            return $this->db->get($this->tbl_logistics_services)->result_array();
       }

       function newService($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_logistics_services, $data);
                 return $this->db->insert_id();
            }
            return false;
       }

       function addServiceImages($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_logistics_services_images, $data);
            } else {
                 return false;
            }
       }

       function getServiceDefaultImage($userId) {
            $image = $this->db->get_where($this->tbl_logistics_services_images, array('logsi_service_id' => $userId))->row_array();
            if (!empty($image)) {
                 return UPLOAD_PATH . 'logistics/' . $image['logsi_image'];
            }
            return null;
       }

       function updateService($data) {
            if (isset($data['service']) && isset($data['logs_id'])) {
                 $this->db->where('logs_id', $data['logs_id']);
                 $this->db->update($this->tbl_logistics_services, $data['service']);
                 return $data['logs_id'];
            }
            return false;
       }

       function removeService($id) {
            if ($id) {
                 $this->db->where('logs_id', $id);
                 $this->db->delete($this->tbl_logistics_services);
                 $this->removeServiceImage($id);
                 return true;
            }
            return false;
       }

       function removeServiceImage($id) {
            if ($id) {
                 $this->db->where('logsi_id', $id);
                 $data = $this->db->get($this->tbl_logistics_services_images)->row_array();
                 if (isset($data['logsi_image']) && !empty($data['logsi_image'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'logistics/' . $data['logsi_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'logistics/' . $data['logsi_image']);
                      }
                      if (file_exists(FILE_UPLOAD_PATH . 'logistics/thumb_' . $data['logsi_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'logistics/thumb_' . $data['logsi_image']);
                      }
                      $this->db->where('logsi_id', $id);
                      if ($this->db->delete($this->tbl_logistics_services_images)) {
                           generate_log(array(
                               'log_title' => 'Delete image',
                               'log_desc' => 'Removed logistics service image',
                               'log_controller' => 'remove-log-service-img',
                               'log_action' => 'D',
                               'log_ref_id' => $id,
                               'log_added_by' => $this->uid,
                           ));
                           return true;
                      } else {
                           generate_log(array(
                               'log_title' => 'Delete image',
                               'log_desc' => 'Error while removing logistics service image',
                               'log_controller' => 'remove-log-service-img',
                               'log_action' => 'D',
                               'log_ref_id' => $id,
                               'log_added_by' => $this->uid,
                           ));
                           return false;
                      }
                 }
            }
            return false;
       }

       function newLogisticsQuote($datas) {
            if (!empty($datas)) {
                 $datas['quote']['logq_added_by'] = $this->uid;
                 $this->db->insert($this->tbl_logistics_quotes, $datas['quote']);
                 $quoteId = $this->db->insert_id();
                 if (!empty($quoteId) && (isset($datas['logTypes']) && !empty($datas['logTypes']))) {
                      foreach ($datas['logTypes'] as $key => $value) {
                           $this->db->insert($this->tbl_logistics_quotes_types, array(
                               'lqt_quote' => $quoteId,
                               'lqt_type' => $value
                           ));
                      }
                 }
                 generate_log(array(
                     'log_title' => 'Logistics quote',
                     'log_desc' => 'New logistics quote',
                     'log_controller' => 'new-logistic-quote',
                     'log_action' => 'D',
                     'log_ref_id' => $quoteId,
                     'log_added_by' => $this->uid,
                 ));
                 return true;
            }
            return false;
       }

       function requestQuotes($id = '') {
            if (!empty($id)) {

                 $this->db->where('logq_id', $id);
                 $this->db->update($this->tbl_logistics_quotes, array('logq_viewed_on' => date('Y-m-d h:i:s')));

                 $data = $this->db->select($this->tbl_logistics_quotes . '.*,' . $this->tbl_users . '.*,' . $this->tbl_logistics_services . '.*')
                                 ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_logistics_quotes . '.logq_logistics')
                                 ->join($this->tbl_logistics_services, $this->tbl_logistics_services . '.logs_id = ' .
                                         $this->tbl_logistics_quotes . '.logq_service', 'LEFT')
                                 ->get_where($this->tbl_logistics_quotes, array($this->tbl_logistics_quotes . '.logq_id' => $id))->row_array();
                 if (!empty($data)) {
                      $data['types'] = $this->db->select($this->tbl_logistics_quotes_types . '.*,' . $this->tbl_logistics_types . '.*')
                                      ->join($this->tbl_logistics_types, $this->tbl_logistics_types . '.logt_id = ' . $this->tbl_logistics_quotes_types . '.lqt_type')
                                      ->get_where($this->tbl_logistics_quotes_types, array($this->tbl_logistics_quotes_types . '.lqt_quote' => $id))->result_array();
                 }
                 return $data;
            }
            return $this->db->select($this->tbl_logistics_quotes . '.*,' . $this->tbl_users . '.*')
                            ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_logistics_quotes . '.logq_logistics')
                            ->get($this->tbl_logistics_quotes)->result_array();
       }

       function delete($id) {
            if (!empty($id)) {
                 if (!$this->db->get_where($this->tbl_logistics_services, array('logs_logistics_id' => $id))->row_array()) {
                      $this->ion_auth->delete_user($id);
                      $this->removeImage($id);
                      $this->db->where('lcs_user_id', $id)->delete($this->tbl_logistics_comp_specification);
                      return true;
                 } else {
                      return false;
                 }
            }
            return false;
       }

       function removeImages($userId) {
            if ($userId) {
                 $this->db->where('lci_user_id', $userId);
                 $data = $this->db->get($this->tbl_logistics_comp_images)->result_array();
                 if (!empty($data)) {
                      foreach ($data as $key => $value) {
                           if (isset($value['lci_image']) && !empty($value['lci_image'])) {
                                if (file_exists(FILE_UPLOAD_PATH . 'logistics/' . $value['lci_image'])) {
                                     unlink(FILE_UPLOAD_PATH . 'logistics/' . $value['lci_image']);
                                }
                                if (file_exists(FILE_UPLOAD_PATH . 'logistics/thumb_' . $value['lci_image'])) {
                                     unlink(FILE_UPLOAD_PATH . 'logistics/thumb_' . $value['lci_image']);
                                }
                                $this->db->where('lci_id', $value['lci_id']);
                                if ($this->db->delete($this->tbl_logistics_comp_images)) {
                                     generate_log(array(
                                         'log_title' => 'Delete image',
                                         'log_desc' => 'Removed shop image',
                                         'log_controller' => strtolower(__CLASS__),
                                         'log_action' => 'D',
                                         'log_ref_id' => $value['lci_id'],
                                         'log_added_by' => $this->uid,
                                     ));
                                } else {
                                     generate_log(array(
                                         'log_title' => 'Delete image',
                                         'log_desc' => 'Error while removing shop image',
                                         'log_controller' => strtolower(__CLASS__),
                                         'log_action' => 'D',
                                         'log_ref_id' => $value['lci_id'],
                                         'log_added_by' => $this->uid,
                                     ));
                                }
                           }
                      }
                      return false;
                 }
            }
            return false;
       }

       function deleteServiceImages($id) {
            if (!empty($id)) {
                 $this->db->where('logsi_service_id', $id);
                 $data = $this->db->get($this->tbl_logistics_services_images)->result_array();
                 if (!empty($data)) {
                      foreach ($data as $key => $value) {
                           if (isset($value['logsi_image']) && !empty($value['logsi_image'])) {
                                if (file_exists(FILE_UPLOAD_PATH . 'logistics/' . $value['logsi_image'])) {
                                     unlink(FILE_UPLOAD_PATH . 'logistics/' . $value['logsi_image']);
                                }
                                if (file_exists(FILE_UPLOAD_PATH . 'logistics/thumb_' . $value['logsi_image'])) {
                                     unlink(FILE_UPLOAD_PATH . 'logistics/thumb_' . $value['logsi_image']);
                                }
                                $this->db->where('logsi_id', $value['logsi_id']);
                                if ($this->db->delete($this->tbl_logistics_services_images)) {
                                     generate_log(array(
                                         'log_title' => 'Delete image',
                                         'log_desc' => 'Removed logistics service image',
                                         'log_controller' => strtolower(__CLASS__),
                                         'log_action' => 'D',
                                         'log_ref_id' => $value['logsi_id'],
                                         'log_added_by' => $this->uid,
                                     ));
                                } else {
                                     generate_log(array(
                                         'log_title' => 'Delete image',
                                         'log_desc' => 'Error while removing logistics service image',
                                         'log_controller' => strtolower(__CLASS__),
                                         'log_action' => 'D',
                                         'log_ref_id' => $value['logsi_id'],
                                         'log_added_by' => $this->uid,
                                     ));
                                }
                           }
                      }
                      return false;
                 }
            }
            return false;
       }

       function searchLogistics($element = '', $limit = 0, $index = 0) {
            if (!empty($element)) {
                 $this->db->like($this->tbl_users . '.usr_first_name', $element, 'both');
            }
            $data['count'] = $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->where(array($this->tbl_groups . '.id' => 6, $this->tbl_users . '.usr_active' => 1))
                            ->get($this->tbl_users)->num_rows();

            if (!empty($limit)) {
                 $this->db->limit($limit, $index);
            }

            if (!empty($element)) {
                 $this->db->like($this->tbl_users . '.usr_first_name', $element, 'both');
            }

            $data['userDetails'] = $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' .
                                    $this->tbl_groups . '.name as group_name,' . $this->tbl_countries . '.*,' . $this->tbl_states . '.*')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_users . '.usr_country', 'LEFT')
                            ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_users . '.usr_state', 'LEFT')
                            ->where(array($this->tbl_groups . '.id' => 6, $this->tbl_users . '.usr_active' => 1))
                            ->get($this->tbl_users)->result_array();

            if (!empty($data['userDetails'])) {
                 foreach ($data['userDetails'] as $ukey => $uvalue) {
                      $id = $uvalue['usr_id'];
                      $data['userDetails'][$ukey]['images'] = $this->db->get_where($this->tbl_logistics_comp_images, array('lci_user_id' => $id))->result_array();
                      $data['userDetails'][$ukey]['specifications'] = $this->db->get_where($this->tbl_logistics_comp_specification, array('lcs_user_id' => $id))->result_array();
                      $data['userDetails'][$ukey]['logisticsTypesSelected'] = $this->db->select($this->tbl_users_logistics . '.*,' . $this->tbl_logistics_types . '.*')
                                      ->join($this->tbl_logistics_types, $this->tbl_logistics_types . '.logt_id = ' . $this->tbl_users_logistics . '.ulog_logistics_id')
                                      ->get_where($this->tbl_users_logistics, array($this->tbl_users_logistics . '.ulog_user_id' => $id))->result_array();

                      $data['userDetails'][$ukey]['logisticsServices'] = $this->db->get_where($this->tbl_logistics_services, array('logs_logistics_id' => $id))->result_array();

                      if (isset($data['userDetails'][$ukey]) && !empty($data['userDetails'][$ukey]['logisticsServices'])) {
                           foreach ($data['userDetails'][$ukey]['logisticsServices'] as $key => $value) {
                                $data['userDetails'][$ukey]['logisticsServices'][$key]['images'] = $this->db->get_where($this->tbl_logistics_services_images, array('logsi_service_id' => $value['logs_id']))->result_array();
                           }
                      }
                 }
            }
            return $data;
       }

       function removeBG($uId) {
            if (!empty($uId)) {
                 $this->db->where('usr_id', $uId);
                 $data = $this->db->get($this->tbl_users)->row_array();
                 if (!empty($data)) {
                      if (isset($data['usr_banner']) && !empty($data['usr_banner'])) {
                           if (file_exists(FILE_UPLOAD_PATH . 'logistics/' . $data['usr_banner'])) {
                                unlink(FILE_UPLOAD_PATH . 'logistics/' . $data['usr_banner']);
                           }
                           if (file_exists(FILE_UPLOAD_PATH . 'logistics/thumb_' . $data['usr_banner'])) {
                                unlink(FILE_UPLOAD_PATH . 'logistics/thumb_' . $data['usr_banner']);
                           }
                           $this->db->where('usr_id', $uId);
                           if ($this->db->update($this->tbl_users, array('usr_banner' => ''))) {
                                generate_log(array(
                                    'log_title' => 'Delete image',
                                    'log_desc' => 'Removed logistics background image',
                                    'log_controller' => strtolower(__CLASS__),
                                    'log_action' => 'D',
                                    'log_ref_id' => $uId,
                                    'log_added_by' => $this->uid,
                                ));
                           } else {
                                generate_log(array(
                                    'log_title' => 'Delete image',
                                    'log_desc' => 'Error while removing logistics background image',
                                    'log_controller' => strtolower(__CLASS__),
                                    'log_action' => 'D',
                                    'log_ref_id' => $uId,
                                    'log_added_by' => $this->uid,
                                ));
                           }
                      }
                      return true;
                 }
            }
            return false;
       }

  }
  