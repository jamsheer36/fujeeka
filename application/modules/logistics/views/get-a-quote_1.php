<form action="<?php echo site_url('logistics/newLogisticsQuote'); ?>" class="frmLogisticsQuote" accept-charset="utf-8" id="qryform" 
      role="form" method="post" enctype="multipart/form-data" data-parsley-validate="true">                        
     <div class="col-md-12">
          <?php foreach ($logisticsTypesSelected as $key => $value) {?>
                 <label class="checkbox-inline">
                      <input name="logTypes[]" type="checkbox" value="<?php echo $value['logt_id'];?>"><?php echo $value['logt_title'];?>
                 </label>
            <?php }?>
     </div>
     <div class="form-group">
          <label></label>
          <input required data-parsley-required-message="Enter CBM" type="text" class="form-control" placeholder="CBM" name="quote[logq_cbm]">
     </div>
     <input type="hidden" name="quote[logq_service]" value="0"/>
     <input type="hidden" name="quote[logq_logistics]" value="<?php echo isset($userDetails['usr_id']) ? $userDetails['usr_id'] : ''; ?>"/>
     <div class="form-group">
          <label></label>
          <input type="text" required class="form-control" placeholder="Weight" name="quote[logq_weight]">
     </div>
     <div class="form-group">
          <label></label>
          <input type="text" required class="form-control" placeholder="Product Type" name="quote[logq_prod_type]">
     </div>
     <div class="form-group">
          <label></label>
          <input type="email" required class="form-control" placeholder="Enter your email" name="quote[logq_email]">
     </div>
     <div class="form-group">
          <label></label>
          <input type="text" required class="form-control" placeholder="Enter your mobile numer" name="quote[logq_mobile]">
     </div>
     <div class="form-group">
          <label></label>
          <input type="text" required class="form-control" placeholder="Location from" name="quote[logq_loc_from]">
     </div>
     <div class="form-group">
          <label></label>
          <input type="text" required class="form-control" placeholder="Location to" name="quote[logq_loc_to]">
     </div>

     <div class="form-group">
          <label></label>
          <textarea name="quote[logq_desc]" class="form-control" rows="3" placeholder="Description"></textarea>
     </div>
     <button type="submit" class="btnSubmitLogisticQuote btn btn-success btn-green enq_submit">Submit</button>
</form>