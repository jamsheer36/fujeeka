<!-- Banner -->
<div class="sr-banner">
     <img src="images/banners/logistic_register.jpg" alt="Register as Supplier" class="img-fluid">
</div>
<!-- Banner ends -->

<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
               <div class="col-md-7">
                    <div class="cntent">
                         <h2 class="heading2">
                              Why register as Logistic ?
                         </h2>
                         <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                         <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry.   </p>
                    </div>
               </div>

               <div class="col-md-5">
                    <div class="card card-frms">
                         <h2 class="heading2">
                              REGISTER AS LOGISTIC
                         </h2>

                         <form data-parsley-validate="true" class="frmSupplierRegister" action="<?php echo site_url($controller . '/register');?>">
                              <div class="col-md-12">
                                   <?php foreach ((array) $logisticsTypes as $key => $value) {?>
                                          <label class="checkbox-inline">
                                               <input name="logistics[]" type="checkbox" value="<?php echo $value['logt_id'];?>"><?php echo $value['logt_title'];?>
                                          </label>
                                     <?php }
                                   ?>
                              </div>

                              <div class="form-group">
                                   <input type="text" name="user[usr_first_name]" class="form-control" id="sphone" placeholder="Name"
                                          required data-parsley-required-message="Enter your name">
                              </div>

                              <div class="form-group">
                                   <input type="email" name="user[usr_email]" class="form-control" id="sEmail1"
                                          required data-parsley-required-message="Enter valid email"
                                          aria-describedby="emailHelp" placeholder="Enter email">
                                   <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                              </div>
                              <div class="form-group">
                                   <input type="number" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" name="user[usr_phone]" class="form-control" id="sphone"
                                          placeholder="Phone">
                              </div>
                              <div class="form-group">
                                   <textarea name="user[usr_desc]" class="form-control" id="sComments" placeholder="Comments" rows="3"></textarea>
                              </div>

                              <div class="w-100 text-center">
                                   <button type="submit" class="btn btn-success btn-green btn-bold btnSupplierRegister">Send Request</button>
                              </div>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</section>

<style>
     p.parsley-success {
          color: #468847;
          background-color: #DFF0D8;
          border: 1px solid #D6E9C6
     }

     p.parsley-error {
          color: #B94A48;
          background-color: #F2DEDE;
          border: 1px solid #EED3D7
     }
     li.parsley-required {
          font-size: 10px;
     }
     ul.parsley-errors-list {
          list-style: none;
          color: #E74C3C;
          padding-left: 0
     }

     input.parsley-error,
     select.parsley-error,
     textarea.parsley-error {
          background: #FAEDEC;
          border: 1px solid #E85445
     }

     .btn-group .parsley-errors-list {
          display: none
     }
</style>