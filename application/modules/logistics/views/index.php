<div class="container-fluid logistic_banner">
     <div class="container">
          <div class="logictitle2">
               <div class="row">
                    <div class="col-md-12">
                         <?php echo isset($banner['bnr_desc']) ? $banner['bnr_desc'] : '';?>
                    </div>
               </div>
          </div>
     </div>
     <?php
       if (!empty($banner) && isset($banner['bnr_image'])) {
            echo img(array('src' => UPLOAD_PATH . 'banner/' . $banner['bnr_image'], 'alt' => ''));
       }
     ?>
</div>
<div class="container">
     <div class="logistic_about2">
          <div class="row">
               <div class="col-md-12 text-center">
                    <div class="logiabout2">
                         <div class="logiabout2_rfq">
                              <h2 class="heading2">LOGISTIC RFQ</h2>
                              <h4 class="heading3 green-txt">One Request, Multiple Quotes</h4>
                              <div class="col-md-12 logiabout2_rfqcon">
                                   <p class="m-grey-txt" style="margin-top: 50px !important;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown 
                                        printer took a galley of type and scrambled it to make a type specimen book.</p>
                              </div>
                              <a href="<?=site_url('logistics#lpartners')?>" class="btn btn-success btn-green font-weight-bold lg" title="Get a Quote" 
                                      >GET A QUOTE</a>
                              <img src="images/banners/rfq.jpg">
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
<div class="container-fluid logistic_shipping2">
     <div class="container">
          <div class="row">
               <div class="col-md-12"><h2 class="heading2">SHIPPING METHODS</h2></div>
               <div class="col-md-3">
                    <div class="logistic_ship logistic_shipping_lcl">
                         <img src="images/icons/lcl.png" class="face">
                         <p><b>Less-Than-Container Load [ LCL ]</b></p>
                         <p class=" m-grey-txt">LCL is a cost-efficient option for buyers who want to save money with ocean freight, but don't have enough volume to fill an entire shipping container</p>

                    </div>
               </div>
               <div class="col-md-3">
                    <div class="logistic_ship logistic_shipping_fcl">
                         <img src="images/icons/fcl.png" class="face">
                         <p><b>Full Container Load [ FCL ]</b></p>
                         <p class=" m-grey-txt">LCL is a cost-efficient option for buyers who want to save money with ocean freight, but don't have enough volume to fill an entire shipping container</p>

                    </div>
               </div>
               <div class="col-md-3">
                    <div class="logistic_ship logistic_shipping_air">
                         <img src="images/icons/air.png" class="face">
                         <p><b>Air Door To Door</b></p>
                         <p class=" m-grey-txt">LCL is a cost-efficient option for buyers who want to save money with ocean freight, but don't have enough volume to fill an entire shipping container</p>

                    </div>
               </div>
               <div class="col-md-3">
                    <div class="logistic_ship logistic_shipping_door" id="logistic_shipping_door">
                         <img src="images/icons/door.png" class="face">
                         <p><b>Air cargo</b></p>
                         <p class=" m-grey-txt">LCL is a cost-efficient option for buyers who want to save money with ocean freight, but don't have enough volume to fill an entire shipping container</p>
                    </div>
               </div>
          </div>
     </div>
</div>


<style>
     .slide {height: auto;}
</style>
<?php if (!empty($inquires)) {?>
       <div class="container" id="lpartners">
            <div class="row">
                 <div class="col-md-12">
                      <div class="col-md-12 logidown pt-5 pt-2"><h2 class="heading2">LOGISTIC PARTNERS</h2></div>
                      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
                           <div class="carousel-inner">
                                <?php
                                foreach ((array) $inquires as $key => $value) {
                                     echo ($key != 0 && $key % 4 == 0) ? '</div></div>' : '';
                                     $active = $key == 0 ? 'active' : '';
                                     echo ($key == 0 || $key % 4 == 0) ? '<div class="item carousel-item ' . $active . '"><div class="row">' : '';
                                     ?>
                                     <div class="col-sm-3">
                                          <div class="thumb-wrapper">
                                               <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                               <div class="img-box">
                                                    <?php
                                                    $img = $this->logistics->getDefaultImage($value['usr_id']);
                                                    $alt = $this->logistics->getImageAlt($value['usr_id']);
                                                    
                                                    echo img(array('src' => $img, 'alt' => $alt));
                                                    ?>
                                               </div>
                                               <div class="thumb-content">
                                                    <h6><?php echo $value['usr_first_name'];?></h6>									
                                                    <div class="star-rating">

                                                    </div>
                                                    <a href="<?php echo site_url($controller . '/get-quote/' . encryptor($value['usr_id']));?>" class="icons-bx btn btn-primary">Get Quote</a>
                                               </div>
                                          </div>
                                     </div>
                                     <?php
                                     echo ($key == (count($inquires) - 1)) ? '</div></div>' : '';
                                }
                                ?>
                           </div>
                           <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                           </a>
                           <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                           </a>
                      </div>
                 </div>
            </div>
       </div>
  <?php }?>

<div class="container-fluid logistic_register">
     <div class="container">
          <div class="row">
               <div class="col-md-12">
                    <h2>Logistic Partner</h2>
                    <p>If you can assure safe and reliable logistic services from China, Register as a logistic partner with fujeeka now</p>
                    <a href="<?php echo site_url('logistics/register');?>">REGISTER</a>
               </div>
          </div>
     </div>
</div>

<div id="inq-pop" class="modal fade inquiry show" role="dialog" style=" padding-right: 17px;">
     <div class="modal-dialog">
          <!--Modal content-->
          <div class="modal-content">
               <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
               <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <div class="enq-container">
                         <div class="left-sec">
                              <div class="pic">
                                   <img src="images/icons/logi.png" alt="" class="img-fluid">
                              </div>
                              <div class="cont" style="top: 20%;text-align: center">
                                   <h2>GET A QUOTE</h2>
                                   <p>Get a Quote in Logistic Control</p>
                              </div>
                         </div>
                         <div class="frms divGetAQuoteForm">

                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>