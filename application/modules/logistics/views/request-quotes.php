<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Logistics</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Logistics</th>
                                        <th>CBM</th>
                                        <th>Weight</th>
                                        <th>Product type</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Location from</th>
                                        <th>Location to</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $requests as $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url('logistics/request_quotes_details/' . encryptor($value['logq_id']));?>">
                                               <td class="trVOE"><?php echo $value['usr_first_name'];?></td>
                                               <td class="trVOE"><?php echo $value['logq_cbm'];?></td>
                                               <td class="trVOE"><?php echo $value['logq_weight'];?></td>
                                               <td class="trVOE"><?php echo $value['logq_prod_type'];?></td>
                                               <td class="trVOE"><?php echo $value['logq_email'];?></td>
                                               <td class="trVOE"><?php echo $value['logq_mobile'];?></td>
                                               <td class="trVOE"><?php echo $value['logq_loc_from'];?></td>
                                               <td class="trVOE"><?php echo $value['logq_loc_to'];?></td>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>