<div class="container">
     <div class="row">
          <div class="col-md-6">
               <div class="service">
                    <div class="row">
                         <div class="col-3">
                              <?php
                                $image = isset($images[0]['lci_image']) ? $images[0]['lci_image'] : '';
                                echo img(array('src' => UPLOAD_PATH . 'logistics/' . $image, 'class' => 'img-thumbnail'));
                              ?>
                         </div>
                         <div class="col-9">
                              <h4><?php echo isset($userDetails['usr_first_name']) ? $userDetails['usr_first_name'] : '';?></h4>
                         </div>
                         <div class="col-12">
                              <p><?php echo isset($userDetails['usr_description1']) ? $userDetails['usr_description1'] : '';?></p>
                         </div>
                    </div>
                    <div id="accordion">
                         <?php
                           if (!empty($logisticsServices)) {
                                foreach ($logisticsServices as $key => $value) {
                                     ?>
                                     <div class="card">
                                          <div class="card-header">
                                               <a class="card-link collapsed" data-toggle="collapse" href="#collapse<?php echo $value['logs_id']; ?>" aria-expanded="false">
                                                    <h5><?php echo $value['logs_service_title'];?></h5>
                                               </a>
                                          </div>
                                          <div id="collapse<?php echo $value['logs_id']; ?>" class="collapse <?php echo $key == 0 ? 'show' : ''; ?>" data-parent="#accordion" style="">
                                               <div class="card-body">
                                                    <?php
                                                    $image = isset($value['images'][0]['logsi_image']) ? $value['images'][0]['logsi_image'] : '';
                                                    echo img(array('src' => UPLOAD_PATH . 'logistics/' . $image));
                                                    ?>

                                                    <div class="companiesss">
                                                         <?php echo $value['logs_desc'];?>
                                                    </div>
                                               </div>
                                          </div>
                                     </div>
                                     <?php
                                }
                           }
                         ?>
                    </div>
               </div>
          </div>

          <div class="col-md-6">
               <div class="getquote">
                    <div class="col-md-12 getheading"><h2 class="heading2">GET A QUOTE</h2></div>
                    <div class="frms">
                         <form action="<?php echo site_url('logistics/newLogisticsQuote');?>" data-parsley-validate="true" class="frmLogisticsQuote"
                               accept-charset="utf-8" id="qryform" role="form" method="post" enctype="multipart/form-data" novalidate="" data-parsley-validate="true">  
                              <input type="hidden" name="quote[logq_logistics]" value="<?php echo isset($userDetails['usr_id']) ? $userDetails['usr_id'] : '';?>"/>
                              <?php if (!empty($logisticsTypesSelected)) {?>
                                     <div class="col-md-12">
                                          <p>LOGISTIC TYPES</p>
                                          <div class="row qty">
                                               <?php foreach ($logisticsTypesSelected as $key => $value) {?>
                                                    <label class="chkcontainer">
                                                         <input type="checkbox" name="logTypes[]" value="<?php echo $value['logt_id'];?>"/>
                                                         <span class="checkmark"></span>
                                                    </label>
                                                    <p class="m-grey-txt" style="margin-right: 5px;"><?php echo $value['logt_title'];?>
                                                         <span for="chkConfirm" generated="true" class="error" style="padding-top: 10px;"></span>
                                                         <br><span id="check_err"></span>
                                                    </p>
                                               <?php }?>
                                          </div>
                                     </div>
                                <?php }?>
                              <div class="form-group">
                                   <label></label>
                                   <input type="text" class="form-control" placeholder="CBM" name="quote[logq_cbm]" required="required" 
                                          data-parsley-required-message="Please enter CBM">
                              </div>

                              <div class="form-group">
                                   <label></label>
                                   <input type="text" class="form-control" placeholder="Weight" name="quote[logq_weight]" required="required" 
                                          data-parsley-required-message="Please enter weight">
                              </div>
                              <div class="form-group">
                                   <label></label>
                                   <input type="text" class="form-control" placeholder="Product Type" name="quote[logq_prod_type]" required="required" 
                                          data-parsley-required-message="Please enter product type">
                              </div>
                              <div class="form-group">
                                   <label></label>
                                   <select class="form-control" name="quote[logq_service]" required="required" 
                                          data-parsley-required-message="Please choose service">
                                        <option value="">Choose Services</option>
                                        <?php
                                          if (!empty($logisticsServices)) {
                                               foreach ($logisticsServices as $key => $value) {
                                                    ?>
                                                    <option value="<?php echo $value['logs_id'];?>"><?php echo $value['logs_service_title'];?></option>
                                                    <?php
                                               }
                                          }
                                        ?>
                                   </select>
                              </div>
                              <div class="form-group">
                                   <label></label>
                                   <input type="email" class="form-control" placeholder="Email" name="quote[logq_email]" required="required" 
                                          data-parsley-required-message="Please enter valid email">
                              </div>
                              <div class="form-group">
                                   <label></label>
                                   <input type="text" class="form-control" placeholder="Mobile" name="quote[logq_mobile]" required="required" 
                                          data-parsley-required-message="Please enter valid mobile">
                              </div>
                              <div class="form-group">
                                   <label></label>
                                   <input type="text" class="form-control" placeholder="Location from" name="quote[logq_loc_from]" required="required" 
                                          data-parsley-required-message="Please enter location from">
                              </div>
                              <div class="form-group">
                                   <label></label>
                                   <input type="text" class="form-control" placeholder="Location to" name="quote[logq_loc_to]" required="required" 
                                          data-parsley-required-message="Please enter location to">
                              </div>

                              <div class="form-group">
                                   <label></label>
                                   <textarea name="quote[logq_desc]" class="form-control" placeholder="Description"></textarea>
                              </div>
                              <button type="submit" class="btnSubmitLogisticQuote btn btn-success btn-green enq_submit">Submit</button>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>                  
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     /* The container */
     .chkcontainer {
          display: block;
          position: relative;
          padding-left: 25px;
          margin-bottom: 12px;
          cursor: pointer;
          font-size: 22px;
          -webkit-user-select: none;
          -moz-user-select: none;
          -ms-user-select: none;
          user-select: none;
     }

     /* Hide the browser's default checkbox */
     .chkcontainer input {
          position: absolute;
          opacity: 0;
          cursor: pointer;
     }

     /* Create a custom checkbox */
     .checkmark {
          position: absolute;
          top: 0;
          left: 0;
          height: 18px;
          width: 18px;
          background-color: #eee;
          border: 1px solid #c1bfbf;
     }

     /* On mouse-over, add a grey background color */
     .chkcontainer:hover input ~ .checkmark {
          background-color: #ccc;
     }

     /* When the checkbox is checked, add a blue background */
     .chkcontainer input:checked ~ .checkmark {
          background-color: #00b904;
          border: 1px solid #00b904;
     }

     /* Create the checkmark/indicator (hidden when not checked) */
     .checkmark:after {
          content: "";
          position: absolute;
          display: none;
     }

     /* Show the checkmark when checked */
     .chkcontainer input:checked ~ .checkmark:after {
          display: block;
     }

     /* Style the checkmark/indicator */
     .chkcontainer .checkmark:after {
          left: 5px;
          top: 0px;
          width: 7px;
          height: 14px;
          border: solid white;
          border-width: 0 3px 3px 0;
          -webkit-transform: rotate(45deg);
          -ms-transform: rotate(45deg);
          transform: rotate(45deg);
     }
     .parsley-required, .parsley-type {
          font-size: 10px;
          font-weight: bold;
          color: red;
          float: left;
          width: 100%;
          list-style: none;
          margin-left: -33px;
     }
</style>
