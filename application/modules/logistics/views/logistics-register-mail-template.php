<html xmlns="http://www.w3.org/1999/xhtml"><head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <title>Shine Gold and diamonds</title>
          <style type="text/css">
               /* Client-specific Styles */
               #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
               body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
               /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
               .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
               .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
               #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
               img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
               a img {border:none;}
               .image_fix {display:block;}
               p {margin: 0px 0px !important;}

               table td {border-collapse: collapse;}
               table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
               /*a {color: #e95353;text-decoration: none;text-decoration:none!important;}*/
               /*STYLES*/
               table[class=full] { width: 100%; clear: both; }
              .otp{ font-size:20px; font-weight:bold; }
          </style>
     </head>
     <body>
          <div class="block">
               <!-- Start of preheader -->
               <table width="100%" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader">
                    <tbody>
                         <tr>
                              <td width="100%">
                                   <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="">
                                        <tbody>
                                             <!-- Spacing -->
                                             <tr>
                                                  <td width="100%" height="5"></td>
                                             </tr>
                                             <!-- Spacing -->
<!--                                             <tr>
                                                  <td align="right" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 10px;color: #999999" st-content="preheader">
                                                       If you cannot read this email, please  <a class="hlite" href="#" style="text-decoration: none; color: #bf1e2e">click here</a> 
                                                  </td>
                                             </tr>-->
                                             <!-- Spacing -->
                                             <tr>
                                                  <td width="100%" height="5"></td>
                                             </tr>
                                             <!-- Spacing -->
                                        </tbody>
                                   </table>
                              </td>
                         </tr>
                    </tbody>
               </table>
               <!-- End of preheader -->
          </div>
          <div class="block">
               <!-- start of header -->
               <table width="100%" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
                    <tbody> 
                         <tr> 
                              <td> 
                                   <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" hlitebg="edit" shadow="edit">
                                        <tbody> 
                                             <tr> 
                                                  <td> 
                                                       <!-- logo -->
                                                       <table width="600" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth"
                                                              style="border-left:1px solid #e6e6e6;border-right:1px solid #e6e6e6;border-top:1px solid #e6e6e6;">
                                                            <tbody>
                                                                 <tr>
                                                                      <td valign="middle" width="580" style="padding:0px;padding-top: 20px;" class="logo" align="center">
                                                                           <div class="imgpop">
                                                                                <a href="<?php echo site_url(); ?>">
                                                                                     <img src="<?php echo site_url(); ?>/assets/images/fujeeka-logo-g.png" alt="logo" border="0" 
                                                                                          style="display:block; border:none; outline:none; text-decoration:none;" st-image="edit" class="logo"/>
                                                                                </a>
                                                                           </div>
                                                                      </td> 
                                                                 </tr> 
                                                            </tbody> 
                                                       </table>
                                                       <!-- End of logo -->
                                                  </td> 
                                             </tr> 
                                        </tbody> 
                                   </table>
                              </td> 
                         </tr>
                    </tbody>
               </table>
               <!-- end of header -->
          </div>

          <div class="block">
               <!-- start of header -->
               <table width="100%" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
                    <tbody> 
                         <tr> 
                              <td> 
                                   <table width="600" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" hlitebg="edit" shadow="edit">
                                        <tbody> 
                                             <tr> 
                                                  <td> 
                                                       <!-- logo -->
                                                       <table width="600" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth" bgcolor="FFFFFF" style="border-left:1px solid #e6e6e6;border-right:1px solid #e6e6e6;"  >
                                                            <tbody>
                                                                 <tr>
                                                                      <!-- <td valign="middle" width="300" style="padding:10px 20px;font-family: Helvetica, arial, sans-serif; font-size: 12px;color: #414141; line-height:20px;"  align="left">
                                                                           <strong>Hi <?php echo $mail; ?></strong>,<br><br>
                                                                           Thanks for using fujeeka! Please confirm your email address by clicking on the link below.
                                                                           We'll communicate with you from time to time via email so it's important that we have an up-to-date email address on file
                                                                      </td> -->
                                                                      <td valign="middle" width="300" style="padding:10px 20px;font-family: Helvetica, arial, sans-serif; font-size: 12px;color: #414141; line-height:20px;"  align="left">
                                                                           <strong>Hi <?php echo $email; ?></strong>,<br><br>
                                                                           Thanks for using fujeeka! 
                                                                           We'll communicate with you from time to time via email so it's important that we have an up-to-date email address on file
                                                                      </td>
                                                                 </tr> 
                                                            </tbody> 
                                                       </table>
                                                       <!-- End of logo -->
                                                  </td> 
                                             </tr> 
                                        </tbody> 
                                   </table>
                              </td> 
                         </tr>
                    </tbody>
               </table>
               <!-- end of header -->
          </div>
          <div class="block">
               <!-- start of header -->
               <table width="100%" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
                    <tbody> 
                         <tr> 
                              <td> 
                                   <table width="600" bgcolor="#e6e6e6" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" hlitebg="edit" shadow="edit">
                                        <tbody> 
                                             <tr> 
                                                  <td> 
                                                       <!-- logo -->
                                                       <table width="600" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth" bgcolor="e6e6e6" >
                                                            <tbody>
                                                                 <tr>
                                                                      <td valign="middle" width="300" style="padding:10px 20px;font-family: Helvetica, arial, sans-serif; font-size: 12px;color: #414141; line-height:20px;"  align="left">
                                                                           Our INTERNATIONAL Free shipping policy shipping charges will be paid by company. won't Responsible for TAX custom and Excise duty VAT charges.<br/>
                                                                           &nbsp;&nbsp;&nbsp; *Minimum 'eight' days duration will take to make if the item not available<br/>
                                                                           &nbsp;&nbsp;&nbsp; *Company not responsible for gold lose Ring Resizing purpose 0.100 to 0.900 mill gram will be lose<br/>
                                                                           Check the box Dialog box (agree T&C Window Before Payment)

                                                                      </td>
                                                                 </tr> 
                                                            </tbody> 
                                                       </table>
                                                       <!-- End of logo -->

                                                  </td> 
                                             </tr> 
                                        </tbody> 
                                   </table>
                              </td> 
                         </tr>
                    </tbody>
               </table>
               <!-- end of header -->
          </div>
          <div class="block">
               <!-- start of header -->
               <table width="100%" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
                    <tbody> 
                         <tr> 
                              <td> 
                                   <table width="600" bgcolor="#e6e6e6" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" hlitebg="edit" shadow="edit">
                                        <tbody> 
                                             <tr> 
                                                  <td> 
                                                       <!-- logo -->
                                                       <table width="600" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth" bgcolor="e6e6e6"  style="border-top:1px solid #FFF;">
                                                            <tbody>
                                                                 <tr>
                                                                      <td valign="middle" width="600" style="padding:10px 20px;font-family: Helvetica, arial, sans-serif; font-size: 12px;color: #414141; line-height:20px;"  align="center">
                                                                           <a href="#" style="text-decoration: none; color: #414141;">About Us </a>| 
                                                                           <a href="#" style="text-decoration: none; color: #414141;">Contact Us </a> | 
                                                                           <a href="#" style="text-decoration: none; color: #414141;">Policies  </a>|
                                                                           <a href="#" style="text-decoration: none; color: #414141;"> Terms & Condition </a> | 
                                                                           Copyright © 2018 <?=site_url(); ?>

                                                                      </td>
                                                                 </tr> 
                                                            </tbody> 
                                                       </table>
                                                       <!-- End of logo -->
                                                  </td> 
                                             </tr> 
                                        </tbody> 
                                   </table>
                              </td> 
                         </tr>
                    </tbody>
               </table>
               <!-- end of header -->
          </div>
     </body>
</html>