<?php

  defined('BASEPATH') or exit('No direct script access allowed');
  require './vendor/autoload.php';

  class logistics extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Logistics';
            //   $this->assets_css = array('faq.css', 'logistic.css', 'slider.css');
            $this->load->library('upload');
            $this->load->model('logistics_model', 'logistics');
            require_once(APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php');
            $this->load->model('manage_banner/manage_banner_model', 'manage_banner');
            $this->mail = new PHPMailer;
       }

       function index() {
            $data['inquires'] = $this->logistics->getLogistics();
            $data['banner'] = $this->manage_banner->getSiteBanner(strtolower(__CLASS__));
            if($data['banner']){
               $data['banner'] = $data['banner'][0];
            }
            $this->template->set_layout('common');
            $this->render_page(strtolower(__CLASS__) . '/index', $data);
       }

       function listlogistics() {
            $data['inquires'] = $this->logistics->getLogistics();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       public function register() {
            if ($this->input->post()) {
                 $datas = $this->input->post('user');
                 if ($this->logistics->register($this->input->post())) {
                      /* Mail to supplier */
                      $this->mail->isSMTP();
                      $this->mail->Host = MAIL_HOST;
                      $this->mail->SMTPAuth = true;
                      $this->mail->Username = MAIL_USERNAME;
                      $this->mail->Password = MAIL_PASSWORD;
                      $this->mail->SMTPSecure = 'ssl';
                      $this->mail->Port = 465;
                      $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                      $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                      $this->mail->addAddress($datas['usr_email']);
                      $this->mail->Subject = 'New Logistic Registration';
                      $this->mail->isHTML(true);
                      $mailData['email'] = $datas['usr_email'];
                      $this->mail->Body = $this->load->view('logistics-register-mail-template', $mailData, true);
                      $this->mail->send();
                      /* Mail end */
                      die(json_encode(array('status' => 'success', 'msg' => 'We will get back to you')));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => 'Email already exists')));
                 }
            } else {
                 $this->page_title = 'Logistics registration';
                 $this->template->set_layout('common');
                 $data['logisticsTypes'] = $this->logistics->getLogisticsTypes();
                 $this->render_page(strtolower(__CLASS__) . '/register', $data);
            }
       }

       function inquires() {
            $data['inquires'] = $this->logistics->getPendingInqueries();
            $this->render_page(strtolower(__CLASS__) . '/inquires', $data);
       }

       function verify($id = '') {
            $id = encryptor($id, 'D');
            $data['userDetails'] = $this->ion_auth->user($id)->row_array();
            $data['selectedLogisticsTypes'] = $this->logistics->selectedLogisticsTypes($id);
            $data['country'] = get_country_list();
            $data['logisticsTypes'] = $this->logistics->getLogisticsTypes();
            $this->render_page(strtolower(__CLASS__) . '/edit', $data);
       }

       function add() {
            if (!empty($this->input->post())) {
                 // Background image
                 if (isset($_FILES['userAvatar']['name']) && !empty($_FILES['userAvatar']['name'])) {
                      $pixel = array();
                      $uploadData = array();
                      $newFileName = microtime(true) . clean_image_name($_FILES['userAvatar']['name']);
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'logistics/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $pixel['x1'][0] = $_POST['x11'];
                      $pixel['y1'][0] = $_POST['y11'];
                      $pixel['x2'][0] = $_POST['x21'];
                      $pixel['y2'][0] = $_POST['y21'];
                      $pixel['w'][0] = $_POST['w1'];
                      $pixel['h'][0] = $_POST['h1'];
                      if ($this->upload->do_upload('userAvatar')) {
                           $uploadData = $this->upload->data();
                           crop($uploadData, $pixel);
                           $_POST['users']['usr_banner'] = $uploadData['file_name'];
                      }
                 }

                 $userId = $this->logistics->add($this->input->post());

                 $x1 = $this->input->post('x12');
                 $fileCount = count($x1);
                 for ($j = 0; $j < $fileCount; $j++) {

                      $data = array();
                      $angle = array();
                      $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'logistics/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $angle['x1']['0'] = $_POST['x12'][$j];
                      $angle['x2']['0'] = $_POST['x22'][$j];
                      $angle['y1']['0'] = $_POST['y12'][$j];
                      $angle['y2']['0'] = $_POST['y22'][$j];
                      $angle['w']['0'] = $_POST['w2'][$j];
                      $angle['h']['0'] = $_POST['h2'][$j];
                      $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                      $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                      $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                      $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                      $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];

                      if (!$this->upload->do_upload('prd_image_tmp')) {
                           $error = array('error' => $this->upload->display_errors());
                      } else {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle, false);
                           $this->logistics->addImages(array('lci_user_id' => $userId, 'lci_image' => $data['file_name'],'ici_alttag' => $_POST['ici_alttag']) );
                      }
                 }
                 $this->session->set_flashdata('app_success', 'Logistics successfully added!');
                 redirect(strtolower(__CLASS__) . '/listlogistics');
            } else {
                 $data['country'] = get_country_list();
                 $data['logisticsTypes'] = $this->logistics->getLogisticsTypes();
                 $this->render_page(strtolower(__CLASS__) . '/add', $data);
            }
       }

       function edit($id = 0) {

            if (!empty($this->input->post())) {
                 $userId = $this->input->post('usr_id');
                 // Background image
                 if (isset($_FILES['userAvatar']['name']) && !empty($_FILES['userAvatar']['name'])) {
                      $pixel = array();
                      $uploadData = array();
                      $newFileName = microtime(true) . clean_image_name($_FILES['userAvatar']['name']);
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'logistics/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $pixel['x1'][0] = $_POST['x11'];
                      $pixel['y1'][0] = $_POST['y11'];
                      $pixel['x2'][0] = $_POST['x21'];
                      $pixel['y2'][0] = $_POST['y21'];
                      $pixel['w'][0] = $_POST['w1'];
                      $pixel['h'][0] = $_POST['h1'];
                      if ($this->upload->do_upload('userAvatar')) {
                           $uploadData = $this->upload->data();
                           crop($uploadData, $pixel);
                           $_POST['users']['usr_banner'] = $uploadData['file_name'];
                      }
                 }
                 $this->logistics->edit($this->input->post());
                 $x1 = $this->input->post('x12');
                 $fileCount = count($x1);
                 for ($j = 0; $j < $fileCount; $j++) {

                      $data = array();
                      $angle = array();
                      $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'logistics/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $angle['x1']['0'] = $_POST['x12'][$j];
                      $angle['x2']['0'] = $_POST['x22'][$j];
                      $angle['y1']['0'] = $_POST['y12'][$j];
                      $angle['y2']['0'] = $_POST['y22'][$j];
                      $angle['w']['0'] = $_POST['w2'][$j];
                      $angle['h']['0'] = $_POST['h2'][$j];
                      $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                      $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                      $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                      $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                      $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];

                      if (!$this->upload->do_upload('prd_image_tmp')) {
                           $error = array('error' => $this->upload->display_errors());
                      } else {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle, false);
                           $this->logistics->removeImages($userId);
                           $this->logistics->addImages(array('lci_user_id' => $userId, 'lci_image' => $data['file_name'],'ici_alttag' => $_POST['icialttag']));
                      }
                 }
                 $this->session->set_flashdata('app_success', 'Logistics successfully updated!');
                 redirect(strtolower(__CLASS__) . '/listlogistics');
            } else {
                 $id = encryptor($id, 'D');
                 $data = $this->logistics->getLogistics($id);
                 $data['selectedLogisticsTypes'] = $this->logistics->selectedLogisticsTypes($id);
                 $data['country'] = get_country_list();
                 $data['logisticsTypes'] = $this->logistics->getLogisticsTypes();
                 $countryId = isset($data['userDetails']['usr_country']) ? $data['userDetails']['usr_country'] : 0;
                 $data['states'] = get_state_province('', $countryId);
                 $data['country'] = get_country_list();
                 $this->render_page(strtolower(__CLASS__) . '/edit', $data);
            }
       }

       function changestatus($userId) {
            $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
            $userId = encryptor($userId, 'D');

            if ($ischecked == 1) { // Active
                 if ($this->ion_auth->update($userId, array('usr_active' => 1))) {
                      generate_log(array(
                          'log_title' => 'Status changed',
                          'log_desc' => 'Logistics activated',
                          'log_controller' => 'logistics-status-changed',
                          'log_action' => 'U',
                          'log_ref_id' => $userId,
                          'log_added_by' => $this->uid
                      ));
                      die(json_encode(array('status' => 'success', 'msg' => "Logistics successfully activated")));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => "Can't change status")));
                 }
            } else { // de activate
                 if ($this->ion_auth->deactivate($userId)) {
                      generate_log(array(
                          'log_title' => 'Status changed',
                          'log_desc' => 'Logistics de-activated',
                          'log_controller' => 'logistics-status-changed',
                          'log_action' => 'U',
                          'log_ref_id' => $userId,
                          'log_added_by' => $this->uid
                      ));
                      die(json_encode(array('status' => 'success', 'msg' => "Logistics successfully de-activated")));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => "Can't varify supplier")));
                 }
            }
       }

       public function removeImage($id) {
            if ($this->logistics->removeImage($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Building image successfully deleted'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete project image"));
                 die();
            }
       }

       function setDefaultImage($id) {
            if ($this->logistics->setDefaultImage($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Image set as default!')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't set default image")));
            }
       }

       function services() {
            $data['services'] = $this->logistics->getLogisticsServices();
            $this->render_page(strtolower(__CLASS__) . '/services', $data);
       }

       function new_service() {
            if ($this->input->post()) {
                 $serviceId = $this->logistics->newService($this->input->post('service'));

                 $x1 = $this->input->post('x12');
                 $fileCount = count($x1);
                 for ($j = 0; $j < $fileCount; $j++) {

                      $data = array();
                      $angle = array();
                      $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'logistics/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $angle['x1']['0'] = $_POST['x12'][$j];
                      $angle['x2']['0'] = $_POST['x22'][$j];
                      $angle['y1']['0'] = $_POST['y12'][$j];
                      $angle['y2']['0'] = $_POST['y22'][$j];
                      $angle['w']['0'] = $_POST['w2'][$j];
                      $angle['h']['0'] = $_POST['h2'][$j];
                      $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                      $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                      $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                      $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                      $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];

                      if (!$this->upload->do_upload('prd_image_tmp')) {
                           $error = array('error' => $this->upload->display_errors());
                      } else {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle, false);
                           $this->logistics->addServiceImages(array('logsi_service_id' => $serviceId, 'logsi_image' => $data['file_name'],
                         'img_altatag' => $_POST['img_altatag']));
                      }
                 }
                 $this->session->set_flashdata('app_success', 'Logistics service successfully added!');
                 redirect(strtolower(__CLASS__) . '/services');
            } else {
                 $data['logistics'] = $this->logistics->getLogistics();
                 $this->render_page(strtolower(__CLASS__) . '/new_service', $data);
            }
       }

       function edit_service($id = '') {
            if ($this->input->post()) {
                 $serviceId = $this->logistics->updateService($this->input->post());

                 $x1 = $this->input->post('x12');
                 $fileCount = count($x1);
                 for ($j = 0; $j < $fileCount; $j++) {

                      $data = array();
                      $angle = array();
                      $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'logistics/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $angle['x1']['0'] = $_POST['x12'][$j];
                      $angle['x2']['0'] = $_POST['x22'][$j];
                      $angle['y1']['0'] = $_POST['y12'][$j];
                      $angle['y2']['0'] = $_POST['y22'][$j];
                      $angle['w']['0'] = $_POST['w2'][$j];
                      $angle['h']['0'] = $_POST['h2'][$j];
                      $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                      $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                      $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                      $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                      $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];

                      if (!$this->upload->do_upload('prd_image_tmp')) {
                           $error = array('error' => $this->upload->display_errors());
                      } else {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle, false);
                           $this->logistics->deleteServiceImages($serviceId);
                           $this->logistics->addServiceImages(array('logsi_service_id' => $serviceId, 'logsi_image' => $data['file_name'] ,
                             'img_altatag' => $_POST['new_img_altatag']));
                      }
                 }
                 $this->session->set_flashdata('app_success', 'Logistics service successfully added!');
                 redirect(strtolower(__CLASS__) . '/services');
            } else {
                 $id = encryptor($id, 'D');
                 $data['service'] = $this->logistics->getLogisticsServices($id);
                 $data['logistics'] = $this->logistics->getLogistics();
                 $this->render_page(strtolower(__CLASS__) . '/edit_service', $data);
            }
       }

       function delete_service($id) {
            if ($this->logistics->removeService($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Service successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete service")));
            }
       }

       function removeServiceImage($id) {
            if ($this->logistics->removeServiceImage($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Image successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete image")));
            }
       }

       function getQuote($id = '') {
            $id = encryptor($id, 'D');
            $data = $this->logistics->getLogistics($id);
            $this->template->set_layout('common');
            $this->render_page(strtolower(__CLASS__) . '/get-a-quote', $data);
       }

       function newLogisticsQuote() {
            if ($this->logistics->newLogisticsQuote($this->input->post())) {
                 $datas = $this->input->post('quote');
                 /* Mail to supplier */
                 $this->mail->isSMTP();
                 $this->mail->Host = MAIL_HOST;
                 $this->mail->SMTPAuth = true;
                 $this->mail->Username = MAIL_USERNAME;
                 $this->mail->Password = MAIL_PASSWORD;
                 $this->mail->SMTPSecure = 'ssl';
                 $this->mail->Port = 465;
                 $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                 $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                 $this->mail->addAddress($datas['logq_email']);
                 $this->mail->Subject = 'New Logistic Quote';
                 $this->mail->isHTML(true);
                 $mailData['email'] = $datas['logq_email'];
                 $this->mail->Body = $this->load->view('logistics-quote-mail-template', $mailData, true);
                 $this->mail->send();
                 /* Mail end */
                 die(json_encode(array('status' => 'success', 'msg' => 'We will get back to you!')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Error occured!")));
            }
       }

       function request_quotes() {
            $data['requests'] = $this->logistics->requestQuotes();
            $this->render_page(strtolower(__CLASS__) . '/request-quotes', $data);
       }

       function request_quotes_details($id) {
            $id = encryptor($id, 'D');
            $data['requests'] = $this->logistics->requestQuotes($id);
            $this->render_page(strtolower(__CLASS__) . '/request-quotes-details', $data);
       }

       function delete($id) {
            if ($this->logistics->delete($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Logistics successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete logistics, it may already used")));
            }
       }
       
       function removeBG($usrId) {
            if ($this->logistics->removeBG($usrId)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Background successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete background image")));
            }
       }
    }
  