<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Brand</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Item</th>
                                        <th>Supplier</th>    
                                        <th>Category</th> 
                                        <th>QTY</th>    
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $stock as $key => $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/view/' . encryptor($value['cst_id']));?>">
                                               <td class="trVOE"><?php echo $value['cst_item_name'];?></td>
                                               <td class="trVOE"><?php echo $value['supm_name_en'];?></td>
                                               <td class="trVOE"><?php echo $value['cat_title'];?></td>
                                               <td class="trVOE"><?php echo $value['cst_item_qty'];?></td>
                                               <?php if (check_permission($controller, 'delete')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url($controller . '/delete/' . $value['cst_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>