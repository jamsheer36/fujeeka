<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  require './vendor/autoload.php';
  class order extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->page_title = 'Product Inquiry';
            $this->load->model('order_model', 'order');
            $this->lock_in();
       }

       public function index() {
            $data['order'] = $this->order->getOrders();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }


      public function report() {

            $date1 = isset($_POST['start_date'])? $_POST['start_date']:null;
            $date2 = isset($_POST['end_date'])? $_POST['end_date']:null;
           
            if($date1 && $date2)
            {
               $exportData =  $this->order->getOrders(null,$date1, $date2);     
                if (empty($exportData)) {

                 $this->session->set_flashdata('app_error', "No Data exist between $date1 and $date2");
                  redirect(strtolower(__CLASS__). '/rfq_list');
                               
                }
                else
                {

                  foreach ($exportData as $key => $value) {
                    
                    $content[$key]['Sl No'] = $key+1;
                    $content[$key]['Enquiry NO'] = $value['ord_number'];
                    $content[$key]['Buyer Name'] = $value['byr_first_name']." ".$value['byr_last_name'];
                    $content[$key]['Buyer Email'] = isset($value['byr_email'])?$value['byr_phone']:'';
                    $content[$key]['Buyer Phone'] = isset($value['usr_phone'])?$value['usr_phone']:'';

                    $content[$key]['Supplier Name'] = $value['supm_name_en'];
                    $content[$key]['Supplier Email'] = isset($value['supm_email'])?$value['supm_email']:'';
                    $content[$key]['Supplier Phone'] = isset($value['supm_number'])?
                                                             $value['supm_number']:'';
                    $content[$key]['Supplier Address'] = isset($value['supm_address_en'])?
                                                          strip_tags($value['supm_address_en']):'';
                    $content[$key]['Supplier City'] = isset($value['supm_city_en'])?
                                                             $value['supm_city_en']:'';
                    $content[$key]['Product Name'] = $value['prd_name_en'];
                    $content[$key]['Order Quantity'] =   $value['ord_qty']." ".$value['unt_unit_name_en'];
                    $content[$key]['Unit Price (Min-Max)'] = $value['prd_price_min']."-".
                                                              $value['prd_price_max'];
                    $content[$key]['Offer Price (Min-Max)'] = $value['prd_offer_price_min']."-".
                                                              $value['prd_offer_price_min'];

                    $content[$key]['Status'] = $value['pos_status'];
                    $content[$key]['Order Date'] = date('M-d,Y',strtotime($value['ord_added_on']));
                 
                 
                

                  }
                                   
                }
                    if($_POST['type']=='excel')
                    {
                        $this->load->library('excel');
                        $this->excel->stream("Order Report ($date1 -$date2)-".now()." .xls",$content);
                    }
                    else
                    {
                          $html=$this->getHtmlTable($content);
                          $pdfFilePath = "Order-Report (".$date1 
                                  ."-".$date2.")-".now().".pdf";
                          $this->load->library('m_pdf');
                          $this->m_pdf->pdf->WriteHTML($html);
                          $this->m_pdf->pdf->Output($pdfFilePath, "D");  
                          $this->m_pdf->pdf->Output($serverpath,'F');
                           
                           }   
                        
                      $this->session->set_flashdata('app_success', 'Report Generated Succesfully');
                       redirect(strtolower(__CLASS__). '/rfq_list');
            }
               else
               {
                $this->session->set_flashdata('app_error', 'Invalid dates');
                 redirect(strtolower(__CLASS__). '/rfq_list');
               
               }

             
       }



                   public function getHtmlTable($exportData) {
                              
                   $start = "<style type='text/css'>
                  .tg  {border-collapse:collapse;border-spacing:0;border-width:1px;border-style:solid;border-color:#aaa;}
                  .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
                  .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}
                  .tg .tg-7d57{background-color:#FCFBE3;border-color:inherit;text-align:left;vertical-align:top}
                  .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
                  @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>
                  <div class='tg-wrap'><table class='tg'>
                    <tr>
                     <th class='tg-0pky'><span style='font-weight:bold'>Sl No</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Enquiry No</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Buyer Name</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Buyer Email</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Buyer Phone</span></th>

                      <th class='tg-0pky'><span style='font-weight:bold'>Supplier Name</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Supplier Email</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Supplier Phone</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Supplier Address</span></th>
               
                      <th class='tg-0pky'><span style='font-weight:bold'>Product Name</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Order Quantity</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Unit Price (Min-Max)</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Offer Price (Min-Max)</span></th>
                      
                      <th class='tg-0pky'><span style='font-weight:bold'>Order Status</span></th>
                      <th class='tg-0pky'><span style='font-weight:bold'>Order Date</span></th>
                    </tr>";
                 
                    $tr ='';
                    foreach($exportData as $key=> $td) {
                    
                    $tr = $tr."<tr>
                     <td class='tg-7d57'>".$td['Sl No']."</td>
                        <td class='tg-7d57'>".$td['Enquiry NO']."</td>
                      <td class='tg-7d57'>".$td['Buyer Name']."</td>
                      <td class='tg-7d57'>".$td['Buyer Email']."</td>
                      <td class='tg-7d57'>".$td['Buyer Phone']."</td>  
                      <td class='tg-7d57'>".$td['Supplier Name']."</td>
                      <td class='tg-7d57'>".$td['Supplier Email']."</td>
                      <td class='tg-7d57'>".$td['Supplier Phone']."</td>  
                      <td class='tg-7d57'>".$td['Supplier Address'].",".$td['Supplier City']."</td>

                      <td class='tg-7d57'>".$td['Product Name']."</td>
                      <td class='tg-7d57'>".$td['Order Quantity']."</td>  

                      <td class='tg-7d57'>".$td['Unit Price (Min-Max)']."</td>
                      <td class='tg-7d57'>".$td['Offer Price (Min-Max)']."</td>
                      <td class='tg-7d57'>".$td['Status']."</td>  
                      <td class='tg-7d57'>".$td['Order Date']."</td>  
                    </tr>";

                    }
                        return $start.$tr."</table></div>";
                          

                          }


       /**
        * Get order summary
        * @param int $id
        * Author : JK
        */
       function order_summery($id) {
            $id = encryptor($id, 'D');
            $data['order'] = $this->order->getOrders($id);
            $data['statuses'] = $this->order->getAllStatuses();
            $this->render_page(strtolower(__CLASS__) . '/order_summery', $data);
       }

       /**
        * Update order status
        * @param int $orderId
        * Author : JK
        */
       function changeOrderStatuse($orderId) {

            if (!empty($orderId) && isset($_POST['status'])) {
                 if ($this->order->changeOrderStatuse($orderId, $_POST['status'])) {
                      die(json_encode(array('status' => 'success', 'msg' => 'Order status changes')));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => "Can't change order status.")));
                 }
            }
       }
       
       function sendComments() {
          $post_det = $this->input->post();
          $chat['title'] = $post_det['poc_title'];
          $chat['desc'] = $post_det['poc_desc'];
          $chat['type'] = 'inq';
          $chat['time'] = date('h:i A') . ' | ' . date('M d');
          $chat['from'] = $this->session->userdata('usr_username');
          $chat['order_id'] = $post_det['poc_order_id'];
          $chat['from_img'] ='assets/uploads/avatar/' . $this->session->userdata('usr_avatar');
          $chat['inq_link'] = site_url('order/order_summery/'. encryptor($post_det['poc_order_id']));

          $res = $this->sendPusherMessage(encryptor($post_det['poc_to']), 'inquiry-event', $chat);

                 $push['type'] = 'recieved';
                 $push['msg_type'] = 'inquiry_chat_notfication';
                 $push['from_id'] = $post_det['poc_from'] == 1 ? 'admin' : $post_det['poc_from'];
                 $push['from_name'] = $this->session->userdata('usr_username');
                 $push['order_title'] = $post_det['poc_title'];
                 $push['order_details'] = $post_det['poc_desc'];
                 $push['time'] = date("Y-m-d H:i:s");
                 $push['order_id'] =$post_det['poc_order_id'];

                 $this->load->model('user/user_model', 'user_model');
                 $tok_det = $this->user_model->getUserById($post_det['poc_to']);
              if($this->common_model->checkIsSupplier($post_det['poc_to']))
              {
                  $push['from_id']=$post_det['poc_from'];
                 
                  $push['poc_from']=$post_det['poc_from'];
                  $push['poc_to']=$post_det['poc_to'];
                  $push['poc_title']=$post_det['poc_title'];
                  $push['poc_desc']=$post_det['poc_desc'];
                  $push['poc_added_on']=date("Y-m-d H:i:s");

                  $push['frm_usr_first_name'] = $this->session->userdata('usr_username');
                  $push['frm_usr_last_name'] = '';
                  $push['frm_usr_avatar']=site_url().'assets/uploads/avatar/' . $this->session->userdata('usr_avatar');
                 
                  $this->common_model->sendPushNotificationForSupplier($push, 
                                                      $tok_det['usr_pushy_token'], '');
              }
            else
            $this->common_model->sendPushNotification($push, $tok_det['usr_pushy_token'], '');
            
          $newOne = $this->order->newInquiryMessage($post_det);
          die(json_encode(array('status' => 'success', 'design' => '')));
       }
       
       function loadInquiryComments($ordId) {
            $data['order'] = $this->order->getOrders($ordId);
            $data['comments'] = $this->order->loadInquiryComments($this->input->post('ids'), $ordId, $this->input->post('from'), $this->input->post('to'));
            $design = $this->load->view('order-comments', $data, true);
            die(json_encode(array('status' => 'success', 'design' => $design)));
       }
       function sendPusherMessage($channel, $event, $chat) {
          $options = array(
              'cluster' => PUSHER_CLUSTER,
              'useTLS' => true
          );
          $pusher = new Pusher\Pusher(
                  PUSHER_APP_KEY, PUSHER_APP_SECRET, PUSHER_APP_ID, $options
          );
          return $pusher->trigger($channel, $event, $chat);
       }
  }