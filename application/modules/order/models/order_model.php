<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class order_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_states = TABLE_PREFIX . 'states';
            $this->tbl_products = TABLE_PREFIX . 'products';
            $this->tbl_countries = TABLE_PREFIX . 'countries';
            $this->tbl_products_units = TABLE_PREFIX . 'products_units';
            $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
            $this->tbl_products_order_master = TABLE_PREFIX . 'products_order_master';
            $this->tbl_products_order_statuses = TABLE_PREFIX . 'products_order_statuses';
            $this->tbl_products_order_comments = TABLE_PREFIX . 'products_order_comments';
       }

       function getAllStatuses() {
            return $this->db->get($this->tbl_products_order_statuses)->result_array();
       }

       function getOrders($id = '',$date1 = '',$date2 = '') {
            if (privilege_exists('SP')) { // logged supplier
                 $this->db->where($this->tbl_products_order_master . '.ord_supplier', $this->suplr);
            } else if ($this->usr_grp == 'BY') { // If buyer logged in
                 $this->db->where($this->tbl_products_order_master . '.ord_buyer', $this->uid);
            }

            if ($id) {
                 $this->db->where('ord_id', $id);
                 $this->db->update($this->tbl_products_order_master, array('ord_is_notified' => 1));

                 $order = $this->db->select($this->tbl_products_order_master . '.*,' . $this->tbl_products . '.*,' . $this->tbl_products_order_statuses . '.*,' .
                                         $this->tbl_supplier_master . '.*, ' . $this->tbl_countries . '.*,' . $this->tbl_states . '.*,' . $this->tbl_products_units . '.*,' .
                                         'buyer.usr_id AS byr_id, buyer.usr_first_name AS byr_first_name, buyer.usr_last_name AS byr_last_name, ' .
                                         'spyr.usr_id AS sup_id, spyr.usr_first_name AS sup_first_name, spyr.usr_last_name AS sup_last_name')
                                 ->join($this->tbl_products, $this->tbl_products . '.prd_id = ' . $this->tbl_products_order_master . '.ord_prod_id', 'LEFT')
                                 ->join($this->tbl_products_order_statuses, $this->tbl_products_order_statuses . '.pos_id = ' . $this->tbl_products_order_master . '.ord_status', 'LEFT')
                                 ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_products_order_master . '.ord_supplier', 'LEFT')
                                 ->join($this->tbl_users . ' buyer', 'buyer.usr_id = ' . $this->tbl_products_order_master . '.ord_added_by', 'LEFT')
                                 ->join($this->tbl_users . ' spyr', 'spyr.usr_supplier = ' . $this->tbl_supplier_master . '.supm_id', 'LEFT')
                                 ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_supplier_master . '.supm_state', 'LEFT')
                                 ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_supplier_master . '.supm_country', 'LEFT')
                                 ->join($this->tbl_products_units, $this->tbl_products_units . '.unt_id = ' . $this->tbl_products_order_master . '.ord_unit', 'LEFT')
                                 ->where($this->tbl_products_order_master . '.ord_id  ', $id)
                                 ->order_by('ord_added_on', 'DESC')->get($this->tbl_products_order_master)->row_array();
                 if (!empty($order)) {
                      $order['buyer'] = $this->db->select($this->tbl_users . '.*,' . $this->tbl_states . '.*,' . $this->tbl_countries . '.*')
                                      ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_users . '.usr_state', 'LEFT')
                                      ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_users . '.usr_country', 'LEFT')
                                      ->where($this->tbl_users . '.usr_id', $order['ord_buyer'])->get($this->tbl_users)->row_array();

                      $order['supplier_personal'] = $this->db->select($this->tbl_users . '.*,' . $this->tbl_states . '.*,' . $this->tbl_countries . '.*')
                                      ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_users . '.usr_state', 'LEFT')
                                      ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_users . '.usr_country', 'LEFT')
                                      ->where($this->tbl_users . '.usr_supplier', $order['ord_supplier'])->get($this->tbl_users)->row_array();

                      $order['verified_by'] = $this->db->select($this->tbl_users . '.*,' . $this->tbl_states . '.*,' . $this->tbl_countries . '.*')
                                      ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_users . '.usr_state', 'LEFT')
                                      ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_users . '.usr_country', 'LEFT')
                                      ->where($this->tbl_users . '.usr_id', $order['ord_verified_by'])->get($this->tbl_users)->row_array();

                      $order['comments'] = $this->db->select($this->tbl_products_order_comments .
                                              '.*, c_from.usr_first_name AS frm_usr_first_name, c_from.usr_last_name AS frm_usr_last_name, c_from.usr_avatar AS frm_usr_avatar, '
                                              . ' c_to.usr_first_name AS to_usr_first_name, c_to.usr_last_name AS to_usr_last_name, c_to.usr_avatar AS to_usr_avatar')
                                      ->join($this->tbl_users . ' c_from', 'c_from.usr_id = ' . $this->tbl_products_order_comments . '.poc_from', 'LEFT')
                                      ->join($this->tbl_users . ' c_to', 'c_to.usr_id = ' . $this->tbl_products_order_comments . '.poc_to', 'LEFT')
                                      ->order_by($this->tbl_products_order_comments . '.poc_added_on', 'ASC')
                                      ->where(array($this->tbl_products_order_comments . '.poc_order_id' => $order['ord_id']))
                                      ->get($this->tbl_products_order_comments)->result_array();
                 }
                 return $order;
            }

            $sql = $this->db->select($this->tbl_products_order_master . '.*,' . $this->tbl_products . '.*,' . $this->tbl_products_order_statuses . '.*,' .
                                    $this->tbl_supplier_master . '.*, ' .
                                    'buyer.usr_id AS byr_id, buyer.usr_first_name AS byr_first_name, buyer.usr_last_name AS byr_last_name, '.
                                     'buyer.usr_email AS byr_email,buyer.usr_phone AS byr_phone,'.
                                     $this->tbl_products_units . '.unt_unit_name_en'
                            )
                            ->join($this->tbl_products, $this->tbl_products . '.prd_id = ' . $this->tbl_products_order_master . '.ord_prod_id', 'LEFT')
                            ->join($this->tbl_products_order_statuses, $this->tbl_products_order_statuses . '.pos_id = ' . $this->tbl_products_order_master . '.ord_status', 'LEFT')
                            ->join($this->tbl_supplier_master, $this->tbl_supplier_master . '.supm_id = ' . $this->tbl_products_order_master . '.ord_supplier', 'LEFT')
                            ->join($this->tbl_users . ' buyer', 'buyer.usr_id = ' . $this->tbl_products_order_master . '.ord_added_by', 'LEFT')

                           ->join($this->tbl_products_units, $this->tbl_products_units . '.unt_id = ' . $this->tbl_products_order_master . '.ord_unit', 'LEFT');
                            // ->join($this->tbl_users . ' spyr', 'spyr.usr_supplier = ' . $this->tbl_supplier_master . '.supm_id', 'LEFT')
                            if($date1 && $date2)
                               {
                                  $date1 = $date1." 00:00:00";
                                  $date2 = $date2." 11:59:59";
                                  $sql = $this->db->where($this->tbl_products_order_master.".ord_added_on 
                                                       between '$date1' and '$date2'");
                                                           
                               }

                           return  $sql->order_by('ord_added_on', 'DESC')->get($this->tbl_products_order_master)->result_array();
       }

       /**
        * Function for update order status
        * @param int $orderId
        * @param int $statusId
        * @return boolean
        * Author : JK
        */
       function changeOrderStatuse($orderId, $statusId) {

            $update['ord_status'] = $statusId; //New status id
            $update['ord_verified_by'] = $this->uid; // who verify the order
            $update['ord_verified_on'] = date('Y-m-d h:i:s'); // verify date
            // Get old status
            $oldStatus = $this->db->get_where($this->tbl_products_order_master, array('ord_id' => $orderId))->row_array();

            $this->db->where('ord_id', $orderId);
            if ($this->db->update($this->tbl_products_order_master, $update)) {
                 $oldStst = isset($oldStatus['ord_status']) ? $oldStatus['ord_status'] : 0; // old status
                 generate_log(array(
                     'log_title' => 'Order status changed',
                     'log_desc' => 'Order status changed to *' . $oldStst . '-' . $statusId,
                     'log_controller' => 'order-status-changed',
                     'log_action' => 'U',
                     'log_ref_id' => $orderId,
                     'log_added_by' => $this->uid
                 ));

                 return true;
            }
            return false;
       }

       function newInquiryMessage($datas) {
            if (!empty($datas)) {
                 
                 //Send push notification
                 $toUser = $this->ion_auth->user($datas['poc_to'])->row_array();
                 if (isset($toUser['usr_pushy_token']) && !empty($toUser['usr_pushy_token'])) {
                      $push['id'] = encryptor($datas['poc_order_id']);
                      $push['msg_type'] = 'prod_inqry_reply_notfication';
                      $push['from'] = $this->uid==1 ? 'Fujeeka' :$this->session->userdata('usr_username');
                      $push['title'] = $datas['poc_title'];
                      $push['time'] = date("Y-m-d H:i:s");
                      $this->common_model->sendPushNotification($push, $toUser['usr_pushy_token'], '');
                 }

                 return $this->db->insert($this->tbl_products_order_comments, $datas);
            }
            return false;
       }

       function loadInquiryComments($excludeIds, $ordId, $from = '', $to = '') {
            if ($excludeIds) {
                 if (is_root_user()) {
                      return $this->db->select($this->tbl_products_order_comments .
                                              '.*, c_from.usr_first_name AS frm_usr_first_name, c_from.usr_last_name AS frm_usr_last_name, c_from.usr_avatar AS frm_usr_avatar, '
                                              . ' c_to.usr_first_name AS to_usr_first_name, c_to.usr_last_name AS to_usr_last_name, c_to.usr_avatar AS to_usr_avatar')
                                      ->join($this->tbl_users . ' c_from', 'c_from.usr_id = ' . $this->tbl_products_order_comments . '.poc_from', 'LEFT')
                                      ->join($this->tbl_users . ' c_to', 'c_to.usr_id = ' . $this->tbl_products_order_comments . '.poc_to', 'LEFT')
                                      ->where($this->tbl_products_order_comments . '.poc_order_id', $ordId)
                                      ->where('((' . $this->tbl_products_order_comments . '.poc_from = ' . $from . ' AND ' . $this->tbl_products_order_comments . '.poc_to = ' . $to . ') OR '
                                              . '(' . $this->tbl_products_order_comments . '.poc_from = ' . $to . ' AND ' . $this->tbl_products_order_comments . '.poc_to = ' . $from . '))')
                                      ->where_not_in($this->tbl_products_order_comments . '.poc_id', $excludeIds)
                                      ->order_by($this->tbl_products_order_comments . '.poc_added_on', 'ASC')
                                      ->get($this->tbl_products_order_comments)->result_array();
                 } else {
                      return $this->db->select($this->tbl_products_order_comments .
                                              '.*, c_from.usr_first_name AS frm_usr_first_name, c_from.usr_last_name AS frm_usr_last_name, c_from.usr_avatar AS frm_usr_avatar, '
                                              . ' c_to.usr_first_name AS to_usr_first_name, c_to.usr_last_name AS to_usr_last_name, c_to.usr_avatar AS to_usr_avatar')
                                      ->join($this->tbl_users . ' c_from', 'c_from.usr_id = ' . $this->tbl_products_order_comments . '.poc_from', 'LEFT')
                                      ->join($this->tbl_users . ' c_to', 'c_to.usr_id = ' . $this->tbl_products_order_comments . '.poc_to', 'LEFT')
                                      ->where($this->tbl_products_order_comments . '.poc_order_id', $ordId)
                                      ->where('((' . $this->tbl_products_order_comments . '.poc_from = ' . $from . ' AND ' . $this->tbl_products_order_comments . '.poc_to = ' . $to . ') OR '
                                              . '(' . $this->tbl_products_order_comments . '.poc_from = ' . $to . ' AND ' . $this->tbl_products_order_comments . '.poc_to = ' . $from . '))')
                                      ->where_not_in($this->tbl_products_order_comments . '.poc_id', $excludeIds)
                                      ->order_by($this->tbl_products_order_comments . '.poc_added_on', 'ASC')
                                      ->get($this->tbl_products_order_comments)->result_array();
                 }
            }
       }

  }
  