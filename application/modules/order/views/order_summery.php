<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Inquiry summary</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <div class="" role="tabpanel" data-example-id="togglable-tabs">
                              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                   <li role="presentation" class="active">
                                        <a href="javascript:;" goto="#rfq" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                             <i class="fa fa-list-alt"></i> Summary</a>
                                   </li>
                                   <li role="presentation">
                                        <a class="btnComments" href="javascript:;" goto="#messages" role="tab" id="profile-tab" 
                                           data-toggle="tab" aria-expanded="false"><i class="fa fa-comments-o"></i> Comments</a>
                                   </li>
                              </ul>

                              <div id="myTabContent" class="tab-content">
                                   <div role="tabpanel" class="tab-pane fade active in" id="rfq" aria-labelledby="home-tab">
                                        <section class="content invoice">
                                             <!-- title row -->
                                             <div class="row">
                                                  <div class="col-xs-12 invoice-header">
                                                       <h1>
                                                            #<?php echo $order['ord_number'];?>
                                                            <small class="pull-right">Date: <?php echo date('j M Y', strtotime($order['ord_added_on']));?></small>
                                                       </h1>
                                                  </div>
                                                  <!-- /.col -->
                                             </div>
                                             <!-- info row -->
                                             <div class="row invoice-info">
                                                  <div class="col-sm-4 invoice-col">
                                                       <strong>Buyer</strong>
                                                       <address>
                                                            <br>
                                                            <?php
                                                              echo isset($order['buyer']['usr_first_name']) ?
                                                                      $order['buyer']['usr_first_name'] . ', ' : '';
                                                              echo isset($order['buyer']['usr_last_name']) ?
                                                                      $order['buyer']['usr_last_name'] : '';
                                                            ?>
                                                            <br><?php echo isset($order['buyer']['ctr_name']) ? $order['buyer']['ctr_name'] : '';?>, 
                                                            <?php echo isset($order['buyer']['stt_name']) ? $order['buyer']['stt_name'] : '';?>
                                                            <br>Phone: <?php echo isset($order['buyer']['usr_phone']) ? $order['buyer']['usr_phone'] : '';?>
                                                            <br>Email: <?php echo isset($order['buyer']['usr_email']) ? $order['buyer']['usr_email'] : '';?>
                                                       </address>
                                                  </div>
                                                  <!-- /.col -->
                                                  <div class="col-sm-4 invoice-col">
                                                       <strong>Supplier</strong>
                                                       <address>
                                                            <br>
                                                            <?php echo $order['supm_name_en'];?>
                                                            <br><?php echo $order['supm_address_en'];?>
                                                            <br><?php echo $order['ctr_name'];?>, <?php echo $order['stt_name'];?>
                                                            <br>Phone: <?php echo $order['supm_number'];?>
                                                            <br>Email: <?php echo $order['supm_email'];?>
                                                       </address>
                                                  </div>
                                                  <!-- /.col -->
                                                  <div class="col-sm-4 invoice-col">
                                                       <br>
                                                       <b>Enquiry NO:</b> <?php echo $order['ord_number'];?>
                                                       <br>
                                                       <b>Verified By:</b> <?php
                                                         echo isset($order['verified_by']['usr_first_name']) ?
                                                                 $order['verified_by']['usr_first_name'] . ', ' : '';
                                                         echo isset($order['verified_by']['usr_last_name']) ?
                                                                 $order['verified_by']['usr_last_name'] : '';
                                                       ?>
                                                       <br>
                                                       <b>Status:</b> 
                                                       <?php if (check_permission($controller, 'changeorderstatuse')) {?>
                                                              <select class="chkChangeOrderStatuse" 
                                                                      data-url="<?php echo site_url('order/changeOrderStatuse/' . $order['ord_id']);?>">
                                                                           <?php foreach ((array) $statuses as $key => $value) {?>
                                                                        <option 
                                                                        <?php echo ($value['pos_status'] == $order['pos_status']) ? 'selected="selected"' : '';?>
                                                                             value="<?php echo $value['pos_id'];?>">
                                                                                  <?php echo $value['pos_status'];?>
                                                                        </option>
                                                                   <?php }?>
                                                              </select> 
                                                         <?php } else {?>
                                                              <strong><?php echo $order['pos_status'];?></strong>
                                                         <?php }?>
                                                  </div>
                                                  <!-- /.col -->
                                             </div>
                                             <!-- /.row -->

                                             <!-- Table row -->
                                             <div class="row">
                                                  <div class="col-xs-12 table">
                                                       <table class="table table-striped">
                                                            <thead>
                                                                 <tr>
                                                                      <th>Product</th>
                                                                      <th>Qty</th>
                                                                      <th>Unit</th>
                                                                      <th>Unit Price <small>(Min-Max)</small></th>
                                                                      <th>Offer Price <small>(Min-Max)</small></th>
                                                                 </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <tr>
                                                                      <td><?php echo $order['prd_name_en'];?></td>
                                                                      <td><?php echo $order['ord_qty'];?></td>
                                                                      <td><?php echo $order['unt_unit_en'];?></td>
                                                                      <td><?php echo $order['prd_price_min'] . ' - ' . $order['prd_price_max'];?></td>
                                                                      <td><?php echo $order['prd_offer_price_min'] . ' - ' . $order['prd_offer_price_max'];?></td>
                                                                 </tr>
                                                            </tbody>
                                                       </table>
                                                  </div>
                                                  <!-- /.col -->
                                             </div>
                                             <!-- /.row -->

                                             <!-- this row will not appear when printing -->
                                             <div class="row no-print">
                                                  <div class="col-xs-12">
                                                       <!--                                                       <button class="btn btn-success pull-right"  style="margin-right: 5px;">
                                                                                                                   <i class="fa fa-comments-o"></i> Message</button>-->

                                                       <button class="btn btn-primary pull-right" onclick="window.print();" style="margin-right: 5px;">
                                                            <i class="fa fa-print"></i> Print</button>
                                                  </div>
                                             </div>
                                        </section>
                                   </div>
                                   <div role="tabpanel" class="tab-pane fade" id="messages" aria-labelledby="profile-tab">
                                   <input type="hidden" id="lgd_img" value="<?= 'assets/uploads/avatar/' . $this->session->userdata('usr_avatar')?>">
                                        <div class="messaging">
                                             <div class="inbox_msg">
                                                  <div class="mesgs" style="width: 100%;">
                                                       <div class="msg_history">
                                                            <?php
                                                              if (isset($order['comments'])) {
                                                                   foreach ((array) $order['comments'] as $key => $value) {
                                                                        echo '<input type="hidden" name="inquiryCmnts[]" value="' . $value['poc_id'] . '"/>';
                                                                        if (($value['poc_from'] == $this->uid)) { //my comments
                                                                             ?>
                                                                             <div class="outgoing_msg" id="msg<?php echo $value['poc_id'];?>">
                                                                                  <div class="outgoing_msg_img"> 
                                                                                       <div class="chat_img"> 
                                                                                            <?php
                                                                                            echo img(array('src' => FILE_UPLOAD_PATH . '/avatar/' . $value['frm_usr_avatar'],
                                                                                                'class' => 'img-circle'));
                                                                                            ?>
                                                                                       </div>
                                                                                  </div>
                                                                                  <div class="sent_msg">
                                                                                       <div>
                                                                                            <div>
                                                                                                 <strong><?php echo $value['poc_title'];?></strong>
                                                                                            </div>
                                                                                            <?php if (!empty($value['poc_desc'])) {?>
                                                                                                 <hr>
                                                                                                 <div class="chat-desc">
                                                                                                      <?php echo $value['poc_desc'];?>
                                                                                                 </div>
                                                                                            <?php }?>
                                                                                       </div>
                                                                                       <span class="time_date txt-align-rt"> 
                                                                                            <?php echo date('h:i A', strtotime($value['poc_added_on']));?> | 
                                                                                            <?php echo date('M d', strtotime($value['poc_added_on']));?>
                                                                                       </span> 
                                                                                  </div>
                                                                             </div>
                                                                        <?php } else { // incomoing msg ?>
                                                                           <div class="incoming_msg" id="msg<?php echo $value['poc_id'];?>">
                                                                                  <div class="incoming_msg_img"> 
                                                                                       <div class="chat_img"> 
                                                                                            <?php
                                                                                            echo img(array('src' => FILE_UPLOAD_PATH . '/avatar/' . $value['frm_usr_avatar'],
                                                                                                'class' => 'img-circle'));
                                                                                            ?>
                                                                                       </div>
                                                                                  </div>
                                                                                  <div class="received_msg">
                                                                                       <div class="received_withd_msg">
                                                                                            <div>
                                                                                                 <div>
                                                                                                      <strong><?php echo $value['poc_title'];?></strong>
                                                                                                 </div>
                                                                                                 <?php if (!empty($value['poc_desc'])) {?>
                                                                                                      <hr>
                                                                                                      <div class="chat-desc">
                                                                                                           <?php echo $value['poc_desc'];?>
                                                                                                      </div>
                                                                                                 <?php }?>
                                                                                            </div> 
                                                                                            <span class="time_date">
                                                                                                 <?php echo date('h:i A', strtotime($value['poc_added_on']));?> | 
                                                                                                 <?php echo date('M d', strtotime($value['poc_added_on']));?>
                                                                                            </span>
                                                                                       </div>
                                                                                  </div>
                                                                             </div> 
                                                                             <?php
                                                                        }
                                                                   }
                                                              }
                                                            ?>
                                                       </div>
                                                       <div class="type_msg" style="<?php echo is_root_user() ? 'display:none;' : '';?>">

                                                            <?php $to = $this->uid == $order['ord_buyer'] ? $order['ord_supplier_user_id'] : $order['ord_buyer'];?>

                                                            <div class="input_msg_write">
                                                                 <textarea type="text" class="write_msg txtInqComments" placeholder="Type title here"
                                                                           data-url="<?php echo site_url('order/sendComments');?>"
                                                                           ord_id="<?php echo $order['ord_id'];?>" to="<?php echo $to;?>" 
                                                                           from="<?php echo $this->uid;?>"></textarea>
                                                                 <textarea placeholder="Type description here" style="float: left;width: 100%;border-top: 1px solid #c4c4c4" 
                                                                           class="poc_desc" name="poc_desc"></textarea>
                                                                 <div style="float: right;width: 100%;">
                                                                      <button class="msg_send_btn btnSendInqMessage" type="button">
                                                                           <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                                                                      </button>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
     $(document).ready(function () {
          if ($(".chat_list").eq(0).length > 0) {
               $(".chat_list").eq(0).trigger("click");
          }

          var anchorHash = window.location.href.toString();
          if (anchorHash.lastIndexOf('#') != -1) {
               anchorHash = anchorHash.substr(anchorHash.lastIndexOf('#'));
               if ($('a[goto="' + anchorHash + '"]').length > 0) {
                    $('a[goto="' + anchorHash + '"]').trigger('click');
               }
          }
     });
</script>