<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Product Inquiries</h2>
                         <div class="clearfix"></div>
                    </div>
                         <?php if(check_permission('order','report')) { ?> 
                      <div class="x_content lg-bg">
                         <form id="demo-form2" class="form-inline f_tp" method="post" action="<?php echo site_url($controller . '/report');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <input value="0" type="hidden" name="" id="usr_id"/>
                              <div class="form-group">
                                       <label class="control-label" for="first-name">Start Date<span class="required">*</span>
                                   </label>
                                        <input type="text" id="date1" required="required" class="form-control" data-parsley-required-message="Start Date is required" name="start_date">
                              </div>
                              <div class="form-group">
                                       <label class="control-label" for="last-name">End Date <span class="required">*</span>
                                   </label>
                                        <input type="text" id="date2" name="end_date" required="required" data-parsley-required-message="End Date is required" class="form-control">
                              </div>

                                        <button type="submit"  name='type' value="excel" class="btn btn-success">Excel</button>
                                         <button type="submit" name='type' value="pdf" class="btn btn-danger">Pdf</button>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
                  <?php } ?>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Order No</th>
                                        <th>Buyer</th>
                                        <?php echo is_root_user() ? '<th>Supplier</th>' : '';?>
                                        <th>Product</th>
                                        <th>Status</th>
                                        <th>Added on</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $order as $key => $value) {
                                          ?>
                                          <tr data-url="<?php echo site_url($controller . '/order_summery/' . encryptor($value['ord_id']));?>">
                                               <td class="trVOE"><?php echo $value['ord_number'];?></td>
                                               <td class="trVOE"><?php echo $value['byr_first_name'];?></td>
                                               <?php echo is_root_user() ? '<td class="trVOE">' . $value['supm_name_en'] . '</td>' : '';?>
                                               <td class="trVOE"><?php echo $value['prd_name_en'];?></td>
                                               <td class="trVOE"><?php echo $value['pos_status'];?></td>
                                               <td class="trVOE"><?php echo date('j M Y', strtotime($value['ord_added_on']));?></td>
                                               <?php if (check_permission($controller, 'delete')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url="<?php echo site_url($controller . '/delete/' . $value['ord_id']);?>">
                                                              <i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>
<script>
$(function() {
  $('input[name="start_date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
     locale: {
      format: 'YYYY-MM-DD'
    }
  });
});
</script>
           
 <script>
$(function() {
  $('input[name="end_date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
     locale: {
     format: 'YYYY-MM-DD'
    }
  });
});
</script>
