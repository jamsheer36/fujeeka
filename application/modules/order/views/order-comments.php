<?php
  if (!empty($comments)) {
       $tmpId = is_root_user() ? $order['ord_added_by'] : $this->uid;
       foreach ((array) $comments as $key => $value) {
            echo '<input type="hidden" name="inquiryCmnts[]" value="' . $value['poc_id'] . '"/>';
            if ($value['poc_from'] == $tmpId) { // my msg
                 ?>
                 <div class="incoming_msg chat_msg_box" id="msg<?php echo $value['poc_id'];?>">
                      <div class="incoming_msg_img"> 
                           <div class="chat_img"> 
                                <?php
                                echo img(array('src' => FILE_UPLOAD_PATH . '/avatar/' . $value['frm_usr_avatar'],
                                    'class' => 'img-circle'));
                                ?>
                           </div>
                      </div>
                      <div class="received_msg">
                           <div class="received_withd_msg">
                                <div>
                                     <div>
                                          <strong><?php echo $value['poc_title'];?></strong>
                                     </div>
                                     <?php if (!empty($value['poc_desc'])) {?>
                                          <hr>
                                          <div class="chat-desc">
                                               <?php echo $value['poc_desc'];?>
                                          </div>
                                     <?php }?>
                                </div> 
                                <span class="time_date">
                 <!--                                     <strong><?php echo $value['to_usr_first_name'];?></strong> |-->
                                     <?php echo date('h:i A', strtotime($value['poc_added_on']));?> | 
                                     <?php echo date('M d', strtotime($value['poc_added_on']));?> 
                                </span>
                           </div>
                      </div>
                 </div> 
            <?php } else {  // outgoing msg  ?>
                 <div class="outgoing_msg chat_msg_box" id="msg<?php echo $value['poc_id'];?>">
                      <div class="outgoing_msg_img"> 
                           <div class="chat_img"> 
                                <?php
                                echo img(array('src' => FILE_UPLOAD_PATH . '/avatar/' . $value['frm_usr_avatar'],
                                    'class' => 'img-circle'));
                                ?>
                           </div>
                      </div>
                      <div class="sent_msg">
                           <div>
                                <div>
                                     <strong><?php echo $value['poc_title'];?></strong>
                                </div>
                                <?php if (!empty($value['poc_desc'])) {?>
                                     <hr>
                                     <div class="chat-desc">
                                          <?php echo $value['poc_desc'];?>
                                     </div>
                                <?php }?>
                           </div>
                           <span class="time_date txt-align-rt"> 
                                <?php echo date('h:i A', strtotime($value['poc_added_on']));?> | 
                                <?php echo date('M d', strtotime($value['poc_added_on']));?> | 
                                <strong><?php echo $value['frm_usr_first_name'];?></strong>
                           </span> 
                      </div>
                 </div>
                 <?php
            }
       }
  }
?>