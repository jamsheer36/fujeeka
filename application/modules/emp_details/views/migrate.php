<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Add New Employee</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <form id="demo-form2" method="post" action="<?php echo site_url($controller . '/migrate');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Migrate From <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select required="required" class="form-control col-md-7 col-xs-12" data-parsley-required-message="Select From Staff" name="from">
                                             <option value="">Select From Staff</option>
                                             <?php foreach($staffs as $row) { ?>
                                                  <option value="<?=$row['usr_id']?>"><?=$row['usr_first_name'].' '.$row['usr_last_name']?></option>
                                             <?php } ?>
                                        </select>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Migrate To <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="to" required="required" data-parsley-required-message="Select To Staff" class="form-control col-md-7 col-xs-12">
                                             <option value="">Select To Staff</option>
                                             <?php foreach($staffs as $row) { ?>
                                                  <option value="<?=$row['usr_id']?>"><?=$row['usr_first_name'].' '.$row['usr_last_name']?></option>
                                             <?php } ?>
                                        </select>
                                   </div>
                              </div>
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                   </div>
                              </div>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>