<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Staff List</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Avatar</th>
                                        <?php echo is_root_user() ? '<th>Root user</th>' : '';?>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>First name</th>
                                        <th>Last name</th>
                                        <th>Group</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                        <?php echo check_permission($controller, 'changeuserstatus') ? '<th>Active</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     if (!empty($users)) {
                                          foreach ($users as $key => $value) {
                                               ?>
                                               <tr data-url="<?php echo site_url('emp_details/view/' . encryptor($value['usr_id']));?>">
                                                    <td class="trVOE">
                                                         <?php
                                                         echo img(array('src' => 'assets/uploads/avatar/' . $value['usr_avatar'], 'width' => '50', 'id' => 'imgBrandImage'));
                                                         ?>
                                                    </td>
                                                    <?php if (is_root_user()) {?>
                                                         <td class="trVOE"><?php
                                                              if ($value['usr_supplier'] == 1) {
                                                                   echo 'Sub admin';
                                                              } else {
                                                                   echo!empty($value['supm_name_en']) ? $value['supm_name_en'] : $value['sup_first_name'];
                                                              }
                                                              ?>
                                                         </td>
                                                    <?php }?>
                                                    <td class="trVOE"><?php echo $value['usr_username'];?></td>
                                                    <td class="trVOE"><?php echo $value['usr_email'];?></td>
                                                    <td class="trVOE"><?php echo $value['usr_first_name'];?></td>
                                                    <td class="trVOE"><?php echo $value['usr_last_name'];?></td>
                                                    <td class="trVOE"><?php echo $value['group_name'];?></td>
                                                    <?php if (check_permission($controller, 'delete')) {?>
                                                         <td>
                                                              <a class="pencile deleteListItem" href="javascript:void(0);" data-url="<?php echo site_url('emp_details/delete/' . $value['usr_id']);?>">
                                                                   <i class="fa fa-remove"></i>
                                                              </a>
                                                         </td>
                                                    <?php } if (check_permission($controller, 'changeuserstatus')) {?>
                                                         <td>
                                                              <label class="switch">
                                                                   <input type="checkbox" value="1" class="chkOnchange"
                                                                   <?php echo $value['usr_active'] == 1 ? 'checked' : '';?>
                                                                          data-url="<?php echo site_url($controller . '/changeuserstatus/' . encryptor($value['usr_id']));?>">
                                                                   <span class="slider round"></span>
                                                              </label>
                                                         </td>
                                                    <?php }?>
                                               </tr>
                                               <?php
                                          }
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>