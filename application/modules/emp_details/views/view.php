<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Edit Employee</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <form id="demo-form2" method="post" action="<?php echo site_url($controller . '/update');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <input value="<?php echo $userDetails['usr_id']?>" type="hidden" name="user[usr_id]" id="usr_id"/>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $userDetails['usr_first_name']?>" type="text" id="first-name"
                                               required="required" class="form-control col-md-7 col-xs-12" name="user[usr_first_name]" data-parsley-required-message="First Name required" >
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $userDetails['usr_last_name']?>" type="text" id="last-name" 
                                               name="user[usr_last_name]" required="required" class="form-control col-md-7 col-xs-12" data-parsley-required-message="Last Name required">
                                   </div>
                              </div>

                              <?php if (is_root_user()) {?>
                                     <div class="form-group">
                                          <label for="usr_group" class="control-label col-md-3 col-sm-3 col-xs-12">Supplier/Sub admin</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                               <select class="select2_group form-control" name="user[usr_group]" required="true" 
                                                       <?php echo $userDetails['usr_supplier'] == 1 ? 'disabled' : '';?>
                                                       data-parsley-required-message="Select a supplier">
                                                    <?php
                                                    foreach ($suppliers as $key => $value) {
                                                         ?>
                                                         <option <?php echo $userDetails['usr_supplier'] == $value['supm_id'] ? 'selected="selected"' : '';?>
                                                              value="<?php echo $value['supm_id'];?>"><?php echo $value['supm_name_en'];?></option>
                                                              <?php
                                                         }
                                                         ?>
                                                         <option value="1" <?php echo $userDetails['usr_supplier'] == 1 ? 'selected="selected"' : '';?>>Sub Admin</option>
                                               </select>
                                          </div>
                                     </div>
                                <?php } else {?>
                                     <input value="<?php echo $this->suplr;?>" type="hidden" name="user[usr_supplier]"/>
                                <?php }?>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Mobile<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $userDetails['usr_phone']?>" type="text" id="last-name" name="user[usr_phone]" 
                                               required="required" data-past=".usr_whatsapp" class="pastContent numOnly form-control col-md-7 col-xs-12" data-parsley-required-message="Mobile required" maxlength="15">
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $userDetails['usr_email']?>" type="email" id="last-name" name="user[usr_email]" 
                                               required="required" class="form-control col-md-7 col-xs-12" data-parsley-trigger="change" data-parsley-required-message="Email required">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="usr_password" name="user[usr_password]" class="form-control col-md-7 col-xs-12 usr_password" data-parsley-trigger="change" minlength="6" data-parsley-minlength-message="Password should be more than 5 character">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Re enter Password<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="" type="text" id="usr_password_conf" name="user[usr_password_conf]" 
                                               class="form-control col-md-7 col-xs-12 usr_password_conf" data-parsley-equalto="#usr_password" data-parsley-trigger="change" data-parsley-equalto-message="Should be same as password">
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_address">Address</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="<?php echo $userDetails['usr_address']?>" type="text" id="usr_address"
                                               name="user[usr_address]" class="form-control col-md-7 col-xs-12">
                                   </div>
                              </div>

                              <?php if (!empty($userDetails['usr_avatar'])) {?>
                                     <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_city">Avatar</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                               <?php
                                               echo img(array('src' => 'assets/uploads/avatar/' . $userDetails['usr_avatar'], 'width' => '100', 'id' => 'imgBrandImage'));
                                               ?>
                                          </div>
                                     </div>
                                <?php }?>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usr_city">New avatar</label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="usr_avatar" class="autoComCity form-control col-md-7 col-xs-12">
                                   </div>
                              </div>

                              <div class="ln_solid"></div>
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success">Submit</button>' : '';?>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                   </div>
                              </div>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>