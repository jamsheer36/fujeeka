<?php

defined('BASEPATH') or exit('No direct script access allowed');

class emp_details extends App_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->page_title = 'New employe';
        $this->load->model('emp_details_model', 'emp_details');
        $this->load->model('supplier/supplier_model', 'supplier');
        $this->load->model('user_permission/user_permission_model', 'user_permission');
        $this->lock_in();
    }

    public function index()
    {
        $data['users'] = $this->emp_details->getUsers();
        $this->render_page(strtolower(__CLASS__) . '/list', $data);
    }

    public function add()
    {
        if (!empty($_POST)) {
            if (isset($_FILES['usr_avatar']['name'])) {
                $this->load->library('upload');
                $newFileName = rand(9999999, 0) . clean_image_name($_FILES['usr_avatar']['name']);
                $config['upload_path'] = './assets/uploads/avatar/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $newFileName;
                $this->upload->initialize($config);
                if ($this->upload->do_upload('usr_avatar')) {
                    $uploadData = $this->upload->data();
                    $_POST['user']['usr_avatar'] = $uploadData['file_name'];
                }
            }
            if ($this->usr_grp == 'SP') {
                $_POST['user']['usr_group'] = 3; // Staff
            } else {
                $_POST['user']['usr_group'] = 5; // sub admin
            }
            $userid = $this->emp_details->register($_POST['user']);
            if ($_POST['user']['usr_group'] == 3) { // Add default permission for staff
                $permission['cua_group_id'] = $userid;
                $permission['cua_access']['product'] = array('index', 'add', 'view', 'update', 'delete', 'rfq', 'rfq_list');
                $this->user_permission->addUserPermission($permission);
            } else if ($_POST['user']['usr_group'] == 5) {
                $permission['cua_group_id'] = $userid;
                $permission['cua_access']['supplier'] = array('index', 'add', 'view', 'update', 'delete');
                $permission['cua_access']['supplier_grade'] = array('index', 'add', 'view', 'update', 'delete');
                $permission['cua_access']['product'] = array('index', 'add', 'view', 'update', 'delete');
                $permission['cua_access']['units'] = array('index', 'add', 'view', 'update', 'delete');
                $this->user_permission->addUserPermission($permission);
            }
            $this->session->set_flashdata('app_success', 'Staff added successfully!');
            redirect(strtolower(__CLASS__));
        } else {
            if ($this->session->userdata('usr_supplier') != 0) {
                $data['suppliers'][0] = $this->supplier->suppliers($this->session->userdata('usr_supplier'));
            } else {
                $data['suppliers'] = $this->supplier->suppliers();
            }
            $this->render_page(strtolower(__CLASS__) . '/add', $data);
        }
    }

    public function view($id)
    {
        $id = encryptor($id, 'D');
        if ($this->session->userdata('usr_supplier') != 0) {
            $data['suppliers'][0] = $this->supplier->suppliers($this->session->userdata('usr_supplier'));
        } else {
            $data['suppliers'] = $this->supplier->suppliers();
        }

        $data['userDetails'] = $this->ion_auth->user($id)->row_array();
        $data['user_group'] = $this->ion_auth->get_users_groups($id)->row_array();
        $this->render_page(strtolower(__CLASS__) . '/view', $data);
    }

    public function update()
    {
        if (isset($_FILES['usr_avatar']['name'])) {
            $this->load->library('upload');
            $newFileName = rand(9999999, 0) . clean_image_name($_FILES['usr_avatar']['name']);
            $config['upload_path'] = './assets/uploads/avatar/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $newFileName;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('usr_avatar')) {
                $uploadData = $this->upload->data();
                $_POST['user']['usr_avatar'] = $uploadData['file_name'];
            }
        }
        $userGrp = $this->ion_auth->get_users_groups($_POST['user']['usr_id'])->row()->id;
        if ($userGrp == 5) { // Sub admin
            $permission['cua_group_id'] = $_POST['user']['usr_id'];
            $permission['cua_access']['supplier'] = array('index', 'add', 'view', 'update', 'delete');
            $permission['cua_access']['supplier_grade'] = array('index', 'add', 'view', 'update', 'delete');
            $permission['cua_access']['product'] = array('index', 'add', 'view', 'update', 'delete');
            $permission['cua_access']['units'] = array('index', 'add', 'view', 'update', 'delete');
            $this->user_permission->addUserPermission($permission);
        }

        if ($this->emp_details->update($_POST['user'])) {
            $this->session->set_flashdata('app_success', 'Row successfully updated!');
        } else {
            $this->session->set_flashdata('app_error', 'Row successfully updated!');
        }
        redirect(strtolower(__CLASS__));
    }

    public function updateProfile($id = '')
    {
        $this->page_title = 'Home | Update Profile';
        if (!empty($_POST)) {
            if (isset($_FILES['usr_avatar']['name'])) {
                $this->load->library('upload');
                $newFileName = rand(9999999, 0) . clean_image_name($_FILES['usr_avatar']['name']);
                $config['upload_path'] = './assets/uploads/avatar/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $newFileName;
                $this->upload->initialize($config);
                if ($this->upload->do_upload('usr_avatar')) {
                    $uploadData = $this->upload->data();
                    $_POST['usr_avatar'] = $uploadData['file_name'];
                    $this->session->set_userdata('usr_avatar', $uploadData['file_name']);
                }
            }
            if ($this->emp_details->update($_POST)) {
                $this->session->set_flashdata('app_success', 'Row successfully updated!');
            } else {
                $this->session->set_flashdata('app_error', 'Row successfully updated!');
            }
            redirect(__CLASS__ . '/updateProfile/' . encryptor($_POST['usr_id']));
        } else {
            $id = encryptor($id, 'D');
            $data['userDetails'] = $this->ion_auth->user($id)->row_array();
            $data['country'] = get_country_list();
            $data['states'] = get_state_province('', $data['userDetails']['usr_country']);
            $data['user_group'] = $this->ion_auth->get_users_groups($id)->row_array();
            $this->render_page(strtolower(__CLASS__) . '/update_profile', $data);
        }
    }

    public function delete($id)
    {
        $allow_to_delete = $this->emp_details->checkDataUnderEmployee($id);
        if ($allow_to_delete) {
            if ($this->ion_auth->delete_user($id)) {
                echo json_encode(array('status' => 'success', 'msg' => 'Staff successfully deleted'));
                die();
            } else {
                echo json_encode(array('status' => 'fail', 'msg' => 'Something went wrong,try again'));
                die();
            }
        }else{
            echo json_encode(array('status' => 'fail', 'msg' => "You can't delete staff now. Please migrate staff data before deleting"));
            die();
        }
    }

    /**
     * Change employee status
     * @param type $userId
     * Author : jk
     */
    public function changeuserstatus($userId)
    {
        $userId = encryptor($userId, 'D');
        $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
        if ($this->common_model->changeStatus($userId, $ischecked, 'users', 'usr_active', 'usr_id')) {
            $logMessage = ($ischecked == 1) ? 'User status activated' : 'User status de-activated';
            generate_log(array(
                'log_title' => 'Status changed',
                'log_desc' => $logMessage,
                'log_controller' => 'user-status-changed',
                'log_action' => 'U',
                'log_ref_id' => $userId,
                'log_added_by' => $this->uid,
            ));

            $msg = ($ischecked == 1) ? "Activated this record successfully" : "De-activated this record successfully";
            die(json_encode(array('status' => 'success', 'msg' => $msg)));
        } else {
            die(json_encode(array('status' => 'fail', 'msg' => "Error occured")));
        }
    }
    public function migrate()
    {
        if ($this->usr_grp != 'SP') {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->page_title = 'Migrate staff data';
        if ($this->input->post()) {
            $from = $this->input->post('from');
            $to = $this->input->post('to');
            if ($from == $to) {
                $this->session->set_flashdata('app_error', 'From staff and to staff should be different');
                redirect(__CLASS__ . '/migrate/');
            }
            if ($this->emp_details->migrate($from, $to)) {
                $this->session->set_flashdata('app_success', 'Migration completed!');
            } else {
                $this->session->set_flashdata('app_error', 'Nothing to migarte');
            }
            redirect(__CLASS__ . '/migrate/');
        } else {
            $data['staffs'] = $this->emp_details->getStaffs();
            $this->render_page(strtolower(__CLASS__) . '/migrate', $data);
        }
    }

}
