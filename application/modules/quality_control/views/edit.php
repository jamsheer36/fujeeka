<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Verify logistics</h2>
                         <a href="<?php echo site_url($controller . '/listlogistics');?>" class="btn btn-round btn-primary" style="float: right">
                                                    <i class="fa fa-arrow-left"></i> Back</a>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart($controller . "/edit", array('id' => "frmProduct", 'class' => "form-horizontal", 'data-parsley-validate' => "true"))?>
                         <input type="hidden" name="usr_id" value="<?php echo isset($userDetails['usr_id']) ? $userDetails['usr_id'] : '';?>"/>
                         <div class="widget-body">
                              <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                   <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active">
                                             <a href="javascript:;" goto="#basic_details" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Basic Details</a>
                                        </li>
                                        <li role="presentation" class="pars">
                                             <a href="javascript:;" goto="#specification" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Specification</a>
                                        </li>
                                   </ul>
                                   <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="basic_details" aria-labelledby="home-tab">
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Name English</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter Name" placeholder="Name English" type="text" name="users[usr_first_name]" id="supm_name_en"
                                                              value="<?php echo isset($userDetails['usr_first_name']) ? $userDetails['usr_first_name'] : '';?>" class="form-control col-md-9 col-xs-12"/>
                                                  </div>
                                                  <div class="col-md-1 col-sm-1 col-xs-1">
                                                       <span style="float: right;cursor: pointer;font-size: 28px;" 
                                                             class="glyphicon glyphicon-globe btnOtherLanguage"></span>
                                                  </div>
                                             </div>
                                             <div class="divOtherLanguage" style="display: none;">
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Name Arabic</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input placeholder="Name Arabic" type="text" name="users[usr_first_name_ar]" id="supm_name_ar"
                                                                   value="<?php echo isset($userDetails['usr_first_name_ar']) ? $userDetails['usr_first_name_ar'] : '';?>"
                                                                   class="form-control col-md-9 col-xs-12"/>
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Name Chinese</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input placeholder="Name Chinese" type="text" name="users[usr_first_name_ch]" id="supm_name_ch"
                                                                   value="<?php echo isset($userDetails['usr_first_name_ch']) ? $userDetails['usr_first_name_ch'] : '';?>"
                                                                   class="form-control col-md-9 col-xs-12"/>
                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <input placeholder="Email" type="email" name="users[usr_email]" 
                                                              required data-parsley-required-message="Enter email"
                                                              value="<?php echo isset($userDetails['usr_email']) ? $userDetails['usr_email'] : '';?>" 
                                                              class="form-control col-md-9 col-xs-12" id="supm_name_ch"/>
                                                       <small class="smlQty"></small>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Phone</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <input placeholder="Contact number" type="text" name="users[usr_phone]" 
                                                              required data-parsley-required-message="Enter contact number"
                                                              value="<?php echo isset($userDetails['usr_phone']) ? $userDetails['usr_phone'] : '';?>" 
                                                              class="form-control col-md-9 col-xs-12 numOnly" id="supm_name_ch"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <select required data-parsley-required-message="Select country" class="form-control col-md-7 col-xs-12 bindToDropdown"
                                                               data-dflt-select="Select State" name="users[usr_country]"
                                                               data-url="<?php echo site_url('states/getStatesByCountry');?>"
                                                               data-bind="cmbModel" id="supm_country">
                                                            <option value="">Select Country</option>
                                                            <?php foreach ((array) $country as $key => $value) {?>
                                                                   <option value="<?php echo $value['ctr_id'];?>"
                                                                           <?php echo ($value['ctr_id'] == $userDetails['usr_country']) ? 'selected="selected"' : '';?>>
                                                                        <?php echo $value['ctr_name'];?></option>
                                                              <?php }?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">State</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <select required data-parsley-required-message="Select state" class="cmbModel select2_group form-control" name="users[usr_state]" id="usr_state">
                                                            <option value="">Select state</option>
                                                            <?php foreach ((array) $states as $key => $value) {?>
                                                                   <option <?php echo ($value['stt_id'] == $userDetails['usr_state']) ? 'selected="selected"' : '';?> 
                                                                        value="<?php echo $value['stt_id'];?>"><?php echo $value['stt_name'];?></option>
                                                                   <?php }?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12"  style="width: 50%;">
                                                       <textarea placeholder="Description" class="editor" 
                                                                 name="users[usr_description1]"><?php echo isset($userDetails['usr_description1']) ? $userDetails['usr_description1'] : '';?></textarea>
                                                  </div>
                                             </div>
                                             <!-- -->
                                             <?php
                                               if (isset($images) && !empty($images)) {
                                                    $i = 0;
                                                    ?>
                                                    <div class="form-group">
                                                         <?php
                                                         foreach ($images as $key => $value) {
                                                              if ($i % 4 == 0) {
                                                                   ?>
                                                                   <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                              <?php }?>
                                                              <div class="col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['qci_id'];?>" style="padding-left: 10px;">
                                                                   <div class="input-group">
                                                                        <?php echo img(array('src' => FILE_UPLOAD_PATH . 'qc/' . $value['qci_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                                   </div>
                                                                   <?php if ($value['qci_image']) {?>
                                                                        <span class="help-block">
                                                                             <a data-url="<?php echo site_url($controller . '/removeImage/' . $value['qci_id']);?>" href="javascript:void(0);" style="width: 100px;" 
                                                                                class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                                        </span>
<!--                                                                        <span class="help-block">
                                                                             <input type="radio" name="supm_default_image" class="btnSetDefaultImage"  
                                                                             <?php echo ($value['qci_default'] == 1) ? 'checked="checked"' : '';?> 
                                                                                    data-url="<?php echo site_url($controller . '/setDefaultImage/' . $value['qci_id']);?>"/>&nbsp; Make it default
                                                                        </span>-->
                                                                   <?php }?>
                                                              </div>
                                                              <?php
                                                              $i++;
                                                         }
                                                         ?>
                                                    </div>
                                               <?php }
                                             ?>
                                             <!-- -->
                                             <div class="form-group">
                                                  <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Images</label>
                                                  <div class="col-md-5 col-sm-3 col-xs-12">
                                                       <div id="newupload">
                                                            <input type="hidden" id="x10" name="x12[]" />
                                                            <input type="hidden" id="y10" name="y12[]" />
                                                            <input type="hidden" id="x20" name="x22[]" />
                                                            <input type="hidden" id="y20" name="y22[]" />
                                                            <input type="hidden" id="w0" name="w2[]" />
                                                            <input type="hidden" id="h0" name="h2[]" />
                                                            <input data-parsley-required-message="upload atleast one image" data-parsley-fileextension="jpg,png,gif,jpeg" type="file" 
                                                                   class="form-control col-md-7 col-xs-12" name="shopImages[]" id="image_file0" onchange="fileSelectHandler('0', '1000', '882', '0')" />
                                                            <img id="preview0" class="preview"/>
                                                            <span class="help-inline">Choose 1000(W) X 882(H)</span>
                                                       </div>
                                                  </div>

<!--                                                  <div class="col-md-1 col-sm-1 col-xs-12">
                                                       <span style="float: right;cursor: pointer;" data-limit="-1"
                                                             class="glyphicon glyphicon-plus btnMoreProductImages"></span>
                                                  </div>-->
                                             </div>
                                             <div  id="divMoreProductImages"></div>
                                             
                                             <?php
                                               if (isset($userDetails['usr_banner']) && !empty($userDetails['usr_banner'])) {
                                                    ?>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                         <div class="col-md-2 col-sm-3 col-xs-12" style="padding-left: 10px;">
                                                              <div class="input-group">
                                                                   <?php echo img(array('src' => FILE_UPLOAD_PATH . 'qc/' . $userDetails['usr_banner'], 'height' => '80', 'width' => '200', 'id' => 'imgBrandImage'));?>
                                                              </div>
                                                              <span class="help-block">
                                                                   <a data-url="<?php echo site_url($controller . '/removeBG/' . $userDetails['usr_id']);?>" href="javascript:void(0);" style="width: 100px;" 
                                                                      class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                              </span>
                                                         </div>
                                                    </div>
                                               <?php }
                                             ?>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Background image</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <div id="newupload">
                                                            <input type="hidden" id="x11" name="x11"/>
                                                            <input type="hidden" id="y11" name="y11"/>
                                                            <input type="hidden" id="x21" name="x21"/>
                                                            <input type="hidden" id="y21" name="y21"/>
                                                            <input type="hidden" id="w1" name="w1"/>
                                                            <input type="hidden" id="h1" name="h1"/>
                                                            <input <?php echo (isset($userDetails['usr_banner']) && !empty($userDetails['usr_banner'])) ? "" : 'required data-parsley-required-message="Select a file"';?> 
                                                                 data-parsley-fileextension="jpg,png,gif,jpeg,JPG,JPEG,PNG,GIF" 
                                                                   type="file" class="form-control col-md-7 col-xs-12" name="userAvatar" 
                                                                   id="image_file1" onchange="fileSelectHandler('1', '759', '250', true)" />
                                                            <img id="preview1" class="preview"/>
                                                            <span class="help-inline">Choose 759(W) X 250(H)</span>
                                                       </div>
                                                  </div>
                                             </div>
                                             
                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>
                                                  </div>
                                             </div>

                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="specification" aria-labelledby="home-tab">
                                             <div class="form-group">
                                                  <label for="enq_cus_email" class="control-label col-md-2 col-sm-3 col-xs-12">Specification</label>
                                                  <div class="col-md-10 col-sm-3 col-xs-12">
                                                       <div class="table-responsive divVehDetailsSale">
                                                            <table class="tblModifiedTable table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                                 <thead>
                                                                      <tr>
                                                                           <th colspan="3" style="text-align: center;">Key</th>
                                                                           <th colspan="3" style="text-align: center;">Value</th>
                                                                           <th><span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMore"></span></th>
                                                                      </tr>
                                                                      <tr>
                                                                           <th>English</th>
                                                                           <th>Arabic</th>
                                                                           <th>Chinese</th>
                                                                           <th>English</th>
                                                                           <th>Arabic</th>
                                                                           <th>Chinese</th>
                                                                           <th></th>
                                                                      </tr>
                                                                 </thead>
                                                                 <tbody>
                                                                      <?php
                                                                        if (!empty($specifications)) {
                                                                             foreach ((array) $specifications as $key => $value) {
                                                                                  ?>
                                                                                  <tr>
                                                                                       <td>
                                                                                            <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['qcs_key_en'];?>"  name="specification_key[en][]">
                                                                                       </td>
                                                                                       <td>
                                                                                            <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['qcs_key_ar'];?>"  name="specification_key[ar][]">
                                                                                       </td>
                                                                                       <td>
                                                                                            <input  id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['qcs_key_ch'];?>"  name="specification_key[ch][]">
                                                                                       </td>

                                                                                       <td>
                                                                                            <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['qcs_val_en'];?>" name="specification_val[en][]">
                                                                                       </td>
                                                                                       <td>
                                                                                            <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['qcs_val_ar'];?>" name="specification_val[ar][]">
                                                                                       </td>
                                                                                       <td>
                                                                                            <input  id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $value['qcs_val_ch'];?>" name="specification_val[ch][]">
                                                                                       </td>

                                                                                       <td>
                                                                                            <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                                                                       </td>
                                                                                  </tr>
                                                                                  <?php
                                                                             }
                                                                        } else {
                                                                             ?>
                                                                             <tr>
                                                                                  <td>
                                                                                       <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" name="specification_key[en][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" name="specification_key[ar][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input  id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" name="specification_key[ch][]">
                                                                                  </td>

                                                                                  <td>
                                                                                       <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" name="specification_val[en][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" name="specification_val[ar][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input  id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" name="specification_val[ch][]">
                                                                                  </td>

                                                                                  <td>
                                                                                       <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                                                                  </td>
                                                                             </tr>
                                                                        <?php }?>
                                                                 </tbody>
                                                            </table>
                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>
                                                       <button type="submit" class="btn btn-success">Submit</button>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>

<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
     .SumoSelect {width: 100% !important;}
</style>
<script>
     $(".pars").click(function () {
          if (!$('#frmProduct').parsley().validate())
               return false;
     });
</script>
