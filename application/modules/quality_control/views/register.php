<!-- Banner -->
<div class="sr-banner">
     <img src="images/banners/quality_register.jpg" alt="Register as Supplier" class="img-fluid">
</div>
<!-- Banner ends -->

<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
               <div class="col-md-7">
                    <div class="cntent">
                         <h2 class="heading2">
                              Why register as Quality Control ?
                         </h2>
                         <p>The era of paying for goods that are a far cry from what its description promised is over as we are a b2b online platform that is like no other, ensuring that all the pieces that make a successful and mutually beneficial transaction for both wholesalers and retailers, check out adequately.
                              We employ revolutionary quality control metrics that ensure the absolute quality of the products you purchase, making sure that you get precisely what you paid for.
                              Before we approve of a wholesaler to partner up with us on our platform, we ensure that both the individual and his or her products are vetted to acceptable industry standards.</p>
                         <p>When a wholesaler applies to partner with us and distribute his or her goods on our platform, we ensure all the products that are proposed to be listed for sale are thoroughly tackled with our world standard quality measures, and if said product doesn't meet all our requirements, the product would be denied the access to be sold on our platform.
                              If you are looking to find a b2b platform that makes available only the highest quality whole sale goods, then you have found just the place.
                              As long as the product you require in wholesale quantity is listed on our website, you can be sure that you are going to be purchasing only the most authentic version of said product and hence you have no worries.</p>
                    </div>
               </div>

               <div class="col-md-5">
                    <div class="card card-frms">
                         <h2 class="heading2">
                              REGISTER AS QUALITY CONTROL
                         </h2>
                         <form data-parsley-validate="true" class="frmSupplierRegister" action="<?php echo site_url($controller . '/register');?>">
                         <div class="form-group">
                                   <input name="usr_first_name" type="text" class="form-control" id="sname" 
                                          required data-parsley-required-message="Enter name"
                                          aria-describedby="nameHelp" placeholder="Enter name">
                                   <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                              </div>
                              <div class="form-group">
                                   <input name="usr_email" type="email" class="form-control" id="sEmail1" 
                                          required data-parsley-required-message="Enter valid email"
                                          aria-describedby="emailHelp" placeholder="Enter email">
                                   <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                              </div>

                              <div class="form-group">
                                   <input type="number" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" name="usr_phone" 
                                          required data-parsley-required-message="Enter phone number"
                                          class="form-control" id="sphone" placeholder="Phone">
                              </div>
                             
                              <div class="form-group">
                                   <textarea name="usr_desc" class="form-control" id="sComments" placeholder="Comments" rows="3"></textarea>
                              </div>

                              <div class="w-100 text-center">
                                   <button type="submit" class="btn btn-success btn-green btn-bold btnSupplierRegister">Send Request</button>
                              </div>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</section>
<style>
     p.parsley-success {
          color: #468847;
          background-color: #DFF0D8;
          border: 1px solid #D6E9C6
     }

     p.parsley-error {
          color: #B94A48;
          background-color: #F2DEDE;
          border: 1px solid #EED3D7
     }
     li.parsley-required {
          font-size: 10px;
     }
     ul.parsley-errors-list {
          list-style: none;
          color: #E74C3C;
          padding-left: 0
     }

     input.parsley-error,
     select.parsley-error,
     textarea.parsley-error {
          background: #FAEDEC;
          border: 1px solid #E85445
     }

     .btn-group .parsley-errors-list {
          display: none
     }
</style>