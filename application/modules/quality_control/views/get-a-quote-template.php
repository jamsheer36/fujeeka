<form action="<?php echo site_url('quality_control/newQAQuote'); ?>" class="frmLogisticsQuote" accept-charset="utf-8" id="qryform" 
      role="form" method="post" enctype="multipart/form-data" data-parsley-validate="true">                        
     
     <input type="hidden" name="quote[qcq_qc_id]" value="<?php echo isset($userDetails['usr_id']) ? $userDetails['usr_id'] : ''; ?>"/>
     <div class="form-group">
          <label></label>
          <input type="text" required class="form-control" placeholder="Name" name="quote[qcq_name]">
     </div>
     <div class="form-group">
          <label></label>
          <input type="text" required class="form-control" placeholder="Email" name="quote[qcq_email]">
     </div>
     <div class="form-group">
          <label></label>
          <input type="text" required class="form-control" placeholder="Enter your mobile numer" name="quote[qcq_mobile]">
     </div>
     <div class="form-group">
          <label></label>
          <textarea name="quote[qcq_desc]" class="form-control" rows="3" placeholder="Description"></textarea>
     </div>
     <button type="submit" class="btnSubmitLogisticQuote btn btn-success btn-green enq_submit">Submit</button>
     <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
</form>