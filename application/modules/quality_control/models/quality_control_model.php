<?php

  if (!defined('BASEPATH')) {
       exit('No direct script access allowed');
  }

  class quality_control_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_states = TABLE_PREFIX . 'states';
            $this->tbl_groups = TABLE_PREFIX . 'groups';
            $this->tbl_qc_quotes = TABLE_PREFIX . 'qc_quotes';
            $this->tbl_countries = TABLE_PREFIX . 'countries';
            $this->tbl_qc_images = TABLE_PREFIX . 'qc_images';
            $this->tbl_qc_services = TABLE_PREFIX . 'qc_services';
            $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
            $this->tbl_users_logistics = TABLE_PREFIX . 'users_logistics';
            $this->tbl_logistics_types = TABLE_PREFIX . 'logistics_types';
            $this->tbl_qc_quotes_types = TABLE_PREFIX . 'qc_quotes_types';
            $this->tbl_qc_specification = TABLE_PREFIX . 'qc_specification';
            $this->tbl_qc_services_images = TABLE_PREFIX . 'qc_services_images';
       }

       function getActiveQC($id = '') {
            if (!empty($id)) {
                 $data['userDetails'] = $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
                                 ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                                 ->where(array($this->tbl_users . '.usr_id' => $id))
                                 ->get($this->tbl_users)->row_array();

                 $data['images'] = $this->db->get_where($this->tbl_qc_images, array('qci_user_id' => $id))->result_array();
                 $data['specifications'] = $this->db->get_where($this->tbl_qc_specification, array('qcs_user_id' => $id))->result_array();
                 return $data;
            }
            return $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->where(array($this->tbl_groups . '.id' => 7, $this->tbl_users . '.usr_active' => 1))
                            ->get($this->tbl_users)->result_array();
       }

       function getQC($id = '') {
            if (!empty($id)) {
                 $data['userDetails'] = $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
                                 ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                                 ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                                 ->where(array($this->tbl_users . '.usr_id' => $id))
                                 ->get($this->tbl_users)->row_array();

                 $data['images'] = $this->db->get_where($this->tbl_qc_images, array('qci_user_id' => $id))->result_array();
                 $data['specifications'] = $this->db->get_where($this->tbl_qc_specification, array('qcs_user_id' => $id))->result_array();

                 $data['QCServices'] = $this->db->get_where($this->tbl_qc_services, array('qcs_qc_id' => $id))->result_array();
                 if (!empty($data['QCServices'])) {
                      foreach ($data['QCServices'] as $key => $value) {
                           $data['QCServices'][$key]['images'] = $this->db->get_where($this->tbl_qc_services_images, array('qcsi_service_id' => $value['qcs_id']))->result_array();
                      }
                 }

                 return $data;
            }
            return $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->where(array($this->tbl_groups . '.id' => 7))
                            ->get($this->tbl_users)->result_array();
       }

       function getLogisticsTypes() {
            return $this->db->get($this->tbl_logistics_types)->result_array();
       }

       function register($datas) {
            if (isset($datas) && !empty($datas)) {
                 $datas['usr_active'] = $this->uid == 1 ? 1 : 0;
                 $this->db->insert($this->tbl_users, $datas);
                 $userId = $this->db->insert_id();
                 $this->ion_auth->add_to_group(7, $userId);
                 return true;
            }
            return false;
       }

       function getPendingInqueries() {
            return $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->where(array($this->tbl_groups . '.id' => 6, $this->tbl_users . '.usr_active' => 0))
                            ->get($this->tbl_users)->result_array();
       }

       function selectedLogisticsTypes($userId) {
            if (!empty($userId)) {
                 $types = $this->db->select('GROUP_CONCAT(ulog_logistics_id) AS ulog_logistics_id')
                                 ->get_where($this->tbl_users_logistics, array('ulog_user_id' => $userId))->row()->ulog_logistics_id;
                 return explode(',', $types);
            }
            return false;
       }

       function add($datas) {
            if ($this->uid == 1) {
                 $datas['users']['usr_active'] = 1;
                 $datas['users']['usr_is_mail_verified'] = 1;
            }
            $this->db->insert($this->tbl_users, $datas['users']);
            $userId = $this->db->insert_id();
            $this->ion_auth->add_to_group(7, $userId);

            if (isset($datas['logTypes']) && !empty($datas['logTypes'])) {
                 foreach ($datas['logTypes'] as $key => $value) {
                      $this->db->insert($this->tbl_users_logistics, array(
                          'ulog_user_id' => $userId,
                          'ulog_logistics_id' => $value,
                          'ulog_added_by' => $this->uid
                      ));
                 }
            }

            $spcEnKey = isset($datas['specification_key']['en']) ? $datas['specification_key']['en'] : 0;
            $spcArKey = isset($datas['specification_key']['ar']) ? $datas['specification_key']['ar'] : 0;
            $spcChKey = isset($datas['specification_key']['ch']) ? $datas['specification_key']['ch'] : 0;
            $spcEnVal = isset($datas['specification_val']['en']) ? $datas['specification_val']['en'] : 0;
            $spcArVal = isset($datas['specification_val']['ar']) ? $datas['specification_val']['ar'] : 0;
            $spcChVal = isset($datas['specification_val']['ch']) ? $datas['specification_val']['ch'] : 0;
            $count = isset($spcEnKey) ? count($spcEnKey) : 0;
            for ($i = 0; $i <= $count - 1; $i++) {
                 $specifi = array(
                     'qcs_user_id' => $userId,
                     'qcs_key_en' => clean_text($spcEnKey[$i], 0),
                     'qcs_key_ar' => clean_text($spcArKey[$i], 0),
                     'qcs_key_ch' => clean_text($spcChKey[$i], 0),
                     'qcs_val_en' => clean_text($spcEnVal[$i], 0),
                     'qcs_val_ar' => clean_text($spcArVal[$i], 0),
                     'qcs_val_ch' => clean_text($spcChVal[$i], 0)
                 );
                 $this->db->insert($this->tbl_qc_specification, $specifi);
            }
            return $userId;
       }

       function edit($datas) {
            if (!empty($datas)) {
                 $userId = $datas['usr_id'];
                 $this->db->where('usr_id', $userId);
                 if ($this->uid == 1) {
                      $datas['users']['usr_active'] = 1;
                 }
                 $this->db->update($this->tbl_users, $datas['users']);
                 $this->db->where('qcs_user_id', $userId)->delete($this->tbl_qc_specification);
                 $spcEnKey = isset($datas['specification_key']['en']) ? $datas['specification_key']['en'] : 0;
                 $spcArKey = isset($datas['specification_key']['ar']) ? $datas['specification_key']['ar'] : 0;
                 $spcChKey = isset($datas['specification_key']['ch']) ? $datas['specification_key']['ch'] : 0;
                 $spcEnVal = isset($datas['specification_val']['en']) ? $datas['specification_val']['en'] : 0;
                 $spcArVal = isset($datas['specification_val']['ar']) ? $datas['specification_val']['ar'] : 0;
                 $spcChVal = isset($datas['specification_val']['ch']) ? $datas['specification_val']['ch'] : 0;
                 $count = isset($spcEnKey) ? count($spcEnKey) : 0;
                 for ($i = 0; $i <= $count - 1; $i++) {
                      $specifi = array(
                          'qcs_user_id' => $userId,
                          'qcs_key_en' => clean_text($spcEnKey[$i], 0),
                          'qcs_key_ar' => clean_text($spcArKey[$i], 0),
                          'qcs_key_ch' => clean_text($spcChKey[$i], 0),
                          'qcs_val_en' => clean_text($spcEnVal[$i], 0),
                          'qcs_val_ar' => clean_text($spcArVal[$i], 0),
                          'qcs_val_ch' => clean_text($spcChVal[$i], 0)
                      );
                      $this->db->insert($this->tbl_qc_specification, $specifi);
                 }
                 $this->db->where('ulog_user_id', $userId)->delete($this->tbl_users_logistics);
                 if (isset($datas['logTypes']) && !empty($datas['logTypes'])) {
                      foreach ($datas['logTypes'] as $key => $value) {
                           $this->db->insert($this->tbl_users_logistics, array(
                               'ulog_user_id' => $userId,
                               'ulog_logistics_id' => $value,
                               'ulog_added_by' => $this->uid
                           ));
                      }
                 }
                 return true;
            }
            return false;
       }

       function addImages($data) {
            $this->db->insert($this->tbl_qc_images, $data);
            return true;
       }

       function removeImage($id) {
            if ($id) {
                 $this->db->where('qci_id', $id);
                 $data = $this->db->get($this->tbl_qc_images)->row_array();
                 if (isset($data['qci_image']) && !empty($data['qci_image'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'qc/' . $data['qci_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'qc/' . $data['qci_image']);
                      }
                      if (file_exists(FILE_UPLOAD_PATH . 'qc/thumb_' . $data['qci_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'qc/thumb_' . $data['qci_image']);
                      }
                      $this->db->where('qci_id', $id);
                      if ($this->db->delete($this->tbl_qc_images)) {
                           generate_log(array(
                               'log_title' => 'Delete image',
                               'log_desc' => 'Removed image',
                               'log_controller' => strtolower(__CLASS__),
                               'log_action' => 'D',
                               'log_ref_id' => $id,
                               'log_added_by' => $this->uid,
                           ));
                           return true;
                      } else {
                           generate_log(array(
                               'log_title' => 'Delete image',
                               'log_desc' => 'Error while removing shop image',
                               'log_controller' => strtolower(__CLASS__),
                               'log_action' => 'D',
                               'log_ref_id' => $id,
                               'log_added_by' => $this->uid,
                           ));
                           return false;
                      }
                 }
            }
            return false;
       }

       function setDefaultImage($id) {

            $image = $this->db->get_where($this->tbl_qc_images, array('qci_id' => $id))->row_array();
            if (!empty($image)) {
                 $this->db->where('qci_user_id', $image['qci_user_id']);
                 $this->db->update($this->tbl_qc_images, array('qci_default' => 0));

                 $this->db->where('qci_id', $id);
                 if ($this->db->update($this->tbl_qc_images, array('qci_default' => 1))) {
                      return true;
                 }
            }
            return false;
       }

       function getDefaultImage($userId) {
            $image = $this->db->get_where($this->tbl_qc_images, array('qci_user_id' => $userId, 'qci_default' => 1))->row_array();
            if (!empty($image)) {
                 return UPLOAD_PATH . 'qc/' . $image['qci_image'];
            } else {
                 $image = $this->db->order_by('qci_id', 'ASC')->get_where($this->tbl_qc_images, array('qci_user_id' => $userId))->row_array();

                 if (!empty($image)) {
                      return UPLOAD_PATH . 'qc/' . $image['qci_image'];
                 }
            }
            return null;
       }

       function getQCServices($id = '') {
            if (!empty($id)) {
                 $this->db->select($this->tbl_qc_services . '.*,' . $this->tbl_users . '.*')
                         ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_qc_services . '.qcs_qc_id');
                 $data = $this->db->get_where($this->tbl_qc_services, array($this->tbl_qc_services . '.qcs_id' => $id))->row_array();
                 if (!empty($data)) {
                      $data['images'] = $this->db->get_where($this->tbl_qc_services_images, array('qcsi_service_id' => $id))->result_array();
                      return $data;
                 }
            }
            $this->db->select($this->tbl_qc_services . '.*,' . $this->tbl_users . '.*')
                    ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_qc_services . '.qcs_qc_id');
            return $this->db->get($this->tbl_qc_services)->result_array();
       }

       function newService($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_qc_services, $data);
                 return $this->db->insert_id();
            }
            return false;
       }

       function addServiceImages($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_qc_services_images, $data);
            } else {
                 return false;
            }
       }

       function getServiceDefaultImage($userId) {
            $image = $this->db->get_where($this->tbl_qc_services_images, array('qcsi_service_id' => $userId))->row_array();
            if (!empty($image)) {
                 return UPLOAD_PATH . 'qc/' . $image['qcsi_image'];
            }
            return null;
       }

       function updateService($data) {
            if (isset($data['service']) && isset($data['qcs_id'])) {
                 $this->db->where('qcs_id', $data['qcs_id']);
                 $this->db->update($this->tbl_qc_services, $data['service']);
                 return $data['qcs_id'];
            }
            return false;
       }

       function removeService($id) {
            if ($id) {
                 $this->db->where('qcs_id', $id);
                 $this->db->delete($this->tbl_qc_services);
                 $this->removeServiceImage($id);
                 return true;
            }
            return false;
       }

       function removeServiceImage($id) {
            if ($id) {
                 $this->db->where('qcsi_id', $id);
                 $data = $this->db->get($this->tbl_qc_services_images)->row_array();
                 if (isset($data['qcsi_image']) && !empty($data['qcsi_image'])) {
                      if (file_exists(FILE_UPLOAD_PATH . 'qc/' . $data['qcsi_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'qc/' . $data['qcsi_image']);
                      }
                      if (file_exists(FILE_UPLOAD_PATH . 'qc/thumb_' . $data['qcsi_image'])) {
                           unlink(FILE_UPLOAD_PATH . 'qc/thumb_' . $data['qcsi_image']);
                      }
                      $this->db->where('qcsi_id', $id);
                      if ($this->db->delete($this->tbl_qc_services_images)) {
                           generate_log(array(
                               'log_title' => 'Delete image',
                               'log_desc' => 'Removed quality control service image',
                               'log_controller' => strtolower(__CLASS__),
                               'log_action' => 'D',
                               'log_ref_id' => $id,
                               'log_added_by' => $this->uid,
                           ));
                           return true;
                      } else {
                           generate_log(array(
                               'log_title' => 'Delete image',
                               'log_desc' => 'Error while removing quality control service image',
                               'log_controller' => strtolower(__CLASS__),
                               'log_action' => 'D',
                               'log_ref_id' => $id,
                               'log_added_by' => $this->uid,
                           ));
                           return false;
                      }
                 }
            }
            return false;
       }

       function newQAQuote($datas) {
            if (!empty($datas)) {
                 $datas['quote']['qcq_added_by'] = $this->uid;
                 $this->db->insert($this->tbl_qc_quotes, $datas['quote']);
                 $quoteId = $this->db->insert_id();
                 generate_log(array(
                     'log_title' => 'Quality control quote',
                     'log_desc' => 'New quality control quote',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'C',
                     'log_ref_id' => $quoteId,
                     'log_added_by' => $this->uid,
                 ));
                 return true;
            }
            return false;
       }

       function requestQuotes($id = '') {
            if (!empty($id)) {

                 $this->db->where('qcq_id', $id);
                 $this->db->update($this->tbl_qc_quotes, array('qcq_viewed_on' => date('Y-m-d h:i:s')));

                 $data = $this->db->select($this->tbl_qc_quotes . '.*,' . $this->tbl_users . '.*,' . $this->tbl_qc_services . '.*')
                                 ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_qc_quotes . '.qcq_qc_id')
                                 ->join($this->tbl_qc_services, $this->tbl_qc_services . '.qcs_qc_id = ' . $this->tbl_users . '.usr_id')
                                 ->get_where($this->tbl_qc_quotes, array($this->tbl_qc_quotes . '.qcq_id' => $id))->row_array();
                 return $data;
            }
            return $this->db->select($this->tbl_qc_quotes . '.*,' . $this->tbl_users . '.*')
                            ->join($this->tbl_users, $this->tbl_users . '.usr_id = ' . $this->tbl_qc_quotes . '.qcq_qc_id')
                            ->get($this->tbl_qc_quotes)->result_array();
       }

       function delete($id) {
            if (!empty($id)) {
                 if (!$this->db->get_where($this->tbl_qc_services, array('qcs_qc_id' => $id))->row_array()) {
                      $this->ion_auth->delete_user($id);
                      $this->removeImage($id);
                      $this->db->where('qcs_user_id', $id)->delete($this->tbl_qc_specification);
                      return true;
                 } else {
                      return false;
                 }
            }
            return false;
       }

       function removeServiceImages($serviceId) {
            if (!empty($serviceId)) {
                 $images = $this->db->get_where($this->tbl_qc_services_images, array('qcsi_service_id' => $serviceId))->result_array();
                 if (!empty($images)) {
                      foreach ($images as $key => $value) {
                           $this->removeServiceImage($value['qcsi_id']);
                      }
                 }
            }
       }

       function removeImages($userId) {
            if (!empty($userId)) {
                 $images = $this->db->get_where($this->tbl_qc_images, array('qci_user_id' => $userId))->result_array();
                 if (!empty($images)) {
                      foreach ($images as $key => $value) {
                           $this->removeImage($value['qci_id']);
                      }
                 }
            }
       }

       function searchQC($element = '', $limit = 0, $index = 0) {
            if (!empty($element)) {
                 $this->db->like($this->tbl_users . '.usr_first_name', $element, 'both');
            }
            $data['count'] = $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' . $this->tbl_groups . '.name as group_name')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->where(array($this->tbl_groups . '.id' => 7, $this->tbl_users . '.usr_active' => 1))
                            ->get($this->tbl_users)->num_rows();

            if (!empty($limit)) {
                 $this->db->limit($limit, $index);
            }

            if (!empty($element)) {
                 $this->db->like($this->tbl_users . '.usr_first_name', $element, 'both');
            }

            $data['userDetails'] = $this->db->select($this->tbl_users . '.*,' . $this->tbl_users_groups . '.group_id as group_id, ' .
                                    $this->tbl_groups . '.name as group_name,' . $this->tbl_countries . '.*,' . $this->tbl_states . '.*')
                            ->join($this->tbl_users_groups, $this->tbl_users_groups . '.user_id = ' . $this->tbl_users . '.usr_id', 'LEFT')
                            ->join($this->tbl_groups, $this->tbl_users_groups . '.group_id = ' . $this->tbl_groups . '.id', 'LEFT')
                            ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_users . '.usr_country', 'LEFT')
                            ->join($this->tbl_states, $this->tbl_states . '.stt_id = ' . $this->tbl_users . '.usr_state', 'LEFT')
                            ->where(array($this->tbl_groups . '.id' => 7, $this->tbl_users . '.usr_active' => 1))
                            ->get($this->tbl_users)->result_array();

            if (!empty($data['userDetails'])) {
                 foreach ($data['userDetails'] as $ukey => $uvalue) {
                      $id = $uvalue['usr_id'];

                      $data['userDetails'][$ukey]['images'] = $this->db->get_where($this->tbl_qc_images, array('qci_user_id' => $id))->result_array();
                      $data['userDetails'][$ukey]['specifications'] = $this->db->get_where($this->tbl_qc_specification, array('qcs_user_id' => $id))->result_array();

                      $data['userDetails'][$ukey]['QCServices'] = $this->db->get_where($this->tbl_qc_services, array('qcs_qc_id' => $id))->result_array();
                      if (isset($data['userDetails'][$ukey]['QCServices']) && !empty($data['userDetails'][$ukey]['QCServices'])) {
                           foreach ($data['userDetails'][$ukey]['QCServices'] as $key => $value) {
                                $data['userDetails'][$ukey]['QCServices'][$key]['images'] = $this->db->get_where($this->tbl_qc_services_images, array('qcsi_service_id' => $value['qcs_id']))->result_array();
                           }
                      }
                 }
            }
            return $data;
       }

       function removeBG($uId) {
            if (!empty($uId)) {
                 $this->db->where('usr_id', $uId);
                 $data = $this->db->get($this->tbl_users)->row_array();
                 if (!empty($data)) {
                      if (isset($data['usr_banner']) && !empty($data['usr_banner'])) {
                           if (file_exists(FILE_UPLOAD_PATH . 'logistics/' . $data['usr_banner'])) {
                                unlink(FILE_UPLOAD_PATH . 'logistics/' . $data['usr_banner']);
                           }
                           if (file_exists(FILE_UPLOAD_PATH . 'logistics/thumb_' . $data['usr_banner'])) {
                                unlink(FILE_UPLOAD_PATH . 'logistics/thumb_' . $data['usr_banner']);
                           }
                           $this->db->where('usr_id', $uId);
                           if ($this->db->update($this->tbl_users, array('usr_banner' => ''))) {
                                generate_log(array(
                                    'log_title' => 'Delete image',
                                    'log_desc' => 'Removed logistics background image',
                                    'log_controller' => strtolower(__CLASS__),
                                    'log_action' => 'D',
                                    'log_ref_id' => $uId,
                                    'log_added_by' => $this->uid,
                                ));
                           } else {
                                generate_log(array(
                                    'log_title' => 'Delete image',
                                    'log_desc' => 'Error while removing logistics background image',
                                    'log_controller' => strtolower(__CLASS__),
                                    'log_action' => 'D',
                                    'log_ref_id' => $uId,
                                    'log_added_by' => $this->uid,
                                ));
                           }
                      }
                      return true;
                 }
            }
            return false;
       }

  }
  