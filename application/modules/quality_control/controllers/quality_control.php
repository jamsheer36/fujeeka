<?php

  defined('BASEPATH') or exit('No direct script access allowed');
  require './vendor/autoload.php';

  class quality_control extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Quality control';
            //   $this->assets_css = array('faq.css', 'logistic.css', 'slider.css');
            $this->load->library('upload');
            $this->load->model('quality_control_model', 'quality_control');
            require_once(APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php');
            $this->mail = new PHPMailer;
            $this->load->model('manage_banner/manage_banner_model', 'manage_banner');
       }

       function index() {
            $data['qualityCtrl'] = $this->quality_control->getActiveQC();
            $data['banner'] = $this->manage_banner->getSiteBanner(strtolower(__CLASS__));
            if($data['banner']){
               $data['banner'] = $data['banner'][0];
            }
            $this->template->set_layout('common');
            $this->render_page(strtolower(__CLASS__) . '/index', $data);
       }

       function listQa() {
            $data['inquires'] = $this->quality_control->getQC();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       public function register() {
            if ($this->input->post()) {
                 if ($this->quality_control->register($this->input->post())) {
                      /* Mail to supplier */
                      $this->mail->isSMTP();
                      $this->mail->Host = MAIL_HOST;
                      $this->mail->SMTPAuth = true;
                      $this->mail->Username = MAIL_USERNAME;
                      $this->mail->Password = MAIL_PASSWORD;
                      $this->mail->SMTPSecure = 'ssl';
                      $this->mail->Port = 465;
                      $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                      $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                      $this->mail->addAddress($this->input->post('usr_email'));
                      $this->mail->Subject = 'New Quality Controll Registration';
                      $this->mail->isHTML(true);
                      $mailData['email'] = $this->input->post('usr_email');
                      $this->mail->Body = $this->load->view('qa-register-mail-template', $mailData, true);
                      $this->mail->send();
                      /* Mail end */
                      die(json_encode(array('status' => 'success', 'msg' => 'We will get back to you')));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => 'Email already exists')));
                 }
            } else {
                 $this->page_title = 'Quality control registration';
                 $this->template->set_layout('common');
                 $this->render_page(strtolower(__CLASS__) . '/register');
            }
       }

       function inquires() {
            $data['inquires'] = $this->quality_control->getPendingInqueries();
            $this->render_page(strtolower(__CLASS__) . '/inquires', $data);
       }

       function add() {
            if (!empty($this->input->post())) {
                 
                 // Background image
                 if (isset($_FILES['userAvatar']['name']) && !empty($_FILES['userAvatar']['name'])) {
                      $pixel = array();
                      $uploadData = array();
                      $newFileName = microtime(true) . clean_image_name($_FILES['userAvatar']['name']);
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'qc/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $pixel['x1'][0] = $_POST['x11'];
                      $pixel['y1'][0] = $_POST['y11'];
                      $pixel['x2'][0] = $_POST['x21'];
                      $pixel['y2'][0] = $_POST['y21'];
                      $pixel['w'][0] = $_POST['w1'];
                      $pixel['h'][0] = $_POST['h1'];
                      if ($this->upload->do_upload('userAvatar')) {
                           $uploadData = $this->upload->data();
                           crop($uploadData, $pixel);
                           $_POST['users']['usr_banner'] = $uploadData['file_name'];
                      }
                 }
                 
                 $userId = $this->quality_control->add($this->input->post());

                 $x1 = $this->input->post('x12');
                 $fileCount = count($x1);
                 for ($j = 0; $j < $fileCount; $j++) {

                      $data = array();
                      $angle = array();
                      $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'qc/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $angle['x1']['0'] = $_POST['x12'][$j];
                      $angle['x2']['0'] = $_POST['x22'][$j];
                      $angle['y1']['0'] = $_POST['y12'][$j];
                      $angle['y2']['0'] = $_POST['y22'][$j];
                      $angle['w']['0'] = $_POST['w2'][$j];
                      $angle['h']['0'] = $_POST['h2'][$j];
                      $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                      $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                      $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                      $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                      $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];

                      if (!$this->upload->do_upload('prd_image_tmp')) {
                           $error = array('error' => $this->upload->display_errors());
                      } else {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle, false);
                           $this->quality_control->addImages(array('qci_user_id' => $userId, 'qci_image' => $data['file_name']));
                      }
                 }
                 $this->session->set_flashdata('app_success', 'Quality control successfully added!');
                 redirect(strtolower(__CLASS__) . '/listQa');
            } else {
                 $data['country'] = get_country_list();
                 $this->render_page(strtolower(__CLASS__) . '/add', $data);
            }
       }

       function edit($id = 0) {
            if (!empty($this->input->post())) {
                 $userId = $this->input->post('usr_id');
                 
                 // Background image
                 if (isset($_FILES['userAvatar']['name']) && !empty($_FILES['userAvatar']['name'])) {
                      $pixel = array();
                      $uploadData = array();
                      $newFileName = microtime(true) . clean_image_name($_FILES['userAvatar']['name']);
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'qc/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $pixel['x1'][0] = $_POST['x11'];
                      $pixel['y1'][0] = $_POST['y11'];
                      $pixel['x2'][0] = $_POST['x21'];
                      $pixel['y2'][0] = $_POST['y21'];
                      $pixel['w'][0] = $_POST['w1'];
                      $pixel['h'][0] = $_POST['h1'];
                      if ($this->upload->do_upload('userAvatar')) {
                           $uploadData = $this->upload->data();
                           crop($uploadData, $pixel);
                           $_POST['users']['usr_banner'] = $uploadData['file_name'];
                      }
                 }
                 
                 $this->quality_control->edit($this->input->post());
                 $x1 = $this->input->post('x12');
                 $fileCount = count($x1);
                 for ($j = 0; $j < $fileCount; $j++) {

                      $data = array();
                      $angle = array();
                      $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'qc/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $angle['x1']['0'] = $_POST['x12'][$j];
                      $angle['x2']['0'] = $_POST['x22'][$j];
                      $angle['y1']['0'] = $_POST['y12'][$j];
                      $angle['y2']['0'] = $_POST['y22'][$j];
                      $angle['w']['0'] = $_POST['w2'][$j];
                      $angle['h']['0'] = $_POST['h2'][$j];
                      $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                      $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                      $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                      $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                      $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];

                      if (!$this->upload->do_upload('prd_image_tmp')) {
                           $error = array('error' => $this->upload->display_errors());
                      } else {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle, false);
//                           qci_user_id
                           $this->quality_control->removeImages($userId);
                           $this->quality_control->addImages(array('qci_user_id' => $userId, 'qci_image' => $data['file_name']));
                      }
                 }
                 $this->session->set_flashdata('app_success', 'Quality control successfully updated!');
                 redirect(strtolower(__CLASS__) . '/listQa');
            } else {
                 $id = encryptor($id, 'D');
                 $data = $this->quality_control->getQC($id);
                 $data['country'] = get_country_list();
                 $countryId = isset($data['userDetails']['usr_country']) ? $data['userDetails']['usr_country'] : 0;
                 $data['states'] = get_state_province('', $countryId);
                 $data['country'] = get_country_list();
                 $this->render_page(strtolower(__CLASS__) . '/edit', $data);
            }
       }

       function changestatus($userId) {
            $ischecked = isset($_POST['ischecked']) ? $_POST['ischecked'] : 0;
            $userId = encryptor($userId, 'D');

            if ($ischecked == 1) { // Active
                 if ($this->ion_auth->update($userId, array('usr_active' => 1))) {
                      generate_log(array(
                          'log_title' => 'Status changed',
                          'log_desc' => 'Logistics activated',
                          'log_controller' => 'logistics-status-changed',
                          'log_action' => 'U',
                          'log_ref_id' => $userId,
                          'log_added_by' => $this->uid
                      ));
                      die(json_encode(array('status' => 'success', 'msg' => "Quality control successfully activated")));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => "Can't change status")));
                 }
            } else { // de activate
                 if ($this->ion_auth->deactivate($userId)) {
                      generate_log(array(
                          'log_title' => 'Status changed',
                          'log_desc' => 'Logistics de-activated',
                          'log_controller' => 'logistics-status-changed',
                          'log_action' => 'U',
                          'log_ref_id' => $userId,
                          'log_added_by' => $this->uid
                      ));
                      die(json_encode(array('status' => 'success', 'msg' => "Quality control successfully de-activated")));
                 } else {
                      die(json_encode(array('status' => 'fail', 'msg' => "Can't varify supplier")));
                 }
            }
       }

       public function removeImage($id) {
            if ($this->quality_control->removeImage($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Quality control image successfully deleted'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete quality control"));
                 die();
            }
       }

       function setDefaultImage($id) {
            if ($this->quality_control->setDefaultImage($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Image set as default!')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't set default image")));
            }
       }

       function services() {
            $data['services'] = $this->quality_control->getQCServices();
            $this->render_page(strtolower(__CLASS__) . '/services', $data);
       }

       function new_service() {
            if ($this->input->post()) {
                 $serviceId = $this->quality_control->newService($this->input->post('service'));

                 $x1 = $this->input->post('x12');
                 $fileCount = count($x1);
                 for ($j = 0; $j < $fileCount; $j++) {

                      $data = array();
                      $angle = array();
                      $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'qc/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $angle['x1']['0'] = $_POST['x12'][$j];
                      $angle['x2']['0'] = $_POST['x22'][$j];
                      $angle['y1']['0'] = $_POST['y12'][$j];
                      $angle['y2']['0'] = $_POST['y22'][$j];
                      $angle['w']['0'] = $_POST['w2'][$j];
                      $angle['h']['0'] = $_POST['h2'][$j];
                      $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                      $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                      $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                      $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                      $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];

                      if (!$this->upload->do_upload('prd_image_tmp')) {
                           $error = array('error' => $this->upload->display_errors());
                      } else {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle, false);
                           $this->quality_control->addServiceImages(array('qcsi_service_id' => $serviceId, 'qcsi_image' => $data['file_name']));
                      }
                 }
                 $this->session->set_flashdata('app_success', 'Quality control service successfully added!');
                 redirect(strtolower(__CLASS__) . '/services');
            } else {
                 $data['qc'] = $this->quality_control->getQC();
                 $this->render_page(strtolower(__CLASS__) . '/new_service', $data);
            }
       }

       function edit_service($id = '') {
            if ($this->input->post()) {
                 $serviceId = $this->quality_control->updateService($this->input->post());

                 $x1 = $this->input->post('x12');
                 $fileCount = count($x1);
                 for ($j = 0; $j < $fileCount; $j++) {

                      $data = array();
                      $angle = array();
                      $newFileName = microtime(true) . $_FILES['shopImages']['name'][$j];
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'qc/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);
                      $angle['x1']['0'] = $_POST['x12'][$j];
                      $angle['x2']['0'] = $_POST['x22'][$j];
                      $angle['y1']['0'] = $_POST['y12'][$j];
                      $angle['y2']['0'] = $_POST['y22'][$j];
                      $angle['w']['0'] = $_POST['w2'][$j];
                      $angle['h']['0'] = $_POST['h2'][$j];
                      $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                      $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                      $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                      $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                      $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];

                      if (!$this->upload->do_upload('prd_image_tmp')) {
                           $error = array('error' => $this->upload->display_errors());
                      } else {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle, false);
                           //qcsi_service_id
                           $this->quality_control->removeServiceImages($serviceId);
                           $this->quality_control->addServiceImages(array('qcsi_service_id' => $serviceId, 'qcsi_image' => $data['file_name']));
                      }
                 }
                 $this->session->set_flashdata('app_success', 'New service added under quality controll successfully added!');
                 redirect(strtolower(__CLASS__) . '/services');
            } else {
                 $id = encryptor($id, 'D');
                 $data['service'] = $this->quality_control->getQCServices($id);
                 $data['qc'] = $this->quality_control->getQC();
                 $this->render_page(strtolower(__CLASS__) . '/edit_service', $data);
            }
       }

       function delete_service($id) {
            if ($this->quality_control->removeService($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Service successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete service")));
            }
       }

       function removeServiceImage($id) {
            if ($this->quality_control->removeServiceImage($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Image successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete image")));
            }
       }

       function getQuote($id = '') {
            $id = encryptor($id, 'D');
            $data = $this->quality_control->getQC($id);
            $this->template->set_layout('common');
            $this->render_page(strtolower(__CLASS__) . '/get-a-quote', $data);
       }

       function newQAQuote() {
            if ($this->quality_control->newQAQuote($this->input->post())) {
                 $datas = $this->input->post('quote');
                 /* Mail to supplier */
                 $this->mail->isSMTP();
                 $this->mail->Host = MAIL_HOST;
                 $this->mail->SMTPAuth = true;
                 $this->mail->Username = MAIL_USERNAME;
                 $this->mail->Password = MAIL_PASSWORD;
                 $this->mail->SMTPSecure = 'ssl';
                 $this->mail->Port = 465;
                 $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                 $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                 $this->mail->addAddress($datas['qcq_email']);
                 $this->mail->Subject = 'New Quality Control Quote';
                 $this->mail->isHTML(true);
                 $mailData['email'] = $datas['qcq_email'];
                 $this->mail->Body = $this->load->view('qa-quote-mail-template', $mailData, true);
                 $this->mail->send();
                 /* Mail end */
                 die(json_encode(array('status' => 'success', 'msg' => 'We will get back to you!')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Error occured!")));
            }
       }

       function request_quotes() {
            $data['requests'] = $this->quality_control->requestQuotes();
            $this->render_page(strtolower(__CLASS__) . '/request-quotes', $data);
       }
       
       function request_quotes_details($id) {
             $id = encryptor($id, 'D');
             $data['requests'] = $this->quality_control->requestQuotes($id);
             $this->render_page(strtolower(__CLASS__) . '/request-quotes-details', $data);
       }
       
       function delete($id) {
            if ($this->quality_control->delete($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Quality control successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete quality control, it may already used")));
            }
       }
       
       function removeBG($usrId) {
            if ($this->quality_control->removeBG($usrId)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Background successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete background image")));
            }
       }
  }
  