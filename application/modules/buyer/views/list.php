<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Buyers</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Avatar</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>First name</th>
                                        <th>Last name</th>
                                        <th>Group</th>
                                        <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody> 
                                   <?php
                                     if (!empty($users)) {
                                          foreach ($users as $key => $value) {
                                               ?>
                                               <tr data-url="<?php echo site_url('buyer/view/' . encryptor($value['usr_id']));?>">
                                                    <td class="trVOE">
                                                         <?php
                                                         echo img(array('src' => 'assets/uploads/avatar/' . $value['usr_avatar'], 'width' => '50', 'id' => 'imgBrandImage'));
                                                         ?>
                                                    </td>
                                                    <td class="trVOE"><?php echo $value['usr_username'];?></td>
                                                    <td class="trVOE"><?php echo $value['usr_email'];?></td>
                                                    <td class="trVOE"><?php echo $value['usr_first_name'];?></td>
                                                    <td class="trVOE"><?php echo $value['usr_last_name'];?></td>
                                                    <td class="trVOE"><?php echo $value['group_name'];?></td>
                                                    <?php if (check_permission($controller, 'delete')) {?>
                                                         <td>
                                                              <a class="pencile deleteListItem" href="javascript:void(0);" data-url="<?php echo site_url('buyer/delete/' . $value['usr_id']);?>">
                                                                   <i class="fa fa-remove"></i>
                                                              </a>
                                                         </td>
                                                    <?php }?>
                                               </tr>
                                               <?php
                                          }
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>