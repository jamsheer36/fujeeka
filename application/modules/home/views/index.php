<section class="white-strip">
     <div class="container">
          <ul>
               <li onclick="window.location.href = '<?php echo site_url('market/markets');?>'">
                    <div class="icons-container">
                         <div class="icn">
                              <img src="images/home-market-icon.png" alt="market icon">
                         </div>
                         <div class="icn-txt">
                              <h5>Market</h5>
                              <p>Wholesale</p>
                         </div>
                    </div>
               </li>

               <li onclick="window.location.href = '<?php echo site_url('product/product-listing');?>'">
                    <div class="icons-container">
                         <div class="icn">
                              <img src="images/home-product-icon.png" alt="products icon">
                         </div>
                         <div class="icn-txt">
                              <h5>Products</h5>
                              <p>Stock & OEM</p>
                         </div>
                    </div>
               </li>

               <li onclick="window.location.href = '<?php echo site_url('product/stock');?>'">
                    <div class="icons-container">
                         <div class="icn">
                              <img src="images/home-stock-icon.png" alt="Stock icon">
                         </div>
                         <div class="icn-txt">
                              <h5>Stock</h5>
                              <p>Stock and Flash stocks</p>
                         </div>
                    </div>
               </li>

               <?php if(!$this->uid) { ?>
               <li data-toggle="modal" data-target="#loginModal"  data-redrct="redirect_rfq">
                    <div class="icons-container">
                         <div class="icn">
                              <img src="images/home-rfq-icon.png" alt="RFQ icon">
                         </div>
                         <div class="icn-txt">
                              <h5>RFQ</h5>
                              <p>Request for quotes</p>
                         </div>
                    </div>
               </li>
               <?php } else{ ?>
               <li onclick="window.location.href = '<?php
                 echo ($this->usr_grp == 'SP' && check_permission('product', 'rfq_list')) ?
                         site_url('product/rfq_list') : site_url('product/rfq');?>'">

                    <div class="icons-container">
                         <div class="icn">
                              <img src="images/home-rfq-icon.png" alt="RFQ icon">
                         </div>
                         <div class="icn-txt">
                              <h5>RFQ</h5>
                              <p>Request for quotes</p>
                         </div>
                    </div>
               </li>
               <?php } ?>

               <li onclick="window.location.href = '<?php echo site_url('logistics');?>'">
                    <div class="icons-container">
                         <div class="icn">
                              <img src="images/home-logistic-icon.png" alt="Logistics icon">
                         </div>
                         <div class="icn-txt">
                              <h5>Logistics</h5>
                              <p>Shipping options</p>
                         </div>
                    </div>
               </li>
          </ul>
     </div>
</section>
<div class="slide">
     <ul>
          <?php foreach ((array) $banner as $key => $value) {?>
                 <li data-bg="<?php echo 'uploads/banner/' . $value['bnr_image'];?>" alt="<?php echo $value['bnr_imagealt'];?>"></li>
            <?php }?>
     </ul>
</div>
<!--/ green strip -->

<section class="prod-display m-1t">
     <div class="container">
          <div class="row">
               <!-- col 9 -->
               <div class="col-md-9">
                    <div class="prd-title padl-10">
                         <h3>Hot Selling</h3>
                         <div class="v-all">
                              <a href="<?= site_url('product/product-listing?type=hot-selling')?>">View all</a>
                         </div>
                    </div>

                    <div class="prod-container">
                         <ul>

                              <!-- single product -->
                              <?php foreach ($hot_selling_product as $prod) {?>
                                     <li onclick="window.location.href = '<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>'">
                                          <div class="prod-box">
                                               <div class="prod-img">
                                               <!-- <img class="lazy" src="<?=site_url().$prod['lazy_image']?>" data-src="<?=site_url().$prod['default_image']?>" alt="<?=$prod['pimg_alttag']?>"/> -->
                                               <img  src="<?=site_url().$prod['default_image']?>"  alt="<?=$prod['pimg_alttag']?>"/>
                                               </div>
                                               <?php if ($prod['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?>
                                               <div class="prod-detail txt-height">
                                                    <h4 class="prod-title-s"><?php echo get_snippet($prod['prd_name_en'], 4);?></h4>
                                                    <?php if ($prod['prd_offer_price_min'] > 0) {?>
                                                         <p><span class="strike-price">US $<?php echo $prod['prd_price_min'];?></span> <span class="price-text">US $<?php echo $prod['prd_offer_price_min'];?></span></p>
                                                    <?php } else {?>
                                                         <p>From <span class="price-text">US $<?php echo $prod['prd_price_min'];?></span>  </p>
                                                    <?php }?>

                                               </div>
                                               <!-- <div class="favourite">
                                                    <a is-logged="<?php echo ($this->uid > 0) ? '1' : $this->calldrop;?>" href="javascript:;" class="btnFav" data-url="<?php echo site_url('favorites/addProductToFavoriteList/' . encryptor($prod['prd_id']));?>">
                                                         <i id="fav" class="far fa-heart <?php echo (isset($prod['isFavorite']) && !empty($prod['isFavorite'])) ? ' fas red-icn' : '';?>"></i></a>
                                               </div> -->
                                          </div>
                                     </li>
                                <?php }?>

                              <!-- /single product -->


                         </ul>

                    </div>
               </div>
               <!-- col 9 ends -->
               <!-- col 3 -->
               <div class="col-md-3">
                    <div class="wh-sps">
                         <div class="prd-title">
                              <h3>360&#176; Shops</h3>
                         </div>

                         <div class="slide-wholesale">
                              <div id="thumbnail-slider" style="float:left;">
                                   <div class="inner">
                                        <ul>
                                             <?php foreach ($suppliers as $supl) {?>

                                                    <li>
                                                         <a style="cursor: pointer" data-dmn="<?=$supl['supm_domain_prefix']?>"
                                                            data-toggle="tooltip" title="<?php echo $supl['supm_name_en']?>"
                                                            data-id="<?php echo encryptor($supl['supm_id']);?>" class="thumb" href="<?php echo $supl['default_image'];?>"  alt="<?php echo $supl['ssi_alttag'];?>" ></a>
                                                            <?php if ($supl['supm_panoramic_image']) {?>
                                                                 <div class="tp-360">
                                                                      <a data-toggle="modal" id="_360_a" class="nav-link btn-360" href="<?php echo site_url() . 'supplier/get_360/' . encryptor($supl['supm_id']);?>" data-target="#Modal-360" title="View the shop in 360 viewer"><img src="images/360-b.png" alt="" class="img-fluid"></a>
                                                                 </div>
                                                            <?php } ?>
                                                    </li>
                                               <?php }?>

                                        </ul>
                                        <a href="<?php echo site_url('market/markets');?>"><div class="fs-icon" title="View All"></div></a>
                                   </div>
                              </div>
                         </div>

                         <div class="box-360">
                              <h2>Introducing a new feature</h2>
                              <img src="images/360-hme.png" alt="view 360">
                              <a href="<?=site_url('supplier/360-market')?>">Click here to explore</a>
                         </div>

                    </div>
               </div>
               <!-- col 3 ends -->
          </div>
     </div>
</section>



<section class="prod-display">
     <div class="container">
          <div class="row">
               <div class="col-md-12">
                    <div class="prd-title padl-10">
                         <h3>Latest Products</h3>
                         <div class="v-all">
                              <a href="<?= site_url('product/product-listing')?>">View all</a>
                         </div>
                    </div>
               </div>
          </div>

          <div class="row">
               <div class="col-md-12">
                    <div class="d-greybg">
                         <div class="row">
                              <!-- col-3 -->
                              <div class="col-md-4">
                                   <div class="banner">
                                        <div class="ban-bg">
                                             <div class="ban-txt">

                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <!-- col-3 ends -->

                              <!-- col 9 -->
                              <div class="col-md-8">
                                   <div class="prod-container">
                                        <ul>
                                             <!-- single product -->
                                             <?php foreach ($latest_product as $prod) {?>
                                                    <li onclick="location.href = '<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>'">
                                                         <div class="prod-box">
                                                              <div class="prod-img">
                                                              <img class="" src="<?=site_url().$prod['default_image']?>"  alt="<?=$prod['pimg_alttag']?>"/>
                                                              </div>
                                                              <?php if ($prod['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?>
                                                              <div class="prod-detail txt-height">
                                                                   <h4 class="prod-title-s"><?php echo $prod['prd_name_en'];?></h4>

                                                              </div>

                                                         </div>
                                                    </li>
                                               <?php }?>
                                             <!-- /single product -->
                                        </ul>
                                   </div>
                              </div>
                              <!-- col 9 ends -->
                         </div>
                    </div>
               </div>
          </div>
     </div>
</section>
<section class="hotsell padb-10 tp-adjustment pt-5">
     <div class="container">
          <div class="row">
               <div class="col-12">
                    <div class="prd-title padl-10">
                         <h3>Offers</h3>
                         <div class="v-all">
                              <a href="<?= site_url('product/product-listing?type=offer')?>">View all</a>
                         </div>
                    </div>
                    <div class="prod-container">
                         <ul id="hot-prod" class="content-slider">
                              <!-- single product -->
                              <?php foreach ($offer_product as $prod) {?>
                                     <li onclick="location.href = '<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>'">
                                          <div class="prod-box">
                                               <div class="prod-img">
                                               <img class="" src="<?=site_url().$prod['default_image']?>"  alt="<?=$prod['pimg_alttag']?>"/>
                                               </div>
                                               <?php if ($prod['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?>
                                               <div class="prod-detail txt-height">
                                                    <h4 class="prod-title-s"><?php echo $prod['prd_name_en'];?></h4>
                                                    <p><span class="strike-price">US $<?php echo $prod['prd_price_min'];?></span> <span class="price-text">US $<?php echo $prod['prd_offer_price_min'];?></span></p>
                                                    <p class="moq"><?php echo $prod['prd_moq'];?> <?php echo $prod['unt_unit_name_en'];?>  (MOQ)</p>
                                               </div>

                                          </div>
                                     </li>
                                <?php }?>
                              <!-- /single product -->


                         </ul>

                    </div>
               </div>
          </div>

          <div class="clearfix"></div>

          <!-- sub section starts -->
          <div class="sub-section">
               <div class="row">
                    <div class="col-lg-5 col-md-12">
                         <!-- stock prod -->
                         <div class="stock-prod-container">
                              <h2 class="heading2">Stock Products</h2>

                              <ul id="stock-prod" class="content-slider">
                                   <?php foreach ($stock_product as $prod) {?>
                                          <!--single product -->
                                          <li>
                                               <div class="stock-pic">
                                                  <img class="" src="<?=site_url().$prod['default_image']?>" alt="<?=$prod['pimg_alttag']?>"/>
                                               </div>
                                               <div class="stock-details txt-height">
                                                    <h4 class="prod-title-s"><?php echo $prod['prd_name_en'];?></h4>
                                                    <?php if ($prod['prd_offer_price_min'] > 0) {?>
                                                         <p><span class="strike-price">US $<?php echo $prod['prd_price_min'];?></span> <span class="price-text">US $<?php echo $prod['prd_offer_price_min'];?></span></p>
                                                    <?php } else {?>
                                                         <p>From <span class="price-text">US $<?php echo $prod['prd_price_min'];?></span> /<?php echo $prod['unt_unit_name_en'];?> </p>
                                                    <?php }?>
                                                    <p>Stock <span class="price-text"><?php echo floatval($prod['prd_stock_qty']);?> <?php echo $prod['unt_unit_name_en'];?></span></p>
                                                    <button onclick="location.href = '<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>'" class="btn btn-success order-now">Inquire now</button>
                                               </div>
                                          </li>
                                          <!-- /single product -->
                                     <?php }?>
                              </ul>
                         </div>
                         <!-- /stock prod -->
                    </div>

                    <div class="col-lg-7 col-md-12">
                         <div class="advert">
                              <ul id="advt" class="content-slider">
                                   <?php foreach ((array) $add as $key => $value) {?>
                                          <li>
                                               <a target="_blank" href="<?php echo empty($value['add_url']) ? 'javascript:;' : $value['add_url'] . "?adds=" . encryptor($value['add_id']);?>">
                                                    <?php echo img(array('src' => FILE_UPLOAD_PATH . '/advt/' . $value['add_image'],'alt'=>$value['add_imgalt']));?>
                                               </a>
                                          </li>
                                     <?php }?>
                              </ul>
                         </div>
                    </div>
               </div>

          </div>
          <!-- /sub section -->
     </div>
</section>
<section class="recommended padb-10 pt-4">
     <div class="container">
          <div class="row">
               <div class="col-12">
                    <div class="prd-title padl-10">
                         <h3>Latest Stock</h3>
                         <div class="v-all">
                              <a href="<?= site_url('product/stock')?>">View all</a>
                         </div>
                    </div>
                    <div class="prod-container">
                         <ul>
                              <!-- single product -->
                              <?php foreach ($stock_product as $prod) {?>
                                     <li onclick="location.href = '<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>'">
                                          <div class="prod-box">
                                               <div class="prod-img">
                                                  <img class="" src="<?=site_url().$prod['default_image']?>"  alt="<?=$prod['pimg_alttag']?>"/>
                                               </div>
                                               <?php if ($prod['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?>
                                               <div class="prod-detail txt-height">
                                                    <h4 class="prod-title-s"><?php echo $prod['prd_name_en'];?></h4>
                                                    <?php if ($prod['prd_offer_price_min'] > 0) {?>
                                                         <p><span class="strike-price">US $<?php echo $prod['prd_price_min'];?></span> <span class="price-text">US $<?php echo $prod['prd_offer_price_min'];?></span></p>
                                                    <?php } else {?>
                                                         <p>From <span class="price-text">US $<?php echo $prod['prd_price_min'];?></span> /<?php echo $prod['unt_unit_name_en'];?> </p>
                                                    <?php }?>                                             <p class="moq"><?php echo $prod['prd_moq'];?> <?php echo $prod['unt_unit_name_en'];?>  (MOQ)</p>
                                               </div>

                                          </div>
                                     </li>
                                <?php }?>
                              <!-- /single product -->
                         </ul>

                    </div>
               </div>
          </div>
     </div>
</section>


<section class="sub-footer tp-adjustment pt-5">
     <div class="container">
          <div class="row">
               <div class="col-md-6">
                    <div class="prd-title">
                         <h3>Catch your flash sale stock</h3>
                         <div class="v-all pr-0">
                              <a href="<?= site_url('product/stock?type=flash_sale')?>">View all</a>
                         </div>
                    </div>

                    <div class="card">
                         <div class="block-hdnews">
                              <div class="cat-head">
                                   <div class="prd-name">PRODUCT</div>
                                   <div class="prd-prce">PRICE</div>
                                   <div class="prd-update">UPDATED</div>
                                   <div class="prd-stck">STOCK</div>
                              </div>
                              <div class="scroll-container">

                                   <div class="list-wrpaaer" style="height:380px">
                                        <ul class="list-aggregate" id="marquee-vertical">
                                             <?php if (!$flash_stock) {?>
                                                    <div class="empty-list">No Products Found!!</div>
                                                    <?php
                                               } else {
                                                    foreach ($flash_stock as $prod) {
                                                         $name = strlen($prod['prd_name_en']) > 40 ? substr($prod['prd_name_en'], 0,40).'..' : $prod['prd_name_en'];
                                                         $updated_time = date('Y-m-d H:i:s', strtotime($prod['prd_updated_on']));
                                                         ?>
                                                         <li onclick="location.href = '<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>'" style="cursor:pointer">
                                                              <div class="prd-name">
                                                                   <div class="img">
                                                                   <img class="" src="<?=site_url().$prod['default_image']?>"  alt="<?=$prod['pimg_alttag']?>"/>
                                                                   </div>
                                                                   <div class="nme">
                                                                        <h5><?php echo $name;?></h5>
                                                                   </div>
                                                              </div>
                                                              <?php if ($prod['prd_offer_price_min'] > 0) {?>
                                                                   <div class="prd-prce"><del>US$ <?php echo $prod['prd_price_min'];?></del> US$ <?php echo $prod['prd_offer_price_min'];?></div>
                                                              <?php } else {?>
                                                                   <div class="prd-prce"> US$ <?php echo $prod['prd_price_min'];?></div>
                                                              <?php }?>
                                                              <div class="prd-update"><?= get_timeago($updated_time);?></div>
                                                              <div class="prd-stck"><?php echo floatval($prod['prd_stock_qty']);?> <?php echo $prod['unt_unit_name_en'];?></div>
                                                         </li>
                                                         <?php
                                                    }
                                               }
                                             ?>
                                        </ul>
                                   </div>
                                   <!-- list-wrpaaer -->

                              </div>
                         </div>
                    </div>
               </div>

               <div class="col-md-6">
                    <div class="prd-title">
                         <h3>Market</h3>
                    </div>

                    <div class="card mkt-card" style="height:360px">
                         <div class="market-pic">
                              <img src="images/banners/market-pic.png" alt="Market Name">
                         </div>
                         <ul>
                              <?php foreach ($building as $build) {?>
                                     <li style="cursor:pointer" onclick="location.href = '<?= site_url('market/markets?building=' . $build['bld_id'] . '&market=' . $market['mar_id'])?>'"><?= $build['bld_title'];?></li>
                                <?php }?>
                         </ul>


                         <div class="m-name">
                              <h3><?= $market['mar_name']?></h3>
                              <a href="<?= site_url('market/markets?market=' . $market['mar_id'])?>" class="btn btn-success btn-vall">View all buildings</a>
                         </div>


                    </div>
               </div>
          </div>
     </div>
</section>
<?php if (!empty($recommended)) {?>

       <!-- recommended -->
       <section class="recommended tp-adjustment padb-20 pt-5">
            <div class="container">
                 <div class="row">
                      <div class="col-12">
                           <div class="prd-title padl-10">
                                <h3>Recommended Products</h3>
                                <!-- <div class="v-all">
                                     <a href="javascript:;">View all</a>
                                </div> -->
                           </div>
                           <div class="prod-container">
                                <ul>
                                     <!-- single product -->
                                     <?php foreach ($recommended as $prod) {?>
                                          <li onclick="location.href = '<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>'">
                                               <div class="prod-box">
                                                    <div class="prod-img">
                                                         <?php echo img(array('src' => $prod['default_image']));?>
                                                    </div>
                                                    <!-- <?php if ($prod['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?> -->
                                                    <div class="prod-detail txt-height">
                                                         <h4 class="prod-title-s"><?php echo $prod['prd_name_en'];?></h4>
                                                         <p>From <span class="price-text">US $<?php echo $prod['prd_price_min'];?></span></p>
                                                         <p class="moq"><?php echo $prod['prd_moq'];?> Piece (MOQ)</p>
                                                    </div>
                                               </div>
                                          </li>
                                     <?php }?>
                                     <!-- /single product -->
                                </ul>

                           </div>
                      </div>
                 </div>
            </div>
       </section>
       <!-- /recommended -->
  <?php } if (!empty($recent_product)) {?>
       <section class="recent padb-30 pt-4">
            <div class="container">
                 <div class="row">
                      <div class="col-12">
                           <div class="prd-title padl-10">
                                <h3>Recently Viewed</h3>
                           </div>
                           <div class="recent-container">
                                <ul id="recent-prod" class="content-slider">
                                     <?php foreach ($recent_product as $prod) {?>
                                          <li>
                                               <!-- product -->
                                               <a href="<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>">
                                                    <div class="recent-box">
                                                         <?php echo img(array('src' => $prod['default_image'] ,
                                                         'alt' => $prod['pimg_alttag'] ));?>
                                                    </div>
                                               </a>
                                               <!-- /product -->
                                          </li>
                                     <?php }?>
                                </ul>
                           </div>
                      </div>
                 </div>
            </div>
       </section>
  <?php }?>

<style>
     .fs-icon {
          /* display:none !important; */
     }
</style>

<!-- 360 modal -->
<div id="Modal-360" class="modal fade modal-360" role="dialog">
     <div class="modal-dialog">
          <!--Filter Modal content-->
          <div class="modal-content">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <div class="modal-body 360_body">
               </div>
          </div>
     </div>
</div>
<!-- /360 modal -->
