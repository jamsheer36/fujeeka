    <section class="dapp-content">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mt-8">
            <h1>Access to millions of products anytime anywhere</h1>
            <p>Take your business on the go with the Fujeeka App</p>
            <div class="download-btns">
              <img src="images/app-store-b.png" alt="Download iOS App">
              <img src="images/playstore-b.png" alt="Download Android App">
            </div>
          </div>
          <div class="col-md-6 mt-8">
            <div class="dapp-banner">
              <img src="images/banners/down-app-banner.png" alt="Download app">
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="app-scan">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <div class="scan-qrc">
              <div class="app-qr">
                <img src="images/qr-app.png" alt="scan code to install the app">
                <!--<img src="images/app-store-qr.png" alt="scan code to install ios app">-->
                <!--<img src="images/playstore-qr.png" alt="scan code to install android app">-->
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <div class="scan-txt">
              <h1>Scan QR Code to install the app</h1>
              <p>Keep in touch with the latest offers & promotions</p>
            </div>
          </div>
        </div>
      </div>
    </section>

