<section class="gstrip">
     <div class="container">
          <ul>
               <li onclick="window.location.href = '<?php echo site_url('market/markets');?>'">
                    <div class="icn"><img src="images/market-icon.png" alt="market icon"></div>
                    <div class="icn-txt">
                         <h2>Market Shop</h2>
                         <p>Wholesale</p>
                    </div>
               </li>
               <li onclick="window.location.href = '<?php echo site_url('product/product-listing');?>'">
                    <div class="icn"><img src="images/stock-icon.png" alt="stock icon"></div>
                    <div class="icn-txt">
                         <h2>Products</h2>
                         <p>Stock & OEM.</p>
                    </div>
               </li>
               <li onclick="window.location.href = '<?php echo site_url('product/rfq');?>'">
                    <div class="icn"><img src="images/rfq-icon.png" alt="rfq icon"></div>
                    <div class="icn-txt">
                         <h2>RFQ</h2>
                         <p>Request pricing</p>
                    </div>
               </li>
          </ul>
     </div>
</section>
<!--/ green strip -->
<section class="shop-container">
     <div class="container">
          <div class="shops-total">
               <?php if (!empty($latest_product)) {?>
                      <div class="col-lg-3 dn">
                           <div class="latest-container">
                                <h2 class="heading2">Latest Products</h2>

                                <ul id="latest-prod" class="content-slider">
                                     <?php foreach ($latest_product as $prod) {?>
                                          <li style="cursor: pointer" onclick="window.location.href = '<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>'">
                                               <div class="prod-bx">
                                                    <div class="img-bx">
                                                         <?php echo img(array('src' => $prod['default_image'], 'id' => '', 'alt' => '', 'class' => 'img-responsive img-fluid'));?>
                                                    </div>
                                                    <div class="detail-bx txt-height">
                                                         <h3><?php echo get_snippet($prod['prd_name_en'], 3);?></h3>
                                                         <p>From <span class="price-text">US $<?php echo $prod['prd_price_min'];?></span> / Piece</p>
                                                         <p><?php echo $prod['prd_moq'];?> Pieces (MOQ)</p>
                                                         <a href="<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>"><button class="btn btn-success ">View Details</button></a>
                                                    </div>
                                               </div>
                                          </li>
                                     <?php }?> 
                                </ul>
                           </div>
                      </div>
                 <?php } if (!empty($suppliers)) {?>
                      <div class="col-lg-9 col-md-12">
                           <div class="shop-box">
                                <h2 class="heading2">Wholesale Shops</h2>

                                <div id="ninja-slider" style="float:left;">
                                     <div class="slider-inner">
                                          <ul>
                                               <?php foreach ($suppliers as $supl) {?>
                                                    <li>
                                                         <a style="cursor: pointer" data-supname="<?php echo slugify($supl['supm_name_en'])?>"
                                                            data-toggle="tooltip" title="Go to Supplier Page"
                                                            data-id="<?php echo encryptor($supl['supm_id']);?>" class="ns-img" href="<?php echo $supl['default_image'];?>"></a>
                                                         <span class="s-title"><?php echo $supl['supm_name_en'];?></span>
                                                         <?php if ($supl['supm_panoramic_image']) {?>
                                                              <div class="hme-360">
                                                                   <a data-toggle="modal" id="_360_a" class="nav-link btn-360" href="<?php echo site_url() . 'supplier/get_360/' . encryptor($supl['supm_id']);?>" data-target="#Modal-360">
                                                                        <img src="images/360-sm.png" alt="view 360"></a>
                                                              </div>
                                                         <?php }?>
                                                    </li>
                                               <?php }?>

                                          </ul>
                                          <a href="<?php echo site_url('market/markets');?>"><div class="fs-icon" title="View All"></div></a>
                                     </div>
                                </div>

                                <div id="thumbnail-slider" style="float:left;">
                                     <div class="inner">
                                          <ul>
                                               <?php foreach ($suppliers as $supl) {?>
                                                    <li>
                                                         <a class="thumb" href="<?php echo $supl['default_image'];?>"  data-toggle="tooltip" title="<?php echo $supl['supm_name_en'];?>"></a>
                                                    </li>
                                               <?php }?>
                                          </ul>
                                     </div>
                                </div>
                                <div style="clear:both;"></div>


                                <!--shops section -->
                                <!-- <ul class="pgwSlider">
                                <?php foreach ($suppliers as $supl) {?>
                                                                                     <li>
                                                                                          <a target="_blank">
                                     <?php echo img(array('src' => $supl['default_image'], 'class' => 'img-fluid'));?>
                                                                                               <span><?php echo $supl['supm_name_en'];?></span>
                                                                                          </a>
                                                                                     </li>
                                <?php }?>      
                                </ul> -->
                           </div>
                      </div>
                 <?php }?>
               <div class="clearfix"></div>
          </div>
     </div>
</section>

<section class="hotsell padb-10">
     <div class="container">
          <div class="col-12">
               <?php if (!empty($hot_selling_product)) {?>             
                      <h2 class="heading2 padl-10">Hot Selling Products</h2>
                      <div class="prod-container">
                           <ul id="hot-prod" class="content-slider">
                                <!-- single product -->
                                <?php foreach ($hot_selling_product as $prod) {?>
                                     <li>
                                          <a href="<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>" style="text-decoration:none">
                                               <div class="prod-box">
                                                    <div class="prod-img">
                                                         <?php echo img(array('src' => $prod['default_image'], 'class' => 'img-fluid'));?>
                                                    </div>
                                                    <?php if ($prod['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?>
                                                    <div class="prod-detail txt-height">
                                                         <h4 class="prod-title-s"><?php echo get_snippet($prod['prd_name_en'], 3);?></h4>
                                                         <p>From <span class="price-text">US $<?php echo $prod['prd_price_min'];?></span> / Piece</p>
                                                         <p><?php echo $prod['prd_moq'];?> Pieces (MOQ)</p>
                                                         <!--<?php if ($prod['prd_is_stock_prod'] == 1) {?><p> Stock : <?php echo floatval($prod['prd_stock_qty']);?> </p> <?php }?>-->
                                                    </div>
                                                    <div class="favourite">
                                                         <a is-logged="<?php echo ($this->uid > 0) ? '1' : $this->calldrop;?>" href="javascript:;" class="btnFav" data-url="<?php echo site_url('favorites/addProductToFavoriteList/' . encryptor($prod['prd_id']));?>">
                                                              <i id="fav" class="far fa-heart <?php echo (isset($prod['isFavorite']) && !empty($prod['isFavorite'])) ? ' fas red-icn' : '';?>"></i></a>
                                                    </div>
                                               </div>
                                          </a>
                                     </li>
                                <?php }?>
                           </ul>
                      </div>
                 <?php }?>
          </div>


          <div class="clearfix"></div>
          <!-- sub section starts -->

          <div class="row sub-section">
               <?php if (!empty($stock_product)) {?>
                      <div class="col-lg-5 col-md-12">
                           <!-- stock prod -->
                           <div class="stock-prod-container">
                                <h2 class="heading2">Stock Products</h2>
                                <ul id="stock-prod" class="content-slider">
                                     <!--single product -->
                                     <?php foreach ($stock_product as $prod) {?>    
                                          <li>
                                               <div class="stock-pic">
                                                    <?php echo img(array('src' => $prod['default_image']));?>
                                                    <!-- <img src="images/product/phone-cover.jpg" alt=""> -->
                                               </div>
                                               <div class="stock-details txt-height">
                                                    <h4 class="prod-title-s"><?php echo get_snippet($prod['prd_name_en'], 3);?></h4>
                                                    <p>From <span class="price-text">US $<?php echo $prod['prd_price_min'];?></span> /<?php echo $prod['unt_unit_name_en'];?> </p>
                                                    <p>Stock <span class="price-text"><?php echo floatval($prod['prd_stock_qty']);?></span></p>
                                                    <a href="<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>"><button class="btn btn-success order-now">Order now</button></a>
                                               </div>
                                          </li>
                                     <?php }?>  
                                     <!-- /single product -->
                                </ul>
                           </div>
                           <!-- /stock prod -->
                      </div>
                 <?php }?>
               <div class="col-lg-7 col-md-12">
                    <div class="advert">
                         <ul id="advt" class="content-slider">
                              <?php foreach ((array) $add as $key => $value) {?>
                                     <li>
                                          <a target="_blank" href="<?php echo empty($value['add_url']) ? 'javascript:;' : $value['add_url'] . "?adds=" . encryptor($value['add_id']);?>">
                                               <?php echo img(array('src' => FILE_UPLOAD_PATH . '/advt/' . $value['add_image']));?>
                                          </a>
                                     </li>
                                <?php }?>
                         </ul>
                    </div>
               </div>
          </div>
     </div>
</section>
<?php 
  $lstProducts = $output = array_slice($latest_product, 0, 11);
  if (!empty($lstProducts)) {?>
       <section class="recommended">
            <div class="container">
                 <div class="col-12">
                      <h2 class="heading2 padl-10">Latest Products</h2>
                      <div class="prod-container">
                           <ul class="content-slider">
                                <?php foreach ($lstProducts as $prod) {?>
                                     <li>
                                          <a href="<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>" style="text-decoration:none">
                                               <div class="prod-box">
                                                    <div class="prod-img">
                                                         <?php echo img(array('src' => $prod['default_image'], 'class' => 'img-fluid'));?>
                                                    </div>
                                                    <?php if ($prod['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?>
                                                    <div class="prod-detail txt-height">
                                                         <h4 class="prod-title-s"><?php echo get_snippet($prod['prd_name_en'], 3);?></h4>
                                                         <p><span class="strike-price">US $<?php echo $prod['prd_price_min'];?></span> <span class="price-text">US $<?php echo $prod['prd_offer_price_min'];?></span></p>
                                                         <p class="moq"><?php echo $prod['prd_moq'];?> Pieces  (MOQ)</p>
                                                         <!--<?php if ($prod['prd_is_stock_prod'] == 1) {?><p> Stock : <?php echo floatval($prod['prd_stock_qty']);?> </p> <?php }?>-->
                                                    </div>
                                                    <div class="favourite">
                                                         <a is-logged="<?php echo ($this->uid > 0) ? '1' : $this->calldrop;?>" href="javascript:;" class="btnFav" data-url="<?php echo site_url('favorites/addProductToFavoriteList/' . encryptor($prod['prd_id']));?>">
                                                              <i id="fav" class="far fa-heart <?php echo (isset($prod['isFavorite']) && !empty($prod['isFavorite'])) ? ' fas red-icn' : '';?>"></i></a>
                                                    </div>
                                               </div>
                                          </a>
                                     </li>
                                <?php }?>     
                           </ul>
                      </div>
                 </div>
            </div>
       </section>
  <?php } if (!empty($offer_product)) {?> 
       <section class="hotsell pb-5 mt-10m">
            <div class="container">
                 <div class="col-12">
                      <h2 class="heading2 padl-10">Offers</h2>
                      <div class="prod-container">
                           <ul id="offer-products" class="content-slider">
                                <?php foreach ($offer_product as $prod) {?>
                                     <li>
                                          <a href="<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>" style="text-decoration:none">
                                               <div class="prod-box">
                                                    <div class="prod-img">
                                                         <?php echo img(array('src' => $prod['default_image'], 'class' => 'img-fluid'));?>
                                                    </div>
                                                    <?php if ($prod['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?>
                                                    <div class="prod-detail txt-height">
                                                         <h4 class="prod-title-s"><?php echo get_snippet($prod['prd_name_en'], 3);?></h4>
                                                         <p><span class="strike-price">US $<?php echo $prod['prd_price_min'];?></span> <span class="price-text">US $<?php echo $prod['prd_offer_price_min'];?></span></p>
                                                         <p class="moq"><?php echo $prod['prd_moq'];?> Pieces  (MOQ)</p>
                                                         <!--<?php if ($prod['prd_is_stock_prod'] == 1) {?><p> Stock : <?php echo floatval($prod['prd_stock_qty']);?> </p> <?php }?>-->
                                                    </div>
                                                    <div class="favourite">
                                                         <a is-logged="<?php echo ($this->uid > 0) ? '1' : $this->calldrop;?>" href="javascript:;" class="btnFav" data-url="<?php echo site_url('favorites/addProductToFavoriteList/' . encryptor($prod['prd_id']));?>">
                                                              <i id="fav" class="far fa-heart <?php echo (isset($prod['isFavorite']) && !empty($prod['isFavorite'])) ? ' fas red-icn' : '';?>"></i></a>
                                                    </div>
                                               </div>
                                          </a>
                                     </li>
                                <?php }?>     
                           </ul>
                      </div>
                 </div>
            </div>
       </section>
  <?php } if (!empty($recommended)) {?> 
       <section class="recommended tp-adjustment padb-20">
            <div class="container">
                 <div class="col-12">
                      <h2 class="heading2 padl-10">Recommended Products</h2>
                      <div class="prod-container">
                           <ul>
                                <?php foreach ($recommended as $prod) {?>
                                     <li>
                                          <a href="<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>">
                                               <div class="prod-box">
                                                    <div class="prod-img">
                                                         <?php echo img(array('src' => $prod['default_image'], 'class' => 'img-fluid'));?>
                                                    </div>
                                                    <?php if ($prod['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?>
                                                    <div class="prod-detail txt-height">
                                                         <h4 class="prod-title-s"><?php echo $prod['prd_name_en'];?></h4>
                                                         <p>From <span class="price-text">US $<?php echo $prod['prd_price_min'];?></span> / Piece</p>
                                                         <p class="moq"><?php echo $prod['prd_moq'];?> Piece  (MOQ)</p>
                                                         <!--<?php if ($prod['prd_is_stock_prod'] == 1) {?><p> Stock : <?php echo floatval($prod['prd_stock_qty']);?> </p> <?php }?>-->
                                                    </div>
                                                    <div class="favourite">
                                                         <a is-logged="<?php echo ($this->uid > 0) ? '1' : $this->calldrop;?>" href="javascript:;" class="btnFav" data-url="<?php echo site_url('favorites/addProductToFavoriteList/' . encryptor($prod['prd_id']));?>">
                                                              <i id="fav" class="far fa-heart <?php echo (isset($prod['isFavorite']) && !empty($prod['isFavorite'])) ? ' fas red-icn' : '';?>"></i></a>
                                                    </div>
                                               </div>
                                          </a>
                                     </li>
                                <?php }?>  
                           </ul>
                      </div>
                 </div>
            </div>
       </section>
  <?php } if (!empty($recent_product)) {?>
       <section class="recent padb-30">
            <div class="container">
                 <div class="col-12">

                      <h2 class="heading2 pad-10">Recently Viewed</h2>
                      <div class="recent-container">
                           <ul id="recent-prod" class="content-slider">
                                <?php foreach ($recent_product as $prod) {?>
                                     <li>
                                          <!-- product -->
                                          <a href="<?php echo site_url('product/product_details/' . encryptor($prod['prd_id']));?>">
                                               <div class="recent-box">
                                                    <?php echo img(array('src' => $prod['default_image'], 'class' => 'img-fluid'));?>
                                               </div>
                                          </a>
                                          <!-- /product -->
                                     </li>
                                <?php }?>     
                           </ul>
                      </div>
                 </div>
            </div>
       </section>
  <?php }?>

<style>
     .fs-icon {
          /* display:none !important; */
     }
</style>

<div id="Modal-360" class="modal fade modal-360" role="dialog">
     <div class="modal-dialog">

          <!--Filter Modal content-->
          <div class="modal-content">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <div class="modal-body 360_body">

               </div>

          </div>

     </div>
</div>
<!-- /360 modal -->
