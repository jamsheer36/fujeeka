<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class home extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->page_title = 'Home | ' . STATIC_TITLE;
            $this->load->model('add/add_model', 'add');
            $this->load->model('home_model', 'home');
            $this->load->model('manage_banner/manage_banner_model', 'manage_banner');
            $this->template->set_layout('home');
       }

       function index() {
            session_start();
            $data['recommended'] = '';
            if ($this->session->userdata('grp_slug') == 'BY' && isset($_SESSION['usr_recommendation'])) {
                 $data['recommended'] = json_decode($_SESSION['usr_recommendation'], true);
            }
            $data['latest_product'] = $this->home->getLatestProducts();
            $data['suppliers'] = $this->home->getSuppliers();
            $data['recent_product'] = $this->home->getRecentProducts();
            $data['hot_selling_product'] = $this->home->getHotSellingProducts(8);
            $data['offer_product'] = $this->home->getOfferProducts(12);
            $data['stock_product'] = $this->home->getStockProducts(10,'stock');
            $data['flash_stock'] = $this->home->getStockProducts(10,'flash_stock');
            $data['banner'] = $this->manage_banner->getSiteBanner('home');
            $data['add'] = $this->add->getHomePageAdd();
            $data['market'] = $this->home->getMarket();
            if(!$data['market']){
               $data['market']['mar_id']='';
            }
            $data['building'] = $this->home->getBuildings($data['market']['mar_id']);
            $this->render_page(strtolower(__CLASS__) . '/index', $data);
       }

       function search() {
            if (!isset($_GET['SearchText'])) {
                 $data['suggestions'] = $this->home->search($_GET);
                 $data['from'] = 'search';
                 echo json_encode($data);
                 exit;
            } else {
                 $data['categories'] = $this->common_model->getAllCategories();
                 $data['result'] = $this->home->productByKeyword($_GET);

                 $this->page_title = 'Home | Product Listing';
                 $this->template->set_layout('product');
                 $this->render_page(strtolower(__CLASS__) . '/search-result', $data);
            }
       }
       public function get_app(){
          $this->page_title = 'Fujeeka | Get App';
          // $this->template->set_layout('common');
          $this->render_page(strtolower(__CLASS__) . '/get_app');
       }

          public function productsearch() { 
            $count = $this->home->getProductsCount($key = $this->input->post('keyword'));
            $page = $this->input->post('page');

            $this->load->library("pagination");
            $config = array();
            $config["base_url"] = base_url() . "home/productsearch";
            $config["total_rows"] = $count;
            $config["per_page"] =  15;
            $config["uri_segment"] = 3;
            $config["use_page_numbers"] = TRUE;
            $config["full_tag_open"] = '<ul class="pagination">';
            $config["full_tag_close"] = '</ul>';
            $config["first_tag_open"] = '<li>';
            $config["first_tag_close"] = '</li>';
            $config["last_tag_open"] = '<li>';
            $config["last_tag_close"] = '</li>';
            $config['next_link'] = '&gt;';
            $config["next_tag_open"] = '<li>';
            $config["next_tag_close"] = '</li>';
            $config["prev_link"] = "&lt;";
            $config["prev_tag_open"] = "<li>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='active'><a>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li>";
            $config["num_tag_close"] = "</li>";
            $config["num_links"] = 5;
            $this->pagination->initialize($config);
            $start = ($page - 1) * $config["per_page"];
            $filter['pages']= $this->pagination->create_links();
            $filter['keyword']= $this->input->post('keyword');
            $output = array(
                'data' =>  $this->home->productsearch($config["per_page"], $start, $filter)
            );
            echo json_encode($output);
            die();
          }
  } 