<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class country extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->page_title = 'Country';
            $this->load->model('country_model', 'country');
            $this->lock_in();
       }

       public function index() {
            $data['countries'] = $this->country->get();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       public function add() {
            if (!empty($_POST)) {
                 if ($this->country->newCountry($_POST)) {
                      $this->session->set_flashdata('app_success', 'Row successfully updated!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't updated row!");
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $this->render_page(strtolower(__CLASS__) . '/add');
            }
       }

       public function view($id) {
            $data['country'] = $this->country->get($id);
            $this->render_page(strtolower(__CLASS__) . '/view', $data);
       }

       public function update() {
            if ($this->country->updateCountry($this->input->post())) {
                 $this->session->set_flashdata('app_success', 'Successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', "Can't update data!");
            }
            redirect(strtolower(__CLASS__));
       }
       
       function deleteCountry($id = '') {
            if ($this->country->deleteCountry($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Row successfully deleted'));die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete row"));die();
            }
       }

  }
  