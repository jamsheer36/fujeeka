<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class country_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->tbl_countries = TABLE_PREFIX . 'countries';
            $this->tbl_market_places = TABLE_PREFIX . 'market_places';
       }

       function get($id = '') {
            if (!empty($id)) {
                 return $this->db->get_where($this->tbl_countries, array('ctr_id' => $id))->row_array();
            }
            return $this->db->get($this->tbl_countries)->result_array();
       }

       function newCountry($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_countries, $data);
                 return true;
            } else {
                 return false;
            }
       }

       function updateCountry($data) {
            if ($data) {
                 $id = isset($data['ctr_id']) ? $data['ctr_id'] : '';
                 unset($data['ctr_id']);
                 $this->db->where('ctr_id', $id);
                 $this->db->update($this->tbl_countries, $data);
                 return true;
            } else {
                 return FALSE;
            }
       }

       function deleteCountry($id) {
            if (!empty($id)) {
                 $this->db->where('ctr_id', $id);
                 $this->db->delete($this->tbl_countries);
                 return true;
            } else {
                 return false;
            }
       }

       function getCountryAssocMarket() {
            return $this->db->select($this->tbl_market_places . '.*,' . $this->tbl_countries . '.*')
                    ->join($this->tbl_countries, $this->tbl_countries . '.ctr_id = ' . $this->tbl_market_places . '.mar_country_id', 'LEFT')
               ->get($this->tbl_market_places)->result_array();
       }

  }
  