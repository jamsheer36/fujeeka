<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>New Country</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br />
                         <?php echo form_open_multipart("country/update", array('id' => "frmNewsEvents", 'class' => "form-horizontal",'data-parsley-validate'=>"true"))?>
                         <input type="hidden" name="ctr_id" id="ctr_id" class="form-control col-md-7 col-xs-12" value="<?php echo $country['ctr_id'];?>"/>
                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required type="text" name="ctr_name" id="ctr_name" class="form-control col-md-7 col-xs-12"  
                                          value="<?php echo $country['ctr_name'];?>"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Country Code</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required type="text" name="ctr_country_code" id="country" class="form-control col-md-7 col-xs-12" 
                                          value="<?php echo $country['ctr_country_code'];?>"/>
                              </div>
                         </div>

                         <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Phone Code</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                   <input required type="text" name="ctr_phone_code" id="ctr_phone_code" class="form-control col-md-7 col-xs-12"  
                                          value="<?php echo $country['ctr_phone_code'];?>"/>
                              </div>
                         </div>

                         <div class="ln_solid"></div>
                         <div class="form-group">
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                   <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success">Submit</button>' : '';?>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                              </div>
                         </div>
                         <?php echo form_close()?>
                    </div>
               </div>
          </div>
     </div>
</div>