<?php

  if (!defined('BASEPATH')) {
       exit('No direct script access allowed');
  }

  class feed_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->feed = TABLE_PREFIX . 'feeds';
            $this->product = TABLE_PREFIX . 'products';
            $this->followers = TABLE_PREFIX . 'supplier_followers';
            $this->users = TABLE_PREFIX . 'users';
            $this->products_images = TABLE_PREFIX . 'products_images';
       }

       public function getProducts() {
            return $this->db->select('prd_id,prd_name_en')
                            ->where('prd_supplier', $this->suplr)
                            ->where('prd_status', 1)
                            ->order_by('prd_name_en', 'asc')
                            ->get($this->product)
                            ->result_array();
       }

       public function insertFeed($data) {
            
            //Send push notification 
            $myFavFollo = $this->common_model->getSuppliersFollowersAndFavs();
            if (!empty($myFavFollo)) {
                 $tockens = array_column($myFavFollo, 'usr_pushy_token');
                 $tockens = array_values(array_filter($tockens));

                 if (isset($toUser['usr_pushy_token']) && !empty($toUser['usr_pushy_token'])) {
                      $push['id'] = $data['f_prd_id'];
                      $push['msg_type'] = 'feed_notfication';
                      $push['from'] = 'Fujeeka';
                      $push['title'] = $data['f_text'];
                      $push['time'] = date("Y-m-d H:i:s");
                      $this->common_model->sendPushNotification($push, $tockens, '');
                 }
            }

            $data['f_sup_id'] = $this->suplr;
            $this->db->insert($this->feed, $data);
            return $this->db->insert_id();
       }

       public function getFeeds() {
            return $this->db->select($this->feed . '.*,' . $this->product . '.prd_name_en')
                            ->join($this->product, 'prd_id=f_prd_id')
                            ->where('f_sup_id', $this->suplr)
                            ->where('f_status', 1)
                            ->order_by('f_added_on', 'desc')
                            ->get($this->feed)
                            ->result_array();
       }

       public function delete($id) {
            return $this->db->set('f_status', 0)->where('f_id', $id)->update($this->feed);
       }

       public function getEditDetails($id) {
            return $this->db->where('f_id', $id)
                            ->get($this->feed)
                            ->row_array();
       }

       public function updateFeed($data, $id) {
            return $this->db->where('f_id', $id)->update($this->feed, $data);
       }

       public function getFollowers($suplr) {
            $foll = $this->db->select('usr_pushy_token')
                            ->join($this->users, 'usr_id = sfol_followed_by')
                            ->where('sfol_supplier', $suplr)
                            ->where('usr_pushy_token !=', '')
                            ->get($this->followers)->result_array();
            return array_column($foll, 'usr_pushy_token');
       }

       public function getProductImage($prd_id) {
            $query = $this->db->select('prd_default_image')
                            ->where('prd_id', $prd_id)
                            ->get($this->product);
              $def_img =isset($query->row()->prd_default_image)?$query->row()->prd_default_image:'';
            if ($def_img) {
                 $images = $this->db->get_where($this->products_images, array('pimg_product' => $prd_id, 'pimg_id' => $def_img))->row_array();
            } else {
                 $images = $this->db->limit(1)->get_where($this->products_images, array('pimg_product' => $prd_id))->row_array();
            }
            $defaultImg = isset($images['pimg_image']) ? $images['pimg_image'] : '';
            $defaultImg = site_url() . 'assets/uploads/product/' . $defaultImg;
            return $defaultImg;
       }

  }
  