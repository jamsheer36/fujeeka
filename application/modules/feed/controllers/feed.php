<?php

defined('BASEPATH') or exit('No direct script access allowed');

class feed extends App_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->page_title = 'Feed';
        $this->load->model('feed_model', 'feed');
        $this->lock_in();
    }

    public function index()
    {

        $data['feeds'] = $this->feed->getFeeds();
        $this->render_page(strtolower(__CLASS__) . '/list', $data);
    }
    public function add_feed()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('f_prd_id', 'Product', 'required|trim|numeric');
            $this->form_validation->set_rules('f_text', 'Text', 'required|trim');
            if ($this->form_validation->run() == false) {
                $error = $this->form_validation->error_array();
                $data['error'] = $error;
            } else {
                if ($feed_id = $this->feed->insertFeed($this->input->post())) {
                    $this->sendPushy($feed_id, $this->input->post('f_text'), $this->input->post('f_prd_id'));
                    $this->session->set_flashdata('app_success', 'Feed successfully added!');
                } else {
                    $this->session->set_flashdata('app_error', 'Failed to add');
                }
                redirect(strtolower(__CLASS__) . '/add-feed');
            }
        }
        $data['products'] = $this->feed->getProducts();
        $this->render_page(strtolower(__CLASS__) . '/add', $data);
    }
    public function delete($id)
    {
        if ($this->feed->delete(encryptor($id, 'D'))) {
            echo json_encode(array('status' => 'success', 'msg' => 'Row successfully deleted'));die();
        } else {
            echo json_encode(array('status' => 'fail', 'msg' => 'Failed to delete'));die();
        }

    }
    public function update($id = '')
    {

        if ($this->input->post()) {
            $this->form_validation->set_rules('f_prd_id', 'Product', 'required|trim|numeric');
            $this->form_validation->set_rules('f_text', 'Text', 'required|trim');
            if ($this->form_validation->run() == false) {
                $error = $this->form_validation->error_array();
                $data['error'] = $error;
            } else {
                $data = $this->input->post();
                if ($this->feed->updateFeed($data, encryptor($id, 'D'))) {
                    $this->session->set_flashdata('app_success', 'Feed successfully updated!');
                    redirect(strtolower(__CLASS__) . '/index');
                } else {
                    $this->session->set_flashdata('app_error', 'Failed to update');
                    redirect(strtolower(__CLASS__) . '/update/' . $id);
                }
            }
        }
        $data['feed'] = $this->feed->getEditDetails(encryptor($id, 'D'));
        $data['products'] = $this->feed->getProducts();
        $this->render_page(strtolower(__CLASS__) . '/update', $data);
    }
    public function sendPushy($feed_id, $f_text, $f_prd_id)
    {
        $followers = $this->feed->getFollowers($this->suplr);
        if ($followers) {
            $push['type'] = 'notification';
            $push['msg_type'] = 'feed';
            $push['content'] = $f_text;
            $push['image'] = $this->feed->getProductImage($f_prd_id);
            $push['from_name'] = $this->session->userdata('usr_username');
            $push['time'] = date("Y-m-d H:i:s");
            $this->common_model->sendPushNotification($push, $followers, '');
        }
        return 1;
    }
}
