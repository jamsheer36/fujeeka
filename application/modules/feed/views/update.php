<div class="right_col" role="main">
     <div class="clearfix"></div>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Add Feed</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <br/>
                         <form id="demo-form2" method="post"  data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <div class="form-group">
                                   <label for="usr_group" class="control-label col-md-3 col-sm-3 col-xs-12">Product<span class="required">*</span></label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="select2_group form-control" data-parsley-required-message="Select a product" name="f_prd_id" required="true">
                                             <option value="">Select Product</option>
                                             <?php foreach ($products as $key => $value) {?>
                                             <option <?php if ($feed['f_prd_id'] == $value['prd_id']) {?> selected <?php }?>value="<?php echo $value['prd_id']; ?>"><?php echo $value['prd_name_en']; ?></option>
                                             <?php }?>
                                        </select>
                                        <span id='error' class="text-danger"><?php if (isset($error['f_prd_id'])) {echo $error['f_prd_id'];}?></span>
                                   </div>
                              </div>
                              <div class="form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="f_text">Feed Text<span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="f_text" name="f_text" required="required" data-parsley-required-message="Text required" maxlength="200"
                                               class="pastContent form-control col-md-7 col-xs-12"><?=$feed['f_text']?></textarea>
                                               <span id='error' class="text-danger"><?php if (isset($error['f_text'])) {echo $error['f_text'];}?></span>
                                               <span> <span id="rem_chr"><?php echo (200 - strlen($feed['f_text'])); ?></span> Characters left</span>
                                   </div>

                              </div>

                              <div class="ln_solid"></div>
                              <div class="form-group">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                   </div>
                              </div>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>
