<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class units extends App_Controller {

       public function __construct() {

            parent::__construct();
            $this->body_class[] = 'skin-blue';
            $this->page_title = 'Units';
            $this->load->model('units_model', 'units');
            $this->lock_in();
       }

       public function index() {
            $data['units'] = $this->units->get();
            $this->render_page(strtolower(__CLASS__) . '/list', $data);
       }

       public function add() {
            if (!empty($_POST)) {
                 if ($this->units->newUnits($_POST)) {
                      $this->session->set_flashdata('app_success', 'Row successfully updated!');
                 } else {
                      $this->session->set_flashdata('app_error', "Can't updated row!");
                 }
                 redirect(strtolower(__CLASS__));
            } else {
                 $this->render_page(strtolower(__CLASS__) . '/add');
            }
       }

       public function view($id) {
            $data['unit'] = $this->units->get($id);
            $this->render_page(strtolower(__CLASS__) . '/view', $data);
       }

       public function update() {
            if ($this->units->updateUnits($this->input->post())) {
                 $this->session->set_flashdata('app_success', 'Successfully updated!');
            } else {
                 $this->session->set_flashdata('app_error', "Can't update data!");
            }
            redirect(strtolower(__CLASS__));
       }
       
       function delete($id = '') {
            if ($this->units->deleteUnits($id)) {
                 die(json_encode(array('status' => 'success', 'msg' => 'Row successfully deleted')));
            } else {
                 die(json_encode(array('status' => 'fail', 'msg' => "Can't delete row")));
            }
       }

  }
  