<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class units_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();
            $this->tbl_products_units = TABLE_PREFIX . 'products_units';
       }

       function get($id = '') {
            if (!empty($id)) {
                 return $this->db->get_where($this->tbl_products_units, array('unt_id' => $id))->row_array();
            }
            return $this->db->get($this->tbl_products_units)->result_array();
       }

       function newUnits($data) {
            if (!empty($data)) {
                 $this->db->insert($this->tbl_products_units, $data);
                 return true;
            } else {
                 return false;
            }
       }

       function updateUnits($data) {
            if ($data) {
                 $id = isset($data['unt_id']) ? $data['unt_id'] : '';
                 unset($data['unt_id']);
                 $this->db->where('unt_id', $id);
                 $this->db->update($this->tbl_products_units, $data);
                 return true;
            } else {
                 return FALSE;
            }
       }

       function deleteUnits($id) {
            if (!empty($id)) {
                 $this->db->where('unt_id', $id);
                 $this->db->delete($this->tbl_products_units);
                 return true;
            } else {
                 return false;
            }
       }

  }
  