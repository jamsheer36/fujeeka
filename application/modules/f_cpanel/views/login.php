<div class="materialContainer">
     <div class="box">
          <?php echo form_open('f_cpanel/login', array('class' => 'frmLogin'))?>
          <div class="title">ADMIN LOGIN</div>
          <div class="input">
               <!-- <label for="identity">Username</label> -->
               <?php echo form_input($identity) ?>
               <span class="spin"></span>
          </div>
          <div class="input">
               <!-- <label for="password">Password</label> -->
               <?php echo form_input($password) ?>
               <span class="spin"></span>
          </div>
          <div class="button login">
               <button type="submit" class="submitMe"><span>GO</span><i class="fa fa-check"></i></button>
          </div>
          <?php echo form_close()?> 
          <a href="javascript:;" class="login-error"><?php echo $this->session->flashdata('app_error'); ?></a>
     </div>
</div>