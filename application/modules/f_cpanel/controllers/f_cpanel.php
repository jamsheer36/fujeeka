<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class f_cpanel extends App_Controller {

       public function __construct() {
            parent::__construct();
            $this->load->model('ion_auth_model');
       }

       public function index() {

            if ($this->ion_auth->logged_in()) {
                 if ($this->usr_grp == 'SA' || $this->usr_grp == 'ST')
                      redirect('dashboard');
                 else
                      redirect('home');
            } else {
                 redirect(strtolower(__CLASS__) . '/login');
            }
       }

       public function login() {
            $this->template->set_layout('admin_login');
            if ($this->ion_auth->logged_in()) {
                 redirect('dashboard');
            }
            $this->page_title = 'Login';

            $this->current_section = 'login';

            // validate form input
            $this->form_validation->set_rules('identity', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == true) {
                 // check to see if the user is logging in
                 // check for "remember me"
                 $remember = (bool) $this->input->post('remember');

                 if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                      $this->session->set_flashdata('app_success', $this->ion_auth->messages());
                      $callback = $this->session->userdata('callback'); // Set callback url from any where, redirect after login 
                      if (!empty($callback)) {
                        $this->session->set_flashdata('from_login', true);
                        redirect($callback);
                      }
                      redirect('dashboard');
                 } else {
                      $this->session->set_flashdata('app_error', $this->ion_auth->errors());
                      redirect(__CLASS__ . '/login');
                 }
            } else {
                 // the user is not logging in so display the login page
                 // set the flash data error message if there is one
                 $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                 $data['identity'] = array('name' => 'identity',
                     'id' => 'identity',
                     'type' => 'text',
                     'value' => $this->form_validation->set_value('identity'),
                     'class' => 'form-control',
                     'placeholder' => 'Username'
                 );
                 $data['password'] = array('name' => 'password',
                     'id' => 'password',
                     'type' => 'password',
                     'class' => 'form-control',
                     'placeholder' => 'Password'
                 );

                 $this->render_page(__CLASS__ . '/login', $data);
            }
       }

       public function logout() {
            // log the user out
            $logout = $this->ion_auth->logout();
            // redirect them back to the login page
            redirect('home');
       }

       public function forgot_password() {
            if ($this->form_validation->run('user_forgot_password')) {
                 $forgotten = $this->ion_auth->forgotten_password($this->input->post('email', TRUE));

                 if ($forgotten) {
                      // if there were no errors
                      $this->session->set_flashdata('app_success', $this->ion_auth->messages());
                      redirect('login');
                 } else {
                      $this->session->set_flashdata('app_error', $this->ion_auth->errors());
                      redirect('login');
                 }
            }

            $this->body_class[] = 'forgot_password';
            $this->page_title = 'Forgot password';
            $this->current_section = 'forgot_password';
            $this->render_page('user/forgot_password');
       }

       public function account() {
            $this->body_class[] = 'my_account';
            $this->page_title = 'My Account';
            $this->current_section = 'my_account';
            $user = $this->ion_auth->user()->row_array();
            $this->render_page('user/account', array('user' => $user));
       }

       function change_password() {

            $this->section = 'Change Password';
            $this->body_class[] = 'skin-blue';
            $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
            $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

            if (!$this->ion_auth->logged_in()) {
                 redirect('user/login', 'refresh');
            }

            $user = $this->ion_auth->user()->row();

            if ($this->form_validation->run() == false) {
                 //set the flash data error message if there is one
                 $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                 $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                 $uname = isset($this->ion_auth->user()->row()->username) ? $this->ion_auth->user()->row()->username : '';
                 $this->data['username'] = array(
                     'name' => 'uname',
                     'id' => 'uname',
                     'type' => 'test',
                     'class' => 'input-xxlarge',
                     'value' => $uname
                 );
                 $this->data['old_password'] = array(
                     'name' => 'old',
                     'id' => 'old',
                     'type' => 'password',
                     'class' => 'input-xxlarge'
                 );
                 $this->data['new_password'] = array(
                     'name' => 'new',
                     'id' => 'new',
                     'type' => 'password',
                     'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                     'class' => 'input-xxlarge'
                 );
                 $this->data['new_password_confirm'] = array(
                     'name' => 'new_confirm',
                     'id' => 'new_confirm',
                     'type' => 'password',
                     'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                     'class' => 'input-xxlarge'
                 );
                 $this->data['user_id'] = array(
                     'name' => 'user_id',
                     'id' => 'user_id',
                     'type' => 'hidden',
                     'value' => $user->id,
                 );

                 //render
                 $this->render_page('user/change_password', $this->data);
                 //$this->render_page('user/login', $data);
            } else {

                 $identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

                 $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'), $this->input->post('uname'));

                 if ($change) {
                      //if the password was successfully changed
                      $this->session->set_flashdata('app_success', $this->ion_auth->messages());
                      $this->logout();
                 } else {
                      $this->session->set_flashdata('app_error', $this->ion_auth->errors());
                      redirect('user/change_password', 'refresh');
                 }
            }
       }

       function add() {
            $this->section = 'New User';
            $this->render_page('user/add');
       }

       function signupFirstStep() {
            if ((isset($_POST['usr_email']) && !empty($_POST['usr_email'])) &&
                    ((isset($_POST['captcha_code']) && isset($_POST['usr_captcha'])) && $_POST['captcha_code'] == $_POST['usr_captcha'])) {
                 $tomail = $_POST['usr_email'];

                 $id = $this->ion_auth->register('', '', $tomail, array(), array(4));

                 /* Sent activation email */
                 $this->mail->isSMTP();
                 $this->mail->Host = MAIL_HOST;
                 $this->mail->SMTPAuth = true;
                 $this->mail->Username = MAIL_USERNAME;
                 $this->mail->Password = MAIL_PASSWORD;
                 $this->mail->SMTPSecure = 'ssl';
                 $this->mail->Port = 465;
                 $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                 $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                 $this->mail->addAddress($tomail);
                 $this->mail->Subject = 'Hi ' . $tomail . ', please verify your fujeeka account';
                 $this->mail->isHTML(true);
                 $mailContent = "Hi, <br><br> Thanks for using fujeeka! Please confirm your email address by clicking on the link below. "
                         . "We'll communicate with you from time to time via email so it's important that we have an up-to-date email address on file."
                         . "<br><br> " . site_url() . "user/activate/" . encryptor($id);
                 $this->mail->Body = $mailContent;
                 $this->mail->send();
                 /* Sent activation email */
                 echo json_encode(array('status' => 'success', 'msg' => site_url() . "user/activate/" . encryptor($id)));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Security code miss match or email already exists!"));
                 die();
            }
       }

       function insert() {
            if (isset($_FILES['avatar']['name']) && !empty($_FILES['avatar']['name'])) {
                 $this->load->library('upload');
                 $newFileName = rand(9999999, 0) . $_FILES['avatar']['name'];
                 $config['upload_path'] = '../assets/uploads/avatar/';
                 $config['allowed_types'] = 'gif|jpg|png';
                 $config['file_name'] = $newFileName;
                 $this->upload->initialize($config);

                 $angle['x1']['0'] = $_POST['x1'];
                 $angle['x2']['0'] = $_POST['x2'];
                 $angle['y1']['0'] = $_POST['y1'];
                 $angle['y2']['0'] = $_POST['y2'];
                 $angle['w']['0'] = $_POST['w'];
                 $angle['h']['0'] = $_POST['h'];

                 if ($this->upload->do_upload('avatar')) {
                      $data = $this->upload->data();
                      crop($this->upload->data(), $angle);
                      $_POST['user']['avatar'] = $data['file_name'];
                 }
            }
            $username = $_POST['user']['username'];
            $password = trim($_POST['user']['password']);
            $email = $_POST['user']['email'];

            unset($_POST['user']['password_confirm']);
            unset($_POST['user']['username']);
            unset($_POST['user']['password']);
            unset($_POST['user']['email']);

            $this->ion_auth->register($username, $password, $email, $_POST['user']);
            $this->session->set_flashdata('app_success', 'User successfully created!');
            redirect(strtolower(__CLASS__) . '/listuser');
       }

       function activate($id) {
            $id = encryptor($id, 'D');

            $data['country'] = get_country_list();
            $user = $this->ion_auth->user($id)->row_array();
            if (!empty($user)) {

                 generate_log(array(
                     'log_title' => 'Email verified',
                     'log_desc' => 'The user verified through email',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $id,
                     'log_added_by' => $id
                 ));

                 $data['user'] = $user;
                 $this->template->set_layout('signup');
                 $this->render_page(strtolower(__CLASS__) . '/activate', $data);
            } else {
                 $this->template->set_layout('signup');
                 $this->render_page(strtolower(__CLASS__) . '/nop');
            }
       }

       function update() {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('usr_id', 'Who', 'required');
            $this->form_validation->set_rules('usr_password', 'Password', 'required');
            $this->form_validation->set_rules('usr_cpassword', 'Password Confirmation', 'required|matches[usr_password]');
            $this->form_validation->set_rules('usr_country', 'Country', 'required');
            $this->form_validation->set_rules('usr_state', 'State', 'required');
            $this->form_validation->set_rules('usr_first_name', 'First name', 'required');

            if ($this->form_validation->run() == FALSE) {
                 echo json_encode(array('status' => 'fail', 'msg' => validation_errors()));
                 die();
            } else {
                 $_POST['usr_id'] = encryptor($_POST['usr_id'], 'D');
                 if (isset($_FILES['avatar']['name']) && !empty($_FILES['avatar']['name'])) {

                      $this->load->library('upload');
                      $newFileName = rand(9999999, 0) . $_FILES['avatar']['name'];
                      $config['upload_path'] = '../assets/uploads/avatar/';
                      $config['allowed_types'] = 'gif|jpg|png';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);

                      $angle['x1']['0'] = $_POST['x1'];
                      $angle['x2']['0'] = $_POST['x2'];
                      $angle['y1']['0'] = $_POST['y1'];
                      $angle['y2']['0'] = $_POST['y2'];
                      $angle['w']['0'] = $_POST['w'];
                      $angle['h']['0'] = $_POST['h'];

                      if ($this->upload->do_upload('avatar')) {
                           $data = $this->upload->data();
                           crop($this->upload->data(), $angle);
                           $_POST['avatar'] = $data['file_name'];
                      }
                 }
                 $userId = $_POST['usr_id'];
                 $_POST['usr_phone'] = $_POST['ccode'] . ' ' . $_POST['usr_phone'];
                 $userid = $_POST['usr_id'];

                 unset($_POST['usr_id']);
                 unset($_POST['usr_cpassword']);
                 unset($_POST['ccode']);
                 $_POST['usr_active'] = 1;
                 $this->ion_auth->update($userId, $_POST);
                 if (!$this->session->userdata('user_id')) {
                      $this->ion_auth->login($_POST['reg_email'], $_POST['usr_password']);
                 }
                 generate_log(array(
                     'log_title' => 'User active',
                     'log_desc' => 'The user activated the account',
                     'log_controller' => strtolower(__CLASS__),
                     'log_action' => 'U',
                     'log_ref_id' => $userid,
                     'log_added_by' => $userid
                 ));

                 echo json_encode(array('status' => 'success', 'msg' => ""));
                 die();
            }
       }

       function checkEmailExists() {

            $email = isset($_POST['email']) ? $_POST['email'] : '';
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            echo ($this->ion_auth->email_check($email, $id)) ? 'false' : 'true';
            die();
       }

       function checkUsernameExists() {
            $username = isset($_POST['user']['username']) ? $_POST['user']['username'] : '';
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            echo ($this->ion_auth->username_check($username, $id)) ? 'false' : 'true';
            die();
       }

       function delete($id) {
            if ($this->ion_auth->delete_user($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'User successfully deleted'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete user"));
                 die();
            }
       }

       function view_user($id) {
            $this->section = "Edit user";
            $this->data['userDetails'] = $this->ion_auth->user($id)->row_array();
            $this->render_page(strtolower(__CLASS__) . '/edit', $this->data);
       }

       function removeImage($id) {
            if ($this->ion_auth->removeAvatar($id)) {
                 echo json_encode(array('status' => 'success', 'msg' => 'Avatar successfully deleted'));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Can't delete brand avatar"));
                 die();
            }
       }

       public function signup() {

            $this->template->set_layout('signup');
            $this->load->helper('captcha');

            $config = array(
                'img_path' => './assets/uploads/captcha/',
                'img_url' => site_url() . '/assets/uploads/captcha/',
                'font_path' => './system/fonts/texb.ttf',
                'img_width' => '150',
                'img_height' => 30,
                'expiration' => 7200,
                'word_length' => 4,
                'font_size' => 16,
                'img_id' => 'Imageid',
                'img_class' => 'mx-auto d-block',
                'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                // White background and border, black text and red grid
                'colors' => array(
                    'background' => array(255, 255, 255),
                    'border' => array(255, 255, 255),
                    'text' => array(0, 0, 0),
                    'grid' => array(255, 255, 255)
                )
            );

            $cap = create_captcha($config);
            $data['captcha'] = $cap;

            if (!empty($_POST)) {
                 $_POST['usr_group'] = 4;
                 if ($this->user_model->register($_POST)) {
                      $this->session->set_flashdata('app_success', 'Successfully Registered!');
                      redirect(strtolower(__CLASS__));
                 } else {
                      $this->session->set_flashdata('app_error', 'Failed to register');
                      $this->render_page(strtolower(__CLASS__) . '/signup', $data);
                 }
            } else {
                 $this->render_page(strtolower(__CLASS__) . '/signup', $data);
            }
       }

       public function insertTestValues() {
            // $this->common_model->insertSupplier(100);
            // $this->common_model->insertProducts(100);
       }

       function checkIsAdmin() {
            if (isset($_POST['usr_email']) && !empty($_POST['usr_email'])) {
                 $tomail = $_POST['usr_email'];

                 $id = $this->ion_auth->register('', '', $tomail, array(), array(4));

                 /* Sent activation email */
                 $this->mail->isSMTP();
                 $this->mail->Host = MAIL_HOST;
                 $this->mail->SMTPAuth = true;
                 $this->mail->Username = MAIL_USERNAME;
                 $this->mail->Password = MAIL_PASSWORD;
                 $this->mail->SMTPSecure = 'ssl';
                 $this->mail->Port = 465;
                 $this->mail->setFrom(FROM_MAIL, FROM_NAME);
                 $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
                 $this->mail->addAddress($tomail);
                 $this->mail->Subject = 'Hi ' . $tomail . ', please verify your fujeeka account';
                 $this->mail->isHTML(true);
                 $mailContent = "Hi, <br><br> Thanks for using fujeeka! Please confirm your email address by clicking on the link below. "
                         . "We'll communicate with you from time to time via email so it's important that we have an up-to-date email address on file."
                         . "<br><br> " . site_url() . "user/activate/" . encryptor($id);
                 $this->mail->Body = $mailContent;
                 $this->mail->send();
                 /* Sent activation email */
                 echo json_encode(array('status' => 'success', 'msg' => site_url() . "user/activate/" . encryptor($id)));
                 die();
            } else {
                 echo json_encode(array('status' => 'fail', 'msg' => "Security code miss match or email already exists!"));
                 die();
            }
       }

       public function activate_supplier($id = '') {
            $supplierID = isset($_POST['supm_id']) ? $_POST['supm_id'] : 0;
            $this->load->library('upload');
            if (!empty($_POST)) {

                 if (isset($_FILES['userAvatar']['name']) && !empty($_FILES['userAvatar']['name'])) {
                      $pixel = array();
                      $uploadData = array();

                      $newFileName = rand(9999999, 0) . clean_image_name($_FILES['userAvatar']['name']);
                      $config['upload_path'] = FILE_UPLOAD_PATH . 'avatar/';
                      $config['allowed_types'] = 'gif|jpg|png|jpeg';
                      $config['file_name'] = $newFileName;
                      $this->upload->initialize($config);

                      $pixel['x1'][0] = $_POST['x1'][0];
                      $pixel['y1'][0] = $_POST['y1'][0];
                      $pixel['x2'][0] = $_POST['x2'][0];
                      $pixel['y2'][0] = $_POST['y2'][0];
                      $pixel['w'][0] = $_POST['w'][0];
                      $pixel['h'][0] = $_POST['h'][0];

                      if ($this->upload->do_upload('userAvatar')) {
                           $uploadData = $this->upload->data();
                           crop($uploadData, $pixel);
                           $_POST['user']['usr_avatar'] = $uploadData['file_name'];
                      }
                 }

                 $x12 = $this->input->post('x12');
                 $fileCount = count($x12);
                 for ($j = 0; $j < $fileCount; $j++) {
                      if (isset($_FILES['shopImages']['name'][$j]) && !empty($_FILES['shopImages']['name'][$j])) {
                           $uploadData = array();
                           $pixel = array();
                           $newFileName = rand(9999999, 0) . $_FILES['shopImages']['name'][$j];
                           $config['upload_path'] = FILE_UPLOAD_PATH . 'shops/';
                           $config['allowed_types'] = 'gif|jpg|png|jpeg';
                           $config['file_name'] = $newFileName;
                           $this->upload->initialize($config);

                           $pixel['x1'][0] = $_POST['x12'][$j];
                           $pixel['y1'][0] = $_POST['y12'][$j];
                           $pixel['x2'][0] = $_POST['x22'][$j];
                           $pixel['y2'][0] = $_POST['y22'][$j];
                           $pixel['w'][0] = $_POST['w2'][$j];
                           $pixel['h'][0] = $_POST['h2'][$j];

                           $_FILES['prd_image_tmp']['name'] = $_FILES['shopImages']['name'][$j];
                           $_FILES['prd_image_tmp']['type'] = $_FILES['shopImages']['type'][$j];
                           $_FILES['prd_image_tmp']['tmp_name'] = $_FILES['shopImages']['tmp_name'][$j];
                           $_FILES['prd_image_tmp']['error'] = $_FILES['shopImages']['error'][$j];
                           $_FILES['prd_image_tmp']['size'] = $_FILES['shopImages']['size'][$j];
                           if (!$this->upload->do_upload('prd_image_tmp')) {
                                $this->upload->display_errors();
                           } else {
                                $uploadData = $this->upload->data();
                                crop($uploadData, $pixel);
                                $this->supplier->addImages(array('ssi_supplier' => $supplierID, 'ssi_image' => $uploadData['file_name']));
                           }
                      }
                 }

                 if (isset($_POST['supm_id']) && !empty($_POST['supm_id'])) { // Update if exists already.
                      if (!empty($supplierID)) {
                           $this->supplier->updateSupplier($_POST);
                           $_POST['user']['usr_group'] = 6;
                           $_POST['user']['usr_supplier'] = $supplierID;
                           $this->emp_details->update($_POST['user']);
                           $this->session->set_flashdata('app_success', 'Row successfully added!');
                           redirect(strtolower(__CLASS__) . '/activate-supplier/' . encryptor($supplierID));
                      }
                 } else {// Add as new row if not exists already.
                      if ($supplierID = $this->supplier->newSupplier($_POST)) {
                           $_POST['user']['usr_group'] = array(4, 2);
                           $_POST['user']['usr_supplier'] = $supplierID;
                           if ($this->emp_details->update($_POST['user'])) {
                                $this->session->set_flashdata('app_success', 'Row successfully added!');
                           } else {
                                $this->session->set_flashdata('app_error', 'Row successfully added!');
                           }
                           redirect(strtolower(__CLASS__) . '/activate-supplier/' . encryptor($supplierID));
                      }
                 }
            } else {

                 if (!empty($id)) {
                      $id = encryptor($id, 'D');
                      $data['suppliers'] = $this->supplier->suppliers($id);
                      $countryId = isset($data['suppliers'] ['supm_country']) ? $data['suppliers']['supm_country'] : 0;
                      $data['states'] = get_state_province('', $countryId);
                      $data['country'] = get_country_list();
                      $data['grades'] = $this->supplier_grade->get();
                      $data['buildings'] = $this->building->getBuilding();
                      $data['marketPlaces'] = $this->market->gerMarketPlaces();
                      $data['buildingsCategories'] = $this->supplier->getBuildingsCategories($data['suppliers']['supm_building']);
                 } else {
                      $data['buildings'] = $this->building->getBuilding();
                      $data['marketPlaces'] = $this->market->gerMarketPlaces();
                      $data['suppliers'] = get_logged_user();
                 }
                 $this->render_page(strtolower(__CLASS__) . '/activate-supplier', $data);
            }
       }

  }
  