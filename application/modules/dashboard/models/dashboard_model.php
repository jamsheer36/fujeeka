<?php

  if (!defined('BASEPATH'))
       exit('No direct script access allowed');

  class dashboard_model extends CI_Model {

       public function __construct() {
            parent::__construct();
            $this->load->database();

            $this->tbl_rfq = TABLE_PREFIX . 'rfq';
            $this->tbl_users = TABLE_PREFIX . 'users';
            $this->tbl_products = TABLE_PREFIX . 'products';
            $this->tbl_favorite_list = TABLE_PREFIX . 'favorite_list';
            $this->tbl_products_images = TABLE_PREFIX . 'products_images';
            $this->tbl_mailboox_master = TABLE_PREFIX . 'mailboox_master';
            $this->tbl_supplier_master = TABLE_PREFIX . 'supplier_master';
            $this->tbl_products_order_master = TABLE_PREFIX . 'products_order_master';
            $this->load->model('favorites/favorites_model', 'favorites');
            $this->tbl_users_groups = TABLE_PREFIX . 'users_groups';
            $this->tbl_feed = TABLE_PREFIX . 'feeds';
            $this->tbl_supplier_followers = TABLE_PREFIX . 'supplier_followers';
            
            
       }

       /**
        * Function for get all statical report for showing dashboard contents, like counts
        * Author : JK
        */
       function dashBoardStatistics() {

            if ($this->usr_grp == 'SP') { // if logged supplier
                 /* First count shows total active users or staff under supplier */
                 $data[0]['label'] = 'Active<br>staff';
                 $data[0]['icon'] = 'fa fa-user';
                 $data[0]['bgcolor'] = '#00BD1A'; 
                 $data[0]['url'] = site_url('emp_details');
                 $data[0]['count'] = sprintf('%02d', $this->db->join($this->tbl_users_groups, 'user_id = usr_id','LEFT')
                 
               //   ->where('usr_active', 1)
                 ->where('usr_supplier' , $this->suplr)
                 ->where('group_id' ,3)
                 ->where('usr_active', 1)
                 ->count_all_results($this->tbl_users));
                 /* Second count shows total active products */
                 $data[1]['label'] = 'Active<br>products';
                 $data[1]['icon'] = 'fa fa-shopping-bag';
                 $data[1]['bgcolor'] = '#efb800';
                 $data[1]['url'] = site_url('product/active');
                 $data[1]['count'] = sprintf('%02d', $this->db->where(array('prd_status' => 1, 'prd_supplier' => get_logged_user('usr_supplier')))
                                 ->count_all_results($this->tbl_products));
               //   debug($this->db->last_query());
                 //Total order
                 $data[2]['label'] = 'Total<br>INQUIRY';
                 $data[2]['icon'] = 'fa fa-shopping-bag';
                 $data[2]['bgcolor'] = '#FF666D';
                 $data[2]['url'] = site_url('order');
                 $data[2]['count'] = sprintf('%02d', $this->db->where(array('ord_supplier' => get_logged_user('usr_supplier')))
                                 ->count_all_results($this->tbl_products_order_master));
                 
                 //Total RFQ
            } else if (is_root_user()) { // if supper admin logged
                 /* First count shows total active users or staff under supplier */
                 $data[0]['label'] = 'Active<br>suppliers';
                 $data[0]['icon'] = 'fa fa-user';
                 $data[0]['bgcolor'] = '#00BD1A';
                 $data[0]['url'] = site_url('supplier/index/1');
                 $data[0]['count'] = sprintf('%02d', $this->db->where(array('supm_status' => 1))
                                 ->count_all_results($this->tbl_supplier_master));

                 /* Second count shows total active products */
                 $data[1]['label'] = 'Active<br>products';
                 $data[1]['icon'] = 'fa fa-shopping-bag';
                 $data[1]['bgcolor'] = '#efb800';
                 $data[1]['url'] = site_url('product/active');
                 $data[1]['count'] = sprintf('%02d', $this->db->where(array('prd_status' => 1))
                                 ->count_all_results($this->tbl_products));
                 
                 //Total order
                 $data[2]['label'] = 'Total<br>INQUIRY';
                 $data[2]['icon'] = 'fa fa-shopping-bag';
                 $data[2]['bgcolor'] = '#FF666D';
                 $data[2]['url'] = site_url('order');
                 $data[2]['count'] = sprintf('%02d', $this->db->count_all_results($this->tbl_products_order_master));
            } else if($this->usr_grp == 'ST') { // Staff logged in 
                 
                /* total active products */
                $data[0]['label'] = 'Total<br>products';
                $data[0]['icon'] = 'fa fa-shopping-bag';
                $data[0]['bgcolor'] = '#efb800';
                $data[0]['url'] = site_url('product');
                $data[0]['count'] = sprintf('%02d', $this->db->where(array('prd_added_by' => $this->uid))
                                ->count_all_results($this->tbl_products));
            } else { // buyer logged in 
                 
                 //Total product enquiry
                 $data[0]['label'] = 'UNREAD<br>MESSAGE';
                 $data[0]['icon'] = 'fa fa-shopping-bag';
                 $data[0]['bgcolor'] = '#00BD1A';
                 $data[0]['url'] = site_url('mailbox/inbox');
                 $data[0]['count'] = sprintf('%02d', $this->db->where(array('mms_to' => $this->uid, 'mms_last_viewd_on' => NULL, 'mms_status' => 1))
                                 ->count_all_results($this->tbl_mailboox_master));

                 //Total rfq
                 $data[1]['label'] = 'RFQ<br>Send';
                 $data[1]['icon'] = 'fa fa-shopping-bag';
                 $data[1]['bgcolor'] = '#efb800';
                 $data[1]['url'] = site_url('product/rfq-list');
                 $data[1]['count'] = sprintf('%02d', $this->db->where('rfq_user', $this->uid)
                                 ->count_all_results($this->tbl_rfq));

//                 //Total order
//                 $data[2]['label'] = 'Total<br>INQUIRY';
//                 $data[2]['icon'] = 'fa fa-shopping-bag';
//                 $data[2]['bgcolor'] = '#FF666D';
//                 $data[2]['count'] = sprintf('%02d', $this->db->where('ord_added_by', $this->uid)
//                                 ->count_all_results($this->tbl_products_order_master));

                 //Total order
                 $data[3]['label'] = 'FAVOURITE<br>PRODUCTS';
                 $data[3]['icon'] = 'fa fa-shopping-bag';
                 $data[3]['bgcolor'] = '#00B7AD';
                 $data[3]['url'] = site_url('favorites/product-list');
                 $data[3]['count'] = sprintf('%02d', $this->db->where(array('fav_added_by' => $this->uid, 'fav_consign' => 'PRD'))
                                 ->count_all_results($this->tbl_favorite_list));
            }
            $analysis['analysis'] = $data;
            return $analysis;
       }

       function dashboardMeterials() {
            $analysis = array();
            if($this->usr_grp == 'SP' || $this->usr_grp == 'ST' || is_root_user()) {
                /* Main graph */
    
                // Start date
                $date = date('Y-m-d', (strtotime('-30 day', strtotime(date('Y-m-d')))));
                // End date
                $end_date = date('Y-m-d');
                $order = array();
                $dates = array();
                while (strtotime($date) <= strtotime($end_date)) {
                     $datesFullFormat[] = date('d-m-Y', strtotime($date));
    
                     $dates[] = "'" . date('d', strtotime($date)) . "'";
                     $fulldate[] = "'" . date('d-m-Y', strtotime($date)) . "'";
                     $where = !is_root_user() ? "AND ord_supplier = " . $this->suplr : '';
                     $order[] = $this->db->query("SELECT COUNT(*) AS order_count FROM " . $this->tbl_products_order_master . " WHERE DATE(ord_added_on) = '$date' " . $where)
                                     ->row()->order_count;
    
                     $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                }
                $analysis['mainGraphContent']['datesFullFormat'] = $datesFullFormat;
                $analysis['mainGraphContent']['dates'] = implode(',', $dates);
                $analysis['mainGraphContent']['order'] = implode(',', $order);
                $analysis['mainGraphContent']['fulldate'] = implode(',', $fulldate);
                /* Main graph */
            }    

            if ($this->usr_grp == 'BY') {
                 
                 $analysis['enquires'] = $this->db->limit(4)->get_where($this->tbl_products_order_master, array('ord_added_by' => $this->uid))->result_array();

                 $analysis['favSupplier'] = array_slice($this->favorites->myFavoritesSuppliers(), 0, 4);
                 $analysis['favProducts'] = array_slice($this->favorites->myFavoritesProducts(), 0, 4);
                   
                 $analysis['followingSup'] = array_slice($this->favorites->followingSuppliers(), 0, 4);
               //   debug($analysis['followingSup']);
                 $products = $this->db->select($this->tbl_feed . '.*,'.$this->tbl_products . '.prd_default_image,prd_id')
                                 ->join($this->tbl_feed, 'f_sup_id = sfol_supplier')
                                 ->join($this->tbl_products, 'prd_id = f_prd_id','RIGHT')
                                 ->where(array('f_status' => 1, 'sfol_followed_by' => $this->uid))
                                 ->order_by('f_added_on', 'DESC')->limit(4)->get($this->tbl_supplier_followers)->result_array();
                 if (!empty($products)) {
                      foreach ($products as $key => $value) {
                           if (isset($value['prd_default_image']) && !empty($value['prd_default_image'])) {
                                $images = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $value['prd_id'], 'pimg_id' => $value['prd_default_image']))->row_array();
                           } else {
                                $images = $this->db->limit(1)->get_where($this->tbl_products_images, array('pimg_product' => $value['prd_id']))->row_array();
                           }
                           $products[$key]['default_image'] = isset($images['pimg_image']) ? $images['pimg_image'] : '';
                         //   $products[$key]['images'] = $this->db->get_where($this->tbl_products_images, array('pimg_product' => $value['prd_id']))->result_array();
                      }
                 }
                 $analysis['supProdFeed'] = $products;
            }
            if($this->usr_grp == 'SP') {
                 $analysis['followers'] = array_slice($this->favorites->followers(), 0, 4);
            }
            return $analysis;
       }

  }
  