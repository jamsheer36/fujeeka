<div class="right_col" role="main" style="min-height: 1179px;">
     <!-- top tiles -->
     <div class="row tile_count mt-130">
          <?php foreach ((array) $analysis as $key => $value) {?>
                 <a class="col-md-3 col-sm-4 col-xs-6" href="<?php echo isset($value['url']) ? $value['url'] : 'javascript:;'?>">
                      <div class="tile_stats_count" style="background:<?php echo $value['bgcolor'];?>">
                           <div class="row">
                                <div class="col-md-6 count_top color-bgs">
                                     <?php echo $value['label'];?>
                                </div>
                                <div class="col-md-6 count"><?php echo $value['count'];?></div>
                           </div>
                      </div>
                 </a>
                 <?php
            }
          ?>
     </div>
     <section class="split-sec">
          <div class="container">
               <div class="row">
                    <!-- Enquiry section-->
                    <div class="col-md-6">
                         <div class="card order-card">
                              <div class="ord-head">
                                   <h3>Enquiries</h3>
                              </div>

                              <ul>
                                   <?php
                                     if (isset($dashboardMeterials['enquires']) && !empty($dashboardMeterials['enquires'])) {
                                          foreach ($dashboardMeterials['enquires'] as $key => $value) {
                                               ?>
                                               <li>
                                                    <a href="<?php echo site_url('order/order_summery/' . encryptor($value['ord_id']));?>">
                                                         <div class="ord-single">
                                                              <div class="ord-item"><?php echo $value['ord_message'];?></div>
                                                              <div class="ord-qty"><?php echo $value['ord_qty'];?> Pieces</div>
                                                         </div>
                                                    </a>
                                               </li>
                                               <?php
                                          }
                                     } else {
                                          ?>
                                          <li>
                                               <a href="javascript:;">
                                                    <div class="ord-single">
                                                         <div class="ord-item" style="width: 100%; text-align: center;">
                                                              No enquiry found
                                                         </div>
                                                    </div>
                                               </a>
                                          </li>
                                     <?php }?>
                              </ul>
                         </div>
                    </div>
                    <!-- /Enquiry section-->

                    <!-- Favourite supplier-->
                    <div class="col-md-6">
                         <div class="card order-card">
                              <div class="ord-head">
                                   <h3>Favorite Supplier</h3>
                              </div>
                              <ul>
                                   <?php
                                     if (isset($dashboardMeterials['favSupplier']) && !empty($dashboardMeterials['favSupplier'])) {
                                          foreach ($dashboardMeterials['favSupplier'] as $key => $value) {
                                               $href = "http://".$value['supm_domain_prefix'] .'.'.$this->http_host;
                                               ?>
                                               <li class="supFavList<?php echo $value['fav_id'];?>">
                                                         <div class="ord-single">
                                                              <div style="cursor:pointer" class="ord-item fav-list" onclick="location.href='<?=$href;?>'">
                                                                   <?php echo $value['supm_name_en'];?>
                                                              </div>
                                                              <div class="ord-close">
                                                                   <button type="button" class="close deleteListItemCommon" aria-label="Close"
                                                                           data-url="<?php echo site_url('favorites/removefavesupp/' . encryptor($value['fav_id']));?>"
                                                                           data-remove="<?php echo 'supFavList' . $value['fav_id'];?>">
                                                                        <span aria-hidden="true">×</span>
                                                                   </button>
                                                              </div>
                                                         </div>
                                               </li>
                                               <?php
                                          }
                                     } else {
                                          ?>
                                          <li>
                                               <a href="javascript:;">
                                                    <div class="ord-single">
                                                         <div class="ord-item" style="width: 100%; text-align: center;">
                                                              No favorite suppliers found
                                                         </div>
                                                    </div>
                                               </a>
                                          </li>
                                     <?php }?>
                              </ul>
                         </div>
                    </div>
                    <!-- /Favourite supplier-->
                    <?php if ($this->usr_grp == 'BY') {?>
                           <div class="col-md-12">
                                <div class="card order-card">
                                     <div class="ord-head">
                                          <h3>Following suppliers</h3>
                                          <span class="v-link">
                                               <a href="<?php echo site_url('favorites/following');?>">View all</a>
                                          </span>
                                     </div>
                                     <ul>
                                          <?php
                                          if (isset($dashboardMeterials['followingSup']) && !empty($dashboardMeterials['followingSup'])) {
                                               foreach ($dashboardMeterials['followingSup'] as $key => $value) {
                                                  $href = "http://".$value['supm_domain_prefix'] .'.'.$this->http_host;
                                                    ?>
                                                    <li class="supFollowing<?php echo $value['sfol_id'];?>">
                                                              <div class="ord-single">
                                                                   <div class="ord-item fav-list" style="cursor:pointer" class="ord-item fav-list" onclick="location.href='<?=$href;?>'">
                                                                        <?php echo $value['supm_name_en'];?>
                                                                   </div>
                                                                   <div class="ord-close">
                                                                        <button type="button" class="close deleteListItemCommon" aria-label="Close"
                                                                                data-url="<?php echo site_url('favorites/removeFollow/' . encryptor($value['sfol_id']));?>"
                                                                                data-remove="<?php echo 'supFollowing' . $value['sfol_id'];?>">
                                                                             <span aria-hidden="true">×</span>
                                                                        </button>
                                                                   </div>
                                                              </div>
                                                    </li>
                                                    <?php
                                               }
                                          } else {
                                               ?>
                                               <li>
                                                    <a href="javascript:;">
                                                         <div class="ord-single">
                                                              <div class="ord-item" style="width: 100%; text-align: center;">
                                                                   No following found
                                                              </div>
                                                         </div>
                                                    </a>
                                               </li>
                                          <?php }?>
                                     </ul>
                                </div>
                           </div>
                      <?php }?>
               </div>
          </div>
     </section>

     <!-- Favourite Product -->
     <section class="card-section">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="card order-card">
                              <div class="ord-head">
                                   <h3>Favorite Products</h3>
                                   <span class="v-link">
                                        <a href="<?php echo site_url('favorites/product-list');?>">View all</a>
                                   </span>
                              </div>

                              <div class="row mt-2">
                                   <?php
                                     if (isset($dashboardMeterials['favProducts']) && !empty($dashboardMeterials['favProducts'])) {
                                          foreach ($dashboardMeterials['favProducts'] as $key => $value) {
                                               ?>
                                               <a class="col-md-3" href="<?php echo site_url('product/product-details/' . encryptor($value['prd_id']));?>">
                                                    <div class="prod-container">
                                                         <div class="pr-img">
                                                              <?php echo img(array('src' => FILE_UPLOAD_PATH . 'product/' . $value['default_image'], 'alt' => '', 'class' => 'img-responsive'));?>
                                                         </div>
                                                         <h5><?php echo get_snippet($value['prd_name_en'], 5);?></h5>
                                                    </div>
                                               </a>
                                               <?php
                                          }
                                     } else {
                                          ?>
                                          <div class="col-md-3">
                                               <div class="prod-container">
                                                    <div class="pr-img">
                                                         No products found
                                                    </div>
                                               </div>
                                          </div>
                                     <?php }?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </section>
     <!-- /Favourite Product -->

     <!-- Supplier Product Feed -->
     <section class="card-section">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="card order-card">
                              <div class="ord-head">
                                   <h3>Feeds from Supplier</h3>
                                   <span class="v-link">
                                        <a href="<?php echo site_url('supplier/feeds');?>">View all</a>
                                   </span>
                              </div>
                              <div class="row mt-2">
                                   <?php
                                     if (isset($dashboardMeterials['supProdFeed']) && !empty($dashboardMeterials['supProdFeed'])) {
                                          foreach ($dashboardMeterials['supProdFeed'] as $key => $value) {
                                               ?>
                                               <div class="col-md-3">
                                                    <div class="prod-container">
                                                         <div class="pr-img">
                                                              <?php echo img(array('src' => FILE_UPLOAD_PATH . 'product/' . $value['default_image'], 'alt' => '', 'class' => 'img-responsive'));?>
                                                         </div>
                                                         <h5><?php echo get_snippet($value['f_text'], 5);?></h5>
                                                    </div>
                                               </div>
                                               <?php
                                          }
                                     } else {
                                          ?>
                                          <div class="col-md-3">
                                               <div class="prod-container">
                                                    <div class="pr-img">
                                                         No products found
                                                    </div>
                                               </div>
                                          </div>
                                     <?php }?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </section>
</div>