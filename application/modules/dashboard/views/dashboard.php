<div class="right_col" role="main">
     <!-- top tiles -->
     <div class="row tile_count mt-130">
          <?php foreach ((array) $analysis as $key => $value) {?>
                  <a class="col-md-3 col-sm-4 col-xs-6" href="<?php echo isset($value['url']) ? $value['url'] : 'javascript:;'?>">
                      <div class="tile_stats_count" style="background:<?php echo $value['bgcolor'];?>">
                           <div class="row">
                                <div class="col-md-8">
                                    <div class="count_top color-bgs">
                                        <?php echo $value['label'];?>
                                    </div>
                                </div>
                                <div class="col-md-4 count"><?php echo $value['count'];?></div>
                           </div>
                      </div>
                 </a>
                 <?php
            }
          ?>
     </div>


     <div class="row">
          <?php if ($this->usr_grp != 'SBA') {?>
               <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="dashboard_graph">
                         <div class="row x_title">
                              <div class="col-md-12">
                                   <h3>Product Inquiry
                                        <small>based on date between
                                             <?php
                                             echo isset($dashboardMeterials['mainGraphContent']['datesFullFormat'][0]) ? $dashboardMeterials['mainGraphContent']['datesFullFormat'][0] : '';
                                             echo ' - ';
                                             echo isset($dashboardMeterials['mainGraphContent']['datesFullFormat'][30]) ? $dashboardMeterials['mainGraphContent']['datesFullFormat'][30] : '';
                                             ?>
                                        </small>
                                   </h3>
                              </div>

                         </div>
                         <div class="col-md-12 col-sm-12 col-xs-12">
                              <div id="jqChart"></div>
                         </div>
                         <div class="clearfix"></div>
                    </div>
               </div>
          <?php } if ($this->usr_grp == 'SP') {?>
               <div class="col-md-12">
                    <div class="card order-card">
                         <div class="ord-head">
                              <h3>Followers</h3>
                              <span class="v-link">
                                   <a href="<?php echo site_url('favorites/followers');?>">View all</a>
                              </span>
                         </div>

                         <ul>
                              <?php
                              if (isset($dashboardMeterials['followers']) && !empty($dashboardMeterials['followers'])) {
                                   foreach ($dashboardMeterials['followers'] as $key => $value) {
                                        ?>
                                        <li>
                                             <a href="javascript:;">
                                                  <div class="ord-single">
                                                       <div class="ord-item"><?php echo $value['usr_first_name'] . ' ' . $value['usr_last_name'];?></div>
                                                  </div>
                                             </a>
                                        </li>
                                        <?php
                                   }
                              } else {
                                   ?>
                                   <li>
                                        <a href="javascript:;">
                                             <div class="ord-single">
                                                  <div class="ord-item" style="width: 100%; text-align: center;">
                                                       No enquiry found
                                                  </div>
                                             </div>
                                        </a>
                                   </li>
                              <?php }?>
                         </ul>
                    </div>
               </div>
          <?php } ?>
     </div>
</div>




<!-- -->
<link href="../vendors/jqchart/jquery.jqChart.css" rel="stylesheet"/>
<script src="../vendors/jqchart/jquery.jqChart.min.js" type="text/javascript"></script>

<script lang="javascript" type="text/javascript">
     $(document).ready(function () {
      $('#jqChart').jqChart({
               title: {text: ''},
               tooltips: {type: 'shared'},
               animation: {duration: 1},
               legend: {location: 'top'},
               border: {
                    lineWidth: 0
               },
               axes: [
                    {
                         type: 'category',
                         location: 'bottom',
                         categories: [<?php echo $dashboardMeterials['mainGraphContent']['dates']; ?>]
                    }
               ],
               series: [
                    {
                         type: 'colomn',
                         title: 'Product Inquiry',
                         fillStyles: ['#00b904', '#fea317'],
                         data: [<?php echo $dashboardMeterials['mainGraphContent']['order']; ?>],
                         customData: [<?php echo $dashboardMeterials['mainGraphContent']['fulldate']; ?>]
                    }
               ]
          });
          
          $('#jqChart').bind('tooltipFormat', function (event, data) {
                var fulldate = data.series.customData[data.index];
                return  "<b>" + data.series.title + "</b></br>" +
                        "Date : " + fulldate + "</br>" +    
                         "Inquiry : " + data.y;
            });
     });
</script>