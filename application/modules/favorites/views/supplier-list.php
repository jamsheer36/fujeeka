<div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Favorite supplier list</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>Image</th>

                                        <th>Supplier Name</th>
                                        <?php echo is_root_user() ? '<th>User</th>' : '';?>
                                        <th>Remove</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                     foreach ((array) $list as $key => $value) {
                                          ?>
                                          <tr>
                                               <td>
                                                    <?php
                                                    $supDetails = $this->favorites->getSuppliersImages($value['supm_id']);
                                                    echo img(array("src" => $supDetails['sup_default_image'], 'style' => 'width:100px;'));
                                                    ?>
                                               </td>
                                               <td><?php echo $value['supm_name_en'];?></td>
                                               <?php echo is_root_user() ? '<td>' . $value['usr_first_name'] . ' ' . $value['usr_last_name'] . '</td>' : '';?>
                                               <td>
                                                    <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                       data-url="<?php echo site_url($controller . '/removefavesupp/' . encryptor($value['fav_id']));?>">
                                                         <i class="fa fa-remove"></i>
                                                    </a>
                                               </td>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>