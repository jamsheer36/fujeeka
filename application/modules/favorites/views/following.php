<div class="right_col" role="main">
     <section class="feed-listing">
          <div class="container">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Following list</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="row">
                         <?php
                           foreach ($list as $key => $value) {
                                $url = site_url('supplier/home/' . slugify($value['supm_name_en']) . '/' . encryptor($value['supm_id']));
                                ?>
                                <div class="col-md-6 <?php echo encryptor($value['sfol_id']);?>">
                                     <div class="list-cont">
                                          <a class="list-img" href="<?php echo $url;?>">
                                               <?php
                                               $supDetails = $this->favorites->getSuppliersImages($value['supm_id']);
                                               echo img(array("src" => $supDetails['sup_default_image'], 'class' => 'img-responsive'));
                                               ?>
                                          </a>
                                          <a class="list-content" href="<?php echo $url;?>">
                                               <?php echo $value['supm_name_en'];?>
                                          </a>
                                          <div class="ord-close">
                                               <button type="button" class="close btnDeleteBlock" 
                                                       data-url="<?php echo site_url($controller . '/removeFollow/' . encryptor($value['sfol_id']));?>" 
                                                       data-area="<?php echo encryptor($value['sfol_id']);?>"
                                                       aria-label="Close"><span aria-hidden="true">×</span>
                                               </button>
                                          </div>
                                     </div>
                                </div>
                           <?php }?>
                    </div>
               </div>
          </div>
     </section>
</div>