<?php if (isset($suppliers['banner_images']) && !empty($suppliers['banner_images'])) {?>
       <section class="banner">
            <!--Slides-->
            <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1920px;height:600px;overflow:hidden;visibility:hidden;">
                 <!-- Loading Screen -->
                 <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                      <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                      <div style="position:absolute;display:block;top:0px;left:0px;width:100%;height:100%;"></div>
                 </div>
                 <div class="slides-container" data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1920px;height:600px;overflow:hidden;">
                      <?php foreach ($suppliers['banner_images'] as $key => $value) {?>
                           <div data-b = "2">
                                <img data-u="image" alt="Banner will be here" data-src2="uploads/home_banner/<?php echo $value['sbi_image']?>" 
                                      alt ="<?php echo $value['sbi_alttag']?>" class="img-fluid" />
                           </div>
                      <?php } ?>
                 </div>
                 <!-- Bullet Navigator -->
                 <div data-u="navigator" class="jssorb01" style="bottom:16px;right:16px;" data-autocenter="1">
                      <div data-u="prototype" style="width:12px;height:12px;"></div>
                 </div>
                 <!-- Arrow Navigator -->
                 <span data-u="arrowleft" class="jssora03l" style="top:0px;left:8px;width:55px;height:55px;" data-autocenter="2"><i class="fa fa-angle-left"></i></span>
                 <span data-u="arrowright" class="jssora03r" style="top:0px;right:8px;width:55px;height:55px;" data-autocenter="2"><i class="fa fa-angle-right"></i></span>
            </div>
            <!--End Slides-->
       </section>
  <?php } if ($suppliersProducts) {?>
       <section class="recommended top-0 pt-5 pb-4">
            <div class="container">
                 <div class="col-12">
                      <div class="prd-title padl-10">
                           <h3>Our Products</h3>
                           <div class="v-all">
                                <a href="<?php echo "http://".$suppliers['supm_domain_prefix'] .'.'.$this->http_host.'/all-product'?>">View all</a>
                           </div>
                      </div>

                      <div class="prod-container">
                           <ul>
                                <!-- single product -->
                                <?php foreach ($suppliersProducts as $prod) {?>
                                     <li>
                                          <a href="<?php echo "http://".$suppliers['supm_domain_prefix'] .'.'.$this->http_host.'/'.'product/product-details/' . encryptor($prod['prd_id']) . '?rdfrom=profile';?>" style="text-decoration:none">
                                               <div class="prod-box">
                                                    <div class="prod-img">
                                                         <?php echo img(array('src' => $prod['default_image'], 'alt' => $prod['pimg_alttag'],'class' => 'img-fluid'));?>
                                                    </div>
                                                    <?php if ($prod['prd_is_stock_prod'] == 1) {?><div class="stock-notif">Stock</div><?php }?>
                                                    <div class="prod-detail txt-height">
                                                         <h4 class="prod-title-s"><?php echo get_snippet($prod['prd_name_en'], 3);?></h4>
                                                         <p><span class="strike-price">US $<?php echo $prod['prd_price_min'];?></span> <span class="price-text">US $<?php echo $prod['prd_offer_price_min'];?></span></p>
                                                         <!-- <p class="d-grey-txt">Stock : <span class="price-text"><?php echo $prod['prd_qty'];?> <?php echo $prod['unt_unit_en'];?></span></p> -->
                                                    </div>
                                                    
                                                    <?php if(!$this->uid) {?>
                                                       <div class="favourite">
                                                       <a href="javascript:;" data-toggle="modal" data-target="#loginModal" class="">
                                                              <i id="fav" class="far fa-heart"></i>
                                                              </a>
                                                       </div>
                                                    <?php } else if($this->uid && $this->usr_grp == 'BY') { ?>
                                                       <div class="favourite">
                                                         <a is-logged="<?php echo ($this->uid > 0) ? '1' : $this->calldrop;?>" href="javascript:;" class="btnFav" data-url="<?php echo site_url('favorites/addProductToFavoriteList/' . encryptor($prod['prd_id']));?>">
                                                              <i id="fav" class="far fa-heart <?php echo (isset($prod['isFavorite']) && !empty($prod['isFavorite'])) ? ' fas red-icn' : '';?>"></i></a>
                                                            </div>
                                                    <?php } ?>
                                               </div>
                                          </a>
                                     </li>
                                <?php }?> 
                                <!-- /single product -->
                           </ul>

                      </div>
                 </div>
            </div>
       </section>
  <?php }?>
<?php if ($latest) {?>
       <section class="recommended pt-4">
            <div class="container">
                 <div class="col-12">
                      <div class="prd-title padl-10">
                           <h3>Latest Products</h3>
                           <div class="v-all">
                                <a href="<?php echo "http://".$suppliers['supm_domain_prefix'] .'.'.$this->http_host.'/all-product'?>">View all</a>
                           </div>
                      </div>
                      <div class="prod-container mb-4">
                           <ul>
                                <!-- single product -->
                                <?php foreach ($latest as $prod) {?>
                                     <li>
                                          <a href="<?php echo "http://".$suppliers['supm_domain_prefix'] .'.'.$this->http_host.'/'.'product/product-details/' . encryptor($prod['prd_id']) . '?rdfrom=profile';?>" style="text-decoration:none">
                                               <div class="prod-box">
                                                    <div class="prod-img">
                                                         <?php echo img(array('src' => $prod['default_image'], 'alt' => $prod['pimg_alttag'], 'class' => 'img-fluid'));?>
                                                    </div>
                                                    <div class="prod-detail txt-height">
                                                         <h4 class="prod-title-s"><?php echo get_snippet($prod['prd_name_en'], 3);?></h4>
                                                         <p><span class="price-text">US $<?php echo $prod['prd_price_min'];?></span></p>
                                                         <p class="moq"><?php echo $prod['prd_moq'];?> <?php echo $prod['unt_unit_en'];?>  (MOQ)</p>
                                                    </div>
                                                    
                                                    <?php if(!$this->uid) {?>
                                                       <div class="favourite">
                                                       <a href="javascript:;" data-toggle="modal" data-target="#loginModal" class="">
                                                              <i id="fav" class="far fa-heart"></i>
                                                              </a>
                                                              </div>
                                                    <?php } else if($this->uid && $this->usr_grp == 'BY') { ?>
                                                       <div class="favourite">
                                                         <a is-logged="<?php echo ($this->uid > 0) ? '1' : $this->calldrop;?>" href="javascript:;" class="btnFav" data-url="<?php echo site_url('favorites/addProductToFavoriteList/' . encryptor($prod['prd_id']));?>">
                                                              <i id="fav" class="far fa-heart <?php echo (isset($prod['isFavorite']) && !empty($prod['isFavorite'])) ? ' fas red-icn' : '';?>"></i></a>
                                                              </div>
                                                    <?php } ?>
                                               </div>
                                          </a>
                                     </li>
                                <?php }?> 
                                <!-- /single product -->
                           </ul>
                      </div>
                 </div>
            </div>
       </section>
  <?php }?>
<!-- /featured products -->
<?php if ($stock) {?>
       <!-- stock products -->
       <section class="recommended pt-1">
            <div class="container">
                 <div class="col-12">
                      <div class="prd-title padl-10">
                           <h3>Stock Products</h3>
                           <div class="v-all">
                                <a href="<?php echo "http://".$suppliers['supm_domain_prefix'] .'.'.$this->http_host.'/all-product'?>">View all</a>
                           </div>
                      </div>
                      <div class="prod-container mb-4">
                           <ul>
                                <!-- single product -->
                                <?php foreach ($stock as $prod) {?>
                                     <li>
                                          <a href="<?php echo "http://".$suppliers['supm_domain_prefix'] .'.'.$this->http_host.'/'.'product/product-details/' . encryptor($prod['prd_id']) . '?rdfrom=profile';?>" style="text-decoration:none">
                                               <div class="prod-box">
                                                    <div class="prod-img">
                                                         <?php echo img(array('src' => $prod['default_image'], 'alt' => $prod['pimg_alttag'], 'class' => 'img-fluid'));?>
                                                    </div>
                                                    <div class="prod-detail txt-height">
                                                         <h4 class="prod-title-s"><?php echo get_snippet($prod['prd_name_en'], 3);?></h4>
                                                         <p><span class="strike-price">US $<?php echo $prod['prd_price_min'];?></span> <span class="price-text">US $<?php echo $prod['prd_offer_price_min'];?></span></p>
                                                         <p class="d-grey-txt">Stock : <span class="price-text"><?php echo $prod['prd_qty'];?> <?php echo $prod['unt_unit_en'];?></span></p>
                                                    </div>
                                                    
                                                    <?php if(!$this->uid) {?>
                                                       <div class="favourite">
                                                       <a href="javascript:;" data-toggle="modal" data-target="#loginModal" class="">
                                                              <i id="fav" class="far fa-heart"></i>
                                                              </a>
                                                              </div>
                                                    <?php } else if($this->uid && $this->usr_grp == 'BY') { ?>
                                                       <div class="favourite">
                                                         <a is-logged="<?php echo ($this->uid > 0) ? '1' : $this->calldrop;?>" href="javascript:;" class="btnFav" data-url="<?php echo site_url('favorites/addProductToFavoriteList/' . encryptor($prod['prd_id']));?>">
                                                              <i id="fav" class="far fa-heart <?php echo (isset($prod['isFavorite']) && !empty($prod['isFavorite'])) ? ' fas red-icn' : '';?>"></i></a>
                                                              </div>
                                                    <?php } ?>
                                               </div>
                                          </a>
                                     </li>
                                <?php }?> 
                                <!-- /single product -->
                           </ul>
                      </div>
                 </div>
            </div>
       </section>
  <?php }?>
<!-- /stock products -->
<section class="qd mt-4 pb-4">
     <div class="container">
          <div class="row">
               <div class="col qdw text-center">
                    <h2 class="heading4 mb-4 text-center">Quick Details</h2>
                    <ul>
                         <li>
                              <div class="qdc">
                                   <div class="qd-img">
                                        <img src="images/location-icon.png" alt="">
                                   </div>
                                   <div class="qd-desc">
                                        Location : <span><?php echo $suppliers['ctr_name']?> (<?php echo $suppliers['stt_name']?>)</span>
                                   </div>
                              </div>
                         </li>
                         <li>
                              <div class="qdc">
                                   <div class="qd-img">
                                        <img src="images/year-icon.png" alt="">
                                   </div>
                                   <div class="qd-desc">
                                        Year Established : <span><?php echo $suppliers['supm_reg_year']?></span>
                                   </div>
                              </div>
                         </li>
                         <li class="p-0">
                              <div class="qdc">
                                   <div class="qd-img">
                                        <img src="images/business-icon.png" alt="">
                                   </div>
                                   <div class="qd-desc">
                                        Business Type : <span>Manufacturer</span>
                                   </div>
                              </div>
                         </li>
                         <li>
                              <div class="qdc">
                                   <div class="qd-img">
                                        <img src="images/verification-icon.png" alt="">
                                   </div>
                                   <div class="qd-desc">
                                        Verification : <span class="green-txt">Verified Profile</span>
                                   </div>
                              </div>
                         </li>
                         <li>
                              <div class="qdc">
                                   <div class="qd-img">
                                        <img src="images/rating-icon.png" alt="">
                                   </div>
                                   <div class="qd-desc">
                                        Customer Rating :
                                        <span>
                                             <i class="fas fa-star"></i>
                                             <i class="fas fa-star"></i>
                                             <i class="fas fa-star"></i>
                                             <i class="far fa-star"></i>
                                             <i class="far fa-star"></i>
                                        </span>
                                   </div>
                              </div>
                         </li>
                         <li>
                              <div class="qdc">
                                   <div class="qd-img">
                                        <img src="images/markets-icon.png" alt="">
                                   </div>
                                   <div class="qd-desc">
                                        Main Market : <span>Domestic Market, Central America, Europe</span>
                                   </div>
                              </div>
                         </li>
                    </ul>
                    <?php if ($this->uid != $supplier_user_id['usr_id']) {?>
                           <a href="<?php echo site_url('supplier/profile/' . encryptor($suppliers['supm_id']))?>" class="btn btn-success btn-green mt-4">ABOUT COMPANY</a>
                      <?php }?>  
               </div>
          </div>
     </div>
</section> 
<!-- /company webpage -->
<!-- contact -->
<section class="grey-bg pb-4" id="scontact">
     <div class="container">
          <h2 class="heading2 mb-4 pt-4 text-center">Contact Us</h2>
          <div class="row">
               <div class="col-lg-12 col-md-12">
                    <!-- details -->
                    <div class="supp-det">
                         <div class="det w-750">
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Company Name :</dt>
                                   <dd class="d-entry-item-value"><?= $suppliers['supm_name_en']?></dd>
                              </dl>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Operational Address :</dt>
                                   <dd class="d-entry-item-value"><?= $suppliers['supm_address_en']?></dd>
                              </dl>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Country/Region :</dt>
                                   <dd class="d-entry-item-value"><?= $suppliers['ctr_name']?> / <?= $suppliers['stt_name']?></dd>
                              </dl>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Website :</dt>
                                   <dd class="d-entry-item-value"><?= $suppliers['supm_website']?></dd>
                              </dl>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Website on fujeeka :</dt>
                                   <dd class="d-entry-item-value"><?php echo $suppliers['supm_domain_prefix'].'.'.$this->http_host;?></dd>
                              </dl>
                         </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- /details -->
                    <!--                    <div class="btns-bx mt-5">
                                              <a href="javascript:;" class="btn btn-success btn-green float-left">Inquire now</a> 
                    <?php if ($this->uid != $supplier_user_id['usr_id']) {?>
                                                                                                                                                             <a href="<?php echo site_url('user/chat/' . encryptor($supplier_user_id['usr_id']))?>" class="chat float-left"><i class="fas fa-comments"></i> <span>Chat with us</span></a>
                      <?php }?>
                                             <div class="clearfix"></div>
                                        </div>-->
               </div>
          </div>
     </div>
</section>
<!-- /company webpage -->