<div class="right_col" role="main"><input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" /></form>
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2>Suppliers</h2>
                         <div class="clearfix"></div>
                    </div>
    <?php if(check_permission('supplier','report')) { ?> 
                       <div class="x_content lg-bg">
                         <form id="demo-form2" class="form-inline f_tp" method="post" action="<?php echo site_url($controller . '/report');?>" data-parsley-validate class="form-horizontal form-label-left frmEmployee" enctype="multipart/form-data">
                              <input value="0" type="hidden" name="" id="usr_id"/>
                              <div class="form-group">
                                       <label class="control-label" for="first-name">Start Date<span class="required">*</span>
                                   </label>
                                        <input type="text" id="date1" required="required" class="form-control" data-parsley-required-message="Start Date is required" name="start_date">
                              </div>
                              <div class="form-group">
                                       <label class="control-label" for="last-name">End Date <span class="required">*</span>
                                   </label>
                                        <input type="text" id="date2" name="end_date" required="required" data-parsley-required-message="End Date is required" class="form-control">
                              </div>

                                        <button type="submit"  name='type' value="excel" class="btn btn-success">Excel</button>
                                         <button type="submit" name='type' value="pdf" class="btn btn-danger">Pdf</button>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>

                  <?php } ?>

               </div>
               <div class="x_panel">
                   
                    <!-- <form id="testfrm"><input type="hidden" value="<?php echo base_url(); ?>" name="" id="url">
                    <input type="file" name="" id="file">
                      <input type="text"  id="message"><input type="button" value="Send" id="btn-chat">
                    </form> -->
                    <div class="x_content table-responsive">
                         <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                   <tr>
                                        <th>QR</th>
                                        <th>Avatar</th>
                                        <th>Name</th>
                                        <th>Country</th>
                                        <th>State</th>
                                        <!-- <?php echo check_permission($controller, 'delete') ? '<th>Delete</th>' : '';?> -->
                                        <?php echo check_permission($controller, 'changestatus') ? '<th style="width: 10%";>Status</th>' : '';?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php foreach ((array) $suppliers as $key => $value) {
                                          ?>
                                          <tr style="<?php echo ($value['supm_status'] == 0) ? 'background-color:#ffd4d4;color:#000;' : '';?>" data-url="<?php echo site_url($controller . '/view/' . encryptor($value['supm_id']));?>">
                                               <td class="trVOE" style="width: auto !important;">
                                                    <?php echo img(array('src' => FILE_UPLOAD_PATH . 'QR/' . $value['supm_ref_number'] . '.png', 'width' => '50'));?>
                                               </td>
                                               <td class="trVOE">
                                                    <?php echo img(array('src' => FILE_UPLOAD_PATH . 'avatar/thumb_' . $value['usr_avatar'], 'width' => '50'));?>
                                               </td>
                                               <td class="trVOE"><?php echo $value['supm_name_en'];?></td>
                                               <td class="trVOE"><?php echo $value['ctr_name'];?></td>
                                               <td class="trVOE"><?php echo $value['stt_name'];?></td>
                                               <!-- <?php if (check_permission($controller, 'delete')) {?>
                                                    <td>
                                                         <a class="pencile deleteListItem" href="javascript:void(0);" 
                                                            data-url=""><i class="fa fa-remove"></i>
                                                         </a>
                                                    </td>
                                               <?php }?> -->
                                               <?php if (check_permission($controller, 'changestatus')) {?>
                                                    <td>
                                                         <label class="switch">
                                                              <input type="checkbox" value="" class="chkOnchange" 
                                                              <?php echo $value['supm_status'] == 1 ? 'checked' : '';?>
                                                                     data-url="<?php echo site_url($controller . '/changestatus/' . encryptor($value['supm_id']) . '/' . encryptor($value['usr_id']));?>">
                                                              <span class="slider round"></span>
                                                         </label>
                                                    </td>
                                               <?php }?>
                                          </tr>
                                          <?php
                                     }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>
 <script>
$(function() {
  $('input[name="start_date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
     locale: {
      format: 'YYYY-MM-DD'
    }
  });
});
</script>
           
 <script>
$(function() {
  $('input[name="end_date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10),
     locale: {
     format: 'YYYY-MM-DD'
    }
  });
});
</script>
                          