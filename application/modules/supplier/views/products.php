<section class="sup-prod grey-bg">
        <div class="container">
          <h2 class="heading2 mb-2 pt-4">Supplier >> <span id="cat_name">All Category</span></h2>
          <div id="products" class="tab-pane active">
            <div class="row">
              <div class="col">
                <div class="filter-cat">
                  <h3>FILTER RESULTS BY : </h3>
                  <div class="s-filter">
                    <div class="s-cont">
                      <select class="form-control" id="sel1">
                        <option value="">Select a category</option>
                        <?php foreach ($categories as $cat) { ?>
                          <option <?php if($cat_id == $cat['cat_id']) { ?> selected <?php } ?> value="<?php echo $cat['cat_id']; ?>"><?php echo $cat['cat_title']; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- filters -->
            <div class="row">
              <div class="filter-container">
                <div class="col-9">
                  <!-- <form>
                   <label class="checkbox-inline">
                     <input type="checkbox" value="">Verified Manufacturers
                   </label>
                   <label class="checkbox-inline">
                     <input type="checkbox" value="">Accepts Small Orders
                   </label>
                   <label class="checkbox-inline">
                     <input type="checkbox" value="">Accepts Sample Orders
                   </label>
                 </form> -->
               </div>

               <div class="col-3">
                 <div class="sort">
                   <label for="sort_by">Sort by:</label>
                   <select class="form-control" id="sort_by">
                      <option value="">None</option>
                      <option value="price_asc">Price Low to High</option>
                      <option value="price_desc">Price High to Low</option>
                    <select>
                 </div>
               </div>

               <div class="clearfix"></div>
              </div>
            </div>
            <!--/ filters -->

            <div class="row">
              <!-- product container -->
              <div class="ajax_loader1 d-none"></div>
              <div class="prodct-container" id="product_listing">
              </div>
              <div class="col-12">
                <div id="pagination_link">
                </div>
              </div>
            </div>
          </div>
        </div>
     </section>