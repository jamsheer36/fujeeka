               
<section class="sup-prod grey-bg pb-4">
     <div class="container">
          <h2 class="heading2 mb-4 pt-4">About Company</h2>
          <div class="row">
               <div class="col-lg-6 col-md-12  pb-5">
                    <!-- details -->
                    <div class="supp-det">
                    <div class="det">
                              <?php if (isset($suppliers['supm_name_en'])) {?>
                                     <dl class="single-item-entry">
                                          <dt class="d-entry-item">Supplier name :</dt>
                                          <dd class="d-entry-item-value"><?php echo $suppliers['supm_name_en'];?></dd>
                                     </dl>
                                <?php } if (isset($suppliers['supm_email'])) {?>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Email :</dt>
                                   <dd class="d-entry-item-value"><?php echo $suppliers['supm_email'];?></dd>
                              </dl>
                              <?php } if (isset($suppliers['supm_website'])) {?>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Website :</dt>
                                   <dd class="d-entry-item-value"><?php echo $suppliers['supm_website'];?></dd>
                              </dl>
                              <?php } ?>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Website on fujeeka:</dt>
                                   <dd class="d-entry-item-value"><?php echo $suppliers['supm_domain_prefix'].'.'.$this->http_host;?></dd>
                              </dl>
                              <?php if (isset($suppliers['stt_name'])) {?>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">State :</dt>
                                   <dd class="d-entry-item-value"><?php echo $suppliers['stt_name'];?></dd>
                              </dl>
                              <?php } if (isset($suppliers['ctr_name'])) {?>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Country :</dt>
                                   <dd class="d-entry-item-value"><?php echo $suppliers['ctr_name'];?></dd>
                              </dl>
                              <?php } if (isset($suppliers['mar_name'])) {?>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Market :</dt>
                                   <dd class="d-entry-item-value"><?php echo $suppliers['mar_name'];?></dd>
                              </dl>
                              <?php } if (isset($suppliers['bld_title'])) {?>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Building :</dt>
                                   <dd class="d-entry-item-value"><?php echo $suppliers['bld_title'];?></dd>
                              </dl>
                              <?php } if (isset($suppliers['supm_room_number'])) {?>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Room Number :</dt>
                                   <dd class="d-entry-item-value"><?php echo $suppliers['supm_room_number'];?></dd>
                              </dl>
                              <?php } if (isset($suppliers['supm_reg_year'])) {?>
                              <dl class="single-item-entry">
                                   <dt class="d-entry-item">Registration year :</dt>
                                   <dd class="d-entry-item-value"><?php echo $suppliers['supm_reg_year'];?></dd>
                              </dl>
                              <?php }?>
                         </div>
                    </div>
                    <!-- /details -->
               </div>

               <div class="col-lg-6 col-md-12">
                    <!-- gallery -->
                    <div id="gallery" style="display:none;">
                         <?php
                           if (isset($suppliers['shop_images']) && !empty($suppliers['shop_images'])) {
                                foreach ($suppliers['shop_images'] as $key => $value) {
                                     ?>
                        <!--                                     <img alt="Youtube Video"
                                                                  data-type="youtube"
                                                                  data-videoid="5DDwX4KqyNM"
                                                                  data-description="Sample youtube video!">-->
                                     <img alt="Preview Image <?php echo $key; ?>"
                                          src="<?php echo 'uploads/shops/' . $value['ssi_image'];?>"
                                          data-image="<?php echo 'uploads/shops/' . $value['ssi_image'];?>"
                                          data-description="Preview Image <?php echo $key; ?> Description">
                                          <?php
                                     }
                                }
                              ?>
                    </div>
                    <!-- /gallery -->
               </div>
          </div>
     </div>
</section>

<section class="border-bottom mb-5 mt-4">
    <div class="container">
        <div class="row">
              <div class="col-md-12">
                  <div class="text-ct">
                      <h2 class="heading2">Company Info</h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                  </div>
              </div>
          </div>
    </div>
</section>
</div>
<!-- /company webpage -->
