<?php $suppliersid = isset($suppliers['supm_id']) ? $suppliers['supm_id'] : '';?>
<div class="right_col" role="main">
     <div class="">
          <div class="">
               <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="x_panel">
                         <?php echo form_open_multipart($controller . "/activate-supplier", array('id' => "frmSupplier", 'class' => "form-horizontal", 'data-parsley-validate' => "true"));?>
                         <input type="hidden" name="supm_id" value="<?php echo $suppliersid;?>" />
                         <div class="x_title">
                         <?php if(privilege_exists('SP') && isset($suppliers['supm_status']) && $suppliers['supm_status']!=0){?>
                            <h2>Edit Your Profile </h2>
                         <?php }  else {?>
                              <h2>Supplier activation form 
                                   <small style="color: red;"><?php echo (!is_root_user() && isset($suppliers['supm_status']) && empty($suppliers['supm_status'])) ? 
                                     'Your request to activate as supplier is in progress, we will touch you soon!' : '';?></small>
                              </h2>
                              <?php }?>
                              <div class="clearfix"></div>
                         </div>
                         <div class="x_content">
                              <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                   <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active">
                                             <a href="javascript:;" goto="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Supplier Details</a>
                                        </li>
                                        <li role="presentation" class="pars">
                                             <a href="javascript:;" goto="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Contact Person Details</a>
                                        </li>
                                        <li role="presentation" class="pars">
                                             <a href="javascript:;" goto="#tab_content3" role="tab" id="photos-tab" data-toggle="tab" aria-expanded="false">Photos</a>
                                        </li>
                                   </ul>
                                   <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier Name English</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter name" value="<?php echo isset($suppliers['supm_name_en']) ? $suppliers['supm_name_en'] : '';?>"  required placeholder="Supplier Name English" type="text" name="supplier[supm_name_en]" id="supm_name_en"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                  </div>
                                                  <div class="col-md-1 col-sm-1 col-xs-1">
                                                       <span style="float: right;cursor: pointer;font-size: 28px;" 
                                                             class="glyphicon glyphicon-globe btnOtherLanguage"></span>
                                                  </div>
                                             </div>
                                             <div class="divOtherLanguage" style="display: none;">
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier Name Arabic</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input value="<?php echo isset($suppliers['supm_name_ar']) ? $suppliers['supm_name_ar'] : '';?>" placeholder="Supplier Name Arabic" type="text" name="supplier[supm_name_ar]" id="supm_name_ar"
                                                                   class="form-control col-md-9 col-xs-12"/>
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier Name Chinese</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input value="<?php echo isset($suppliers['supm_name_ch']) ? $suppliers['supm_name_ch'] : '';?>" placeholder="Supplier Name Chinese" type="text" name="supplier[supm_name_ch]" id="supm_name_ch"
                                                                   class="form-control col-md-9 col-xs-12"/>
                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier Email</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter email" value="<?php echo isset($suppliers['supm_email']) ? $suppliers['supm_email'] : '';?>" placeholder="Supplier Email" type="email" name="supplier[supm_email]" id="supm_email" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier contact number</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter contact number" value="<?php echo isset($suppliers['supm_number']) ? $suppliers['supm_number'] : '';?>" placeholder="Supplier contact number" type="text" name="supplier[supm_number]" id="supm_number" 
                                                              class="form-control col-md-7 col-xs-12 numOnly"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier website link</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input data-parsley-type="url" value="<?php echo isset($suppliers['supm_website']) ? $suppliers['supm_website'] : '';?>" placeholder="www" type="text" name="supplier[supm_website]" id="supm_website" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <select required data-parsley-required-message="Select country" class="form-control col-md-7 col-xs-12 bindToDropdown"
                                                               data-dflt-select="Select State" name="supplier[supm_country]"
                                                               data-url="<?php echo site_url('states/getStatesByCountry');?>"
                                                               data-bind="cmbModel" id="supm_country">
                                                            <option value="">Select Country</option>
                                                            <?php foreach ((array) $country as $key => $value) {?>
                                                                   <option <?php echo ((isset($suppliers['supm_country'])) && ($value['ctr_id'] == $suppliers['supm_country'])) ? 'selected="selected"' : '';?>
                                                                        value="<?php echo $value['ctr_id'];?>"><?php echo $value['ctr_name'];?></option>
                                                                   <?php }?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">State</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <select required data-parsley-required-message="Select state" class="cmbModel select2_group form-control" name="supplier[supm_state]" i="supm_state">
                                                            <option value="0">Select state</option>
                                                            <?php foreach ((array) $states as $key => $value) {?>
                                                                   <option <?php echo ((isset($suppliers['supm_state'])) && ($value['stt_id'] == $suppliers['supm_state'])) ? 'selected="selected"' : '';?> 
                                                                        value="<?php echo $value['stt_id'];?>"><?php echo $value['stt_name'];?></option>
                                                                   <?php }?>
                                                       </select>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier City English</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter city" value="<?php echo isset($suppliers['supm_city_en']) ? $suppliers['supm_city_en'] : '';?>" placeholder="Supplier City English" type="text" name="supplier[supm_city_en]" id="supm_city_en" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                                  <div class="col-md-1 col-sm-1 col-xs-1">
                                                       <span style="float: right;cursor: pointer;font-size: 28px;" 
                                                             class="glyphicon glyphicon-globe btnOtherLanguage"></span>
                                                  </div>
                                             </div>
                                             <div class="divOtherLanguage" style="display: none;">
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier City Arabic</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input value="<?php echo isset($suppliers['supm_city_ar']) ? $suppliers['supm_city_ar'] : '';?>" placeholder="Supplier City Arabic" type="text" name="supplier[supm_city_ar]" id="supm_city_ar" 
                                                                   class="form-control col-md-7 col-xs-12"/>
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier City Chinese</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input value="<?php echo isset($suppliers['supm_city_ch']) ? $suppliers['supm_city_ch'] : '';?>" placeholder="Supplier City Chinese" type="text" name="supplier[supm_city_ch]" id="supm_city_ch" 
                                                                   class="form-control col-md-7 col-xs-12"/>
                                                       </div>
                                                  </div>
                                             </div>

                                             <?php if (isset($suppliers['supm_building'])) {?>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Market</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <select  required data-parsley-required-message="Select market" name="supplier[supm_market]" id="supm_market" class="form-control">
                                                                   <option value="">Select Market</option>
                                                                   <?php foreach ((array) $marketPlaces as $key => $value) {?>
                                                                        <option <?php echo ((isset($suppliers['supm_market'])) && ($value['mar_id'] == $suppliers['supm_market'])) ? 'selected="selected"' : '';?>  
                                                                             value="<?php echo $value['mar_id'];?>"><?php echo $value['mar_name'];?></option>
                                                                        <?php }?>
                                                              </select>
                                                         </div>
                                                    </div>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Buildings</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <select required data-parsley-required-message="Select Building" name="supplier[supm_building]" id="supm_building" class="form-control cmbBindBuildingCategories"
                                                                      data-url="<?php echo site_url('building/getBuildingCategories');?>">
                                                                   <option value="">Select Building</option>
                                                                   <?php foreach ((array) $buildings as $key => $value) {?>
                                                                        <option <?php echo ((isset($suppliers['supm_building'])) && ($value['bld_id'] == $suppliers['supm_building'])) ? 'selected="selected"' : '';?> 
                                                                             value="<?php echo $value['bld_id'];?>"><?php echo $value['bld_title'];?></option>
                                                                        <?php }?>
                                                              </select>
                                                         </div>
                                                    </div>
                                                    <div class="divBuildingCategories">
                                                         <div class="form-group">
                                                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Building Category</label>
                                                              <div class="col-md-6 col-sm-6 col-xs-12">
                                                                   <div class="div-category">
                                                                        <ul class="li-category">
                                                                             <?php
                                                                             foreach ((array) $buildingsCategories as $key => $value) {
                                                                                  $checked = '';
                                                                                  if (isset($suppliers['buildingCates'])) {
                                                                                       $checked = ((isset($suppliers['buildingCates'])) && (in_array($value['cat_id'], $suppliers['buildingCates']))) ? 'checked="checked"' : '';
                                                                                  }
                                                                                  ?>
                                                                                  <li><input data-parsley-errors-container="#cat_error" required data-parsley-required-message="Select category" <?php echo $checked;?> name="categories[]" value="<?php echo $value['cat_id'];?>" type="checkbox"><span><?php echo $value['cat_title'];?></span></li>
                                                                             <?php }?>
                                                                        </ul>
                                                                   </div><span id="cat_error"></span>
                                                              </div>
                                                         </div>
                                                    </div>
                                               <?php } else {?>
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Market</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <select required data-parsley-required-message="Select Market" name="supplier[supm_market]" id="supm_market" class="form-control">
                                                                   <option value="0">Select Market</option>
                                                                   <?php foreach ((array) $marketPlaces as $key => $value) {?>
                                                                        <option value="<?php echo $value['mar_id'];?>"><?php echo $value['mar_name'];?></option>
                                                                   <?php }?>
                                                              </select>
                                                         </div>
                                                    </div>

                                                    <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Buildings</label>
                                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                                              <select required data-parsley-required-message="Select Building" name="supplier[supm_building]" id="supm_building" class="form-control cmbBindBuildingCategories"
                                                                      data-url="<?php echo site_url('building/getBuildingCategories');?>">
                                                                   <option value="0">Select Building</option>
                                                                   <?php foreach ((array) $buildings as $key => $value) {?>
                                                                        <option value="<?php echo $value['bld_id'];?>"><?php echo $value['bld_title'];?></option>
                                                                   <?php }?>
                                                              </select>
                                                         </div>
                                                    </div>
                                                    <div class="divBuildingCategories"></div>
                                               <?php }?>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Address English</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <textarea style="width: 460px; height: 80px;" required data-parsley-required-message="Enter valid address" name="supplier[supm_address_en]" id="supm_address_en"  placeholder="Address English"><?php echo isset($suppliers['supm_address_en']) ? $suppliers['supm_address_en'] : '';?></textarea>
                                                  </div>
                                                  <div class="col-md-1 col-sm-1 col-xs-1">
                                                       <span style="float: right;cursor: pointer;font-size: 28px;" 
                                                             class="glyphicon glyphicon-globe btnOtherLanguage"></span>
                                                  </div>
                                             </div>

                                             <div class="divOtherLanguage" style="display: none;">
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Arabic</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <textarea style="width: 460px; height: 80px;" name="supplier[supm_address_ar]" id="supm_address_ar" placeholder="Address Arabic"><?php echo isset($suppliers['supm_address_ar']) ? $suppliers['supm_address_ar'] : '';?></textarea>
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Chinese</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <textarea style="width: 460px; height: 80px;" name="supplier[supm_address_ch]" id="supm_address_ch" placeholder="Address Chinese"><?php echo isset($suppliers['supm_address_ch']) ? $suppliers['supm_address_ch'] : '';?></textarea>
                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Room Number</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input value="<?php echo isset($suppliers['supm_room_number']) ? $suppliers['supm_room_number'] : '';?>" placeholder="Room Number" type="text" name="supplier[supm_room_number]" id="supm_room_number" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Domain Prefix</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input placeholder="Domain Prefix" data-parsley-pattern="^[a-z](?:_?[a-z]+)*$" data-parsley-pattern-message="Only alphabets , underscores with starting and ending with an alphabet are allowed" data-parsley-remote="<?php echo site_url("supplier/checkIfValueExists")?>" data-parsley-remote-options='{ "type": "POST","data": { "field": "supm_domain_prefix","in":"supplier","excludedomain":"<?php echo $suppliers['supm_domain_prefix'];?>" } }' data-parsley-remote-message="Prefix already taken" required data-parsley-required-message="Enter domain prefix" value="<?php echo isset($suppliers['supm_domain_prefix']) ? $suppliers['supm_domain_prefix'] : '';?>" type="text" name="supplier[supm_domain_prefix]" id="supm_domain_prefix" 
                                                            class="form-control col-md-7 col-xs-12"/>
                                                            <span id="domain_eg"></span>  
                                                  </div>
                                             </div>

                                             <!-- -->
                                             <div class="form-group">
                                                  <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Supplier keyword</label>
                                                  <div class="col-md-6 col-sm-3 col-xs-12">
                                                       <div class="table-responsive divVehDetailsSale">
                                                            <table class="tblModifiedTable table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                                 <thead>
                                                                      <tr>
                                                                           <th>English</th>
                                                                           <th>Arabic</th>
                                                                           <th>Chinese</th>
                                                                           <th><span style="cursor: pointer;" class="glyphicon glyphicon-plus btnAddMore"></span></th>
                                                                      </tr>
                                                                 </thead>
                                                                 <tbody>
                                                                      <?php
                                                                        if (!empty($suppliers['keywords']) && isset($suppliers['keywords'])) {
                                                                             foreach ((array) $suppliers['keywords'] as $key => $value) {
                                                                                  ?>
                                                                                  <tr>
                                                                                       <td>
                                                                                            <input value="<?php echo $value['supk_keyword_en'];?>" id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" name="keywords[en][]">
                                                                                       </td>
                                                                                       <td>
                                                                                            <input value="<?php echo $value['supk_keyword_ar'];?>" id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" name="keywords[ar][]">
                                                                                       </td>
                                                                                       <td>
                                                                                            <input value="<?php echo $value['supk_keyword_ch'];?>" id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" name="keywords[ch][]">
                                                                                       </td>
                                                                                       <td>
                                                                                            <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                                                                       </td>
                                                                                  </tr>
                                                                                  <?php
                                                                             }
                                                                        } else {
                                                                             ?>
                                                                             <tr>
                                                                                  <td>
                                                                                       <input id="keywords_en" placeholder="English" class="form-control col-md-7 col-xs-12" type="text" name="keywords[en][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input id="keywords_ar" placeholder="Arabic" class="form-control col-md-7 col-xs-12" type="text" name="keywords[ar][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <input id="keywords_ch" placeholder="Chinese" class="form-control col-md-7 col-xs-12" type="text" name="keywords[ch][]">
                                                                                  </td>
                                                                                  <td>
                                                                                       <span style="cursor: pointer;font-size: 12px;" class="glyphicon glyphicon-trash btnRemoveRow"></span>
                                                                                  </td>
                                                                             </tr>
                                                                        <?php }
                                                                      ?>
                                                                 </tbody>
                                                            </table>
                                                       </div>
                                                  </div>
                                             </div>
                                             <!-- -->

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Registration year </label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter Reg year" value="<?php echo isset($suppliers['supm_reg_year']) ? $suppliers['supm_reg_year'] : '';?>" placeholder="Registration year" type="text" name="supplier[supm_reg_year]" id="supm_reg_year" 
                                                              class="form-control col-md-7 col-xs-12 numOnly"/>
                                                       <small>For the getting how many years in market</small>
                                                  </div>
                                             </div>

                                             <!-- <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier grade</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <select name="supplier[supm_grade]" id="supm_grade" class="form-control">
                                                            <option value="0">Select grade</option>
                                             <?php foreach ($grades as $key => $value) {?>
                                                                                        <option <?php echo (isset($suppliers['supm_grade']) && ($suppliers['supm_grade'] == $value['sgrd_id'])) ? 'selected="selected"' : '';?> 
                                                                                             value="<?php echo $value['sgrd_id'];?>"><?php echo $value['sgrd_grade_en'];?></option>
                                               <?php }?>
                                                       </select>
                                                  </div>
                                             </div> -->

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Trade Assurance English</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <input value="<?php echo isset($suppliers['supm_trade_assurance_en']) ? $suppliers['supm_trade_assurance_en'] : '';?>" placeholder="Trade Assurance English" type="text" name="supplier[supm_trade_assurance_en]" 
                                                              id="supm_trade_assurance_en" class="form-control col-md-7 col-xs-12"/>
                                                       <small>Description</small>
                                                  </div>
                                                  <div class="col-md-1 col-sm-1 col-xs-1">
                                                       <span style="float: right;cursor: pointer;font-size: 28px;" 
                                                             class="glyphicon glyphicon-globe btnOtherLanguage"></span>
                                                  </div>
                                             </div>

                                             <div class="divOtherLanguage" style="display: none;">
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Trade Assurance Arabic</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input value="<?php echo isset($suppliers['supm_trade_assurance_ar']) ? $suppliers['supm_trade_assurance_ar'] : '';?>" placeholder="Trade Assurance Arabic" type="text" name="supplier[supm_trade_assurance_ar]" 
                                                                   id="supm_trade_assurance_ar" class="form-control col-md-7 col-xs-12"/>
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Trade Assurance Chinese</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input value="<?php
                                                              echo isset($suppliers['supm_trade_assurance_ch']) ? $suppliers['supm_trade_assurance_ch'] : '';
                                                              ;
                                                            ?>" placeholder="Trade Assurance Chinese" type="text" name="supplier[supm_trade_assurance_ch]" 
                                                                   id="supm_trade_assurance_ch" class="form-control col-md-7 col-xs-12"/>
                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Transaction level English</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <input value="<?php echo isset($suppliers['supm_transaction_level_en']) ? $suppliers['supm_transaction_level_en'] : '';?>" placeholder="Transaction level English" type="text" name="supplier[supm_transaction_level_en]" 
                                                              id="supm_transaction_level_en" class="form-control col-md-7 col-xs-12"/>
                                                       <small>Description</small>
                                                  </div>
                                                  <div class="col-md-1 col-sm-1 col-xs-1">
                                                       <span style="float: right;cursor: pointer;font-size: 28px;" 
                                                             class="glyphicon glyphicon-globe btnOtherLanguage"></span>
                                                  </div>
                                             </div>

                                             <div class="divOtherLanguage" style="display: none;">
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Transaction level Arabic</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input value="<?php
                                                              echo isset($suppliers['supm_transaction_level_ar']) ? $suppliers['supm_transaction_level_ar'] : '';
                                                              ;
                                                            ?>" placeholder="Transaction level Arabic" type="text" name="supplier[supm_transaction_level_ar]" 
                                                                   id="supm_transaction_level_ar" class="form-control col-md-7 col-xs-12"/>
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Transaction level Chinese</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input value="<?php echo isset($suppliers['supm_transaction_level_ch']) ? $suppliers['supm_transaction_level_ch'] : '';?>" placeholder="Transaction level Chinese" type="text" name="supplier[supm_transaction_level_ch]" 
                                                                   id="supm_transaction_level_ch" class="form-control col-md-7 col-xs-12"/>
                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Total Revenue</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter total revenue"  value="<?php echo isset($suppliers['supm_total_revenue']) ? $suppliers['supm_total_revenue'] : '';?>" placeholder="Total Revenue" type="text" name="supplier[supm_total_revenue]" 
                                                              id="supm_total_revenue" class="form-control col-md-7 col-xs-12 numOnly"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Transactions English</label>
                                                  <div class="col-md-5 col-sm-6 col-xs-12">
                                                       <input value="<?php echo isset($suppliers['supm_transactions_en']) ? $suppliers['supm_transactions_en'] : '';?>" placeholder="Transactions English" type="text" name="supplier[supm_transactions_en]" 
                                                              id="supm_transactions_en" class="form-control col-md-7 col-xs-12"/>
                                                       <small>eg:167 Transactions(6 months) $500,000+</small>
                                                  </div>
                                                  <div class="col-md-1 col-sm-1 col-xs-1">
                                                       <span style="float: right;cursor: pointer;font-size: 28px;" 
                                                             class="glyphicon glyphicon-globe btnOtherLanguage"></span>
                                                  </div>
                                             </div>

                                             <div class="divOtherLanguage" style="display: none;">
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Transactions Arabic</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input value="<?php echo isset($suppliers['supm_transactions_ar']) ? $suppliers['supm_transactions_ar'] : '';?>" placeholder="Transactions Arabic" type="text" name="supplier[supm_transactions_ar]" 
                                                                   id="supm_transactions_ar" class="form-control col-md-7 col-xs-12"/>
                                                       </div>
                                                  </div>
                                                  <div class="form-group">
                                                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Transactions Chinese</label>
                                                       <div class="col-md-5 col-sm-6 col-xs-12">
                                                            <input value="<?php echo isset($suppliers['supm_transactions_ch']) ? $suppliers['supm_transactions_ch'] : '';?>" placeholder="Transactions Chinese" type="text" name="supplier[supm_transactions_ch]" 
                                                                   id="supm_transactions_ch" class="form-control col-md-7 col-xs-12"/>
                                                       </div>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Supplier Official video</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input value="<?php echo isset($suppliers['supm_official_video']) ? $suppliers['supm_official_video'] : '';?>" placeholder="Supplier Official video" type="text" name="supplier[supm_official_video]" 
                                                              id="supm_official_video" class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>
                                                  </div>
                                             </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                             <input value="<?php echo $suppliers['usr_id'];?>" type="hidden" name="user[usr_id]" id="usr_id"/>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter name" value="<?php echo $suppliers['usr_first_name'];?>" placeholder="Name" type="text" name="user[usr_first_name]" id="usr_first_name" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter email"  data-parsley-remote="<?php echo site_url("supplier/checkIfValueExists")?>" data-parsley-remote-options='{ "type": "POST","data": { "field": "usr_email" ,"excludemail":"<?php echo $suppliers['usr_email'];?>"} }' data-parsley-remote-message="Email already exists" value="<?php echo $suppliers['usr_email'];?>" placeholder="Email" type="email" name="user[usr_email]" id="usr_email" 
                                                              class="form-control col-md-7 col-xs-12" autocomplete="off"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12 hidden">User name</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input value="<?php echo $suppliers['usr_first_name'];?>" placeholder="User name" type="hidden" name="user[usr_username]" id="usr_username"
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input placeholder="Password" type="password" name="user[usr_password]" id="usr_password" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input data-parsley-equalto="#usr_password" data-parsley-trigger="change" data-parsley-equalto-message="Should be same as password" placeholder="Confirm Password" type="password" name="user[usr_password_confirm]" id="usr_password_confirm" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact number</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input required data-parsley-required-message="Enter contact number" value="<?php echo $suppliers['usr_phone'];?>" placeholder="Contact number" type="text" name="user[usr_phone]" id="usr_phone" 
                                                              class="form-control col-md-7 col-xs-12 numOnly"/>
                                                  </div>
                                             </div>
                                             
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <textarea style="width: 480px; height: 80px;" name="user[usr_address]" placeholder="Address"><?php echo $suppliers['usr_address'];?></textarea>
                                                  </div>
                                             </div>

                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>
                                                       <button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>
                                                  </div>
                                             </div>
                                        </div>
                                        <!-- <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                             <input value="<?php echo $suppliers['usr_id'];?>" type="hidden" name="user[usr_id]" id="usr_id"/>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input value="<?php echo $suppliers['usr_first_name'];?>" placeholder="Name" type="text" name="user[usr_first_name]" id="usr_first_name" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">User name</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input value="<?php echo $suppliers['usr_username'];?>" placeholder="User name" type="text" name="user[usr_username]" id="usr_username"
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input placeholder="Password" type="password" name="user[usr_password]" id="usr_password" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input placeholder="Confirm Password" type="password" name="user[usr_password_confirm]" id="usr_password_confirm" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact number</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input value="<?php echo $suppliers['usr_phone'];?>" placeholder="Contact number" type="text" name="user[usr_phone]" id="usr_phone" 
                                                              class="form-control col-md-7 col-xs-12"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <input value="<?php echo $suppliers['usr_email'];?>" placeholder="Email" type="email" name="user[usr_email]" id="usr_email" 
                                                              class="form-control col-md-7 col-xs-12" autocomplete="off"/>
                                                  </div>
                                             </div>
                                             <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <textarea name="user[usr_address]" class='editor' placeholder="Address"><?php echo $suppliers['usr_address'];?></textarea>
                                                  </div>
                                             </div>

                                             <div class="ln_solid"></div>
                                             <div class="form-group">
                                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                       <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>
                                                       <button class="btn btn-primary btnNextTab" type="reset">Next <i class="fa fa-arrow-right"></i></button>
                                                  </div>
                                             </div>
                                        </div> -->
                                                         <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="photos-tab">
                                        <?php if (isset($suppliers['usr_avatar']) && !empty($suppliers['usr_avatar'])) {?>                            
                                               <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                         <div class="input-group">
                                                              <?php echo img(array('src' => FILE_UPLOAD_PATH . 'avatar/' . $suppliers['usr_avatar'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                         </div>
                                                         <?php if ($suppliers['usr_avatar']) {?>
                                                              <span class="help-block">
                                                                   <a data-url="<?php echo site_url('supplier/removeAvatar/' . $suppliers['usr_id']);?>" href="javascript:void(0);" 
                                                                     style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                             </span>
                                                         <?php }?>
                                                    </div>
                                               </div>
                                          <?php }?>

                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Logo</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <div id="newupload">
                                                       <input type="hidden" id="x10" name="x10" />
                                                       <input type="hidden" id="y10" name="y10" />
                                                       <input type="hidden" id="x20" name="x20" />
                                                       <input type="hidden" id="y20" name="y20" />
                                                       <input type="hidden" id="w0" name="w0" />
                                                       <input type="hidden" id="h0" name="h0" />
                                                       <input <?php if (!isset($suppliers['usr_avatar']) || empty($suppliers['usr_avatar'])) {?> 
                                                                   required data-parsley-required-message="Upload a file" <?php }?> data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                            type="file" class="form-control col-md-7 col-xs-12" name="userAvatar" id="image_file0" onchange="fileSelectHandler('0', '600', '300')" />
                                                       <img id="preview0" class="preview"/>
                                                  </div>
                                             </div>
                                        </div>

                                        <!-- Panorama view -->
                                        <?php if (isset($suppliers['supm_panoramic_image']) && !empty($suppliers['supm_panoramic_image'])) {?>                            
                                               <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                         <div class="input-group">
                                                              <?php echo img(array('src' => FILE_UPLOAD_PATH . 'panorama-shop/' . $suppliers['supm_panoramic_image'], 'height' => '200', 'width' => '700', 'id' => 'imgBrandImage'));?>
                                                         </div>
                                                         <?php if ($suppliers['supm_panoramic_image']) {?>
                                                              <span class="help-block">
                                                                   <a data-url="<?php echo site_url('supplier/removePanorama/' . $suppliers['supm_id']);?>" href="javascript:void(0);" 
                                                                      style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                              </span>
                                                         <?php }?>
                                                    </div>
                                               </div>
                                          <?php }?>

                                       <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Panorama View</label>
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                 <div id="newupload">
                                                       <input type="hidden" id="x11" name="x11" />
                                                       <input type="hidden" id="y11" name="y11" />
                                                       <input type="hidden" id="x21" name="x21" />
                                                       <input type="hidden" id="y21" name="y21" />
                                                       <input type="hidden" id="w1" name="w1" />
                                                       <input type="hidden" id="h1" name="h1" />
                                                       <input data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                              type="file" class="form-control col-md-7 col-xs-12" name="panoramaImage" id="image_file1" onchange="fileSelectHandler('1', '600', '300')" />
                                                       <img id="preview1" class="preview"/>
                                                       <span class="help-inline">Choose 2160(W) X 1080(H)</span>
                                                  </div>
                                             </div>
                                        </div>
                                        <!-- Banners -->
                                        <?php
                                          if (isset($suppliers['shop_images']) && !empty($suppliers['shop_images'])) {
                                               $i = 0;
                                               ?>
                                               <div class="form-group">

                                                    <?php
                                                    foreach ($suppliers['shop_images'] as $key => $value) {
                                                         if ($i % 4 == 0) {
                                                              ?>
                                                               <label class="control-label col-md-3 col-sm-3 col-xs-12">Shop Images</label>
                                                             
                                                         <?php }?>
                                                         <div class="bannerImageGroupshop col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['ssi_id'];?>" style="padding-left: 10px;">
                                                              <div class="input-group">
                                                                   <?php echo img(array('src' => FILE_UPLOAD_PATH . 'shops/' . $value['ssi_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                              </div>
                                                              <?php if ($value['ssi_image']) {?>
                                                                   <span class="help-block">
                                                                        <a data-url="<?php echo site_url('supplier/removeImage/' . $value['ssi_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                                   </span>
                                                                  <!-- <span class="help-block">
                                                                        <input <?php echo ($value['sbi_is_default'] == '1') ? 'checked="checked"' : '';?> class="btnSetDefaultImage" type="radio" 
                                                                              name="default_home_banner" data-url="<?php echo site_url('supplier/setDefaultBannerImage/' . $value['sbi_id']. '/' . $suppliers['supm_id']);?>"/>&nbsp; Make it default
                                                                   </span> -->
                                                                    <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="ssi_alttag[<?php echo $value['ssi_id']?>]" id="ssi_alttag" data-parsley-required ="true"data-parsley-required-message="Please enter an alt tag for the image" value="<?php echo $value['ssi_alttag']; ?>"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                            </div>
                                                              <?php }?>
                                                         </div>

                                                         <?php
                                                         $i++;
                                                    }
                                                    ?>
                                               </div>
                                          <?php }
                                        ?>
                                        <?php if (count($suppliers['shop_images']) < get_settings_by_key('sup_shop_img_limit') ) { ?>
                                        <div class="form-group">
                                             <label for="enq_cus_email" class="control-label col-md-3 col-sm-3 col-xs-12">Shop Images 
                                                  <small>(maximum <?php echo get_settings_by_key('sup_shop_img_limit');?> images)</small>
                                             </label>
                                             <div class="col-md-5 col-sm-3 col-xs-12">
                                                  <div id="newupload">
                                                       <input type="hidden" id="x13" name="x13[]" />
                                                       <input type="hidden" id="y13" name="y13[]" />
                                                       <input type="hidden" id="x23" name="x23[]" />
                                                       <input type="hidden" id="y23" name="y23[]" />
                                                       <input type="hidden" id="w3" name="w3[]" />
                                                       <input type="hidden" id="h3" name="h3[]" />
                                                       <input <?php if (!isset($suppliers['shop_images']) || empty($suppliers['shop_images'])) {?> 
                                                                   required data-parsley-required-message="Please Upload image" <?php }?> data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                            type="file" class="form-control col-md-7 col-xs-12" name="shopImages[]" 
                                                            id="image_file3" onchange="fileSelectHandler('3', '724', '378', '1')" />
                                                       <img id="preview3" class="preview"/>
                                                       <span class="help-inline">Choose 724(W) X 378(H)</span>
                                                  </div>
                                                  <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="altshopImgs[]" id="altshopImg0" data-parsley-required ="false" data-parsley-required-message="Please enter an alt tag for the image"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                         </div>
                                             </div>

                                             <div class="col-md-1 col-sm-1 col-xs-12">
                                                  <span style="float: right;cursor: pointer;" data-limit="<?php echo get_settings_by_key('sup_shop_img_limit') - count($suppliers['shop_images']) ;?>"
                                                        class="glyphicon glyphicon-plus btnMoreShopImages"></span>

                                             </div>

                                        </div>

                                        <div id="divMoreProductImages"></div>
<?php } ?>
                                        <!-- Banners -->
                                        <?php
                                          if (isset($suppliers['banner_images_web']) && !empty($suppliers['banner_images_web'])) {
                                               $i = 0;
                                               ?>
                                               <div class="form-group">
                                                    <?php
                                                    foreach ($suppliers['banner_images_web'] as $key => $value) {
                                                         if ($i % 4 == 0) {
                                                              ?>
                                                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Page Banner for website</label>
                                                         <?php }?>
                                                         <div class="bannerImageGroup col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['sbi_id'];?>" style="padding-left: 10px;">
                                                              <div class="input-group">
                                                                   <?php echo img(array('src' => FILE_UPLOAD_PATH . 'home_banner/' . $value['sbi_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                              </div>
                                                              <?php if ($value['sbi_image']) {?>
                                                                    <span class="help-block">
                                                                        <a data-url="<?php echo site_url('supplier/removeSupplierBanner/' . $value['sbi_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                                   </span>
                                                                   <span class="help-block">
                                                                        <input class="btnSetDefaultImage" type="radio" name="2default_home_bannerweb" 
                                                                        <?php echo ($value['sbi_is_default'] == '1') ? 'checked="checked"' : '';?>
                                                                               data-url="<?php echo site_url('supplier/setDefaultBannerImage/' . $value['sbi_id'] . '/' . $suppliers['supm_id']);?>"/>&nbsp; Make it default
                                                                   </span>


                                                                   <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="sbi_alttag[<?php echo $value['sbi_id']?>]" id="ssi_alttag" data-parsley-required ="true"data-parsley-required-message="Please enter an alt tag for the image" value="<?php echo $value['sbi_alttag']; ?>"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                            </div>
                                                                     
                                                              <?php }?>
                                                         </div>

                                                         <?php
                                                         $i++;
                                                    }
                                                    ?>
                                               </div>
                                          <?php }
                                        ?>

    <?php if (count($suppliers['banner_images_web']) < get_settings_by_key('sup_home_bnr_img_limit') ) { ?>

                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Page Banner for website with 1280(W) X 400(H)
                                                  <small>(maximum <?php echo get_settings_by_key('sup_home_bnr_img_limit');?> images)</small>
                                             </label>
                                             <div class="col-md-5 col-sm-3 col-xs-12">
                                                  <div id="newupload">
                                                       <input type="hidden" id="x19" name="x12[]" />
                                                       <input type="hidden" id="y19" name="y12[]" />
                                                       <input type="hidden" id="x29" name="x22[]" />
                                                       <input type="hidden" id="y29" name="y22[]" />
                                                       <input type="hidden" id="w9" name="w2[]" />
                                                       <input type="hidden" id="h9" name="h2[]" />
                                                       <input <?php if (!isset($suppliers['banner_images_web']) || empty($suppliers['banner_images_web'])) {?> 
                                                                   required1 data-parsley-required1-message="Upload a file" <?php }?> data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                            type="file" class="form-control col-md-7 col-xs-12" name="supm_home_banner[]" 
                                                            id="image_file9" onchange="fileSelectHandler('9', '1280', '400', '1')" />
                                                       <img id="preview9" class="preview"/>
                                                  </div>
                                                    <div id="altTag"><input placeholder="Alt tag for image" type="text" name="altSupBanImgs[]"id="altSupBanImg0" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image" class="form-control col-md-9 col-xs-12"/></div>

                                             </div>
                                             <div class="col-md-1 col-sm-1 col-xs-12">
                                                  <span style="float: right;cursor: pointer;" data-limit="<?php echo get_settings_by_key('sup_home_bnr_img_limit');?>" 
                                                        class="glyphicon glyphicon-plus btnMoreImages" data-h="400" data-w="1280" data-dest="#divMorePageBanner"
                                                        data-file-name="supm_home_banner[]" 
                                                        data-file-name2="altSupBanImgs[]" 
                                                        data-index="2" data-image-group=".bannerImageGroup"></span>
                                             </div>
                                        </div>

                                        <div id="divMorePageBanner"></div>
 <?php } ?>
                                        <?php
                                          if (isset($suppliers['banner_images_app']) && !empty($suppliers['banner_images_app'])) {
                                               $i = 0;
                                               ?>
                                               <div class="form-group">
                                                    <?php
                                                    foreach ($suppliers['banner_images_app'] as $key => $value) {
                                                         if ($i % 4 == 0) {
                                                              ?>
                                                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Page Banner for mobile/app</label>
                                                         <?php }?>
                                                         <div class="bannerImageGroup2 col-md-2 col-sm-3 col-xs-12 imgBox<?php echo $value['sbi_id'];?>" style="padding-left: 10px;">
                                                              <div class="input-group">
                                                                   <?php echo img(array('src' => FILE_UPLOAD_PATH . 'home_banner/' . $value['sbi_image'], 'height' => '80', 'width' => '100', 'id' => 'imgBrandImage'));?>
                                                              </div>
                                                              <?php if ($value['sbi_image']) {?>
                                                                   <span class="help-block">
                                                                        <a data-url="<?php echo site_url('supplier/removeSupplierBanner/' . $value['sbi_id']);?>" href="javascript:void(0);" style="width: 100px;" class="btn btn-block btn-danger btn-xs btnDeleteImage">Delete</a>
                                                                   </span>
                                                                   <span class="help-block">
                                                                        <input class="btnSetDefaultImage" type="radio" name="2default_home_bannerapp" 
                                                                        <?php echo ($value['sbi_is_default'] == '1') ? 'checked="checked"' : '';?>
                                                                               data-url="<?php echo site_url('supplier/setDefaultBannerImage/' . $value['sbi_id'] . '/' . $suppliers['supm_id']);?>"/>&nbsp; Make it default
                                                                   </span>

                                                                   <div id="altTag">
                                                            <input placeholder="Alt tag for image" type="text" name="sbi_alttag[<?php echo $value['sbi_id']?>]" id="ssi_alttag" data-parsley-required ="true"data-parsley-required-message="Please enter an alt tag for the image" value="<?php echo $value['sbi_alttag']; ?>"
                                                              class="form-control col-md-9 col-xs-12"/>
                                                            </div>
                                                              <?php }?>
                                                         </div>

                                                         <?php
                                                         $i++;
                                                    }
                                                    ?>
                                               </div>
                                          <?php }
                                        ?>

                                          <?php if (count($suppliers['banner_images_app']) < get_settings_by_key('sup_home_bnr_img_limit') ) { ?>
                                        <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Page Banner for mobile/app with 800(W) X 400(H)
                                                  <small>(maximum <?php echo get_settings_by_key('sup_home_bnr_img_limit');?> images)</small>
                                             </label>
                                             <?php
                                               $rand = rand(1, 100);
                                             ?>
                                             <div class="col-md-5 col-sm-3 col-xs-12">
                                                  <div id="newupload">
                                                       <input type="hidden" id="x1<?php echo $rand;?>" name="x14[]" />
                                                       <input type="hidden" id="y1<?php echo $rand;?>" name="y14[]" />
                                                       <input type="hidden" id="x2<?php echo $rand;?>" name="x24[]" />
                                                       <input type="hidden" id="y2<?php echo $rand;?>" name="y24[]" />
                                                       <input type="hidden" id="w<?php echo $rand;?>" name="w4[]" />
                                                       <input type="hidden" id="h<?php echo $rand;?>" name="h4[]" />
                                                       <input <?php if (!isset($suppliers['banner_images_web']) || empty($suppliers['banner_images_web'])) {?> 
                                                                   required1 data-parsley-required1-message="Upload a file" <?php }?> data-parsley-fileextension="jpg,png,gif,jpeg" 
                                                            type="file" class="form-control col-md-7 col-xs-12" name="supm_home_banner_mobile[]" 
                                                            id="image_file<?php echo $rand;?>" onchange="fileSelectHandler('<?php echo $rand;?>', '800', '400', '1')" />
                                                       <img id="preview<?php echo $rand;?>" class="preview"/>
                                                  </div>
                                                   <div id="altTag"><input placeholder="Alt tag for image" type="text" name="altSupBanmobImgs[]"id="altSupBanImg0" data-parsley-required ="false"data-parsley-required-message="Please enter an alt tag for the image" class="form-control col-md-9 col-xs-12"/></div>
                                             </div>
                                             <div class="col-md-1 col-sm-1 col-xs-12">
                                                  <span style="float: right;cursor: pointer;" data-limit="<?php echo get_settings_by_key('sup_home_bnr_img_limit');?>" 
                                                        class="glyphicon glyphicon-plus btnMoreImages" data-h="400" data-w="800" data-dest="#divMorePageBannerapp"
                                                        data-file-name="supm_home_banner_mobile[]"
                                                        data-file-name2="altSupBanmobImgs[]"  
                                                        data-index="4" data-image-group=".bannerImageGroup2"></span>
                                             </div>
                                        </div>
                                        <div id="divMorePageBannerapp"></div>
                                      <?php } ?>
                                        <!-- Banners -->                  

                                        <div class="ln_solid"></div>

                                        <div class="form-group">

                                             <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                                  <button class="btn btn-primary btnPrevTab" type="reset"><i class="fa fa-arrow-left"></i> Previous</button>

                                                  <?php echo check_permission($controller, 'update') ? '<button type="submit" class="btn btn-success">Submit</button>' : '';?>

                                             </div>

                                        </div>

                                   </div>
                                   </div>
                              </div>
                              <!-- Settings panel -->
                              <?php if (is_root_user() && check_permission('supplier', 'changestatus')) {?>
                                     <div class="theme-panel theme-panel-lg">
                                          <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
                                          <div class="theme-panel-content">
                                               <h5 class="m-t-0 text-center">Settings</h5>
                                               <div class="divider"></div>
                                               <div class="row m-t-10">
                                                    <div class="col-md-2">
                                                         <input type="checkbox" class="js-switch chkOnchange" name="product[prd_oem]" value="1" data-switchery="true" 
                                                                data-url="<?php echo site_url('supplier/verifySupplier/' . encryptor($suppliersid) . '/' . encryptor($suppliers['usr_id']));?>"
                                                                <?php echo $suppliers['supm_status'] == 1 ? 'checked' : '';?>>
                                                    </div>
                                                    <div class="col-md-10"><div class="fullwidth">Activate</div></div>
                                               </div>
                                               <div class="row m-t-10">
                                                    <div class="col-md-2">
                                                         <input type="checkbox" class="js-switch chkOnchange" 
                                                                data-url="<?php echo site_url('supplier/sendSupplierVerifyLink/' . encryptor($suppliersid) . '/' . encryptor($suppliers['usr_id']));?>"
                                                                value="1" data-switchery="true" data-url="">
                                                    </div>
                                                    <div class="col-md-10"><div class="fullwidth">Send confirmation link</div></div>
                                               </div>
                                          </div>
                                     </div>
                                <?php }?>
                              <!--Settings panel-->
                                                                  
                              <?php echo form_close()?>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
<style>
     .div-category {
          max-height: 260px;
          overflow-x: hidden;
          overflow-y: scroll;
          border: 2px solid #EEEEEE;
          width: 500px;
     }
     .li-category {
          list-style: none;
     }
     .li-category li {
          margin-left: -20px;
     }
     .li-category li span {
          margin-left: 10px;
     }
     label.error {
          display: none !important;
     }
     input.error {
          border: 1px solid red;
     }
</style>
<script>
     $("#supm_domain_prefix").keyup(function () {
          _val = $(this).val();
          if(_val){
              $("#domain_eg").html("Your Website on Fujeeka Will be <i>http://"+_val+".halal86.com</i>") 
          }
     });
</script>
