<!DOCTYPE HTML>
<html>
     <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <base href="<?php echo base_url('assets/');?>/"/>
          <title>Supplier Name 360</title>
          <link rel="stylesheet" href="css/pannellum.css"/>
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
          <script type="text/javascript" src="js/pannellum.js"></script>

          <!-- favicon-->
          <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
          <link rel="icon" href="images/favicon.ico" type="image/x-icon">

          <style>
               body {
                    width: 100%;
                    height: 100vh;
                    margin: 0;
                    padding: 0;
               }
               #panorama {
                    width: 100%;
                    height: 100%;
               }
               #controls {
                    position: absolute;
                    bottom: 0;
                    z-index: 2;
                    text-align: center;
                    width: 100%;
                    padding-bottom: 3px;
               }
               .ctrl {
                    padding: 8px 5px;
                    width: 30px;
                    text-align: center;
                    background: rgba(200, 200, 200, 0.8);
                    display: inline-block;
                    cursor: pointer;
               }
               .ctrl:hover {
                    background: rgba(200, 200, 200, 1);
               }

          </style>
     </head>
     <body>

          <div id="panorama">
               <div id="controls">
                    <div class="ctrl" id="pan-up">&#9650;</div>
                    <div class="ctrl" id="pan-down">&#9660;</div>
                    <div class="ctrl" id="pan-left">&#9664;</div>
                    <div class="ctrl" id="pan-right">&#9654;</div>
                    <div class="ctrl" id="zoom-in">&plus;</div>
                    <div class="ctrl" id="zoom-out">&minus;</div>
                    <div class="ctrl" id="fullscreen">&#x2922;</div>
               </div>
          </div>
          <?php $Image_360 = 'uploads/panorama-shop/' . $suppliers['supm_panoramic_image'];?>
          <script>
               // Create viewer
               viewer = pannellum.viewer('panorama', ﻿{
               "title": "<?php echo $suppliers['supm_name_en'];?>",
               "panorama": "<?php echo $Image_360; ?>",
               "autoLoad": true,
               "showControls": false,
                "autoRotate": -5
               });

               // Make buttons work
               document.getElementById('pan-up').addEventListener('click', function (e) {
                    viewer.setPitch(viewer.getPitch() + 10);
               });
               document.getElementById('pan-down').addEventListener('click', function (e) {
                    viewer.setPitch(viewer.getPitch() - 10);
               });
               document.getElementById('pan-left').addEventListener('click', function (e) {
                    viewer.setYaw(viewer.getYaw() - 10);
               });
               document.getElementById('pan-right').addEventListener('click', function (e) {
                    viewer.setYaw(viewer.getYaw() + 10);
               });
               document.getElementById('zoom-in').addEventListener('click', function (e) {
                    viewer.setHfov(viewer.getHfov() - 10);
               });
               document.getElementById('zoom-out').addEventListener('click', function (e) {
                    viewer.setHfov(viewer.getHfov() + 10);
               });
               document.getElementById('fullscreen').addEventListener('click', function (e) {
                    viewer.toggleFullscreen();
               });

          </script>

     </body>
</html>
