<!-- Banner -->
<div class="sr-banner">
     <img src="images/banners/sr.jpg" alt="Register as Supplier" class="img-fluid">
</div>
<!-- Banner ends -->

<section class="sup-content-area grey-bg">
     <div class="container">
          <div class="row">
               <div class="col-md-7">
                    <div class="cntent">
                         <h2 class="heading2">
                              Why register as Supplier?
                         </h2>
                         <p style="text-align: justify;">By Registering as supplier, you are eligible for many services as,</p>
                         <ul>
                           <li>Suppliers can add staff for managing their dashboard and give a permission for accessing a module (like product add, manage etc.,).</li>
                           <li>Suppliers can update their basic features and upload Shop images and 360-degree images of their shops.</li>
                           <li>Product adding: Suppliers can add product with all the details and manage the added product.</li>
                           <li>Stock Updating: Suppliers can add stock on daily basis.</li>
                           <li>Supplier offer: Supplier can update their offers which can be visible to the buyers.</li>
                           <li>Inquiry: Suppliers can view the product inquiry from buyers. </li>
                           <li>RFQ: Suppliers will get RFQ (Request for a quote) from various buyer and option for replying.</li>
                           <li>Chatting system: Suppliers can reply to the chats from buyers.</li>
                           <li>Communication system: The mobile app is enabled with a communication feature to help suppliers communicate with buyers.</li>
                           <li>Report:  The website enables users to access on demand product reports. </li>
                           <li>Supplier page: All suppliers are assigned a dedicated web page on the Fujeeka website.</li>
                           <li>Cost Effective : This system is cost effective for both suppliers and customers. Supplier does not have to maintain large store fronts which helps with huge room rents, electricity charges, maintenance charges, etc. The customer does not have to travel to remote destinations to find the matching products and suppliers for his requirements. This helps to cut the costs by a huge level.</li>
                           <li>Better reachability : The products of the suppliers can reach a wide range of customers at global level. This ensures better sales than current system. The reachability can be further increased by ads.</li>
                           <li>Better security : Each suppliers will be verified physically and rating system for products, suppliers & customers ensures peace of mind for both suppliers & customers.</li>
                           <li>The language differences of Suppliers and customers won’t affect.</li>
                         </ul>
                    </div>
               </div>

               <div class="col-md-5">
                    <div class="card card-frms">
                         <h2 class="heading2">
                              REGISTER AS SUPPLIER
                         </h2>

                         <form data-parsley-validate="true" class="frmSupplierRegister" action="<?php echo site_url('supplier/supplier-register'); ?>">
                              <div class="form-group">
                                   <input type="text" name="supm_name_en" class="form-control" id="sphone" placeholder="Name"
                                          required data-parsley-required-message="Enter your name">
                              </div>

                              <div class="form-group">
                                   <input type="email" name="supm_email" class="form-control" id="sEmail1"
                                          required data-parsley-required-message="Enter valid email"
                                          aria-describedby="emailHelp" placeholder="Enter email">
                                   <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                              </div>
                              <div class="form-group">
                                   <input type="number" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" name="supm_number" class="form-control" id="sphone" placeholder="Phone">
                              </div>
                              <div class="form-group">
                                   <textarea name="supm_desc" class="form-control" id="sComments" placeholder="Comments" rows="3"></textarea>
                              </div>

                              <div class="w-100 text-center">
                                   <button type="submit" class="btn btn-success btn-green btn-bold btnSupplierRegister">Send Request</button>
                              </div>
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                         </form>
                    </div>
               </div>
          </div>
     </div>
</section>

<style>
     p.parsley-success {
          color: #468847;
          background-color: #DFF0D8;
          border: 1px solid #D6E9C6
     }

     p.parsley-error {
          color: #B94A48;
          background-color: #F2DEDE;
          border: 1px solid #EED3D7
     }
     li.parsley-required {
          font-size: 10px;
     }
     ul.parsley-errors-list {
          list-style: none;
          color: #E74C3C;
          padding-left: 0
     }

     input.parsley-error,
     select.parsley-error,
     textarea.parsley-error {
          background: #FAEDEC;
          border: 1px solid #E85445
     }

     .btn-group .parsley-errors-list {
          display: none
     }
</style>
