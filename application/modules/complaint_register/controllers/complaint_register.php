<?php

defined('BASEPATH') or exit('No direct script access allowed');
require './vendor/autoload.php';

class complaint_register extends App_Controller
{

    public function __construct()
    {

        parent::__construct();
     
        if (substr(CI_VERSION, 0, 1) == '2') {
            $this->load->library('session');
       } else {
            $this->load->driver('session');
       }
        $this->load->model('complaint_model', 'complaint');
        require_once APPPATH . 'libraries/PHPMailer/PHPMailerAutoload.php';
        $this->mail = new PHPMailer;
            $this->template->set_layout('common');
      
    }

 
    public function index()
    {
       
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_userdata(array('callback' => site_url(strtolower(__CLASS__))));
            redirect('user/login');
        }
        $this->page_title = 'Complaint Register';
        $this->page_header = "Complaint Register";
        $this->template->set_layout('common');
        $data['banner'] = $this->complaint->getComplaintBanner();
        $this->render_page(strtolower(__CLASS__) . '/index',$data);
    }

    public function register()
    {
        
        $data['property_infringement'] =  $data['trade_dispute'] = null;
        $data['captcha'] = getCiCaptcha();
        $this->page_title = 'Complaint Register';
        $this->page_header = "Complaint Register";

        $post = $this->input->post();
        $complaint_option= isset($post['complaintoption'])?$post['complaintoption']:$this->session->userdata('c_option');
       
        if(!$complaint_option)
        {
            redirect('complaint_register');
        }

        if($complaint_option == "otherdispute")
        {
            $data['trade_dispute'] = $complaint_option = isset($post['dispute_type'])?$post['dispute_type']:null;
            $this->session->set_userdata('c_option', $complaint_option);
            $this->render_page(strtolower(__CLASS__) ,$data);
        }
        
        // else if($complaint_option == "otherinfrgmnt")
        //{
        //    $data['property_infringement'] = $complaint_option = isset($post['property_infringement'])?$post['property_infringement']:null;
         //   $this->session->set_userdata('c_option', $complaint_option);
         //   $this->render_page('property_infringement' ,$data);
        //}

        else
        {
            if(in_array($complaint_option,['Quantity not match with contract','Products not received after payment','Packaging not as agreed','Quality not as agreed']))
            {
                $data['trade_dispute'] = $complaint_option;
                $this->session->set_userdata('c_option', $complaint_option);
                $this->render_page(strtolower(__CLASS__) ,$data);
            }
            else
            {
                $data['property_infringement'] = $complaint_option;
                $this->session->set_userdata('c_option', $complaint_option);
                $this->render_page('property_infringement' ,$data);

            }

        }
 
      
    }
    public function register_complaint()
    {
       
        //PRINT_R($_POST);DIE();
        if ($this->input->post()) {
            if($_POST['captcha_code'] != $_POST['captcha']) {
                $this->session->set_flashdata('app_error', 'Invalid Captcha code');
                redirect($_SERVER['HTTP_REFERER']);
            }
            //$this->load->library('form_validation');

            $this->form_validation->set_rules('company_name', 'company_email', 'company_website', 'product_name', 'currency', 
            'transaction_amount','complaint_desc','contact_person', 'contact_email', 'contact_phone');
            $this->form_validation->run();
            if ( 1) {
                $_POST['user_id'] = $this->uid;
                if (isset($_FILES['attach_file']['name']) && !empty($_FILES['attach_file']['name'])) {
                    $this->load->library('upload');
                    $newFileName = microtime(true) . $_FILES['attach_file']['name'];

                    $config['upload_path'] = FILE_UPLOAD_PATH . 'complaints/';
                    $config['allowed_types'] = 'doc|docx|pdf|jpg|jpeg|png';
                    $config['file_name'] = $newFileName;
                    $config['max_size'] = 10500;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('attach_file') && !empty($_FILES['attach_file']['name'])) {
                        $up = $this->upload->display_errors();
                        $this->session->set_flashdata('app_error', $up);
                     //redirect($_SERVER['HTTP_REFERER']);
                     redirect($_SERVER['HTTP_REFERER']);
                    } else {
                        $data = $this->upload->data();
                        $attachment = isset($data['file_name']) ? $data['file_name'] : '';
                        $_POST['attachment_file'] = base_url() . 'assets/uploads/complaints/'.$attachment;
                     
                    }
                }
                unset($_POST['captcha_code']);
                unset($_POST['captcha']);
     
                if(empty($_POST['trade_dispute']))
                    $_POST['trade_dispute']= $this->session->userdata('c_option'); 
                 
             
                if ($_POST['c_id']=$this->complaint->insertCompalint($_POST)) {
                    
                    $_POST['cname'] = $_POST['trade_dispute'];

                              /* Mail to admin */
                              $this->sendMailtoAdmin($_POST);
                            /* Mail end */
                    
                    $this->session->set_flashdata('app_success', 'Complaint submitted, we will contact you soon!');
                    $this->page_title = 'Complaint Preview';
                    $this->page_header = "Complaint Preview";
                    //die(print_r($_POST));
                    $this->render_page(strtolower(__CLASS__) . '/complaint_preview',$_POST);
                } else {
                    $this->session->set_flashdata('app_error', 'Something went wrong');
                    redirect($_SERVER['HTTP_REFERER']);
                }
               
            } else {
             
                $this->session->set_flashdata('app_error', 'Please fill all required fields');
                redirect($_SERVER['HTTP_REFERER']);
            }
        } 
    }

        
    public function sendMailtoAdmin($mailInfo)
    {
          $admin_email = $this->complaint->getAdminEmail();
           $this->mail->isSMTP();
           $this->mail->Host = MAIL_HOST;
           $this->mail->SMTPAuth = true;
           $this->mail->Username = MAIL_USERNAME;
           $this->mail->Password = MAIL_PASSWORD;
           $this->mail->SMTPSecure = 'ssl';
           $this->mail->Port = 465;
           $this->mail->setFrom(FROM_MAIL, FROM_NAME);
           $this->mail->addReplyTo(REPLY_TO_MAIL, REPLY_TO_NAME);
           $this->mail->addAddress($admin_email);
           $this->mail->Subject = 'Complaint registration - '.$mailInfo['cname'];
           $this->mail->isHTML(true);
           $this->mail->Body = $this->load->view('complaint-register-mail-template', $mailInfo, true);
           $this->mail->send();
    }

    public function register_property_infringement()
    {
       
    
        if ($this->input->post()) {
     
        // print_r($_POST);
        // print_r($_FILES);DIE();
            //$this->load->library('form_validation');

            $this->form_validation->set_rules('company_name', 'company_email', 'company_website', 'product_name', 'currency', 
            'transaction_amount','complaint_desc','contact_person', 'contact_email', 'contact_phone');
            $this->form_validation->run();
            if ( 1) {
                $_POST['user_id'] = $this->uid;
                if (isset($_FILES['attach_file']['name']) && !empty($_FILES['attach_file']['name'])) {
                  
                    $this->load->library('upload');
                    $newFileName = microtime(true) . $_FILES['attach_file']['name'];

                    $config['upload_path'] = FILE_UPLOAD_PATH . 'complaints/';
                    $config['allowed_types'] = 'doc|docx|pdf|jpg|jpeg|png';
                    $config['file_name'] = $newFileName;
                    $config['max_size'] = 10500;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('attach_file') && !empty($_FILES['attach_file']['name'])) {
                        $up = $this->upload->display_errors();
                        $this->session->set_flashdata('app_error', $up);
                     //redirect($_SERVER['HTTP_REFERER']);
                     redirect($_SERVER['HTTP_REFERER']);
                    } else {
                        $data = $this->upload->data();
                        $attachment = isset($data['file_name']) ? $data['file_name'] : '';
                        $_POST['attachment_file'] = base_url() . 'assets/uploads/complaints/'.$attachment;
                     
                    }
                }

                if (isset($_FILES['ipr_attachment']['name']) && !empty($_FILES['ipr_attachment']['name'])) {
                    $this->load->library('upload');
                    $newFileName = microtime(true) . $_FILES['ipr_attachment']['name'];

                    $config['upload_path'] = FILE_UPLOAD_PATH . 'complaints/';
                    $config['allowed_types'] = 'doc|docx|pdf|jpg|jpeg|png';
                    $config['file_name'] = $newFileName;
                    $config['max_size'] = 10500;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('ipr_attachment') && !empty($_FILES['ipr_attachment']['name'])) {
                        $up = $this->upload->display_errors();
                        $this->session->set_flashdata('app_error', $up);
                     redirect($_SERVER['HTTP_REFERER']);
                    } else {
                        $data = $this->upload->data();
                        $attachment = isset($data['file_name']) ? $data['file_name'] : '';
                        $_POST['ipr_attachment'] = base_url() . 'assets/uploads/complaints/'.$attachment;
                     
                    }
                }

                if (isset($_FILES['ipr_poa']['name']) && !empty($_FILES['ipr_poa']['name'])) {
                    $this->load->library('upload');
                    $newFileName = microtime(true) . $_FILES['ipr_poa']['name'];

                    $config['upload_path'] = FILE_UPLOAD_PATH . 'complaints/';
                    $config['allowed_types'] = 'doc|docx|pdf|jpg|jpeg|png';
                    $config['file_name'] = $newFileName;
                    $config['max_size'] = 10500;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('ipr_poa') && !empty($_FILES['ipr_poa']['name'])) {
                        $up = $this->upload->display_errors();
                        $this->session->set_flashdata('app_error', $up);
                     redirect($_SERVER['HTTP_REFERER']);
                    } else {
                        $data = $this->upload->data();
                        $attachment = isset($data['file_name']) ? $data['file_name'] : '';
                        $_POST['ipr_poa'] = base_url() . 'assets/uploads/complaints/'.$attachment;
                     
                    }
                }


                unset($_POST['captcha_code']);
                unset($_POST['captcha']);
                unset($_POST['iprcheck']);
     
                if(empty($_POST['trade_dispute']))
                    $_POST['trade_dispute']= $this->session->userdata('c_option'); 
                 
                    //print_r($_POST);DIE();
                if ($_POST['c_id']=$this->complaint->insertpptyInfrngmntComplaint($_POST)) {
                    
                    $_POST['cname'] = $_POST['trade_dispute'];

                              /* Mail to admin */
                              $this->sendMailtoAdmin($_POST);
                            /* Mail end */
                    
                    $this->session->set_flashdata('app_success', 'Complaint submitted, we will contact you soon!');
                    $this->page_title = 'Complaint Preview';
                    $this->page_header = "Complaint Preview";
    
                    $this->render_page(strtolower(__CLASS__) . '/complaints_property_review',$_POST);
                } else {
                    $this->session->set_flashdata('app_error', 'Something went wrong');
                    redirect($_SERVER['HTTP_REFERER']);
                }
               
            } else {
              
                $this->session->set_flashdata('app_error', 'Please fill all required fields');
                redirect($_SERVER['HTTP_REFERER']);
            }
        } 
    }

   


    



    

}
