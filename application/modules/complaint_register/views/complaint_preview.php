<link rel="stylesheet" href="css/custom-min.css">

<section class="pt-3 grey-bg">
    <div class="container">
    <div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                    <div class="x_title">
                         <h2 class="heading2">Complaint Details</h2>
                         <div class="clearfix"></div>
                    </div>
                    
                    <div class="sucs">
                        <div class="sucs-container">
                          <div class="img">
                            <img src="images/icons/success-icon.png" alt="">
                          </div>
                          <div class="txt">
                            <h4>Complaint registered successfully!</h4>
                            <p>We will get back to you soon</p>
                          </div>
                        </div>
                      </div>

                    <div class="x_content comp-det">    
                                  
                     <td>
                     <h5>   <label>Complaint no</label>  <?php echo "CMPL".$c_id;?></h5> 
                    </td>
                    <br>
                    <td>
                        <h5>   <label>Complaint type</label>  <?php echo $trade_dispute;?></h5> 
                    </td>
                    <br>

                    <td>
                    <h5>  <label>Company name</label>  <?php echo $company_name;?></h5> 
                    </td>
                    <br>
                    <td>
                    <h5>  <label>Product name </label>  <?php echo $product_name;?></h5> 
                    </td>
                    <br>
                    <td>
                    <h5>  <label>Transaction amount</label>  <?php echo $currency ." ".$transaction_amount;?></h5> 
                    </td>
                    <br>
                    <td>
                    <h5>   <label>Company email</label>  <?php echo $company_email;?></h5>
                    </td>
                    <br>
                    <td>
                    <h5>  <label>Company website</label>  <?php echo $company_website;?></h5> 
                    </td><br>
                    <td>
                    <h5>  <label>Contact Person</label>  <?php echo $contact_person;?></h5> 
                    </td><br>
                    <td>
                    <h5>  <label>Contact phone</label>  <?php echo $contact_phone;?></h5> 
                    </td><br>
                    <td>
                    <h5>  <label>Contact email</label>  <?php echo $contact_email;?></h5> 
                    </td><br>
                  
                  
                    
                    <td>
                    <h5 class="w-100">  <label>Complaint details</label>  <?php echo $complaint_desc;?>  </h5> 
                    </td><br>
                    <?php if(isset($attachment_file)) { ?>
                    <td>
                    <h5>  <label>Attachments</label>  <a  href="<?php echo $attachment_file; ?>" target="_blank" >View</a></td>
                    </td><br>
                    <?php } ?>
                    <td>
                        
                        <a href="<?=base_url() ?>" class="btn btn-success btn-green mb-2">Back to Homepage</a>
                 
                                         
                  
                    </div>
               </div>
          </div>
     </div>
</div>
</div>
</section>

