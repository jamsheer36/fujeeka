
        <link rel="stylesheet" href="css/compliant_form.css">
<?php echo form_open_multipart($controller . "/register_complaint", array('class' => "mob-form", 'data-parsley-validate' => "true"))?>
     <div class="container-fluid complaintform_back">
     	<div class="container complaintform_cont">
     		<div class="row">
     			<div class="col-md-12">
     				<h4 class="heading2">Report a Trade Dispute</h4>
     			</div>
				<!--<div class="col-md-12"><hr></div>-->
     			
     			<div class="col-md-12">
     			<h2 class="heading3">The Respondent's Information</h2>
     			<div class="complaintform_form">
     			<div class="row">
     			
   	
     			<div class="col-md-4 fo required">
     			<p>Respondent's Company Name
</p>
					</div>
					<div class="col-md-8 fo">
					<input type="text" class="form-control" name="company_name" required data-parsley-required-message="Enter Company Name" value="">
					</div>
					
				<div class="col-md-4 fo required">
     			<p>Respondent's Email Address
</p>
					</div>
					<div class="col-md-8 fo required">
					<input type="text" class="form-control" name="company_email" data-parsley-type-message="Invalid  email format" data-parsley-type="email" required data-parsley-required-message="Enter Email" value="">
					</div>

   		<div class="col-md-4 fo required">
     			<p>Respondent's Company Website
</p>
					</div>
					<div class="col-md-8 fo required">
					<input type="text" class="form-control" name="company_website"  required data-parsley-required-message="Enter Company Website" value="">
					</div>
    		
    		
    		
    		</div>
     		</div>
     		</div>
     		
     		
     		
   
     		<div class="col-md-12">
     			<h2 class="heading3">Complaint Details</h2>
     			<div class="complaintform_form">
     			<div class="row">
     			
     			<div class="col-md-4 fo">
     			<p>Product Name
</p>
					</div>
					<div class="col-md-8 fo required">
					<input type="text" class="form-control" name="product_name"  required data-parsley-required-message="Enter Product Name" value="">
					</div>
					
				<div class="col-md-4 fo required">
     			<p>Transaction Amount
</p>
					</div>
					<div class="col-md-8 fo" data_liveedit_tagid="000000001EAF2790">
					
					<div class="input-group" data_liveedit_tagid="000000001EAF29F0">
  <div class="input-group-prepend" data_liveedit_tagid="000000001EAF2C50">
    <select name="currency" class="form-control" data_liveedit_tagid="000000001EAF2EB0">
                        <option class="option" value="USD" data_liveedit_tagid="000000001EAF3110">USD</option>
						<option class="option" value="EUR" data_liveedit_tagid="000000001EAF34A0">EUR</option>
						<option class="option" value="AUD" data_liveedit_tagid="000000001EAF3830">AUD</option>
						<option class="option" value="RMB" data_liveedit_tagid="000000001EAF3BC0">RMB</option>
						<option class="option" value="HKD" data_liveedit_tagid="000000001CCC7020">HKD</option>
                   </select>
  </div>
  <input type="text" class="form-control" name="transaction_amount" required data-parsley-pattern-message="Amount should be numeric" data-parsley-pattern="^[1-9]\d*(\.\d+)?$" data-parsley-required-message="Transaction Amount is required" value="">
</div>
				
				</div>

  		<div class="col-md-4 fo required">
     			<p>Complaint Description
</p>
					</div>
					<div class="col-md-8 fo required">
					<textarea name="complaint_desc" required data-parsley-required-message="Complaint Description is required" class="form-control" rows="5"></textarea>
					</div>
					
					
					
					<div class="col-md-4 fo required">
     			<p>Upload Evidence Files
</p>
					</div>
					<div class="col-md-8 fo required">
					<input type="file" required  data-parsley-required-message="Please attch document" data-parsley-errors-container="#file_err"  data-parsley-fileextension="doc,docx,pdf,jpg,jpeg" class="form-control" style="
    padding: .19rem .75rem;
"  id="attach_file" name="attach_file" /><span id="file_name"></span>  <div id="file_err"></div>
					<p style="margin-top: 10px;color: #444444">Required Evidences: First email from respondent, Communication Records (include emails or chat records from instant messenger), Payment Receipt or Documents.</p>
					<ul style="    color: #939393;">
					
<li> Other files: Proforma Invoice, Sales Contract, Product Photos, Inspection Documents from third parties and any other proof files.</li>
						
						<li> Upload doc,docx,pdf,jpg,jpeg,png format only.</li>
						
					</ul>
					</div>
					
					
   		
    		
    		
    		
    		</div>
     		</div>
     		</div>
     		
     		
     		
     		<div class="col-md-12">
     			<h2 class="heading3">Your Contact Details</h2>
     			<div class="complaintform_form">
     			<div class="row">
     			
     			<div class="col-md-4 fo required">
     			<p>Contact Person
</p>
					</div>
					<div class="col-md-8 fo">
					<input type="text" class="form-control" name="contact_person" required data-parsley-required-message="Please Enter Name" value="">
					</div>
					
				<div class="col-md-4 fo required">
     			<p>Email Address
</p>
					</div>
					<div class="col-md-8 fo">
					<input type="text" class="form-control" name="contact_email"  data-parsley-type-message="Invalid  email format" data-parsley-type="email" required data-parsley-required-message="Please Enter Email" value="">
					</div>

  		<div class="col-md-4 fo required">
     			<p>Telephone Number
</p>
					</div>
					<div class="col-md-8 fo required">
					
					<input type="text" class="form-control"  name="contact_phone" data-parsley-type-message="Phone should be numeric" data-parsley-type="number" required data-parsley-required-message="Please Enter Phone">
					
					</div>
					
					<input type="hidden" value="<?php  echo $trade_dispute; ?>"  name="trade_dispute">
				

					<div class="col-md-12"><hr></div>
					
					<div class="col-md-4 fo required">
     			<p>Verification Code
</p>
                    </div>
    
					<div class="col-md-8 fo">


                    
					<div class="row">
					<div class="col-md-4 bo required">
					<div class="input-group captcha-center ">
                                                            <div id="captcha_img">
                                                            <?php site_url() . '/assets/images/captcha/'.$captcha['image'];?>
                                                            <?php echo $captcha['image']; ?>
                                                         
                                                        </div>
                                                            <input value="<?=$captcha['word'] ?>" type="hidden" name="captcha_code" id="captcha_code" required data-parsley-required-message="Please Enter Captch" id="captcha_code">
															<div class="input-group-append">
                                                                 <a  class="btn btn-default btn-sm refresh_captcha">
                                                                      <i class="fas fa-redo"></i>
                                                                 </a>
                                                            </div>
                                                           
                                                       </div></div>
                          <div class="col-md-8">                             
					<input type="text" required data-parsley-required-message="Captcha is required"  class="form-control"  name="captcha" name="captcha_code" data-parsley-equalto="#captcha_code" data-parsley-equalto-message="Invalid captcha code">
						</div>
					</div>
					</div>
					
				
					
   		<div class="col-md-12 text-right"><button type="submit" class="btn btn-success btn-green btn-bold btnSupplierRegister">submit</button></div>
    		
    		
    		</div>
    		</div>
     		</div>
     		</div>
     		
     		
     		
     		
     		
     		</div>
     	</div>
		 <?php echo form_close()?>
		  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>


	<script>
	$(function() {
   
   $(document).on('click', '.refresh_captcha', function() {
	   $.ajax({
		   url: site_url + "user/refreshCaptcha",
		   method: "GET",
		   dataType: "json",
		 
		   success: function(data) {
			   $('#captcha_code').val('');
			   $('#captcha_img').html(data.data.image);
			   $('#captcha_code').val(data.data.word);
			   $('#captcha').val('');
		   }
	   });
   });
 
});

	</script>


<style>
.mob-form .form-control {
	border: 1px solid #ced4da;
}


  .autocomplete-suggestions {
    border: 1px solid #e4e4e4;
    background: #F4F4F4;
    cursor: default;
    overflow: auto
}

.autocomplete-suggestion {
    padding: 2px 5px;
    font-size: 1.2em;
    white-space: nowrap;
    overflow: hidden
}

.autocomplete-selected {
    background: #f0f0f0
}

.autocomplete-suggestions strong {
    color: #39f;
    font-weight: bolder
}
p.parsley-success {
    color: #468847;
    background-color: #DFF0D8;
    border: 1px solid #D6E9C6
}

p.parsley-error {
    color: #B94A48;
    background-color: #F2DEDE;
    border: 1px solid #EED3D7
}

ul.parsley-errors-list {
    list-style: none;
    color: #E74C3C;
    padding-left: 0
}

input.parsley-error,
select.parsley-error,
textarea.parsley-error {
    background: #FAEDEC;
    border: 1px solid #E85445
}

.btn-group .parsley-errors-list {
    display: none
}
</style>