<link rel="stylesheet" href="css/custom-min.css">

<section class="pt-3 grey-bg">
    <div class="container">
    <div class="right_col" role="main">
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
               <div class="x_title">
                         <h2 class="heading2">Complaint Details</h2>
                         <div class="clearfix"></div>
                    </div>
                    
                    <div class="sucs">
                        <div class="sucs-container">
                          <div class="img">
                            <img src="images/icons/success-icon.png" alt="">
                          </div>
                          <div class="txt">
                            <h4>Complaint registered successfully!</h4>
                            <p>We will get back to you soon</p>
                          </div>
                        </div>
                      </div>

                      <div class="x_title">
                         <h2 class="heading2">Complaint Details</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="x_content comp-det">    
                                  
                     <td>
                     <h5>   <label>Complaint no</label>  <?php echo "CMPLPI".$c_id;?></h5> 
                    </td>
                    <br>
                    <td>
                        <h5>   <label>Complaint type</label>  <?php echo $type;?></h5> 
                    </td>
                    <br>

                    <td>
                    <h5>  <label>Company name</label>  <?php echo $company_name;?></h5> 
                    </td>
                    <br>
                  
                    <br>
                    <td>
                    <h5>  <label>Complainant name</label>  <?php echo $complainant_name;?></h5> 
                    </td>
                    <br>
                    <td>
                    <h5>   <label>Complainant email</label>  <?php echo $complainant_email;?></h5>
                    </td>
                    <br>
                    <td>
                    <h5>  <label>Complainant phone</label>  <?php echo $complainant_phone;?></h5> 
                    </td><br>
                    <td>
                    <h5>  <label>Business registration Number</label>  <?php echo $b_id;?></h5> 
                    </td><br>

                    <h5 class="w-100">  <label>Complaint details</label>  <?php echo $complaint_desc;?>  </h5> 
                    </td><br>
                    <?php if(isset($attachment_file)) { ?>
                    <td>
                    <h5>  <label>Attachments</label>  <a  href="<?php echo $attachment_file; ?>" target="_blank" >View</a></td>
                    </td><br>
                    <?php } ?>



                    </td>
                    <br>
                    <?php if($ipr_name) {?>
                    <td>
                    <h5>  <label>IPR holder name</label>  <?php echo $ipr_name;?></h5> 
                    </td><br>
                    <td>
                    <h5>  <label> IPR business registration Number</label>  <?php echo $ipr_b_id;?></h5> 
                    </td><br>
                    <td>
                    <?php if(isset($ipr_attachment)) { ?>
                    <td>
                    <h5>  <label> Attachments</label>  <a  href="<?php echo $ipr_attachment; ?>" target="_blank" >View</a></td>
                    </td><br>
                    <?php } ?>

                    <?php if(isset($ipr_poa)) { ?>
                    <td>
                    <h5>  <label>Power of Attorney</label>  <a  href="<?php echo $ipr_poa; ?>" target="_blank" >View</a></td>
                    </td><br>
                    <?php } ?>
                    <td>
                    <?php }?>


                             <a href="<?=base_url() ?>" class="btn btn-success btn-green mb-2">Back to Home</a>              
                  
                    </div>

                    </div>
          </div>
     </div>
</div>
</div>
</section>

