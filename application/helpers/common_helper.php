<?php

  $CI = get_instance();

  /**
   * Function for crop image with jcrop
   * @param array $upload_data
   * @param array $postDatas
   * @return boolean
   */
  function crop($upload_data, $postDatas, $watermark = false) {
       $CI = & get_instance();

       $x1 = $postDatas['x1'];
       $x1 = isset($x1['0']) ? $x1['0'] : '';

       $x2 = $postDatas['x2'];
       $x2 = isset($x2['0']) ? $x2['0'] : '';

       $y1 = $postDatas['y1'];
       $y1 = isset($y1['0']) ? $y1['0'] : '';

       $y2 = $postDatas['y2'];
       $y2 = isset($y2['0']) ? $y2['0'] : '';

       $w = $postDatas['w'];
       $w = isset($w['0']) ? $w['0'] : '';

       $h = $postDatas['h'];
       $h = isset($h['0']) ? $h['0'] : '';

       $CI->load->library('image_lib');

       $image_config['image_library'] = 'gd2';
       $image_config['source_image'] = $upload_data["file_path"] . $upload_data["file_name"];
       $image_config['new_image'] = $upload_data["file_path"] . $upload_data["file_name"];
       $image_config['quality'] = "100%";
       $image_config['maintain_ratio'] = FALSE;
       $image_config['x_axis'] = $x1;
       $image_config['y_axis'] = $y1;
       $image_config['width'] = $w;
       $image_config['height'] = $h;

       $CI->image_lib->initialize($image_config);
       $CI->image_lib->crop();
       if ($watermark) {
            watermark($upload_data["file_path"] . $upload_data["file_name"]);
       }
       $CI->image_lib->clear();

       /* Second size */
       $configSize2['image_library'] = 'gd2';
       $configSize2['source_image'] = $upload_data["file_path"] . $upload_data["file_name"];
       $configSize2['create_thumb'] = TRUE;
     //   $configSize2['maintain_ratio'] = TRUE;
       $configSize2['quality'] = "20%";
       $width = get_settings_by_key('thumbnail_width');
       $height = get_settings_by_key('thumbnail_height');
       $configSize2['width'] = !empty($width) ? $width : DEFAULT_THUMB_W;
       $configSize2['height'] = !empty($height) ? $height : DEFAULT_THUMB_H;
       $configSize2['new_image'] = 'thumb_' . $upload_data["file_name"];

       $CI->image_lib->initialize($configSize2);
       $CI->image_lib->resize();
       $CI->image_lib->clear();
       if(stripos($upload_data["file_path"], '/product') !== false){
          $img = imagecreatefromstring(file_get_contents(FILE_UPLOAD_PATH.'product/thumb_' . $upload_data["file_name"]));           
               for ($x=1; $x<=20; $x++)
               {
                    imagefilter($img, IMG_FILTER_GAUSSIAN_BLUR);
               }
               imagejpeg($img,FILE_UPLOAD_PATH . 'product/' . 'thumb_'.$upload_data["file_name"]);
       }
       return true;
  }

  function watermark($source, $image = true) {
       global $CI;
       if ($image) {
            $config['source_image'] = $source;
            $config['new_image'] = $source;
            $config['wm_type'] = 'overlay';
            $config['wm_opacity'] = 50;
            $config['wm_vrt_alignment'] = 'bottom';
            $config['wm_hor_alignment'] = 'right';
            $config['wm_overlay_path'] = 'assets/images/watermark-sample.png';
       } else {
            $config['source_image'] = '/path/to/image/mypic.jpg';
            $config['wm_text'] = 'Copyright 2006 - John Doe';
            $config['wm_type'] = 'text';
            $config['wm_font_path'] = './system/fonts/texb.ttf';
            $config['wm_font_size'] = '16';
            $config['wm_font_color'] = 'ffffff';
            $config['wm_vrt_alignment'] = 'bottom';
            $config['wm_hor_alignment'] = 'center';
            $config['wm_padding'] = '20';
       }
       $CI->image_lib->initialize($config);
       $CI->image_lib->watermark();
       echo $CI->image_lib->display_errors();
  }

  function get_options($array, $parent = 0, $indent = "") {
       $return = array();
       foreach ($array as $key => $val) {
            if ($val["parent_category_id"] == $parent) {
                 $return["x" . $val["category_id"]] = $indent . $val["category_name"];
                 $return = array_merge($return, get_options($array, $val["category_id"], $indent . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
            }
       }
       return $return;
  }

  function getCategories() {
       global $CI;
       $CI->load->model('home_model');
       return $CI->home_model->getCategories();
  }

  function getParentCategories() {
       global $CI;
       $CI->load->model('home_model');
       return $CI->home_model->getParentCategories();
  }

  function debug($array = array(), $exit = 1) {
       echo '<pre>';
       print_r($array);
       if ($exit == 1)
            exit;
  }

  function getCaptcha() {
       $number1 = rand(1, 9999);
       $number2 = rand(1, 9999);
       return $number1 + $number2;
  }

  function getPaginationDesign() {
       $config['full_tag_open'] = '<ul class="pagination">';
       $config['full_tag_close'] = '</ul>';
       $config['prev_link'] = '&lt;';
       $config['prev_tag_open'] = '<li>';
       $config['prev_tag_close'] = '</li>';
       $config['next_link'] = '&gt;';
       $config['next_tag_open'] = '<li>';
       $config['next_tag_close'] = '</li>';
       $config['cur_tag_open'] = '<li class="active">';
       $config['cur_tag_close'] = '</li>';
       $config['num_tag_open'] = '<li>';
       $config['num_tag_close'] = '</li>';

       $config['first_tag_open'] = '<li>';
       $config['first_tag_close'] = '</li>';
       $config['last_tag_open'] = '<li>';
       $config['last_tag_close'] = '</li>';

       $config['first_link'] = '&laquo;';
       $config['last_link'] = '&raquo;';
       return $config;
  }

  function get_state_province($id = '', $countryId = '') {
       global $CI;
       if (!empty($id)) {
            $CI->db->where('stt_id', $id);
       }
       if (!empty($countryId)) {
            $CI->db->where('stt_country_id', $countryId);
       }
       $CI->db->order_by('stt_name');
       return $CI->db->select('*')->get(TABLE_PREFIX . 'states')->result_array();
  }

  function get_country_list($id = '') {
       global $CI;
       if (!empty($id)) {
            $CI->db->where('ctr_id', $id);
       }
       $CI->db->order_by('ctr_name');
       return $CI->db->select('*')->get(TABLE_PREFIX . 'countries')->result_array();
  }

  function get_hashed_password($pass) {
       if ($pass) {
            return base64_encode(base64_encode(base64_encode($pass)));
       }
  }

  function get_original_password($hash) {
       if ($hash) {
            return base64_decode(base64_decode(base64_decode($hash)));
       }
  }

  function check_login() {

       $CI = & get_instance();
       $userdata = $CI->session->userdata;
       if (isset($userdata['usr_user_id']) &&
               !empty($userdata['usr_user_id'])) {
            return true;
       } else {
            return false;
       }
  }

  /**
   * Return the value of logged user by specified field
   * @global object $CI
   * @param string $key
   * @return array
   */
  function get_logged_user($key = '') {
       $CI = & get_instance();
       if (@check_login()) {
            $id = $CI->session->userdata['usr_user_id'];
            if (empty($key)) {
                 return $CI->common_model->getUser($id);
            } else {
                 $userdata = $CI->common_model->getUser($id);
                 return isset($userdata[$key]) ? $userdata[$key] : '';
            }
       } else {
            return null;
       }
  }

  /* Settings */

  function get_settings_by_key($key) {
       if ($key) {
            global $CI;
            $CI->load->model('settings/settings_model');
            $settings = $CI->settings_model->getSettings($key);
            return isset($settings['set_value']) ? $settings['set_value'] : '';
       } else {
            return false;
       }
  }

  /* Settings */

  /**
   * Check is serialized data
   * @param serialized $data
   * @return boolean
   */
  function is_serialized($data) {
       return (is_string($data) && preg_match("#^((N;)|((a|O|s):[0-9]+:.*[;}])|((b|i|d):[0-9.E-]+;))$#um", $data));
  }

  function slugify($text) {

       // replace & to and
       $text = str_replace('&', 'and', $text);

       // replace non letter or digits by -
       $text = preg_replace('~[^\pL\d]+~u', '-', $text);

       // transliterate
       $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

       // remove unwanted characters
       $text = preg_replace('~[^-\w]+~', '', $text);

       // trim
       $text = trim($text, '-');

       // remove duplicate -
       $text = preg_replace('~-+~', '-', $text);

       // lowercase
       $text = strtolower($text);

       if (empty($text)) {
            return '';
       }

       return $text;
  }

  function facebook_time_ago($timestamp) {
       date_default_timezone_set('Asia/Kolkata');
       $time_ago = strtotime($timestamp);
       $current_time = time();
       $time_difference = $current_time - $time_ago;
       $seconds = $time_difference;
       $minutes = round($seconds / 60);           // value 60 is seconds  
       $hours = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
       $days = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
       $weeks = round($seconds / 604800);          // 7*24*60*60;  
       $months = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
       $years = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
       if ($seconds <= 60) {
            return "Just Now";
       } else if ($minutes <= 60) {
            if ($minutes == 1) {
                 return "one minute ago";
            } else {
                 return "$minutes minutes ago";
            }
       } else if ($hours <= 24) {
            if ($hours == 1) {
                 return "an hour ago";
            } else {
                 return "$hours hrs ago";
            }
       } else if ($days <= 7) {
            if ($days == 1) {
                 return "yesterday";
            } else {
                 return "$days days ago";
            }
       } else if ($weeks <= 4.3) { //4.3 == 52/12  
            if ($weeks == 1) {
                 return "a week ago";
            } else {
                 return "$weeks weeks ago";
            }
       } else if ($months <= 12) {
            if ($months == 1) {
                 return "a month ago";
            } else {
                 return "$months months ago";
            }
       } else {
            if ($years == 1) {
                 return "one year ago";
            } else {
                 return "$years years ago";
            }
       }
  }

  function generate_log($logData, $table = "general_log") {
       global $CI;
       $CI->common_model->generateLog($logData, $table);
  }

  function check_permission($controller, $method) {
       global $CI;
       return $CI->check_permission($controller, $method);
  }

  /**
   * 
   * @param string $action
   * @param string $string
   * @return If passing E get a encripted string, If passing D get a decripted string, 
   */
  function encryptor($string, $action = 'E') {
       //do the encyption given text/string/number
       if ($action == 'E') {
            $output = encode($string);
       } else if ($action == 'D') {
            $output = decode($string);
       }
       return $output;
  }

  define('skey', "SuPerEncKey20018");

  function safe_b64encode($string) {

       $data = base64_encode($string);
       $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
       return $data;
  }

  function safe_b64decode($string) {
       $data = str_replace(array('-', '_'), array('+', '/'), $string);
       $mod4 = strlen($data) % 4;
       if ($mod4) {
            $data .= substr('====', $mod4);
       }
       return base64_decode($data);
  }

  function encode($value) {

       if (!$value) {
            return false;
       }
       $text = $value;
       $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
       $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
       $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, skey, $text, MCRYPT_MODE_ECB, $iv);
       return trim(safe_b64encode($crypttext));
  }

  function decode($value) {

       if (!$value) {
            return false;
       }
       $crypttext = safe_b64decode($value);
       $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
       $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
       $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, skey, $crypttext, MCRYPT_MODE_ECB, $iv);
       return trim($decrypttext);
  }

  function get_snippet($str, $wordCount = 10) {

       $count = str_word_count($str);
       $word = implode(
               '', array_slice(
                       preg_split(
                               '/([\s,\.;\?\!]+)/', $str, $wordCount * 2 + 1, PREG_SPLIT_DELIM_CAPTURE
                       ), 0, $wordCount * 2 - 1
               )
       );
       return ($count > $wordCount) ? $word . '...' : $word;
  }

  function clean_image_name($name) {
       return preg_replace("/[^a-zA-Z.0-9]/", "", $name);
  }

  function is_root_user() {
       global $CI;
       if ($CI->uid == 1 || $CI->usr_grp == "SBA") {
            return true;
       } else {
            return false;
       }
  }

  function generate_vehicle_virtual_id($input) {
       if (!empty($input)) {
            $input = md5($input);
            $input = preg_replace("/[^0-9,.]/", "", $input);
            return substr($input, 0, 6);
       } else {
            return false;
       }
  }

  /**
   * Generate random number
   * @return int
   */
  function gen_random($length = 10) {
       $rand = round(microtime(true) * 1000) + rand(0, 9999999);
       return substr($rand, -$length);
  }

  /**
   * Clean the text and also remove -
   * @return int
   */
  function clean_text($name, $ucfirst = 1) {
       $name = trim(preg_replace("/\s+/", ' ', $name)); // Remove multiple space in between words.
       $name = str_replace('-', ' ', $name); // Replaces all hyphens with spaces.
       if ($ucfirst) {
            return ucfirst(preg_replace('/[^A-Za-z0-9\-]/', ' ', $name));
       }
       return preg_replace('/[^A-Za-z0-9\-]/', ' ', $name);
  }

  function resize($filename, $width, $height) {
       $extension = pathinfo($filename);
       global $CI;
       $config['new_image'] = $width . 'x' . $height . '_' . $extension['filename'] . '.' . $extension['extension'];
       if ($CI->load->is_loaded('image')) {
            $image = new Image($filename);
       } else {
            $image = $CI->load->library('image', $filename);
       }
       $image->resize($width, $height);
       $image->save($extension['dirname'] . '/' . $config['new_image']);
       return $extension['dirname'] . '/' . $config['new_image'];
  }

//   function resize($filename, $width, $height) {
//         $extension = pathinfo($filename);
//         global $CI;
//         $config['image_library'] = 'gd2';
//         $config['source_image'] = $filename;
//         $config['create_thumb'] = FALSE;
//         $config['maintain_ratio'] = FALSE;
//         $config['width']         = $width;
//         $config['height']       = $height;
//         $config['quality'] = "100%";
//         $config['new_image'] = $extension['filename'] . '_' . $width.'x'.$height.'.'.$extension['extension'];
//         if($CI->load->is_loaded('image_lib') ){
//                 $CI->image_lib->initialize($config);
//         }else{
//                 $CI->load->library('image_lib', $config);
//         }
//         $CI->image_lib->resize();
//         if ( ! $CI->image_lib->resize()){
//                 echo $CI->image_lib->display_errors();die();
//         }
//         else
//                 return $extension['dirname'].'/'.$config['new_image'];
//   }
  function pagination($page, $base_url, $count, $model, $method, $filter = '') {
       global $CI;
       if ($model == "market_model") {
            $CI->load->model('market/market_model');
       }
       $perpage = isset($filter['per_page'])?$filter['per_page']:36;
       $CI->load->library("pagination");
       $config = array();
       $config["base_url"] = $base_url;
       $config["total_rows"] = $count;
       $config["per_page"] =  $perpage;
       $config["uri_segment"] = 3;
       $config["use_page_numbers"] = TRUE;
       $config["full_tag_open"] = '<ul class="pagination">';
       $config["full_tag_close"] = '</ul>';
       $config["first_tag_open"] = '<li>';
       $config["first_tag_close"] = '</li>';
       $config["last_tag_open"] = '<li>';
       $config["last_tag_close"] = '</li>';
       $config['next_link'] = '&gt;';
       $config["next_tag_open"] = '<li>';
       $config["next_tag_close"] = '</li>';
       $config["prev_link"] = "&lt;";
       $config["prev_tag_open"] = "<li>";
       $config["prev_tag_close"] = "</li>";
       $config["cur_tag_open"] = "<li class='active'><a>";
       $config["cur_tag_close"] = "</a></li>";
       $config["num_tag_open"] = "<li>";
       $config["num_tag_close"] = "</li>";
       $config["num_links"] = 5;
       $CI->pagination->initialize($config);
       $start = ($page - 1) * $config["per_page"];
       $output = array(
           'pagination_link' => $CI->pagination->create_links(),
           'data' => $CI->$model->$method($config["per_page"], $start, $filter)
       );
       echo json_encode($output);
       die();
  }

  function getCiCaptcha($count = '') {
       global $CI;
       $CI->load->helper('captcha');
       $img_height = 38;
       $img_width = 150;
       $bg = array(255, 255, 255);
       $text_col = array(0, 0, 0);
       $grid_col = array(255, 255, 255);
       $border = array(206, 212, 218);
       if ($count) {
            $img_height = 64;
            $img_width = 100;
            $bg = array(0, 185, 4);
            $text_col = array(255, 255, 255);
            $grid_col = $border = $bg;
       }
       $config = array(
           'img_path' => './assets/uploads/captcha/',
           'img_url' => site_url() . '/assets/uploads/captcha/',
           'font_path' => './system/fonts/texb.ttf',
           'img_width' => $img_width,
           'img_height' => $img_height,
           'expiration' => 7200,
           'word_length' => 4,
           'font_size' => 16,
           'img_id' => 'Imageid',
           'img_class' => 'mx-auto d-block',
           'pool' => '123456789ABCDEFGHJKLMNPQRSTUVWXYZ',
           // White background and border, black text and red grid
           'colors' => array(
               'background' => $bg,
               'border' => $border,
               'text' => $text_col,
               'grid' => $grid_col
           )
       );
       if ($count) {
            for ($i = 1; $i <= $count; $i++) {
                 $captcha[] = create_captcha($config);
            }
            return $captcha;
       }

       return create_captcha($config);
  }

  /**
   * Check privilege exists purticular user
   * @global object $CI
   * @param string $grpslug
   * @param int $userid
   * @return bool
   * Author : JK
   */
  function privilege_exists($grpslug, $userid = '') {
       global $CI;
       return $CI->common_model->privilegeExists($grpslug, $userid);
  }

  /**
   * To show time ago label Eg: one minute ago, Just Now etc...
   * @param timestamp $timestamp
   * @return string
   * Author : JK
   */
  function get_timeago($timestamp) {
       $time_ago = strtotime($timestamp);
       $current_time = time();
       $time_difference = $current_time - $time_ago;
       $seconds = $time_difference;
       $minutes = round($seconds / 60);           // value 60 is seconds  
       $hours = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
       $days = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
       $weeks = round($seconds / 604800);          // 7*24*60*60;  
       $months = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
       $years = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
       if ($seconds <= 60) {
            return "Just Now";
       } else if ($minutes <= 60) {
            if ($minutes == 1) {
                 return "one minute ago";
            } else {
                 return "$minutes minutes ago";
            }
       } else if ($hours <= 24) {
            if ($hours == 1) {
                 return "an hour ago";
            } else {
                 return "$hours hrs ago";
            }
       } else if ($days <= 7) {
            if ($days == 1) {
                 return "yesterday";
            } else {
                 return "$days days ago";
            }
       } else if ($weeks <= 4.3) { //4.3 == 52/12  
            if ($weeks == 1) {
                 return "a week ago";
            } else {
                 return "$weeks weeks ago";
            }
       } else if ($months <= 12) {
            if ($months == 1) {
                 return "a month ago";
            } else {
                 return "$months months ago";
            }
       } else {
            if ($years == 1) {
                 return "one year ago";
            } else {
                 return "$years years ago";
            }
       }
  }

  function is_image($filename) {
       $name = explode('.', $filename);
       $name = isset($name['1']) ? $name['1'] : '';
       if (in_array($name, array('jpg', 'jpeg', 'gif', 'png', 'jpe'))) {
            return true;
       }
       return false;
  }

  function chat_time($timestamp) {
       $time_ago = strtotime($timestamp);
       $current_time = time();
       $time_difference = $current_time - $time_ago;
       $seconds = $time_difference;
       $days = round($seconds / 86400); //86400 = 24 * 60 * 60;
       if ($days == 0) {
            return date("g:i A", $time_ago);
       } if ($days == 1) {
            return "Yesterday";
       } else {
            return date("d-m-Y", $time_ago);
       }
  }

  function get_buying_config($type) {
       global $CI;
       return $CI->common_model->getBuyingConfig($type);
  }
  
  function get_supplier_user($supId, $key = '') {
       
       global $CI;
       if(!empty($supId)) {
            $data = $CI->common_model->getSupplierUser($supId);
            if(!empty($key)) {
                 return isset($data[$key]) ? $data[$key] : '';
            }
            return $data;
       }
       return null;
  }
  
  function can_access_module($module) {
       global $CI;
       if ($CI->usr_grp == 'SA' || $CI->uid == 1) {
            return true;
       } else if(!empty ($module)){
            if (is_serialized($CI->userAccess)) {
                 $access = unserialize($CI->userAccess);
                 if(key_exists($module, $access)){
                      return true;
                 } else {
                      return false;
                 }
            } else {
                 return false;
            }
       } else {
            return false;
       }
  }
  function switchLang($language = "") {
       global $CI;
       $language = ($language != "") ? $language : "english";
       $CI->session->set_userdata('fujeeka_lang', $language);
       redirect($_SERVER['HTTP_REFERER']);
  }
  function fujeekalang($key){
     global $CI;
     $lang = $CI->lang->line($key);
     if(!$lang){
           $lang = ucfirst(str_replace("_", " ", $key));
     }
     return $lang;
  }
?>