/*
* ----------------------------------------------------------------------------------------
  Scripts carried from products page
* ----------------------------------------------------------------------------------------
*/
    /*----------------------------------------------------*/
  	/* inner header sticky
  	/*----------------------------------------------------*/
  	$(window).on('scroll', function () {
  		if ($(window).scrollTop() > 100) {
  			$('.inner-head').addClass('stick-menu');
        $ ( '.logo img' ).attr('src','assets/images/fujeeka-logo-green.png');
  		} else {
  			$('.inner-head').removeClass('stick-menu');
        	$ ( '.logo img' ).attr('src','assets/images/fujeeka-logo-g.png');
  		}
  	});


    /*----------------------------------------------------*/
    /* Search bar
    /*----------------------------------------------------*/

    $(document).ready(function(e){
      $('.search-panel .dropdown-menu').find('a').click(function(e) {
      e.preventDefault();
      var param = $(this).attr("href").replace("#","");
      var concept = $(this).text();
      $('.search-panel span#search_concept').text(concept);
      $('.input-group #search_param').val(param);
    });
    });


    /*------------------------------------------------------*/
    /* list / grid view
    /*-----------------------------------------------------*/

      // Get the elements with class="column"
      var elements = document.getElementsByClassName("single-prod");

      // Declare a loop variable
      var i;

      // List View
      function listView() {
          for (i = 0; i < elements.length; i++) {
            $(".single-prod").addClass("list-style");
            $(".single-prod").removeClass("grid-style");
          }
      }

      // Grid View
      function gridView() {
          for (i = 0; i < elements.length; i++) {
          $(".single-prod").removeClass("list-style");
          $(".single-prod").addClass("grid-style");
          }
      }


      /*------------------------------------------------------*/
      /* tooltip
      /*-----------------------------------------------------*/

      $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
